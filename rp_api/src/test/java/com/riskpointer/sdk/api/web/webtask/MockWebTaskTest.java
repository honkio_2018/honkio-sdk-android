package com.riskpointer.sdk.api.web.webtask;

import com.riskpointer.sdk.api.exception.BadStatusCodeException;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.protocol.json.JsonRequestBuilder;
import com.riskpointer.sdk.api.web.protocol.json.SimpleResponseJsonParser;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * Created by Shurygin Denis on 2016-11-17.
 */
@Config(manifest=Config.NONE)
@RunWith(RobolectricTestRunner.class)
public class MockWebTaskTest {

    @Test
    public void testMockWebTask() throws IOException, BadStatusCodeException {
        String responseString = "test String";
        MockWebTask<Void> webTask = new MockWebTask<>(RuntimeEnvironment.application, buildTestMessage(),
                responseString, 0);
        assertEquals(responseString, webTask.sendWebRequest(null));
    }

    private Message<Void> buildTestMessage() {
        return new Message<>(RuntimeEnvironment.application, "test", "url", null, null,
                new Identity("id", "password"), new Identity("id", "password"),
                new JsonRequestBuilder(), new SimpleResponseJsonParser(), 0);
    }
}
