package com.riskpointer.sdk.api.web;

import android.content.Context;

import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.protocol.json.JsonRequestBuilder;
import com.riskpointer.sdk.api.web.protocol.json.SimpleResponseJsonParser;
import com.riskpointer.sdk.api.web.response.Response;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by Shurygin Denis on 2016-11-16.
 */
@RunWith(RobolectricTestRunner.class)
public class ApiCacheTest {

    static class TestSerializableObject implements Serializable {

        private String mString;
        private int mInt;

        TestSerializableObject(String stringValue, int intValue) {
            mString = stringValue;
            mInt = intValue;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof TestSerializableObject) {
                TestSerializableObject testObj = (TestSerializableObject) obj;
                return mString.equals(testObj.mString) && mInt == testObj.mInt;
            }
            return super.equals(obj);
        }
    }

    @Rule
    public TemporaryFolder tempFolder= new TemporaryFolder();

    @Test
    public void testPutGet() throws IOException {
        TestSerializableObject testObject = new TestSerializableObject("Test string", 23);

        File file = tempFolder.newFile();
        ApiCache.put(file, testObject);

        TestSerializableObject cachedObject = (TestSerializableObject) ApiCache.get(file);

        assertEquals(testObject, cachedObject);
    }

    @Test
    public void testPutGetWebResponse() throws IOException {
        String testFileName = "testFile";
        TestSerializableObject testObject = new TestSerializableObject("Test string2", 123);
        Response<TestSerializableObject> response = new Response<>(testObject);

        ApiCache.putWebResponse(RuntimeEnvironment.application, response, testFileName, false);
        assertEquals(response,
                ApiCache.getWebResponse(RuntimeEnvironment.application, testFileName, false));

        assertNull(ApiCache.getWebResponse(RuntimeEnvironment.application, testFileName, true));
        assertNull(ApiCache.getApi(RuntimeEnvironment.application, testFileName, false));
        assertNull(ApiCache.getApi(RuntimeEnvironment.application, testFileName, true));

        // Authorized cache
        testFileName = "authorizedTestFile";
        ApiCache.putWebResponse(RuntimeEnvironment.application, response, testFileName, true);
        assertEquals(response,
                ApiCache.getWebResponse(RuntimeEnvironment.application, testFileName, true));

        assertNull(ApiCache.getWebResponse(RuntimeEnvironment.application, testFileName, false));
        assertNull(ApiCache.getApi(RuntimeEnvironment.application, testFileName, false));
        assertNull(ApiCache.getApi(RuntimeEnvironment.application, testFileName, true));
    }

    @Test
    public void testPutWebResponseNotAuthorisedResponse() throws IOException {
        Response<Void> response = new Response<>(null);
        //Nothing happen
        ApiCache.putWebResponse(RuntimeEnvironment.application, response);

        Message<Void> message = new Message<>(RuntimeEnvironment.application, "testCommand", "url", null,
                null, new Identity("shop_id", "pass"), new Identity("app", "pass"),
                new JsonRequestBuilder(), new SimpleResponseJsonParser(), 0);
        response.setMessage(message);

        //Nothing happen
        ApiCache.putWebResponse(RuntimeEnvironment.application, response);
        assertNull(ApiCache.getWebResponse(RuntimeEnvironment.application, "testCommand", false));
        assertNull(ApiCache.getWebResponse(RuntimeEnvironment.application, "testCommand", true));

        message.setCacheFileName("testCommand", false);
        ApiCache.putWebResponse(RuntimeEnvironment.application, response);
        assertEquals(response,
                ApiCache.getWebResponse(RuntimeEnvironment.application, "testCommand", false));

        // If message has parameter LOGIN_IDENTITY cache should be authorized
        message.addParameter(Message.Param.LOGIN_IDENTITY, "login");
        message.setCacheFileName("testCommand2", false);
        ApiCache.putWebResponse(RuntimeEnvironment.application, response);
        assertEquals(response,
                ApiCache.getWebResponse(RuntimeEnvironment.application, "testCommand2", true));

        // If message has parameter USER_EMAIL cache should be authorized
        message.addParameter(Message.Param.USER_EMAIL, "login");
        message.setCacheFileName("testCommand3", false);
        ApiCache.putWebResponse(RuntimeEnvironment.application, response);
        assertEquals(response,
                ApiCache.getWebResponse(RuntimeEnvironment.application, "testCommand3", true));
    }

    @Test
    public void testPutWebResponseAuthorisedResponse() throws IOException {
        Response<Void> response = new Response<>(null);

        Message<Void> message = new Message<>(RuntimeEnvironment.application, "testCommand", "url",
                new Connection(RuntimeEnvironment.application
                        .getSharedPreferences("prefs", Context.MODE_PRIVATE),
                        "log", "pass", "1234"), null,
                new Identity("shop_id", "pass"), new Identity("app", "pass"),
                new JsonRequestBuilder(), new SimpleResponseJsonParser(), 0);
        response.setMessage(message);

        //Nothing happen
        ApiCache.putWebResponse(RuntimeEnvironment.application, response);
        assertNull(ApiCache.getWebResponse(RuntimeEnvironment.application, "testCommand", false));
        assertNull(ApiCache.getWebResponse(RuntimeEnvironment.application, "testCommand", true));

        message.setCacheFileName("testCommand", false);
        ApiCache.putWebResponse(RuntimeEnvironment.application, response);
        assertEquals(response,
                ApiCache.getWebResponse(RuntimeEnvironment.application, "testCommand", true));
    }

    @Test
    public void testPutGetApi() throws IOException {
        String testFileName = "testFile";
        TestSerializableObject testObject = new TestSerializableObject("Test string2", 123);

        ApiCache.putApi(RuntimeEnvironment.application, testObject, testFileName, false);
        assertEquals(testObject, ApiCache.getApi(RuntimeEnvironment.application, testFileName, false));

        assertNull(ApiCache.getWebResponse(RuntimeEnvironment.application, testFileName, false));
        assertNull(ApiCache.getWebResponse(RuntimeEnvironment.application, testFileName, true));
        assertNull(ApiCache.getApi(RuntimeEnvironment.application, testFileName, true));

        // Authorized cache
        testFileName = "authorizedTestFile";
        ApiCache.putApi(RuntimeEnvironment.application, testObject, testFileName, true);
        assertEquals(testObject, ApiCache.getApi(RuntimeEnvironment.application, testFileName, true));

        assertNull(ApiCache.getWebResponse(RuntimeEnvironment.application, testFileName, false));
        assertNull(ApiCache.getWebResponse(RuntimeEnvironment.application, testFileName, true));
        assertNull(ApiCache.getApi(RuntimeEnvironment.application, testFileName, false));
    }

    @Test
    public void testRemoveApiFile() {
        String testFileName = "testFile";
        TestSerializableObject testObject = new TestSerializableObject("Test string2", 123);

        ApiCache.putApi(RuntimeEnvironment.application, testObject, testFileName, false);
        ApiCache.removeApiFile(RuntimeEnvironment.application, testFileName, false);
        assertNull(ApiCache.getApi(RuntimeEnvironment.application, testFileName, false));

        ApiCache.putApi(RuntimeEnvironment.application, testObject, testFileName, true);
        ApiCache.removeApiFile(RuntimeEnvironment.application, testFileName, true);
        assertNull(ApiCache.getApi(RuntimeEnvironment.application, testFileName, true));
    }

    //FIXME test will work only on real device. ApiCache.clear uses deletion via bash command 'rm'.
    public void testClear() {
        String testFileName = "testFile";
        TestSerializableObject testObject = new TestSerializableObject("Test string2", 123);
        Response<TestSerializableObject> response = new Response<>(testObject);

        ApiCache.putApi(RuntimeEnvironment.application, testObject, testFileName, false);
        ApiCache.putApi(RuntimeEnvironment.application, testObject, testFileName, true);
        ApiCache.putWebResponse(RuntimeEnvironment.application, response, testFileName, false);
        ApiCache.putWebResponse(RuntimeEnvironment.application, response, testFileName, true);

        ApiCache.clearAuthorized(RuntimeEnvironment.application);

        assertNull(ApiCache.getApi(RuntimeEnvironment.application, testFileName, true));
        assertNull(ApiCache.getWebResponse(RuntimeEnvironment.application, testFileName, true));

        assertEquals(testObject, ApiCache.getApi(RuntimeEnvironment.application, testFileName, false));
        assertEquals(response,
                ApiCache.getWebResponse(RuntimeEnvironment.application, testFileName, false));

    }
}
