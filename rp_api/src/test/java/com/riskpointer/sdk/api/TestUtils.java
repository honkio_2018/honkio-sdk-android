package com.riskpointer.sdk.api;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

/**
 * Created by Shurygin Denis on 2016-11-16.
 */
public class TestUtils {

    public static byte[] serialize(Object object) throws IOException {
        ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
        ObjectOutput objectOutput = new ObjectOutputStream(byteOutStream);
        objectOutput.writeObject(object);

        return byteOutStream.toByteArray();
    }

    public static Object deserialize(byte[] byteArray) throws IOException, ClassNotFoundException {
        ByteArrayInputStream byteInputStream = new ByteArrayInputStream(byteArray);
        ObjectInput objectInput = new ObjectInputStream(byteInputStream);
        return objectInput.readObject();
    }
}
