package com.riskpointer.sdk.api.model.entity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Shurygin Denis on 2016-11-18.
 */
@RunWith(RobolectricTestRunner.class)
public class RateTest {

    @Test
    public void testEquals() {
        assertEquals(new Rate(2, 1), new Rate(2, 1));
        assertEquals(new Rate(-20000, 1), new Rate(-20000, 1));
        assertEquals(new Rate(2, -15001), new Rate(2, -15001));

        assertNotEquals(new Rate(0, 0), new Rate(0, 0){});
        assertNotEquals(new Rate(2, 1), new Rate(2, 2));
        assertNotEquals(new Rate(-20000, 3), new Rate(-20000, 1));
        assertNotEquals(new Rate(5, -15001), new Rate(2, -15001));
    }

    @Test
    public void testPositiveRatesCount() {
        ArrayList<Rate> list = buildRatesList();
        assertEquals(45, Rate.positiveRatesCount(list));
        list.add(new Rate(2, 5));
        assertEquals(50, Rate.positiveRatesCount(list));
        list.add(new Rate(-2, 5));
        assertEquals(50, Rate.positiveRatesCount(list));
    }

    @Test
    public void testNegativeRatesCount() {
        ArrayList<Rate> list = buildRatesList();
        assertEquals(26, Rate.negativeRatesCount(list));
        list.add(new Rate(2, 5));
        assertEquals(26, Rate.negativeRatesCount(list));
        list.add(new Rate(-2, 5));
        assertEquals(31, Rate.negativeRatesCount(list));
    }


    private ArrayList<Rate> buildRatesList() {
        return new ArrayList<>(Arrays.asList(
                new Rate(-1, 1),
                new Rate(-2, 20),
                new Rate(Integer.MIN_VALUE, 5),
                new Rate(0, 100),
                new Rate(1, 15),
                new Rate(2, 15),
                new Rate(Integer.MAX_VALUE, 15)
        ));
    }
}
