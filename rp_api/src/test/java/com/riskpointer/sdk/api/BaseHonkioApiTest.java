package com.riskpointer.sdk.api;

import android.content.Context;
import android.content.SharedPreferences;

import com.riskpointer.sdk.api.exception.WrongKeyException;
import com.riskpointer.sdk.api.model.entity.AppInfo;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.MerchantSimple;
import com.riskpointer.sdk.api.model.entity.ServerInfo;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.ShopInfo;
import com.riskpointer.sdk.api.model.entity.merchant.Merchant;
import com.riskpointer.sdk.api.testclasses.TestErrorCallback;
import com.riskpointer.sdk.api.utils.ApiCryptoUtils;
import com.riskpointer.sdk.api.web.ApiCache;
import com.riskpointer.sdk.api.web.Connection;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.protocol.json.JsonProtocolFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.robolectric.RuntimeEnvironment.application;

/**
 * Created by Shurygin Denis on 2016-11-21.
 */
@RunWith(RobolectricTestRunner.class)
public class BaseHonkioApiTest {

    private static final String PIN = "1234";

    private BaseApiState apiState;

    @Before
    public void init() {
        BaseHonkioApi.reset();
        prepareApiCache();
        prepareConnectionCache("login", "pas123", PIN);
        apiState = new BaseApiState(application,
                "http://url.url", new Identity("id", "pass"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInitWithoutState() {
        BaseHonkioApi.initInstance(null, new JsonProtocolFactory());
    }

    @Test(expected = IllegalStateException.class)
    public void testInitTwice() {
        BaseHonkioApi.initInstance(apiState, null);
        BaseHonkioApi.initInstance(apiState, null);
    }

    @Config(manifest = Config.NONE)
    @Test(expected = SecurityException.class)
    public void testInitWithoutPermissions() {
        BaseHonkioApi.initInstance(apiState, null);
    }

    @Test
    public void testNotInitializedApi() {
        assertFalse(BaseHonkioApi.isInitialised());
        assertFalse(BaseHonkioApi.isConnected());
        assertNull(BaseHonkioApi.getServerInfo());
        assertNull(BaseHonkioApi.getAppInfo());
        assertNull(BaseHonkioApi.getMainShopInfo());
        assertNull(BaseHonkioApi.getMainShopIdentity());
        assertNull(BaseHonkioApi.getContext());
        assertNull(BaseHonkioApi.getUrl());
        assertNull(BaseHonkioApi.getAppIdentity());
        assertNull(BaseHonkioApi.getApiPreferences());
        assertNull(BaseHonkioApi.getActiveConnection());
        assertNotNull(BaseHonkioApi.getProtocolFactory());
    }

    @Test
    public void testInitializedApi() {
        BaseHonkioApi.initInstance(apiState, null);
        assertTrue(BaseHonkioApi.isInitialised());
        assertFalse(BaseHonkioApi.isConnected());
        assertNotNull(BaseHonkioApi.getServerInfo());
        assertNotNull(BaseHonkioApi.getAppInfo());
        assertNotNull(BaseHonkioApi.getMainShopInfo());
        assertNotNull(BaseHonkioApi.getMainShopIdentity());
        assertNotNull(BaseHonkioApi.getContext());
        assertNotNull(BaseHonkioApi.getUrl());
        assertNotNull(BaseHonkioApi.getAppIdentity());
        assertNotNull(BaseHonkioApi.getApiPreferences());
        assertNotNull(BaseHonkioApi.getActiveConnection());
        assertNotNull(BaseHonkioApi.getProtocolFactory());

        BaseHonkioApi.getActiveConnection().changePin(PIN);
        assertTrue(BaseHonkioApi.isConnected());
    }

    @Test
    public void testCheckInitialisation() {
        TestErrorCallback callback = new TestErrorCallback(1, RpError.Api.NO_INITIALIZED);
        assertFalse(BaseHonkioApi.checkInitialisation(callback));
        assertEquals(0, callback.getCountdownValue());

        BaseHonkioApi.initInstance(apiState, null);

        callback = new TestErrorCallback(0);
        assertTrue(BaseHonkioApi.checkInitialisation(callback));
    }

    @Test
    public void testCheckConnection() {
        TestErrorCallback callback = new TestErrorCallback(1, RpError.Api.NO_LOGIN);
        assertFalse(BaseHonkioApi.checkConnection(callback));
        assertEquals(0, callback.getCountdownValue());

        BaseHonkioApi.initInstance(apiState, null);
        callback = new TestErrorCallback(1, RpError.Api.ABORTED);
        assertFalse(BaseHonkioApi.checkConnection(callback));
        assertEquals(0, callback.getCountdownValue());

        BaseHonkioApi.getActiveConnection().changePin(PIN);
        callback = new TestErrorCallback(0);
        assertTrue(BaseHonkioApi.checkConnection(callback));
        assertEquals(0, callback.getCountdownValue());

        BaseHonkioApi.getActiveConnection().setToInvalid();
        callback = new TestErrorCallback(1, RpError.Api.NO_LOGIN);
        assertFalse(BaseHonkioApi.checkConnection(callback));
        assertEquals(0, callback.getCountdownValue());
    }

    @Test
    public void testInitConnectionFromCache() throws WrongKeyException {
        Connection connection = BaseHonkioApi.initConnectionFromCache(
                RuntimeEnvironment.application.getSharedPreferences(
                        BaseHonkioApi.PREFERENCE_FILE, Context.MODE_PRIVATE), PIN);
        assertNotNull(connection);
        assertTrue(connection.isValid());
        assertFalse(connection.isEncrypted());
    }

    @Test
    public void testInitConnectionFromCacheNoPin() throws WrongKeyException {
        Connection connection = BaseHonkioApi.initConnectionFromCache(
                RuntimeEnvironment.application.getSharedPreferences(
                        BaseHonkioApi.PREFERENCE_FILE, Context.MODE_PRIVATE), null);
        assertNotNull(connection);
        assertTrue(connection == BaseHonkioApi.getActiveConnection());
        assertFalse(connection.isValid());
        assertTrue(connection.isEncrypted());
    }

    @Test(expected = WrongKeyException.class)
    public void testInitConnectionFromCacheWrongPin() throws WrongKeyException {
        BaseHonkioApi.initConnectionFromCache(
                RuntimeEnvironment.application.getSharedPreferences(
                        BaseHonkioApi.PREFERENCE_FILE, Context.MODE_PRIVATE), PIN + "_wrong");
    }

    private void prepareConnectionCache(String login, String password, String pin) {
        SharedPreferences preferences = RuntimeEnvironment.application.getSharedPreferences(
                BaseHonkioApi.PREFERENCE_FILE, Context.MODE_PRIVATE);
        preferences.edit()
                .putString(Connection.LOGIN, login)
                .putString(Connection.PASSWORD, ApiCryptoUtils.encrypt(password, pin))
                .putLong(Connection.PIN_FAILS, 0).apply();
    }

    private void prepareApiCache() {
        ApiCache.putApi(application, buildServerInfo(),
                Message.Command.SERVER_INFO, false);

        ApiCache.putApi(application, buildAppInfo(),
                Message.Command.APP_INFO, false);

        ApiCache.putApi(application, buildShopInfo(),
                Message.Command.SHOP_INFO, false);
    }

    private ServerInfo buildServerInfo() {
        ServerInfo serverInfo = new ServerInfo();
        serverInfo.setVersion(2);
        serverInfo.setOAuthTokenUrl("url");
        return serverInfo;
    }

    private AppInfo buildAppInfo() {
        AppInfo appInfo = new AppInfo();
        appInfo.setShopIdentity(new Identity("shopId", "pass"));
        appInfo.setOauthIdentity(new Identity("key", "password"));
        return appInfo;
    }

    private ShopInfo buildShopInfo() {
        MerchantSimple merchant = new Merchant();
        merchant.setId("123123");
        merchant.setBusinessId("1232139309808435");
        merchant.setName("Merchant");

        Shop shop = new Shop();
        shop.setId("shopId");
        shop.setPassword("pass123");
        shop.setName("Some shop");

        ShopInfo shopInfo = new ShopInfo();
        shopInfo.setTimeStamp(2000012);
        shopInfo.setMerchant(merchant);
        shopInfo.setShop(shop);
        return shopInfo;
    }
}
