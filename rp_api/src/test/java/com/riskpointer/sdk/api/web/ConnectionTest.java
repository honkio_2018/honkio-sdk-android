package com.riskpointer.sdk.api.web;

import android.content.Context;
import android.content.SharedPreferences;

import com.riskpointer.sdk.api.exception.WrongKeyException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Shurygin Denis on 2016-11-15.
 */
@RunWith(RobolectricTestRunner.class)
public class ConnectionTest {

    private SharedPreferences preferences;

    @Before
    public void init() {
        preferences = RuntimeEnvironment.application
                .getSharedPreferences("prefFile", Context.MODE_PRIVATE);
    }

    @Test
    public void testConstructor() throws Exception {
        Connection connection = new Connection(preferences, "test_login", "123123", "122822121");

        assertEquals("test_login", connection.getLogin());
        assertEquals("123123", connection.getOtp());
        assertTrue(connection.isValid());
        assertTrue(connection.checkPin("122822121"));
        Thread.currentThread().getStackTrace();
    }

    @Test
    public void testConstructorWithPin() throws Exception {
        Connection connection = new Connection(preferences, "test_login", "123123", "correct");
        // Set new password to init writing to cache
        connection.setOtp("new_password");

        connection = new Connection(preferences, "correct");
        assertEquals("test_login", connection.getLogin());
        assertEquals("new_password", connection.getOtp());
        assertTrue(connection.isValid());

        connection = new Connection(preferences, null);
        assertTrue(connection.isEncrypted());
        assertFalse(connection.isValid());

        assertFalse(connection.changePin("wrong"));
        assertTrue(connection.isEncrypted());
        assertFalse(connection.isValid());

        assertTrue(connection.changePin("correct"));
        assertFalse(connection.isEncrypted());
        assertTrue(connection.isValid());
    }


    @Test(expected = WrongKeyException.class)
    public void testConstructorWithPinException() throws Exception {
        Connection connection = new Connection(preferences, "test_login", "123123", "correct");
        // Set new password to init writing to cache
        connection.setOtp("new_password");

        new Connection(preferences, "wrong");
    }

    @Test
    public void testCheckPin() throws Exception {
        Connection connection = new Connection(preferences, "test_login", "123123", "correct");

        assertTrue(connection.checkPin("correct"));
        assertEquals(0, connection.getPinFailsCount());

        assertFalse(connection.checkPin("wrong"));
        assertEquals(1, connection.getPinFailsCount());

        assertTrue(connection.checkPin("correct"));
        assertEquals(0, connection.getPinFailsCount());

        assertEquals("test_login", connection.getLogin());
        assertEquals("123123", connection.getOtp());
        assertTrue(connection.isValid());

        for (int i = 0; i < Connection.MAX_PIN_FAIL_COUNT; i++) {
            assertFalse(connection.checkPin("wrong"));
            assertEquals(i + 1, connection.getPinFailsCount());
        }

        assertEquals("test_login", connection.getLogin());
        assertNull(connection.getOtp());
        assertFalse(connection.isValid());
        // For invalid connection pin check always false
        assertFalse(connection.checkPin("correct"));

        // If valid connection pin is null pin check always true.
        connection = new Connection(preferences, "test_login", "123123", null);
        assertTrue(connection.checkPin("wrong"));
        assertTrue(connection.checkPin(null));
    }

    @Test
    public void testChangePin() throws Exception {
        Connection connection = new Connection(preferences, "test_login", "123123", null);

        assertTrue(connection.changePin(null, "new_correct"));
        assertTrue(connection.changePin("another"));

        assertFalse(connection.changePin("wrong", "another"));
        assertTrue(connection.changePin("another", "new_another"));

        for (int i = 0; i < Connection.MAX_PIN_FAIL_COUNT; i++) {
            assertFalse(connection.changePin("wrong", "new"));
        }
        assertFalse(connection.isValid());
    }

    @Test
    public void testIncreasePinFails() throws Exception {
        Connection connection = new Connection(preferences, "test_login", "123123", "correct");

        for (int i = 1; i < Connection.MAX_PIN_FAIL_COUNT; i++) {
            connection.increasePinFails();
            assertEquals(i, connection.getPinFailsCount());
            assertTrue(connection.isValid());
        }

        connection.increasePinFails();
        assertEquals(Connection.MAX_PIN_FAIL_COUNT, connection.getPinFailsCount());

        assertEquals("test_login", connection.getLogin());
        assertFalse(connection.isValid());
        assertNull(connection.getOtp());
        assertFalse(connection.checkPin("correct"));
    }

    @Test
    public void testExit() throws Exception {
        Connection connection = new Connection(preferences, "test_login", "123123", "correct");

        connection.exit();

        assertEquals("test_login", connection.getLogin());
        assertFalse(connection.isValid());
        assertNull(connection.getOtp());
        assertFalse(connection.checkPin("correct"));
    }

    @Test
    public void testSetToInvalid() throws Exception {
        Connection connection = new Connection(preferences, "test_login", "123123", "correct");
        connection.setToInvalid();
        assertFalse(connection.isValid());
        assertNotNull(connection.getLogin());
        assertNotNull(connection.getOtp());

        // For invalid connection pin check always false
        assertFalse(connection.checkPin("correct"));
        assertFalse(connection.checkPin(null));
        assertFalse(connection.checkPin("wrong"));
    }

    @Test
    public void testEncryptDecrypt() throws Exception {
        Connection connection = new Connection(preferences, "test_login", "123123", "correct");

        String testString = "testteststring";
        String encryptedString = connection.encrypt(testString);

        assertNotEquals(testString, encryptedString);

        assertEquals(testString, connection.decrypt(encryptedString));

        connection.changePin("new_pin");

        assertNotEquals(encryptedString, connection.encrypt(testString));
        assertEquals(testString, connection.decrypt(connection.encrypt(testString)));
    }

    @Test(expected = WrongKeyException.class)
    public void testDecryptException() throws Exception {
        Connection connection = new Connection(preferences, "test_login", "123123", "correct");
        String encryptedString = connection.encrypt("testteststring");

        connection.changePin("new_pin");
        connection.decrypt(encryptedString);
    }

    @Test
    public void testWriteToCache() throws Exception {
        Connection connection = new Connection(preferences, "test_login", "123123", "correct");

        connection.setOtp("new_password");

        assertEquals("test_login", preferences.getString(Connection.LOGIN, null));
        assertEquals(connection.encrypt("new_password"),
                preferences.getString(Connection.PASSWORD, null));
        assertEquals(0, preferences.getLong(Connection.PIN_FAILS, -1));

        connection.changePin("new_pin");
        assertEquals(connection.encrypt("new_password"),
                preferences.getString(Connection.PASSWORD, null));
        assertEquals(0, preferences.getLong(Connection.PIN_FAILS, -1));

        connection.increasePinFails();
        assertEquals(1, preferences.getLong(Connection.PIN_FAILS, -1));

        connection.exit();
        assertNull(preferences.getString(Connection.PASSWORD, null));
    }
}