package com.riskpointer.sdk.api.web.error;

import android.annotation.SuppressLint;

import com.riskpointer.sdk.api.exception.WrongHashException;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.protocol.ResponseParser;
import com.riskpointer.sdk.api.web.protocol.json.JsonRequestBuilder;
import com.riskpointer.sdk.api.web.response.Response;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Shurygin Denis on 2016-11-11.
 */

@SuppressLint("Assert")
@RunWith(RobolectricTestRunner.class)
public class RpErrorTest {

    @Test
    public void testEquals() {

        assertEquals(new RpError(0), new RpError(0));
        assertEquals(new RpError(1), new RpError(1));
        assertEquals(new RpError(1222), new RpError(1222));
        assertEquals(new RpError(1, 1, null, null, null), new RpError(1, 1, null, null, null));

        assertNotEquals(new RpError(1), new RpError(2));
        assertNotEquals(new RpError(1), new RpError(1001));

        assertEquals(new RpError(1, 1, buildSomeMessage(), null, null),
                new RpError(1, 1, buildSomeMessage(), null, null));

        assertEquals(new RpError(1, 1, buildSomeMessage(), new String[] {"1", "2"}, null),
                new RpError(1, 1, buildSomeMessage(), new String[] {"1", "2"}, null));

        assertEquals(new RpError(1, 1, buildSomeMessage(), new String[] {"1", "2"}, "message"),
                new RpError(1, 1, buildSomeMessage(), new String[] {"1", "2"}, "message"));

        assertNotEquals(new RpError(1, 1, buildSomeMessage(), new String[] {"1", "2"}, "message"),
                new RpError(2, 1, buildSomeMessage(), new String[] {"1", "2"}, "message"));

        assertNotEquals(new RpError(1, 1, buildSomeMessage(), new String[] {"1", "2"}, "message"),
                new RpError(1, 2, buildSomeMessage(), new String[] {"1", "2"}, "message"));

        assertNotEquals(new RpError(1, 1, buildSomeMessage(), new String[] {"1", "2"}, "message"),
                new RpError(1, 1, null, new String[] {"1", "2"}, "message"));

        assertNotEquals(new RpError(1, 1, buildSomeMessage(), new String[] {"1", "2"}, "message"),
                new RpError(1, 1, buildSomeMessage(), new String[] {"3", "2"}, "message"));

        assertNotEquals(new RpError(1, 1, buildSomeMessage(), new String[] {"1", "2"}, "message"),
                new RpError(1, 1, buildSomeMessage(), new String[] {"1", "2"}, "message2"));

        assertNotEquals(new RpError(1, 1, buildSomeMessage(), new String[] {"1", "2"}, "message"),
                new RpError(1, 1, buildSomeMessage(), new String[] {"2", "1"}, "message"));
    }

    @Test
    public void testCodeFromGroupAndSubItem() throws Exception {
        assertEquals(23, RpError.codeFromGroupAndSubItem(0, 23));
        assertEquals(33 + RpError.RP_ERR_GROUP_MULT, RpError.codeFromGroupAndSubItem(1, 33));
        assertEquals(54 + RpError.RP_ERR_GROUP_MULT * 5, RpError.codeFromGroupAndSubItem(5, 54));
    }

    @Test
    public void testGroupFromCode() throws Exception {
        assertEquals(0, RpError.groupFromCode(23));
        assertEquals(1, RpError.groupFromCode(23 + RpError.RP_ERR_GROUP_MULT));
        assertEquals(5, RpError.groupFromCode(62 + RpError.RP_ERR_GROUP_MULT * 5));
    }

    @Test
    public void testSubItemFromCode() throws Exception {
        assertEquals(23, RpError.subItemFromCode(23));
        assertEquals(26, RpError.subItemFromCode(26 + RpError.RP_ERR_GROUP_MULT));
        assertEquals(62, RpError.subItemFromCode(62 + RpError.RP_ERR_GROUP_MULT * 7));
    }

    private Message<?> buildSomeMessage() {
        JsonRequestBuilder requestBuilder = new JsonRequestBuilder();
        ResponseParser voidResponseParser = new ResponseParser<Void>() {
            @Override
            public Response<Void> parse(Message<?> message,
                                        String responseString,
                                        String password) throws WrongHashException {
                return null;
            }

            @Override
            public boolean equals(Object o) {
                return true;
            }
        };

        return new Message<Void>(RuntimeEnvironment.application, "COMMAND", "URL", null,
                null, new Identity(), new Identity(), requestBuilder, voidResponseParser, 0);
    }
}
