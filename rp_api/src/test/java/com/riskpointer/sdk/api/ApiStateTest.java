package com.riskpointer.sdk.api;

import android.content.Context;

import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.ApiCache;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.response.Response;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;


/**
 * Created by Shurygin Denis on 2016-11-18.
 */
@RunWith(RobolectricTestRunner.class)
public class ApiStateTest extends BaseApiStateTest {

    @Test
    public void testConstructorDefault() {
        super.testConstructorDefault();

        ApiState apiState = buildApiState(RuntimeEnvironment.application,
                "api_url", new Identity("app", "passwor_1239"));

        assertNull(apiState.getActiveUser());
    }

    @Test
    public void testConstructorFromCache() {
        User user = buildUser();
        ApiCache.putApi(RuntimeEnvironment.application, user,
                Message.Command.USER_GET, true);

        super.testConstructorFromCache();

        ApiState apiState = buildApiState(RuntimeEnvironment.application,
                "api_url", new Identity("app", "passwor_1239"));

        assertEquals(user, apiState.getActiveUser());
    }

    @Test
    public void testBroadcastUser() {
        ApiState apiState = buildApiState(RuntimeEnvironment.application,
                "url", new Identity("app", "passwor_12349"));

        Response<User> response = new Response<>(buildUser());
        Response.Editor.setStatus(response, Response.Status.ACCEPT);

        BroadcastHelper.sendActionOnComplete(RuntimeEnvironment.application,
                Message.Command.USER_GET, response);

        assertEquals(response.getResult(), apiState.getActiveUser());
        assertEquals(response.getResult(), ApiCache.getApi(RuntimeEnvironment.application,
                Message.Command.USER_GET, true));
    }

    @Test
    public void testLogout() {
        User user = buildUser();
        ApiCache.putApi(RuntimeEnvironment.application, user,
                Message.Command.USER_GET, true);

        ApiState apiState = buildApiState(RuntimeEnvironment.application,
                "url", new Identity("app", "passwor_12349"));
        assertNotNull(apiState.getActiveUser());

        apiState.logout();
        assertNull(apiState.getActiveUser());

    }

    @Override
    protected ApiState buildApiState(Context context, String apiUrl, Identity appIdentity) {
        return new ApiState(context, apiUrl, appIdentity);
    }

    private User buildUser() {
        User user = new User();
        user.setUserId("j2h34gj2h34g");
        user.setFirstName("Tom");
        return user;
    }
}
