package com.riskpointer.sdk.api.web.webtask;

import android.content.Context;

import com.riskpointer.sdk.api.exception.BadStatusCodeException;
import com.riskpointer.sdk.api.exception.WrongHashException;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.web.ApiCache;
import com.riskpointer.sdk.api.web.Connection;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.protocol.ResponseParser;
import com.riskpointer.sdk.api.web.protocol.json.JsonRequestBuilder;
import com.riskpointer.sdk.api.web.protocol.json.SimpleResponseJsonParser;
import com.riskpointer.sdk.api.web.response.Response;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.io.IOException;

import static com.riskpointer.sdk.api.web.response.Response.Editor.setCommand;
import static com.riskpointer.sdk.api.web.response.Response.Editor.setDescription;
import static com.riskpointer.sdk.api.web.response.Response.Editor.setId;
import static com.riskpointer.sdk.api.web.response.Response.Editor.setOffline;
import static com.riskpointer.sdk.api.web.response.Response.Editor.setOtp;
import static com.riskpointer.sdk.api.web.response.Response.Editor.setShopId;
import static com.riskpointer.sdk.api.web.response.Response.Editor.setShopReference;
import static com.riskpointer.sdk.api.web.response.Response.Editor.setStatus;
import static com.riskpointer.sdk.api.web.response.Response.Editor.setUrl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Shurygin Denis on 2016-11-16.
 */
@Config(manifest=Config.NONE)
@RunWith(RobolectricTestRunner.class)
public class BaseWebTaskTest {

    private static class MockWebTask extends BaseWebTask<Void> {

        private String mResponse;

        MockWebTask(Context context, Message<Void> message) {
            this(context, message, null);
        }

        MockWebTask(Context context, Message<Void> message, String response) {
            super(context, message);
            mResponse = response;
        }

        @Override
        public Object clone() { return this; }

        @Override
        String sendWebRequest(Message<Void> message) throws IOException, BadStatusCodeException {
            return mResponse;
        }
    }

    private static class TestConnection extends Connection {

        private boolean isAcquireOnce = false;
        private boolean isReleaseOnce = false;

        public TestConnection() {
            super(RuntimeEnvironment.application.getSharedPreferences("prefs", Context.MODE_PRIVATE),
                    "log", "pass", "1234");
        }

        @Override
        public void acquire() throws InterruptedException {
            if (isAcquireOnce)
                throw new RuntimeException("Acquire should be called only once");
            super.acquire();
            isAcquireOnce = true;
        }

        @Override
        public void release() {
            if (isReleaseOnce)
                throw new RuntimeException("Release should be called only once");
            if (!isAcquireOnce)
                throw new RuntimeException("Release should be called after acquire");
            super.release();
            isReleaseOnce = true;
        }
    }

    @Test
    public void testConstructor() {
        Message<Void> message = buildTestMessage();
        BaseWebTask<Void> webTask = new MockWebTask(RuntimeEnvironment.application, message);

        assertTrue(message == webTask.getMessage());
        assertTrue(RuntimeEnvironment.application == webTask.getContext());

        assertFalse(webTask.isAborted());
        assertFalse(webTask.isCompleted());
        assertNull(webTask.getResponse());
        assertEquals(RpError.NONE, webTask.getError());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorWithNullContext() {
        new MockWebTask(null, buildTestMessage());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorWithNullMessage() {
        new MockWebTask(RuntimeEnvironment.application, null);
    }

    @Test
    public void testAbortion() {
        BaseWebTask<Void> webTask = new MockWebTask(RuntimeEnvironment.application, buildTestMessage());

        assertTrue(webTask.abort());
        assertTrue(webTask.isAborted());
        assertEquals(RpError.ABORTED, webTask.getError());

        webTask.setError(RpError.Api.NO_CONNECTION);
        assertEquals(RpError.ABORTED, webTask.getError());
    }

    @Test
    public void testSetError() {
        Message<Void> message = buildTestMessage();
        BaseWebTask<Void> webTask = new MockWebTask(RuntimeEnvironment.application, message);
        webTask.setError(RpError.Api.NO_CONNECTION);

        assertEquals(new RpError(RpError.Api.NO_CONNECTION, message), webTask.getError());
    }

    @Test
    public void testProcessOnlineBadResponse() {
        Message<Void> message = buildTestMessageWithConnection();

        BaseWebTask<Void> webTask = new MockWebTask(RuntimeEnvironment.application, message) {
            @Override
            String sendWebRequest(Message<Void> message) throws IOException, BadStatusCodeException {
                throw new BadStatusCodeException();
            }
        };

        assertNull(webTask.processOnline(message));
        assertEquals(RpError.Api.BAD_RESPONSE, webTask.getError().code);
    }

    @Test
    public void testProcessOnlineNoConnection() {
        Message<Void> message = buildTestMessageWithConnection();

        BaseWebTask<Void> webTask = new MockWebTask(RuntimeEnvironment.application, message) {
            @Override
            String sendWebRequest(Message<Void> message) throws IOException, BadStatusCodeException {
                throw new IOException();
            }
        };

        assertNull(webTask.processOnline(message));
        assertEquals(RpError.Api.NO_CONNECTION, webTask.getError().code);
    }

    @Test
    public void testProcessOnlineInvalidParser() {
        Message<Void> message = buildTestMessageWithInvalidParser();

        BaseWebTask<Void> webTask = new MockWebTask(RuntimeEnvironment.application, message, "{}");

        assertNull(webTask.processOnline(message));
        assertEquals(RpError.Api.BAD_RESPONSE, webTask.getError().code);
    }

    @Test
    public void testProcessOnlineAborted() {
        Message<Void> message = buildTestMessage();

        BaseWebTask<Void> webTask = new MockWebTask(RuntimeEnvironment.application, message, "{}");
        webTask.abort();

        assertNull(webTask.processOnline(message));
        assertEquals(RpError.ABORTED, webTask.getError());
    }

    @Test
    public void testProcessOnlineWrongHash() {
        Message<Void> message = buildTestMessage();

        String respStr = "{\"hash\":\"wrong\"}";

        BaseWebTask<Void> webTask = new MockWebTask(RuntimeEnvironment.application, message, respStr);

        assertNull(webTask.processOnline(message));
        assertEquals(RpError.Api.WRONG_HASH, webTask.getError().code);
    }

    @Test
    public void testProcessOnlineCorrectRequest() {
        Message<Void> message = buildTestMessageWithConnection();
        message.setHashIgnore(true);
        message.setCacheFileName("fileName", false);

        String respStr =
                "{\n" +
                "    \"status\": \"accept\",\n" +
                "    \"shop\": \"5804e89d762b0a34b353cc20\",\n" +
                "    \"otp\": \"sptLvJi6YX3K1YAzsMcNR9BszCVbSaVZRzapxGkw\",\n" +
                "    \"random\": \"v[)#Z;pt<59kM2RsAW|-V\u007Fhd~I_[a`V'$[pmHV,>\",\n" +
                "    \"login_identity\": \"testuser@test.test\"\n" +
                "}";

        BaseWebTask<Void> webTask = new MockWebTask(RuntimeEnvironment.application, message, respStr);

        Response<Void> response = webTask.processOnline(message);
        assertNotNull(response);
        assertFalse(response.isOffline());
        assertEquals("sptLvJi6YX3K1YAzsMcNR9BszCVbSaVZRzapxGkw", response.getOtp());
        assertEquals("sptLvJi6YX3K1YAzsMcNR9BszCVbSaVZRzapxGkw", message.getConnection().getOtp());

        // Response with status "accept" should be cached
        assertEquals(response, ApiCache.getWebResponse(RuntimeEnvironment.application, "fileName", true));

        // Flag NO_CACHE
        message.setCacheFileName("fileName2", false);
        message.setFlags(Message.FLAG_NO_CACHE);
        new MockWebTask(RuntimeEnvironment.application, message, respStr).processOnline(message);
        assertNull(ApiCache.getWebResponse(RuntimeEnvironment.application, "fileName2", true));
    }

    @Test
    public void testProcessOfflineCacheNotSupported() {
        Message<Void> message = buildTestMessage();

        BaseWebTask<Void> webTask = new MockWebTask(RuntimeEnvironment.application, message);

        assertNull(webTask.processOffline(message));
        assertEquals(RpError.Api.CACHE_NOT_SUPPORTED, webTask.getError().code);

        message.setCacheFileName("file", false);
        message.setSupportOffline(false);

        webTask = new MockWebTask(RuntimeEnvironment.application, message);

        assertNull(webTask.processOffline(message));
        assertEquals(RpError.Api.CACHE_NOT_SUPPORTED, webTask.getError().code);
    }

    @Test
    public void testProcessOfflineCacheNotFound() {
        Message<Void> message = buildTestMessage();
        message.setCacheFileName("file", false);

        BaseWebTask<Void> webTask = new MockWebTask(RuntimeEnvironment.application, message);

        assertNull(webTask.processOffline(message));
        assertEquals(RpError.Api.CACHE_NOT_FOUND, webTask.getError().code);
    }


    @Test
    public void testProcessOfflineValidCache() {
        // Prepare response object
        Response<Void> response = new Response<>(null);
        setStatus(response, Response.Status.ACCEPT);
        setCommand(response, "command1");
        setId(response, "response_id");
        setDescription(response, "desc");
        setOtp(response, "pas_124");
        setUrl(response, "url");
        setShopId(response, "some_id");
        setShopReference(response, "ref");

        ApiCache.putWebResponse(RuntimeEnvironment.application, response, "file", false);

        Message<Void> message = buildTestMessage();
        message.setCacheFileName("file", false);

        BaseWebTask<Void> webTask = new MockWebTask(RuntimeEnvironment.application, message);

        Response<Void> webResponse = webTask.processOffline(message);
        assertTrue(webResponse.isOffline());
        assertTrue(message == webResponse.getMessage());

        setOffline(response, true);
        assertEquals(response, webResponse);
    }

    @Test
    public void testExecutionOnline() {
        final Response<Void> response = new Response<>(null);

        MockWebTask webTask = new MockWebTask(RuntimeEnvironment.application, buildTestMessage()) {
            @Override
            Response<Void> processOffline(Message<Void> message) {
                throw new RuntimeException("Offline execution should be called");
            }

            @Override
            Response<Void> processOnline(Message<Void> message) {
                return response;
            }
        };

        assertTrue(response == webTask.execute());
        assertTrue(webTask.isCompleted());
        assertEquals(RpError.NONE, webTask.getError());
    }

    @Test
    public void testExecutionOffline() {
        final Response<Void> response = new Response<>(null);
        Message<Void> message = buildTestMessage();
        message.setFlags(Message.FLAG_OFFLINE_MODE);

        MockWebTask webTask = new MockWebTask(RuntimeEnvironment.application, message) {
            @Override
            Response<Void> processOffline(Message<Void> message) {
                return response;
            }

            @Override
            Response<Void> processOnline(Message<Void> message) {
                throw new RuntimeException("Online execution should be called");
            }
        };

        assertTrue(response == webTask.execute());
        assertTrue(webTask.isCompleted());
        assertEquals(RpError.NONE, webTask.getError());
    }


    @Test
    public void testExecutionAborted() {
        MockWebTask webTask = new MockWebTask(RuntimeEnvironment.application, buildTestMessage()) {
            @Override
            Response<Void> processOffline(Message<Void> message) {
                throw new RuntimeException("Offline execution should be called");
            }

            @Override
            Response<Void> processOnline(Message<Void> message) {
                throw new RuntimeException("Online execution should be called");
            }
        };
        webTask.abort();

        assertNull(webTask.execute());
        assertTrue(webTask.isCompleted());
        assertEquals(RpError.ABORTED, webTask.getError());
    }

    @Test(expected = IllegalStateException.class)
    public void testExecutionTwice() {
        MockWebTask webTask = new MockWebTask(RuntimeEnvironment.application, buildTestMessage()) {
            @Override
            Response<Void> processOffline(Message<Void> message) {
                return null;
            }

            @Override
            Response<Void> processOnline(Message<Void> message) {
                return null;
            }
        };

        webTask.execute();
        webTask.execute();
    }

    @Test
    public void testExecutionAcquireConnection() {
        TestConnection connection = new TestConnection();
        Message<Void> message = new Message<>(RuntimeEnvironment.application, "test", "url",
                connection, null,
                new Identity("id", "password"), new Identity("id", "password"),
                new JsonRequestBuilder(), new SimpleResponseJsonParser(), 0);

        MockWebTask webTask = new MockWebTask(RuntimeEnvironment.application, message) {
            @Override
            Response<Void> processOffline(Message<Void> message) { return null; }

            @Override
            Response<Void> processOnline(Message<Void> message) { return null; }
        };

        webTask.execute();

        assertTrue(connection.isAcquireOnce);
        assertTrue(connection.isReleaseOnce);
    }

    private Message<Void> buildTestMessage() {
        return new Message<>(RuntimeEnvironment.application, "test", "url", null, null,
                new Identity("id", "password"), new Identity("id", "password"),
                new JsonRequestBuilder(), new SimpleResponseJsonParser(), 0);
    }

    private Message<Void> buildTestMessageWithConnection() {
        return new Message<>(RuntimeEnvironment.application, "test", "url",
                new Connection(RuntimeEnvironment.application
                        .getSharedPreferences("prefs", Context.MODE_PRIVATE),
                        "log", "pass", "1234"), null,
                new Identity("id", "pass"), new Identity("id", "pass"),
                new JsonRequestBuilder(), new SimpleResponseJsonParser(), 0);
    }

    private Message<Void> buildTestMessageWithInvalidParser() {
        return new Message<>(RuntimeEnvironment.application, "test", "url",
                new Connection(RuntimeEnvironment.application
                        .getSharedPreferences("prefs", Context.MODE_PRIVATE),
                        "log", "pass", "1234"), null,
                new Identity("id", "pass"), new Identity("id", "pass"),
                new JsonRequestBuilder(), new ResponseParser<Void>() {
            @Override
            public Response<Void> parse(Message<?> message, String responseString, String password) throws WrongHashException {
                return null;
            }
        }, 0);
    }

}
