package com.riskpointer.sdk.api.model.entity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Shurygin Denis on 2016-11-18.
 */
@RunWith(RobolectricTestRunner.class)
public class AppInfoTest {

    @Test
    public void testEquals() {
        assertNotEquals(new AppInfo(), new AppInfo(){});

        AppInfo appInfo1 = new AppInfo();
        AppInfo appInfo2 = new AppInfo();
        assertEquals(appInfo1, appInfo2);
        assertEquals(appInfo2, appInfo1);

        appInfo1.setShopIdentity(new Identity("shop", "Password123"));
        assertNotEquals(appInfo1, appInfo2);
        assertNotEquals(appInfo2, appInfo1);
        appInfo2.setShopIdentity(new Identity("shop", "Password123"));
        assertEquals(appInfo1, appInfo2);
        assertEquals(appInfo2, appInfo1);

        appInfo1.setOauthIdentity(new Identity("oauthKey", "secret"));
        assertNotEquals(appInfo1, appInfo2);
        assertNotEquals(appInfo2, appInfo1);
        appInfo2.setOauthIdentity(new Identity("oauthKey", "secret"));
        assertEquals(appInfo1, appInfo2);
        assertEquals(appInfo2, appInfo1);
    }
}
