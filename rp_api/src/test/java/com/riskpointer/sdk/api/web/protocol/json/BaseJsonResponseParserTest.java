package com.riskpointer.sdk.api.web.protocol.json;

import android.annotation.SuppressLint;

import com.riskpointer.sdk.api.exception.WrongHashException;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.response.Response;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Shurygin Denis on 2016-11-11.
 */

@SuppressLint("Assert")
@RunWith(RobolectricTestRunner.class)
public class BaseJsonResponseParserTest {

    private static final String CORRECT_JSON_RESPONSE =
            "{\n" +
            "    \"identity_plugindata\": \"dAec6cf891c3fc6ecd5d21247e7013011d\",\n" +
            "    \"html_accuracy\": \"23\",\n" +
            "    \"command\": \"consumerget\",\n" +
            "    \"hash\": \"94d69dfea446b742a4dadcf5848248426796" +
                            "c5d865693fa1074116bbfd0d0e504d0e636a" +
                            "ae6d85d0d860a9e0da2ed0489445bae241da1d341d2a8e1553e87734\",\n" +
            "    \"shop\": \"5804e89d762b0a34b353cc20\",\n" +
            "    \"identity_client\": \"com\\/riskpointer\\/test;android;0.00.002\",\n" +
            "    \"html_longitude\": \"30.3894721\",\n" +
            "    \"login_password\": \"sptLvJi6YX3K1YAzsMcNR9BszCVbSaVZRzapxGkw\",\n" +
            "    \"version\": \"1\",\n" +
            "    \"language\": \"en\",\n" +
            "    \"random\": \"v[)#Z;pt<59kM2RsAW|-V\u007Fhd~I_[a`V'$[pmHV,>\",\n" +
            "    \"login_identity\": \"testuser@test.test\",\n" +
            "    \"html_latitude\": \"59.9153254\"\n" +
            "}";
    private static final String CORRECT_PASSWORD = "password";

    private BaseJsonResponseParser<Void> responseParser;
    private Message<Void> message;

    @Before
    public void init() {
        responseParser = new BaseJsonResponseParser<Void>() {

            @Override
            protected Void parseJson(Message message, JSONObject json) {
                return null;
            }
        };
        message = new Message<>(
                RuntimeEnvironment.application,
                "command", "url://url.url", null, null,
                new Identity("id", "password"),
                new Identity("id", "password"),
                new JsonRequestBuilder(),
                responseParser, 0);
    }

    @Test
    public void testParseResponse() throws Exception {
        JSONObject json = new JSONObject();
        json.put("status", Response.Status.PENDING);
        json.put("id", "some_id_1");
        json.put("description", "desc1121");
        json.put("url", "url://url.url");
        json.put("otp", "password_123456");
        json.put(Message.Param.SHOP_REFERENCE, "ref_1");

        Response<Void> response1 = responseParser.parseResponse(message, json);

        assertEquals(Response.Status.PENDING, response1.getStatus());
        assertEquals("some_id_1", response1.getId());
        assertEquals("desc1121", response1.getDescription());
        assertEquals("url://url.url", response1.getUrl());
        assertEquals("password_123456", response1.getOtp());
        assertEquals("ref_1", response1.getShopReference());

        assertEquals(response1, responseParser.parseResponse(message, json));

        json.put("status", Response.Status.ACCEPT);
        assertNotEquals(response1, responseParser.parseResponse(message, json));

        json.put("pending", new JSONArray(Arrays.asList("pending_1", "pend_2")));
        response1 = responseParser.parseResponse(message, json);
        assertEquals(2, response1.getPendings().size());
        assert response1.hasPending("pending_1");
        assert response1.hasPending("pend_2");
    }

    @Test
    public void testParseResponseError() throws Exception {
        JSONObject json = new JSONObject();
        json.put("status", Response.Status.PENDING);
        json.put("id", "some_id_1");
        json.put("description", "E1002 identity_client: Invalid value for parameter");
        json.put("error", new JSONArray(Arrays.asList(3, 23)));

        Response<Void> response = responseParser.parseResponse(message, json);

        RpError error = response.getError();
        assertNotNull(error);
        assertEquals(3, error.group);
        assertEquals(RpError.codeFromGroupAndSubItem(3, 23), error.code);
        assertEquals(message, error.message);
        assert Arrays.equals(new String[] {"identity_client"}, error.fields);
        assertEquals("E1002 identity_client: Invalid value for parameter", error.description);
    }

    @Test
    public void testParseResponseIsSameThatParse() throws Exception {
        JSONObject json = new JSONObject();
        json.put("status", Response.Status.PENDING);
        json.put("id", "some_id_1");
        json.put("description", "desc1121");
        json.put("url", "url://url.url");
        json.put("otp", "password_123456");
        json.put(Message.Param.SHOP_REFERENCE, "ref_1");
        json.put("pending", new JSONArray(Arrays.asList("pending_1", "pend_2")));

        assertEquals(responseParser.parse(message, json.toString(), null),
                responseParser.parseResponse(message, json));
    }

    @Test
    public void testParseWithoutHash() throws Exception {
        JSONObject json = new JSONObject();
        json.put("status", Response.Status.PENDING);
        json.put("id", "some_id_1");
        String jsonString = json.toString();

        responseParser.parse(message, jsonString, null);
        responseParser.parse(message, jsonString, "password");
        // Exception shouldn't be thrown
    }

    @Test(expected = WrongHashException.class)
    public void testParseWithWrongHash() throws Exception {
        JSONObject json = new JSONObject();
        json.put("status", Response.Status.PENDING);
        json.put("id", "some_id_1");
        json.put("hash", "wrong");

        responseParser.parse(message, json.toString(), "password");
    }

    @Test
    public void testParseWithCorrectHash() throws Exception {
        responseParser.parse(message, CORRECT_JSON_RESPONSE, CORRECT_PASSWORD);
        // Exception shouldn't be thrown
    }

    @Test
    public void testJsonToMap() throws Exception {
        Map<String, Object> map = new HashMap<>();
        JSONObject json = new JSONObject();
        assertEquals(map, BaseJsonResponseParser.jsonToMap(json));

        map.put("key1", "value");
        json.put("key1", "value");

        assertEquals(map, BaseJsonResponseParser.jsonToMap(json));

        map.put("key2", 2.2d);
        json.put("key2", 2.2d);

        assertEquals(map, BaseJsonResponseParser.jsonToMap(json));

        map.put("key3", 5f);
        json.put("key3", 5f);

        // NOTE!!!
        // Json cast Float values to Double type, so in this case values not equal
        assertNotEquals(map, BaseJsonResponseParser.jsonToMap(json));

        // remove invalid values
        map.remove("key3");
        json.remove("key3");

        Map<String, Object> subMap = new HashMap<>();
        subMap.put("subKey", "subValue");
        map.put("subMap", subMap);

        JSONObject subJson = new JSONObject();
        subJson.put("subKey", "subValue");
        json.put("subMap", subJson);

        assertEquals(map, BaseJsonResponseParser.jsonToMap(json));

        subMap.put("subSubMap", new HashMap<String, Object>());
        subJson.put("subSubMap", new JSONObject());

        assertEquals(map, BaseJsonResponseParser.jsonToMap(json));

        subMap.put("array", Arrays.asList("item1", "item2"));
        subJson.put("array", Arrays.asList("item1", "item2"));

        assertEquals(map, BaseJsonResponseParser.jsonToMap(json));

        // test json to object
        assertEquals(map, BaseJsonResponseParser.jsonToObject(json));
    }

    @Test
    public void testJsonArrayToList() throws Exception {
        List<Object> list = new ArrayList<>();
        JSONArray jsonArray = new JSONArray();
        assertEquals(list, BaseJsonResponseParser.jsonArrayToList(jsonArray));

        list.add("string");
        jsonArray.put("string");

        assertEquals(list, BaseJsonResponseParser.jsonArrayToList(jsonArray));

        list.add(2.5d);
        jsonArray.put(2.5d);

        assertEquals(list, BaseJsonResponseParser.jsonArrayToList(jsonArray));

        Map<String, Object> map = new HashMap<>();
        list.add(map);
        JSONObject json = new JSONObject();
        jsonArray.put(json);

        assertEquals(list, BaseJsonResponseParser.jsonArrayToList(jsonArray));

        map.put("key", "value");
        json.put("key", "value");

        assertEquals(list, BaseJsonResponseParser.jsonArrayToList(jsonArray));

        // test json to object
        assertEquals(list, BaseJsonResponseParser.jsonToObject(jsonArray));
    }


    @Test
    public void testCheckHash() throws Exception {
        JSONObject json = new JSONObject(CORRECT_JSON_RESPONSE);

        assert responseParser.checkHash(json, CORRECT_PASSWORD, true);

        String wrongPasswor = CORRECT_PASSWORD + "wrong";

        json.put("hash", "");
        assert responseParser.checkHash(json, wrongPasswor, true);

        json.put("hash", null);
        assert responseParser.checkHash(json, wrongPasswor, true);

        json.put("hash", "wrong");
        assert responseParser.checkHash(json, "", true);

        json.put("hash", "wrong");
        assert responseParser.checkHash(json, null, true);

        assert !responseParser.checkHash(json, wrongPasswor, false);
        assert !responseParser.checkHash(json, CORRECT_PASSWORD, false);
    }

    @Test(expected = WrongHashException.class)
    public void testCheckHashWrongPasswordException() throws Exception {
        responseParser.checkHash(
                new JSONObject(CORRECT_JSON_RESPONSE),
                CORRECT_PASSWORD + "wrong", true);
    }

    @Test(expected = WrongHashException.class)
    public void testCheckHashWrongHashException() throws Exception {
        JSONObject json = new JSONObject(CORRECT_JSON_RESPONSE);
        json.put("hash", "wrong");
        responseParser.checkHash(json, CORRECT_PASSWORD, true);
    }
}
