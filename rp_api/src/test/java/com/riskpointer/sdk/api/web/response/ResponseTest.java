package com.riskpointer.sdk.api.web.response;

import android.annotation.SuppressLint;

import com.riskpointer.sdk.api.TestUtils;
import com.riskpointer.sdk.api.exception.WrongHashException;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.protocol.ResponseParser;
import com.riskpointer.sdk.api.web.protocol.json.JsonRequestBuilder;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.io.IOException;
import java.util.Arrays;

import static com.riskpointer.sdk.api.web.response.Response.Editor.setCommand;
import static com.riskpointer.sdk.api.web.response.Response.Editor.setDescription;
import static com.riskpointer.sdk.api.web.response.Response.Editor.setError;
import static com.riskpointer.sdk.api.web.response.Response.Editor.setId;
import static com.riskpointer.sdk.api.web.response.Response.Editor.setOffline;
import static com.riskpointer.sdk.api.web.response.Response.Editor.setOtp;
import static com.riskpointer.sdk.api.web.response.Response.Editor.setPendings;
import static com.riskpointer.sdk.api.web.response.Response.Editor.setShopId;
import static com.riskpointer.sdk.api.web.response.Response.Editor.setShopReference;
import static com.riskpointer.sdk.api.web.response.Response.Editor.setStatus;
import static com.riskpointer.sdk.api.web.response.Response.Editor.setUrl;
import static com.riskpointer.sdk.api.web.response.Response.Status;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by Shurygin Denis on 2016-11-11.
 */

@SuppressLint("Assert")
@RunWith(RobolectricTestRunner.class)
public class ResponseTest {

    @Test
    public void testEditor() throws Exception {
        Response<Void> response = new Response<>(null);

        Response.Editor.setStatus(response, Status.ACCEPT);
        assertEquals(Status.ACCEPT, response.getStatus());
        Response.Editor.setStatus(response, Status.REJECT);
        assertEquals(Status.REJECT, response.getStatus());

        Response.Editor.setCommand(response, "test1");
        assertEquals("test1", response.getCommand());
        Response.Editor.setCommand(response, "test2");
        assertEquals("test2", response.getCommand());

        Response.Editor.setId(response, "response_id1");
        assertEquals("response_id1", response.getId());
        Response.Editor.setId(response, "response_id2");
        assertEquals("response_id2", response.getId());

        Response.Editor.setDescription(response, "desc1");
        assertEquals("desc1", response.getDescription());
        Response.Editor.setDescription(response, "desc2");
        assertEquals("desc2", response.getDescription());

        Response.Editor.setOtp(response, "pas_124");
        assertEquals("pas_124", response.getOtp());
        Response.Editor.setOtp(response, "pas_1245");
        assertEquals("pas_1245", response.getOtp());

        Response.Editor.setOffline(response, true);
        assertEquals(true, response.isOffline());
        Response.Editor.setOffline(response, false);
        assertEquals(false, response.isOffline());

        Response.Editor.setUrl(response, "url1");
        assertEquals("url1", response.getUrl());
        Response.Editor.setUrl(response, "url2");
        assertEquals("url2", response.getUrl());

        Response.Editor.setShopId(response, "some_id1");
        assertEquals("some_id1", response.getShopId());
        Response.Editor.setShopId(response, "some_id2");
        assertEquals("some_id2", response.getShopId());

        Response.Editor.setShopReference(response, "ref1");
        assertEquals("ref1", response.getShopReference());
        Response.Editor.setShopReference(response, "ref2");
        assertEquals("ref2", response.getShopReference());

        Response.Editor.setError(response, new RpError(RpError.Api.ABORTED));
        assertEquals(new RpError(RpError.Api.ABORTED), response.getError());
        Response.Editor.setError(response, new RpError(RpError.Api.BAD_RESPONSE));
        assertEquals(new RpError(RpError.Api.BAD_RESPONSE), response.getError());

        Response.Editor.setPendings(response, Arrays.asList("pending1"));
        assertEquals(Arrays.asList("pending1"), response.getPendings());
        Response.Editor.setPendings(response, Arrays.asList("pending2", "3"));
        assertEquals(Arrays.asList("pending2", "3"), response.getPendings());
    }

    @Test
    public void testEquals() throws Exception {
        Response<Void> response1 = new Response<>(null);
        Response<Void> response2 = new Response<>(null);
        assertEquals(response1, response2);
        assertEquals(response2, response1);

        // Not equal if one of the property is null
        setStatus(response1, Status.ACCEPT);
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);
        setStatus(response2, Status.ACCEPT);
        assertEquals(response1, response2);
        assertEquals(response2, response1);

        setCommand(response1, "command1");
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);
        setCommand(response2, "command1");
        assertEquals(response1, response2);
        assertEquals(response2, response1);

        setId(response1, "response_id");
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);
        setId(response2, "response_id");
        assertEquals(response1, response2);
        assertEquals(response2, response1);

        setDescription(response1, "desc");
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);
        setDescription(response2, "desc");
        assertEquals(response1, response2);
        assertEquals(response2, response1);

        setOtp(response1, "pas_124");
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);
        setOtp(response2, "pas_124");
        assertEquals(response1, response2);
        assertEquals(response2, response1);

        setOffline(response1, true);
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);
        setOffline(response2, true);
        assertEquals(response1, response2);
        assertEquals(response2, response1);

        setUrl(response1, "url");
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);
        setUrl(response2, "url");
        assertEquals(response1, response2);
        assertEquals(response2, response1);

        setShopId(response1, "some_id");
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);
        setShopId(response2, "some_id");
        assertEquals(response1, response2);
        assertEquals(response2, response1);

        setShopReference(response1, "ref");
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);
        setShopReference(response2, "ref");
        assertEquals(response1, response2);
        assertEquals(response2, response1);

        setError(response1, new RpError(RpError.Api.BAD_RESPONSE));
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);
        setError(response2, new RpError(RpError.Api.BAD_RESPONSE));
        assertEquals(response1, response2);
        assertEquals(response2, response1);

        // Not equal if property not equal
        setStatus(response2, Status.REJECT);
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);

        response2 = new Response<>(response1, null);
        setCommand(response2, "wrong");
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);

        response2 = new Response<>(response1, null);
        setId(response2, "wrong");
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);

        response2 = new Response<>(response1, null);
        setDescription(response2, "wrong");
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);

        response2 = new Response<>(response1, null);
        setOtp(response2, "wrong");
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);

        response2 = new Response<>(response1, null);
        setOffline(response2, false);
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);

        response2 = new Response<>(response1, null);
        setUrl(response2, "wrong");
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);

        response2 = new Response<>(response1, null);
        setShopId(response2, "wrong");
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);

        response2 = new Response<>(response1, null);
        setShopReference(response2, "wrong");
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);

        response2 = new Response<>(response1, null);
        setError(response2, new RpError(RpError.Api.ABORTED));
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);

        //Equal pendings
        response2 = new Response<>(response1, null);
        setPendings(response1, Arrays.asList("pending1", "pending2"));
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);
        setPendings(response2, Arrays.asList("pending1"));
        assertNotEquals(response1, response2);
        assertNotEquals(response2, response1);
        setPendings(response2, Arrays.asList("pending1", "pending2"));
        assertEquals(response1, response2);
        assertEquals(response2, response1);
    }

    @Test
    public void testClone() throws Exception {
        Response<Void> response1 = new Response<>(null);
        Response<Void> response2 = new Response<>(response1, null);
        assertEquals(response1, response2);

        setStatus(response1, Status.ACCEPT);
        response2 = new Response<>(response1, null);
        assertEquals(response1, response2);

        setCommand(response1, "command1");
        response2 = new Response<>(response1, null);
        assertEquals(response1, response2);

        setId(response1, "response_id");
        response2 = new Response<>(response1, null);
        assertEquals(response1, response2);

        setDescription(response1, "desc");
        response2 = new Response<>(response1, null);
        assertEquals(response1, response2);

        setOtp(response1, "pas_124");
        response2 = new Response<>(response1, null);
        assertEquals(response1, response2);

        setOffline(response1, true);
        response2 = new Response<>(response1, null);
        assertEquals(response1, response2);

        setUrl(response1, "url");
        response2 = new Response<>(response1, null);
        assertEquals(response1, response2);

        setShopId(response1, "some_id");
        response2 = new Response<>(response1, null);
        assertEquals(response1, response2);

        setShopReference(response1, "ref");
        response2 = new Response<>(response1, null);
        assertEquals(response1, response2);

        setError(response1, new RpError(RpError.Api.BAD_RESPONSE));
        response2 = new Response<>(response1, null);
        assertEquals(response1, response2);

        setPendings(response1, Arrays.asList("pending1", "pending2"));
        response2 = new Response<>(response1, null);
        assertEquals(response1, response2);
    }

    @Test
    public void testIsStatus() throws Exception {
        Response<Void> response = new Response<>(null);

        setStatus(response, null);
        assert !response.isStatusAccept();
        assert !response.isStatusPending();
        assert !response.isStatusReject();
        assert !response.isStatusError();

        setStatus(response, "unknown_status");
        assert !response.isStatusAccept();
        assert !response.isStatusPending();
        assert !response.isStatusReject();
        assert !response.isStatusError();

        setStatus(response, Status.ACCEPT);
        assert response.isStatusAccept();
        assert !response.isStatusPending();
        assert !response.isStatusReject();
        assert !response.isStatusError();

        setStatus(response, Status.PENDING);
        assert !response.isStatusAccept();
        assert response.isStatusPending();
        assert !response.isStatusReject();
        assert !response.isStatusError();

        setStatus(response, Status.REJECT);
        assert !response.isStatusAccept();
        assert !response.isStatusPending();
        assert response.isStatusReject();
        assert !response.isStatusError();

        setStatus(response, Status.ERROR);
        assert !response.isStatusAccept();
        assert !response.isStatusPending();
        assert !response.isStatusReject();
        assert response.isStatusError();
    }

    @Test
    public void testHasPending() throws Exception {
        Response<Void> response = new Response<>(null);

        assert !response.hasPending("pending");
        assert !response.hasPending(null);

        setPendings(response, Arrays.asList("pending"));
        assert response.hasPending("pending");
        assert !response.hasPending("another pending");
        assert !response.hasPending(null);

        setPendings(response, Arrays.asList("another pending", "pending", "another"));
        assert response.hasPending("pending");
        assert response.hasPending("another pending");
        assert !response.hasPending(null);
        assert !response.hasPending("invalid pending");
    }

    @Test
    public void testSerialization() throws IOException, ClassNotFoundException {
        Message<String> testMessage = new Message<>(RuntimeEnvironment.application, "test", "url", null,
                null, new Identity("id", "pass"), new Identity("id", "pass"),
                new JsonRequestBuilder(), new ResponseParser<String>() {
            @Override
            public Response<String> parse(Message<?> message, String responseString,
                                          String password) throws WrongHashException {
                return new Response<>("Test String");
            }
        }, 0);

        Response<String> response = new Response<>("Test String");
        response.setMessage(testMessage);

        setStatus(response, Status.ACCEPT);
        setCommand(response, "command1");
        setId(response, "response_id");
        setDescription(response, "desc");
        setOtp(response, "pas_124");
        setOffline(response, true);
        setUrl(response, "url");
        setShopId(response, "some_id");
        setShopReference(response, "ref");
        setError(response, new RpError(RpError.Api.BAD_RESPONSE));
        setPendings(response, Arrays.asList("pending1", "pending2"));

        Object result = TestUtils.deserialize(TestUtils.serialize(response));

        assertEquals(response, result);
        assertNull(((Response<?>) result).getMessage());
    }
}
