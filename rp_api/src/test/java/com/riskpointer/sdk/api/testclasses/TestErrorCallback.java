package com.riskpointer.sdk.api.testclasses;

import com.riskpointer.sdk.api.web.callback.ErrorCallback;
import com.riskpointer.sdk.api.web.error.RpError;

/**
 * Created by Shurygin Denis on 2016-11-21.
 */

public class TestErrorCallback implements ErrorCallback {

    private int mCount;
    private int[] mErrors;
    private final int mMaxCount;


    public TestErrorCallback(int count) {
        this(count, null);
    }

    public TestErrorCallback(int count, int expectedError) {
        this(count, new int[] {expectedError});
    }

    public TestErrorCallback(int count, int[] expectedErrors) {
        mCount = count;
        mErrors = expectedErrors;
        mMaxCount = count;
    }

    @Override
    public boolean onError(RpError error) {
        boolean isExpectedError = mErrors == null;
        if (!isExpectedError) {
            for (int errorCode : mErrors) {
                if (error.code == errorCode) {
                    isExpectedError = true;
                    break;
                }
            }
            if (!isExpectedError)
                throw new RuntimeException("Unexpected error code " + error.toString());
        }

        mCount--;
        if (mCount < 0)
            throw new RuntimeException("onError should be called only " + mMaxCount + "times.");
        return true;
    }

    public int getCountdownValue() {
        return mCount;
    }
}
