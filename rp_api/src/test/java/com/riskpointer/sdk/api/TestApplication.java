package com.riskpointer.sdk.api;

import android.Manifest;
import android.app.Application;
import android.content.pm.PackageManager;

/**
 * Created by Shurygin Denis on 2016-11-17.
 */

public class TestApplication extends Application {

    @Override
    public int checkCallingOrSelfPermission(String permission) {
        switch (permission) {
            case Manifest.permission.INTERNET:
            case Manifest.permission.ACCESS_FINE_LOCATION:
                return PackageManager.PERMISSION_GRANTED;
        }
        return super.checkCallingOrSelfPermission(permission);
    }
}

