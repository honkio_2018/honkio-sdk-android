package com.riskpointer.sdk.api.model.entity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Shurygin Denis on 2016-11-18.
 */
@RunWith(RobolectricTestRunner.class)
public class MerchantSimpleTest {

    @Test
    public void testEquals() {
        assertNotEquals(new MerchantSimple(), new MerchantSimple(){});

        MerchantSimple merchant1 = new MerchantSimple();
        MerchantSimple merchant2 = new MerchantSimple();
        assertEquals(merchant1, merchant2);
        assertEquals(merchant2, merchant1);

        merchant1.setId("1212");
        assertNotEquals(merchant1, merchant2);
        assertNotEquals(merchant2, merchant1);
        merchant2.setId("1212");
        assertEquals(merchant1, merchant2);
        assertEquals(merchant2, merchant1);

        merchant1.setBusinessId("s8879");
        assertNotEquals(merchant1, merchant2);
        assertNotEquals(merchant2, merchant1);
        merchant2.setBusinessId("s8879");
        assertEquals(merchant1, merchant2);
        assertEquals(merchant2, merchant1);

        merchant1.setName("m name");
        assertNotEquals(merchant1, merchant2);
        assertNotEquals(merchant2, merchant1);
        merchant2.setName("m name");
        assertEquals(merchant1, merchant2);
        assertEquals(merchant2, merchant1);

        merchant1.setSupportedAccounts(new String[] {"type1" ,"type3"});
        assertNotEquals(merchant1, merchant2);
        assertNotEquals(merchant2, merchant1);
        merchant2.setSupportedAccounts(new String[] {"type1" ,"type3"});
        assertEquals(merchant1, merchant2);
        assertEquals(merchant2, merchant1);

        merchant1.setLoginShopIdentity(new Identity("id1", "pas123"));
        assertNotEquals(merchant1, merchant2);
        assertNotEquals(merchant2, merchant1);
        merchant2.setLoginShopIdentity(new Identity("id1", "pas123"));
        assertEquals(merchant1, merchant2);
        assertEquals(merchant2, merchant1);
    }

    @Test
    public void testIsAccountSupported() {
        String accountType = "type1";

        MerchantSimple merchant = new MerchantSimple();
        assertFalse(merchant.isAccountSupported(accountType));

        merchant.setSupportedAccounts(new String[] {accountType ,"type3"});
        assertTrue(merchant.isAccountSupported(accountType));

        merchant.setSupportedAccounts(new String[] {accountType + "_" ,"type3"});
        assertFalse(merchant.isAccountSupported(accountType));
        assertTrue(merchant.isAccountSupported("type3"));
    }
}
