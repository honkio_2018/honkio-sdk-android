package com.riskpointer.sdk.api.web.callback;

import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Shurygin Denis on 2016-11-16.
 */
@RunWith(RobolectricTestRunner.class)
public class RequestCallbackWrapperTest {

    private static class TestCallback<T> implements RequestCallback<T> {

        int mCompleteCalls = 0;
        int mErrorCalls = 0;

        @Override
        public boolean onComplete(Response<T> response) {
            mCompleteCalls++;
            return true;
        }

        @Override
        public boolean onError(RpError error) {
            mErrorCalls++;
            return true;
        }
    }

    @Test
    public void testNullCallback() {
        RequestCallback<Void> wrapper = new RequestCallbackWrapper<>(null);

        assertFalse(wrapper.onComplete(new Response<Void>(null)));
        assertFalse(wrapper.onError(new RpError(RpError.Api.NO_CONNECTION)));
    }

    @Test
    public void testBaseCallbacks() {
        TestCallback<Void> callback = new TestCallback<>();

        assertEquals(0, callback.mCompleteCalls);
        assertEquals(0, callback.mErrorCalls);

        RequestCallback<Void> wrapper = new RequestCallbackWrapper<>(callback);

        assertTrue(wrapper.onComplete(new Response<Void>(null)));
        assertEquals(1, callback.mCompleteCalls);
        assertEquals(0, callback.mErrorCalls);

        assertTrue(wrapper.onError(new RpError(RpError.Api.NO_CONNECTION)));
        assertEquals(1, callback.mCompleteCalls);
        assertEquals(1, callback.mErrorCalls);

        assertTrue(wrapper.onComplete(new Response<Void>(null)));
        assertTrue(wrapper.onComplete(new Response<Void>(null)));
        assertEquals(3, callback.mCompleteCalls);
        assertEquals(1, callback.mErrorCalls);

        assertTrue(wrapper.onError(new RpError(RpError.Api.NO_CONNECTION)));
        assertTrue(wrapper.onError(new RpError(RpError.Api.NO_CONNECTION)));
        assertEquals(3, callback.mCompleteCalls);
        assertEquals(3, callback.mErrorCalls);
    }

    @Test
    public void testSetGet() {
        TestCallback<Void> callback1 = new TestCallback<>();
        TestCallback<Void> callback2 = new TestCallback<>();

        RequestCallbackWrapper<Void> wrapper = new RequestCallbackWrapper<>(callback1);
        assertTrue(callback1 == wrapper.get());

        wrapper.set(callback2);
        assertTrue(callback2 == wrapper.get());

        wrapper.onComplete(null);

        assertEquals(0, callback1.mCompleteCalls);
        assertEquals(1, callback2.mCompleteCalls);
    }

    @Test
    public void testSetGoDepth() {
        RequestCallback<Void> callback1 = new TestCallback<>();
        RequestCallback<Void> callback2 = new TestCallback<>();

        RequestCallbackWrapper<Void> wrapper1 = new RequestCallbackWrapper<>(callback1);
        RequestCallbackWrapper<Void> wrapper2 = new RequestCallbackWrapper<>(wrapper1);

        wrapper2.set(callback2, true);

        assertTrue(wrapper1 == wrapper2.get());
        assertTrue(callback2 == wrapper1.get());

        wrapper2.set(callback1); // go depth by default
        assertTrue(wrapper1 == wrapper2.get());
        assertTrue(callback1 == wrapper1.get());

        wrapper2.set(callback2, false);
        assertTrue(callback2 == wrapper2.get());
        assertTrue(callback1 == wrapper1.get());

        //Level 3
        wrapper2.set(wrapper1, false);
        RequestCallbackWrapper<Void> wrapper3 = new RequestCallbackWrapper<>(wrapper2);

        wrapper3.set(callback2, true);
        assertTrue(wrapper2 == wrapper3.get());
        assertTrue(wrapper1 == wrapper2.get());
        assertTrue(callback2 == wrapper1.get());
    }
}
