package com.riskpointer.sdk.api;

import android.content.Context;

import com.riskpointer.sdk.api.model.entity.AppInfo;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.MerchantSimple;
import com.riskpointer.sdk.api.model.entity.ServerInfo;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.ShopInfo;
import com.riskpointer.sdk.api.model.entity.merchant.Merchant;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.ApiCache;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.response.Response;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Shurygin Denis on 2016-11-17.
 */
@RunWith(RobolectricTestRunner.class)
public class BaseApiStateTest {

    @Test
    public void testConstructorDefault() {
        String url = "api_url";
        Identity appIdentity = new Identity("app", "passwor_1239");
        BaseApiState apiState = buildApiState(RuntimeEnvironment.application, url, appIdentity);

        assertEquals(url, apiState.getUrl());
        assertEquals(appIdentity, apiState.getAppIdentity());
        assertTrue(RuntimeEnvironment.application == apiState.getContext());

        assertNull(apiState.getServerInfo());
        assertNull(apiState.getAppInfo());
        assertNull(apiState.getMainShopInfo());
        assertNull(apiState.getMainShopIdentity());
    }

    @Test
    public void testConstructorFromCache() {
        ServerInfo serverInfo = buildServerInfo();
        ApiCache.putApi(RuntimeEnvironment.application, serverInfo,
                Message.Command.SERVER_INFO, false);

        AppInfo appInfo = buildAppInfo();
        ApiCache.putApi(RuntimeEnvironment.application, appInfo,
                Message.Command.APP_INFO, false);

        ShopInfo shopInfo = buildShopInfo();
        ApiCache.putApi(RuntimeEnvironment.application, shopInfo,
                Message.Command.SHOP_INFO, false);

        String url = "api_url";
        Identity appIdentity = new Identity("app", "passwor_1239");
        BaseApiState apiState = buildApiState(RuntimeEnvironment.application, url, appIdentity);

        assertEquals(url, apiState.getUrl());
        assertEquals(appIdentity, apiState.getAppIdentity());

        assertEquals(serverInfo, apiState.getServerInfo());
        assertEquals(appInfo, apiState.getAppInfo());
        assertEquals(shopInfo, apiState.getMainShopInfo());
        assertEquals(appInfo.getShopIdentity(), apiState.getMainShopIdentity());
    }

    @Test
    public void testBroadcastServerInfo() {
        BaseApiState apiState = buildApiState(RuntimeEnvironment.application,
                "url", new Identity("app", "passwor_12349"));

        Response<ServerInfo> response = new Response<>(buildServerInfo());
        Response.Editor.setStatus(response, Response.Status.ACCEPT);

        BroadcastHelper.sendActionOnComplete(RuntimeEnvironment.application,
                Message.Command.SERVER_INFO, response);

        assertEquals(response.getResult(), apiState.getServerInfo());
        assertEquals(response.getResult(), ApiCache.getApi(RuntimeEnvironment.application,
                Message.Command.SERVER_INFO, false));
    }

    @Test
    public void testBroadcastAppInfo() {
        BaseApiState apiState = buildApiState(RuntimeEnvironment.application,
                "url", new Identity("app", "passwor_12349"));

        Response<AppInfo> response = new Response<>(buildAppInfo());
        Response.Editor.setStatus(response, Response.Status.ACCEPT);

        BroadcastHelper.sendActionOnComplete(RuntimeEnvironment.application,
                Message.Command.APP_INFO, response);

        assertEquals(response.getResult(), apiState.getAppInfo());
        assertEquals(response.getResult(), ApiCache.getApi(RuntimeEnvironment.application,
                Message.Command.APP_INFO, false));
    }

    @Test
    public void testBroadcastShopInfoNoAppInfo() {
        BaseApiState apiState = buildApiState(RuntimeEnvironment.application,
                "url", new Identity("app", "passwor_12349"));

        Response<ShopInfo> response = new Response<>(buildShopInfo());
        Response.Editor.setStatus(response, Response.Status.ACCEPT);

        BroadcastHelper.sendActionOnComplete(RuntimeEnvironment.application,
                Message.Command.SHOP_INFO, response);

        assertNull(apiState.getMainShopInfo());
        assertNull(ApiCache.getApi(RuntimeEnvironment.application,
                Message.Command.SHOP_INFO, false));
    }

    @Test
    public void testBroadcastShopInfoWithCachedAppInfo() {
        AppInfo appInfo = buildAppInfo();
        ApiCache.putApi(RuntimeEnvironment.application, appInfo,
                Message.Command.APP_INFO, false);

        BaseApiState apiState = buildApiState(RuntimeEnvironment.application,
                "url", new Identity("app", "passwor_12349"));

        ShopInfo shopInfo = buildShopInfo();
        // Set Wrong shop Id
        shopInfo.getShop().setId(appInfo.getShopIdentity().getId() + "_");
        Response<ShopInfo> response = new Response<>(shopInfo);
        Response.Editor.setStatus(response, Response.Status.ACCEPT);

        BroadcastHelper.sendActionOnComplete(RuntimeEnvironment.application,
                Message.Command.SHOP_INFO, response);

        assertNull(apiState.getMainShopInfo());
        assertNull(ApiCache.getApi(RuntimeEnvironment.application,
                Message.Command.SHOP_INFO, false));

        // Set Correct shop Id
        shopInfo.getShop().setId(appInfo.getShopIdentity().getId());

        BroadcastHelper.sendActionOnComplete(RuntimeEnvironment.application,
                Message.Command.SHOP_INFO, response);

        assertEquals(shopInfo, apiState.getMainShopInfo());
        assertEquals(shopInfo, ApiCache.getApi(RuntimeEnvironment.application,
                Message.Command.SHOP_INFO, false));
    }

    protected BaseApiState buildApiState(Context context, String apiUrl, Identity appIdentity) {
        return new BaseApiState(context, apiUrl, appIdentity);
    }

    private ServerInfo buildServerInfo() {
        ServerInfo serverInfo = new ServerInfo();
        serverInfo.setVersion(2);
        serverInfo.setOAuthTokenUrl("url");
        return serverInfo;
    }

    private AppInfo buildAppInfo() {
        AppInfo appInfo = new AppInfo();
        appInfo.setShopIdentity(new Identity("shop", "pass"));
        appInfo.setOauthIdentity(new Identity("key", "password"));
        return appInfo;
    }

    private ShopInfo buildShopInfo() {
        MerchantSimple merchant = new Merchant();
        merchant.setId("123123");
        merchant.setBusinessId("1232139309808435");
        merchant.setName("Merchant");

        Shop shop = new Shop();
        shop.setId("shopId");
        shop.setPassword("pass123");
        shop.setName("Some shop");

        ShopInfo shopInfo = new ShopInfo();
        shopInfo.setTimeStamp(2000012);
        shopInfo.setMerchant(merchant);
        shopInfo.setShop(shop);
        return shopInfo;
    }
}
