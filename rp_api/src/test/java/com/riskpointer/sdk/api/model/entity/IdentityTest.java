package com.riskpointer.sdk.api.model.entity;

import android.annotation.SuppressLint;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Shurygin Denis on 2016-11-14.
 */
@SuppressLint("Assert")
@RunWith(RobolectricTestRunner.class)
public class IdentityTest {

    @Test
    public void testEquals() throws Exception {
        assertEquals(new Identity(), new Identity());
        assertEquals(new Identity(null, null), new Identity(null, null));
        assertEquals(new Identity("id1", null), new Identity("id1", null));
        assertEquals(new Identity("id1", "pass"), new Identity("id1", "pass"));
        assertEquals(new Identity(null, "pass"), new Identity(null, "pass"));

        assertNotEquals(new Identity(), new Identity(){});
        assertNotEquals(new Identity("id1", "pass"), new Identity("id2", "pass"));
        assertNotEquals(new Identity("id1", "pass2"), new Identity("id", "pass2"));
        assertNotEquals(new Identity("id1", "pass"), new Identity(null, "pass"));
        assertNotEquals(new Identity("id1", "pass2"), new Identity("id", null));
        assertNotEquals(new Identity(null, "pass"), new Identity("id2", "pass"));
        assertNotEquals(new Identity("id1", null), new Identity("id", "pass2"));
    }

    @Test
    public void testIsCorrupt() throws Exception {
        Identity identity = new Identity("id", "pass");
        assert !identity.isCorrupt();

        identity.setId("");
        assert identity.isCorrupt();

        identity.setId(null);
        assert identity.isCorrupt();

        identity.setId("id");
        assert !identity.isCorrupt();

        identity.setPassword("");
        assert !identity.isCorrupt();

        identity.setPassword(null);
        assert identity.isCorrupt();

    }
}
