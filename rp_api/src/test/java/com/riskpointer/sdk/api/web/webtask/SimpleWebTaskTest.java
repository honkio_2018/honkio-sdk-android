package com.riskpointer.sdk.api.web.webtask;

import com.riskpointer.sdk.api.exception.BadStatusCodeException;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.utils.ApiWebUtils;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.protocol.json.JsonRequestBuilder;
import com.riskpointer.sdk.api.web.protocol.json.SimpleResponseJsonParser;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.io.IOException;
import java.net.HttpURLConnection;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;
import static org.robolectric.RuntimeEnvironment.application;

/**
 * Created by Shurygin Denis on 2016-11-17.
 */
@RunWith(RobolectricTestRunner.class)
public class SimpleWebTaskTest {

    private static class MockCall implements Call {

        private Request mRequest;
        private Response mResponse;

        MockCall(Request request, Response response) {
            mRequest = request;
            mResponse = response;
        }

        @Override
        public Request request() { return mRequest; }

        @Override
        public Response execute() throws IOException { return mResponse; }

        @Override
        public void enqueue(Callback responseCallback) { }

        @Override
        public void cancel() { }

        @Override
        public boolean isExecuted() { return false; }

        @Override
        public boolean isCanceled() { return false; }
    }

    @Test
    public void testSendWebRequestValid() throws IOException, BadStatusCodeException {
        final String jsonString = "{\"test\":\"value\"}";
        Call.Factory client = new Call.Factory() {
            @Override
            public Call newCall(Request request) {
                return new MockCall(request, new Response.Builder()
                        .body(ResponseBody.create(ApiWebUtils.MEDIA_TYPE_JSON, jsonString))
                        .request(request)
                        .protocol(Protocol.HTTP_2)
                        .code(HttpURLConnection.HTTP_OK).build());
            }
        };
        Message<Void> message = buildTestMessage();

        SimpleWebTask<Void> webTask = new SimpleWebTask<>(application, message, client);

        assertEquals(jsonString, webTask.sendWebRequest(message));
    }


    @Test
    public void testSendWebRequestValidAborted() throws IOException, BadStatusCodeException {
        final String jsonString = "{\"test\":\"value\"}";
        Call.Factory client = new Call.Factory() {
            @Override
            public Call newCall(Request request) {
                return new MockCall(request, new Response.Builder()
                        .body(ResponseBody.create(ApiWebUtils.MEDIA_TYPE_JSON, jsonString))
                        .request(request)
                        .protocol(Protocol.HTTP_2)
                        .code(HttpURLConnection.HTTP_OK).build());
            }
        };
        Message<Void> message = buildTestMessage();

        SimpleWebTask<Void> webTask = new SimpleWebTask<>(application, message, client);
        webTask.abort();

        assertNull(webTask.sendWebRequest(message));
    }

    @Test(expected = BadStatusCodeException.class)
    public void testSendWebRequestBadResponse() throws IOException, BadStatusCodeException {
        Call.Factory client = new Call.Factory() {
            @Override
            public Call newCall(Request request) {
                return new MockCall(request, new Response.Builder()
                        .body(ResponseBody.create(ApiWebUtils.MEDIA_TYPE_JSON, "{}"))
                        .request(request)
                        .protocol(Protocol.HTTP_2)
                        .code(HttpURLConnection.HTTP_NOT_FOUND).build());
            }
        };
        Message<Void> message = buildTestMessage();

        new SimpleWebTask<>(application, message, client).sendWebRequest(message);
    }

    @Test
    public void testSendWebRequestBadResponseAborted() throws IOException, BadStatusCodeException {
        Call.Factory client = new Call.Factory() {
            @Override
            public Call newCall(Request request) {
                return new MockCall(request, new Response.Builder()
                        .body(ResponseBody.create(ApiWebUtils.MEDIA_TYPE_JSON, "{}"))
                        .request(request)
                        .protocol(Protocol.HTTP_2)
                        .code(HttpURLConnection.HTTP_NOT_FOUND).build());
            }
        };
        Message<Void> message = buildTestMessage();

        SimpleWebTask<Void> webTask = new SimpleWebTask<>(application, message, client);
        webTask.abort();

        assertNull(webTask.sendWebRequest(message));
    }


    private Message<Void> buildTestMessage() {
        return new Message<Void>(application, "test", "http://url.url", null, null,
                new Identity("id", "password"), new Identity("id", "password"),
                new JsonRequestBuilder(), new SimpleResponseJsonParser(), 0) {
            @Override
            public String buildRequest() {
                return "{}";
            }
        };
    }
}
