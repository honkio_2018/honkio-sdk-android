package com.riskpointer.sdk.api.model.entity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Shurygin Denis on 2016-11-18.
 */
@RunWith(RobolectricTestRunner.class)
public class HKDeviceTest {

    @Test
    public void testEquals() {
        assertNotEquals(new HKDevice(), new HKDevice(){});

        HKDevice device1 = new HKDevice();
        HKDevice device2 = new HKDevice();
        assertEquals(device1, device2);
        assertEquals(device2, device1);

        device1.setId("k123hk1j2h3");
        assertNotEquals(device1, device2);
        assertNotEquals(device2, device1);
        device2.setId("k123hk1j2h3");
        assertEquals(device1, device2);
        assertEquals(device2, device1);

        device1.setApp("app_id");
        assertNotEquals(device1, device2);
        assertNotEquals(device2, device1);
        device2.setApp("app_id");
        assertEquals(device1, device2);
        assertEquals(device2, device1);

        device1.setToken("23y4u2y3t4uy2");
        assertNotEquals(device1, device2);
        assertNotEquals(device2, device1);
        device2.setToken("23y4u2y3t4uy2");
        assertEquals(device1, device2);
        assertEquals(device2, device1);

        device1.setPlatform("another_platform");
        assertNotEquals(device1, device2);
        assertNotEquals(device2, device1);
        device2.setPlatform("another_platform");
        assertEquals(device1, device2);
        assertEquals(device2, device1);

        device1.setName("Another name");
        assertNotEquals(device1, device2);
        assertNotEquals(device2, device1);
        device2.setName("Another name");
        assertEquals(device1, device2);
        assertEquals(device2, device1);

        device1.setNotifyType("another_notify_type");
        assertNotEquals(device1, device2);
        assertNotEquals(device2, device1);
        device2.setNotifyType("another_notify_type");
        assertEquals(device1, device2);
        assertEquals(device2, device1);
    }

    @Test
    public void testToParams() {
        //TODO test
    }
}
