package com.riskpointer.sdk.api.web.protocol.json;

import android.annotation.SuppressLint;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.riskpointer.sdk.api.web.protocol.json.JsonRequestBuilder.wrap;
import static com.riskpointer.sdk.api.web.protocol.json.JsonRequestBuilder.wrapCollection;
import static com.riskpointer.sdk.api.web.protocol.json.JsonRequestBuilder.wrapMap;
import static org.junit.Assert.assertEquals;

/**
 * Created by Shurygin Denis on 2016-11-11.
 */
@SuppressLint("Assert")
@RunWith(RobolectricTestRunner.class)
public class JsonRequestBuilderTest {

    private JsonRequestBuilder requestBuilder;

    @Before
    public void init() {
        requestBuilder = new JsonRequestBuilder();
    }

    @Test
    public void testBuildEmpty() throws Exception {
        String resultEmpty = requestBuilder.build(new HashMap<String, Object>());
        String resultNull = requestBuilder.build(null);

        String emptyJson = new JSONObject().toString();
        assertEquals(emptyJson, resultEmpty);
        assertEquals(emptyJson, resultNull);
    }

    @Test
    public void testWrap() throws Exception {
        assertEquals(JSONObject.NULL, wrap(null));

        // Following objects shouldn't be wrapped
        List<Object> objects = Arrays.asList(
                new JSONArray(),
                new JSONObject(),
                (byte) 10,
                'a',
                10002.5d,
                21.4f,
                2,
                123L,
                (short) 2,
                "string"
        );
        for (Object object : objects) {
            assert wrap(object) == object;
        }

        Map<String, Object> map = new HashMap<>();
        map.put("key", "value");
        assert wrap(map) instanceof JSONObject;
        assertEquals(wrap(map).toString(), wrapMap(map).toString());

        Collection<String> collection = Arrays.asList("string", "sdsd");
        assert wrap(collection) instanceof JSONArray;
        assertEquals(wrap(collection).toString(), wrapCollection(collection).toString());
    }

    @Test
    public void testWrapMapWithStrings() throws Exception {
        Map<String, Object> map = new HashMap<>();
        JSONObject json = new JSONObject();

        assertEquals(wrapMap(map).toString(), json.toString());

        map.put("key", "value");
        json.put("key", "value");

        assertEquals(wrapMap(map).toString(), json.toString());

        map.put("key2", "value2");
        json.put("key2", "value2");

        assertEquals(wrapMap(map).toString(), json.toString());
    }

    @Test
    public void testWrapMapWithStringArrays() throws Exception {
        Map<String, Object> map = new HashMap<>();
        JSONObject json = new JSONObject();

        map.put("key", "value");
        json.put("key", "value");

        Collection<String> array1 = Arrays.asList("item1", "item2");
        map.put("array1", array1);
        json.put("array1", new JSONArray(array1));

        assertEquals(wrapMap(map).toString(), json.toString());
    }

    @Test
    public void testWrapMapWithMapArrays() throws Exception {
        Map<String, Object> map = new HashMap<>();
        JSONObject json = new JSONObject();

        Collection<Map<String, Object>> array1 = new ArrayList<>(2);
        Map<String, Object> subMap1 = new HashMap<>();
        subMap1.put("key", "value1");
        subMap1.put("key2", Arrays.asList("item1", "item2"));

        Map<String, Object> subMap2 = new HashMap<>();
        subMap2.put("key", "value1");
        subMap2.put("key2", 2);

        array1.add(subMap1);
        array1.add(subMap2);

        JSONObject subJson1 = new JSONObject();
        subJson1.put("key", "value1");
        subJson1.put("key2", new JSONArray(Arrays.asList("item1", "item2")));

        JSONObject subJson2 = new JSONObject();
        subJson2.put("key", "value1");
        subJson2.put("key2", 2);

        map.put("key", "value");
        map.put("array1", array1);

        json.put("key", "value");
        json.put("array1", new JSONArray(Arrays.asList(subJson1, subJson2)));

        assertEquals(wrapMap(map).toString(), json.toString());
    }

    @Test
    public void testWrapMapWithSubMap() throws Exception {
        Map<String, Object> map = new HashMap<>();
        JSONObject json = new JSONObject();
        for (int i = 0; i < 10; i++) {
            map.put("key1", "value1");
            map.put("key2", "value2");
            map.put("key3", 2);
            map.put("key4", 2.5f);
            map.put("key5", 3f);

            json.put("key1", "value1");
            json.put("key2", "value2");
            json.put("key3", 2);
            json.put("key4", 2.5f);
            json.put("key5", 3f);

            Map<String, Object> subMap = map;
            map = new HashMap<>();
            map.put("submap", subMap);

            JSONObject subJson = json;
            json = new JSONObject();
            json.put("submap", subJson);
        }

        assertEquals(wrapMap(map).toString(), json.toString());
    }

    @Test
    public void testWrapCollection() throws Exception {
        List<Object> collection = new ArrayList<>();
        JSONArray jsonArray = new JSONArray();

        Map<String, Object> map = new HashMap<>();
        JSONObject json = new JSONObject();

        map.put("key1", "value1");
        map.put("key5", 3f);

        json.put("key1", "value1");
        json.put("key5", 3f);

        collection.add("string");
        jsonArray.put("string");

        collection.add(2f);
        jsonArray.put(2f);

        collection.add(map);
        jsonArray.put(json);

        assertEquals(wrapCollection(collection).toString(), jsonArray.toString());
    }


    @Test
    public void testCalculateHash() throws Exception {
        String password = "test_password";
        Map<String, Object> map = new HashMap<>();
        map.put("key1", "value1");
        map.put("key2", "value2");
        map.put("key3", 2);
        map.put("key4", 2.5f);
        map.put("key5", 3f);

        String hash = requestBuilder.calculateHash(map, password);
        assertEquals("c5a823d05949560ebef1444ad29e00cff88ed2dada043fe652f7fc327afdd158dcbf684128a" +
                "5d8c649aedc337fd483b53a2d44d0dabe16200bd49942a84e6f4f", hash);

        List<String> strings = Arrays.asList("string1", "string2", "StRING3");
        map.put("strings", strings);

        hash = requestBuilder.calculateHash(map, password);
        assertEquals("a21fb0c230dd9e64680da067ee6d6c264a403632395efe1dbe0bea9effabf48e18019ecdbae" +
                "f88fed8050a7d14cecd1fbcf16071fccdbba1b24d8219b067742d", hash);

        Map<String, Object> subMap = new HashMap<>();
        subMap.put("key1", "value1");
        subMap.put("key2", "value2");
        subMap.put("key3", 2);
        subMap.put("key4", 2.5f);
        subMap.put("key5", 3f);
        map.put("submap", subMap);

        hash = requestBuilder.calculateHash(map, password);
        assertEquals("d7f95f71b6e252b3d2a306f5a7ce08a487130cfe155258fa254d0b65861a1260d1969c741bb" +
                "050bb4f4e3ab997fbcbc1f32f592cfe46cc47ef731b8b55c0936a", hash);

        Map<String, Object> subMap2 = new HashMap<>(subMap);
        Map<String, Object> subMap3 = new HashMap<>(subMap);
        subMap3.put("key2", "new value");
        List<Map<String, Object>> mapsList = Arrays.asList(subMap2, subMap3);
        map.put("mapslist", mapsList);

        hash = requestBuilder.calculateHash(map, password);
        assertEquals("bd33d2a7d560f7e226ad51a2d47123be33fb00c0118f137c5fdf99e018fe0b880b3513811f5" +
                "27ecaa84d7235c6a34d2e6cd87f19e66fec8a553f00f865c103b0", hash);
    }
}
