package com.riskpointer.sdk.api.model.entity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Shurygin Denis on 2016-11-18.
 */
@RunWith(RobolectricTestRunner.class)
public class ShopTest {

    @Test
    public void testEquals() {
        assertNotEquals(new Shop(), new Shop(){});

        Shop shop1 = new Shop();
        Shop shop2 = new Shop();
        assertEquals(shop1, shop2);
        assertEquals(shop2, shop1);


        shop1.setName("name");
        assertNotEquals(shop1, shop2);
        assertNotEquals(shop2, shop1);
        shop2.setName("name");
        assertEquals(shop1, shop2);
        assertEquals(shop2, shop1);

        shop1.setLatitude(213.23d);
        assertNotEquals(shop1, shop2);
        assertNotEquals(shop2, shop1);
        shop2.setLatitude(213.23d);
        assertEquals(shop1, shop2);
        assertEquals(shop2, shop1);

        shop1.setLongitude(34234.3d);
        assertNotEquals(shop1, shop2);
        assertNotEquals(shop2, shop1);
        shop2.setLongitude(34234.3d);
        assertEquals(shop1, shop2);
        assertEquals(shop2, shop1);

        shop1.setPublicKey("asdasdasdlkjahsdljhalsdjlasjdhlajsdhlkjasd");
        assertNotEquals(shop1, shop2);
        assertNotEquals(shop2, shop1);
        shop2.setPublicKey("asdasdasdlkjahsdljhalsdjlasjdhlajsdhlkjasd");
        assertEquals(shop1, shop2);
        assertEquals(shop2, shop1);

        shop1.setType("shop_type");
        assertNotEquals(shop1, shop2);
        assertNotEquals(shop2, shop1);
        shop2.setType("shop_type");
        assertEquals(shop1, shop2);
        assertEquals(shop2, shop1);

        shop1.setServiceType("service");
        assertNotEquals(shop1, shop2);
        assertNotEquals(shop2, shop1);
        shop2.setServiceType("service");
        assertEquals(shop1, shop2);
        assertEquals(shop2, shop1);

        shop1.setLogoSmall("http://logo1");
        assertNotEquals(shop1, shop2);
        assertNotEquals(shop2, shop1);
        shop2.setLogoSmall("http://logo1");
        assertEquals(shop1, shop2);
        assertEquals(shop2, shop1);

        shop1.setLogoLarge("http://logo2");
        assertNotEquals(shop1, shop2);
        assertNotEquals(shop2, shop1);
        shop2.setLogoLarge("http://logo2");
        assertEquals(shop1, shop2);
        assertEquals(shop2, shop1);

        shop1.setAddress("address");
        assertNotEquals(shop1, shop2);
        assertNotEquals(shop2, shop1);
        shop2.setAddress("address");
        assertEquals(shop1, shop2);
        assertEquals(shop2, shop1);

        shop1.setMaxDistance(205d);
        assertNotEquals(shop1, shop2);
        assertNotEquals(shop2, shop1);
        shop2.setMaxDistance(205d);
        assertEquals(shop1, shop2);
        assertEquals(shop2, shop1);

        shop1.setRatings(Arrays.asList(new Rate(-1, 1), new Rate(2, 15)));
        assertNotEquals(shop1, shop2);
        assertNotEquals(shop2, shop1);
        shop2.setRatings(Arrays.asList(new Rate(-1, 1), new Rate(2, 15)));
        assertEquals(shop1, shop2);
        assertEquals(shop2, shop1);

        MerchantSimple merchant1 = new MerchantSimple();
        merchant1.setName("name");
        shop1.setMerchant(merchant1);
        assertNotEquals(shop1, shop2);
        assertNotEquals(shop2, shop1);
        MerchantSimple merchant2 = new MerchantSimple();
        merchant2.setName("name");
        shop2.setMerchant(merchant2);
        assertEquals(shop1, shop2);
        assertEquals(shop2, shop1);
    }

    @Test
    public void testCheckDistance() {
        // TODO test
    }
}
