package com.riskpointer.sdk.api.model.entity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Shurygin Denis on 2016-11-18.
 */
@RunWith(RobolectricTestRunner.class)
public class ShopInfoTest {

    @Test
    public void testEquals() {
        assertNotEquals(new ShopInfo(), new ShopInfo(){});

        ShopInfo shopInfo1 = new ShopInfo();
        ShopInfo shopInfo2 = new ShopInfo();
        assertEquals(shopInfo1, shopInfo2);
        assertEquals(shopInfo2, shopInfo1);

        shopInfo1.setTimeStamp(213123L);
        assertNotEquals(shopInfo1, shopInfo2);
        assertNotEquals(shopInfo2, shopInfo1);
        shopInfo2.setTimeStamp(213123L);
        assertEquals(shopInfo1, shopInfo2);
        assertEquals(shopInfo2, shopInfo1);

        Shop shop = new Shop();
        shop.setId("d87sd68a7s6d87as");
        shop.setName("some name");
        shopInfo1.setShop(shop);
        assertNotEquals(shopInfo1, shopInfo2);
        assertNotEquals(shopInfo2, shopInfo1);

        shop = new Shop();
        shop.setId("d87sd68a7s6d87as");
        shop.setName("some name");
        shopInfo2.setShop(shop);
        assertEquals(shopInfo1, shopInfo2);
        assertEquals(shopInfo2, shopInfo1);

        MerchantSimple merchant = new MerchantSimple();
        merchant.setId("32h4j2h4jh23g4jh");
        merchant.setName("merchant");
        shopInfo1.setMerchant(merchant);
        assertNotEquals(shopInfo1, shopInfo2);
        assertNotEquals(shopInfo2, shopInfo1);

        merchant = new MerchantSimple();
        merchant.setId("32h4j2h4jh23g4jh");
        merchant.setName("merchant");
        shopInfo2.setMerchant(merchant);
        assertEquals(shopInfo1, shopInfo2);
        assertEquals(shopInfo2, shopInfo1);
    }
}
