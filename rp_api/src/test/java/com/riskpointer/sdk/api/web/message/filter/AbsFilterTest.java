package com.riskpointer.sdk.api.web.message.filter;

import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.protocol.json.JsonRequestBuilder;
import com.riskpointer.sdk.api.web.protocol.json.SimpleResponseJsonParser;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Shurygin Denis on 2016-11-16.
 */
@RunWith(RobolectricTestRunner.class)
public class AbsFilterTest {

    private static class MockFilter extends AbsFilter {

        private Map<String, Object> mParams;

        MockFilter(Map<String, Object> params) {
            this.mParams = params;
        }

        @Override
        public Map<String, Object> toParams() {
            return mParams;
        }
    }

    @Test
    public void testApplyToMessage() {
        Map<String, Object> map = new HashMap<>();
        map.put("param1", "String");
        map.put("param2", 2.2d);

        AbsFilter filter = new MockFilter(map);
        Message<Void> message = buildTestMessage();

        filter.applyTo(message);

        assertEquals("String", message.getParameter("param1"));
        assertEquals(2.2d, message.getParameter("param2"));
        assertTrue(filter == message.getParameter(null));
    }

    @Test
    public void testApplyToMessageIfNotNull() {
        Message<Void> message = buildTestMessage();

        AbsFilter.applyIfNotNull(message, "param1", "String");
        assertEquals("String", message.getParameter("param1"));

        AbsFilter.applyIfNotNull(message, "param2", 2.2d);
        assertEquals(2.2d, message.getParameter("param2"));

        AbsFilter.applyIfNotNull(message, "param1", null);
        assertEquals("String", message.getParameter("param1"));
    }

    @Test
    public void testApplyToMapIfNotNull() {
        Map<String, Object> map = new HashMap<>();

        AbsFilter.applyIfNotNull(map, "param1", "String");
        assertEquals("String", map.get("param1"));

        AbsFilter.applyIfNotNull(map, "param2", 2.2d);
        assertEquals("String", map.get("param1"));
        assertEquals(2.2d, map.get("param2"));

        AbsFilter.applyIfNotNull(map, "param1", null);
        assertEquals("String", map.get("param1"));
        assertEquals(2.2d, map.get("param2"));
    }

    @Test
    public void testApplyOrRemove() {
        Map<String, Object> map = new HashMap<>();
        map.put("param1", "Initial String");
        map.put("param2", 3.2d);

        AbsFilter.applyOrRemove(map, "param1", "String");
        assertEquals("String", map.get("param1"));

        AbsFilter.applyOrRemove(map, "param2", 2.2d);
        assertEquals("String", map.get("param1"));
        assertEquals(2.2d, map.get("param2"));

        AbsFilter.applyOrRemove(map, "param1", null);
        assertNull(map.get("param1"));
        assertEquals(2.2d, map.get("param2"));

        AbsFilter.applyOrRemove(map, "param2", null);
        assertNull(map.get("param2"));
    }

    @Test
    public void testAmountToString() {
        assertEquals("0.00", AbsFilter.amountToString(0));
        assertEquals("1.00", AbsFilter.amountToString(1));
        assertEquals("1.50", AbsFilter.amountToString(1.5f));
        assertEquals("21244.50", AbsFilter.amountToString(21244.5d));
        assertEquals("1.59", AbsFilter.amountToString(1.59d));

        assertEquals("1.60", AbsFilter.amountToString(1.599d));
        assertEquals("1.59", AbsFilter.amountToString(1.595d));
        assertEquals("1.59", AbsFilter.amountToString(1.594d));
    }

    private Message<Void> buildTestMessage() {
        return new Message<>(RuntimeEnvironment.application, "test", "url", null,
                null, new Identity("id", "pass"), new Identity("id", "pass"),
                new JsonRequestBuilder(), new SimpleResponseJsonParser(), 0);
    }
}
