package com.riskpointer.sdk.api.model.entity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.Arrays;
import java.util.HashMap;

import static com.riskpointer.sdk.api.model.entity.AppInfo.TouInfo.KEY_DEFAULT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Shurygin Denis on 2016-11-18.
 */
@RunWith(RobolectricTestRunner.class)
public class UserTest {

    @Test
    public void testEquals() {
        assertNotEquals(new User(), new User(){});

        User user1 = new User();
        User user2 = new User();
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setLogin("login");
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setLogin("login");
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setPassword("pass123");
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setPassword("pass123");
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setFirstName("FName");
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setFirstName("FName");
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setLastName("LName");
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setLastName("LName");
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setPhone("2319238");
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setPhone("2319238");
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setSsn("000000-0000");
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setSsn("000000-0000");
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setLanguage("en");
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setLanguage("en");
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setTimezone("time");
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setTimezone("time");
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setAddress1("addres 1");
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setAddress1("addres 1");
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setAddress2("Addres 2");
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setAddress2("Addres 2");
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setZip("12121");
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setZip("12121");
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setCity("Big Cyty");
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setCity("Big Cyty");
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setState("State");
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setState("State");
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setCountry("Finland");
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setCountry("Finland");
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setReceipt(true);
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setReceipt(true);
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setActive(true);
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setActive(true);
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setTempPasswordUsed(true);
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setTempPasswordUsed(true);
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setOperatorBillingEnabled(true);
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setOperatorBillingEnabled(true);
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setRegistrationId("23jh1jk23h1k2jh3");
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setRegistrationId("23jh1jk23h1k2jh3");
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setUserId("23234234uy23t4uy2t34uy");
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setUserId("23234234uy23t4uy2t34uy");
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setAccountsList(Arrays.asList(new UserAccount("type", "4")));
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setAccountsList(Arrays.asList(new UserAccount("type", "4")));
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        user1.setDictValue("key", "value");
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        user2.setDictValue("key", "value");
        assertEquals(user1, user2);
        assertEquals(user2, user1);

        HashMap<String, HashMap<String, User.TouVersion>> touMap = new HashMap<>(1);
        HashMap<String, User.TouVersion> touVersionMap = new HashMap<>(1);
        touVersionMap.put(KEY_DEFAULT, new User.TouVersion(23, 1238012938));
        touMap.put("aadasdasd", touVersionMap);
        user1.setTouVersions(touMap);
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user1);
        touVersionMap = new HashMap<>(1);
        touVersionMap.put(KEY_DEFAULT, new User.TouVersion(23, 1238012938));
        user2.setTouVersions(touMap);
        touMap.put("aadasdasd", touVersionMap);
        assertEquals(user1, user2);
        assertEquals(user2, user1);
    }

    @Test
    public void testGetName() {
        User user = new User();
        assertEquals("", user.getName());

        user.setFirstName("FName");
        assertEquals("FName", user.getName());

        user.setLastName("LName");
        assertEquals("FName LName", user.getName());

        user.setFirstName(null);
        assertEquals("LName", user.getName());
    }

    @Test
    public void testTouVersion() {
        User.TouVersion tou = new User.TouVersion(2, 23213123);
        String key = "iwuryeiuweyr";
        AppInfo appInfo = new AppInfo(key, "none");

        User user = new User();

        appInfo.addTouInfo(new AppInfo.TouInfo("no", tou.version));

        assertNull(user.getTouVersion(key));
        assertTrue(user.isTouVersionObsolete(appInfo));

        HashMap<String, HashMap<String, User.TouVersion>> touMap = new HashMap<>(1);
        HashMap<String, User.TouVersion> touVersionMap = new HashMap<>(1);
        touVersionMap.put(KEY_DEFAULT, tou);
        touMap.put(key, touVersionMap);
        user.setTouVersions(touMap);

        assertEquals(tou, user.getTouVersion(key));

        assertFalse(user.isTouVersionObsolete(appInfo));

        appInfo.addTouInfo(new AppInfo.TouInfo("no", tou.version -1));
        assertFalse(user.isTouVersionObsolete(appInfo));

        appInfo.addTouInfo(new AppInfo.TouInfo("no", tou.version));
        assertFalse(user.isTouVersionObsolete(appInfo));

        appInfo.addTouInfo(new AppInfo.TouInfo("no", tou.version +1));
        assertTrue(user.isTouVersionObsolete(appInfo));
    }

    @Test
    public void testTIsHasAddress() {
        User user = new User();
        assertFalse(user.isHasAddress());

        user.setCountry("country");
        user.setCity("city");
        user.setZip("zip");
        user.setAddress1("address");
        assertTrue(user.isHasAddress());

        user.setCountry(null);
        assertFalse(user.isHasAddress());
        user.setCountry("country");
        assertTrue(user.isHasAddress());

        user.setCity(null);
        assertFalse(user.isHasAddress());
        user.setCity("city");
        assertTrue(user.isHasAddress());

        user.setZip(null);
        assertFalse(user.isHasAddress());
        user.setZip("zip");
        assertTrue(user.isHasAddress());

        user.setAddress1(null);
        assertFalse(user.isHasAddress());
        user.setAddress1("address");
        assertTrue(user.isHasAddress());
    }

    @Test
    public void testIsHasAnyAccount() {
        User user = new User();
        assertFalse(user.isHasAnyAccount());

        // Hidden accounts
        UserAccount account1 = new UserAccount();
        UserAccount account2 = new UserAccount();

        user.setAccountsList(Arrays.asList(account1, account2));
        assertFalse(user.isHasAnyAccount());

        // Not hidden
        account2.setType(UserAccount.Type.CREDITCARD);
        account2.setNumber("213123");
        assertTrue(user.isHasAnyAccount());

        // Not hidden
        account1.setType(UserAccount.Type.CREDITCARD);
        account1.setNumber("4645645");
        assertTrue(user.isHasAnyAccount());

        // Hidden
        account2.setType(null);
        assertTrue(user.isHasAnyAccount());

        // Both Hidden
        account1.setType(null);
        assertFalse(user.isHasAnyAccount());
    }

    @Test
    public void testToParams() {
        //TODO test
    }
}
