package com.riskpointer.sdk.api.model.entity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Shurygin Denis on 2016-11-17.
 */
@RunWith(RobolectricTestRunner.class)
public class ServerInfoTest {

    @Test
    public void testEquals() {
        assertNotEquals(new ServerInfo(), new ServerInfo(){});

        ServerInfo serverInfo1 = new ServerInfo();
        ServerInfo serverInfo2 = new ServerInfo();

        assertEquals(serverInfo1, serverInfo2);
        assertEquals(serverInfo2, serverInfo1);

        serverInfo1.setVersion(3);
        assertNotEquals(serverInfo1, serverInfo2);
        assertNotEquals(serverInfo2, serverInfo1);
        serverInfo2.setVersion(3);
        assertEquals(serverInfo1, serverInfo2);
        assertEquals(serverInfo2, serverInfo1);

        serverInfo1.setTimeZones(Arrays.asList("zon1", "zone2"));
        assertNotEquals(serverInfo1, serverInfo2);
        assertNotEquals(serverInfo2, serverInfo1);
        serverInfo2.setTimeZones(Arrays.asList("zon1", "zone2"));
        assertEquals(serverInfo1, serverInfo2);
        assertEquals(serverInfo2, serverInfo1);

        serverInfo1.setCountries(Arrays.asList(new ServerInfo.Country()));
        assertNotEquals(serverInfo1, serverInfo2);
        assertNotEquals(serverInfo2, serverInfo1);
        serverInfo2.setCountries(Arrays.asList(new ServerInfo.Country()));
        assertEquals(serverInfo1, serverInfo2);
        assertEquals(serverInfo2, serverInfo1);

        serverInfo1.setLanguages(Arrays.asList("en", "ru"));
        assertNotEquals(serverInfo1, serverInfo2);
        assertNotEquals(serverInfo2, serverInfo1);
        serverInfo2.setLanguages(Arrays.asList("en", "ru"));
        assertEquals(serverInfo1, serverInfo2);
        assertEquals(serverInfo2, serverInfo1);

        serverInfo1.setVerifyUrl("url1");
        assertNotEquals(serverInfo1, serverInfo2);
        assertNotEquals(serverInfo2, serverInfo1);
        serverInfo2.setVerifyUrl("url1");
        assertEquals(serverInfo1, serverInfo2);
        assertEquals(serverInfo2, serverInfo1);

        serverInfo1.setConsumerUrl("url2");
        assertNotEquals(serverInfo1, serverInfo2);
        assertNotEquals(serverInfo2, serverInfo1);
        serverInfo2.setConsumerUrl("url2");
        assertEquals(serverInfo1, serverInfo2);
        assertEquals(serverInfo2, serverInfo1);

        serverInfo1.setServiceFee(23.5d);
        assertNotEquals(serverInfo1, serverInfo2);
        assertNotEquals(serverInfo2, serverInfo1);
        serverInfo2.setServiceFee(23.5d);
        assertEquals(serverInfo1, serverInfo2);
        assertEquals(serverInfo2, serverInfo1);

        serverInfo1.setAssetsUrl("url3");
        assertNotEquals(serverInfo1, serverInfo2);
        assertNotEquals(serverInfo2, serverInfo1);
        serverInfo2.setAssetsUrl("url3");
        assertEquals(serverInfo1, serverInfo2);
        assertEquals(serverInfo2, serverInfo1);

        serverInfo1.setOAuthAuthorizeUrl("url4");
        assertNotEquals(serverInfo1, serverInfo2);
        assertNotEquals(serverInfo2, serverInfo1);
        serverInfo2.setOAuthAuthorizeUrl("url4");
        assertEquals(serverInfo1, serverInfo2);
        assertEquals(serverInfo2, serverInfo1);

        serverInfo1.setOAuthTokenUrl("url5");
        assertNotEquals(serverInfo1, serverInfo2);
        assertNotEquals(serverInfo2, serverInfo1);
        serverInfo2.setOAuthTokenUrl("url5");
        assertEquals(serverInfo1, serverInfo2);
        assertEquals(serverInfo2, serverInfo1);
    }

    @Test
    public void testCountryEquals() {
        ServerInfo.Country country1 = new ServerInfo.Country();
        ServerInfo.Country country2 = new ServerInfo.Country();
        assertEquals(country1, country2);
        assertEquals(country2, country1);

        country1.setName("some name");
        assertNotEquals(country1, country2);
        assertNotEquals(country2, country1);
        country2.setName("some name");
        assertEquals(country1, country2);
        assertEquals(country2, country1);

        country1.setNameTranslated("translated name");
        assertNotEquals(country1, country2);
        assertNotEquals(country2, country1);
        country2.setNameTranslated("translated name");
        assertEquals(country1, country2);
        assertEquals(country2, country1);

        country1.setIso("ISO");
        assertNotEquals(country1, country2);
        assertNotEquals(country2, country1);
        country2.setIso("ISO");
        assertEquals(country1, country2);
        assertEquals(country2, country1);

        country1.setLocale("Loc");
        assertNotEquals(country1, country2);
        assertNotEquals(country2, country1);
        country2.setLocale("Loc");
        assertEquals(country1, country2);
        assertEquals(country2, country1);

        country1.setPhonePrefix("+100");
        assertNotEquals(country1, country2);
        assertNotEquals(country2, country1);
        country2.setPhonePrefix("+100");
        assertEquals(country1, country2);
        assertEquals(country2, country1);

        country1.setTimeZone("GCM");
        assertNotEquals(country1, country2);
        assertNotEquals(country2, country1);
        country2.setTimeZone("GCM");
        assertEquals(country1, country2);
        assertEquals(country2, country1);
    }
}
