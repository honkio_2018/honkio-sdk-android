package com.riskpointer.sdk.api.web.message;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.riskpointer.sdk.api.exception.WrongHashException;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.web.Connection;
import com.riskpointer.sdk.api.web.protocol.RequestBuilder;
import com.riskpointer.sdk.api.web.protocol.ResponseParser;
import com.riskpointer.sdk.api.web.protocol.json.JsonRequestBuilder;
import com.riskpointer.sdk.api.web.response.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by Shurygin Denis on 2016-11-11.
 */

@SuppressLint("Assert")
@RunWith(RobolectricTestRunner.class)
public class MessageTest {

    private static final String URL_1 = "url_1";
    private static final String URL_2 = "url_2";
    private static final String COMMAND_1 = "command_1";
    private static final String COMMAND_2 = "command_2";

    private Connection connection1;
    private Connection connection2;
    private RequestBuilder requestBuilder;
    private ResponseParser<Void> voidResponseParser;

    @Before
    public void init() {
        SharedPreferences preferences = RuntimeEnvironment.application
                .getSharedPreferences("prefs", Context.MODE_PRIVATE);
        connection1 = new Connection(preferences, "login", "password", null);
        connection2 = new Connection(preferences, "login", "password", null);
        requestBuilder = new JsonRequestBuilder();
        voidResponseParser = new ResponseParser<Void>() {
            @Override
            public Response<Void> parse(Message<?> message, String responseString, String password) throws WrongHashException {
                return null;
            }
        };
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorNoContext() throws Exception {
        new Message<>(null, COMMAND_1, URL_1, connection1,
                null, null, null, requestBuilder, voidResponseParser, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorNoCommand() throws Exception {
        new Message<>(RuntimeEnvironment.application, null, URL_1, connection1,
                null, null, null, requestBuilder, voidResponseParser, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorNoRequestBuilder() throws Exception {
        new Message<>(RuntimeEnvironment.application, COMMAND_1, URL_1, connection1,
                null, null, null, null, voidResponseParser, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorNoResponseParser() throws Exception {
        new Message<>(RuntimeEnvironment.application, COMMAND_1, URL_1, connection1,
                null, null, null, requestBuilder, null, 0);
    }

    @Test
    public void testEquals() throws Exception {
        Identity shop1 = new Identity("shop1", "pass");
        Identity shop2 = new Identity("shop2", "pass");
        Identity app1 = new Identity("app1", "pass");
        Identity app2 = new Identity("app2", "pass");

        Message<Void> mes1 = new Message<>(RuntimeEnvironment.application, COMMAND_1, URL_1, connection1,
                        null, shop1, app1, requestBuilder, voidResponseParser, 0);
        Message<Void> mes2 = new Message<>(RuntimeEnvironment.application, COMMAND_1, URL_1, connection1,
                null, shop1, app1, requestBuilder, voidResponseParser, 0);

        assertEquals(mes1, mes2);

        mes2.addParameter("param_1", "value");
        assertNotEquals(mes1, mes2);
        mes1.addParameter("param_1", "value");
        assertEquals(mes1, mes2);

        assertNotEquals(mes1, new Message<>(RuntimeEnvironment.application, COMMAND_2, URL_1, connection1,
                null, shop1, app1, requestBuilder, voidResponseParser, 0));
        assertNotEquals(mes1, new Message<>(RuntimeEnvironment.application, COMMAND_1, URL_2, connection1,
                null, shop1, app1, requestBuilder, voidResponseParser, 0));
        assertNotEquals(mes1, new Message<>(RuntimeEnvironment.application, COMMAND_1, URL_1, connection2,
                null, shop1, app1, requestBuilder, voidResponseParser, 0));
        assertNotEquals(mes1, new Message<>(RuntimeEnvironment.application, COMMAND_1, URL_1, connection1,
                null, shop2, app1, requestBuilder, voidResponseParser, 0));
        assertNotEquals(mes1, new Message<>(RuntimeEnvironment.application, COMMAND_1, URL_1, connection1,
                null, shop1, app2, requestBuilder, voidResponseParser, 0));


        assertNotEquals(mes1, new Message<>(RuntimeEnvironment.application, COMMAND_1, URL_1, null,
                null, shop1, app1, requestBuilder, voidResponseParser, 0));
        assertNotEquals(mes1, new Message<>(RuntimeEnvironment.application, COMMAND_1, URL_1, connection1,
                null, null, app1, requestBuilder, voidResponseParser, 0));
        assertNotEquals(mes1, new Message<>(RuntimeEnvironment.application, COMMAND_1, URL_1, connection1,
                null, shop1, null, requestBuilder, voidResponseParser, 0));
        assertNotEquals(mes1, new Message<>(RuntimeEnvironment.application, COMMAND_1, URL_1, connection1,
                null, shop1, app1, requestBuilder, voidResponseParser, 1));

        ResponseParser<Object> anotherResponseParser = new ResponseParser<Object>() {
            @Override
            public Response<Object> parse(Message<?> message, String responseString, String password) throws WrongHashException {
                return null;
            }
        };
        assertNotEquals(mes1, new Message<>(RuntimeEnvironment.application, COMMAND_1, URL_1, connection1,
                null, shop1, app1, requestBuilder, anotherResponseParser, 0));

        assertNotEquals(mes1, new Message<Void>(RuntimeEnvironment.application, COMMAND_1, URL_1, connection1,
                null, shop1, app1, requestBuilder, voidResponseParser, 0) {});
    }

    @Test
    public void testGetHashPassword() {
        Message<Void> mes = new Message<>(RuntimeEnvironment.application, COMMAND_1, URL_1, connection1,
                null, new Identity(), new Identity(), requestBuilder, voidResponseParser, 0);
        assertEquals("", mes.getHashPassword());

        mes.setShopIdentity(new Identity("id", "password"));
        assertEquals("password", mes.getHashPassword());

        mes = new Message<>(RuntimeEnvironment.application, COMMAND_1, URL_1, connection1,
                null, new Identity("id", "shop"), new Identity("id", "app"),
                requestBuilder, voidResponseParser, 0);
        assertEquals("shopapp", mes.getHashPassword());
        mes.setShopIdentity(new Identity("id", "password"));
        assertEquals("passwordapp", mes.getHashPassword());
        mes.setShopIdentity(new Identity());
        assertEquals("app", mes.getHashPassword());
    }

    @Test
    public void testRandomString() throws Exception {
        for(int i = 10; i <= 40; i++) {
            if (Message.generateRandomString(1).equals(Message.generateRandomString(i)))
                assertNotEquals(Message.generateRandomString(1), Message.generateRandomString(1));
        }
    }

    @Test
    public void testParameters() throws Exception {
        Message<Void> mes = new Message<>(RuntimeEnvironment.application, COMMAND_1, URL_1, connection1, null,
                new Identity(), new Identity(), requestBuilder, voidResponseParser, 0);
        String name = "param_name";
        assertNull(mes.getParameter(name));

        mes.addParameter(name, "param");
        assertEquals("param", mes.getParameter(name));

        mes.removeParameter(name);
        assertNull(mes.getParameter(name));

        mes.addParameter("name1", "param1");
        mes.addParameter("name2", "param2");
        assertEquals("param1", mes.getParameter("name1"));
        assertEquals("param2", mes.getParameter("name2"));

        mes.removeParameter("name1");
        assertNull(mes.getParameter("name1"));
    }
}
