package com.riskpointer.sdk.api.web.message.filter;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Shurygin Denis
 */

public class PropertiesFilter extends AbsFilter {

    public interface Item extends Serializable {

        Map<String, Object> toParams();

    }

    public static class Equal<Type> implements Item {

        public final Type value;

        public Equal(Type value) {
            this.value = value;
        }

        @Override
        public Map<String, Object> toParams() {
            Map<String, Object> map = new HashMap<>(1);
            map.put("eq", value);
            return map;
        }
    }

    public static class NotEqual<Type> extends Equal<Type> {

        public NotEqual(Type value) {
            super(value);
        }

        @Override
        public Map<String, Object> toParams() {
            Map<String, Object> map = new HashMap<>(1);
            map.put("ne", value);
            return map;
        }
    }

    public static class Range<Type> implements Item {

        public final Type start;
        public final boolean includeStart;

        public final Type end;
        public final boolean includeEnd;

        public Range(Type start, Type end) {
            this(start, true, end, true);
        }

        public Range(Type start, boolean includeStart, Type end, boolean includeEnd) {
            this.start = start;
            this.includeStart = includeStart;
            this.end = end;
            this.includeEnd = includeEnd;
        }

        @Override
        public Map<String, Object> toParams() {
            if (this.start == null && this.end == null)
                return null;

            Map<String, Object> map = new HashMap<>(2);
            if (this.start != null) {
                if (this.includeStart)
                    map.put("gte", this.start);
                else
                    map.put("gt", this.start);
            }
            if (this.end != null) {
                if (this.includeEnd)
                    map.put("lte", this.end);
                else
                    map.put("lt", this.end);
            }
            return map;
        }
    }

    public static class InList<Type> implements Item {

        public final List<Type> list;

        public InList(Type... list) {
            this.list = Arrays.asList(list);
        }

        public InList(List<Type> list) {
            this.list = list;
        }

        @Override
        public Map<String, Object> toParams() {
            if (this.list == null || this.list.size() == 0)
                return null;

            Map<String, Object> map = new HashMap<>(1);
            map.put("in", this.list);
            return map;
        }
    }

    public static class AllInList<Type> extends InList<Type> {

        @Override
        public Map<String, Object> toParams() {
            if (this.list == null || this.list.size() == 0)
                return null;

            Map<String, Object> map = new HashMap<>(1);
            map.put("all", this.list);
            return map;
        }
    }

    private Map<String, Item> mItems = new HashMap<>();

    public PropertiesFilter() {
        this(new HashMap<String, Item>());
    }

    public PropertiesFilter(PropertiesFilter filter) {
        this(new HashMap<>(filter.mItems));
    }

    private PropertiesFilter(Map<String, Item> items) {
        mItems = items;
    }

    @Override
    public Map<String, Object> toParams() {
        Map<String, Object> params = new HashMap<>(mItems.size());
        for (Map.Entry<String, Item> entry: mItems.entrySet()) {
            Object itemParams = entry.getValue().toParams();
            if (itemParams != null)
                params.put(entry.getKey(), itemParams);
        }
        return params;
    }

    public PropertiesFilter addItem(String key, Item item) {
        mItems.put(key, item);
        return this;
    }

    public Item getItem(String key) {
        return mItems.get(key);
    }

}
