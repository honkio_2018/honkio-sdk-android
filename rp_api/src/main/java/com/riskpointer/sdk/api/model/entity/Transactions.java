/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Data object that holds list of the Transactions.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
public class Transactions implements Serializable {
	private static final long serialVersionUID = 5632385478584609933L;

	private final long mTimeStamp;
	private final long mTimeDifference;
	private ArrayList<Transaction> mTransactionsList;
	private HashMap<String, Order> mOrdersMap;

	/**
	 * Construct Transaction object with timeStamp equal current time.<br>
	 * Time difference equal zero.
	 */
	public Transactions() {
		this(System.currentTimeMillis());
	}

	/**
	 * Construct Transactions with specified Time stamp.<br>
	 * Time difference calculate as: timeStamp - current time.
	 * @param timeStamp Server time in millis when Transactions list was sent.
	 */
	public Transactions(long timeStamp) {
		this(timeStamp, timeStamp - System.currentTimeMillis());
	}

	/**
	 * Construct Transactions with specified Time stamp and time difference.<br>
	 * @param timeStamp Server time in millis when Transactions list was sent.
	 * @param timeDifference Difference between server and device time.
	 */
	public Transactions(long timeStamp, long timeDifference) {
		mTimeStamp = timeStamp;
		mTimeDifference = timeDifference;
	}

	/**
	 * Gets Server time in millis when Transactions list was sent.
	 * @return Server time in millis when Transactions list was sent.
	 */
	public long getTimeStamp() {
		return mTimeStamp;
	}

	/** 
	 * Returns current time on the Server.
	 * @return Time on the Server in millis.
	 */
	public long currentTimeMillis() {
		return currentTimeMillis(System.currentTimeMillis());
	}
	
	/** 
	 * Returns time on the Server for specified device time.
	 * @param deviceTimeMillis Device time in millis.
	 * @return Time on the Server for specified device time.
	 */
	public long currentTimeMillis(long deviceTimeMillis) {
		return deviceTimeMillis + mTimeDifference;
	}

	/**
	 * Gets difference in millis between server and device time.
	 * @return Difference in millis between server and device time.
	 */
	public long getTimeDifference() {
		return mTimeDifference;
	}

	public ArrayList<Transaction> getTransactionsList() {
		return mTransactionsList;
	}

	public void setTransactionsList(ArrayList<Transaction> transactionsList) {
		mTransactionsList = transactionsList;
	}

	public HashMap<String, Order> getOrdersMap() {
		return mOrdersMap;
	}

	public void setOrdersMap(HashMap<String, Order> ordersMap) {
		mOrdersMap = ordersMap;
	}

	/**
	 * Merge this Transactions list with another list.<br>
	 * <b>WARNING!!! This method has bad realisation and must be improved.</b>
	 * @param transactions Transactions list that should be merged into current List.
	 */
	public void merge(Transactions transactions) {
		mTransactionsList.addAll(transactions.getTransactionsList());
	}
}
