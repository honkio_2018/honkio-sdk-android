package com.riskpointer.sdk.api.model.push.entity;

import android.util.Log;

import com.riskpointer.sdk.api.model.push.Push;

import java.util.Map;

/**
 * @author Shurygin Denis
 */
public class RoleChangedPushEntity extends Push.PushEntity {

    public final class Action {

        private Action() {
        }

        public final static String CREATED = "created";
        public final static String UPDATED = "updated";
        public final static String REMOVED = "removed";

    }

    public RoleChangedPushEntity(Map<String, Object> content) {
        super(Push.Type.ROLE_CHANGED, content);
    }

    public void setRoleDisplayName(String value) {
        setContent("display_name", value);
    }

    public String getRoleDisplayName() {
        return (String) getContent("display_name");
    }

    public void setRoleId(String value) {
        setContent("role_id", value);
    }

    public String getRoleId() {
        return (String) getContent("role_id");
    }

    public void setMerchantId(String value) {
        setContent("merchant", value);
    }

    public String getMerchantId() {
        return (String) getContent("merchant");
    }

    public void setAction(String value) {
        setContent("action", value);
    }

    public String getAction() {
        return (String) getContent("action");
    }

    public boolean actionIs(String action) {
        String pushAction = getAction();
        return (action != null && action.equals(pushAction))
                || (pushAction == null && action == null);
    }

    public void setUserId(String value) {
        setContent("user_id", value);
    }

    public String getUserId() {
        if (getContent("user_id") instanceof String) {
            return (String) getContent("user_id");
        } else {
            return "";
        }
    }
}
