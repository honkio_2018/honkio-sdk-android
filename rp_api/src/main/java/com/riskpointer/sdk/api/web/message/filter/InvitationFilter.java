package com.riskpointer.sdk.api.web.message.filter;

import android.text.TextUtils;

import com.riskpointer.sdk.api.web.message.Message;

import java.util.HashMap;

/**
 * Created by I.N. on 2017-02-16.
 */
public class InvitationFilter extends ListFilter {
    private static final long serialVersionUID = -4755206533226004785L;
    private String mParentOrderId;
    private String mReceiverId;
    private String mReceiverPhone;
    private String mReceiverEmail;
    private Boolean mOrderDetails;
    private Boolean mProductDetails;
    private Boolean mUsed;
    private Boolean mExcludeExpired;
    private String mQueryId;
    private String[] mStatuses;

    public Boolean isOrderDetails() {
        return mOrderDetails;
    }

    public void setOrderDetails(Boolean orderDetails) {
        mOrderDetails = orderDetails;
    }

    public Boolean isProductDetails() {
        return mProductDetails;
    }

    public void setProductDetails(Boolean productDetails) {
        mProductDetails = productDetails;
    }

    public Boolean isUsed() {
        return mUsed;
    }

    public void setUsed(Boolean used) {
        mUsed = used;
    }

    public String getParentOrderId() {
        return mParentOrderId;
    }

    public void setParentOrderId(String parentOrderId) {
        mParentOrderId = parentOrderId;
    }

    public String getReceiverId() {
        return mReceiverId;
    }

    public void setReceiverId(String receiverId) {
        mReceiverId = receiverId;
    }

    public String getReceiverPhone() {
        return mReceiverPhone;
    }

    public void setReceiverPhone(String receiverPhone) {
        mReceiverPhone = receiverPhone;
    }

    public String getReceiverEmail() {
        return mReceiverEmail;
    }

    public void setReceiverEmail(String receiverEmail) {
        mReceiverEmail = receiverEmail;
    }

    public Boolean isExcludeExpired() {
        return mExcludeExpired;
    }

    public void setExcludeExpired(Boolean excludeExpired) {
        mExcludeExpired = excludeExpired;
    }

    public String getQueryId() {
        return mQueryId;
    }

    public void setQueryId(String queryId) {
        mQueryId = queryId;
    }

    public String[] getStatuses() {
        return mStatuses;
    }

    public void setStatus(String[] statuses) {
        this.mStatuses = statuses;
    }

    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = super.toParams();
        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_RECEIVER_ID, getReceiverId());
        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_RECEIVER_EMAIL, getReceiverEmail());
        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_RECEIVER_PHONE, getReceiverPhone());
        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_PARENT_ORDER_ID, getParentOrderId());
        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_ORDER_DETAILS, isOrderDetails());
        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_PRODUCT_DETAILS, isProductDetails());
        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_USED, isUsed());
        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_EXCLUDE_EXPIRED, isExcludeExpired());
        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_ID, getQueryId());
        if (getStatuses() != null)
            applyIfNotNull(params, Message.Param.QUERY_STATUS, TextUtils.join("|", getStatuses()));
        return params;
    }
}
