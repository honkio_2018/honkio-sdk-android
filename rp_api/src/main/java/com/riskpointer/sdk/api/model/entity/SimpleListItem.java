package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;

/**
 * Data object that holds id, name and string that represent this object.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
public class SimpleListItem implements Serializable {

    /** ID of the Item */
    public final String id;
    /** String value of the Item */
    private final String string;
    /** Name of the Item */
    public final String name;

    /**
     * Creates new Item with specified params.
     * @param id ID of the Item
     * @param string String value of the Item
     * @param name Name of the Item
     */
    public SimpleListItem(String id, String string, String name) {
        this.id = id;
        this.string = string;
        this.name = name;
    }

    @Override
    public String toString() {
        if (name != null)
            return name;
        return string;
    }
}
