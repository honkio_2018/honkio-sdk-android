package com.riskpointer.sdk.api.web.message.filter;

import java.util.HashMap;

public class MerchantProductsFilter extends ListFilter {
    private String mMerchantId;

    public String getMerchantId() {
        return mMerchantId;
    }

    public void setMerchantId(String id) {
        this.mMerchantId = id;
    }

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = super.toParams();
        AbsFilter.applyIfNotNull(params, "query_merchant", mMerchantId);
        return params;
    }
}
