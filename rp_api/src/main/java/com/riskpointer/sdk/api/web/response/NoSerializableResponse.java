/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.response;

import com.riskpointer.sdk.api.web.message.Message;

/**
 * No Serializable part of the response. Information from this class will not send thru broadcasts and Bundle.<br>
 * Contains an additional information that used for inner usage.
 * @param <T> Type of the response result object.
 *
 * @author Denis Shurygin
 */
public class NoSerializableResponse<T> {
	private Message<T> mMessage;

	/**
	 * Gets message that was used in the request.
	 * @return Message that was used in the request. May be null. Will disappear after serialization.
	 */
	public Message<T> getMessage() {
		return mMessage;
	}

	/**
	 * Sets Message that was used in the request.
	 * @param message Message that was used in the request. Will disappear after serialization.
	 */
	public void setMessage(Message<T> message) {
		this.mMessage = message;
	}
}
