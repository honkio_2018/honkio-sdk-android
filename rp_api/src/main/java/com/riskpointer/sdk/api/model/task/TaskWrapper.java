/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.task;

/**
 * Wrapper for {@link Task}.
 */
public class TaskWrapper implements Task {
	private volatile Task mTask;
	private volatile boolean isAborted;

	/**
	 * Construct new wrapper. Wrapped task is null.
	 */
	public TaskWrapper() {
		this(null);
	}

	/**
	 * Construct new wrapper.
	 * @param task Task that will be wrapped.
	 */
	public TaskWrapper(Task task) {
		mTask = task;
	}

	/**
	 * Sets new wrapped task. If wrapper already aborted new task will aborted to.
	 * @param task New task to set.
	 */
	public synchronized void set(Task task) {
		if(isAborted)
			task.abort();
		mTask = task;
	}

	/**
	 * Gets wrapped task.
	 * @return Wrapped task.
	 */
    public Task get() {
        return mTask;
    }

	@Override
	public synchronized boolean abort() {
		isAborted = true;
		if(mTask != null)
			mTask.abort();
		return isAborted();
	}

	@Override
	public boolean isAborted() {
        if(mTask != null)
		    return mTask.isAborted();
        return isAborted;
	}

    public boolean isCompleted() {
        if(mTask != null)
            return mTask.isCompleted();
        return false;
    }
}
