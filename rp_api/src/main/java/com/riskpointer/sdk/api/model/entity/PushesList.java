package com.riskpointer.sdk.api.model.entity;

import com.riskpointer.sdk.api.model.push.Push;
import com.riskpointer.sdk.api.web.message.filter.ListFilter;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Shurygin Denis
 */

// TODO docs
public class PushesList implements Serializable {

    private ListFilter mFilter;
    private ArrayList<Push<?>> mList;

    private int mTotalCount;
    private int mUnreadedCount;

    /**
     * Gets list of Notifications.
     * @return List of Notifications.
     */
    public ArrayList<Push<?>> getList() {
        return mList;
    }

    /**
     * Sets list of Notifications.
     * @param list List of Notifications.
     */
    public void setList(ArrayList<Push<?>> list) {
        this.mList = list;
    }

    /**
     * Gets the Filter which was used to get Notifications list.
     * @return Filter which was used to get Notifications list.
     */
    public ListFilter getFilter() {
        return mFilter;
    }

    /**
     * Sets the Filter which was used to get Notifications list.
     * @param filter Filter which was used to get Notifications list.
     */
    public void setFilter(ListFilter filter) {
        this.mFilter = filter;
    }

    // TODO docs
    public int getTotalCount() {
        return mTotalCount;
    }

    // TODO docs
    public int getUnreadedCount() {
        return mUnreadedCount;
    }

    // TODO docs
    public void setUnreadedCount(int count) {
        this.mUnreadedCount = count;
    }

    // TODO docs
    public void setTotalCount(int count) {
        this.mTotalCount = count;
    }
}
