/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.process;

import android.content.Context;
import android.os.Bundle;

/**
 * Base Process model.
 */
public class BaseProcessModel {

	private static final String SAVED_STATUS = "BaseProcessModel.SAVED_STATUS";
	private static final String SAVED_CLASS = "BaseProcessModel.SAVED_CLASS";

	/**
	 * Available statuses of the Process model.
	 */
    public enum Status {
		/** Process just created or reset. */
		CREATED,
		/** Process started. */
		STARTED,
		/** Process await for special condition to start again. */
		PENDING,
		/** Process finish work. */
		FINISHED,
		/** Process aborted. */
		ABORTED,
	}
	
	private Status mStatus = Status.CREATED;

	/**
	 * Public constructor.
	 */
	public BaseProcessModel() {
	}

	/**
	 * Gets status of the model.
	 * @return Status.
	 */
	public synchronized Status getStatus() {
		return mStatus;
	}

	/**
	 * Sets status of the model.
	 * @param status Status
	 */
	protected synchronized void setStatus(Status status) {
		this.mStatus = status;
	}

	/**
	 * Abortion of the process.
	 */
	public synchronized boolean abort() {
		mStatus = Status.ABORTED;
		return true;
	}

	/**
	 * Reset model and set status {@link BaseProcessModel.Status#CREATED}.
	 */
	public synchronized void reset() {
		mStatus = Status.CREATED;
	}

	/**
	 * Check if process is aborted.
	 * @return True if process aborted, false otherwise.
	 */
	public synchronized boolean isAborted() {
		return mStatus == Status.ABORTED;
	}

	/**
	 * Check if process is completed.
	 * @return True if process completed, false otherwise.
	 */
    public synchronized boolean isCompleted() {
        return mStatus == Status.FINISHED;
    }

	/**
	 * Starting the process. Process can be started only in status {@link BaseProcessModel.Status#CREATED}.
	 */
    public void start() {
		if(!isAborted()) {
			if(getStatus() == Status.CREATED) {
				setStatus(Status.STARTED);
			}
			else
				throw new IllegalStateException(this.getClass().getSimpleName() + " is started. Use a new object.");
		}
		else
			throw new IllegalStateException(this.getClass().getSimpleName() + " is started. Use a new object.");
	}

	public Bundle saveInstanceState() {
		Bundle instanceState = new Bundle();
		instanceState.putSerializable(SAVED_CLASS, getClass());
		instanceState.putSerializable(SAVED_STATUS, mStatus);
		return instanceState;
	}

	public void restoreInstanceState(Context context, Bundle instanceState) {
		mStatus = (Status) instanceState.getSerializable(SAVED_STATUS);
	}

	public static BaseProcessModel restoreFromSavedState(Context context, Bundle instanceState) {
		if (instanceState.containsKey(SAVED_CLASS)) {
			Object modelClass = instanceState.getSerializable(SAVED_CLASS);
			if (modelClass instanceof Class<?>) {
				Object model = null;
				try {
					model = ((Class<?>) modelClass).newInstance();
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				if (model != null && model instanceof BaseProcessModel) {
					((BaseProcessModel) model).restoreInstanceState(context, instanceState);
					return (BaseProcessModel) model;
				}

			}
		}
		return null;
	}
}
