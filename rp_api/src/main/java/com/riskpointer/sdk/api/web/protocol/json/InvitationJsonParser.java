package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Invitation;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONObject;

import static com.riskpointer.sdk.api.web.protocol.json.OrderJsonParser.parseOrder;

/**
 * @author Shurygin Denis
 */

public class InvitationJsonParser extends BaseJsonResponseParser<Invitation> {
    @Override
    public Invitation parseJson(Message<?> message, JSONObject json) {
        return parseInvitation(json);
    }

    public static Invitation parseInvitation(JSONObject json) {
        Invitation invitation = null;
        JSONObject invitationJson = json.optJSONObject("invitation");
        if (invitationJson != null) {
            invitation = new Invitation();
            invitation.setId(invitationJson.optString("_id"));
            invitation.setBonus(invitationJson.optDouble("bonus", 0));
            invitation.setMessage(invitationJson.optString("message"));
            if ("null".equals(invitation.getMessage()))
                invitation.setMessage(null);
            invitation.setUsed(invitationJson.optBoolean("used"));
            JSONObject customFields = invitationJson.optJSONObject("invite_fields");
            if (customFields != null) {
                invitation.setCustomFields(jsonToMap(customFields));
            }
            JSONObject inviter = invitationJson.optJSONObject("inviter");
            if (inviter != null) {
                invitation.setInviter(jsonToMap(inviter));
            }

            invitation.setStatus(invitationJson.optString("status"));
            if ("null".equals(invitation.getStatus()))
                invitation.setStatus(null);

            JSONObject order = invitationJson.optJSONObject("order");
            if (order != null) {
                invitation.setOrder(parseOrder(order));
            }
            invitation.setReceiverId(invitationJson.optString("receiver_id"));
            if ("None".equals(invitation.getReceiverId()))
                invitation.setReceiverId(null);
        }
        return invitation;
    }
}
