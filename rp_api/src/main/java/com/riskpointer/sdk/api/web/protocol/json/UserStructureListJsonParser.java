package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.HkStructure;
import com.riskpointer.sdk.api.model.entity.HkStructureList;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * @author I.N.
 * user role structure, description of user roles
 */

public class UserStructureListJsonParser extends BaseJsonResponseParser<HkStructureList> {

    @Override
    protected HkStructureList parseJson(Message<?> message, JSONObject json) {
        ArrayList<HkStructure> list = new ArrayList<>();
        JSONArray structureJsonArray = json.optJSONArray("descriptions");
        if (structureJsonArray != null) {
            for (int i = 0; i < structureJsonArray.length(); i++) {
                JSONObject structureJson = structureJsonArray.optJSONObject(i);
                if (structureJson != null) {
                    list.add(UserStructureJsonParser.toStructure(structureJson));
                }
            }
        }

        HkStructureList structureList = new HkStructureList();
        structureList.setList(list);

        return structureList;
    }
}
