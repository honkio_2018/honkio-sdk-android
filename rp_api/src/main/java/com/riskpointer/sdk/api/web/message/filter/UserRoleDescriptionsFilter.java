package com.riskpointer.sdk.api.web.message.filter;

import java.util.HashMap;

/**
 * @author Shurygin Denis
 */

public class UserRoleDescriptionsFilter extends ListFilter {

    private String mType;

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = super.toParams();
        applyIfNotNull(params, "query_type", mType);
        return params;
    }
}
