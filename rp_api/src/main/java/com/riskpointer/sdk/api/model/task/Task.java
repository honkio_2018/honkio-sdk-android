/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.task;

/**
 * Interface that represent a some task that can be aborted.
 * Method abort() doesn't guarantees immediately abortion of the task, some tasks doesn't allow
 * abortion after switching to special stage. For correct behaviour see concrete realisation.
 */
public interface Task {

    /**
     * Abortion of the task. Method doesn't guarantees abortion of the task.
     * @return True if task is aborted, false otherwise.
     */
	public boolean abort();

    /**
     * Check if task is aborted.
     * @return True if task is aborted, false otherwise.
     */
	public boolean isAborted();

    /**
     * Check if task is completed.
     * @return True if task is completed, false otherwise.
     */
    public boolean isCompleted();
}
