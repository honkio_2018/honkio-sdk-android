package com.riskpointer.sdk.api.utils;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;

import com.riskpointer.sdk.api.model.push.Push;
import com.riskpointer.sdk.api.support.BroadcastManager;
import com.riskpointer.sdk.api.web.WebServiceAsync;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.response.Response;

import java.util.ArrayList;

/**
 * A helper class to send broadcast via {@link BroadcastManager}.
 *
 * @see BroadcastManager
 * @author Shurygin Denis on 2014-11-20.
 */
public final class BroadcastHelper {

    private static final String PACKAGE = "com.riskpointer.api";

    /**
     * Action that invoke when new Google Cloud Message received.
     * Action will contains following extras: {@link #BROADCAST_EXTRA_PUSH}.
     * @see #sendActionOnPushReceived(Context, Push)
     * @see #BROADCAST_EXTRA_PUSH
     */
    public static final String BROADCAST_ACTION_ON_PUSH_RECEIVED =
            PACKAGE + ".ON_PUSH_RECEIVED";

    /**
     * Extra which contains {@link Push} object.
     * @see #BROADCAST_ACTION_ON_PUSH_RECEIVED
     */
    public static final String BROADCAST_EXTRA_PUSH = "BroadcastHelper.BROADCAST_EXTRA_PUSH";

    /**
     * Action that invoke when device location is changed.
     * Action will contains following extras: {@link #BROADCAST_EXTRA_LOCATION}.
     * @see #sendActionOnLocationChanged(Context, Location)
     * @see #BROADCAST_EXTRA_LOCATION
     */
    public static final String BROADCAST_ACTION_ON_LOCATION_CHANGED =
            PACKAGE + ".ON_LOCATION_CHANGED";

    /**
     * Extra which contains {@link Location} object.
     * @see #BROADCAST_ACTION_ON_LOCATION_CHANGED
     */
    public static final String BROADCAST_EXTRA_LOCATION = "BroadcastHelper.BROADCAST_EXTRA_LOCATION";

    /**
     * Prefix of the action that invoke when API receives accept server response.
     * Action will contains following extras: {@link #BROADCAST_EXTRA_RESPONSE}.
     * @see #getActionOnComplete(String)
     * @see #sendActionOnComplete(Context, String, Response)
     * @see #BROADCAST_EXTRA_RESPONSE
     */
    public static final String BROADCAST_ACTION_ON_COMPLETE =
            PACKAGE + ".ON_COMPLETE";

    /**
     * Action that invoke when API login.
     */
    public static final String BROADCAST_ACTION_LOGIN =
            PACKAGE + ".BROADCAST_ACTION_LOGIN";

    /**
     * Action that invoke when API initialized.
     */
    public static final String BROADCAST_ACTION_INITIALIZED =
            PACKAGE + ".BROADCAST_ACTION_INITIALIZED";

    /**
     * Action that invoke when application doesn't handle response error.
     * Action will contains following extras: {@link #BROADCAST_EXTRA_COMMAND},
     * {@link #BROADCAST_EXTRA_ERROR}.
     * @see #sendActionUnhandledError
     * @see #BROADCAST_EXTRA_COMMAND
     * @see #BROADCAST_EXTRA_ERROR
     */
    public static final String BROADCAST_ACTION_UNHANDLED_ERROR =
            PACKAGE + ".UNHANDLED_ERROR";

    /**
     * Action that invoke when application doesn't handle response.
     * Action will contains following extras: {@link #BROADCAST_EXTRA_COMMAND},
     * {@link #BROADCAST_EXTRA_RESPONSE}.
     * @see #sendActionUnhandledError
     * @see #BROADCAST_EXTRA_COMMAND
     * @see #BROADCAST_EXTRA_RESPONSE
     */
    public static final String BROADCAST_ACTION_UNHANDLED_RESPONSE =
            PACKAGE + ".UNHANDLED_RESPONSE";

    /**
     * Extra which contains {@link Response} object.
     * @see #BROADCAST_ACTION_ON_COMPLETE
     * @see #BROADCAST_ACTION_UNHANDLED_RESPONSE
     */
    public static final String BROADCAST_EXTRA_RESPONSE = "response";

    /**
     * Extra which contains String command.
     * @see #BROADCAST_ACTION_UNHANDLED_ERROR
     * @see #BROADCAST_ACTION_UNHANDLED_RESPONSE
     */
    public static final String BROADCAST_EXTRA_COMMAND = "command";

    /**
     * Extra which contains {@link java.util.ArrayList} of {@link StackTraceElement}.
     * @see #BROADCAST_ACTION_UNHANDLED_ERROR
     * @see #BROADCAST_ACTION_UNHANDLED_RESPONSE
     */
    public static final String BROADCAST_EXTRA_STACKTRACE = "stacktrace";


    /**
     * Extra which contains {@link RpError} object.
     * @see #BROADCAST_ACTION_UNHANDLED_ERROR
     */
    public static final String BROADCAST_EXTRA_ERROR = "error";

    private BroadcastHelper() {}

    /**
     * Sends Broadcast message with action {@link #BROADCAST_ACTION_ON_PUSH_RECEIVED}.
     * @param context Application context.
     * @param push Push message.
     */
    public static void sendActionOnPushReceived(Context context, Push<?> push) {
        Intent broadcastIntent = new Intent(BROADCAST_ACTION_ON_PUSH_RECEIVED);
        broadcastIntent.putExtra(BROADCAST_EXTRA_PUSH, push);
        BroadcastManager.getInstance(context).sendBroadcast(broadcastIntent);
    }

    /**
     * Sends Broadcast message with action {@link #BROADCAST_ACTION_ON_LOCATION_CHANGED}.
     * @param context Application context.
     * @param location New location.
     */
    public static void sendActionOnLocationChanged(Context context, Location location) {
        Intent broadcastIntent = new Intent(BROADCAST_ACTION_ON_LOCATION_CHANGED);
        broadcastIntent.putExtra(BROADCAST_EXTRA_LOCATION, location);
        BroadcastManager.getInstance(context).sendBroadcast(broadcastIntent);
    }

    /**
     * Construct onComplete Broadcast action for specified command.
     * @param command Command of the action.
     * @return onComplete Broadcast action for specified command.
     */
    public static String getActionOnComplete(String command) {
        return BROADCAST_ACTION_ON_COMPLETE + "." + command;
    }

    /**
     * Construct onComplete Broadcast {@link android.content.IntentFilter} for specified command.
     * @param command Command of the {@link android.content.IntentFilter}.
     * @return onComplete Broadcast {@link android.content.IntentFilter} for specified command.
     */
    public static IntentFilter getIntentFilterOnComplete(String command) {
        return new IntentFilter(BroadcastHelper.getActionOnComplete(command));
    }

    /**
     * Sends Broadcast message with onComplete action.
     * @param context Application context.
     * @param command Command of the action.
     * @param response Server response object.
     * @see #getActionOnComplete(String)
     */
    public static void sendActionOnComplete(Context context, String command, Response<?> response) {
        if(response.isStatusAccept()) {
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(getActionOnComplete(command));
            broadcastIntent.putExtra(BROADCAST_EXTRA_RESPONSE, response);
            BroadcastManager.getInstance(context).sendBroadcast(broadcastIntent);
        }
    }

    public static void sendActionLogin(Context context) {
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(BROADCAST_ACTION_LOGIN);
            BroadcastManager.getInstance(context).sendBroadcast(broadcastIntent);
    }

    /**
     * Sens Broadcast message with action {@link #BROADCAST_ACTION_UNHANDLED_ERROR}.
     * @param context Application context.
     * @param command Command that was used for the request.
     * @param error Server response error.
     * @see Message#FLAG_NO_ERROR_WARNING
     */
    public static void sendActionUnhandledError(Context context, String command, RpError error, StackTraceElement[] stackTrace) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(BROADCAST_ACTION_UNHANDLED_ERROR);
        broadcastIntent.putExtra(BROADCAST_EXTRA_COMMAND, command);
        broadcastIntent.putExtra(BROADCAST_EXTRA_ERROR, error);

        if (stackTrace != null) {
            broadcastIntent.putExtra(BROADCAST_EXTRA_STACKTRACE, wrapStackTrace(stackTrace));
        }
        BroadcastManager.getInstance(context).sendBroadcast(broadcastIntent);
    }

    /**
     * Sends Broadcast message with action {@link #BROADCAST_ACTION_UNHANDLED_RESPONSE}.
     * @param context Application context.
     * @param command Command that was used for the request.
     * @param response Server response object.
     * @see Message#FLAG_NO_RESPONSE_WARNING
     */
    public static void sendActionUnhandledResponse(Context context, String command, Response<?> response, StackTraceElement[] stackTrace) {
        Message<?> message = response.getMessage();
        if (message == null || !message.isHasFlag(Message.FLAG_NO_RESPONSE_WARNING)) {
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(BROADCAST_ACTION_UNHANDLED_RESPONSE);
            broadcastIntent.putExtra(BROADCAST_EXTRA_COMMAND, command);
            broadcastIntent.putExtra(BROADCAST_EXTRA_RESPONSE, response);

            if (stackTrace != null) {
                broadcastIntent.putExtra(BROADCAST_EXTRA_STACKTRACE, wrapStackTrace(stackTrace));
            }
            BroadcastManager.getInstance(context).sendBroadcast(broadcastIntent);
        }
    }

    private static ArrayList<StackTraceElement> wrapStackTrace(StackTraceElement[] stackTrace) {
        ArrayList<StackTraceElement> list = new ArrayList<>(stackTrace.length);
        for (StackTraceElement element: stackTrace) {
            if (Thread.class.getCanonicalName().equals(element.getClassName())) {
                if ("getStackTrace".equals(element.getMethodName()))
                    continue;
                else if ("getAllStackTraces".equals(element.getMethodName()))
                    continue;
            }
            else if ("dalvik.system.VMStack".equals(element.getClassName()))
                continue;
            else if (WebServiceAsync.class.getCanonicalName().equals(element.getClassName())
                    && "execute".equals(element.getMethodName()))
                continue;

            list.add(element);
        }
        return list;
    }
}
