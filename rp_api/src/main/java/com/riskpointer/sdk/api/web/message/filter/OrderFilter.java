package com.riskpointer.sdk.api.web.message.filter;

import android.text.TextUtils;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Order;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.message.Message;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Filter for request to get list of orders.
 *
 * @author Denis Shurygin
 * @see Order
 * @see HonkioApi#userGetOrders(OrderFilter, int, RequestCallback)
 */
public class OrderFilter extends ListFilter implements Serializable {

    private HashMap<String, String[]> mModelsStatuses; //  ex. "query_models_statuses": {"xclu_event_order": "initialized", "xclu_invitee_order": ["accepted", "rejected"]}
    private String[] mStatuses;
    private String mOwner;
    private String mThirdPerson;
    private String mModel;
    private boolean mChildMerchants;
    private HashMap<String, Object> mCustom;
    private boolean mExcludeExpired;
    private boolean mProductDetails;
    private boolean mQueryUnreadMessages;
    private boolean mQueryOnlyCount;
    private String mQueryParentOrderId;

    public OrderFilter(OrderFilter filter) {
        if (filter.mStatuses != null)
            this.mStatuses = Arrays.copyOf(filter.mStatuses, filter.mStatuses.length);
        this.mOwner = filter.mOwner;
        this.mThirdPerson = filter.mThirdPerson;
        this.mModel = filter.mModel;
        this.mChildMerchants = filter.mChildMerchants;
        this.mExcludeExpired = filter.mExcludeExpired;
        this.mProductDetails = filter.mProductDetails;
        this.mQueryUnreadMessages = filter.mQueryUnreadMessages;
        this.mQueryOnlyCount = filter.mQueryOnlyCount;
        if (this.mCustom != null)
            this.mCustom = new HashMap<String, Object>(filter.mCustom);
        if (this.mModelsStatuses != null)
            this.mModelsStatuses = new HashMap<String, String[]>(filter.mModelsStatuses);
        this.mQueryParentOrderId = filter.mQueryParentOrderId;
    }

    public OrderFilter() {
    }

    /**
     * Gets third person user id.
     *
     * @return Third person user id.
     */
    public String getThirdPerson() {
        return mThirdPerson;
    }

    /**
     * Sets third person user id.
     *
     * @param thirdPerson Third person user id.
     */
    public void setThirdPerson(String thirdPerson) {
        this.mThirdPerson = thirdPerson;
    }

    /**
     * Gets order owner user id.
     *
     * @return Order owner user id.
     */
    public String getOwner() {
        return mOwner;
    }

    /**
     * Sets order owner user id.
     *
     * @param ownerId Order owner user id.
     */
    public void setOwner(String ownerId) {
        this.mOwner = ownerId;
    }

    /**
     * Gets Order statuses specified for the filter.
     *
     * @return Order statuses specified for the filter.
     */
    public String[] getStatuses() {
        return mStatuses;
    }

    /**
     * Sets Order statuses. Only Orders with the specified status will be found.
     *
     * @param statuses Order statuses.
     */
    public void setStatus(String[] statuses) {
        this.mStatuses = statuses;
    }

    /**
     * Gets if expired orders should be excluded from response.
     *
     * @return True if expired orders should be excluded from response, false otherwise.
     */
    public boolean isExcludeExpired() {
        return mExcludeExpired;
    }

    /**
     * Sets if expired orders should be excluded from response.
     *
     * @param excludeExpired True if expired orders should be excluded from response, false otherwise.
     */
    public void setExcludeExpired(boolean excludeExpired) {
        mExcludeExpired = excludeExpired;
    }

    //TODO write docs
    public String getModel() {
        return mModel;
    }

    //TODO write docs
    public void setModel(String mModel) {
        this.mModel = mModel;
    }

    public boolean isChildMerchants() {
        return mChildMerchants;
    }

    public void setChildMerchants(boolean childMerchants) {
        mChildMerchants = childMerchants;
    }

    /**
     * Gets Pair of custom parameter
     *
     * @return Pair<String, String> where first string is filter query field name, and the second is value
     */
    public HashMap<String, Object> getCustom() {
        return mCustom;
    }

    /**
     * Sets Pair custom parameter
     *
     * @param key   is custom Filter key
     * @param value is custom Filter value
     */
    public void addCustom(String key, Object value) {
        if (mCustom == null) mCustom = new HashMap<>();
        this.mCustom.put(key, value);
    }

    public void removeCustom(String key) {
        if (mCustom != null)
            mCustom.remove(key);
    }

    public HashMap<String, String[]> getModelsStatuses() {
        return mModelsStatuses;
    }

    public void removeModelsStatuses(String key) {
        if (mModelsStatuses != null)
            mModelsStatuses.remove(key);
    }

    public void addModelsStatuses(String key, String[] value) {
        if (mModelsStatuses == null) mModelsStatuses = new HashMap<>();
        this.mModelsStatuses.put(key, value);
    }

    public boolean isProductDetails() {
        return mProductDetails;
    }

    public void setProductDetails(boolean showProductDetails) {
        this.mProductDetails = showProductDetails;
    }

    public boolean isQueryUnreadMessages() {
        return mQueryUnreadMessages;
    }

    public void setQueryUnreadMessages(boolean queryUnreadMessages) {
        this.mQueryUnreadMessages = queryUnreadMessages;
    }

    public boolean isQueryOnlyCount() {
        return mQueryOnlyCount;
    }

    public void setQueryOnlyCount(boolean queryOnlyCount) {
        this.mQueryOnlyCount = queryOnlyCount;
    }

    public String getQueryParentOrderId() {
        return mQueryParentOrderId;
    }

    public void setQueryParentOrderId(String queryParentOrderId) {
        mQueryParentOrderId = queryParentOrderId;
    }

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = super.toParams();

        if (isExcludeExpired())
            applyIfNotNull(params, Message.Param.QUERY_EXCLUDE_EXPIRED, "1");
        if (getStatuses() != null)
            applyIfNotNull(params, Message.Param.QUERY_STATUS, TextUtils.join("|", getStatuses()));
        applyIfNotNull(params, Message.Param.QUERY_OWNER, getOwner());
        applyIfNotNull(params, Message.Param.QUERY_THIRD_PERSON, getThirdPerson());
        applyIfNotNull(params, "query_model", getModel());
        if (isProductDetails())
            params.put("query_product_details", true);
        if (isQueryUnreadMessages())
            params.put("query_unread_messages", true);
        if (isQueryOnlyCount())
            params.put("query_only_count", true);
        if (isChildMerchants())
            params.put(Message.Param.QUERY_CHILD_MERCHANTS, true);
        if (mCustom != null) {
            for (Map.Entry<String, Object> custom : mCustom.entrySet()) {
                applyIfNotNull(params, custom.getKey(), custom.getValue());
            }
        }
        if (mModelsStatuses != null) {
            AbsFilter.applyIfNotNull(params, "query_models_statuses", getModelsStatuses());
        }
        applyIfNotNull(params, Message.Param.QUERY_PARENT_ORDER_ID, getQueryParentOrderId());
        return params;
    }
}
