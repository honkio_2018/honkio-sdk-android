/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Formatter;
import java.util.Iterator;
import java.util.Locale;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Class that calculate the SHA hash of a string/JSON for a secret key.
 */
public final class ApiHash {
	/** SHA-512 hash algorithm. */
	public static final String SHA512_HASH_ALGORITHM = "HmacSHA512";
	/** SHA-256 hash algorithm. */
	public static final String SHA256_HASH_ALGORITHM = "HmacSHA256";

	private ApiHash() {}

	/**
	 * Calculates SHA-512 hash of the JSON object.
	 * @param json JSON object which hash should be calculated.
	 * @param key Secret Key.
	 * @return SHA-512 hash.
	 */
	public static String sha512(JSONObject json, String key) {
        return sha512(findValues(json).trim(), key);
	}

	/**
	 * Calculates SHA-512 hash of the String.
	 * @param string String which hash should be calculated.
	 * @param key Secret Key.
	 * @return SHA-512 hash.
	 */
	public static String sha512(String string, String key) {
		return sha(string, key, SHA512_HASH_ALGORITHM);
	}

	/**
	 * Calculates MD5 hash of the String.
	 * @param string String which hash should be calculated.
	 * @return MD5 hash.
	 */
	public static String md5(String string) {
		String hashText = null;
		if(string != null) try {
			MessageDigest messageDigest;
			messageDigest = MessageDigest.getInstance("MD5");
			messageDigest.reset();
			messageDigest.update(string.getBytes());
			byte[] digest = messageDigest.digest();
			BigInteger bigInt = new BigInteger(1, digest);
			hashText = bigInt.toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hashText;
	}

	/**
	 * Calculates hash of the String for specified hash algorithm.
	 * @param string String which hash should be calculated.
	 * @param key Secret Key.
	 * @param hashAlgorithm Hash algorithm.
	 * @return SHA-512 hash.
	 * @see #SHA512_HASH_ALGORITHM
	 * @see #SHA256_HASH_ALGORITHM
	 */
	private static String sha(String string, String key, String hashAlgorithm) {
		try {
			Key sk = new SecretKeySpec(key.getBytes(), hashAlgorithm);
			Mac mac = Mac.getInstance(sk.getAlgorithm());
			mac.init(sk);
			final byte[] hmac = mac.doFinal(string.getBytes("UTF-8"));

			return toHexString(hmac);
		} catch (NoSuchAlgorithmException | InvalidKeyException | IllegalStateException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static String findValues(JSONObject json) {
		ArrayList<String> data = new ArrayList<>();

        ArrayList<String> keys = new ArrayList<>(json.length());
        @SuppressWarnings("unchecked")
		Iterator<String> keysIterator = json.keys();
        while(keysIterator.hasNext())
        	keys.add(keysIterator.next());
        Collections.sort(keys);
        
        for (String key : keys) {
			if("hash".equals(key) || key.startsWith("rp-api-"))
				continue;

			Object token = json.opt(key);

            if(token == null)
                data.add(key + "=null");
            else if(token instanceof JSONObject)
				data.add(findValues((JSONObject) token));
			else if(token instanceof JSONArray) {
				JSONArray array = (JSONArray) token;
				if(array.length() > 0) {
					if(array.opt(0) instanceof JSONObject) {
						for(int i = 0; i < array.length(); i++)
							data.add(findValues(array.optJSONObject(i)));
					}
					else {
						StringBuilder builder = new StringBuilder();
						for(int i = 0; i < array.length(); i++) {
							String value = array.optString(i);
							if(builder.length() > 0)
								builder.append(",");
							builder.append(value);
						}
                        builder.insert(0, "=");
                        builder.insert(0, key);
						data.add(builder.toString());
					}
				}
				else
					data.add(String.format("%s=", key));
			} else if (token instanceof Float) {
				float value = (Float) token;
				data.add(key + "=" + String.format(Locale.ENGLISH, "%.1f", roundHashValue(value)));
			}
			else if (token instanceof Double) {
				double value = (Double) token;
				data.add(key + "=" + String.format(Locale.ENGLISH, "%.1f", roundHashValue(value)));
			}
			else if(token instanceof Boolean)
				data.add(String.format("%s=%b", key, token));
			else data.add(String.format("%s=%s", key, token.toString()));
		}
        
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < data.size(); i++) {
            if(i > 0)
                builder.append("*");
            builder.append(data.get(i));
        }
        return builder.toString();
    }

	public static String getCertificateSHA1Fingerprint(Context context) {
		PackageManager pm = context.getPackageManager();
		String packageName = context.getPackageName();
		int flags = PackageManager.GET_SIGNATURES;
		PackageInfo packageInfo = null;
		try {
			packageInfo = pm.getPackageInfo(packageName, flags);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		Signature[] signatures = packageInfo.signatures;
		byte[] cert = signatures[0].toByteArray();
		InputStream input = new ByteArrayInputStream(cert);
		CertificateFactory cf = null;
		try {
			cf = CertificateFactory.getInstance("X509");
		} catch (CertificateException e) {
			e.printStackTrace();
		}
		X509Certificate c = null;
		try {
			c = (X509Certificate) cf.generateCertificate(input);
		} catch (CertificateException e) {
			e.printStackTrace();
		}
		String hexString = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA1");
			byte[] publicKey = md.digest(c.getEncoded());
			hexString = byte2HexFormatted(publicKey);
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		} catch (CertificateEncodingException e) {
			e.printStackTrace();
		}
		return hexString;
	}

	private static String toHexString(byte[] bytes) {
		StringBuilder sb = new StringBuilder(bytes.length * 2);

		Formatter formatter = new Formatter(sb);
		for (byte b : bytes) {
			formatter.format("%02x", b);
		}
		formatter.close();

		return sb.toString();
	}

	private static String byte2HexFormatted(byte[] arr) {
		StringBuilder str = new StringBuilder(arr.length * 2);
		for (int i = 0; i < arr.length; i++) {
			String h = Integer.toHexString(arr[i]);
			int l = h.length();
			if (l == 1) h = "0" + h;
			if (l > 2) h = h.substring(l - 2, l);
			str.append(h.toUpperCase());
			if (i < (arr.length - 1)) str.append(':');
		}
		return str.toString();
	}

	private static float roundHashValue(float value) {
		float roundValue = Math.round(value * 10f);
		return roundValue / 10f;
	}

	private static double roundHashValue(double value) {
		double roundValue = Math.round(value * 10d);
		return roundValue / 10d;
	}
}
