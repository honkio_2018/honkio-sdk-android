package com.riskpointer.sdk.api.model.entity;

import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.AbsFilter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by I.N. on 2018-02-13.
 */
public class CalendarEvent extends Event implements Serializable {
    public class Repeating {
        public static final String ONCE = "once";
        public static final String DAILY = "daily";
        public static final String WEEKLY = "weekly";
        public static final String MONTHLY = "monthly";
    }

    private String mId;
    private long mInitialStart;
    private long mInitialEnd;
    private String mRepeating;
    private ArrayList<Product> mProducts;

    public CalendarEvent() {
    }

    public CalendarEvent(CalendarEvent event) {
        super(event);
        this.mId = event.mId;
        this.mInitialStart = event.mInitialStart;
        this.mInitialEnd = event.mInitialEnd;
        this.mRepeating = event.getRepeating();
        if (event.getProducts() != null) {
            mProducts = new ArrayList<>();
            for (Product product : event.getProducts()) {
                mProducts.add(new Product(product));
            }
        }
    }

    public void applyTo(Message<?> message) {
        message.addParameters(toParams());
    }

    public HashMap<String, Object> toParams() {
        HashMap<String, Object> hashMap = new HashMap<>();

        AbsFilter.applyIfNotNull(hashMap, "id", getId());
        AbsFilter.applyIfNotNull(hashMap, "type", getType());
        AbsFilter.applyIfNotNull(hashMap, "start", ApiFormatUtils.dateToServerFormat(getInitialStart()));
        AbsFilter.applyIfNotNull(hashMap, "end", ApiFormatUtils.dateToServerFormat(getInitialEnd()));
        AbsFilter.applyIfNotNull(hashMap, "repeat", getRepeating());
        if (getProducts() != null) {
            List<String> products_ids = new ArrayList<>(mProducts.size());
            for (Product product : mProducts) {
                products_ids.add(product.getId());
            }
            AbsFilter.applyIfNotNull(hashMap, "products", products_ids);
        }
        return hashMap;
    }

    public ArrayList<Product> getProducts() {
        return mProducts;
    }

    public void setProducts(ArrayList<Product> products) {
        mProducts = products;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public long getInitialStart() {
        return mInitialStart;
    }

    public void setInitialStart(long date) {
        this.mInitialStart = date;
    }

    public long getInitialEnd() {
        return mInitialEnd;
    }

    public void setInitialEnd(long date) {
        this.mInitialEnd = date;
    }

    public String getRepeating() {
        return mRepeating;
    }

    public void setRepeating(String repeating) {
        this.mRepeating = repeating;
    }

    @Override
    public String toString() {
        return "Event: start=" + ApiFormatUtils.dateToServerFormat(getStart())
                + ", end=" + ApiFormatUtils.dateToServerFormat(getEnd())
                + ", initial start=" + ApiFormatUtils.dateToServerFormat(getInitialStart())
                + ", initial end=" + ApiFormatUtils.dateToServerFormat(getInitialEnd())
                + ", repeat " + getRepeating()
                + ", type=" + getType();
    }
}
