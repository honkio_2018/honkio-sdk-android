/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.entity;

import com.riskpointer.sdk.api.web.response.Response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Data object that holds information about transaction on the server.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
public class Transaction implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final class Type {

		private Type() {}

		public static final String BONUS = "bonus";
		public static final String PAYMENT = "payment";
		public static final String ORDER = "order";

	}

	private String mType;
	private String mStatus;
	private String mId;
	private String mShopId;
	private String mShopName;
	private String mShopReceipt;
	private String mShopReference;
    private String mMerchantName;
	private UserAccount mAccount;
	private double mAmount;
	private String mCurrency;
	private long mTimeStamp;
	private ArrayList<BoughtProduct> mProductsList;
    private List<TransactionMessage> mMessagesList;
    private HashMap<String, Field<?>> mAccountExtra;
	private String mOrderId;

	//TODO write docs
	public void setType(String mType) {
		this.mType = mType;
	}

	//TODO write docs
	public String getType() {
		return mType;
	}

	//TODO write docs
	public boolean typeIs(String... types) {
		if (mType == null)
			return false;
		for (String type: types)
			if (mType.equals(type))
				return true;
		return false;
	}

	/**
	 * Gets Status of the transaction.
	 * @return Status of the transaction.
	 * @see Response.Status
	 */
	public String getStatus() {
		return mStatus;
	}

	/**
	 * Sets Status of the transaction.
	 * @param status Status of the transaction.
	 * @see Response.Status
	 */
	public void setStatus(String status) {
		this.mStatus = status;
	}

	/**
	 * Check if Status of the Transaction equal {@link Response.Status#ACCEPT}.
	 * @return True if Status of the Transaction equal {@link Response.Status#ACCEPT}, false otherwise.
	 */
	public boolean isStatusAccept() {
		return Response.Status.ACCEPT.equals(mStatus);
	}

	/**
	 * Check if Status of the Transaction equal {@link Response.Status#ERROR}.
	 * @return True if Status of the Transaction equal {@link Response.Status#ERROR}, false otherwise.
	 */
	public boolean isStatusError() {
		return Response.Status.ERROR.equals(mStatus);
	}

	/**
	 * Check if Status of the Transaction equal {@link Response.Status#PENDING}.
	 * @return True if Status of the Transaction equal {@link Response.Status#PENDING}, false otherwise.
	 */
	public boolean isStatusPending() {
		return Response.Status.PENDING.equals(mStatus);
	}

	/**
	 * Check if Status of the Transaction equal {@link Response.Status#REJECT}.
	 * @return True if Status of the Transaction equal {@link Response.Status#REJECT}, false otherwise.
	 */
	public boolean isStatusReject() {
		return Response.Status.REJECT.equals(mStatus);
	}

	public String getId() {
		return mId;
	}

	public void setId(String id) {
		this.mId = id;
	}

	/**
	 * Gets Shop ID that is contain this transaction.
	 * @return Shop ID that is contain this transaction.
	 */
	public String getShopId() {
		return mShopId;
	}

	/**
	 * Sets Shop ID that is contain this transaction.
	 * @param id Shop ID that is contain this transaction.
	 */
	public void setShopId(String id) {
		this.mShopId = id;
	}

	/**
	 * Gets Shop Name that is contain this transaction.
	 * @return Shop Name that is contain this transaction.
	 */
	public String getShopName() {
		return mShopName;
	}

	/**
	 * Sets Shop Name that is contain this transaction.
	 * @param name Shop Name that is contain this transaction.
	 */
	public void setShopName(String name) {
		this.mShopName = name;
	}

	/**
	 * Gets Shop receipt for the Transaction.
	 * @return Shop receipt for the Transaction.
	 */
	public String getShopReceipt() {
		return mShopReceipt;
	}

	/**
	 * Sets Shop receipt for the Transaction.
	 * @param receipt Shop receipt for the Transaction.
	 */
	public void setShopReceipt(String receipt) {
		this.mShopReceipt = receipt;
	}

	/**
	 * Gets Reference string that was set in the payment request.
	 * @return Reference string that was set in the payment request.
	 */
	public String getShopReference() {
		return mShopReference;
	}

	/**
	 * Sets Reference string that was set in the payment request.
	 * @param reference Reference string that was set in the payment request.
	 */
	public void setShopReference(String reference) {
		this.mShopReference = reference;
	}

	/**
	 * Gets Payment Account that was used for the payment.
	 * @return Payment Account that was used for the payment.
	 */
	public UserAccount getAccount() {
		return mAccount;
	}

	/**
	 * Sets Payment Account that was used for the payment.
	 * @param account Payment Account that was used for the payment.
	 */
	public void setAccount(UserAccount account) {
		this.mAccount = account;
	}

	/**
	 * Gets Merchant name that is owner of the Shop which contain this Transaction.
	 * @return Merchant name that is owner of the Shop which contain this Transaction.
	 */
	public String getMerchantName() {
		return mMerchantName;
	}

	/**
	 * Sets Merchant name that is owner of the Shop which contain this Transaction.
	 * @param merchantName Merchant name that is owner of the Shop which contain this Transaction.
	 */
	public void setMerchantName(String merchantName) {
		this.mMerchantName = merchantName;
	}

	// TODO write docs
	public double getAmount() {
        if (mAmount == Double.NaN)
            return 0;
		return mAmount;
	}

	// TODO write docs
	public void setAmount(double mAmount) {
		this.mAmount = mAmount;
	}

	public String getCurrency() {
		return mCurrency;
	}

	public void setCurrency(String mCurrency) {
		this.mCurrency = mCurrency;
	}

	/**
	 * Gets time in millis whet transaction is executed.
	 * @return Time in millis whet transaction is executed.
	 */
	public long getTimeStamp() {
		return mTimeStamp;
	}

	/**
	 * Sets time in millis whet transaction is executed.
	 * @param timeMillis Time in millis whet transaction is executed.
	 */
	public void setTimeStamp(long timeMillis) {
		this.mTimeStamp = timeMillis;
	}

	/**
	 * Gets list of the products bought by this Transaction.
	 * @return List of the products bought by this Transaction.
	 */
	public ArrayList<BoughtProduct> getProductsList() {
		return mProductsList;
	}

	/**
	 * Sets list of the products bought by this Transaction.<br>
	 * Also this method calculate Transaction amount and currency depends on Products amount, count and currency.
	 * @param productsList List of the products bought by this Transaction.
	 */
	public void setProductsList(ArrayList<BoughtProduct> productsList) {
		this.mProductsList = productsList;
        if (Double.isNaN(mAmount) && productsList.size() > 0) {
            double amount = 0;
            String currency = productsList.get(0).getCurrency();
            if (currency != null)
                for (BoughtProduct product: productsList) {
                    if (currency.equals(product.getCurrency()))
                        amount += product.getAmount() * product.getCount();
                    else {
                        currency = null;
                        break;
                    }
                }
            if (currency != null) {
                mAmount = amount;
                mCurrency = currency;
            }
        }
	}

	/**
	 * Gets count of bought products.
	 * @return Count of bought products.
	 * @see #setProductsList(ArrayList)
	 */
	public int getProductsCount() {
		if(mProductsList != null)
			return mProductsList.size();
		return 0;
	}

	/**
	 * Get Bought Product by specified index.
	 * @param index Index of the product in the products list.
	 * @return Bought Product for specified index or null if index out of range.
	 */
	public BoughtProduct getProduct(int index) {
		if (mProductsList != null && mProductsList.size() > index)
			return mProductsList.get(index);
		return null;
	}

	/**
	 * Sets Messages list attached for the Transaction.
	 * @param messages Messages list attached for the Transaction.
	 */
    public void setMessagesList(List<TransactionMessage> messages) {
        mMessagesList = messages;
    }

	/**
	 * Gets Messages list attached for the Transaction.
	 * @return Messages list attached for the Transaction.
	 */
    public List<TransactionMessage> getMessagesList() {
        return mMessagesList;
    }

	public HashMap<String, Field<?>> getAccountExtra() {
		return mAccountExtra;
	}

	public void setAccountExtra(HashMap<String, Field<?>> mAccountExtra) {
		this.mAccountExtra = mAccountExtra;
	}

	public String getOrderId() {
		return mOrderId;
	}

	public void setOrderId(String orderId) {
		mOrderId = orderId;
	}

	/**
	 * Data object that holds information about message attached to transaction.
	 */
    public static class TransactionMessage implements Serializable {
		/** Sender of the message. */
        public final String from;
		/** Text of the message. */
        public final String text;
		/** Time in millis when message was sent. */
        public final long timeStamp;

		/**
		 * Construct Message with specified params.
		 * @param from Sender of the message.
		 * @param text Text of the message.
		 * @param timeStamp Time in millis when message was sent.
		 */
        public TransactionMessage(String from, String text, long timeStamp) {
            this.from = from;
            this.text = text;
            this.timeStamp = timeStamp;
        }

		/**
		 * Check if text or from is empty.
		 * @return True if text or from is empty, false otherwise.
		 */
        public boolean isEmpty() {
            return (text == null || text.isEmpty()) || (from == null || from.isEmpty());
        }
    }
}
