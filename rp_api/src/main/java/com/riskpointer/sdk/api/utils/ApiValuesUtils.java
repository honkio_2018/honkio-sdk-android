/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.utils;

import com.riskpointer.sdk.api.model.entity.BookedProduct;
import com.riskpointer.sdk.api.model.entity.UserAccount;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;

/**
 * Utils class to validate or calculate some API values.
 */
public final class ApiValuesUtils {
	
	private static final String SSN_CHECK_CHARS = "0123456789ABCDEFHJKLMNPRSTUVWXY";

	private static double NOISE = 0.000000000001;

	private ApiValuesUtils() {}

	/**
	 * Check if SSN is valid.
	 * @param ssn Social Security number
	 * @return True if SSN is valid, false otherwise.
	 */
	public static boolean checkSsn(String ssn) {
		return ApiValuesUtils.checkFinlandSsn(ssn) || ApiValuesUtils.checkItalianSsn(ssn) || ApiValuesUtils.checkUKSsn(ssn) || ApiValuesUtils.checkSwedenSsn(ssn);
	}



	/**
	 * Check if SSN is valid.
	 * @param ssn Social Security number
	 * @param countryIso Country A3 ISO code
	 * @return True if SSN is valid, false otherwise.
	 */
	public static boolean checkSsn(String ssn, String countryIso) {
		if (countryIso == null || ssn == null)
			return false;

		switch (countryIso) {
			case "FIN":
				return checkFinlandSsn(ssn);
			case "ITA":
				return checkItalianSsn(ssn);
			case "GBR":
				return checkUKSsn(ssn);
			case "SWE":
				return checkSwedenSsn(ssn);
		}
		return false;
	}

	/**
	 * Gets birthday by SSN.
	 * @param ssn Social Security number
	 * @param countryIso Country A3 ISO code
	 * @return Birthday date in milliseconds or Zero if unable to find date.
	 */
	public static long getBirthdayFromSsn(String ssn, String countryIso) {
		if (countryIso == null || ssn == null)
			return 0;

		switch (countryIso) {
			case "FIN":
				return getBirthdayFromFinlandSsn(ssn);
		}
		return 0;
	}

	/**
	 * Check if Finland SSN is valid.
	 * @param ssn Social Security number
	 * @return True if SSN is valid, false otherwise.
	 */
	public static boolean checkFinlandSsn(String ssn) {
		if(ssn == null)
			return false;

		ssn = ssn.trim().toUpperCase();

		if(ssn.length() != 11)
			return false;

		StringBuilder buffer = new StringBuilder(ssn);
		char controlChar = buffer.charAt(10);
		buffer.deleteCharAt(10);
		buffer.deleteCharAt(6);
		int index;
		try {
			index = Integer.parseInt(buffer.toString());
		} catch (NumberFormatException e) {
			return false;
		}
		index = index % SSN_CHECK_CHARS.length();
		char calculatedControlChar = SSN_CHECK_CHARS.charAt(index);
		return controlChar == calculatedControlChar;
	}

	/**
	 * Gets birthday by Finland SSN.
	 * @param ssn Social Security number
	 * @return Birthday date in milliseconds or Zero if unable to find date.
	 */
	public static long getBirthdayFromFinlandSsn(String ssn) {
		if(ssn == null)
			return 0;

		ssn = ssn.trim().toUpperCase();

		if(ssn.length() != 11)
			return 0;

		String strDay = ssn.substring(0, 2);
		String strMonth = ssn.substring(2, 4);
		String strYear = ssn.substring(4, 6);
		char charCentury = ssn.charAt(6);

		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		calendar.setTimeInMillis(0);

		switch (charCentury) {
			case '+':
				calendar.set(Calendar.YEAR, 1800);
				break;
			case '-':
				calendar.set(Calendar.YEAR, 1900);
				break;
			case 'A':
				calendar.set(Calendar.YEAR, 2000);
				break;
			default:
				return 0;
		}

		try {
			calendar.add(Calendar.YEAR, Integer.valueOf(strYear));
			calendar.set(Calendar.MONTH, Integer.valueOf(strMonth) - 1);
			calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(strDay));
		}
		catch (NumberFormatException e) {
			e.printStackTrace();
			return 0;
		}

		return calendar.getTimeInMillis();
	}

	/**
	 * Checks if Italian fiscal code card for individuals (16 alphanumeric characters)
	 * (italian analog SSN) is valid
	 * @param ssn string represents Codice Fiscale
	 * @return True if Codice Fiscale is valid, false otherwise.
     */
	public static boolean checkItalianSsn(String ssn){
		if (ssn==null) return false;
		String code = ssn.trim().replaceAll("\\s+","").toUpperCase();
		if(code.length() != 16) return false;

		String[] chars = new String[]{"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
		String[] remainder = new String[]{"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
		int[] odd = new int[] {1,0,5,7,9,13,15,17,19,21,1,0,5,7,9,13,15,17,19,21,2,4,18,20,11,3,6,8,12,14,16,10,22,25,24,23};
		int[] even = new int[] {0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25};
		Pattern pattern = compile("[a-z]{6}\\d{2}[a-z]\\d{2}[a-z]\\d{3}[a-z]", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(code);
		if (!matcher.matches()) {
			return false;
		} else {
			String[] c = code.split("");
			List<String> charList = Arrays.asList(chars);
			int oddSum = 0;
			int evenSum = 0;
			int index;
			//odd chars in code
			for (int i = 1; i < c.length; i += 2) {
				index = charList.indexOf(c[i]);
				if (index==-1)return false;
				oddSum = oddSum + odd[index];
			}
			//even chars in code except 16
			for (int i = 2; i < c.length-1; i += 2) {
				index = charList.indexOf(c[i]);
				if (index==-1)return false;
				evenSum = evenSum + even[index];
			}
			int checkDigit = (oddSum + evenSum) % 26;
			String checkChar = remainder[checkDigit];
			if (code.endsWith(checkChar)) {
				return true;
			} else {
				return false;
			}
		}
	}

	/**
	 * Check if UK SSN (NINO) is valid.
	 * @param ssn Social Security number
	 * @return True if SSN is valid, false otherwise.
	 */
	public static boolean checkUKSsn(String ssn) {
		if(ssn == null)
			return false;
		ssn = ssn.trim().replaceAll("\\s+","").toUpperCase();
		if(ssn.length() != 9)
			return false;
		Pattern pattern1 = compile("^[A-CEGHJ-NOPR-TW-Z]{1}[A-CEGHJ-NPR-TW-Z]{1}[0-9]{6}[A-D\\s]{1}", Pattern.CASE_INSENSITIVE);
		Pattern pattern2 = compile("(^GB)|(^BG)|(^NK)|(^KN)|(^TN)|(^NT)|(^ZZ)", Pattern.CASE_INSENSITIVE);
		Matcher matcher1 = (pattern1.matcher(ssn));
		Matcher matcher2 = (pattern2.matcher(ssn));
		if (matcher1.matches() && !matcher2.matches()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check if Sweden Personal Identity Number (personnummer) is valid.
	 * @param ssn Social Security number
	 * @return True if SSN is valid, false otherwise.
	 */
	public static boolean checkSwedenSsn(String ssn) {
		if(ssn == null || ssn.length() < 10)
			return false;
		ssn = ssn.trim().replace("-", "").replace("+", "");

		String century = "";
		// Remove century and check number
		if (ssn.length() == 12) {
			century = ssn.substring(0, 2);
			ssn = ssn.substring(2, 12);
		} else if (ssn.length() == 10) {
			ssn = ssn.substring(0, 10);
		} else {
			return false;
		}
		// Remove check number
		Integer check = tryParse(ssn.substring(9, 10));
		boolean isValid = false;
		if (check != null) {
			String sValue = ssn.substring(0, 9);
			int result = 0;
			// Calculate check number
			for (int i = 0, len = sValue.length(); i < len; i++) {
				int tmp = Integer.parseInt(sValue.substring(i, i + 1));
				if ((i % 2) == 0) {
					tmp = (tmp * 2);
				}
				if (tmp > 9) {
					result += (1 + (tmp % 10));
				} else {
					result += tmp;
				}
			}
			isValid = (((check + result) % 10) == 0);
		}
		int month = Integer.parseInt(ssn.substring(2, 4), 10);
		int day = Integer.parseInt(ssn.substring(4, 6), 10);
		boolean isSSN = month < 13 && month > 0 && day < 32 && day > 0;
		return isValid && isSSN;
	}

	private static Integer tryParse(String text) {
		try {
			return Integer.parseInt(text);
		} catch (NumberFormatException e) {
			return null;
		}
	}
	public static boolean checkIban(String ibanString) {
		if (ibanString == null || ibanString.length() == 0)
			return false;

		// Check invalid characters
		if (Pattern.compile("!([0-9]|[A-Z]|' ')").matcher(ibanString).find())
			return false;

		// Remove spaces
		Matcher matcher = Pattern.compile(" ").matcher(ibanString);
		if (matcher.find())
			ibanString = matcher.replaceAll("");

		if (ibanString.length() <= 5)
			return false;

		if (ibanString.charAt(2) < '0'
				|| ibanString.charAt(2) > '9'
				|| ibanString.charAt(3) < '0'
				|| ibanString.charAt(3) > '9')
			// Characters at index 2 and 3 not both numeric.
			return false;

		// Calculate and check check sum
		int checkDigit = Integer.parseInt(ibanString.substring(2, 4));

		StringBuilder builder = new StringBuilder(ibanString);
		builder.append(builder.subSequence(0, 4)).delete(0, 4);

		for (int i = 0; i < builder.length(); i++) {
			char c = builder.charAt(i);
			if (c >= 'A' && c <= 'Z')
				builder.delete(i, i+1).insert(i, Integer.toString (c - 'A' + 10));
		}

		builder.setCharAt(builder.length() - 1, '0');
		builder.setCharAt(builder.length() - 2, '0');

		while (true) {
			int min = Math.min(builder.length(), 9);
			int decNumber = Integer.parseInt(builder.substring(0, min));
			if (decNumber < 97 || builder.length() < 3)
				break;
			int del = decNumber % 97;

			builder.delete(0, min);
			builder.insert(0, Integer.toString(del));
		}

		return checkDigit == (98 - Integer.parseInt(builder.toString()));
	}

	/**
	 * Calculates sum of booked products for specified account.
	 * @param bookProducts List of booked products.
	 * @param account Payment account which will be used for a payment.
	 * @return Sum of booked products
	 */
	public static double calcSum(ArrayList<BookedProduct> bookProducts, UserAccount account) {
		double sum = 0;
		if(bookProducts != null)
			for (BookedProduct product : bookProducts)
				sum += product.getPrice(account.getType());
		return sum;
	}

	/**
	 * Puts additional Delays params into params map.
	 * @param params Params map. May be null.
	 * @param pin PIN code.
	 * @param delays Delays between PIN buttons clicks. May be null.
	 * @return Params map.
	 */
	public static HashMap<String, Object> putPinDelayParams(HashMap<String, Object> params, String pin, long[] delays) {
		if (delays == null)
			return params;

		int pinNumber = 0;
		try {
			pinNumber = Integer.parseInt(pin);
		} catch (NumberFormatException e) {
			return params;
		}

		if(params == null)
			params = new HashMap<>(2);
		
		StringBuilder stringBuilder = new StringBuilder();
		if(delays.length > 0) {
			stringBuilder.append(Long.toString(delays[0]));
			for(int i = 1; i < delays.length; i++) {
				stringBuilder.append(",");
				stringBuilder.append(Long.toString(delays[i]));
			}
		}
		params.put("str_pin_delays", stringBuilder.toString());
		params.put("pin_id", Integer.toHexString(pinNumber ^ 0xbc25));
		
		return params;
	}

	public static double putNoise(double value) {
		if (value == (double) Math.round(value)) {
			return value + NOISE;
		}
		return value;
	}
}
