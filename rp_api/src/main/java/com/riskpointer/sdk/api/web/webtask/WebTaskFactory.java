package com.riskpointer.sdk.api.web.webtask;

import android.content.Context;

import com.riskpointer.sdk.api.web.message.Message;

import java.io.File;
import java.io.InputStream;

import okhttp3.MediaType;


// TODO Write docs
/**
 * Created by Shurygin Denis on 2016-11-01.
 */
public class WebTaskFactory {

    private static final Object LOCK = new Object();
    private WebTaskFactory() {}

    public interface Instance {
        <T> WebTask<T> buildTask(Context context, Message<T> message);
        <T> WebTask<T> buildTask(Context context, Message<T> message, File file);
        <T> WebTask<T> buildTask(Context context, Message<T> message, InputStream inputStream, MediaType mediaType);
    }

    public static class DefaultInstance implements Instance {

        @Override
        public <T> WebTask<T> buildTask(Context context, Message<T> message) {
            return new SimpleWebTask<>(context, message);
        }

        @Override
        public <T> WebTask<T> buildTask(Context context, Message<T> message, File file) {
            return new MultiPartWebTask<>(context, message, file);
        }

        @Override
        public <T> WebTask<T> buildTask(Context context, Message<T> message, InputStream inputStream, MediaType mediaType) {
            return new MultiPartWebTask<>(context, message, inputStream, mediaType);
        }
    }

    private static Instance sInstance;

    public static void setInstance(Instance instance) {
        synchronized (LOCK) {
            sInstance = instance;
        }
    }

    public static Instance getInstance() {
        if (sInstance == null) {
            synchronized (LOCK) {
                if (sInstance == null)
                    sInstance = new DefaultInstance();
            }
        }
        return sInstance;
    }
}
