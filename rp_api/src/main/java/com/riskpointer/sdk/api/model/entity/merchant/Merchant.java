package com.riskpointer.sdk.api.model.entity.merchant;

import com.riskpointer.sdk.api.model.entity.MerchantSimple;
import com.riskpointer.sdk.api.web.message.filter.AbsFilter;

import java.util.HashMap;
/**
 * Data object that hold full information about merchant.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 *
 * @author Shurygin Denis on 2015-01-16.
 */
public class Merchant extends MerchantSimple {

    private String mBankIban;
    private String mBankName;
    private String mBankSwift;

    private double mVat;
    private String mVatnr;

    private String mEmail;
    private String mPhone;

    private String mParentMerchant;

    private Boolean isActive;

    public HashMap<String, Object> toParams() {
        HashMap<String, Object> hashMap = super.toParams();

        AbsFilter.applyIfNotNull(hashMap, "str_businessid", getBusinessId());
        AbsFilter.applyIfNotNull(hashMap, "str_sepa", getBankIban());
        AbsFilter.applyIfNotNull(hashMap, "str_bankname", getBankName());
        AbsFilter.applyIfNotNull(hashMap, "str_swift", getBankSwift());
        AbsFilter.applyIfNotNull(hashMap, "str_vatnumber", getVatnr());
        AbsFilter.applyIfNotNull(hashMap, "int_vat", getVat());

        AbsFilter.applyIfNotNull(hashMap, "str_email", getEmail());
        AbsFilter.applyIfNotNull(hashMap, "str_telephone", getPhone());

        AbsFilter.applyIfNotNull(hashMap, "parent", getParentMerchant());

        AbsFilter.applyIfNotNull(hashMap, "active", isActive());

        return hashMap;
    }

    public String getBankIban() {
        return mBankIban;
    }

    public void setBankIban(String mBankIban) {
        this.mBankIban = mBankIban;
    }

    public String getBankName() {
        return mBankName;
    }

    public void setBankName(String mBankName) {
        this.mBankName = mBankName;
    }

    public String getBankSwift() {
        return mBankSwift;
    }

    public void setBankSwift(String mBankSwift) {
        this.mBankSwift = mBankSwift;
    }

    public double getVat() {
        return mVat;
    }

    public void setVat(double mVat) {
        this.mVat = mVat;
    }

    public String getVatnr() {
        return mVatnr;
    }

    public void setVatnr(String mVatnr) {
        this.mVatnr = mVatnr;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    public String getParentMerchant() {
        return mParentMerchant;
    }

    public void setParentMerchant(String merchantId) {
        this.mParentMerchant = merchantId;
    }

    public void setActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean isActive() {
        return isActive;
    }

}
