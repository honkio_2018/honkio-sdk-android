package com.riskpointer.sdk.api.model.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.BoughtProduct;
import com.riskpointer.sdk.api.model.entity.ShopInfo;
import com.riskpointer.sdk.api.model.entity.Transaction;
import com.riskpointer.sdk.api.utils.ApiCryptoUtils;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;

import java.security.InvalidParameterException;
import java.security.SignatureException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Provide easy work with stamps. All work with data base hidden in the methods of this class.<br>
 * @author Shurygin Denis on 2015-08-18.
 */
@Deprecated
public class StampStorage {

    private static final SimpleDateFormat sBarcodeDateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);
    private static final SimpleDateFormat sVerifyDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

    static {
        sBarcodeDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        sVerifyDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    private static StampStorage sInstance;

    /**
     * Gets static instance of StampStorage.
     * @param context Application context.
     * @return Static instance of StampStorage.
     */
    public static StampStorage getInstance(Context context) {
        if (sInstance == null)
            sInstance = new StampStorage(context);
        return sInstance;
    }

    private Context mContext;
    private ApiDataBaseHelper mDataBaseHelper;

    private StampStorage(Context context) {
        mContext = context.getApplicationContext();
        mDataBaseHelper = ApiDataBaseHelper.getInstance(context);
    }

    public Cursor getStampsCursor(String[] columns, String selection, String[] selectionArgs) {
        return mDataBaseHelper.getReadableDatabase().query(Table.STAMP, columns, selection, selectionArgs, null, null, null);
    }

    public long putStamp(String ticketDataString) throws ParseException, SignatureException, InvalidParameterException {
        ContentValues values = parseFrom(ticketDataString);
        if (values != null) {
            values.put(StampColumns.READING_TIME, System.currentTimeMillis());
            values.put(StampColumns.IS_SENT_TO_SERVER, false);
            values.put(StampColumns.SERVER_ERROR_CODE, RpError.Api.NONE);
            return mDataBaseHelper.getWritableDatabase().insert(Table.STAMP, null, values);
        }
        throw new ParseException("Failed to parse stamp string.", 0);
    }

    public void updateStampFromResponse(Response<Transaction> response, long stampId, String productName) {
        ContentValues values = new ContentValues();
        values.put(StampColumns.IS_VALID, false);
        values.put(StampColumns.IS_SENT_TO_SERVER, true);
        values.put(StampColumns.SERVER_ERROR_CODE, RpError.Api.NONE);
        if (response.getResult() != null) {
            Transaction transaction = response.getResult();
            if (transaction.getProductsList().size() > 0) {
                BoughtProduct foundedTicket = null;
                for (BoughtProduct product : transaction.getProductsList()) {
                    if (productName.equals(product.getName())) {
                        foundedTicket = product;
                    }
                }
                if (foundedTicket == null) {
                    foundedTicket = transaction.getProductsList().get(0);
                }
                values.put(StampColumns.IS_VALID, response.isStatusAccept() && transaction.isStatusAccept() && foundedTicket.isValid());
                values.put(StampColumns.VALID_FROM, HonkioApi.serverTimeMillis(foundedTicket.getValidFrom()));
                values.put(StampColumns.VALID_TO, HonkioApi.serverTimeMillis(foundedTicket.getValidTo()));
                values.put(StampColumns.NAME, foundedTicket.getName());
            }
        }
        mDataBaseHelper.getWritableDatabase().update(Table.STAMP, values, BaseColumns._ID + "=" + stampId, null);
    }

    public void updateStampFromError(RpError error, long stampId) {
        ContentValues values = new ContentValues();
        values.put(StampColumns.SERVER_ERROR_CODE, error.code);
        mDataBaseHelper.getWritableDatabase().update(Table.STAMP, values, BaseColumns._ID + "=" + stampId, null);
    }

    public Cursor getTicketByString(String ticketDataString, long delay) {
        return getStampsCursor(null,
                StampColumns.READING_TIME + " > " + (System.currentTimeMillis() - delay) + " AND " + StampColumns.ORIGINAL_STRING + "=?",
                new String[]{ticketDataString});
    }

    public Cursor getTicketById(long id) {
        return getStampsCursor(null, BaseColumns._ID + "=" + id, null);
    }

    private ContentValues parseFrom(String ticketDataString) throws SignatureException, InvalidParameterException{
        StringBuilder stringBuilder = new StringBuilder(ticketDataString);
        if (stringBuilder.subSequence(stringBuilder.length() - 4, stringBuilder.length()).equals(":iqt")) {
            stringBuilder.delete(stringBuilder.length() - 4, stringBuilder.length());
            String shopId = pullPart(stringBuilder, true);
            if (shopId == null || !shopId.equals(HonkioApi.getMainShopInfo().getShop().getId()))
                throw new InvalidParameterException("");
            String transactionId = pullPart(stringBuilder, true);
            String signature = pullPart(stringBuilder, false);
            Date validTo = toDate(pullPart(stringBuilder, false));
            Date validFrom = toDate(pullPart(stringBuilder, false));
            String name = stringBuilder.toString();
            if (shopId != null && transactionId != null && signature != null && validFrom != null && validTo != null && stringBuilder.length() > 0) {
                ShopInfo mainShopInfo = HonkioApi.getMainShopInfo();

                ContentValues values = new ContentValues();
                values.put(StampColumns.SHOP_ID, shopId);
                values.put(StampColumns.TRANSACTION_ID, transactionId);
                values.put(StampColumns.NAME, name);
                values.put(StampColumns.VALID_FROM, HonkioApi.serverTimeMillis(validFrom.getTime()));
                values.put(StampColumns.VALID_TO, HonkioApi.serverTimeMillis(validTo.getTime()));
                values.put(StampColumns.SIGNATURE, signature);
                values.put(StampColumns.ORIGINAL_STRING, ticketDataString);
                values.put(StampColumns.IS_VALID, true);

                stringBuilder = new StringBuilder();
                stringBuilder.append(HonkioApi.getMainShopInfo().getShop().getId()).append("\n")
                        .append(transactionId).append("\n")
                        .append(name).append("\n")
                        .append(sVerifyDateFormat.format(validFrom)).append("\n")
                        .append(Integer.toString((int) ((validTo.getTime() - validFrom.getTime()) / 1000))).append("\n");
                boolean isSignatureValid = ApiCryptoUtils.validate(values.getAsString(StampColumns.SIGNATURE), stringBuilder.toString());

                if (!isSignatureValid)
                    throw new SignatureException();

                if (HonkioApi.serverTimeMillis(validTo.getTime()) < System.currentTimeMillis())
                    values.put(StampColumns.IS_VALID, false);
                return values;
            }
        }
        return null;
    }

    private static String pullPart(StringBuilder stringBuilder, boolean first) {
        if (stringBuilder.length() > 0) {
            if (first) {
                int index = stringBuilder.indexOf(":");
                if (index > 0) {
                    String extractedString = stringBuilder.substring(0, index);
                    stringBuilder.delete(0, index + 1);
                    return extractedString;
                }
            }
            else {
                int index = stringBuilder.lastIndexOf(":");
                if (index > 0 && index < stringBuilder.length() - 1) {
                    String extractedString = stringBuilder.substring(index + 1, stringBuilder.length());
                    stringBuilder.delete(index, stringBuilder.length());
                    return extractedString;
                }
            }
        }
        return null;
    }

    private static Date toDate(String dateString) {
        try {
            return sBarcodeDateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date(0);
    }
}
