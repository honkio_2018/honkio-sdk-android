package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.CalendarEvent;
import com.riskpointer.sdk.api.model.entity.CalendarSchedule;
import com.riskpointer.sdk.api.model.entity.Product;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.ScheduleFilter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by I.N. on 2018-02-13.
 */
public class CalendarScheduleJsonParser extends BaseJsonResponseParser<CalendarSchedule> {

    @Override
    protected CalendarSchedule parseJson(Message<?> message, JSONObject json) {
        ArrayList<CalendarEvent> list = new ArrayList<>();
        JSONArray eventsJsonArray = json.optJSONArray("schedule");
        if (eventsJsonArray != null) {
            for (int i = 0; i < eventsJsonArray.length(); i++) {
                JSONObject eventJson = eventsJsonArray.optJSONObject(i);
                if (eventJson != null) {
                    list.add(CalendarEventJsonParser.parseCalendarEvent(eventJson));
                }
            }
        }

        CalendarSchedule schedule = new CalendarSchedule();
        schedule.setList(list);

        Object nullParameter = message.getParameter(null);
        if(nullParameter instanceof ScheduleFilter)
            schedule.setFilter((ScheduleFilter) nullParameter);

        return schedule;
    }
}
