package com.riskpointer.sdk.api.model.entity;

public class Earnings {
    private double mAmount;
    private long mPeriodDateStart;
    private long mPeriodDateEnd;
    private long mVat;
    private double mVatAmount;
    private String mCurrency;

    public double getAmount() {
        return mAmount;
    }

    public void setAmount(double amount) {
        mAmount = amount;
    }

    public long getPeriodDateStart() {
        return mPeriodDateStart;
    }

    public void setPeriodDateStart(long periodDateStart) {
        mPeriodDateStart = periodDateStart;
    }

    public long getPeriodDateEnd() {
        return mPeriodDateEnd;
    }

    public void setPeriodDateEnd(long periodDateEnd) {
        mPeriodDateEnd = periodDateEnd;
    }

    public long getVat() {
        return mVat;
    }

    public void setVat(long vat) {
        mVat = vat;
    }

    public double getVatAmount() {
        return mVatAmount;
    }

    public void setVatAmount(double vatAmount) {
        mVatAmount = vatAmount;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String currency) {
        mCurrency = currency;
    }
}
