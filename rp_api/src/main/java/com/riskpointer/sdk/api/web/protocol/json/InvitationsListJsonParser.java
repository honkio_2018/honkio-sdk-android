package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Invitation;
import com.riskpointer.sdk.api.model.entity.InvitationsList;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Parse media file list from JSON.
 */
public class InvitationsListJsonParser extends BaseJsonResponseParser<InvitationsList> {

    @Override
    public InvitationsList parseJson(Message<?> message, JSONObject json) {
        InvitationsList invitationsList = new InvitationsList();
        ArrayList<Invitation> list = new ArrayList<>();

        JSONArray invitesListJson = json.optJSONArray("invites");
        if (invitesListJson != null) {
            for (int i = 0; i < invitesListJson.length(); i++) {
                JSONObject invitationJson = invitesListJson.optJSONObject(i);
                if (invitationJson != null) {
                    Invitation invitation = InvitationJsonParser.parseInvitation(invitationJson);
                    if (invitation != null)
                        list.add(invitation);
                }
            }
        }
        invitationsList.setList(list);
        return invitationsList;
    }

}
