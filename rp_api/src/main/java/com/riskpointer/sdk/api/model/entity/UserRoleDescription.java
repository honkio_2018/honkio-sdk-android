package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;

/**
 * @author Shurygin Denis
 */

public class UserRoleDescription implements Serializable {

    public static final class Type {

        private Type() {}

        public static final String PUBLIC = "public";
        public static final String PRIVATE = "private";

    }

    private String mId;
    private String mType;
    private String mName;
    private String mDisplayName;
    private String mDescription;
    private int mTotalUsers;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getDisplayName() {
        return mDisplayName;
    }

    public void setDisplayName(String displayName) {
        this.mDisplayName = displayName;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public int getTotalUsers() {
        return mTotalUsers;
    }

    public void setTotalUsers(int totalUsers) {
        this.mTotalUsers = totalUsers;
    }
}
