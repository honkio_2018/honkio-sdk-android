package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;

/**
 * @author Shurygin Denis
 */
//TODO write docs
public class Field<T> implements Serializable {

    public static final class Type {

        private Type() {}

        public static final String STRING = "str";
        public static final String BOOLEAN = "bool";

    }

    private String mLabel;
    private T mValue;
    private String mValueLabel;

    private String mType;
    private boolean isVisible = true;

    public String getLabel() {
        return mLabel;
    }

    public void setLabel(String label) {
        this.mLabel = label;
    }

    public T getValue() {
        return mValue;
    }

    public void setValue(T value) {
        this.mValue = value;
    }

    public String getValueLabel() {
        return mValueLabel;
    }

    public void setValueLabel(String valueLabel) {
        this.mValueLabel = valueLabel;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }
}
