package com.riskpointer.sdk.api.web.message.filter;

import java.util.HashMap;

/**
 * @author Shurygin Denis
 */

public class ApplicantFilter extends ListFilter {

    private String mOrderId;
    private String mQueryRole;

    public String getOrderId() {
        return mOrderId;
    }

    public String getQueryRole() {
        return mQueryRole;
    }

    public void setOrderId(String orderId) {
        this.mOrderId = orderId;
    }

    public void setQueryRole(String queryRole) {
        this.mQueryRole = queryRole;
    }

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = super.toParams();
        AbsFilter.applyIfNotNull(params, "order_id", mOrderId);
        AbsFilter.applyIfNotNull(params, "query_role", mQueryRole);
        return params;
    }

}
