package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by asenko on 12/15/2015.
 * TODO write docs
 */
public class ApplicantsList implements Serializable {

    private ArrayList<Applicant> mList;
    private int mTotalCount;

    public ArrayList<Applicant> getList() {
        return mList;
    }

    public void setList(ArrayList<Applicant> list) {
        this.mList = list;
    }

    // TODO write docs
    public int getTotalCount() {
        return mTotalCount;
    }

    // TODO write docs
    public void setTotalCount(int total) {
        this.mTotalCount = total;
    }
}
