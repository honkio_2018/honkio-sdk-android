/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.utils;

import android.util.Base64;
import android.util.Log;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.exception.WrongKeyException;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * An utils class to encrypt/decrypt strings or byte arrays with specified key.
 */
public final class ApiCryptoUtils {

	private static final String CRYPT_TRANSFORMATION = "AES/ECB/PKCS5Padding";
	private static final String CRYPT_ALGORITHM = "AES";
	private static final String CRYPT_KEY_ALGORITHM = "PBKDF2WithHmacSHA1";
	private static final byte[] CRYPT_KEY_SALT = "c034c431c9a4d5415284af783398af297fb175ccf5ec20be9430bbe18da6dc4cb50bb5592eeb7d9b".getBytes();
	private static final int CRYPT_KEY_ITERATIONS = 1000;
	private static Cipher CIPHER;

	static String PUB_KEY_HEADER = "-----BEGIN PUBLIC KEY-----\n";
	static String PUB_KEY_FOOTER = "-----END PUBLIC KEY-----\n";
	static String VALIDATION_ALGORITHM = "DSA";

	static {
		try {
			CIPHER = Cipher.getInstance(CRYPT_TRANSFORMATION);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private ApiCryptoUtils() { }

	/**
	 * Encrypt String into Base64 encoded String.
	 * @param string String to encrypt.
	 * @param key Secret key.
	 * @return Base64 encoded String.
	 */
	public static String encrypt(String string, String key) {
		byte[] byteArray = null;
		try {
			byteArray = cryptByteArray(string.getBytes(), key, Cipher.ENCRYPT_MODE);
		} catch (WrongKeyException e) {
			// When string encrypt WrongPinException shall not throw.
			e.printStackTrace();
			return null;
		}
		return new String(Base64.encode(byteArray, 0));
	}

	/**
	 * Decrypt Base64 encoded String.
	 * @param string Base64 encoded String to decrypt.
	 * @param key Secret key.
	 * @return Decrypted String.
	 * @throws WrongKeyException if key isn't correct.
	 */
	public static String decrypt(String string, String key) throws WrongKeyException {
		byte[] inputByteArray = Base64.decode(string.getBytes(), 0);
		byte[] byteArray = cryptByteArray(inputByteArray, key, Cipher.DECRYPT_MODE);
		if(byteArray != null)
			return new String(byteArray);
		return "";
	}

	/**
	 * Encrypt/decrypt byte array.
	 * @param byteArray Byte array to encrypt/decrypt.
	 * @param key Secret key.
	 * @param cryptMode {@link Cipher#ENCRYPT_MODE} if array should be encrypted or {@link Cipher#DECRYPT_MODE} if array should be decrypted.
	 * @return Encrypted/decrypted byte array.
	 * @throws WrongKeyException if key isn't correct. Throws only for crypt mode {@link Cipher#DECRYPT_MODE}
	 */
	public static byte[] cryptByteArray(byte[] byteArray, String key, int cryptMode) throws WrongKeyException {
		byte[] result = null;
		SecretKeyFactory factory;
		try {
			factory = SecretKeyFactory
					.getInstance(CRYPT_KEY_ALGORITHM);
			SecretKey tmp = factory.generateSecret(new PBEKeySpec(key
					.toCharArray(), CRYPT_KEY_SALT, CRYPT_KEY_ITERATIONS, 128));
			SecretKeySpec keySpec = new SecretKeySpec(tmp.getEncoded(), CRYPT_ALGORITHM);
			CIPHER.init(cryptMode, keySpec);
			result = CIPHER.doFinal(byteArray);
		} catch (Exception e) {
			throw new WrongKeyException(e.getMessage());
		}
		return result;
	}

	/** DOESN'T WORK !!!!! */
	@Deprecated
	public static boolean validate(String string, String signature) {
		HonkioApi.getMainShopInfo().getShop().getPublicKey();
		return true;
	}

	/** DOESN'T WORK !!!!! */
	@Deprecated
	public static boolean validateBySystem(String publicKey, String signature, String text) {
		int pos1 = publicKey.indexOf(PUB_KEY_HEADER);
		int pos2 = publicKey.indexOf(PUB_KEY_FOOTER, pos1 + 1);
		String keyStr = publicKey.substring(pos1 + PUB_KEY_HEADER.length(), pos2).replace("\n", "");

		byte[] decodedKey= android.util.Base64.decode(keyStr, android.util.Base64.DEFAULT);
		byte[] decodedSign= android.util.Base64.decode(signature, android.util.Base64.DEFAULT);

		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			digest.update(text.getBytes("UTF-8"));
			byte[] data = digest.digest();

			EncodedKeySpec bobPubKeySpec = new X509EncodedKeySpec(decodedKey);
			KeyFactory keyFactory = KeyFactory.getInstance(VALIDATION_ALGORITHM);
			PublicKey bobPubKey = keyFactory.generatePublic(bobPubKeySpec);

			Signature sig = Signature.getInstance("SHA1withDSA");
			sig.initVerify(bobPubKey);
			sig.update(data);
			boolean result = sig.verify(decodedSign);
			if (result)
				Log.d("VALIDATION", "Valid!!!!!");

			return result;
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException | SignatureException | InvalidKeyException | InvalidKeySpecException e) {
			e.printStackTrace();
		}

		return false;
	}
}
