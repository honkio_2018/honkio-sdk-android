package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Shurygin Denis on 2016-11-03.
 */

public class Inventory implements Serializable {

    private final List<InventoryProduct> mList = new ArrayList<>();

    public List<InventoryProduct> getList() {
        return mList;
    }

    public void add(InventoryProduct product) {
        mList.add(product);
    }

    public boolean remove(InventoryProduct product) {
        return mList.remove(product);
    }

    public InventoryProduct findProductById(String productId) {
        return findProductById(productId, true);
    }

    public InventoryProduct findProductById(String productId, boolean onlyValid) {
        if (productId == null)
            return null;

        InventoryProduct invalidProduct = null;

        for (InventoryProduct product: mList)
            if (productId.equals(product.getId())) {
                if (product.isValid())
                    return product;
                else if (!onlyValid)
                    invalidProduct = product;
            }

        return invalidProduct;
    }

    public InventoryProduct findGroupedProductById(String productId) {
        if (productId == null)
            return null;

        InventoryProduct groupedProduct = null;

        for (InventoryProduct product: mList)
            if (productId.equals(product.getId()) && product.isValid()) {
                if (groupedProduct == null)
                    groupedProduct = new InventoryProduct(product);
                else
                    groupedProduct.setCount(groupedProduct.getCount() + product.getCount());
            }

        return groupedProduct;
    }

    public InventoryProduct findProductForTarget(String targetId) {
        Map<String, InventoryProduct> map = new HashMap<>();
        for (InventoryProduct product: mList) {
            Product.Target target = product.findTarget(targetId);
            if (target != null) {
                InventoryProduct group = map.get(product.getId());
                if (group == null) {
                    group = new InventoryProduct(product);
                    map.put(group.getId(), group);
                } else
                    group.setCount(group.getCount() + product.getCount());
                if (target.count <= group.getCount())
                    return group;
            }

        }
        return null;
    }

    public List<InventoryProduct> groupByProduct() {
        Map<String, InventoryProduct> map = new HashMap<>();
        List<InventoryProduct> list = new ArrayList<>();
        for (InventoryProduct product: mList) {
            InventoryProduct group = map.get(product.getId());
            if (group == null) {
                group = new InventoryProduct(product);
                map.put(group.getId(), group);
                list.add(group);
            }
            else
                group.setCount(group.getCount() + product.getCount());
        }
        return list;
    }

}
