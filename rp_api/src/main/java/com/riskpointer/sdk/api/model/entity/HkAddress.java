package com.riskpointer.sdk.api.model.entity;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Shurygin Denis
 */

public class HkAddress implements Serializable {

    private String mAddress1;
    private String mAddress2;
    private String mCity;
    private String mAdmin;
    private String mPostalCode;
    private String mCountryIso;
    private String mCountryName;

    public HkAddress() {}

    public HkAddress(Map<String, String> map) {
        mAddress1 = map.get("address_1");
        mAddress2 = map.get("address_2");
        mCity = map.get("city");
        mAdmin = map.get("admin");
        mPostalCode = map.get("postal_code");
        mCountryIso = map.get("country_iso");
        mCountryName = map.get("country_name");
    }

    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<>();

        map.put("address_1", mAddress1);
        map.put("address_2", mAddress2);
        map.put("city", mCity);
        map.put("admin", mAdmin);
        map.put("postal_code", mPostalCode);
        map.put("country_iso", mCountryIso);
        map.put("country_name", mCountryName);

        return map;
    }

    public void setAddress1(String string) {
        mAddress1 = string;
    }

    public void setAddress2(String string) {
        mAddress2 = string;
    }

    public void setCity(String string) {
        mCity = string;
    }

    public void setAdmin(String string) {
        mAdmin = string;
    }

    public void setCountry(String iso, String name) {
        mCountryIso = iso;
        mCountryName = name;
    }

    public void setPostalCode(String string) {
        mPostalCode = string;
    }

    public String getAddress1() {
        return mAddress1;
    }

    public String getAddress2() {
        return mAddress2;
    }

    public String getCity() {
        return mCity;
    }

    public String getAdmin() {
        return mAdmin;
    }

    public String getCountryIso() {
        return mCountryIso;
    }

    public String getCountryName() {
        return mCountryName;
    }

    public String getPostalCode() {
        return mPostalCode;
    }

    public String getFullAddress() {
        StringBuilder builder = new StringBuilder();

        appendAddressPart(builder, mAddress1);
        appendAddressPart(builder, mAddress2);
        appendAddressPart(builder, mCity);

        if (mCity != null && !mCity.equals(mAdmin))
            appendAddressPart(builder, mAdmin);

        appendAddressPart(builder, mPostalCode);
        appendAddressPart(builder, mCountryName);
            
        return builder.toString();
    }
    
    private void appendAddressPart(StringBuilder builder, String part) {
        if (!TextUtils.isEmpty(part)) {
            if (builder.length() > 0)
                builder.append(", ");
            builder.append(part);
        }
    }
}
