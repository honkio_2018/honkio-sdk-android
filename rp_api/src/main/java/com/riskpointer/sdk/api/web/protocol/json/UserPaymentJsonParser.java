/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.UserPayment;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONObject;

/**
 * Parse UserPayment from JSON.
 */
public class UserPaymentJsonParser extends BaseJsonResponseParser<UserPayment> {

	@Override
	public UserPayment parseJson(Message<?> message, JSONObject json) {
		UserPayment result = new UserPayment();
		result.setTimeStamp(ApiFormatUtils.stringDateToLong(json.optString("timestamp")));
		result.setAmount(json.optDouble("account_amount"));
		result.setCurrency(json.optString("account_currency"));
		return result;
	}
}