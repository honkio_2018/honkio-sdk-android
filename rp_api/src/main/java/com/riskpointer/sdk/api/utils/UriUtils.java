package com.riskpointer.sdk.api.utils;

import android.net.Uri;

import com.riskpointer.sdk.api.model.entity.BoughtProduct;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Utils class to build URI's.
 */
public final class UriUtils {
    public static final String PATH_SEGMENT_SHOP = "shop";
    public static final String PATH_SEGMENT_QR_ID = "qrid";

    public static final Uri URI_AUTHORITY = Uri.parse("https://honkio.com");
    public static final String QUERY_QRID = "shop";
    public static final String QUERY_SHOP_ID = "shopId";
    public static final String QUERY_PRODUCT = "product";
    public static final String QUERY_INVENTORY = "inventory";

    private UriUtils() {}

    private static final SimpleDateFormat mBarcodeDateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);
    static {
        mBarcodeDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public static String buildInventoryProductUri(Uri authorityUri, String inventoryProductId) {
        return authorityUri.buildUpon().appendQueryParameter(QUERY_INVENTORY, inventoryProductId)
                .build().toString();
    }

    /**
     * Builds Shop URI.
     * @param authorityUri Authority URI.
     * @param shopId Id of the Shop.
     * @return Sop URI.
     */
    public static String buildShopUri(Uri authorityUri, String shopId) {
        return buildShopUriBuilder(authorityUri, shopId).build().toString();
    }

    /**
     * Build Shop product URI.
     * @param authorityUri Authority URI.
     * @param shopId Id of the Shop.
     * @param productId Id of the Product.
     * @return
     */
    public static String buildShopProductUri(Uri authorityUri, String shopId, String productId) {
        return buildShopUriBuilder(authorityUri, shopId).appendQueryParameter(QUERY_PRODUCT, productId).build().toString();
    }

    /**
     * Build Bought product URI
     * @param shopId Id of the Shop.
     * @param product Bought product.
     * @return Bought product URI.
     */
    @Deprecated
    public static String buildBoughtProductUri(String shopId, BoughtProduct product) {
        StringBuilder builder = new StringBuilder();

        Date date1 = new Date(product.getValidFrom());
        Date date2 = new Date(product.getValidTo());

        builder.append(shopId).append(':');
        builder.append(product.getTransactionId()).append(':');
        builder.append(product.getName()).append(':');
        builder.append(mBarcodeDateFormat.format(date1)).append(':');
        builder.append(mBarcodeDateFormat.format(date2)).append(':');
        builder.append(product.getSignature()).append(":iqt");

        return builder.toString();
    }

    private static Uri.Builder buildShopUriBuilder(Uri authorityUri, String shopId) {
        return authorityUri.buildUpon().appendQueryParameter(QUERY_SHOP_ID, shopId);
    }
}
