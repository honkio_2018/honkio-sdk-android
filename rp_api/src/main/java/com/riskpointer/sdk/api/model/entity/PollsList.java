package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by I.N. on 2018-11-14.
 */

public class PollsList implements Serializable {
    private ArrayList<Poll> mList;
    /**
     * mPollResults
     * this is a poll statistic with poll questions and number of the accepted votes
     * 1st parameter String - id of poll
     * 2nd parameter PollResultList - map of poll options and number of votes for this option
     */
    private HashMap<String, PollAnswersList> mPollResults;
    /**
     * mMyAnswers
     * contain my answer for answered polls
     * 1st parameter - id of poll
     * 2nd parameter string key of question
     */
    private HashMap<String, String> mMyAnswers;

    public ArrayList<Poll> getList() {
        return mList;
    }

    public void setList(ArrayList<Poll> list) {
        mList = list;
    }

    public void addResult(String name, PollAnswersList value) {
        if (mPollResults == null)
            mPollResults = new HashMap<>();

        mPollResults.put(name, value);
    }

    public Map<String, PollAnswersList> getPollResults() {
        return mPollResults;
    }

    public void setPollResults(HashMap<String, PollAnswersList> results) {
        mPollResults = results;
    }

    /**
     * get results for selected poll
     *
     * @param pollId
     * @return
     */
    public PollAnswersList getPollAnswers(String pollId) {
        return mPollResults.get(pollId);
    }

    public void arrangePollsAnswers() {
        if (mPollResults != null) {
            for (Poll poll : mList) {
                poll.setAnswersList(mPollResults.get(poll.getId()));
            }
        }
    }

    public HashMap<String, String> getMyAnswers() {
        return mMyAnswers;
    }

    public void setMyAnswers(HashMap<String, String> myAnswers) {
        mMyAnswers = myAnswers;
    }

    public void arrangeMyAnswers() {
        if (mMyAnswers!=null) {
            for (Poll poll : mList) {
                poll.setMyAnswer(mMyAnswers.get(poll.getId()));
            }
        }
    }
}
