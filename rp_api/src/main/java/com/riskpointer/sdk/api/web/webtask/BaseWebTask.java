package com.riskpointer.sdk.api.web.webtask;

import android.content.Context;

import com.riskpointer.sdk.api.exception.BadStatusCodeException;
import com.riskpointer.sdk.api.exception.WrongHashException;
import com.riskpointer.sdk.api.utils.ApiDebugUtils;
import com.riskpointer.sdk.api.utils.ApiLog;
import com.riskpointer.sdk.api.web.ApiCache;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.response.Response;

import org.apache.http.conn.ConnectTimeoutException;

import java.io.IOException;

// TODO Write docs
/**
 * Created by Shurygin Denis on 2016-11-01.
 */
abstract class BaseWebTask<ResponseResult> implements WebTask<ResponseResult> {

    protected final boolean isWillLogged;
    protected final Context mContext;

    private volatile RpError mError = RpError.NONE;
    private volatile Response<ResponseResult> mResponse;
    private volatile boolean isAborted;
    private volatile boolean isExecuted;
    private volatile boolean isCompleted = false;

    private Message<ResponseResult> mMessage;

    /**
     * Construct new WebServiceAsync.
     *
     * @param context Application Context.
     * @param message Message object.
     */
    BaseWebTask(Context context, Message<ResponseResult> message) {
        if(context == null)
            throw new IllegalArgumentException("Context must be not null.");
        if(message == null)
            throw new IllegalArgumentException("Message must be not null.");
        mContext = context;
        mMessage = message;

        isWillLogged = ApiDebugUtils.isDebuggable(mContext);
    }

    @Override
    abstract public Object clone();
    abstract String sendWebRequest(Message<ResponseResult> message) throws IOException, BadStatusCodeException;

    /**
     *  Abortion of the task. Is different for different priority.<br>
     *  Low Priority and High Priority - can be aborted immediately only if task is in queue and not executed, otherwise abortion callback will send when execution completed.<br>
     *  No queue - Even the task already executed and HTTP connection is created, the task will be aborted.
     *  <br><br>
     *  Aborted task calls oError with the error code ABORTED.
     */
    @Override
    public boolean abort() {
        ApiLog.d("Task '" + mMessage.getCommand() + "' aborted");
        isAborted = true;
        setError(RpError.Api.ABORTED);
        return isAborted;
    }

    @Override
    public boolean isAborted() {
        return isAborted;
    }

    @Override
    public boolean isCompleted() {
        return isCompleted;
    }

    @Override
    public Message<ResponseResult> getMessage() {
        return mMessage;
    }

    @Override
    public RpError getError() {
        if (isAborted())
            return RpError.ABORTED;
        return mError;
    }

    @Override
    public Response<ResponseResult> getResponse() {
        return mResponse;
    }

    @Override
    public Response<ResponseResult> execute() {
        if (isExecuted)
            throw new IllegalStateException("Service already in use. Use a new object.");

        if (mMessage.getConnection() != null)
            try {
                mMessage.getConnection().acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
                isAborted = true;
            }
        if (!isAborted) {
            isExecuted = true;
            if (mMessage.isOffline())
                mResponse = processOffline(mMessage);
            else
                mResponse = processOnline(mMessage);
        }
        isCompleted = true;

        if (mMessage.getConnection() != null)
            mMessage.getConnection().release();

        return mResponse;
    }

    /**
     * Gets Application context which was passed into constructor.
     * @return Application context.
     */
    public Context getContext() {
        return mContext;
    }

    @SuppressWarnings("unchecked")
    Response<ResponseResult> processOffline(Message<ResponseResult> message) {
        Response<ResponseResult> result = null;
        String cacheFileName = message.getCacheFileName();
        if(cacheFileName == null || !message.isSupportOffline()) {
            setError(RpError.Api.CACHE_NOT_SUPPORTED);
        }
        else {
            try {
                result = (Response<ResponseResult>) ApiCache.getWebResponse(mContext, cacheFileName, message.getConnection() != null);
            } catch (ClassCastException e) {
                e.printStackTrace();
                result = null;
            }
            if(result == null) {
                setError(RpError.Api.CACHE_NOT_FOUND);
            }
            else {
                result.setMessage(message);
                Response.Editor.setOffline(result, true);
            }
        }
        return result;
    }

    Response<ResponseResult> processOnline(Message<ResponseResult> message) {
        String responseString = null;
        Response<ResponseResult> result = null;

        try {
            responseString = sendWebRequest(message);
        } catch (BadStatusCodeException | ConnectTimeoutException e) {
            setError(RpError.Api.BAD_RESPONSE);
            return null;
        } catch (IOException e) {
            setError(RpError.Api.NO_CONNECTION);
            return null;
        }
        if(responseString != null) try {
            result = message.parseResponse(responseString);
            if(result != null) {
                if (!"".equals(result.getOtp()) && result.getOtp() != null && message.getConnection() != null) {
                    if (result.isStatusError() && result.getAnyError().code == RpError.Common.INVALID_USER_LOGIN)
                        message.getConnection().increasePinFails();
                    else {
                        message.getConnection().setOtp(result.getOtp());}
                }
            }
            else {
                setError(RpError.Api.BAD_RESPONSE);
                return null;
            }
        } catch (WrongHashException e) {
            ApiLog.e(e.getMessage());
            setError(RpError.Api.WRONG_HASH);
            return null;
        }
        if(!message.isHasFlag(Message.FLAG_NO_CACHE)
                && responseString != null && (result.isStatusAccept() || result.isStatusPending())) {
            ApiCache.putWebResponse(mContext, result);
        }

        if (isAborted())
            return null;

        return result;
    }

    void setError(int errorCode) {
        mError = new RpError(errorCode, mMessage);
    }
}