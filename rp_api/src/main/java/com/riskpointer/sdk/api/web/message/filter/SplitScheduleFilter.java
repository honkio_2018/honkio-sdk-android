package com.riskpointer.sdk.api.web.message.filter;

import com.riskpointer.sdk.api.model.entity.BookedProduct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Shurygin Denis on 2016-07-13.
 */
public class SplitScheduleFilter extends ScheduleFilter {

    private boolean isQueryChild = false;
    private long mStep;
    private long mDuration;
    private int mRound;
    private ArrayList<BookedProduct> mProducts;

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = super.toParams();

        params.put("query_childs", isQueryChild);
        params.put("query_step", mStep);
        params.put("query_round", mRound);
        if (getDuration()!=0) params.put("query_duration", mDuration);
        if (mProducts != null) {
            List<Map<String, Object>> productsArray = new ArrayList<>(mProducts.size());
            for (BookedProduct product: mProducts) {
                Map<String, Object> map = new HashMap<>(2);
                map.put("id", product.getProduct().getId());
                map.put("count", product.getCount());
                productsArray.add(map);
            }
            params.put("query_product", productsArray);
        }
        return params;
    }

    public boolean isQueryChild() {
        return isQueryChild;
    }

    public void setQueryChild(boolean queryChild) {
        isQueryChild = queryChild;
    }

    public long getStep() {
        return mStep;
    }

    public void setStep(long step) {
        mStep = step;
    }

    public long getDuration() {
        return mDuration;
    }

    public void setDuration(long duration) {
        mDuration = duration;
    }

    public int getRound() {
        return mRound;
    }

    public void setRound(int round) {
        mRound = round;
    }

    public ArrayList<BookedProduct> getProductId() {
        return mProducts;
    }

    public void setProducts(ArrayList<BookedProduct> products) {
        mProducts = products;
    }

}
