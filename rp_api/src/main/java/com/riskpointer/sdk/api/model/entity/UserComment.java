package com.riskpointer.sdk.api.model.entity;

import com.riskpointer.sdk.api.web.message.filter.AbsFilter;

import java.util.HashMap;

/**
 * Created by asenko on 1/12/2016.
 */
public class UserComment extends AbsFilter {

    private UserRole mVoter;
    private String mObject;
    private String mObjectType;
    private String mOrderId;
    private String mText;
    private long mTime;
    private int mMark;

    private String mJobTagId;
    private String mOwnerId;

    private String mUserId;
    private String mUserRoleId;

    public UserRole getVoter() {
        return mVoter;
    }

    public void setVoter(UserRole user) {
        this.mVoter = user;
    }

    /**
     * Gets owner of the Order of Comment
     * @return owner of the Order of Comment
     */
    public String getOwnerId() {
        return mOwnerId;
    }

    public void setOwnerId(String userId) {
        this.mOwnerId = userId;
    }

    public long getTime() {
        return mTime;
    }

    public void setTime(long time) {
        this.mTime = time;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        this.mText = text;
    }

    public int getMark() {
        return mMark;
    }

    public void setMark(int mark) {
        this.mMark = mark;
    }

    public String getJobTagId() {
        return mJobTagId;
    }

    public void setJobTagId(String jobTagId) {
        this.mJobTagId = jobTagId;
    }

    public String getOrderId() {
        return mOrderId;
    }

    public void setOrderId(String orderId) {
        this.mOrderId = orderId;
    }

    public String getObject() {
        return mObject;
    }

    public void setObject(String object) {
        mObject = object;
    }

    public String getObjectType() {
        return mObjectType;
    }

    public void setObjectType(String objectType) {
        mObjectType = objectType;
    }

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = new HashMap<>();

        applyIfNotNull(params, "order_id", mOrderId);
        applyIfNotNull(params, "voter_role_id", mVoter != null ? mVoter.getRoleId() : null);
        applyIfNotNull(params, "mark", mMark);
        applyIfNotNull(params, "comment", mText);
        applyIfNotNull(params, "object", mObject);
        applyIfNotNull(params, "object_type", mObjectType);

        applyIfNotNull(params, "user_id", mUserId);
        applyIfNotNull(params, "user_role_id", mUserRoleId);

        return params;
    }

    @Deprecated
    public String getUserId() {
        return mUserId;
    }

    @Deprecated
    public void setUserId(String userId) {
        this.mUserId = userId;
    }

    @Deprecated
    public String getUserRoleId() {
        return mUserRoleId;
    }

    @Deprecated
    public void setUserRoleId(String roleId) {
        this.mUserRoleId = roleId;
    }
}
