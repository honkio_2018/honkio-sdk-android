package com.riskpointer.sdk.api.model.push.entity;

import com.riskpointer.sdk.api.model.push.Push;

import java.io.Serializable;

//TODO write docs
/**
 * Created by Shurygin Denis on 2016-01-15.
 */
public class OrderStatusChangedPushEntity extends Push.PushEntity implements Serializable {

    private String mOrderId;
    private String mOrderTitle;
    private String mOrderStatus;
    private String mOrderOwnerId;
    private String mUserId;
    private String mUserName;

    public OrderStatusChangedPushEntity() {
        super(Push.Type.ORDER_STATUS_CHANGED);
    }

    public String getOrderId() {
        return mOrderId;
    }

    public void setOrderId(String orderId) {
        this.mOrderId = orderId;
    }

    public String getOrderTitle() {
        return mOrderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.mOrderTitle = orderTitle;
    }

    public String getOrderStatus() {
        return mOrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.mOrderStatus = orderStatus;
    }

    public String getOrderOwnerId() {
        return mOrderOwnerId;
    }

    public void setOrderOwnerId(String orderOwnerId) {
        this.mOrderOwnerId = orderOwnerId;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        this.mUserId = userId;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        this.mUserName = userName;
    }

    @Override
    public String getUid() {
        String orderId = getOrderId();
        return "null|" + (orderId != null ? orderId : "null");
    }
}
