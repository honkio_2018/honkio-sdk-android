/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.message.filter;

import com.riskpointer.sdk.api.web.message.Message;

import java.text.DecimalFormat;
import java.util.Map;

/**
 * Abstract filter that can be used for setting a filter parameters for the message.
 * @author Denis Shurygin
 */
public abstract class AbsFilter implements HKFilter {
    /** DecimalFormat to convert double value into String. */
    public static final DecimalFormat AMOUNT_FORMAT = new DecimalFormat("#0.00");

    /**
     * Convert filter to HashMap that hold set of params.
     * @return HashMap that represent this filter.
     */
	public abstract Map<String, Object> toParams();

    /**
     * Apply filter for specified message. Also filter put self into message as null parameter.
     * @param message Message for apply filter.
     */
    public void applyTo(Message<?> message) {
        message.addParameters(toParams());
        message.addParameter(null, this);
    }

    /**
     * Sets parameter for message if value is not {@code null}.
     * @param message Message for which the parameter is set.
     * @param name Name of the parameter.
     * @param value Value of the parameter.
     */
	public static void applyIfNotNull(Message<?> message, String name, Object value) {
		if(value != null)
			message.addParameter(name, value);
	}

    /**
     * Sets parameter for map if value is not {@code null}.
     * @param params map for which the parameter is set.
     * @param name Name of the parameter.
     * @param value Value of the parameter.
     */
	public static void applyIfNotNull(Map<String, Object> params, String name, Object value) {
		if(value != null && name != null)
			params.put(name, value);
	}

    /**
     * Set parameter for map if value is not {@code null} otherwise remove previous value.
     * @param params Map for which the parameter is set.
     * @param name Name of the parameter.
     * @param value Value of the parameter.
     */
    public static void applyOrRemove(Map<String, Object> params, String name, Object value) {
        if(value != null)
            params.put(name, value);
        else
            params.remove(name);
    }

    /**
     * Convert amount value to String.
     * @param amount Value that should be converted.
     * @return Amount converted to String.
     */
    public static String amountToString(double amount) {
        return AMOUNT_FORMAT.format(amount).replace(',', '.');
    }
}
