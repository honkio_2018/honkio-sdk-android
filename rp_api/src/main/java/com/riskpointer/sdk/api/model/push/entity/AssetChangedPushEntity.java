package com.riskpointer.sdk.api.model.push.entity;

import com.riskpointer.sdk.api.model.push.Push;

import java.util.Map;

/**
 * @author Shurygin Denis
 */
public class AssetChangedPushEntity extends Push.PushEntity {

    public final class Action {

        private Action() {}

        public final static String CREATED = "created";
        public final static String UPDATED = "updated";
        public final static String REMOVED = "removed";

    }

    public AssetChangedPushEntity(Map<String, Object> content) {
        super(Push.Type.ASSET_CHANGED, content);
    }

    public void setAssetId(String value) {
        setContent("asset_id", value);
    }

    public String getAssetId() {
        return (String) getContent("asset_id");
    }

    public void setAssetName(String value) {
        setContent("asset_name", value);
    }

    public String getAssetName() {
        return (String) getContent("asset_name");
    }

    public void setStructureId(String value) {
        setContent("structure_id", value);
    }

    public String getStructureId() {
        return (String) getContent("structure_id");
    }

    public void setMerchantId(String value) {
        setContent("merchant", value);
    }

    public String getMerchantId() {
        return (String) getContent("merchant");
    }

    public void setAction(String value) {
        setContent("action", value);
    }

    public String getAction() {
        return (String) getContent("action");
    }

    public boolean actionIs(String action) {
        String pushAction = getAction();
        return (action != null && action.equals(pushAction))
                || (pushAction == null && action == null);
    }

    public void setShopId(String value) {
        setContent("shop_id", value);
    }

    public String getShopId() {
        return (String) getContent("shop_id");
    }

}
