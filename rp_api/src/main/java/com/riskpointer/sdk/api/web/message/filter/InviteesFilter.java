package com.riskpointer.sdk.api.web.message.filter;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by I.N. on 06.04.2018
 */
public class InviteesFilter extends ListFilter implements Serializable {
    public static final String QUERY_STATUS = "query_status";
    public static final String QUERY_PARENT_ORDER_ID = "query_parent_order_id";
    private String mQueryStatus;
    private String mQueryParentOrderId;

    public String getQueryStatus() {
        return mQueryStatus;
    }

    public void setQueryStatus(String queryStatus) {
        mQueryStatus = queryStatus;
    }

    public String getQueryParentOrderId() {
        return mQueryParentOrderId;
    }

    public void setQueryParentOrderId(String queryParentOrderId) {
        mQueryParentOrderId = queryParentOrderId;
    }

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = super.toParams();

        applyIfNotNull(params, QUERY_STATUS, getQueryStatus());
        applyIfNotNull(params, QUERY_PARENT_ORDER_ID, getQueryParentOrderId());

        return params;
    }
}