/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.entity;

import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.AbsFilter;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Data object that hold list of shops that was get by {@link Message.Command#SHOP_FIND} command.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 *
 * @author KamiSempai
 */
public class ShopFind implements Serializable{
	private static final long serialVersionUID = 5928226964771860914L;

	private AbsFilter mFilter;
	private ArrayList<Shop> mList;

	/**
	 * Gets list of the found Shops.
	 * @return List of the found Shops.
	 */
	public ArrayList<Shop> getList() {
		return mList;
	}

	/**
	 * Sets list of the found Shops.
	 * @param list List of the found Shops.
	 */
	public void setList(ArrayList<Shop> list) {
		this.mList = list;
	}

	/**
	 * Sets the Filter that was used to get shops list.
	 * @param filter Filter that was used to get shops list.
	 */
	public void setFilter(AbsFilter filter) {
		mFilter = filter;
	}

	/**
	 * Gets the Filter that was used to get shops list.
	 * @return Filter that was used to get shops list.
	 */
	public AbsFilter getFilter() {
		return mFilter;
	}
}
