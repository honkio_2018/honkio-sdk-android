package com.riskpointer.sdk.api.model.entity;

import com.riskpointer.sdk.api.web.message.filter.ChatMessageFilter;

import java.util.List;

/**
 * Created by Shurygin Denis on 2016-05-11.
 */
public class ChatMessages {

    private List<ChatMessage> mList;
    private ChatMessageFilter mFilter;

    public List<ChatMessage> getList() {
        return mList;
    }

    public void setList(List<ChatMessage> list) {
        this.mList = list;
    }

    public ChatMessageFilter getFilter() {
        return mFilter;
    }

    public void setFilter(ChatMessageFilter filter) {
        this.mFilter = filter;
    }
}
