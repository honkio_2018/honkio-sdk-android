package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.HkStructure;
import com.riskpointer.sdk.api.model.entity.HkStructureList;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * @author Shurygin Denis
 */

public class StructureListJsonParser extends BaseJsonResponseParser<HkStructureList> {

    @Override
    protected HkStructureList parseJson(Message<?> message, JSONObject json) {
        ArrayList<HkStructure> list = new ArrayList<>();
        JSONArray structureJsonArray = json.optJSONArray("assets");
        if (structureJsonArray != null) {
            for (int i = 0; i < structureJsonArray.length(); i++) {
                JSONObject structureJson = structureJsonArray.optJSONObject(i);
                if (structureJson != null) {
                    list.add(StructureJsonParser.toStructure(structureJson));
                }
            }
        }
        JSONArray userStructureJsonArray = json.optJSONArray("descriptions");
        if (userStructureJsonArray != null) {
            for (int i = 0; i < userStructureJsonArray.length(); i++) {
                JSONObject structureJson = userStructureJsonArray.optJSONObject(i);
                if (structureJson != null) {
                    list.add(UserStructureJsonParser.toStructure(structureJson));
                }
            }
        }
        HkStructureList structureList = new HkStructureList();
        structureList.setList(list);

        return structureList;
    }
}
