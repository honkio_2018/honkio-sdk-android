package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Rate;
import com.riskpointer.sdk.api.model.entity.RateList;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Parse Ratings list from JSON.
 */
public class RateListJsonParser extends BaseJsonResponseParser<RateList> {

    @Override
    public RateList parseJson(Message<?> message, JSONObject json) {
        RateList assetList = new RateList();
        ArrayList<Rate> list = new ArrayList<>();

        JSONArray assetListJson = json.optJSONArray("rates");
        if (assetListJson != null) {
            for (int i = 0; i < assetListJson.length(); i++) {
                JSONObject rateJson = assetListJson.optJSONObject(i);
                if (rateJson != null) {
                    Rate rate = new Rate(rateJson.optInt("mark"), rateJson.optInt("count"));
                    list.add(rate);
                }
            }
        }
        assetList.setList(list);
        return assetList;
    }

}
