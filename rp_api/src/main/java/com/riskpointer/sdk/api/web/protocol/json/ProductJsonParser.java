/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Product;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class ProductJsonParser extends BaseJsonResponseParser<Product> {

    @Override
    public Product parseJson(Message<?> message, JSONObject json) {
        return parseProduct(json);
    }

    public static Product parseProduct(JSONObject productJson) {
        Product product = new Product(jsonToMap(productJson));
        if (productJson != null) {
            product.setId(productJson.optString("id", ""));
            product.setName(productJson.optString("name", ""));
            product.setDescription(productJson.optString("description", ""));
            product.setCurrency(productJson.optString("currency", ""));
            product.setAmount(productJson.optDouble("amount", 0));
            product.setAmountVat(productJson.optDouble("amount_vat", 0));
            product.setAmountNoVat(productJson.optDouble("amount_no_vat", 0));
            product.setAmountVatPercent(productJson.optDouble("amount_vat_percent", 0));

            product.setValidityTime(productJson.optLong("int_validity_time", 0));
            product.setValidityTimeType(productJson.optString("str_validity_time_type"));
            if (productJson.has("id_type")) {
                product.setIdType(productJson.optString("id_type", ""));
            }else {
                if (productJson.has("type_id")) {
                    product.setIdType(productJson.optString("type_id", ""));
                }
            }
            product.setActive(productJson.optBoolean("active",true));

            if (productJson.has("int_duration"))
                product.setDuration(productJson.optLong("int_duration", 0));

            JSONArray targetProductsJson = productJson.optJSONArray("target_products");
            if (targetProductsJson != null) {
                ArrayList<Product.Target> targetsList
                        = new ArrayList<>(targetProductsJson.length());
                for (int i = 0; i < targetProductsJson.length(); i++) {
                    JSONObject targetJson = targetProductsJson.optJSONObject(i);
                    if (targetJson != null) {
                        String id = targetJson.optString("id");
                        int count = targetJson.optInt("required_count");
                        if (id != null && count > 0)
                            targetsList.add(new Product.Target(id, count));
                    }
                }

                product.setTargetProducts(targetsList);
            }

            if (productJson.has("amount_surplus")) {
                HashMap<String, Double> suppluses = new HashMap<>();
                JSONObject supplusesJson = productJson.optJSONObject("amount_surplus");
                if (supplusesJson != null) {
                    Iterator<String> keysIterator = supplusesJson.keys();
                    while (keysIterator.hasNext()) {
                        String key = keysIterator.next();
                        suppluses.put(key, supplusesJson.optDouble(key));
                    }
                }
                product.setSupplusMap(suppluses);
            }

            ArrayList<String> tagsList = new ArrayList<>();
            JSONArray tagsListJson = productJson.optJSONArray("list_tags");
            if (tagsListJson != null && tagsListJson.length() > 0) {
                for (int j = 0; j < tagsListJson.length(); j++) {
                    String tagId = tagsListJson.optString(j);
                    if (tagId != null)
                        tagsList.add(tagId);
                }
            }
            product.setTagIds(tagsList);

            JSONArray picsJson = productJson.optJSONArray("list_pics");
            if (picsJson != null && picsJson.length() > 0) {
                ArrayList<Product.Pic> pics = new ArrayList<Product.Pic>();
                for (int j = 0; j < picsJson.length(); j++) {
                    JSONObject picJson = picsJson.optJSONObject(j);
                    if (picJson != null) {
                        String url = picJson.optString("url");
                        int x = picJson.optInt("x");
                        int y = picJson.optInt("y");
                        if (url != null && !"".equals(url)) {
                            pics.add(new Product.Pic(url, x, y));
                        }
                    }
                }
                product.setPicsList(pics);
            }
        }
        return product;
    }
}
