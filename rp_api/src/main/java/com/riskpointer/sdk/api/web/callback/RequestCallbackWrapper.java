/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.callback;

import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;

/**
 * Wrapper for {@link RequestCallback}. This wrapper has special behavior:
 * @param <Res> Response type
 */
public class RequestCallbackWrapper<Res> implements RequestCallback<Res> {

    private RequestCallback<Res> mCallback;

    /**
     * Construct new Wrapper.
     * @param callback Callback that will be wrapped.
     */
    public RequestCallbackWrapper(RequestCallback<Res> callback) {
        mCallback = callback;
    }

    @Override
    public boolean onError(RpError error) {
        return invokeOnError(error);
    }

    @Override
    public boolean onComplete(Response<Res> response) {
        return invokeOnComplete(response);
    }

    /**
     * Sets a new callback.<br>
     * Similarly {@link #set(RequestCallback, boolean)} when isGoDeep equal true.
     * @param callback Callback that will be wrapped.
     */
    public void set(RequestCallback<Res> callback) {
        set(callback, true);
    }

    /**
     * Sets a new callback.<br>
     * These method will set new callback for the wrapped callback if wrapped callback is instance
     * of {@link RequestCallbackWrapper} and isGoDeep equal true.
     * @param callback Callback that will be wrapped.
     * @param isGoDeep True if new callback should set for wrapped callback when wrapped callback
     *                 is instance of {@link RequestCallbackWrapper}, false otherwise.
     */
    public void set(RequestCallback<Res> callback, boolean isGoDeep) {
        if (mCallback != null && isGoDeep && mCallback instanceof RequestCallbackWrapper)
            ((RequestCallbackWrapper<Res>) mCallback).set(callback, true);
        else
            mCallback = callback;
    }

    public RequestCallback<Res> get() {
        return mCallback;
    }

    /**
     * Invokes onComplete of wrapped callback if it is not null.
     * @param response Object that represent the server response.
     * @return True if response was handled, false otherwise.
     */
    protected boolean invokeOnComplete(Response<Res> response) {
        return mCallback != null && mCallback.onComplete(response);
    }

    /**
     * Invokes onError of wrapped callback if it is not null.
     * @param error Error object with error description.
     * @return true if error is handled, false otherwise.
     */
    protected boolean invokeOnError(RpError error) {
        return mCallback != null && mCallback.onError(error);
    }
	
}
