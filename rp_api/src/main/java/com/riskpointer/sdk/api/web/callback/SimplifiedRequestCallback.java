package com.riskpointer.sdk.api.web.callback;

import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;

/**
 * @author Shurygin Denis
 */
public class SimplifiedRequestCallback<Type> implements RequestCallback<Type> {

    private SimpleCallback mCallback;

    public SimplifiedRequestCallback(SimpleCallback callback) {
        mCallback = callback;
    }

    @Override
    public boolean onComplete(Response<Type> response) {
        if (response.isStatusAccept()) {
            if (mCallback != null){
                mCallback.onComplete();
                return true;
            }
            return false;
        }
        return onError(response.getAnyError());
    }

    @Override
    public boolean onError(RpError error) {
        return mCallback != null && mCallback.onError(error);
    }
}
