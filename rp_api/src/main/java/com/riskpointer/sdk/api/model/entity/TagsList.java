package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by asenko on 12/15/2015.
 * TODO write docs
 */
public class TagsList implements Serializable {

    private HashMap<String, Tag> mMap;

    public HashMap<String, Tag> getList() {
        return mMap;
    }

    public void setList(HashMap<String, Tag> map) {
        this.mMap = map;
    }
}