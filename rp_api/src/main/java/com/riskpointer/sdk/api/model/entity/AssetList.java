package com.riskpointer.sdk.api.model.entity;

import java.util.List;

/**
 * Created by I.N. on 2017-02-16.
 */

public class AssetList {

    private List<Asset> mList;

    public List<Asset> getList() {
        return mList;
    }

    public void setList(List<Asset> list) {
        mList = list;
    }
}
