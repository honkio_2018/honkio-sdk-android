/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.entity;

import android.location.Location;
import android.text.TextUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Data object that holds information about shop.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
public class Shop extends Identity implements Serializable {
    private static final long serialVersionUID = 2L;

    /**
     * Known types of the shops.
     */
    public static class Type {
        /**
         * Shop has list of the products. Payment requests should contains list of {@link BoughtProduct}.
         */
        public static final String PRODUCT = "product";
        /**
         * Shop doesn't has a products. Payment requests should contains amount of money.
         */
        public static final String PAYMENT = "payment";
        /**
         * Shop for authentication requests.
         */
        public static final String LOGIN = "login";
        /**
         * Shop for registration requests.
         */
        public static final String REGISTRATION = "registration";
        /**
         * Shop for "lostPassword" request.
         */
        public static final String LOSTPASSWORD = "lostpassword";
    }

    /**
     * Known service types of the shops.
     */
    public static class ServiceType {
        public static final String GAS_STATION = "gas_station";
        public static final String CAR_WASH = "car_wash";
        public static final String VENDING = "vending";
        public static final String GATE_PARKING = "gate_parking";
        public static final String OPEN_PARKING = "open_parking";
        public static final String ACCESS_FREE = "access_fee";

        public static final String E_COMMERCE = "ecommerce";
    }

    private String mName = "";
    private String mDescription = "";
    private double mLatitude;
    private double mLongitude;
    private String mPublicKey = "";
    private String mType;
    private String mServiceType;
    private String mLogoSmall;
    private String mLogoLarge;
    private MerchantSimple mMerchant;
    private String mAddress;
    private List<Rate> mRatings;
    private List<Tag> mTags;
    private double mMaxDistance;
    private Long mClosingTime;
    private long mClosingOffset;
    private double mPreAuthorizationAmount;
    private HashMap<String, Object> mSettings = new HashMap<>();

    public Shop() {
        super();
    }

    /**
     * Constructor from HKModelEntity.
     */
    public Shop(Map<String, Object> props) {
        super(props);
    }

    public Shop(Identity identity) {
        super(identity);
    }

    /**
     * Gets Shop name.
     *
     * @return Shop name.
     */
    public String getName() {
        return mName;
    }

    /**
     * Sets Shop name.
     *
     * @param name Shop name.
     */
    public void setName(String name) {
        this.mName = name;
    }

    /**
     * Gets Shop description.
     *
     * @return Shop description.
     */
    public String getDescription() {
        return mDescription;
    }

    /**
     * Sets Shop description.
     *
     * @param description Shop description.
     */
    public void setDescription(String description) {
        this.mDescription = description;
    }

    /**
     * Gets latitude of real position of the Shop.
     *
     * @return Coordinate latitude of real position.
     */
    public double getLatitude() {
        return mLatitude;
    }

    /**
     * Sets latitude of real position of the Shop.
     *
     * @param latitude Coordinate latitude of real position.
     */
    public void setLatitude(double latitude) {
        this.mLatitude = latitude;
    }

    /**
     * Gets longitude of real position of the Shop.
     *
     * @return Coordinate longitude of real position.
     */
    public double getLongitude() {
        return mLongitude;
    }

    /**
     * Sets longitude of real position of the Shop.
     *
     * @param longitude Coordinate longitude of real position.
     */
    public void setLongitude(double longitude) {
        this.mLongitude = longitude;
    }

    /**
     * Gets public SSH key. The Key can be used to check transaction or product signatures.
     *
     * @return Public SSH key.
     */
    public String getPublicKey() {
        return mPublicKey;
    }

    /**
     * Sets public SSH key. The Key will be used to check transaction or product signatures.
     *
     * @param publicKey Public SSH key.
     */
    public void setPublicKey(String publicKey) {
        this.mPublicKey = publicKey;
    }

    /**
     * Gets Shop type.
     *
     * @return Shop type.
     * @see Shop.Type
     */
    public String getType() {
        return mType;
    }

    /**
     * Sets Shop type.
     *
     * @param type Shop type.
     */
    public void setType(String type) {
        this.mType = type;
    }

    /**
     * Gets Shop service type.
     *
     * @return Shop service type.
     * @see Shop.ServiceType
     */
    public String getServiceType() {
        return mServiceType;
    }

    /**
     * Sets Shop service type.
     *
     * @param type Shop service type.
     */
    public void setServiceType(String type) {
        this.mServiceType = type;
    }

    /**
     * Gets Small logo URL or file name in server assets. To build URL for server assets use {@link ServerInfo#getAssetsUrl()}.<br>
     * The Small logo should be used in the shop lists.
     *
     * @return Small logo URL or file name in server assets.
     * @see ServerInfo#getAssetsUrl()
     */
    public String getLogoSmall() {
        return mLogoSmall;
    }

    /**
     * Sets Small logo URL or file name in server assets.<br>
     * The Small logo will be used in the shop lists.
     *
     * @param logoUrl Small logo URL or file name in server assets.
     */
    public void setLogoSmall(String logoUrl) {
        this.mLogoSmall = logoUrl;
    }

    /**
     * Gets Large logo URL or file name in server assets. To build URL for server assets use {@link ServerInfo#getAssetsUrl()}.<br>
     * The Large logo should be used in the shop detail pages.
     *
     * @return Large logo URL or file name in server assets.
     * @see ServerInfo#getAssetsUrl()
     */
    public String getLogoLarge() {
        return mLogoLarge;
    }

    /**
     * Sets Large logo URL or file name in server assets.<br>
     * The Large logo will be used in the shop detail pages.
     *
     * @param logoUrl Large logo URL or file name in server assets.
     */
    public void setLogoLarge(String logoUrl) {
        this.mLogoLarge = logoUrl;
    }

    /**
     * Gets parent Merchant of the Shop.
     *
     * @return Parent Merchant
     */
    public MerchantSimple getMerchant() {
        return mMerchant;
    }

    /**
     * Sets parent Merchant of the Shop.
     *
     * @param merchant Parent Merchant
     */
    public void setMerchant(MerchantSimple merchant) {
        this.mMerchant = merchant;
    }

    /**
     * Gets Shop address.
     *
     * @return Shop address.
     */
    public String getAddress() {
        return (mAddress == null ? ""  :mAddress);
    }

    /**
     * Sets Shop address.
     *
     * @param address Shop address.
     */
    public void setAddress(String address) {
        this.mAddress = address;
    }

    /**
     * Gets shop ratings
     * @return Shop ratings
     */
    public List<Rate> getRatings() {
        return this.mRatings;
    }

    /**
     * Sets shop ratings
     * @param ratings Shop ratings
     */
    public void setRatings (List<Rate> ratings){
        this.mRatings = ratings;
    }

    /**
     * Gets shop tags
     * @return Shop tags
     */
    public List<Tag> getTags() {
        return this.mTags;
    }

    // TODO docs
    public boolean isHasTagWithId(String tagId) {
        if (tagId != null && mTags != null)
            for (Tag tag: mTags)
                if (tagId.equals(tag.getId()))
                    return true;

        return false;
    }

    // TODO docs
    public boolean isHasTagWithName(String tagName) {
        if (tagName != null && mTags != null)
            for (Tag tag: mTags)
                if (tagName.equals(tag.getName()))
                    return true;

        return false;
    }

    /**
     * Sets shop tags
     * @param tags Shop tags
     */
    public void setTags (List<Tag> tags){
        this.mTags = tags;
    }

    // TODO docs
    public double getMaxDistance() {
        return mMaxDistance;
    }

    // TODO docs
    public void setMaxDistance(double distance) {
        this.mMaxDistance = distance;
    }

    // TODO docs
    public Long getClosingTime() {
        return mClosingTime;
    }

    // TODO docs
    public void setClosingTime(Long time) {
        this.mClosingTime = time;
    }

    // TODO docs
    public long getClosingOffset() {
        return mClosingOffset;
    }

    // TODO docs
    public void setClosingOffset(long time) {
        this.mClosingOffset = time;
    }

    // TODO docs
    public double getPreAuthorizationAmount() {
        return mPreAuthorizationAmount;
    }

    // TODO docs
    public void setPreAuthorizationAmount(double mPreAuthorisationValue) {
        this.mPreAuthorizationAmount = mPreAuthorisationValue;
    }
    //TODO doc fields
    public Object getField(String name) {
        if (mSettings != null) {
            return mSettings.get(name);
        }
        return null;
    }
    public void setField(String name, Object value) {
        if (mSettings != null) {
            mSettings.put(name, value);
        } else {
            mSettings = new HashMap<>();
            mSettings.put(name, value);
        }
    }

    public Map<String, Object> getSettings() {
        return mSettings;
    }

    public void setSettings(HashMap<String, Object> settings) {
        mSettings = settings;
    }

    // TODO docs
    public boolean checkDistance(double latitude, double longitude) {
        float[] results = new float[1];
        Location.distanceBetween(mLatitude, mLongitude, latitude, longitude, results);
        return results[0] < mMaxDistance;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (getClass() != obj.getClass())
            return false;

        Shop shop = (Shop) obj;

        return super.equals(obj) &&
                TextUtils.equals(mName, shop.mName) &&
                mLatitude == shop.mLatitude &&
                mLongitude == shop.mLongitude &&
                TextUtils.equals(mPublicKey, shop.mPublicKey) &&
                TextUtils.equals(mType, shop.mType) &&
                TextUtils.equals(mServiceType, shop.mServiceType) &&
                TextUtils.equals(mLogoSmall, shop.mLogoSmall) &&
                TextUtils.equals(mLogoLarge, shop.mLogoLarge) &&
                ((mMerchant != null && mMerchant.equals(shop.mMerchant)) ||
                        (mMerchant == null && shop.mMerchant == null)) &&
                TextUtils.equals(mAddress, shop.mAddress) &&
                ((mRatings != null && mRatings.equals(shop.mRatings)) ||
                        (mRatings == null && shop.mRatings == null)) &&
                mMaxDistance == shop.mMaxDistance;

    }
}
