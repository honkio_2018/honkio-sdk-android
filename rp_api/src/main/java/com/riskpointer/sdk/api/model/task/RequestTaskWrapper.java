package com.riskpointer.sdk.api.model.task;

import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;

/**
 * Created by Shurygin Denis on 2016-08-01.
 */
// TODO write docs
public class RequestTaskWrapper<Result> extends TaskWrapper implements RequestTask<Result>, RequestCallback<Result> {

    private RequestTask<Result> mWrappedRequestTask;
    private RequestCallback<Result> mWrappedCallback;

    /**
     * Construct new wrapper. Wrapped task is null.
     */
    public RequestTaskWrapper() {
        this(null);
    }

    /**
     * Construct new wrapper.
     * @param task Task that will be wrapped.
     */
    public RequestTaskWrapper(Task task) {
        super(task);
    }

    // TODO write docs
    public RequestTaskWrapper(RequestTask<Result> task, RequestCallback<Result> callback) {
        this(task);
        mWrappedCallback = callback;
        mWrappedRequestTask = task;
        if (mWrappedRequestTask != null)
            mWrappedRequestTask.setCallback(this, true);
    }

    @Override
    public void setCallback(RequestCallback<Result> callback, boolean deliverResult) {
        mWrappedCallback = callback;
        if (mWrappedRequestTask != null)
            mWrappedRequestTask.setCallback(this, deliverResult);
    }

    @Override
    public synchronized void set(Task task) {
        super.set(task);
        if (mWrappedRequestTask != null) {
            mWrappedRequestTask.setCallback(null, false);
            mWrappedRequestTask = null;
        }
    }

    public synchronized void set(RequestTask<Result> task) {
        super.set(task);
        mWrappedRequestTask = task;
        mWrappedRequestTask.setCallback(this, true);
    }

    @Override
    public boolean onError(RpError error) {
        return mWrappedCallback != null && mWrappedCallback.onError(error);
    }

    @Override
    public boolean onComplete(Response<Result> response) {
        return mWrappedCallback != null && mWrappedCallback.onComplete(response);
    }
}
