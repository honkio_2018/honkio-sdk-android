package com.riskpointer.sdk.api.model.database;

/**
 * Columns for table {@link Table#STAMP}.
 * @author Shurygin Denis on 2015-08-18.
 */
public interface StampColumns {
    public static final String SHOP_ID = "stamp_shop_id";
    public static final String TRANSACTION_ID = "stamp_transaction_id";
    public static final String NAME = "stamp_product_name";
    public static final String VALID_FROM = "stamp_product_valid_from";
    public static final String VALID_TO = "stamp_product_valid_to";
    public static final String SIGNATURE = "stamp_transaction_signature";
    public static final String READING_TIME = "stamp_reading_time";
    public static final String IS_SENT_TO_SERVER = "stamp_is_sent_to_server";
    public static final String IS_VALID = "stamp_product_is_valid";
    public static final String ORIGINAL_STRING = "stamp_string_data";
    public static final String SERVER_ERROR_CODE = "stamp_server_error_code";
}
