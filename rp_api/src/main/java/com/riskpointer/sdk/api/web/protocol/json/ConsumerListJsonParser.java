package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Consumer;
import com.riskpointer.sdk.api.model.entity.ConsumerList;
import com.riskpointer.sdk.api.model.entity.RolesList;
import com.riskpointer.sdk.api.model.entity.UserRole;
import com.riskpointer.sdk.api.model.entity.UserRoleDescription;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Parse Consumers list from JSON
 */
public class ConsumerListJsonParser extends BaseJsonResponseParser<ConsumerList> {

    @Override
    public ConsumerList parseJson(Message<?> message, JSONObject json) {
        ConsumerList consumerList = new ConsumerList();
        ArrayList<Consumer> list = new ArrayList<>();

        JSONArray consumerListJson = json.optJSONArray("consumers");
        if (consumerListJson != null) {
            for (int i = 0; i < consumerListJson.length(); i++) {
                JSONObject consumerJson = consumerListJson.optJSONObject(i);
                if (consumerJson != null) {
                    Consumer consumer = ConsumerJsonParser.parseConsumer(message, consumerJson);
                    if (consumer != null) {
                        list.add(consumer);
                    }
                }
            }
        }
        consumerList.setList(list);
        JSONArray rolesJsonArray = json.optJSONArray("roles");
        if (rolesJsonArray != null) {
            List<UserRoleDescription> roles = new ArrayList<>(rolesJsonArray.length());
            for (int i = 0; i < rolesJsonArray.length(); i++) {
                JSONObject roleJson = rolesJsonArray.optJSONObject(i);
                if (roleJson != null) {
                    roles.add(UserRoleDescriptionJsonParser.parseRoleDescription(roleJson));
                }
            }
            consumerList.setRolesDescription(roles);
        }
        return consumerList;
    }

}
