package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Shurygin Denis
 */
public class HKString implements Serializable {

    private final HashMap<String, String> mValues = new HashMap<>();

    public void setLocaleValue(String locale, String value) {
        mValues.put(locale, value);
    }

    public String getLocaleValue(String locale) {
        return mValues.get(locale);
    }

    public Set<String> getLocales() {
        return mValues.keySet();
    }

    @Override
    public String toString() {
        String defaultValue = mValues.get(null);

        if (defaultValue != null)
            return defaultValue;

        if (mValues.size() > 0) {
            for (String value: mValues.values())
                if (value != null)
                    return value;
        }
        return "";
    }

    public Map<String, String> toParams(String name) {
        Map<String, String> params = new HashMap<>();

        String defaultValue = mValues.get(null);
        if (defaultValue != null)
            params.put(name, defaultValue);

        for (Map.Entry<String, String> entry: mValues.entrySet()) {
            params.put("_" + entry.getKey() + "_" + name, entry.getValue());
        }
        return params;
    }
}
