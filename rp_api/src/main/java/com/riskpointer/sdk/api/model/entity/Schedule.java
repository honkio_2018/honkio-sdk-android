package com.riskpointer.sdk.api.model.entity;

import com.riskpointer.sdk.api.web.message.filter.ScheduleFilter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//TODO write docs
/**
 * Created by Shurygin Denis on 2016-05-31.
 */
public class Schedule implements Serializable {

    private List<Event> mList;
    private ScheduleFilter mFilter;

    public List<Event> getList() {
        return mList;
    }

    public void setList(List<Event> list) {
        this.mList = list;
    }

    public ScheduleFilter getFilter() {
        return mFilter;
    }

    public void setFilter(ScheduleFilter filter) {
        this.mFilter = filter;
    }

    public Schedule split(long duration, long step, String[] types) {
        List<Event> list = new ArrayList<>();
        for (Event event: mList) {
            boolean isTypeOk = true;
            if (types != null) {
                isTypeOk = false;
                for (String type : types)
                    if (type.equals(event.getType())) {
                        isTypeOk = true;
                        break;
                    }
            }

            if (isTypeOk) {
                long time = event.getStart();
                if (time % step > 0)
                    time += step - time % step;
                while (time + duration <= event.getEnd()) {
                    Event subEvent = new Event();
                    subEvent.setType(event.getType());
                    subEvent.setStart(time);
                    subEvent.setEnd(time + duration);
                    list.add(subEvent);
                    time += step;
                }
            }
        }
        Schedule schedule = new Schedule();
        schedule.setList(list);
        schedule.setFilter(mFilter);
        return schedule;
    }
}
