/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.entity;

import android.text.TextUtils;

import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.AbsFilter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.riskpointer.sdk.api.model.entity.AppInfo.TouInfo.KEY_DEFAULT;

/**
 * An object that contains information of the user.
 * <br>Also this class extent {@code AbsFilter} and can be used like a Filter.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 * 
 * @author Denis Shurygin
 *
 */
public class User extends AbsFilter implements Serializable {
	private static final long serialVersionUID = -4643239168338132289L;

    /**
	 * Templates and patterns of the user dictionary.
	 * 
	 * @author Denis Shurygin
	 *
	 */
	public static class Dict {
		/** Template for a dictionary value. Will replaced on the current date and time. */ 
		public static final String TIMESTAMP_TEMPLATE = "{timestamp}";
		/** Pattern for a dictionary key. Used for writing a license accept. */
		public static final String LICENSE_PATTERN = "_%0$s_license";

		// TODO write docs, make public
		private static final String DEFAULT_ACCOUNT_TYPE = "def_account_type";
		private static final String DEFAULT_ACCOUNT_NUMBER = "def_account_number";
	}

	/**
	 * "Terms of Use" agree record.
	 */
	public static class TouVersion implements Serializable {

		public final int version;
		public final long timeStamp;

		public TouVersion(int version, long timeStamp) {
			this.version = version;
			this.timeStamp = timeStamp;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;

			if (obj == null)
				return false;

			if (getClass() != obj.getClass())
				return false;

			TouVersion tou = (TouVersion) obj;

			return version == tou.version && timeStamp == tou.timeStamp;
		}
	}
	
	private String mLogin;
	private String mPassword;
	
	private String mFirstName;
	private String mLastName;

    private String mTelephone;
    private String mSsn;
	private String mLanguage;
	private String mTimezone;

	private String mAddress1;
	private String mAddress2;
	private String mZip;
    private String mCity;
    private String mState;
    private String mCountry;

	private List<UserAccount> mAccounts = new ArrayList<>();
	private HashMap<String, HashMap<String, TouVersion>> mTouVersions;

	@Deprecated // Not Tested
	private Map<String, String> mDict = new HashMap<>();
	@Deprecated // Not Tested
	private Map<String, Invoicer> mInvoicers = new HashMap<>();
	
	private Boolean isReceipt;
	private boolean isActive;
	private boolean isTempPasswordUsed;
    private boolean isOperatorBillingEnabled;

    private String mRegistrationId;
	private String mUserId;

	/**
	 * Gets User login (email).
	 * @return User login (email).
	 */
	public String getLogin() {
		return mLogin;
	}

	/**
	 * Sets User login (email).
	 * @param login User login (email).
	 */
	public void setLogin(String login) {
		this.mLogin = login;
	}

	/**
	 * Gets User password. If User gets via userGet or another similar request the password will be null. Server newer send password for the client.
	 * @return User password.
	 */
	public String getPassword() {
		return mPassword;
	}

	/**
	 * Sets User password.
	 * @param password User password.
	 */
	public void setPassword(String password) {
		this.mPassword = password;
	}

	public String getFirstName() {
		return mFirstName;
	}

	public void setFirstName(String firstName) {
		this.mFirstName = firstName;
	}

	public String getLastName() {
		return mLastName;
	}

	public void setLastName(String lastName) {
		this.mLastName = lastName;
	}

	/**
	 * Gets user first name and last name as a single strung.
	 * @return user first name and last name as a single strung.
     */
	public String getName() {
		StringBuilder builder = new StringBuilder();
		if (mFirstName != null)
			builder.append(mFirstName);
		if (mLastName != null) {
			if (mFirstName != null)
				builder.append(" ");
			builder.append(mLastName);
		}
		return builder.toString();
	}

	/**
	 * Check if User is active. Inactive User can't do purchases.
	 * @return True if User is active, false otherwise.
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * Sets if user is active. Inactive User can't do purchases.
	 * @param isActive True if User is active, false otherwise.
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * Gets registration transaction ID. Status of this transaction should help to know reasons whu user is inactive.
	 * @return Registration transaction ID.
	 */
	public String getRegistrationId() {
		return mRegistrationId;
	}

	/**
	 * Gets user ID.
	 * @return User ID.
	 */
	public String getUserId() {
		return mUserId;
	}

	/**
	 * Sets registration transaction ID.
	 * @param registrationId Registration transaction ID.
	 */
	public void setRegistrationId(String registrationId) {
		this.mRegistrationId = registrationId;
	}

	/**
	 * Sets user ID.
	 * @param userId Registration transaction ID.
	 */
	public void setUserId(String userId) {
		mUserId = userId;
	}


	/**
	 * Check if for getting User object was used temporary password.
	 * @return True if temporary password was used, false otherwise.
	 */
	public boolean isTempPasswordUsed() {
		return isTempPasswordUsed;
	}

	/**
	 * Sets if for getting User object was used temporary password.
	 * @param isTempPasswordUsed True if temporary password was used, false otherwise.
	 */
	public void setTempPasswordUsed(boolean isTempPasswordUsed) {
		this.isTempPasswordUsed = isTempPasswordUsed;
	}

	/**
	 * Gets User language code. All responses will translated if server support this language.
	 * @return User language code.
	 */
	public String getLanguage() {
		return mLanguage;
	}

	/**
	 * Sets User language code. All responses wil translated if server support this language.
	 * @param lnguage User language code.
	 */
	public void setLanguage(String lnguage) {
		this.mLanguage = lnguage;
	}

	public String getTimezone() {
		return mTimezone;
	}

	public void setTimezone(String mTimezone) {
		this.mTimezone = mTimezone;
	}

	public String getCountry() {
		return mCountry;
	}

	public void setCountry(String mCountry) {
		this.mCountry = mCountry;
	}

	public String getState() {
		return mState;
	}

	public void setState(String mState) {
		this.mState = mState;
	}

	public String getCity() {
		return mCity;
	}

	public void setCity(String mCity) {
		this.mCity = mCity;
	}

	public String getAddress1() {
		return mAddress1;
	}

	public void setAddress1(String mAddress1) {
		this.mAddress1 = mAddress1;
	}

	public String getAddress2() {
		return mAddress2;
	}

	public void setAddress2(String mAddress2) {
		this.mAddress2 = mAddress2;
	}

	public String getZip() {
		return mZip;
	}

	public void setZip(String mZip) {
		this.mZip = mZip;
	}

	public String getPhone() {
		return mTelephone;
	}

	public void setPhone(String mTelephone) {
		this.mTelephone = mTelephone;
	}

	public String getSsn() {
		return mSsn;
	}

	public void setSsn(String mSsn) {
		this.mSsn = mSsn;
	}
	
	public Boolean isReceipt() {
		return isReceipt;
	}

	public void setReceipt(Boolean isReceipt) {
		this.isReceipt = isReceipt;
	}

	/**
	 * Sets User Payment Accounts list.
	 * @param list User Payment Accounts list.
	 */
	public void setAccountsList(List<UserAccount> list) {
		mAccounts = list;
	}

	/**
	 * Gets User Payment Accounts list.
	 * @return User Payment Accounts list.
	 */
	public List<UserAccount> getAccountsList() {
		return mAccounts;
	}

	public UserAccount getAccount(String type, String number) {
		if (type != null) {
			if (number != null) {
				for (UserAccount account : getAccountsList()) {
					if (type.equals(account.getType()) && number.equals(account.getNumber()))
						return account;
				}
			}
			else {
				for (UserAccount account : getAccountsList()) {
					if (type.equals(account.getType()))
						return account;
				}
			}
		}
		return null;
	}

    /**
     * Sets versions of "Terms of Use" which user agree.
     * @param versions versions of Terms of Use.
     */
    public void setTouVersions(HashMap<String, HashMap<String, TouVersion>> versions) {
        mTouVersions = versions;
    }

	// TODO write docs (No parameters)
	/**
	 * Sets version of "Terms of Use" which user agree.
	 */
	public void setTouVersion(String appId, String touUid, TouVersion version) {
		if (mTouVersions == null)
			mTouVersions = new HashMap<>();

		HashMap<String, TouVersion> appTou = mTouVersions.get(appId);
		if (appTou == null) {
			appTou = new HashMap<>();
			mTouVersions.put(appId, appTou);
		}

		appTou.put(touUid, version);
	}

	/**
	 *
	 * Gets version of "Terms of Use" which user agree.
	 * @return Tou version reccord.
	 */
	public TouVersion getTouVersion(String appId) {
		return getTouVersion(appId, KEY_DEFAULT);
	}

	/**
	 *
	 * Gets version of "Terms of Use" which user agree.
	 * @return Tou version reccord.
	 */
    public TouVersion getTouVersion(String appId, String key) {
		if (mTouVersions != null) {
			Map<String, TouVersion> versions = mTouVersions.get(appId);
			if (versions != null) {
				return versions.get(key);
			}
		}
		return null;
    }

	// TODO write docs (No parameters)
	/**
	 * Check if last accepted version of "Terms of Use" for specified application is lower that minimal version from parameter.
	 * @return True if ToU for specified application is not accepted or it's version lover that minimal version from parameter, false otherwise.
	 */
	public boolean isTouVersionObsolete(AppInfo appInfo, String touUid) {
		return appInfo != null &&
				isTouVersionObsolete(appInfo.getId(), touUid, appInfo.getTouInfo(touUid));
	}

	/**
	 * Check if last accepted version of "Terms of Use" for specified application is lower that minimal version from parameter.
	 * @param appInfo Application info.
	 * @return True if ToU for specified application is not accepted or it's version lover that minimal version from parameter, false otherwise.
	 */
	public boolean isTouVersionObsolete(AppInfo appInfo) {
		return isTouVersionObsolete(appInfo, KEY_DEFAULT);

	}

	// TODO write docs (No parameters)
	/**
	 * Check if last accepted version of "Terms of Use" for specified application is lower that minimal version from parameter.
	 * @return True if ToU for specified application is not accepted or it's version lover that minimal version from parameter, false otherwise.
	 */
	public boolean isTouVersionObsolete(String appId, String touUid, AppInfo.TouInfo touInfo) {
		if (touInfo == null || appId == null|| touInfo.version == 0)
			return false;

		TouVersion touVersion = getTouVersion(appId, touUid);
		return touVersion == null || touVersion.version < touInfo.version;
	}

    /**
     * Check if Operator billing is enabled for current user.
     * 
     * @return true if Operator billing is enabled for current user, false otherwise.
	 * @see UserAccount.Type#OPERATOR
     */
    public boolean isOperatorBillingEnabled() {
        return isOperatorBillingEnabled;
    }

    /**
     * Sets if Operator billing enabled.
     * 
     * @param isOperatorBillingEnabled
     *          true if Operator billing is enabled, false otherwise.
	 * @see UserAccount.Type#OPERATOR
     */
    public void setOperatorBillingEnabled(boolean isOperatorBillingEnabled) {
        this.isOperatorBillingEnabled = isOperatorBillingEnabled;
    }

    /**
     * Check if user has set an address.
     *
     * @return True if user has set an address, false otherwise.
     */
	public boolean isHasAddress() {
		return mCountry != null && !"".equals(mCountry) &&
				mCity != null && !"".equals(mCity) &&
				mAddress1 != null && !"".equals(mAddress1) &&
				mZip != null && !"".equals(mZip);
	}

    /**
     * Check if user has set any visible account.
     *
     * @return True if user has set any visible account, false otherwise.
     */
    public boolean isHasAnyAccount() {
        for (UserAccount account: mAccounts) {
            if (account != null && !account.isHidden()) {
                return true;
            }
        }
        return false;
    }
	
	@Override
	public void applyTo(Message<?> message) {
		message.addParameters(toParams());
	}
	
	@Override
	public Map<String, Object> toParams() {
		Map<String, Object> params = new HashMap<>();

		applyIfNotNull(params, Message.Param.USER_EMAIL, getLogin());
		applyIfNotNull(params, Message.Param.USER_PASSWORD, getPassword());
		applyIfNotNull(params, "consumer_str_firstname", getFirstName());
		applyIfNotNull(params, "consumer_str_lastname", getLastName());
		applyIfNotNull(params, "consumer_str_telephone", getPhone());

		applyIfNotNull(params, "consumer_str_timezone", getTimezone());
		applyIfNotNull(params, "consumer_str_country", getCountry());
		applyIfNotNull(params, "consumer_str_city", getCity());
		applyIfNotNull(params, "consumer_str_address1", getAddress1());
		applyIfNotNull(params, "consumer_str_address2", getAddress2());
		applyIfNotNull(params, "consumer_str_zip", getZip());
		applyIfNotNull(params, "consumer_str_ssn", getSsn());
		applyIfNotNull(params, "consumer_bool_receipt", isReceipt());
		applyIfNotNull(params, "consumer_str_language", getLanguage());
		
		return params;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (obj == null)
			return false;

		if (getClass() != obj.getClass())
			return false;

		User user = (User) obj;
		return TextUtils.equals(mLogin, user.mLogin) &&
				TextUtils.equals(mPassword, user.mPassword) &&
				TextUtils.equals(mFirstName, user.mFirstName) &&
				TextUtils.equals(mLastName, user.mLastName) &&
				TextUtils.equals(mTelephone, user.mTelephone) &&
				TextUtils.equals(mSsn, user.mSsn) &&
				TextUtils.equals(mLanguage, user.mLanguage) &&
				TextUtils.equals(mTimezone, user.mTimezone) &&
				TextUtils.equals(mAddress1, user.mAddress1) &&
				TextUtils.equals(mAddress2, user.mAddress2) &&
				TextUtils.equals(mZip, user.mZip) &&
				TextUtils.equals(mCity, user.mCity) &&
				TextUtils.equals(mState, user.mState) &&
				TextUtils.equals(mCountry, user.mCountry) &&
				((isReceipt != null && isReceipt.equals(user.isReceipt)) ||
						(isReceipt == null && user.isReceipt == null)) &&
				isActive == user.isActive &&
				isTempPasswordUsed == user.isTempPasswordUsed &&
				isOperatorBillingEnabled == user.isOperatorBillingEnabled &&
				TextUtils.equals(mRegistrationId, user.mRegistrationId) &&
				TextUtils.equals(mUserId, user.mUserId) &&
				((mAccounts != null && mAccounts.equals(user.mAccounts)) ||
						(mAccounts == null && user.mAccounts == null)) &&
				((mTouVersions != null && mTouVersions.equals(user.mTouVersions)) ||
						(mTouVersions == null && user.mTouVersions == null)) &&
				((mDict != null && mDict.equals(user.mDict)) ||
						(mDict == null && user.mDict == null)) &&
				((mInvoicers != null && mInvoicers.equals(user.mInvoicers)) ||
						(mInvoicers == null && user.mInvoicers == null));
	}

	// =============================================================================================
	// Deprecated

	@Deprecated
	public void setDictValue(String key, String value) {
		mDict.put(key, value);
	}

	@Deprecated
	public String getDictValue(String key) {
		return mDict.get(key);
	}

	@Deprecated
	public Set<String> getDictKeys() {
		return mDict.keySet();
	}

	@Deprecated
	public void addInvoicer(Invoicer invoicer) {
		mInvoicers.put(invoicer.getId(), invoicer);
	}

	@Deprecated
	public Invoicer getInvoicer(String id) {
		return mInvoicers.get(id);
	}

	@Deprecated
	public Set<String> getInvoicerKeys() {
		return mInvoicers.keySet();
	}
}
