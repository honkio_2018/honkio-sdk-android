package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Event;
import com.riskpointer.sdk.api.model.entity.Schedule;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.ScheduleFilter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Shurygin Denis on 2016-05-31.
 */
public class ScheduleJsonParser extends BaseJsonResponseParser<Schedule> {

    @Override
    protected Schedule parseJson(Message<?> message, JSONObject json) {
        ArrayList<Event> list = new ArrayList<>();
        JSONArray eventsJsonArray = json.optJSONArray("schedule");
        if (eventsJsonArray != null) {
            for (int i = 0; i < eventsJsonArray.length(); i++) {
                JSONObject eventJson = eventsJsonArray.optJSONObject(i);
                if (eventJson != null) {
                    Event event = new Event();
                    event.setType(eventJson.optString("type", Event.Type.TENTATIVE));
                    event.setStart(ApiFormatUtils.stringDateToLong(eventJson.optString("start")));
                    event.setEnd(ApiFormatUtils.stringDateToLong(eventJson.optString("end")));

                    ArrayList<Map<String, Object>> metadataArray = new ArrayList<>();
                    event.setMetaData(metadataArray);

                    JSONArray shopsJson = eventJson.optJSONArray("shops");
                    if (shopsJson != null) {
                        for (int j = 0; j < shopsJson.length(); j++) {
                            JSONObject shopJson = shopsJson.optJSONObject(j);
                            if (shopJson != null) {
                                Map<String, Object> metadata;
                                JSONObject metadataJson = shopJson.optJSONObject("metadata");
                                if (metadataJson != null)
                                    metadata = jsonToMap(metadataJson);
                                else
                                    metadata = new HashMap<>(1);
                                metadata.put(Event.MetaData.SHOP, shopJson.optString("shop"));
                                metadataArray.add(metadata);
                            }
                        }
                    }

                    list.add(event);
                }
            }
        }

        Schedule schedule = new Schedule();
        schedule.setList(list);

        Object nullParameter = message.getParameter(null);
        if(nullParameter instanceof ScheduleFilter)
            schedule.setFilter((ScheduleFilter) nullParameter);

        return schedule;
    }
}
