package com.riskpointer.sdk.api.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.riskpointer.sdk.api.web.response.Response;

/**
 * @author Shurygin Denis
 */

public abstract class CompleteActionReceiver<Type> extends BroadcastReceiver {

    public abstract void onReceive(Context context, Intent intent, Response<Type> response);

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.hasExtra(BroadcastHelper.BROADCAST_EXTRA_RESPONSE)) {
            Response<Type> response = ((Response<Type>)
                    intent.getSerializableExtra(BroadcastHelper.BROADCAST_EXTRA_RESPONSE));

            onReceive(context, intent, response);
        }
    }
}
