package com.riskpointer.sdk.api.web.webtask;

import android.content.Context;
import android.text.TextUtils;

import com.riskpointer.sdk.api.web.message.Message;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.internal.Util;
import okio.BufferedSink;
import okio.Okio;
import okio.Source;

/**
 * Created by Shurygin Denis on 2015-12-23.
 */
public class MultiPartWebTask<ResponseResult> extends SimpleWebTask<ResponseResult> {
    public static final MediaType MEDIA_TYPE_ALL
            = MediaType.parse("*/*");

    public static final MediaType MEDIA_TYPE_IMAGE_PNG
            = MediaType.parse("image/png");

    public static final MediaType MEDIA_TYPE_IMAGE_JPEG
            = MediaType.parse("image/jpeg");

    private File mFile;
    private InputStream mInputStream;
    private MediaType mMediaType;

    /**
     * Construct new WebServiceAsync.
     *
     * @param context Application Context.
     * @param message Message object.
     */
    public MultiPartWebTask(Context context, Message<ResponseResult> message, File file) {
        super(context, message);
        mFile = file;
    }

    /**
     * Construct new WebServiceAsync.
     *
     * @param context Application Context.
     * @param message Message object.
     */
    public MultiPartWebTask(Context context, Message<ResponseResult> message, InputStream inputStream, MediaType mediaType) {
        super(context, message);
        mInputStream = inputStream;
        mMediaType = mediaType;
    }

    @Override
    RequestBody buildRequestBody(String jsonString) {
        MultipartBody.Builder requestBodyBuilder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("message", "message.json", super.buildRequestBody(jsonString));
        if (mFile != null) {
            requestBodyBuilder.addFormDataPart("file", mFile.getName(), RequestBody.create(mMediaType != null ? mMediaType : MEDIA_TYPE_IMAGE_JPEG, mFile));
        }
        else if (mInputStream != null){
            MediaType mediaType = mMediaType != null ? mMediaType : MEDIA_TYPE_ALL;
            String extension = mediaType.subtype();
            if (TextUtils.isEmpty(extension) || extension.contains("*"))
                extension = "";
            else
                extension = "." + extension;

            requestBodyBuilder.addFormDataPart("file", "file" + extension,
                    new InputStreamRequestBody(mInputStream, mediaType));
        }
        return requestBodyBuilder.build();
    }

    @Override
    public MultiPartWebTask<ResponseResult> clone() {
        if (mFile != null)
            return new MultiPartWebTask<>(mContext, getMessage(), mFile);
        return new MultiPartWebTask<>(mContext, getMessage(), mInputStream, mMediaType);
    }

    private static class InputStreamRequestBody extends RequestBody {

        private MediaType mMediaType;
        private InputStream mInputStream;

        public InputStreamRequestBody(InputStream inputStream, MediaType mediaType) {
            super();
            mInputStream = inputStream;
            mMediaType = mediaType;
        }

        @Override
        public MediaType contentType() {
            return mMediaType != null ? mMediaType : MEDIA_TYPE_ALL;
        }

        @Override
        public long contentLength() {
            try {
                return mInputStream.available();
            } catch (IOException e) {
                return 0;
            }
        }

        @Override
        public void writeTo(BufferedSink sink) throws IOException {
            Source source = null;
            try {
                source = Okio.source(mInputStream);
                sink.writeAll(source);
            } finally {
                Util.closeQuietly(source);
            }
        }
    }
}
