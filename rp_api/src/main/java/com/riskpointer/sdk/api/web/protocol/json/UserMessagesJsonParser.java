package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.ChatMessage;
import com.riskpointer.sdk.api.model.entity.ChatMessages;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.ChatMessageFilter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Shurygin Denis on 2016-05-11.
 */
public class UserMessagesJsonParser extends BaseJsonResponseParser<ChatMessages> {

    @Override
    protected ChatMessages parseJson(Message<?> message, JSONObject json) {
        ChatMessages result = new ChatMessages();

        Object nullParameter = message.getParameter(null);
        if(nullParameter instanceof ChatMessageFilter)
            result.setFilter((ChatMessageFilter) nullParameter);

        ArrayList<ChatMessage> list = new ArrayList<>();

        JSONArray messagesJsonArray = json.optJSONArray("messages");
        if(messagesJsonArray != null) {
            for (int i = 0; i < messagesJsonArray.length(); i++) {
                JSONObject messagesJson = messagesJsonArray.optJSONObject(i);
                if (messagesJson != null) {
                    list.add(UserMessagesJsonParser.parseMessage(messagesJson));
                }
            }
        }

        result.setList(list);

        return result;
    }

    public static ChatMessage parseMessage(JSONObject json) {
        ChatMessage result = new ChatMessage();

        result.setTime(ApiFormatUtils.stringDateToLong(json.optString("timestamp")));
        result.setSenderId(json.optString("sender_id"));
        result.setSenderRole(json.optString("sender_role_id"));
        result.setSenderName(json.optString("sender_name"));
        result.setOrderThirdPerson(json.optString("order_third_person"));
        result.setSenderFullName(json.optString("sender_first_name"),json.optString("sender_last_name"));
        result.setReceiverId(json.optString("receiver_id"));
        result.setReceiverRole(json.optString("receiver_role_id"));
        result.setReceiverName(json.optString("receiver_name"));
        result.setOrderId(json.optString("order_id"));
        result.setText(json.optString("text"));
        result.setShopId(json.optString("shop_id"));
        result.setShopName(json.optString("shop_name"));

        return result;
    }
}
