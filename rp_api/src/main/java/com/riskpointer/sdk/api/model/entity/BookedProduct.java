/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;

/**
 * BookedProduct used for purchasing. Is a data object which contains information about product for purchase.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 * 
 * @author Denis Shurygin
 *
 */
public class BookedProduct implements Serializable {
	
	private Product mProduct;
	private int mCount;

	/**
	 * Public constructor without parameters. All fields equal null.
	 */
	public BookedProduct() {
	}
	
	/**
	 * Public constructor.
	 * @param product Product for booking.
	 * @param count Count of the products.
	 */
	public BookedProduct(Product product, int count) {
		mProduct = product;
		mCount = count;
	}

	public BookedProduct(String productId, int count) {
		mProduct = new Product();
		mProduct.setId(productId);
		mCount = count;
	}

	public BookedProduct(BookedProduct bookedProduct) {
		mProduct = new Product(bookedProduct.mProduct);
		mCount = bookedProduct.mCount;
	}

	/**
     * Gets booked product.
	 * @return Booked product.
	 */
	public Product getProduct() {
		return mProduct;
	}

	/**
	 * Sets product for booking.
	 * @param product Booked product.
	 */
	public void setProduct(Product product) {
		this.mProduct = product;
	}

	/**
     * Gets count of the products.
	 * @return Count of the products.
	 */
	public int getCount() {
		return mCount;
	}

	/**
	 * Sets count of the products.
	 * @param count Count of the products.
	 */
	public void setCount(int count) {
		this.mCount = count;
	}
	
	/**
	 * Calculate price of the product for specified account type. To original price
	 * will be added surplus for selected account.
	 * 
	 * @param accountType Account that will used for surplus calculation.
	 * @return Total price for this product.
	 */
	@Deprecated
	public double getPrice(String accountType) {
		return mCount * (mProduct.getAmount() + mProduct.getSupplusFor(accountType));
	}

	/**
	 * Calculate price of the product.
	 *
	 * @return Total price for this product.
	 */
	public double getPrice() {
		if (mProduct != null)
			return mCount * mProduct.getAmount();
		return 0;
	}
}