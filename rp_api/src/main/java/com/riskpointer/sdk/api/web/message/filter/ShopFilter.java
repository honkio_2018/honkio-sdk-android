/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.message.filter;

import com.riskpointer.sdk.api.BaseHonkioApi;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.message.Message;

import java.util.HashMap;

/**
 * Filter for shopFind request.
 *
 * @see BaseHonkioApi#shopFind(ShopFilter, HashMap, int, RequestCallback)
 * @see BaseHonkioApi#shopFind(ShopFilter, int, RequestCallback)
 * @see BaseHonkioApi#shopFindById(String, int, RequestCallback)
 * @see BaseHonkioApi#shopFindByQrId(String, int, RequestCallback)
 *
 * @author Denis Shurygin
 */
public class ShopFilter extends ListFilter {
	private static final long serialVersionUID = 7455361221430531976L;
	
	private String mServiceType;
	private String mMerchantId;
	private String mNetworkId;
    private String mLatitude;
    private String mLongitude;
    private String mRadius;
	private Boolean mQuerySubMerchants;
	private Boolean mQueryAllMerchants;
	private Boolean mQueryShowRatings;

	/**
	 * Gets Shop service type specified for the filter.
	 * @return Shop service type specified for the filter.
	 * @see Shop.ServiceType
	 */
	public String getServiceType() {
		return mServiceType;
	}

	/**
	 * Sets Shop service type. Only shops with the specified service type will be found.
	 * @param serviceType Shop service type
	 * @see Shop.ServiceType
	 */
	public void setServiceType(String serviceType) {
		this.mServiceType = serviceType;
	}

	/**
	 * Gets Merchant Id specified for the filter.
	 * @return Merchant Id specified for the filter.
	 */
	public String getMerchantId() {
		return mMerchantId;
	}

	/**
	 * Sets Merchant Id. Only shops of the specified merchant will be found.
	 * @param id Merchant Id
	 */
	public void setMerchantId(String id) {
		this.mMerchantId = id;
	}

	/**
	 * Gets Network Id specified for the filter.
	 * @return Network Id specified for the filter.
	 */
	public String getNetworkId() {
		return mNetworkId;
	}

	/**
	 * Sets Network Id. Only shops from the specified Network will be found.
	 * @param id Network Id
	 */
	public void setNetworkId(String id) {
		this.mNetworkId = id;
	}
    
    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String mLatitude) {
        this.mLatitude = mLatitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String mLongitude) {
        this.mLongitude = mLongitude;
    }

	/**
	 * Gets radius of the search.
	 * @return Radius of the search.
	 */
    public String getRadius() {
        return mRadius;
    }

	/**
	 * Sets radius of the search.
	 * @param radius Radius of the search.
	 */
    public void setRadius(String radius) {
        this.mRadius = radius;
    }

	// TODO write docs
	public Boolean isQuerySubMerchants() {
		return mQuerySubMerchants != null && mQuerySubMerchants;
	}

	// TODO write docs
	public void setQuerySubMerchants(Boolean querySubMerchants) {
		mQuerySubMerchants = querySubMerchants;
	}

	// TODO write docs
	public Boolean isQueryAllMerchants() {
		return mQueryAllMerchants != null && mQueryAllMerchants;
	}

	// TODO write docs
	public void setQueryAllMerchants(Boolean querySubMerchants) {
		mQueryAllMerchants = querySubMerchants;
	}

	@Override
	public HashMap<String, Object> toParams() {
		HashMap<String, Object> params = super.toParams();

		AbsFilter.applyIfNotNull(params, Message.Param.SHOP_SERVICE_TYPE, getServiceType());
		AbsFilter.applyIfNotNull(params, Message.Param.SHOP_QUERY_MERCHANT, getMerchantId());
		AbsFilter.applyIfNotNull(params, Message.Param.SHOP_QUERY_SUBMERCHANTS, mQuerySubMerchants);
		AbsFilter.applyIfNotNull(params, Message.Param.SHOP_QUERY_ALL_MERCHANTS, mQueryAllMerchants);
		AbsFilter.applyIfNotNull(params, Message.Param.SHOP_QUERY_SHOW_RATINGS, mQueryShowRatings);
        AbsFilter.applyIfNotNull(params, Message.Param.SHOP_NETWORK_ID, getNetworkId());
        AbsFilter.applyIfNotNull(params, Message.Param.LOC_LATITUDE, getLatitude());
        AbsFilter.applyIfNotNull(params, Message.Param.LOC_LONGITUDE, getLongitude());
        applyOrRemove(params, Message.Param.LOC_ACCURACY, getRadius());
        
		return params;
	}

	public Boolean isQueryShowRatings() {
		return mQueryShowRatings != null && mQueryShowRatings;
	}

	public void setQueryShowRatings(Boolean queryShowRatings) {
		mQueryShowRatings = queryShowRatings;
	}
}
