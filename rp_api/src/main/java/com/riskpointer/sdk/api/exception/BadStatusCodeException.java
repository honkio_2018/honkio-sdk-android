/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.exception;

/**  
 * Thrown when response code is not 200.
 * 
 * @author Denis Shurygin
 *
 */
public class BadStatusCodeException extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new {@code BadStatusCodeException} that includes the current stack trace.
	 */
	public BadStatusCodeException() {
		super();
	}

	/**
	 * Constructs a new {@code BadStatusCodeException} with the current stack trace and the
	 * specified detail message.
	 *
	 * @param message
	 *            the detail message for this exception.
	 */
	public BadStatusCodeException(String message) {
		super(message);
	}
}
