package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Shurygin Denis
 */

public class HkStructureList implements Serializable {

    private ArrayList<HkStructure> mList;

    public ArrayList<HkStructure> getList() {
        return mList;
    }

    public void setList(ArrayList<HkStructure> list) {
        this.mList = list;
    }
}
