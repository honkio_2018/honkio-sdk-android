package com.riskpointer.sdk.api.web.message.filter;

import java.util.HashMap;

/**
 * @author Shurygin Denis
 */

public class ShopProductsFilter extends ListFilter {

    private String[] mTags;
    private Boolean mIsParent;
    private Boolean mIsBonusProducts;
    private String mProductId;

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = super.toParams();

        AbsFilter.applyIfNotNull(params, "query_tags", mTags);
        AbsFilter.applyIfNotNull(params, "query_id", mProductId);
        AbsFilter.applyIfNotNull(params, "query_parent", mIsParent);
        AbsFilter.applyIfNotNull(params, "query_bonus_products", mIsBonusProducts);

        return params;
    }


    public String[] getTags() {
        return mTags;
    }

    public void setTags(String[] mTags) {
        this.mTags = mTags;
    }

    public Boolean isParent() {
        return mIsParent;
    }

    public void setParent(Boolean parent) {
        mIsParent = parent;
    }

    public Boolean isBonusProducts() {
        return mIsBonusProducts;
    }

    public void setBonusProducts(Boolean bonusProducts) {
        mIsBonusProducts = bonusProducts;
    }

    public String getProductId() {
        return mProductId;
    }

    public void setProductId(String productId) {
        mProductId = productId;
    }
}
