package com.riskpointer.sdk.api.model.push.entity;

import com.riskpointer.sdk.api.model.push.Push;

import java.io.Serializable;

/**
 * Created by Shurygin Denis on 2016-10-19.
 */
public class OrderChatMessagePushEntity extends Push.PushEntity implements Serializable {

    private long mTime;
    private String mSenderId;
    private String mSenderRole;
    private String mReceiverId;
    private String mReceiverRole;
    private String mOrderId;
    private String mText;
    private String mOrderThirdPerson;
    private String mSenderFirstName;
    private String mSenderLastName;
    private String mShopName;
    private String mShopId;

    public OrderChatMessagePushEntity() {
        super(Push.Type.CHAT_MESSAGE);
    }

    public String getSenderId() {
        return mSenderId;
    }

    public void setSenderId(String senderId) {
        this.mSenderId = senderId;
    }

    public String getSenderRole() {
        return mSenderRole;
    }

    public void setSenderRole(String senderRole) {
        this.mSenderRole = senderRole;
    }

    public String getReceiverId() {
        return mReceiverId;
    }

    public void setReceiverId(String receiverId) {
        this.mReceiverId = receiverId;
    }

    public String getReceiverRole() {
        return mReceiverRole;
    }

    public void setReceiverRole(String receiverRole) {
        this.mReceiverRole = receiverRole;
    }

    public String getOrderId() {
        return mOrderId;
    }

    public void setOrderId(String orderId) {
        this.mOrderId = orderId;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        this.mText = text;
    }

    public String getThirdPerson() {
        return mOrderThirdPerson;
    }

    public void setOrderThirdPerson(String thirdPerson) {
        this.mOrderThirdPerson = thirdPerson;
    }

    public String getSenderFirstName() {
        return mSenderFirstName;
    }

    public void setSenderFirstName(String name) {
        this.mSenderFirstName = name;
    }

    public String getSenderLastName() {
        return mSenderLastName;
    }

    public void setSenderLastName(String name) {
        this.mSenderLastName = name;
    }

    public String getShopName() {
        return mShopName;
    }

    public void setShopName(String name) {
        this.mShopName = name;
    }

    public String getShopId() {
        return mShopId;
    }

    public void setShopId(String id) {
        this.mShopId = id;
    }

    public long getTime() {
        return mTime;
    }

    public void setTime(long time) {
        this.mTime = time;
    }

    @Override
    public String getUid() {
        String orderId = getOrderId();
        return "null|" + (orderId != null ? orderId : "null");
    }
}
