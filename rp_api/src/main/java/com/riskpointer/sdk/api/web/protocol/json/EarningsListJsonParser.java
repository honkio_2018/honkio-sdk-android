package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Earnings;
import com.riskpointer.sdk.api.model.entity.EarningsList;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Parse Earnings list from JSON.
 */
public class EarningsListJsonParser extends BaseJsonResponseParser<EarningsList> {

    @Override
    public EarningsList parseJson(Message<?> message, JSONObject json) {
        EarningsList earningsList = new EarningsList();
        JSONObject earningsJSON = json.optJSONObject("earnings");
        if (earningsJSON != null) {
            ArrayList<Earnings> list = new ArrayList<>();
            JSONArray earningsListJSON = earningsJSON.optJSONArray("earnings_list");
            if (earningsListJSON != null) {
                for (int i = 0; i < earningsListJSON.length(); i++) {
                    JSONObject object = earningsListJSON.optJSONObject(i);
                    if (object != null) {
                        Earnings earnings = EarningsJsonParser.parseEarnings(object);
                        if (earnings != null)
                            list.add(earnings);
                    }
                }
            }
            earningsList.setList(list);
            JSONObject jsonObject = earningsJSON.optJSONObject("last_event");
            if (jsonObject != null) {
                Earnings earnings = EarningsJsonParser.parseEarnings(jsonObject);
                if (earnings != null)
                    earningsList.setLastEventEarnings(earnings);
            }
        }
        return earningsList;
    }
}
