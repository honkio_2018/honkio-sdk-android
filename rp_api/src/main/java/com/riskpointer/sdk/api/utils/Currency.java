package com.riskpointer.sdk.api.utils;

/**
 * Special class to work with currency.
 */
public final class Currency {
    /** Currency Id: Euro. */
    public static final String EUR = "EUR";
    /** Currency Id: USA Dollar. */
    public static final String USD = "USD";

    private Currency() {
    }

    /**
     * Convert Currency Id to the currency symbol.
     * @param currency Currency Id.
     * @return Currency symbol.
     */
    public static String getSymbol(String currency) {
        if (currency != null) {
            if (EUR.equals(currency))
                return "\u20AC";
            if (USD.equals(currency))
                return "$";
        }
        return currency;
    }
}
