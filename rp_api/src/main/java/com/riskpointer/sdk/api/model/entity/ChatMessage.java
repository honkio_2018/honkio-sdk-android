package com.riskpointer.sdk.api.model.entity;

import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.AbsFilter;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Shurygin Denis on 2016-05-11.
 */
public class ChatMessage implements Serializable {

    private long mTime;
    private String mSenderId;
    private String mSenderName;
    private String mSenderFullName;
    private String mSenderRole;
    private String mReceiverId;
    private String mReceiverName;
    private String mReceiverRole;
    private String mOrderId;
    private String mText;
    private String mOrderThirdPerson;
    private String mShopName;
    private String mShopId;

    public void applyTo(Message<?> message) {
        message.addParameters(toParams());
    }

    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = new HashMap<>();

        AbsFilter.applyIfNotNull(params, "sender_role_id", mSenderRole);
        AbsFilter.applyIfNotNull(params, "receiver_id", mReceiverId);
        AbsFilter.applyIfNotNull(params, "receiver_role_id", mReceiverRole);
        AbsFilter.applyIfNotNull(params, "order_id", mOrderId);
        AbsFilter.applyIfNotNull(params, "text", mText);

        return params;
    }

    public String getSenderId() {
        return mSenderId;
    }

    public void setSenderId(String senderId) {
        this.mSenderId = senderId;
    }

    public String getSenderName() {
        return mSenderName;
    }

    public void setSenderName(String senderName) {
        this.mSenderName = senderName;
    }

    public String getSenderFullName() {
        return mSenderFullName;
    }

    public void setSenderFullName(String firstName, String lastName) {
        this.mSenderFullName = (firstName != null ? firstName : "") + " " + (lastName != null ? lastName : "");
    }

    public String getSenderRole() {
        return mSenderRole;
    }

    public void setSenderRole(String senderRole) {
        this.mSenderRole = senderRole;
    }

    public String getReceiverId() {
        return mReceiverId;
    }

    public void setReceiverId(String receiverId) {
        this.mReceiverId = receiverId;
    }

    public String getReceiverName() {
        return mReceiverName;
    }

    public void setReceiverName(String receiverName) {
        this.mReceiverName = receiverName;
    }

    public String getReceiverRole() {
        return mReceiverRole;
    }

    public void setReceiverRole(String receiverRole) {
        this.mReceiverRole = receiverRole;
    }

    public String getOrderId() {
        return mOrderId;
    }

    public void setOrderId(String orderId) {
        this.mOrderId = orderId;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        this.mText = text;
    }

    public String getThirdPerson() {
        return mOrderThirdPerson;
    }

    public void setOrderThirdPerson(String thirdPerson) {
        this.mOrderThirdPerson = thirdPerson;
    }

    public String getShopName() {
        return mShopName;
    }

    public void setShopName(String mShopName) {
        this.mShopName = mShopName;
    }

    public String getShopId() {
        return mShopId;
    }

    public void setShopId(String mShopId) {
        this.mShopId = mShopId;
    }

    public long getTime() {
        return mTime;
    }

    public void setTime(long time) {
        this.mTime = time;
    }
}