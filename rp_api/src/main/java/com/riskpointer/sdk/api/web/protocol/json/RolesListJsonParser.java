package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.RolesList;
import com.riskpointer.sdk.api.model.entity.UserRole;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Parse users list from JSON.
 */
public class RolesListJsonParser extends BaseJsonResponseParser<RolesList> {

    @Override
    public RolesList parseJson(Message<?> message, JSONObject json) {
        RolesList userList = new RolesList();
        ArrayList<UserRole> list = new ArrayList<>();

        JSONArray userListJson = json.optJSONArray("roles");
        if (userListJson != null) {
            for (int i = 0; i < userListJson.length(); i++) {
                JSONObject userJson = userListJson.optJSONObject(i);
                if (userJson != null) {
                    UserRole user = UserRoleJsonParser.parseRole(userJson);
                    if (user != null)
                        list.add(user);
                }
            }
        }
        userList.setList(list);
        return userList;
    }

}
