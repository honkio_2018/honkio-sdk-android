/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web;

import android.content.SharedPreferences;

import com.riskpointer.sdk.api.exception.WrongKeyException;
import com.riskpointer.sdk.api.utils.ApiCryptoUtils;

import java.util.concurrent.Semaphore;

import javax.crypto.Cipher;

/**
 * Class designed for holding authentication information.
 * As storage used SharedPreferences file.
 * To store OTP required PIN code which used for encryption and decryption.
 */
public class Connection {

	/** Maximum amount of PIN enter fails. */
	public static final long MAX_PIN_FAIL_COUNT = 3;

	/** Key to store user login into preferences file. */
	public static final String LOGIN = "com.riskpointer.Connection.LOGIN";
	/** Key to store encrypted OTP into preferences file. */
	public static final String PASSWORD = "com.riskpointer.Connection.PASSWORD";
	/** Key to store PIN fails count into preferences file. */
	public static final String PIN_FAILS = "com.riskpointer.Connection.PIN_FAILS";

	private final Semaphore mSemaphore = new Semaphore(1);

	private String mLogin;
	private String mPassword;
	private String mOtp;
	private String mPin;
	private long mPinFailsCount;
	private boolean isValid = true;
	private boolean isEncrypted = false;
	
	private SharedPreferences mApiPreferences;

	/**
	 * Construct new Connection with given Login and password.
	 * @param apiPreferences Shared Preferences to store login and password.
	 * @param login User login.
	 * @param password User password.
	 * @param pin PIN code. Will used to encrypt/decrypt OTP.
	 */
	public Connection(SharedPreferences apiPreferences, String login, String password, String pin) {
		mApiPreferences = apiPreferences;
		mPin = pin;
		mPinFailsCount = 0;
		mLogin = login;
		mPassword = password;
	}

	/**
	 * Construct new Connection. Login and password will get from Shared Preferences file.
	 * @param apiPreferences Shared Preferences to store login and password.
	 * @param pin PIN code. Will used to encrypt/decrypt OTP.
	 * @throws WrongKeyException If PIN code is invalid.
	 */
	public Connection(SharedPreferences apiPreferences, String pin) throws WrongKeyException {
		mApiPreferences = apiPreferences;
		mPin = pin;
		mPinFailsCount = mApiPreferences.getLong(PIN_FAILS, 0);
		mLogin = mApiPreferences.getString(LOGIN, "");
		isEncrypted = (pin == null);
		try {
			mPassword = decrypt(mApiPreferences.getString(PASSWORD, ""));
		} catch (WrongKeyException exception) {
			increasePinFails();
			throw exception;
		}
	}

	public void acquire() throws InterruptedException {
		mSemaphore.acquire();
	}

	public void release() {
		mSemaphore.release();
	}

	/**
	 * Gets user login.
	 * @return User login.
	 */
	public String getLogin() {
		return mLogin;
	}

	/**
	 * Gets One Time Password.
	 * @return One Time Password.
	 */
	public String getOtp() {
		if(mOtp == null)
			return mPassword;
		return mOtp;
	}

	/**
	 * Sets new One Time Password. A password will stored into Shared Preferences if PIN code isn't null.
	 * @param otp One Time Password.
	 */
	public void setOtp(String otp) {
		mOtp = otp;
		writeToCache();
	}

	/**
	 * Check if given PIN code is the PIN code of the Connection. If check will be failed {@link #increasePinFails()} will called.
	 * @param pin PIN code.
	 * @return True if Connection has the same PIN code or Connection PIN code is null, false otherwise.
	 */
	public boolean checkPin(String pin) {
		if (!isValid)
			return false;
		if(mPin == null)
			return true;
		if(pin != null && pin.equals(mPin)) {
			mPinFailsCount = 0;
			writeToCache();
			return true;
		}
		increasePinFails();
		return false;
	}

	/**
	 * Change Connection PIN to another. PIN will be changed only if current PIN is correct.
	 * @param currentPin Current PIN code.
	 * @param newPin New PIN code.
	 * @return True if check of current PIN is success, false otherwise.
	 * @see #checkPin(String)
	 */
	public boolean changePin(String currentPin, String newPin) {
		if(checkPin(currentPin)){
			changePin(newPin);
			return true;
		}
		return false;
	}

	/**
	 * Force change Connection PIN to another / reset pin
	 * Note: You have to handle PinFailsCount & newPin validity manually
	 * @param newPin New PIN code.
	 * @see #increasePinFails()
	 */
	public boolean changePin(String newPin) {
		mPin = newPin;

		if (isEncrypted && newPin != null) {
			try {
				mPassword = decrypt(mApiPreferences.getString(PASSWORD, ""));
			} catch (WrongKeyException exception) {
				increasePinFails();
				return false;
			}
			isEncrypted = false;
		}
		else
			writeToCache();

		return true;
	}

	/**
	 * Gets amount of invalid PIN checks.
	 * @return Amount of invalid PIN checks.
	 */
	public long getPinFailsCount() {
		return mPinFailsCount;
	}

	/**
	 * Check if Connection is valid. New Connection object is valid by default. To set Connection
	 * invalid used {@link #setToInvalid()}.
	 * @return True if Connection marked as valid, false otherwise.
	 */
	public boolean isValid() {
		return isValid && !isEncrypted;
	}

	/**
	 * Mark Connection as invalid.
	 */
	public void setToInvalid() {
		isValid = false;
	}

	/**
	 * Gets if connection is encrypted. Encryption equals true if used constructor
	 * {@Link #Connection(SharedPreferences, String)} with null pin. You should setup correct pin
	 * before use this connection.
	 * @return True if connection is encrypted, false otherwise.
     */
	public boolean isEncrypted() {
		return isEncrypted;
	}

	/**
	 * Increase PIN fails count. If amount of fails become equal or bigger that
	 * {@link #MAX_PIN_FAIL_COUNT} method {@link #exit()} will called.
	 */
	public void increasePinFails() {
		mPinFailsCount++;
		mApiPreferences.edit().putLong(PIN_FAILS, mPinFailsCount).commit();
		if(mPinFailsCount >= MAX_PIN_FAIL_COUNT) {
			mPin = null;
			exit();
		}
	}

	/**
	 * Removes Password from memory and Shared Preferences, marks Connection as invalid.
	 */
	public void exit() {
		mApiPreferences.edit()
				.remove(PASSWORD)
				.remove(LOGIN)
				.apply();
		mOtp = null;
        mPassword = null;
		setToInvalid();
	}

	/**
	 * Encrypt String via Connection PIN.
	 * @param string String to encrypt.
	 * @return Base64 string or null of Connection PIN code is null.
	 */
	public String encrypt(String string) {
		if(mPin != null)
			return ApiCryptoUtils.encrypt(string, mPin);
		return null;
	}

	/**
	 * Decrypt Base64 String via Connection PIN.
	 * @param string Base64 string to decrypt.
	 * @return Decrypted string or null of Connection PIN code is null.
	 * @throws WrongKeyException If Base64 string encrypted with another PIN code different from Connection PIN.
	 */
	public String decrypt(String string) throws WrongKeyException {
		if(mPin != null)
			return ApiCryptoUtils.decrypt(string, mPin);
		return null;
	}

	/**
	 * Encrypt byte array via Connection PIN.
	 * @param byteArray Byte array to encrypt.
	 * @return Encrypted byte array or null of Connection PIN code is null.
	 */
	public byte[] encrypt(byte[] byteArray) {
		if(mPin != null)
			try {
				return ApiCryptoUtils.cryptByteArray(byteArray, mPin, Cipher.ENCRYPT_MODE);
			} catch (WrongKeyException e) {
				e.printStackTrace();
			}
		return null;
	}

	/**
	 * Decrypt byte array via Connection PIN.
	 * @param byteArray Byte array to decrypt.
	 * @return Decrypted byte array or null of Connection PIN code is null.
	 * @throws WrongKeyException If byte array encrypted with another PIN code different from Connection PIN.
	 */
	public byte[] decrypt(byte[] byteArray) throws WrongKeyException {
		if(mPin != null)
			return ApiCryptoUtils.cryptByteArray(byteArray, mPin, Cipher.DECRYPT_MODE);
		return null;
	}
	
	private void writeToCache(){
		if(mPin != null) {
			mApiPreferences.edit()
				.putString(LOGIN, mLogin)
				.putString(PASSWORD, encrypt(getOtp()))
				.putLong(PIN_FAILS, mPinFailsCount)
				.commit();
		}
	}
}
