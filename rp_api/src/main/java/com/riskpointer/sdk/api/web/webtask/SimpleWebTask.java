package com.riskpointer.sdk.api.web.webtask;

import android.content.Context;

import com.riskpointer.sdk.api.exception.BadStatusCodeException;
import com.riskpointer.sdk.api.utils.ApiLog;
import com.riskpointer.sdk.api.utils.ApiWebUtils;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;

import okhttp3.Call;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Shurygin Denis on 2015-12-22.
 */
public class SimpleWebTask<ResponseResult> extends BaseWebTask<ResponseResult> {

    private Call.Factory mClient;
    private Call mCallRequest;

    /**
     * Construct new WebServiceAsync.
     *
     * @param context Application Context.
     * @param message Message object.
     */
    public SimpleWebTask(Context context, Message<ResponseResult> message) {
        this(context, message, ApiWebUtils.getClient());
    }

    SimpleWebTask(Context context, Message<ResponseResult> message, Call.Factory client) {
        super(context, message);
        mClient = client;
    }

    @Override
    public SimpleWebTask<ResponseResult> clone() {
        return new SimpleWebTask<>(getContext(), getMessage(), mClient);
    }

    /**
     *  Abortion of the task. Is different for different priority.<br>
     *  Low Priority and High Priority - can be aborted immediately only if task is in queue and not executed, otherwise abortion callback will send when execution completed.<br>
     *  No queue - Even the task already executed and HTTP connection is created, the task will be aborted.
     *  <br><br>
     *  Aborted task calls oError with the error code ABORTED.
     */
    @Override
    public boolean abort() {
        super.abort();
        if(getMessage().getConnection() == null) {
            if (mCallRequest != null) {
                new Thread() {
                    @Override
                    public void run() {
                        ApiLog.d("Abort HTTP request of '" + getMessage().getCommand() + "'");
                        mCallRequest.cancel();
                    }
                }.start();
            }
        }
        return isAborted();
    }

    @Override
    String sendWebRequest(Message<ResponseResult> message) throws IOException, BadStatusCodeException {
        String requestString = message.buildRequest();
        if(isWillLogged) {
            StringBuilder logString =
                    new StringBuilder("POST to URL = ")
                    .append(message.getUrl())
                    .append("\n");
            try {
                JSONObject json = new JSONObject(requestString);
                ApiLog.d(logString
                        .append("Request ")
                        .append(message.getCommand())
                        .append(" =>> ")
                        .append(json.toString(4)).toString());
            } catch (JSONException e) {
                ApiLog.d(logString
                        .append("Request ")
                        .append(message.getCommand())
                        .append(" =>> ")
                        .append(requestString).toString());
            }
        }

        String result = null;
        if(!isAborted()) {

            Request.Builder builder = new Request.Builder().url(message.getUrl());
            builder.addHeader("Connection", "Keep-Alive");
            builder.post(buildRequestBody(requestString));

            mCallRequest = mClient.newCall(builder.build());

            Response response = mCallRequest.execute();

            result = response.body().string();

            if (isWillLogged)
                ApiLog.d("Response(" + response.code()+ ") " + message.getCommand() + " <<= " + result);

            if (response.code() != HttpURLConnection.HTTP_OK) {
                throw new BadStatusCodeException("Server return status code: "
                        + response.code());
            }
        }

        return result;
    }

    RequestBody buildRequestBody(String jsonString) {
        return RequestBody.create(ApiWebUtils.MEDIA_TYPE_JSON, jsonString);
    }
}
