package com.riskpointer.sdk.api.web.error;

import android.text.TextUtils;

import com.riskpointer.sdk.api.web.message.Message;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Description of the error.
 * @author Denid Shurygin
 */
@SuppressWarnings({"WeakerAccess", "unused", "PointlessArithmeticExpression"})
public final class RpError implements Serializable {

    /**  {@code RpError} with error code {@code RpError.Api.NONE}. */
    public static final RpError NONE = new RpError(Api.NONE);

    /**  {@code RpError} with error code {@code RpError.Api.ABORTED}. */
    public static final RpError ABORTED = new RpError(Api.ABORTED);

    /**  {@code RpError} with error code {@code RpError.Api.UNKNOWN}. */
    public static final RpError UNKNOWN = new RpError(Api.UNKNOWN);

    static final int RP_ERR_GROUP_MULT = 1000;

    /** Error group. */
    public final int group;

    /** Error code. */
    public final int code;

    /** Message which produce this error. May be null. */
    public final Message<?> message;

    /** Description returned by server. May be null. */
    public final String description;

    /** Fields array returned by server. May be null. */
    public final String[] fields;

    public RpError(int code) {
        this(code, (Message<?>) null);
    }

    public RpError(int code, String description) {
        this(code, null, description);
    }

    public RpError(int code, Message<?> message) {
        this(groupFromCode(code), subItemFromCode(code), message);
    }

    public RpError(int code, Message<?> message, String description) {
        this(groupFromCode(code), subItemFromCode(code), message, null, description);
    }

    public RpError(int group, int subItem, Message<?> message) {
        this(group, subItem, message, null, null);
    }

    public RpError(int group, int subItem, Message<?> message, String[] fields, String description) {
        this.group = group;
        this.code = codeFromGroupAndSubItem(group, subItem);
        this.message = message;
        this.description = description;
        this.fields = fields;
    }

    public boolean isHasField(String fieldName) {
        if (fieldName == null)
            return false;
        if (fields != null)
            for (String field : fields)
                if (fieldName.equals(field))
                    return true;
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (getClass() != obj.getClass())
            return false;

        RpError error = (RpError) obj;
        return group == error.group &&
                code == error.code &&
                ((message != null && message.equals(error.message)) ||
                        (message == null && error.message == null)) &&
                TextUtils.equals(description, error.description) &&
                Arrays.equals(fields, error.fields);
    }

    @Override
    public String toString() {
        return "Error [" + group + ", " + subItemFromCode(code) + "] " + description;
    }

    public static int codeFromGroupAndSubItem(int group, int subItem) {
        return group * RP_ERR_GROUP_MULT + subItem;
    }

    public static int groupFromCode(int code) {
        return code / RP_ERR_GROUP_MULT;
    }

    public static int subItemFromCode(int code) {
        return code % RP_ERR_GROUP_MULT;
    }

    public static final class Group {
        
        /** Client Api errors. This errors invokes by client; not by server. */
        public static final int API                 = -1;

        public static final int SYSTEM              = 0;
        public static final int COMMON              = 1;

        public static final int SMS                 = 6; // TODO describe error codes
        public static final int VERIFY              = 7; // TODO describe error codes

        public static final int LOGIN_STATUS        = 11; // TODO describe error codes

        public static final int QUERY               = 13;

        public static final int SHOP_FIND           = 14;

        public static final int SERVER_TOU          = 16;

        public static final int SHOP_STAMP          = 26; // TODO describe error codes
        public static final int STATUS_SET          = 28;

        public static final int USER_DISPUTE        = 50; // TODO describe error codes
        public static final int USER                = 51;
        public static final int USER_FILE           = 52;
        public static final int USER_INVOICE        = 53; // TODO describe error codes
        public static final int USER_PAYMENT        = 55;
        public static final int USER_PRODUCT_TAGS   = 60; // TODO describe error codes
        public static final int USER_ROLE           = 61;
        public static final int USER_ORDER          = 62;
        public static final int USER_PHOTO          = 64;
        public static final int USER_ORDER_COMMENTS = 67;
        public static final int USER_ORDER_PROPOSAL = 68;
    }

    // Group value -1
    /** Client Api errors. This errors invokes by client; not by server. */
    public static final class Api {

        /** No error. */
        public static final int NONE = Group.API * RP_ERR_GROUP_MULT + -1;

        /** Unknown error */
        public static final int UNKNOWN = Group.API * RP_ERR_GROUP_MULT + 0;

        /** Bad connection. No Internet connection or request was closed by timeout. */
        public static final int NO_CONNECTION = Group.API * RP_ERR_GROUP_MULT + 1;

        /** Cache for re-enter not found or active connection was closed; need to login. */
        public static final int NO_LOGIN = Group.API * RP_ERR_GROUP_MULT + 2;

        /** Server return code that no equal 200 or server response format is wrong.*/
        public static final int BAD_RESPONSE = Group.API * RP_ERR_GROUP_MULT + 3;

        /** Server return result with wrong hash. */
        public static final int WRONG_HASH = Group.API * RP_ERR_GROUP_MULT + 4;

        /** Operation was canceled by user. */
        public static final int CANCELED_BY_USER = Group.API * RP_ERR_GROUP_MULT + 5;

        /** User enter invalid PIN. */
        public static final int INVALID_PIN = Group.API * RP_ERR_GROUP_MULT + 6;

        /** Error for offline mode. Cache for request not found. */
        public static final int CACHE_NOT_FOUND = Group.API * RP_ERR_GROUP_MULT + 7;

        /** Error for offline mode. Cache for request not supported. */
        public static final int CACHE_NOT_SUPPORTED = Group.API * RP_ERR_GROUP_MULT + 8;

        /** API not initialised. */
        public static final int NO_INITIALIZED = Group.API * RP_ERR_GROUP_MULT + 9;

        /** Account not supported for selected shop. */
        public static final int ACCOUNT_NOT_SUPPORTED = Group.API * RP_ERR_GROUP_MULT + 10;

        /** Request not supported for selected shop. */
        public static final int UNSUPPORTED_REQUEST = Group.API * RP_ERR_GROUP_MULT + 11;

        /** Error when request task was aborted manually. */
        public static final int ABORTED = Group.API * RP_ERR_GROUP_MULT + 12;

        /** Error when app didn't provide critical permissions. */
        public static final int NO_CRITICAL_PERMISSIONS = Group.API * RP_ERR_GROUP_MULT + 13;

    }


    // Group value 00
    public static final class System {

        /** Internal error */
        public static final int INTERNAL = Group.SYSTEM * RP_ERR_GROUP_MULT + 1;

        /** Invalid value for parameter */
        public static final int INVALID_VALUE_FOR_PARAMETER = Group.SYSTEM * RP_ERR_GROUP_MULT + 2;

        /** HTTP auth error */
        public static final int HTTP_AUTH = Group.SYSTEM * RP_ERR_GROUP_MULT + 4;

        /** Internal: malformed message */
        public static final int MAILFORMED_MESSAGE = Group.SYSTEM * RP_ERR_GROUP_MULT + 10;

        /** Command not found */
        public static final int COMMAND_NOT_FOUND = Group.SYSTEM * RP_ERR_GROUP_MULT + 11;

        /** Server on service work */
        public static final int SERVICE_WORKS = Group.SYSTEM * RP_ERR_GROUP_MULT + 12;
    }

    // Group value 01
    public static final class Common {

        /** Required parameter missing */
        public static final int PARAMETER_MISSING = Group.COMMON * RP_ERR_GROUP_MULT + 1;

        /** Invalid value for parameter */
        public static final int INVALID_VALUE_FOR_PARAMETER = Group.COMMON * RP_ERR_GROUP_MULT + 2;

        /** Invalid message security */
        public static final int INVALID_SECURITY = Group.COMMON * RP_ERR_GROUP_MULT + 3;

        /** Invalid adminuserer login */
        public static final int INVALID_ADMIN_LOGIN = Group.COMMON * RP_ERR_GROUP_MULT + 5;

        /** Invalid user login */
        public static final int INVALID_USER_LOGIN = Group.COMMON * RP_ERR_GROUP_MULT + 6;

        /** Shop is not active yet */
        public static final int SHOP_NOT_ACTIVE = Group.COMMON * RP_ERR_GROUP_MULT + 10;

        /** Merchant is not active yet */
        public static final int MERCHANT_NOT_ACTIVE = Group.COMMON * RP_ERR_GROUP_MULT + 11;

        /** No such shop */
        public static final int SHOP_NOT_EXIST = Group.COMMON * RP_ERR_GROUP_MULT + 13;
    }


    // Group value 13
    public static final class Query {

        /** No such transaction in shop */
        public static final int NO_TRANSACTION = Group.QUERY * RP_ERR_GROUP_MULT + 1;

    }


    // Group value 14
    public static final class ShopFind {

        /** No such shop */
        public static final int NO_SHOP = Group.SHOP_FIND * RP_ERR_GROUP_MULT + 1;

        /** Invalid value for parameter */
        public static final int INVALID_VALUE_FOR_PARAMETR = Group.SHOP_FIND * RP_ERR_GROUP_MULT + 2;

    }


    // Group value 16
    public static final class ServerTou {

        /** ToU not found */
        public static final int NOT_FOUND = Group.SERVER_TOU * RP_ERR_GROUP_MULT + 1;

    }


    // Group value 28
    public static final class StatusSet {

        /** Invalid transaction */
        public static final int INVALID_TRANSACTION = Group.STATUS_SET * RP_ERR_GROUP_MULT + 1;

        /** Invalid status value */
        public static final int INVALID_STATUS = Group.STATUS_SET * RP_ERR_GROUP_MULT + 2;

    }


    // Group value 51
    public static final class User {

        /** User already exists */
        public static final int EXISTS = Group.USER * RP_ERR_GROUP_MULT + 1;

        /** Invalid value for parameter */
        public static final int INVALID_VALUE_FOR_PARAMETR = Group.USER * RP_ERR_GROUP_MULT + 2;

        /** No registration transaction found */
        public static final int NO_REGISTRATION = Group.USER * RP_ERR_GROUP_MULT + 3;

        /** Registration transaction status is not reject */
        public static final int REGISTRATION_NOT_REJECTED = Group.USER * RP_ERR_GROUP_MULT + 4;

        /** Manual review done */
        public static final int MANUAL_REVIEW_DONE = Group.USER * RP_ERR_GROUP_MULT + 5;

        /** Manual review done */
        public static final int LOGIN_REJECTED = Group.USER * RP_ERR_GROUP_MULT + 55;

    }


    // Group value 52
    public static final class UserFile {

        /** File not found */
        public static final int NOT_FOUND = Group.USER_FILE * RP_ERR_GROUP_MULT + 1;

    }


    // Group value 55
    public static final class UserPayment {

        /** User is not active */
        public static final int USER_NOT_ACTIVE = Group.USER_PAYMENT * RP_ERR_GROUP_MULT + 1;

        /** Shop type does not allow product list */
        public static final int PRODUCTS_NOT_SUPPORTED = Group.USER_PAYMENT * RP_ERR_GROUP_MULT + 2;

        /** Wrong shop configuration; Mecsel mode required */
        public static final int MECSEL_MODE_REQUIRED = Group.USER_PAYMENT * RP_ERR_GROUP_MULT + 3;

        /** Required parameter missing: account_amount account_currency */
        public static final int NO_PRICE = Group.USER_PAYMENT * RP_ERR_GROUP_MULT + 4;

        /** Required parameter missing: account_type account_number */
        public static final int NO_ACCOUNT = Group.USER_PAYMENT * RP_ERR_GROUP_MULT + 5;

        /** Shop type does not allow amount */
        public static final int AMOUNT_NOT_SUPPORTED = Group.USER_PAYMENT * RP_ERR_GROUP_MULT + 10;

        /** Product parameter missing */
        public static final int NO_PRODUCT = Group.USER_PAYMENT * RP_ERR_GROUP_MULT + 11;

        /** Only dicts are allowed in product list */
        public static final int INVALID_PRODUCTS = Group.USER_PAYMENT * RP_ERR_GROUP_MULT + 12;

        /** Product not in merchant list */
        public static final int PRODUCT_NO_IN_MERCHANT = Group.USER_PAYMENT * RP_ERR_GROUP_MULT + 13;

        /** Invalid value product list count */
        public static final int INVALID_PRODUCT_COUNT = Group.USER_PAYMENT * RP_ERR_GROUP_MULT + 14;
    }


    // Group value 60
    public static final class UserProductTags {

        /** Tags not found */
        public static final int NOT_FOUND = Group.USER_PRODUCT_TAGS * RP_ERR_GROUP_MULT + 1;

        /** Tag with specified name already exists */
        public static final int ALREADY_EXIST = Group.USER_PRODUCT_TAGS * RP_ERR_GROUP_MULT + 2;

    }


    // Group value 61
    public static final class UserRole {

        /** User does not exists */
        public static final int USER_NOT_EXISTS = Group.USER_ROLE * RP_ERR_GROUP_MULT + 1;

        /** Role does not exists */
        public static final int ROLE_NOT_EXISTS = Group.USER_ROLE * RP_ERR_GROUP_MULT + 2;

        /** User has not registered for the role */
        public static final int ROLE_NOT_REGISTERED = Group.USER_ROLE * RP_ERR_GROUP_MULT + 3;

        /** Role is not allowed for merchant */
        public static final int ROLE_NOT_ALLOWED_FOR_MERCHANT = Group.USER_ROLE * RP_ERR_GROUP_MULT + 4;

        /** User is not allowed to set the role */
        public static final int USER_NOT_ALLOWED_FOR_EDIT = Group.USER_ROLE * RP_ERR_GROUP_MULT + 5;

    }


    // Group value 62
    public static final class UserOrder {

        /** User is not allowed to get the orders */
        public static final int USER_NOT_ALLOWED_TO_GET = Group.USER_ORDER * RP_ERR_GROUP_MULT + 1;

        /** User is not allowed to set the order */
        public static final int USER_NOT_ALLOWED_TO_SET = Group.USER_ORDER * RP_ERR_GROUP_MULT + 2;

        /** Order not found */
        public static final int ORDER_NOT_FOUND = Group.USER_ORDER * RP_ERR_GROUP_MULT + 3;

        /** Payment error while moving order on payment status */
        public static final int PAYMENT_ERROR = Group.USER_ORDER * RP_ERR_GROUP_MULT + 4;

    }


    // Group value 64
    public static final class UserPhoto {

        /** Failed to create photo directory */
        public static final int INNER_ERROR = Group.USER_PHOTO * RP_ERR_GROUP_MULT + 1; // TODO merge with public static final int FailedToSave

        /** Unsupported file type */
        public static final int UNSUPPORTED_FILE_FORMAT = Group.USER_PHOTO * RP_ERR_GROUP_MULT + 2;

        /** Failed to save photo */
        public static final int FAILED_TO_SAVE = Group.USER_PHOTO * RP_ERR_GROUP_MULT + 3;

    }


    // Group value 67
    public static final class UserOrderComments {

        /** Order not found */
        public static final int ORDER_NOT_FOUND = Group.USER_ORDER_COMMENTS * RP_ERR_GROUP_MULT + 1;
        
        /** The user is not allowed to rate order */
        public static final int USER_NOT_ALLOWED = Group.USER_ORDER_COMMENTS * RP_ERR_GROUP_MULT + 2;
        
        /** User does not exist */
        public static final int USER_NOT_EXIST = Group.USER_ORDER_COMMENTS * RP_ERR_GROUP_MULT + 3;
        
        /** Role does not exist */
        public static final int ROLE_NOT_EXIST = Group.USER_ORDER_COMMENTS * RP_ERR_GROUP_MULT + 4;

    }


    // Group value 68
    public static final class UserOrderProposal {

        /** Order not found */
        public static final int ORDER_NOT_FOUND = Group.USER_ORDER_PROPOSAL * RP_ERR_GROUP_MULT + 1;
        
        /** User is not allowed to propose for this order */
        public static final int USER_NOT_ALLOWED = Group.USER_ORDER_PROPOSAL * RP_ERR_GROUP_MULT + 2;
        
        /** Already proposed for this order */
        public static final int ALREADY_PROPOSED = Group.USER_ORDER_PROPOSAL * RP_ERR_GROUP_MULT + 3;

        /** This invitation already used */
        public static final int ALREADY_USED = Group.USER_ORDER_PROPOSAL * RP_ERR_GROUP_MULT + 4;
    }
}
