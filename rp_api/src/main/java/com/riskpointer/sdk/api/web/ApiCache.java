/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web;

import android.content.Context;

import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.response.Response;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

/**
 * Helper class that allow to store server responses into files in the internal storage.
 */
public final class ApiCache {
	/** Time in millis when cache file become expired and should be removed. */
	public static final long CACHE_FILE_EXPIRE_TIME = 691200000 * 2;
	/** Extension of the cache file. */
    public static final String CACHE_FILE_EXTENSION = ".ch";

	private static final String WEB_CACHE_FOLDER = "WebServiceCache";
	private static final String API_CACHE_FOLDER = "ApiStateCache";
	private static final String AUT_CACHE_SUB_FOLDER = "aut";

	private ApiCache() {}

	/**
	 * Puts response into cache file. Response must contain {@link Message} object that used for
	 * request, so deserialized Response can't be used for these method.
	 * @param context Application context.
	 * @param response Server response.
	 */
	public static void putWebResponse(Context context, Response<?> response) {
		Message<?> message = response.getMessage();
        String cacheFileName = message != null ? message.getCacheFileName() : null;
        if (cacheFileName != null) {
            if (message.getConnection() == null
					&& message.getParameter(Message.Param.LOGIN_IDENTITY) != null) {
				putWebResponse(context, response, cacheFileName, true);
            } else if (message.getConnection() == null
					&& message.getParameter(Message.Param.USER_EMAIL) != null) {
				putWebResponse(context, response, cacheFileName, true);
            } else
				putWebResponse(context, response, cacheFileName, message.getConnection() != null);
        }
	}

	/**
	 * Puts response object into cache file.
	 * @param context Application context.
	 * @param response Response that will be cached.
	 * @param fileName Name of the cache file exclude extension.
	 * @param isAuthorised True if this is authorised user cache. This data should be removed after
	 *                        logout.
	 */
	public static void putWebResponse(Context context,  Response<?> response, String fileName,
									  boolean isAuthorised) {
        if (fileName != null) {
            File file = new File(getWebCacheFolder(context, isAuthorised),
					fileName + CACHE_FILE_EXTENSION);
            put(file, response);
        }
	}

	// TODO docs
	public static void putApi(Context context, Object object, String fileName,
							  boolean isAuthorised) {
		if (fileName != null) {
			File file = new File(getApiCacheFolder(context, isAuthorised),
					fileName + CACHE_FILE_EXTENSION);
			put(file, object);
		}
	}

	/**
	 * Puts specified object into cache file.
	 * @param file Cache file.
	 * @param object An Object which will be cached.
	 */
	public static void put(File file, Object object) {
		if (file != null) {
			FileOutputStream fileOutStream = null;
			ObjectOutput objectOutput = null;
			try {
				ByteArrayOutputStream byteOutStream = new ByteArrayOutputStream();
				objectOutput = new ObjectOutputStream(byteOutStream);
				objectOutput.writeObject(object);
				byte[] byteArray = byteOutStream.toByteArray();

				fileOutStream = new FileOutputStream(file);
				fileOutStream.write(byteArray);
				fileOutStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (objectOutput != null) {
						objectOutput.close();
					}
				} catch (IOException ex) {
					// ignore close exception
				}
				try {
					if(fileOutStream != null)
						fileOutStream.close();
				} catch (IOException ex) {
					// ignore close exception
				}
			}
		}
	}

	/**
	 * Gets cached response from the cache file.
	 * @param context Application context.
	 * @param fileName Name of the cache file exclude extension.
	 * @param isAuthorised True if this is authorised user cache. This data should be removed after
	 *                        logout.
	 * @return Cached response or null if file doesn't exists.
	 */
    public static Response<?> getWebResponse(Context context, String fileName,
											 boolean isAuthorised) {
        return (Response<?>) get(new File(getWebCacheFolder(context, isAuthorised),
				fileName + CACHE_FILE_EXTENSION));
    }

	public static Object getApi(Context context, String fileName, boolean isAuthorised) {
		return get(new File(getApiCacheFolder(context, isAuthorised),
				fileName + CACHE_FILE_EXTENSION));
	}


	public static <T> T getApi(Context context, String fileName, boolean isAuthorised,
									Class<T> clazz) {
		Object object = getApi(context, fileName, isAuthorised);
		if (object != null && object.getClass() == clazz) {
			return (T) object;
		}
		return null;
	}

	/**
	 * Gets cached object from the cache file.
	 * @param file Cache file.
	 * @return Cached object or null if file doesn't exists.
	 */
	public static Object get(File file) {
		if (!file.exists())
			return null;

		Object result = null;
		ObjectInput objectInput = null;
		FileInputStream fileInputSream = null;
		try {
			fileInputSream = new FileInputStream(file);
			byte[] byteArray = new byte[fileInputSream.available()];
			fileInputSream.read(byteArray);
			ByteArrayInputStream byteInputStream = new ByteArrayInputStream(byteArray);
			objectInput = new ObjectInputStream(byteInputStream);
			result = objectInput.readObject();
		}
		catch (Exception exception) {
		    exception.printStackTrace();
		    return null;
		} finally {
			try {
				if (objectInput != null) {
					objectInput.close();
				}
			} catch (IOException ex) {
				// ignore close exception
			}
			try {
				if(fileInputSream != null)
					fileInputSream.close();
			} catch (IOException ex) {
				// ignore close exception
			}
		}
		return result;
	}

	//TODO docs
	public static void removeApiFile(Context context, String fileName, boolean isAuthorised) {
		File file = new File(getApiCacheFolder(context, isAuthorised),
				fileName + CACHE_FILE_EXTENSION);
		delete(file);
	}

	/**
	 * Removes all authorized cache files.
	 * @param context Application context.
	 */
	public static void clearAuthorized(Context context) {
		delete(getWebCacheFolder(context, true));
		delete(getApiCacheFolder(context, true));
	}

	/**
	 * Removes all cache files.
	 * @param context Application context.
	 */
	public static void clearAll(Context context) {
		delete(getWebCacheFolder(context, false));
		delete(getApiCacheFolder(context, false));
	}

	/**
	 * Removes expired cache files.
	 * @param context Application context.
	 * @see #CACHE_FILE_EXPIRE_TIME
	 */
	public static void trim(Context context) {
		trim(context, System.currentTimeMillis() - CACHE_FILE_EXPIRE_TIME);
	}

	/**
	 * Removes expired cache files.
	 * @param context Application context.
	 * @param dateToTrim Data in millis when cache file considered expired.
	 */
	public static void trim(final Context context, final long dateToTrim) {
		new Thread() {
			@Override
			public void run() {
				trim(getWebCacheFolder(context, false), dateToTrim);
			}
		}.start();
	}
	
	private static void trim(File file, long dateToTrim) {
		if(file.lastModified() < dateToTrim) {
            delete(file);
        }
		else {
			if (file.isDirectory()) {
				for(File childFile : file.listFiles())
					trim(childFile, dateToTrim);
			}
		}
	}

	public static File getWebCacheFolder(Context context, boolean isAuthorised) {
		return getCacheFolder(context, WEB_CACHE_FOLDER, isAuthorised);
	}

	public static File getApiCacheFolder(Context context, boolean isAuthorised) {
		return getCacheFolder(context, API_CACHE_FOLDER, isAuthorised);
	}

	public static File getCacheFolder(Context context, String folder, boolean isAuthorised) {
		File cacheFolder = new File(context.getFilesDir(), folder);
		createDir(cacheFolder);
		if(isAuthorised) {
			cacheFolder = new File(cacheFolder, AUT_CACHE_SUB_FOLDER);
			createDir(cacheFolder);
		}
		return cacheFolder;
	}
	
	private static boolean createDir(File folder) {
        if (!folder.exists())
        	try {
        		return folder.mkdirs();
    		} catch (Exception e) {
    			e.printStackTrace();
    			return false;
    		}
        return true;
	}
	
	private static boolean delete(File file) {
		if (file.exists()) {
			boolean result = true;
			if(file.isDirectory()) {
				for (File child : file.listFiles())
					result = result & delete(child);
			}
			return result & file.delete();
		}
		return true;
	}
}
