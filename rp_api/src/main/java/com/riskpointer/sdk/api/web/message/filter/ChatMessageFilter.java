package com.riskpointer.sdk.api.web.message.filter;

import java.util.HashMap;

/**
 * Created by Shurygin Denis on 2016-05-11.
 */
public class ChatMessageFilter extends ListFilter {

    private String mOrderId;

    public String getOrderId() {
        return mOrderId;
    }

    public void setOrderId(String orderId) {
        this.mOrderId = orderId;
    }

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = super.toParams();
        AbsFilter.applyIfNotNull(params, "order_id", mOrderId);
        return params;
    }
}
