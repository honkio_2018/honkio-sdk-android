/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol;

import com.riskpointer.sdk.api.exception.WrongHashException;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.response.Response;

/**
 * The Parser of the server response.
 * @param <BuildResult> Type of the response.
 *
 * @author Denis Shurygin
 */
public interface ResponseParser<BuildResult> {
	/**
	 * Parse server response into Client Api object.
	 * @param responseString String which server returns.
	 * @param password The password to check hash of the response or null if hash should not be checked.
	 * @return Object that represent the server response.
	 * @throws WrongHashException If hash of the response is not correct.
	 */
	Response<BuildResult> parse(Message<?> message, String responseString, String password) throws WrongHashException;
}
