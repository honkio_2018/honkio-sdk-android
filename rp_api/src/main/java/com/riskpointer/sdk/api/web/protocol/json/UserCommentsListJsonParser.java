/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.UserComment;
import com.riskpointer.sdk.api.model.entity.UserCommentsList;
import com.riskpointer.sdk.api.model.entity.UserRole;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//TODO: write docs
public class UserCommentsListJsonParser extends BaseJsonResponseParser<UserCommentsList> {

    @Override
    public UserCommentsList parseJson(Message<?> message, JSONObject json) {

        Map<String, UserRole> rolesMap = parseRolesArray(json.optJSONArray("user_roles"));

        UserCommentsList userCommentsList = new UserCommentsList();

        ArrayList<UserComment> list = new ArrayList<UserComment>();

        JSONArray commentsListJson = json.optJSONArray("user_comments");
        if (commentsListJson == null)
            commentsListJson = json.optJSONArray("rates");

        if (commentsListJson != null) {
            for (int i = 0; i < commentsListJson.length(); i++) {
                JSONObject feedJson = commentsListJson.optJSONObject(i);
                if (feedJson != null) {
                    UserComment userComment = new UserComment();
                    userComment.setText(feedJson.optString("comment"));
                    userComment.setObject(feedJson.optString("object"));
                    userComment.setObjectType(feedJson.optString("object_type"));
                    userComment.setUserId(feedJson.optString("user_id"));
                    //todo userComment.setUserRoleId(feedJson.optString("user_role_id"));

                    userComment.setVoter(getRole(
                            feedJson.optString("voter_role_id"),
                            feedJson.optString("voter_id"),
                            rolesMap,
                            feedJson.optString("voter_first_name"),
                            feedJson.optString("voter_last_name")
                    ));

                    userComment.setOwnerId(feedJson.optString("user_owner_id"));
                    userComment.setOrderId(feedJson.optString("order_id"));
                    userComment.setTime(ApiFormatUtils.stringDateToLong(feedJson.optString("timestamp")));
                    userComment.setMark(feedJson.optInt("mark"));
                    userComment.setJobTagId(feedJson.optString("tag_id"));

                    list.add(userComment);
                }
            }
        }

        userCommentsList.setList(list);
        return userCommentsList;
    }

    private Map<String, UserRole> parseRolesArray(JSONArray array) {
        Map<String, UserRole> rolesMap = new HashMap<>();

        if (array != null) {
            UserRoleJsonParser roleParser = new UserRoleJsonParser();

            for (int i = 0; i < array.length(); i++) {
                JSONObject roleJson = array.optJSONObject(i);
                if (roleJson != null) {
                    UserRole role = roleParser.parseJson(null, roleJson);
                    if (role != null) {
                        rolesMap.put(keyForRole(role.getRoleId(), role.getUserId()), role);
                    }
                }
            }

        }

        return rolesMap;
    }

    private UserRole getRole(String roleId, String userId, Map<String, UserRole>map, String defFirstName, String defLastName) {
        UserRole role = map.get(keyForRole(roleId, userId));
        if (role == null) {
            role = new UserRole();
            role.setRoleId(roleId);
            role.setUserId(userId);
            role.setUserFirstName(defFirstName);
            role.setUserLastName(defLastName);
            map.put(keyForRole(roleId, userId), role);
        }

        return role;
    }

    private String keyForRole(String roleId, String userId) {
        return roleId + " " + userId;
    }
}
