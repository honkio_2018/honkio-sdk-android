package com.riskpointer.sdk.api.web.message;

import android.content.Context;

import com.riskpointer.sdk.api.model.entity.HKDevice;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.web.protocol.RequestBuilder;
import com.riskpointer.sdk.api.web.protocol.ResponseParser;
import com.riskpointer.sdk.api.web.response.Response;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by Shurygin Denis on 2016-06-09.
 */
public class BatchMessage extends Message<Response<?>> {

    private ArrayList<Message<?>> mMessages = new ArrayList<>();

    public BatchMessage(Context context, String url,
                        HKDevice device,
                        Identity shopIdentity, Identity appIdentity,
                        RequestBuilder requestBuilder, ResponseParser<Response<?>> responseParser,
                        int flags) {
        super(context, Command.BATCH, url, null, device, shopIdentity, appIdentity, requestBuilder, responseParser, flags);
    }

    public void addMessage(Message<?> message) {
        mMessages.add(message);
    }

    public void addMessages(Collection<Message<?>> messages) {
        mMessages.addAll(messages);
    }

    @Override
    public Map<String, Object> buildRequestMap() {
        Map<String, Object> request = super.buildRequestMap();

        List<Map<String, Object>> list = new ArrayList<>(mMessages.size());
        for (Message<?> message : mMessages) {
            Map<String, Object> map = message.buildRequestMap();

            for (Map.Entry<String, Object> entry : request.entrySet()) {
                if (!map.containsKey(entry.getKey()))
                    map.put(entry.getKey(), entry.getValue());
            }

            map = message.putHash(map);

            for (Map.Entry<String, Object> commonEntry : request.entrySet()) {
                Object commonObject = map.get(commonEntry.getKey());
                if (commonObject != null && commonObject.equals(commonEntry.getValue()))
                    map.remove(commonEntry.getKey());
            }

            list.add(map);
        }

        request.put("query_messages", list);

        return request;
    }
}
