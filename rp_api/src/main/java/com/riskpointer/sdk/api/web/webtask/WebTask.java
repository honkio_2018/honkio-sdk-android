package com.riskpointer.sdk.api.web.webtask;

import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.response.Response;

// TODO Write docs
/**
 * Created by Shurygin Denis on 2016-11-01.
 */
public interface WebTask<ResponseResult> extends Task, Cloneable {

    Response<ResponseResult> execute();

    Message getMessage();
    Response<ResponseResult> getResponse();
    RpError getError();

    Object clone();
}
