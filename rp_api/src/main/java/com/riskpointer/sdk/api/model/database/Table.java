package com.riskpointer.sdk.api.model.database;

/**
 * Class that contain names for data base tables.
 *
 * @author Shurygin Denis on 2015-08-12.
 */
public final class Table {

    /** Table that contain stamps */
    public static final String STAMP = "stamp";

    private Table() {}
}
