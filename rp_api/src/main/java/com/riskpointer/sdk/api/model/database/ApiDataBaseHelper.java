package com.riskpointer.sdk.api.model.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * SQLiteOpenHelper that provide access to API data base.
 *
 * @author Shurygin Denis on 2015-03-02.
 */
public class ApiDataBaseHelper extends SQLiteOpenHelper {
    /** File name of the data base. */
    public static final String DATABASE_NAME = "api_base.db";
    /** Version number of the data base. */
    public static final int DATABASE_VERSION = 1;

    private static ApiDataBaseHelper sInstance;

    /**
     * Gets static instance of Data base helper.
     * @param context Application Context.
     * @return Static Instance of Data base helper.
     */
    public static synchronized ApiDataBaseHelper getInstance(Context context) {
        if (sInstance == null)
            sInstance = new ApiDataBaseHelper(context.getApplicationContext());
        return sInstance;
    }

    /**
     * PuConstruct new ApiDataBaseHelper
     * @param context Application context.
     */
    public ApiDataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Table.STAMP + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + StampColumns.SHOP_ID + " TEXT,"
                + StampColumns.TRANSACTION_ID + " TEXT,"
                + StampColumns.NAME + " TEXT,"
                + StampColumns.VALID_FROM + " TEXT,"
                + StampColumns.VALID_TO + " TEXT,"
                + StampColumns.SIGNATURE + " TEXT,"
                + StampColumns.READING_TIME + " INTEGER,"
                + StampColumns.IS_SENT_TO_SERVER + " INTEGER,"
                + StampColumns.IS_VALID + " INTEGER,"
                + StampColumns.SERVER_ERROR_CODE + " INTEGER,"
                + StampColumns.ORIGINAL_STRING + " TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
