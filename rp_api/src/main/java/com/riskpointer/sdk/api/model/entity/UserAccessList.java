package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * @author Shurygin Denis
 */

public class UserAccessList implements Serializable {

    public static class Item implements Serializable{

        private String mMerchant;
        private ArrayList<String> mAccess;

        public String getMerchant() {
            return mMerchant;
        }

        public void setMerchant(String merchant) {
            this.mMerchant = merchant;
        }

        public ArrayList<String> getAccess() {
            return mAccess;
        }

        public void setAccess(ArrayList<String> access) {
            this.mAccess = access;
        }

        public boolean isHasAccess(String... access) {
            return mAccess != null && mAccess.containsAll(Arrays.asList(access));
        }
    }

    private ArrayList<Item> mList;
    private HashMap<String, MerchantSimple> mMerchants;

    public ArrayList<Item> getList() {
        return mList;
    }

    public void setList(ArrayList<Item> list) {
        this.mList = list;
    }

    public boolean isEmpty() {
        return mList == null || mList.size() == 0;
    }

    public Item findByMerchant(String merchantId) {
        if (mList == null || merchantId == null)
            return null;

        for (Item item: mList)
            if (merchantId.equals(item.getMerchant()))
                return item;

        return null;
    }

    public boolean isHasAccess(String merchantId, String... access) {
        Item item = findByMerchant(merchantId);
        return item != null && item.isHasAccess(access);
    }

    public HashMap<String, MerchantSimple> getMerchants() {
        return mMerchants;
    }

    public void setMerchants(HashMap<String, MerchantSimple> merchants) {
        this.mMerchants = merchants;
    }
}
