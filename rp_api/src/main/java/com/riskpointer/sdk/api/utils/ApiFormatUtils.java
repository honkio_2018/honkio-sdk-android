package com.riskpointer.sdk.api.utils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by asenko on 12/1/2015.
 */
public class ApiFormatUtils {

    /** DecimalFormat to convert Location double value into String. */
    private static final DecimalFormat sLocationFormat = new DecimalFormat("#.########");

    /** DecimalFormat to convert double value into String. */
    public static final DecimalFormat AMOUNT_FORMAT = new DecimalFormat("#0.00");

    private static final SimpleDateFormat mResponseDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

    static {
        mResponseDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    /**
     * Convert amount value to String.
     * @param value Value that should be converted.
     * @return Amount converted to String.
     */
    public static String formatLocation(double value) {
        return sLocationFormat.format(value).replaceAll(",", ".");
    }

    /**
     * Convert amount value to String.
     * @param amount Value that should be converted.
     * @return Amount converted to String.
     */
    public static String formatAmount(double amount) {
        return AMOUNT_FORMAT.format(amount).replace(',', '.');
    }


    //TODO docs
    public static String dateToServerFormat(long date) {
        return mResponseDateFormat.format(new Date(date));
    }

    /**
     * Convert string into date in millis.
     *
     * @param dateString String date.
     * @return Date in millis.
     */
    public static long stringDateToLong(String dateString) {
        if (dateString != null && !"".equals(dateString)) try {
            return mResponseDateFormat.parse(dateString).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
