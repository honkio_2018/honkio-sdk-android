package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;

/**
 * Created by I.N. on 11.01.2018
 */

public class Consumer implements Serializable {
    private boolean mActive;
    private String mId;
    private UserRolesList mRoles;

    public UserRolesList getRoles() {
        return mRoles;
    }

    public void setRoles(UserRolesList roles) {
        mRoles = roles;
    }

    public boolean isActive() {
        return mActive;
    }

    public void setActive(boolean active) {
        mActive = active;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }
}
