package com.riskpointer.sdk.api.merchant;

import android.text.TextUtils;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.merchant.protocol.json.MerchantShopInfoJsonParser;
import com.riskpointer.sdk.api.merchant.protocol.json.MerchantShopsListJsonParser;
import com.riskpointer.sdk.api.merchant.protocol.json.MerchantsListJsonParser;
import com.riskpointer.sdk.api.model.entity.Asset;
import com.riskpointer.sdk.api.model.entity.AssetList;
import com.riskpointer.sdk.api.model.entity.CalendarEvent;
import com.riskpointer.sdk.api.model.entity.ChatMessage;
import com.riskpointer.sdk.api.model.entity.ChatMessages;
import com.riskpointer.sdk.api.model.entity.ConsumerList;
import com.riskpointer.sdk.api.model.entity.EarningsList;
import com.riskpointer.sdk.api.model.entity.HKLink;
import com.riskpointer.sdk.api.model.entity.HKLinksList;
import com.riskpointer.sdk.api.model.entity.HkQrCode;
import com.riskpointer.sdk.api.model.entity.HkStructure;
import com.riskpointer.sdk.api.model.entity.HkStructureList;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.InventoryProduct;
import com.riskpointer.sdk.api.model.entity.Order;
import com.riskpointer.sdk.api.model.entity.OrdersList;
import com.riskpointer.sdk.api.model.entity.Poll;
import com.riskpointer.sdk.api.model.entity.Product;
import com.riskpointer.sdk.api.model.entity.ShopFind;
import com.riskpointer.sdk.api.model.entity.ShopProducts;
import com.riskpointer.sdk.api.model.entity.SimpleListItem;
import com.riskpointer.sdk.api.model.entity.Transactions;
import com.riskpointer.sdk.api.model.entity.UserComment;
import com.riskpointer.sdk.api.model.entity.UserRole;
import com.riskpointer.sdk.api.model.entity.UserRolesList;
import com.riskpointer.sdk.api.model.entity.merchant.Merchant;
import com.riskpointer.sdk.api.model.entity.merchant.MerchantShop;
import com.riskpointer.sdk.api.model.entity.merchant.MerchantShopInfo;
import com.riskpointer.sdk.api.model.entity.merchant.MerchantShopsList;
import com.riskpointer.sdk.api.model.task.RequestTask;
import com.riskpointer.sdk.api.model.task.RequestTaskWrapper;
import com.riskpointer.sdk.api.web.WebServiceAsync;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.callback.RequestCallbackWrapper;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.AssetFilter;
import com.riskpointer.sdk.api.web.message.filter.ChatMessageFilter;
import com.riskpointer.sdk.api.web.message.filter.ConsumerFilter;
import com.riskpointer.sdk.api.web.message.filter.EarningsFilter;
import com.riskpointer.sdk.api.web.message.filter.HKLinkFilter;
import com.riskpointer.sdk.api.web.message.filter.ListFilter;
import com.riskpointer.sdk.api.web.message.filter.MerchantProductsFilter;
import com.riskpointer.sdk.api.web.message.filter.OrderFilter;
import com.riskpointer.sdk.api.web.message.filter.ShopFilter;
import com.riskpointer.sdk.api.web.message.filter.TransactionFilter;
import com.riskpointer.sdk.api.web.response.Response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static com.riskpointer.sdk.api.BaseHonkioApi.checkApi;
import static com.riskpointer.sdk.api.BaseHonkioApi.getActiveConnection;
import static com.riskpointer.sdk.api.BaseHonkioApi.getAppIdentity;
import static com.riskpointer.sdk.api.BaseHonkioApi.getContext;
import static com.riskpointer.sdk.api.BaseHonkioApi.getDevice;
import static com.riskpointer.sdk.api.BaseHonkioApi.getMainShopIdentity;
import static com.riskpointer.sdk.api.BaseHonkioApi.getProtocolFactory;
import static com.riskpointer.sdk.api.BaseHonkioApi.getSuitableLanguage;
import static com.riskpointer.sdk.api.BaseHonkioApi.getUrl;
import static com.riskpointer.sdk.api.BaseHonkioApi.shopFindById;
import static com.riskpointer.sdk.api.merchant.MerchantCommand.MERCHANT_GET_CHAT;

// TODO write docs
/**
 * Created by Shurygin Denis on 2016-11-07.
 */
public class HonkioMerchantApi {

    public static RequestTask<Merchant> merchantGet(String merchantId, int flags, RequestCallback<Merchant> callback) {
        if (checkApi(callback)) {
            Message<Merchant> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_GET, getUrl(), getActiveConnection(), getDevice(), getMainShopIdentity(),
                    getAppIdentity(), getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(Merchant.class), flags);

            message.addParameter("id", merchantId);

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    public static RequestTask<Merchant> merchantSet(Merchant merchant, int flags, RequestCallback<Merchant> callback) {
        if (checkApi(callback)) {
            Message<Merchant> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_SET, getUrl(), getActiveConnection(), getDevice(),
                    getMainShopIdentity(), getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(Merchant.class), flags);

            message.addParameter("merchant", merchant.toParams());

            if (merchant.getId() != null)
                message.addParameter("id", merchant.getId());

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    /**
     * The request to load the list of merchants available for current merchant account.
     *
     * @param flags
     *            flags for request.
     * @param callback
     *            The Object that implement an interface
     *            RequestCallback{@literal <}ArrayList{@literal <}SimpleListItem>>. This object will be used for
     *            sending callbacks in to UI thread.
     */
    public static RequestTask<ArrayList<SimpleListItem>> merchantGetList(ListFilter filter, int flags, RequestCallback<ArrayList<SimpleListItem>> callback) {
        return merchantGetList(filter,null, flags, callback);
    }

    /**
     * The request to load the list of merchants available for current merchant account.
     *
     * @param params
     *            Special params that will insert into request.
     * @param flags
     *            flags for request.
     * @param callback
     *            The Object that implement an interface
     *            RequestCallback{@literal <}ArrayList{@literal <}SimpleListItem>>. This object will be used for
     *            sending callbacks in to UI thread.
     */
    public static RequestTask<ArrayList<SimpleListItem>> merchantGetList(
            ListFilter filter, HashMap<String, Object> params, int flags,
            RequestCallback<ArrayList<SimpleListItem>> callback) {
        Message<ArrayList<SimpleListItem>> message = new Message<>(getContext(),
                MerchantCommand.MERCHANT_GET_LIST, getUrl(), getActiveConnection(), getDevice(), null,
                getAppIdentity(), getProtocolFactory().newRequestBuilder(),
                new MerchantsListJsonParser(), flags);

        if (filter != null)
            filter.applyTo(message);

        message.setCacheFileName(MerchantCommand.MERCHANT_GET_LIST, false);
        message.addParameters(params);

        return new WebServiceAsync<>(getContext(), message, callback).execute();
    }

    /**
     *
     * @param shopIdentity shop identity
     * @param filter count, skip, consumer_id, role_id, query_roles, query_account, query_merchant :can be null
     * @param flags
     * @param callback
     * @return
     */
    public static RequestTask<ConsumerList> merchantConsumerList(Identity shopIdentity,
                                                                 ConsumerFilter filter, int flags, RequestCallback<ConsumerList> callback) {
        Message<ConsumerList> message = new Message<>(getContext(),
                MerchantCommand.MERCHANT_CONSUMER_LIST, getUrl(), getActiveConnection(), getDevice(), shopIdentity,
                getAppIdentity(), getProtocolFactory().newRequestBuilder(),
                getProtocolFactory().newParser(ConsumerList.class), flags);

        if (filter != null)
            filter.applyTo(message);

        return new WebServiceAsync<>(getContext(), message, callback).execute();
    }
    /**
     *
     * @param shopIdentity shop identity
     * @param filter count, skip, consumer_id, role_id, query_roles, query_account, query_merchant :can be null
     * @param flags
     * @param callback
     * @return
     */
    public static RequestTask<ConsumerList> merchantConsumerListByRole(Identity shopIdentity,
                                                                       ConsumerFilter filter, int flags, RequestCallback<ConsumerList> callback) {
        Message<ConsumerList> message = new Message<>(getContext(),
                MerchantCommand.MERCHANT_CONSUMER_LIST_BY_ROLE, getUrl(), getActiveConnection(), getDevice(), shopIdentity,
                getAppIdentity(), getProtocolFactory().newRequestBuilder(),
                getProtocolFactory().newParser(ConsumerList.class), flags);

        if (filter != null)
            filter.applyTo(message);

        return new WebServiceAsync<>(getContext(), message, callback).execute();
    }

    public static RequestTask<InventoryProduct> merchantInventoryConsume(Identity shopIdentity,
                             String itemId, int flags, RequestCallback<InventoryProduct> callback) {
        if (checkApi(callback)) {
            Message<InventoryProduct> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_INVENTORY_CONSUME, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity,
                    getAppIdentity(), getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(InventoryProduct.class), flags);
            message.addParameter("item_id", itemId);

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;

    }

    public static RequestTask<MerchantShopsList> merchantShopsList(ShopFilter filter, int flags,
                                                   RequestCallback<MerchantShopsList> callback) {
        if (checkApi(callback)) {
            Message<MerchantShopsList> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_GET_SHOP_LIST, getUrl(), getActiveConnection(), getDevice(),
                    getMainShopIdentity(), getAppIdentity(), getProtocolFactory().newRequestBuilder(),
                    new MerchantShopsListJsonParser(), flags);
            message.setHashIgnore(true); //fixme delete this
            if (filter != null) {
                filter.applyTo(message);
            }

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    public static RequestTask<MerchantShopInfo> merchantShopGet(Identity shopIdentity,
                                            int flags, RequestCallback<MerchantShopInfo> callback) {
        if (checkApi(callback)) {
            Message<MerchantShopInfo> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_GET_SHOP, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(), getProtocolFactory().newRequestBuilder(),
                    new MerchantShopInfoJsonParser(), flags);
            message.setHashIgnore(true); //fixme delete this

            return new WebServiceAsync<>(getContext(), message,
                    new MerchantShopCallbackWrapper(callback)).execute();
        }
        return null;
    }

    public static RequestTask<OrdersList> merchantGetOrders(OrderFilter filter, int flags,
                                                            RequestCallback<OrdersList> callback) {
        return merchantGetOrders(filter, null, flags, callback);
    }

    public static RequestTask<OrdersList> merchantGetOrders(OrderFilter filter,
                        Identity shopIdentity, int flags, RequestCallback<OrdersList> callback) {
        if (checkApi(callback)) {
            Message<OrdersList> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_GET_ORDERS, getUrl(),
                    getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(OrdersList.class), flags);
            message.setCacheFileName(Message.Command.USER_GET_ORDERS, true);

            if (filter != null) {
                filter.applyTo(message);
            }

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    public static RequestTask<Order> merchantSetOrder(final Order order, final int flags,
                                                      RequestCallback<Order> callback) {
        Identity shopIdentity = getMainShopIdentity();
        if (order.getShopId() == null || order.getShopId().equals(shopIdentity.getId()))
            return merchantSetOrder(order, null, flags, callback);

        final RequestTaskWrapper<Order> wrapper = new RequestTaskWrapper<>();
        wrapper.setCallback(callback, false);
        wrapper.set(shopFindById(order.getShopId(), 0, new RequestCallback<ShopFind>() {
            @Override
            public boolean onComplete(Response<ShopFind> response) {
                if (response.isStatusAccept()) {
                    ShopFind find = response.getResult();
                    if (find.getList().size() > 0) {
                        wrapper.set(merchantSetOrder(order, find.getList().get(0), flags, wrapper));
                        return true;
                    }
                }

                return onError(response.getAnyError());
            }

            @Override
            public boolean onError(RpError error) {
                return wrapper.onError(error);
            }
        }));

        return wrapper;
    }

    // TODO write docs
    public static RequestTask<Order> merchantSetOrder(Order order, Identity shopIdentity, int flags,
                                                      RequestCallback<Order> callback) {
        if (checkApi(callback)) {
            Message<Order> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_SET_ORDER, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(Order.class), flags);

            order.applyTo(message);

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    // TODO write docs
    public static RequestTask<Void> merchantSetOrders(Identity shopIdentity, OrdersList ordersList, int flags,
                                                      RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            Message<Void> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_SET_ORDERS, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(Void.class), flags);

            ordersList.applyTo(message);

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    public static RequestTask<MerchantShopInfo> merchantShopSet(MerchantShop merchantShop,
                                            int flags, RequestCallback<MerchantShopInfo> callback) {
        if (checkApi(callback)) {
            Message<MerchantShopInfo> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_SET_SHOP, getUrl(), getActiveConnection(), getDevice(),
                    merchantShop, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    new MerchantShopInfoJsonParser(), flags);

            merchantShop.applyTo(message);

            return new WebServiceAsync<>(getContext(), message,
                    new MerchantShopCallbackWrapper(callback)).execute();
        }
        return null;
    }

    public static RequestTask<Void> merchantShopDelete(MerchantShop merchantShop, int flags,
                                                       RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            Message<Void> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_SET_SHOP, getUrl(), getActiveConnection(), getDevice(),
                    merchantShop, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(Void.class), flags);

            message.addParameter("delete", true);

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    // TODO write docs
    public static RequestTask<Merchant> userMerchantCreate(Identity shopIdentity, String name,
                                                           String adminEmail, String adminPhone,
                                                           int flags, RequestCallback<Merchant> callback) {
        if (checkApi(callback)) {

            Message<Merchant> message = new Message<>(getContext(),
                    MerchantCommand.USER_MERCHANT_CREATE, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(Merchant.class), flags);

            HashMap<String, String> merchant = new HashMap<>();
            merchant.put("str_name", name);
            merchant.put("str_email", adminEmail);
            merchant.put("str_telephone", adminPhone);

            message.addParameter("merchant", merchant);

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    // TODO docs
    public static RequestTask<Asset> merchantAssetSet(Asset asset, Identity shopIdentity, int flags, RequestCallback<Asset> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    Builder.merchantAssetSet(asset, shopIdentity, flags), callback).execute();
        }
        return null;
    }

    public static RequestTask<Asset> merchantAssetDelete(Asset asset, Identity shopIdentity, int flags, RequestCallback<Asset> callback) {
        if (checkApi(callback)) {
            Message<Asset> message = Builder.merchantAssetSet(asset, shopIdentity, flags);
            message.addParameter("delete", true);
            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }
    // TODO docs

    /**
     * change asset location, other properties will not be changed
     * @param asset asset object
     * @param shopIdentity Identity of the shop
     * @param flags flags for request
     * @param callback
     * @return asset object updated
     */
    public static RequestTask<Asset> merchantAssetSetLocation(Asset asset, Identity shopIdentity, int flags, RequestCallback<Asset> callback) {
        if (checkApi(callback)) {
            Message<Asset> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_ASSET_SET, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(Asset.class), flags);
            asset.applyLocationTo(message);
            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    // TODO docs
    public static RequestTask<AssetList> merchantAssetList(AssetFilter filter,
                                                           Identity shopIdentity, int flags, RequestCallback<AssetList> callback) {
        if(checkApi(callback)) {
            Message<AssetList> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_ASSET_LIST, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(AssetList.class), flags);

            if(filter != null)
                filter.applyTo(message);

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    // TODO docs
    public static RequestTask<HkStructure> merchantStructureSet(HkStructure structure, Identity shopIdentity, int flags, RequestCallback<HkStructure> callback) {
        if (checkApi(callback)) {
            Message<HkStructure> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_ASSET_STRUCTURE_SET, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(HkStructure.class), flags);

            message.addParameter("structure", structure.toParams());

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    // TODO docs
    public static RequestTask<UserRolesList> merchantUserRolesList(
                                                ListFilter filter, String[] roleTypes,
                                                Identity shopIdentity,
                                             int flags, RequestCallback<UserRolesList> callback) {
        if(checkApi(callback)) {
            Message<UserRolesList> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_USER_ROLES_LIST, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(UserRolesList.class), flags);

            if(filter != null)
                filter.applyTo(message);

            if (roleTypes != null && roleTypes.length > 0)
                message.addParameter("roles", new ArrayList<>(Arrays.asList(roleTypes)));

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    // TODO write docs
    public static RequestTask<UserRole> merchantUserRoleSet(UserRole role, Identity shopIdentity, int flags,
                                                    RequestCallback<UserRole> callback) {
        if (checkApi(callback)) {
            Message<UserRole> message = new Message<>(getContext(), MerchantCommand.MERCHANT_USER_ROLE_SET, getUrl(),
                    getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(), getProtocolFactory().newParser(UserRole.class), flags);
            role.applyTo(message);
            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    // TODO write docs
    public static RequestTask<UserRole> merchantUserRoleDelete(String roleId, String userId, Identity shopIdentity, int flags,
                                                            RequestCallback<UserRole> callback) {
        if (checkApi(callback)) {
            Message<UserRole> message = new Message<>(getContext(), MerchantCommand.MERCHANT_USER_ROLE_SET, getUrl(),
                    getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(), getProtocolFactory().newParser(UserRole.class), flags);
            message.addParameter("role_id", roleId);
            message.addParameter("user_id", userId);
            message.addParameter("delete", true);
            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    // TODO write docs
    public static RequestTask<HkQrCode> merchantQrSet(HkQrCode qrCode, int flags,
                                                      RequestCallback<HkQrCode> callback) {
        if (checkApi(callback)) {

            Message<HkQrCode> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_QR_SET, getUrl(), getActiveConnection(), getDevice(),
                    getMainShopIdentity(), getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(HkQrCode.class), flags);

            message.addParameter("id", qrCode.getId());
            message.addParameter("merchant", qrCode.getMerchantId());
            message.addParameter("object_type", qrCode.getObjectType());

            if (qrCode.getObject() != null)
                message.addParameter("object", qrCode.getObject().getId());
            else
                message.addParameter("object", "null");

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    public static RequestTask<Poll> merchantPollDelete(Identity shopIdentity, String pollId, int flags, RequestCallback<Poll> callback) {
        if (checkApi(callback)) {
            Message<Poll> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_POLL_SET, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(Poll.class), flags);

            message.addParameter("id", pollId);
            message.addParameter("delete", true);

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    public static RequestTask<Poll> merchantPollSet(Poll poll, Identity shopIdentity, int flags, RequestCallback<Poll> callback) {
        if (checkApi(callback)) {
            Message<Poll> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_POLL_SET, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(Poll.class), flags);

            poll.applyTo(message);

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    public static RequestTask<Transactions> merchantTransactionsList(TransactionFilter filter,
                                                                     Identity shopIdentity, int flags, RequestCallback<Transactions> callback) {
        if(checkApi(callback)) {
            Message<Transactions> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_LIST_TRANSACTIONS, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(Transactions.class), flags);
            if(filter != null)
                filter.applyTo(message);
            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    private static class MerchantShopCallbackWrapper extends RequestCallbackWrapper<MerchantShopInfo> {

        public MerchantShopCallbackWrapper(RequestCallback<MerchantShopInfo> callback) {
            super(callback);
        }

        @Override
        protected boolean invokeOnComplete(Response<MerchantShopInfo> response) {
            MerchantShop shop = response.getResult().getMerchantShop();
            if (TextUtils.isEmpty(shop.getPassword())) {
                if (shop.getId() != null) {
                    Identity shopIdentity = response.getMessage().getShopIdentity();
                    if (shopIdentity == null)
                        shopIdentity = getMainShopIdentity();
                    if (shopIdentity != null && shop.getId().equals(shopIdentity.getId())) {
                        shop.setPassword(shopIdentity.getPassword());
                    }
                }
            }
            return super.invokeOnComplete(response);
        }
    }
    /**
     * The request to load the list of products for specified Merchant.
     *
     * @param shopIdentity Identity of the shop.
     * @param flags Flags for request.
     */
    private static Message<ShopProducts> merchantGetProducts(MerchantProductsFilter filter, Identity shopIdentity, int flags) {
        Message<ShopProducts> message = new Message<>(getContext(),
                MerchantCommand.MERCHANT_GET_PRODUCT_LIST, getUrl(),
                getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                getProtocolFactory().newRequestBuilder(), getProtocolFactory().newParser(ShopProducts.class), flags);
        message.setCacheFileName(MerchantCommand.MERCHANT_GET_PRODUCT_LIST, true);
        message.addParameter(Message.Param.QUERY_LANGUAGE, getSuitableLanguage());

        if (filter != null)
            filter.applyTo(message);

        return message;
    }
    public static RequestTask<ShopProducts> merchantProductList(MerchantProductsFilter filter,
                                                           Identity shopIdentity, int flags, RequestCallback<ShopProducts> callback) {
        if(checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    merchantGetProducts(filter, shopIdentity, flags),
                    callback).execute();
        }
        return null;
    }

    private static Message<Product> merchantProductSet(Product product, Identity shopIdentity,
                                                  int flags) {
        Message<Product> message = new Message<>(getContext(),
                MerchantCommand.MERCHANT_PRODUCT_SET, getUrl(), getActiveConnection(), getDevice(),
                shopIdentity, getAppIdentity(),
                getProtocolFactory().newRequestBuilder(),
                getProtocolFactory().newParser(Product.class), flags);
        message.addParameter("product", product.toParams());
        return message;
    }

    public static RequestTask<Product> merchantProductSet(Product product, Identity shopIdentity, RequestCallback<Product> callback, int flags) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    merchantProductSet(product, shopIdentity, flags),
                    callback).execute();
        }
        return null;
    }

    public static RequestTask<CalendarEvent> merchantEventSet(CalendarEvent calendarEvent,
                                      String objectId, String objectType, Identity shopIdentity,
                                      RequestCallback<CalendarEvent> callback, int flags) {
        if (checkApi(callback)) {
            Message<CalendarEvent> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_SET_EVENT, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(CalendarEvent.class), flags);

            calendarEvent.applyTo(message);

            message.addParameter("object", objectId);
            message.addParameter("object_type", objectType);

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    public static RequestTask<CalendarEvent> merchantEventDelete(CalendarEvent calendarEvent,
                                                                 Identity shopIdentity,
                                                              RequestCallback<CalendarEvent> callback, int flags) {
        if (checkApi(callback)) {
            Message<CalendarEvent> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_SET_EVENT, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(CalendarEvent.class), flags);

            calendarEvent.applyTo(message);
            message.addParameter("object", "");
            message.addParameter("object_type", "");
            message.addParameter("delete", true);

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    public static RequestTask<Void> merchantSendChatMessages(ChatMessage chatMessage, int flags,
                                                             RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    merchantSendChatMessages(chatMessage, flags), callback).execute();
        }
        return null;
    }

    public static Message<Void> merchantSendChatMessages(ChatMessage chatMessage, int flags) {
        Message<Void> message = new Message<>(getContext(),
                MerchantCommand.MERCHANT_SEND_CHAT_MESSAGE, getUrl(),
                getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                getProtocolFactory().newRequestBuilder(), getProtocolFactory().newParser(Void.class), flags);
        chatMessage.applyTo(message);
        return message;
    }

    public static RequestTask<Void> merchantVote(UserComment userComment, int flags,
                                             RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    merchantVote(userComment, flags), callback).execute();
        }
        return null;
    }

    public static Message<Void> merchantVote(UserComment userComment, int flags) {
        Message<Void> message = new Message<>(getContext(), MerchantCommand.MERCHANT_VOTE, getUrl(),
                getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                getProtocolFactory().newRequestBuilder(), getProtocolFactory().newParser(Void.class), flags);

        userComment.applyTo(message);

        return message;
    }

    public static RequestTask<ChatMessages> merchantGetChatMessages(ChatMessageFilter filter, int flags,
                                                                RequestCallback<ChatMessages> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    userGetChatMessages(filter, flags), callback).execute();
        }
        return null;
    }

    public static RequestTask<Void> merchantReportRun(String reportId, String email,
                                                      HashMap<String, Serializable> params, int flags,
                                                      RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            Message<Void> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_REPORT_RUN, getUrl(), getActiveConnection(), getDevice(),
                    getMainShopIdentity(), getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(Void.class), flags);

            message.addParameter("id", reportId);
            message.addParameter("parameters", params);
            if (email != null)
                message.addParameter("email", email);

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    public static RequestTask<HkStructureList> assetStructureList(String structureId,
                                                                  Identity shopIdentity, int flags, RequestCallback<HkStructureList> callback) {
        return assetStructureList(new String[] {structureId}, shopIdentity, flags, callback);
    }

    public static RequestTask<HkStructureList> assetStructureList(String[] structureIds,
                                                                  Identity shopIdentity, int flags, RequestCallback<HkStructureList> callback) {
        if (HonkioApi.checkApi(callback)) {

            Message<HkStructureList> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_ASSET_STRUCTURE_LIST, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    HonkioApi.getProtocolFactory().newRequestBuilder(),
                    HonkioApi.getProtocolFactory().newParser(HkStructureList.class), flags);

            message.addParameter("query_id", structureIds);

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    private static Message<ChatMessages> userGetChatMessages(ChatMessageFilter filter, int flags) {
        Message<ChatMessages> message = new Message<>(getContext(),
                MERCHANT_GET_CHAT, getUrl(),
                getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                getProtocolFactory().newRequestBuilder(), getProtocolFactory().newParser(ChatMessages.class), flags);

        if (filter != null) {
            filter.applyTo(message);
        }

        return message;
    }

    public static RequestTask<EarningsList> merchantGetEarnings(EarningsFilter filter, Identity shopIdentity, int flags, RequestCallback<EarningsList> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    merchantGetEarnings(filter, shopIdentity, flags), callback).execute();
        }
        return null;
    }

    public static Message<EarningsList> merchantGetEarnings(EarningsFilter filter, Identity shopIdentity, int flags) {
        Message<EarningsList> message = new Message<>(getContext(),
                MerchantCommand.MERCHANT_GET_EARNINGS, getUrl(),
                getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                getProtocolFactory().newRequestBuilder(), getProtocolFactory().newParser(EarningsList.class), flags);
        if (filter != null) {
            filter.applyTo(message);
        }
        return message;
    }

    public static RequestTask<HKLink> merchantSetLink(HKLink link, Identity shopIdentity, int flags,
                                                      RequestCallback<HKLink> callback) {
        if (HonkioApi.checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    Builder.merchantSetLink(link, shopIdentity, flags), callback).execute();
        }
        return null;
    }


    public static RequestTask<Void> merchantDeleteLink(String linkId, Identity shopIdentity, int flags,
                                                      RequestCallback<Void> callback) {
        if (HonkioApi.checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    Builder.merchantDeleteLink(linkId, shopIdentity, flags), callback).execute();
        }
        return null;
    }

    public static RequestTask<HKLinksList> merchantGetLinks(HKLinkFilter filter,
                                                            Identity shopIdentity, int flags,
                                                            RequestCallback<HKLinksList> callback) {
        if (HonkioApi.checkApi(callback)) {
            Message<HKLinksList> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_GET_LINK, getUrl(),
                    getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(HKLinksList.class), flags);

            if (filter != null)
                filter.applyTo(message);

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    public static class Builder {

        public static Message<Asset> merchantAssetSet(Asset asset, Identity shopIdentity,
                                                          int flags) {
            Message<Asset> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_ASSET_SET, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(Asset.class), flags);

            asset.applyTo(message);

            return message;
        }

        public static Message<HKLink> merchantSetLink(HKLink link, Identity shopIdentity,
                                                      int flags) {
            Message<HKLink> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_SET_LINK, getUrl(),
                    getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(HKLink.class), flags);

            link.applyTo(message);

            return message;
        }


        public static Message<Void> merchantDeleteLink(String linkId, Identity shopIdentity,
                                                       int flags) {
            Message<Void> message = new Message<>(getContext(),
                    MerchantCommand.MERCHANT_SET_LINK, getUrl(),
                    getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                    getProtocolFactory().newRequestBuilder(),
                    getProtocolFactory().newParser(Void.class), flags);

            message.addParameter("id", linkId);
            message.addParameter("delete", "yes");

            return message;
        }
    }

}
