package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Asset;
import com.riskpointer.sdk.api.model.entity.HKLink;
import com.riskpointer.sdk.api.model.entity.HKLinksList;
import com.riskpointer.sdk.api.model.entity.HkObjectType;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class HKLinksListJsonParser extends BaseJsonResponseParser<HKLinksList> {

    private HKLinkJsonParser mLinkParser = new HKLinkJsonParser();

    @Override
    protected HKLinksList parseJson(Message<?> message, JSONObject json) {
        ArrayList<HKLink> links = new ArrayList<>();
        HashMap<String, Asset> assetsMap = new HashMap<>();

        JSONArray linksJson = json.optJSONArray("links");
        if (linksJson != null) {
            for (int i = 0; i < linksJson.length(); i++) {
                links.add(mLinkParser.parseJson(message, linksJson.optJSONObject(i)));
            }
        }

        JSONObject assetsJson = json.optJSONObject(HkObjectType.ASSET);
        if (assetsJson != null) {
            Iterator<String> iterator = assetsJson.keys();
            while (iterator.hasNext()) {
                String assetId = iterator.next();
                JSONObject assetJson = assetsJson.optJSONObject(assetId);

                if (assetJson != null) {
                    Asset asset = AssetJsonParser.parseAsset(assetJson);
                    asset.setId(assetId);
                    assetsMap.put(assetId, asset);
                }
            }
        }

        HKLinksList linksList = new HKLinksList();
        linksList.setList(links);
        linksList.setAssetsMap(assetsMap);

        return linksList;
    }
}
