package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Asset;
import com.riskpointer.sdk.api.model.entity.AssetList;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Parse Merchants list from JSON.
 */
public class AssetListJsonParser extends BaseJsonResponseParser<AssetList> {

    @Override
    public AssetList parseJson(Message<?> message, JSONObject json) {
        AssetList assetList = new AssetList();
        ArrayList<Asset> list = new ArrayList<>();

        JSONArray assetListJson = json.optJSONArray("assets");
        if (assetListJson != null) {
            for (int i = 0; i < assetListJson.length(); i++) {
                JSONObject assetJson = assetListJson.optJSONObject(i);
                if (assetJson != null) {
                    Asset asset = AssetJsonParser.parseAsset(assetJson);
                    if (asset != null)
                        list.add(asset);
                }
            }
        }

        assetList.setList(list);
        return assetList;
    }

}
