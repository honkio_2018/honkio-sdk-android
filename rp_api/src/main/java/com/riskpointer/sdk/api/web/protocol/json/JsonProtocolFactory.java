/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.AppInfo;
import com.riskpointer.sdk.api.model.entity.ApplicantsList;
import com.riskpointer.sdk.api.model.entity.Asset;
import com.riskpointer.sdk.api.model.entity.AssetList;
import com.riskpointer.sdk.api.model.entity.CalendarEvent;
import com.riskpointer.sdk.api.model.entity.CalendarSchedule;
import com.riskpointer.sdk.api.model.entity.ChatMessages;
import com.riskpointer.sdk.api.model.entity.ConsumerList;
import com.riskpointer.sdk.api.model.entity.EarningsList;
import com.riskpointer.sdk.api.model.entity.HKDevice;
import com.riskpointer.sdk.api.model.entity.HKLink;
import com.riskpointer.sdk.api.model.entity.HKLinksList;
import com.riskpointer.sdk.api.model.entity.HkQrCode;
import com.riskpointer.sdk.api.model.entity.HkStructure;
import com.riskpointer.sdk.api.model.entity.HkStructureList;
import com.riskpointer.sdk.api.model.entity.Inventory;
import com.riskpointer.sdk.api.model.entity.InventoryProduct;
import com.riskpointer.sdk.api.model.entity.Invitation;
import com.riskpointer.sdk.api.model.entity.InvitationsList;
import com.riskpointer.sdk.api.model.entity.InvoicesList;
import com.riskpointer.sdk.api.model.entity.LaunchpadInfo;
import com.riskpointer.sdk.api.model.entity.MediaFile;
import com.riskpointer.sdk.api.model.entity.MediaFileList;
import com.riskpointer.sdk.api.model.entity.Order;
import com.riskpointer.sdk.api.model.entity.OrdersList;
import com.riskpointer.sdk.api.model.entity.Poll;
import com.riskpointer.sdk.api.model.entity.PollsList;
import com.riskpointer.sdk.api.model.entity.Product;
import com.riskpointer.sdk.api.model.entity.PushesList;
import com.riskpointer.sdk.api.model.entity.RateList;
import com.riskpointer.sdk.api.model.entity.Schedule;
import com.riskpointer.sdk.api.model.entity.ServerFile;
import com.riskpointer.sdk.api.model.entity.ServerInfo;
import com.riskpointer.sdk.api.model.entity.ShopFind;
import com.riskpointer.sdk.api.model.entity.ShopInfo;
import com.riskpointer.sdk.api.model.entity.ShopProducts;
import com.riskpointer.sdk.api.model.entity.TagsList;
import com.riskpointer.sdk.api.model.entity.Transaction;
import com.riskpointer.sdk.api.model.entity.Transactions;
import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.model.entity.UserAccessList;
import com.riskpointer.sdk.api.model.entity.UserCommentsList;
import com.riskpointer.sdk.api.model.entity.RolesList;
import com.riskpointer.sdk.api.model.entity.UserRolesList;
import com.riskpointer.sdk.api.model.entity.UserPayment;
import com.riskpointer.sdk.api.model.entity.UserRole;
import com.riskpointer.sdk.api.model.entity.UserRoleDescription;
import com.riskpointer.sdk.api.model.entity.UserRoleDescriptionsList;
import com.riskpointer.sdk.api.model.entity.MediaUrl;
import com.riskpointer.sdk.api.model.entity.merchant.Merchant;
import com.riskpointer.sdk.api.web.protocol.ProtocolFactory;
import com.riskpointer.sdk.api.web.protocol.RequestBuilder;
import com.riskpointer.sdk.api.web.protocol.ResponseParser;
import com.riskpointer.sdk.api.web.protocol.json.merchant.MerchantJsonParser;

/**
 * Is a factory for requests and responses in JSON format.
 * 
 * @author Denis Shurygin
 * 
 */
public class JsonProtocolFactory implements ProtocolFactory {

	@Override
	public RequestBuilder newRequestBuilder() {
		return new JsonRequestBuilder();
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> ResponseParser<T> newParser(Class<T> clazz) {
		// COMMON =================================================================
		if (clazz == Void.class) {
			return (ResponseParser<T>) new SimpleResponseJsonParser();
		}
		if (clazz == String.class) {
			return (ResponseParser<T>) new StringResponseJsonParser();
		}
		else if (clazz == HKDevice.class) {
			return (ResponseParser<T>) new HKDeviceJsonParser();
		}
		else if (clazz == ShopFind.class) {
			return (ResponseParser<T>) new ShopFindJsonParser();
		}
		else if (clazz == ShopInfo.class) {
			return (ResponseParser<T>) new ShopInfoJsonParser();
		}
		else if (clazz == ServerInfo.class) {
			return (ResponseParser<T>) new ServerInfoJsonParser();
		}
		else if (clazz == AppInfo.class) {
			return (ResponseParser<T>) new AppInfoJsonParser();
		}
		else if (clazz == ShopProducts.class) {
			return (ResponseParser<T>) new ShopProductsJsonParser();
		}
		else if (clazz == Transactions.class) {
			return (ResponseParser<T>) new TransactionsJsonParser();
		}
		else if (clazz == Transaction.class) {
			return (ResponseParser<T>) new TransactionJsonParser();
		}
		else if (clazz == InvoicesList.class) {
			return (ResponseParser<T>) new InvoicesListJsonParser();
		}
		else if (clazz == ServerFile.class) {
			return (ResponseParser<T>) new ServerFileJsonParser();
		}
		else if (clazz == OrdersList.class) {
			return (ResponseParser<T>) new OrdersListJsonParser();
		}
		else if (clazz == Order.class) {
			return (ResponseParser<T>) new OrderJsonParser();
		}
		else if (clazz == UserRole.class) {
			return (ResponseParser<T>) new UserRoleJsonParser();
		}
		else if (clazz == UserRolesList.class) {
			return (ResponseParser<T>) new UserRolesListJsonParser();
		}
		else if (clazz == ChatMessages.class) {
			return (ResponseParser<T>) new UserMessagesJsonParser();
		}
		else if (clazz == Schedule.class) {
			return (ResponseParser<T>) new ScheduleJsonParser();
		}
		else if (clazz == CalendarSchedule.class) {
			return (ResponseParser<T>) new CalendarScheduleJsonParser();
		}
		else if (clazz == CalendarEvent.class) {
			return (ResponseParser<T>) new CalendarEventJsonParser();
		}
		else if (clazz == UserCommentsList.class) {
			return (ResponseParser<T>) new UserCommentsListJsonParser();
		}
		else if (clazz == TagsList.class) {
			return (ResponseParser<T>) new TagsListJsonParser();
		}
		else if (clazz == ApplicantsList.class) {
			return (ResponseParser<T>) new ApplicantsListJsonParser();
		}
		else if (clazz == Inventory.class) {
			return (ResponseParser<T>) new InventoryJsonParser();
		}
		else if (clazz == InventoryProduct.class) {
			return (ResponseParser<T>) new InventoryProductJsonParser();
		}
		else if (clazz == UserRoleDescription.class) {
			return (ResponseParser<T>) new UserRoleDescriptionJsonParser();
		}
		else if (clazz == UserRoleDescriptionsList.class) {
			return (ResponseParser<T>) new UserRoleDescriptionsListJsonParser();
		}
		else if (clazz == UserAccessList.class) {
			return (ResponseParser<T>) new UserAccessListJsonParser();
		}
		else if (clazz == HkQrCode.class) {
			return (ResponseParser<T>) new QrCodeJsonParser();
		}
		else if (clazz == MediaUrl.class) {
			return (ResponseParser<T>) new MediaUrlJsonParser();
		}
		else if (clazz == MediaFile.class) {
			return (ResponseParser<T>) new MediaFileJsonParser();
		}
		else if (clazz == MediaFileList.class) {
			return (ResponseParser<T>) new MediaFileListJsonParser();
		}
		// USER =================================================================
		else if (clazz == User.class) {
			return (ResponseParser<T>) new UserJsonParser();
		}
		else if (clazz == RolesList.class) {
			return (ResponseParser<T>) new RolesListJsonParser();
		}
		else if (clazz == UserPayment.class) {
			return (ResponseParser<T>) new UserPaymentJsonParser();
		}
		else if (clazz == PushesList.class) {
			return (ResponseParser<T>) new PushesListJsonParser();
		}
		else if (clazz == Invitation.class) {
			return (ResponseParser<T>) new InvitationJsonParser();
		}
		else if (clazz == InvitationsList.class) {
			return (ResponseParser<T>) new InvitationsListJsonParser();
		}
		else if (clazz == LaunchpadInfo.class) {
			return (ResponseParser<T>) new LaunchpadInfoJsonParser();
		}
		// MERCHANT =================================================================
		else if (clazz == Merchant.class) {
			return (ResponseParser<T>) new MerchantJsonParser();
		}
		else if (clazz == AssetList.class) {
			return (ResponseParser<T>) new AssetListJsonParser();
		}
		else if (clazz == Asset.class) {
			return (ResponseParser<T>) new AssetJsonParser();
		}
		else if (clazz == HkStructure.class) {
			return (ResponseParser<T>) new StructureJsonParser();
		}
		else if (clazz == HkStructureList.class) {
			return (ResponseParser<T>) new StructureListJsonParser();
		}
		else if (clazz == Product.class) {
			return (ResponseParser<T>) new ProductJsonParser();
		}
		else if (clazz == RateList.class) {
			return (ResponseParser<T>) new RateListJsonParser();
		}
		else if (clazz == EarningsList.class) {
			return (ResponseParser<T>) new EarningsListJsonParser();
		}
		else if (clazz == HKLink.class) {
			return (ResponseParser<T>) new HKLinkJsonParser();
		}
		else if (clazz == HKLinksList.class) {
			return (ResponseParser<T>) new HKLinksListJsonParser();
		}
		else if (clazz == ConsumerList.class) {
			return (ResponseParser<T>) new ConsumerListJsonParser();
		}
		else if (clazz == PollsList.class) {
			return (ResponseParser<T>) new PollsListJsonParser();
		}
		else if (clazz == Poll.class) {
			return (ResponseParser<T>) new PollJsonParser();
		}
		else
			throw new IllegalArgumentException("Unable to find suitable response parser for class "
				+ clazz.getName() + ".");
	}
}
