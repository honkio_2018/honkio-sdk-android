package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Inventory;
import com.riskpointer.sdk.api.model.entity.InventoryProduct;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Shurygin Denis on 2016-11-03.
 */

public class InventoryJsonParser extends BaseJsonResponseParser<Inventory> {

    @Override
    protected Inventory parseJson(Message<?> message, JSONObject json) {
        Inventory result = new Inventory();

        JSONArray productsListJson = json.optJSONArray("product");
        if(productsListJson != null) {
            for(int i = 0; i < productsListJson.length(); i++) {
                JSONObject productJson = productsListJson.optJSONObject(i);
                InventoryProduct product = new InventoryProduct();
                ParseHelper.parseInventoryProduct(productJson, product);

                if (product.isValid())
                    result.add(product);
            }
        }

        return result;
    }
}
