package com.riskpointer.sdk.api.model.entity;

import com.riskpointer.sdk.api.web.message.filter.AbsFilter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class HKLink extends AbsFilter implements Serializable {

    public static final String FIELD_META_DATA = "metadata";

    private String objectId;
    private String type;

    private String fromObject;
    private String fromType;

    private String toObject;
    private String toType;

    private HashMap<String, Object> mMetaData;

    public HKLink(HKLink link) {
        this(new HashMap<>(link.mMetaData));

        this.objectId = link.objectId;
        this.type = link.type;
        this.fromObject = link.fromObject;
        this.fromType = link.fromType;
        this.toObject = link.toObject;
        this.toType = link.toType;
    }

    public HKLink(HashMap<String, Object> metadata) {
        mMetaData = metadata;
    }

    public HKLink() {
        this((HashMap<String, Object>) null);
    }

    public String getId() {
        return objectId;
    }

    public void setId(String objectId) {
        this.objectId = objectId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setFrom(String object, String type) {
        this.fromObject = object;
        this.fromType = type;
    }

    public String getFromObject() {
        return fromObject;
    }

    public String getFromType() {
        return fromType;
    }

    public void setTo(String object, String type) {
        this.toObject = object;
        this.toType = type;
    }

    public String getToObject() {
        return toObject;
    }

    public String getToType() {
        return toType;
    }

    public void setMetaData(String key, Object value) {
        if (mMetaData == null)
            mMetaData = new HashMap<>();
        mMetaData.put(key, value);
    }

    public <T> T getMetaData(String key) {
        if (mMetaData != null)
            return (T) mMetaData.get(key);
        return null;
    }

    @Override
    public Map<String, Object> toParams() {
        Map<String, Object> params = new HashMap<>();

        applyIfNotNull(params, "id",objectId);
        applyIfNotNull(params, "type_id",type);
        applyIfNotNull(params, "from_object",fromObject);
        applyIfNotNull(params, "from_object_type",fromType);
        applyIfNotNull(params, "to_object",toObject);
        applyIfNotNull(params, "to_object_type",toType);
        applyIfNotNull(params, FIELD_META_DATA, mMetaData);

        return params;
    }
}
