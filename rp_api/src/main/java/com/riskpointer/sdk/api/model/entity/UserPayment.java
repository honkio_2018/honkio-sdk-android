/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;

/**
 * Data object that hold information about payment.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 *
 * @author Denis Shurygin
 */
public class UserPayment implements Serializable {
	private static final long serialVersionUID = 7800070847448555957L;
	
	private long mTimeStamp;
	private double mAmount;
    private String mCurrency;

	/**
	 * Gets time in millis whet payment is executed.
	 * @return Time in millis whet payment is executed.
	 */
	public long getTimeStamp() {
		return mTimeStamp;
	}

	/**
	 * Sets time in millis whet payment is executed.
	 * @param timeStamp Time in millis whet payment is executed.
	 */
	public void setTimeStamp(long timeStamp) {
		this.mTimeStamp = timeStamp;
	}

	public double getAmount() {
		return mAmount;
	}

	public void setAmount(double mAmount) {
		this.mAmount = mAmount;
	}

	public String getCurrency() {
		return mCurrency;
	}

	public void setCurrency(String mCurrency) {
		this.mCurrency = mCurrency;
	}
}
