package com.riskpointer.sdk.api.model.entity;

import com.riskpointer.sdk.api.web.message.filter.ListFilter;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Shurygin Denis
 */

// TODO docs
public class InvitationsList implements Serializable {

    private ListFilter mFilter;
    private ArrayList<Invitation> mList;
    private int mTotalCount;

    /**
     * Gets list of Notifications.
     * @return List of Notifications.
     */
    public ArrayList<Invitation> getList() {
        return mList;
    }

    /**
     * Sets list of Notifications.
     * @param list List of Notifications.
     */
    public void setList(ArrayList<Invitation> list) {
        this.mList = list;
    }

    /**
     * Gets the Filter which was used to get Notifications list.
     * @return Filter which was used to get Notifications list.
     */
    public ListFilter getFilter() {
        return mFilter;
    }

    /**
     * Sets the Filter which was used to get Notifications list.
     * @param filter Filter which was used to get Notifications list.
     */
    public void setFilter(ListFilter filter) {
        this.mFilter = filter;
    }

    // TODO docs
    public int getTotalCount() {
        return mTotalCount;
    }

    // TODO docs
    public void setTotalCount(int count) {
        this.mTotalCount = count;
    }
}
