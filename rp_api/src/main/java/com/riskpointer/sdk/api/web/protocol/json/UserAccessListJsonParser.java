package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.UserAccessList;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * @author Shurygin Denis
 */

public class UserAccessListJsonParser extends BaseJsonResponseParser<UserAccessList> {

    @Override
    protected UserAccessList parseJson(Message<?> message, JSONObject json) {

        UserAccessList access = new UserAccessList();

        JSONArray userAccessJson = json.optJSONArray("user_access");

        if (userAccessJson != null) {
            ArrayList<UserAccessList.Item> list = new ArrayList<>(userAccessJson.length());

            for (int i = 0; i < userAccessJson.length(); i++) {
                JSONObject accessJsonItem = userAccessJson.optJSONObject(i);
                if (accessJsonItem != null) {

                    ArrayList<String> accessArray = new ArrayList<>();
                    JSONArray accessJsonArray = accessJsonItem.optJSONArray("access");
                    if (accessJsonArray != null) {
                        for (int j = 0; j < accessJsonArray.length(); j++) {
                            String assess = accessJsonArray.optString(j, null);
                            if (access != null)
                                accessArray.add(assess);
                        }
                    }

                    UserAccessList.Item item = new UserAccessList.Item();
                    item.setMerchant(accessJsonItem.optString("merchant", "None"));
                    item.setAccess(accessArray);

                    list.add(item);
                }
            }

            access.setList(list);
        }

        access.setMerchants(ParseHelper.parseSimpleMerchantsMap(json.optJSONObject("merchants")));

        return access;
    }

}
