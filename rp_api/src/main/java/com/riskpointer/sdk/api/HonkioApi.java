/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;

import com.riskpointer.sdk.api.exception.WrongKeyException;
import com.riskpointer.sdk.api.model.entity.AppInfo;
import com.riskpointer.sdk.api.model.entity.ApplicantsList;
import com.riskpointer.sdk.api.model.entity.BookedProduct;
import com.riskpointer.sdk.api.model.entity.ChatMessage;
import com.riskpointer.sdk.api.model.entity.ChatMessages;
import com.riskpointer.sdk.api.model.entity.HkQrCode;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.Inventory;
import com.riskpointer.sdk.api.model.entity.Invitation;
import com.riskpointer.sdk.api.model.entity.InvitationsList;
import com.riskpointer.sdk.api.model.entity.Invoice;
import com.riskpointer.sdk.api.model.entity.InvoicesList;
import com.riskpointer.sdk.api.model.entity.LaunchpadInfo;
import com.riskpointer.sdk.api.model.entity.MediaFileList;
import com.riskpointer.sdk.api.model.entity.MediaUrl;
import com.riskpointer.sdk.api.model.entity.Order;
import com.riskpointer.sdk.api.model.entity.OrdersList;
import com.riskpointer.sdk.api.model.entity.Poll;
import com.riskpointer.sdk.api.model.entity.PollsList;
import com.riskpointer.sdk.api.model.entity.PushesList;
import com.riskpointer.sdk.api.model.entity.RateList;
import com.riskpointer.sdk.api.model.entity.RolesList;
import com.riskpointer.sdk.api.model.entity.ServerFile;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.ShopFind;
import com.riskpointer.sdk.api.model.entity.TagsList;
import com.riskpointer.sdk.api.model.entity.Transactions;
import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.model.entity.UserAccessList;
import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.api.model.entity.UserAccount.Type;
import com.riskpointer.sdk.api.model.entity.UserComment;
import com.riskpointer.sdk.api.model.entity.UserCommentsList;
import com.riskpointer.sdk.api.model.entity.UserPayment;
import com.riskpointer.sdk.api.model.entity.UserRole;
import com.riskpointer.sdk.api.model.entity.UserRolesList;
import com.riskpointer.sdk.api.model.process.user.AccountCreationProcessModel;
import com.riskpointer.sdk.api.model.task.RequestTask;
import com.riskpointer.sdk.api.model.task.RequestTaskWrapper;
import com.riskpointer.sdk.api.support.HoneycombAsyncTask;
import com.riskpointer.sdk.api.utils.ApiWebUtils;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.ApiCache;
import com.riskpointer.sdk.api.web.Connection;
import com.riskpointer.sdk.api.web.SimpleAsyncGet;
import com.riskpointer.sdk.api.web.WebServiceAsync;
import com.riskpointer.sdk.api.web.callback.ErrorCallback;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.callback.RequestCallbackWrapper;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.Message.Command;
import com.riskpointer.sdk.api.web.message.Message.Param;
import com.riskpointer.sdk.api.web.message.filter.AbsFilter;
import com.riskpointer.sdk.api.web.message.filter.ApplicantFilter;
import com.riskpointer.sdk.api.web.message.filter.ChatMessageFilter;
import com.riskpointer.sdk.api.web.message.filter.InvitationFilter;
import com.riskpointer.sdk.api.web.message.filter.InviteesFilter;
import com.riskpointer.sdk.api.web.message.filter.InvoiceFilter;
import com.riskpointer.sdk.api.web.message.filter.ListFilter;
import com.riskpointer.sdk.api.web.message.filter.MediaFileFilter;
import com.riskpointer.sdk.api.web.message.filter.OrderFilter;
import com.riskpointer.sdk.api.web.message.filter.PollsFilter;
import com.riskpointer.sdk.api.web.message.filter.PushFilter;
import com.riskpointer.sdk.api.web.message.filter.RatingsFilter;
import com.riskpointer.sdk.api.web.message.filter.TransactionFilter;
import com.riskpointer.sdk.api.web.message.filter.UserCommentsFilter;
import com.riskpointer.sdk.api.web.protocol.ProtocolFactory;
import com.riskpointer.sdk.api.web.protocol.json.JsonProtocolFactory;
import com.riskpointer.sdk.api.web.response.Response;

import java.io.File;
import java.io.InputStream;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;

/**
 * Extension of BaseHonkioApi to work with the user.
 *
 * @author Denis Shurygin
 */
@SuppressWarnings(value = {
        "UnusedReturnValue",
        "WeakerAccess",
        "unqualified-field-access",
        "SameParameterValue"
})
public class HonkioApi extends BaseHonkioApi {

    @SuppressLint("StaticFieldLeak")
    private static ApiState sApiState;

    /**
     * Initialization of API.<br>
     * <p>
     * Without initialization all requests will return an error
     * <b>NO_INITIALIZED</b>.<br>
     * Also, this error can happen when application task was stopped.
     * In this case you should care about restarting application to loadServerData API again.<br>
     *
     * @param context Application context
     * @param apiUrl Url address of the server.
     * @param appIdentity Application identity.
     */
    public static void initInstance(Context context, String apiUrl, Identity appIdentity) {
        initInstance(new ApiState(context, apiUrl, appIdentity));
    }

    //TODO docs
    public static void initInstance(ApiState apiState) {
        initInstance(apiState, new JsonProtocolFactory());
    }

    //TODO docs
    protected static void initInstance(ApiState apiState, ProtocolFactory protocolFactory) {
        sApiState = apiState;
        BaseHonkioApi.initInstance(apiState, protocolFactory);
    }

//-------------------------------------------------------------------------------------------
//-------- Authentication  ------------------------------------------------------------------

    /**
     * The request to login via OAuth2.0 token. After login will created new Connection object.
     *
     * @param token    OAuth token.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}User>. This object will be used to
     *                 send callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<User> loginWithOAuthToken(String token, int flags,
                                                        RequestCallback<User> callback) {
        HonkioApi.logout(Message.FLAG_HIGH_PRIORITY, null);
        if (checkInitialisation(callback)) {

            Message<User> message = MessageBuilder.userGet(null, flags);
            message.setShopIdentity(getMainShopInfo().getMerchant().getLoginShopIdentity());
            message.addParameter(Param.AUTH_TOKEN, token);
            message.addParameter(Param.USER_LOGIN, true);

            WebServiceAsync<User> webService = new UserWebServiceAsync(getContext(), message, callback);
            webService.executeOnExecutor(HoneycombAsyncTask.HIGH_PRIORITY_SERIAL_EXECUTOR);
            return webService;
        }
        return null;
    }

    /**
     * The request to re-login via data from cache. After login will created new Connection object.
     *
     * @param pin      PIN code to decode encrypted OTP.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}User>. This object will be used to
     *                 send callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<User> login(String pin, int flags, RequestCallback<User> callback) {
        return login(pin, null, flags, callback);
    }

    /**
     * The request to re-login via data from cache. After login will created new Connection object.
     *
     * @param pin      PIN code to decode encrypted OTP.
     * @param params   Additional params that will insert into request.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}User>. This object will be used to
     *                 send callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<User> login(String pin, HashMap<String, Object> params, int flags,
                                          RequestCallback<User> callback) {
        if (checkReLogin(callback)) {
            try {
                initConnectionFromCache(getApiPreferences(), pin);
            } catch (WrongKeyException e) {
                e.printStackTrace();
                callback.onError(new RpError(RpError.Api.INVALID_PIN));
                return null;
            }

            if (checkApi(callback)) {
                Message<User> message = MessageBuilder.userGet(params, flags);
                message.setShopIdentity(getMainShopInfo().getMerchant().getLoginShopIdentity());
                message.addParameter(Param.USER_LOGIN, true);
                return new UserWebServiceAsync(getContext(), message, new RequestCallbackWrapper<User>(callback) {
                    @Override
                    public boolean onError(RpError error) {
                        if (error.code == RpError.Common.INVALID_USER_LOGIN) {
                            // This is a one situation when we should increase PIN fails.
                            // Others times it happened in User class.
                            if (getActiveConnection() != null)
                                getActiveConnection().increasePinFails();
                            return super.onError(new RpError(RpError.Api.INVALID_PIN));
                        } else
                            return super.onError(error);
                    }
                }).execute();
            }
        }
        return null;
    }

    /**
     * The request to get access list for provided merchant ant its child merchants.
     *
     * @param parentMerchant Merchant id.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}UserAccessList>. This object will be used to
     *                 send callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<UserAccessList> userAccessList(String parentMerchant, int flags,
                                                         RequestCallback<UserAccessList> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userAccessList(parentMerchant, flags), callback).execute();
        }
        return null;
    }

    /**
     * The request to get access list only for provided merchant without its child merchants.
     *
     * @param merchant Merchant id.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}UserAccessList>. This object will be used to
     *                 send callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<UserAccessList> userMerchantAccessList(String merchant, int flags,
                                                         RequestCallback<UserAccessList> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userMerchantAccessList(merchant, flags), callback).execute();
        }
        return null;
    }

//-------------------------------------------------------------------------------------------
//-------- User get        ------------------------------------------------------------------

    /**
     * The request to get personal information of the user.
     *
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}User>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<User> userGet(int flags, RequestCallback<User> callback) {
        return userGet(null, flags, callback);
    }

    /**
     * The request to get personal information of the user.
     *
     * @param params   Additional params that will insert into request.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}User>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<User> userGet(HashMap<String, Object> params, int flags,
                                            RequestCallback<User> callback) {
        if (checkApi(callback)) {
            return new UserWebServiceAsync(getContext(), MessageBuilder.userGet(params, flags), callback).execute();
        }
        return null;
    }

    /**
     * Request to get list of transactions for current user and main Shop by specified filter.
     *
     * @param filter   Filter for request.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}UserTransactions>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<Transactions> userGetTransactions(TransactionFilter filter, int flags,
                                                        RequestCallback<Transactions> callback) {
        return userGetTransactions(filter, getMainShopIdentity(), flags, callback);
    }

    /**
     * Request to get list of transactions for current user and specified shop by specified filter.
     *
     * @param filter       Filter for request.
     * @param shopIdentity Identity of the specified shop.
     * @param flags        Flags for request.
     * @param callback     The Object that implement an interface RequestCallback
     *                     {@literal <}UserTransactions>. This object will be used
     *                     to send callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<Transactions> userGetTransactions(TransactionFilter filter,
                        Identity shopIdentity, int flags, RequestCallback<Transactions> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(), MessageBuilder
                    .userGetTransactions(filter, shopIdentity, flags), callback).execute();
        }
        return null;
    }

    // TODO write docs
    public static RequestTask<TagsList> userGetTags(Identity shopIdentity, int flags,
                                                    RequestCallback<TagsList> callback) {
        return new WebServiceAsync<>(getContext(), MessageBuilder
                .userGetTags(shopIdentity, flags), callback).execute();
    }

    // TODO write docs
    public static RequestTask<UserCommentsList> userGetComments(UserCommentsFilter filter,
                                            Identity shopIdentity,
                                            int flags, RequestCallback<UserCommentsList> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(), MessageBuilder
                    .userGetComments(filter, shopIdentity, flags), callback).execute();
        }
        return null;
    }

    /**
     * Request to load some file from the server.
     *
     * @param fileName Name of file that will be loaded.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}UserFile>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<ServerFile> userGetFile(String fileName, int flags,
                                                      RequestCallback<ServerFile> callback) {
        if (checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(), MessageBuilder
                    .userGetFile(fileName, flags), callback).execute();
        }
        return null;
    }

//-------------------------------------------------------------------------------------------
//-------- User update     ------------------------------------------------------------------

    /**
     * The request to update personal information of the user.
     *
     * @param user     User object that contains information to change.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}User>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<User> userUpdate(User user, int flags,
                                               RequestCallback<User> callback) {
        return userUpdate(user.toParams(), flags, callback);
    }

    /**
     * The Request to update User photo.
     *
     * @param photo    Photo file.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}User>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<Void> userUpdatePhoto(File photo, int flags,
                                                    RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            Message<Void> message = new Message<>(getContext(), Command.USER_SET_PHOTO, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(Void.class), flags);
            message.setSupportOffline(false);

            return new WebServiceAsync<>(getContext(), message, photo, callback).execute();
        }
        return null;
    }

    /**
     * The Request to update User photo.
     *
     * @param photoInputStream Photo InputStream.
     * @param mediaType        Photo media type.
     * @param flags            Flags for request.
     * @param callback         The Object that implement an interface RequestCallback
     *                         {@literal <}User>. This object will be used to send
     *                         callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour see
     * {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<Void> userUpdatePhoto(InputStream photoInputStream,
                                MediaType mediaType, int flags, RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            Message<Void> message = new Message<>(getContext(), Command.USER_SET_PHOTO, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(Void.class), flags);
            message.setSupportOffline(false);

            WebServiceAsync<Void> webService = new WebServiceAsync<>(getContext(),
                    message, photoInputStream, mediaType, callback);
            webService.execute();
            return webService;
        }
        return null;
    }

    /**
     * The Request to update User video.
     *
     * @param videoInputStream Photo InputStream.
     * @param mediaType        Photo media type.
     * @param flags            Flags for request.
     * @param callback         The Object that implement an interface RequestCallback
     *                         {@literal <}User>. This object will be used to send
     *                         callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour see
     * {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<Void> userUpdateVideo(InputStream videoInputStream,
                                                    MediaType mediaType, int flags, RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            Message<Void> message = new Message<>(getContext(), Command.USER_SET_MEDIA, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(Void.class), flags);
            message.setSupportOffline(false);

            WebServiceAsync<Void> webService = new WebServiceAsync<>(getContext(),
                    message, videoInputStream, mediaType, callback);
            webService.execute();
            return webService;
        }
        return null;
    }

    /**
     * @param id         id of media
     * @param object     id  of type
     * @param objectType object type (ex. "user_role", 'asset')
     * @param flags
     * @param callback
     * @return id, url and token, that will be used for sending media
     * to clear the link to media file with particular id - use this commant with id = id_of_object, object = "", objectType = ""
     * to create new link to media file for ex. user role use this command with id = null, object = roleId, objectType = "user_role"
     */
    private static RequestTask<MediaUrl> userGetMediaUrl(Identity shopIdentity, String id, String object, String objectType,  HashMap<String, Object> properties,int flags, RequestCallback<MediaUrl> callback) {
        if (checkApi(callback)) {
            Message<MediaUrl> message = new Message<>(getContext(), Command.USER_SET_MEDIA, getUrl(),
                    getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(MediaUrl.class), flags);
            message.setSupportOffline(false);
            message.addParameter("access",  "public");
            if (id != null) message.addParameter("id",  id);
            message.addParameter("object",  object);
            message.addParameter("object_type",  objectType);
            if (properties != null) message.addParameter("properties", properties);
            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    public static RequestTask<MediaUrl> userClearMediaUrl(String id, int flags, RequestCallback<MediaUrl> callback) {
        return userClearMediaUrl(null, id, flags, callback);
    }

    public static RequestTask<MediaUrl> userClearMediaUrl(Identity shopIdentity, String id, int flags, RequestCallback<MediaUrl> callback) {
        return userGetMediaUrl(shopIdentity, id, "", "", null, flags, callback);
    }

    public static RequestTask<MediaUrl> userGetMediaUrl(String object, String objectType, int flags, RequestCallback<MediaUrl> callback) {
        return userGetMediaUrl(null, object, objectType,flags, callback);
    }

    public static RequestTask<MediaUrl> userGetMediaUrl(Identity shopIdentity, String object, String objectType, int flags, RequestCallback<MediaUrl> callback) {
        return userGetMediaUrl(shopIdentity, null, object, objectType,null, flags, callback);
    }

    public static RequestTask<MediaUrl> userGetMediaUrl(String object, String objectType, HashMap<String, Object> properties, int flags, RequestCallback<MediaUrl> callback) {
        return userGetMediaUrl(null, object, objectType, properties, flags, callback);
    }

    public static RequestTask<MediaUrl> userGetMediaUrl(Identity shopIdentity, String object, String objectType, HashMap<String, Object> properties, int flags, RequestCallback<MediaUrl> callback) {
        return userGetMediaUrl(shopIdentity, null, object, objectType, properties, flags, callback);
    }

    /**
     *
     * @param file name of media file
     * @param url url, which get from userGetMediaUrl
     * @param flags
     * @param callback
     * @return url to new media file
     */
    public static RequestTask<Void> userUpdateMedia(File file,
                                                    MediaUrl url, int flags,
                                                    RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            Message<Void> message = MessageBuilder.userUpdateMedia(url, flags);
            return new WebServiceAsync<>(getContext(), message, file, callback)
                    .execute();
        }
        return null;
    }

    // TODO write docs
    public static RequestTask<Void> userUpdateMedia(InputStream inputStream, MediaType mediaType,
                                                    MediaUrl url, int flags,
                                                    RequestCallback<Void> callback) {
        if (checkInitialisation(callback)) {
            Message<Void> message = MessageBuilder.userUpdateMedia(url, flags);
            return new WebServiceAsync<>(getContext(), message, inputStream, mediaType, callback)
                    .execute();
        }
        return null;
    }

    /**
     *
     * @param filter see MediaFileList
     * @param flags
     * @param callback
     * @return list of media file links according to filter
     */
    public static RequestTask<MediaFileList> userMediaFilesList(MediaFileFilter filter, int flags, RequestCallback<MediaFileList> callback) {
        if (checkInitialisation(callback)) {
            Message<MediaFileList> message = new Message<>(getContext(), Command.USER_GET_MEDIA_FILES_LIST, getUrl(),
                    null, getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(MediaFileList.class), flags);
            message.setSupportOffline(false);
            if (filter != null) {
                filter.applyTo(message);
            }
            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

    /**
     *
     * @param filter see MediaFileList
     * @param flags
     * @param callback
     * @return list of media file links according to filter
     */
    public static RequestTask<MediaFileList> userParentOrderMediaFilesList(MediaFileFilter filter, int flags, RequestCallback<MediaFileList> callback) {
        if (checkInitialisation(callback)) {
            Message<MediaFileList> message = new Message<>(getContext(), Command.USER_GET_MEDIA_FILES_LIST, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(MediaFileList.class), flags);
            message.setSupportOffline(false);
            if (filter != null) {
                filter.applyTo(message);
            }
            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }


    /**
     * Request to add user payment account into accounts list. This method adds account without any
     * initialization, use {@link AccountCreationProcessModel}
     * to correct account adding.
     *
     * @param account  Account object with params to adding.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}User>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<User> userAddAccount(UserAccount account, int flags,
                                                   RequestCallback<User> callback) {
        if (checkApi(callback)) {
            return new UserWebServiceAsync(getContext(),
                    MessageBuilder.userAddAccount(account, flags), callback).execute();
        }
        return null;
    }

    /**
     * Request to editing user accounts.
     *
     * @param account  The action with edited params.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}User>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<User> userEditAccount(UserAccount account, int flags,
                                                    RequestCallback<User> callback) {
        if (checkApi(callback)) {
            return new UserWebServiceAsync(getContext(),
                    MessageBuilder.userEditAccount(account, flags), callback).execute();
        }
        return null;
    }

    /**
     * Request to move user account.
     *
     * @param account     The account that will moved.
     * @param newPosition New position for the account.
     * @param flags       Flags for request.
     * @param callback    The Object that implement an interface RequestCallback
     *                    {@literal <}User>. This object will be used to send
     *                    callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<User> userMoveAccount(UserAccount account, int newPosition, int flags,
                                                    RequestCallback<User> callback) {
        if (checkApi(callback)) {
            return new UserWebServiceAsync(getContext(),
                    MessageBuilder.userMoveAccount(account, newPosition, flags), callback)
                .execute();
        }
        return null;
    }

    /**
     * Request to delete user accounts.
     *
     * @param account  The action that will deleted.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}User>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<User> userDeleteAccount(UserAccount account, int flags,
                                                      RequestCallback<User> callback) {
        if (checkApi(callback)) {
            return new UserWebServiceAsync(getContext(),
                    MessageBuilder.userDeleteAccount(account, flags), callback).execute();
        }
        return null;
    }

    /**
     * Private method that update user by specified params. Used in methods to work
     * with User Accounts.
     *
     * @param params   Params that will insert into request.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}User>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    private static RequestTask<User> userUpdate(Map<String, Object> params, int flags, RequestCallback<User> callback) {
        if (checkApi(callback)) {
            return new UserWebServiceAsync(getContext(),
                    MessageBuilder.userUpdate(params, flags), callback).execute();
        }
        return null;
    }

    /**
     * Request to change password of the current User. If old password is
     * wrong it will be handled like wrong pin.
     *
     * @param oldPassword Old password.
     * @param newPassword New password.
     * @param flags       Flags for request.
     * @param callback    The Object that implement an interface RequestCallback
     *                    {@literal <}User>. This object will be used to send
     *                    callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour see
     * {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<User> userUpdatePassword(String oldPassword, String newPassword,
                                                       int flags, RequestCallback<User> callback) {
        if (checkInitialisation(callback)) {
            Message<User> message = new Message<>(getContext(), Command.USER_SET, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(User.class), flags);
            message.setCacheFileName(Command.USER_SET, false);
            message.setSupportOffline(false);

            if (oldPassword != null)
                message.addParameter(Param.LOGIN_PASSWORD, oldPassword);

            WebServiceAsync<User> webService = new ChangePasswordWebServiceAsync(getContext(), message, callback, oldPassword, newPassword);
            webService.execute();
            return webService;
        }
        return null;
    }

    /**
     * Request to put or edit values form the special dictionary map of the User.
     *
     * @param dict     HashMap that contains a key value pairs to write in to user dictionary map.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}User>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<User> userUpdateDictValues(HashMap<String, String> dict, int flags,
                                                         RequestCallback<User> callback) {
        if (dict == null || dict.size() == 0)
            throw new InvalidParameterException("Dict must contain values.");
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userUpdateDictValues(dict, flags), callback).execute();
        }
        return null;
    }

//-------------------------------------------------------------------------------------------
//-------- User check      ------------------------------------------------------------------

    /**
     * The request to get check user's password
     *
     * @param flags    Password for checking
     * @param flags    Flags for request
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}User>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<User> userCheckPassword(String password, int flags,
                                                      RequestCallback<User> callback) {
        if (checkApi(callback)) {
            HashMap<String, Object> map = new HashMap<>();
            map.put(Param.LOGIN_PASSWORD, password);
            return userGet(map, flags, new RequestCallbackWrapper<User>(callback) {
                @Override
                public boolean onError(RpError error) {
                    if (error.code == RpError.Common.INVALID_USER_LOGIN) {
                        // This is an exception when we should increase PIN fails.
                        // Others times it happened in User class.
                        getActiveConnection().increasePinFails();
                    }
                    return super.onError(error);
                }
            });
        }
        return null;
    }

    // TODO write docs
    public static RequestTask<Invitation> userDonate(Identity shopIdentity, UserAccount account, double amount,
                                                     String userEmail, String message, int flags,
                                                     RequestCallback<Invitation> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userDonate(shopIdentity, account, amount, userEmail, message, flags),
                    callback).execute();
        }
        return null;
    }

    /**
     *
     * @param invitationId invitation which should be accepted
     * @param invitationStatus invitation status (ex. accept when user accept it and reject when it is rejected)
     * @param flags
     * @param callback
     * @return updated invitation
     */
    public static RequestTask<Invitation> userAcceptInvitation(String invitationId, String invitationStatus, int flags,
                                                               RequestCallback<Invitation> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userAcceptInvitation(invitationId, invitationStatus, flags), callback).execute();
        }
        return null;
    }

    // TODO write docs
    public static RequestTask<Invitation> userAcceptInvitation(String invitationId, int flags, RequestCallback<Invitation> callback) {
        return userAcceptInvitation(invitationId, null, flags, callback);
    }

    // TODO write docs
    public static RequestTask<Void> userVote(UserComment userComment, int flags,
                                             RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userVote(userComment, flags), callback).execute();
        }
        return null;
    }

    public static RequestTask<RateList> getRate(RatingsFilter filter, int flags, RequestCallback<RateList> callback) {
        if (checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.getRate(filter, flags), callback).execute();
        }
        return null;
    }
    @Deprecated // TODO Should be refactored and renamed
    public static RequestTask<Void> userApplyForOrder(String orderId, String proposalTime,
                                                      int flags, RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userApplyForOrder(orderId, proposalTime, flags), callback)
                .execute();
        }
        return null;
    }

    @Deprecated // TODO Should be refactored and renamed
    public static RequestTask<Void> userDeleteApplyForOrder(String orderId, int flags,
                                                            RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userDeleteApplyForOrder(orderId, flags), callback).execute();
        }
        return null;
    }

    public static RequestTask<ApplicantsList> orderGetApplicants(ApplicantFilter filter, int flags,
                                                     RequestCallback<ApplicantsList> callback) {
        return orderGetApplicants(filter, null, flags, callback);
    }

    public static RequestTask<ApplicantsList> orderGetApplicants(ApplicantFilter filter,
                     Identity shopIdentity, int flags, RequestCallback<ApplicantsList> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.orderGetApplicants(filter, shopIdentity, flags), callback).execute();
        }
        return null;
    }

    /**
     * Request to ask server for new temporary password when user forgot his password.
     * Password will send on the User email.
     *
     * @param email    User mail that used for login.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}Response>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return Task interface implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Service has priority: No queue.
     */
    public static RequestTask<Void> userLostPassword(String email, int flags,
                                                     RequestCallback<Void> callback) {
        if (checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userLostPassword(email, flags), callback).execute();
        }
        return null;
    }

    /**
     * Request to agree current version of Terms of Use.
     *
     * @param touUid   UID of terms that should be accepted.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}User>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<Void> userTouAgree(String touUid, int flags, RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userTouAgree(touUid, flags), callback).execute();
        }
        return null;
    }

    // TODO write docs
    public static RequestTask<Inventory> userGetInventory(int flags,
                                                          RequestCallback<Inventory> callback) {
        return userGetInventory(null, flags, callback);
    }

    // TODO write docs
    public static RequestTask<Inventory> userGetInventory(Identity shopIdentity, int flags,
                                                          RequestCallback<Inventory> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userGetInventory(shopIdentity, flags), callback).execute();
        }
        return null;
    }

    // TODO write docs
    public static RequestTask<Inventory> userInventoryConvert(String productId, String targetId,
                                                  int flags, RequestCallback<Inventory> callback) {
        return userInventoryConvert(productId, targetId, getMainShopIdentity(), flags, callback);
    }

    // TODO write docs
    public static RequestTask<Inventory> userInventoryConvert(String productId, String targetId,
                          Identity shopIdentity, int flags, RequestCallback<Inventory> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(), MessageBuilder.userInventoryConvert(
                    productId, targetId, shopIdentity, flags), callback).execute();
        }
        return null;
    }

//-------------------------------------------------------------------------------------------
//-------- Payments        ------------------------------------------------------------------

    /**
     * Request for a payment in the main Shop.
     *
     * @param bookProducts List of the products that would to be buy.
     * @param account      User account that would be used for purchasing. <b>Not null</b>.
     * @param flags        Flags for request.
     * @param callback     The Object that implement an interface RequestCallback
     *                     {@literal <}UserPayment>. This object will be used to send
     *                     callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<UserPayment> paymentRequest(List<BookedProduct> bookProducts,
                          UserAccount account, int flags, RequestCallback<UserPayment> callback) {
        if (checkInitialisation(callback))
            return paymentRequest(bookProducts, account,
                    getMainShopInfo().getShop(), flags, callback);
        return null;
    }

    /**
     * Request for a payment in the specified shop.
     *
     * @param bookProducts List of the products that would to be buy.
     * @param account      User account that would be used for purchasing. <b>Not null</b>.
     * @param shop         Shop that will be used for request.
     * @param flags        Flags for request.
     * @param callback     The Object that implement an interface RequestCallback
     *                     {@literal <}UserPayment>. This object will be used to send
     *                     callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<UserPayment> paymentRequest(List<BookedProduct> bookProducts, UserAccount account, Shop shop, int flags, RequestCallback<UserPayment> callback) {
        return paymentRequest(bookProducts, account, shop, null, flags, callback);
    }

    /**
     * Request for a payment in the specified shop.
     *
     * @param bookProducts List of the products that would to be buy.
     * @param account      User account that would be used for purchasing. <b>Not null</b>.
     * @param shop         Shop that will be used for request.
     * @param params       Additional params that will insert into request.
     * @param flags        Flags for request.
     * @param callback     The Object that implement an interface RequestCallback
     *                     {@literal <}UserPayment>. This object will be used to send
     *                     callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */

    public static RequestTask<UserPayment> paymentRequest(List<BookedProduct> bookProducts,
                                  UserAccount account, Shop shop, HashMap<String, Object> params,
                                  int flags, RequestCallback<UserPayment> callback) {
        return paymentRequest(bookProducts, account, shop, null, params, flags, callback);
    }

    // TODO write docs
    public static RequestTask<UserPayment> paymentRequest(List<BookedProduct> bookProducts,
                  UserAccount account, Shop shop, String orderId, HashMap<String, Object> params,
                  int flags, RequestCallback<UserPayment> callback) {

        if (checkPaymentAvailable(account, shop, callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.paymentRequest(bookProducts, account, shop, orderId, params,
                            flags),
                    wrapPaymentCallback(callback, account, shop)).execute();
        }
        return null;
    }

    /**
     * Request for a payment in the main Shop.
     *
     * @param amount   Amount of money that would be spend.
     * @param currency Currency of the payment.
     * @param account  User account that would be used for purchasing. <b>Not null</b>.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}UserPayment>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<UserPayment> paymentRequest(double amount, String currency,
                          UserAccount account, int flags, RequestCallback<UserPayment> callback) {
        return paymentRequest(amount, currency, account,
                getMainShopInfo().getShop(), flags, callback);
    }

    /**
     * Requesting a payment on the specified shop.
     *
     * @param amount   Amount of money that would be spend.
     * @param currency Currency of the payment.
     * @param account  User account that would be used for purchasing. <b>Not null</b>.
     * @param shop     Shop that will be used for request.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}UserPayment>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<UserPayment> paymentRequest(double amount, String currency,
              UserAccount account, Shop shop, int flags, RequestCallback<UserPayment> callback) {
        return paymentRequest(amount, currency, account, shop, null, flags, callback);
    }

    /**
     * Requesting a payment on the specified shop.
     *
     * @param amount   Amount of money that would be spend.
     * @param currency Currency of the payment.
     * @param account  User account that would be used for purchasing. <b>Not null</b>.
     * @param shop     Shop that will be used for request.
     * @param params   Additional params that will insert into request.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}UserPayment>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<UserPayment> paymentRequest(double amount, String currency,
                                        UserAccount account, Shop shop, Map<String, Object> params,
                                        int flags, RequestCallback<UserPayment> callback) {
        return paymentRequest(amount, currency, account, shop, null, params, flags, callback);
    }

    // TODO write docs
    public static RequestTask<UserPayment> paymentRequest(double amount, String currency,
                  UserAccount account, Shop shop, String orderId,
                  Map<String, Object> params, int flags, RequestCallback<UserPayment> callback) {
        if (checkPaymentAvailable(account, shop, callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.paymentRequest(amount, currency, account, shop, orderId, params,
                            flags),
                    wrapPaymentCallback(callback, account, shop)).execute();
        }
        return null;
    }

    /**
     * The method that directly send the request for a payment.
     */
    public static RequestTask<UserPayment> paymentRequest(UserAccount account,
                                              Shop shop, String orderId, Map<String, Object> params,
                                              int flags, RequestCallback<UserPayment> callback) {
        if (checkPaymentAvailable(account, shop, callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.paymentRequest(account, shop, orderId, params, flags),
                    wrapPaymentCallback(callback, account, shop)).execute();
        }
        return null;
    }

    /**
     * Check opportunity of the payment.
     *
     * @param account      User account that would be used for purchasing. <b>Not null</b>.
     * @param shopIdentity Identity of the specified shop. <b>Not null</b>.
     * @param callback     Callback that will receive error if condition is not quite.
     * @return True if payment is available, false otherwise.
     */
    public static boolean checkPaymentAvailable(UserAccount account, Shop shopIdentity,
                                                ErrorCallback callback) {
        if (account == null) {
            throw new InvalidParameterException("UserAccount must be not null.");
        }
        if (shopIdentity == null) {
            throw new InvalidParameterException("Shop must be not null.");
        }
        if (checkApi(callback)) {
            if (shopIdentity.getMerchant() != null
                    && shopIdentity.getMerchant().isAccountSupported(account.getType())) {
                return true;
            } else callback.onError(new RpError(RpError.Api.ACCOUNT_NOT_SUPPORTED));
        }
        return false;
    }

    //TODO write docs
    public static RequestCallback<UserPayment> wrapPaymentCallback(
            RequestCallback<UserPayment> callback, UserAccount account, Identity shopIdentity) {
        callback = new PaymentCallbackWrapper(callback);

        if (Type.OPERATOR.equals(account.getType()))
            callback = new MobilePaymentCallbackWrapper(callback, shopIdentity);

        return callback;
    }

    /**
     * Request to re-query pending payment request on the main Shop.
     *
     * @param queryId  Payment request ID.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}UserPayment>. This object will be used for
     *                 sending callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<UserPayment> queryPaymentRequest(String queryId, int flags,
                                                           RequestCallback<UserPayment> callback) {
        return queryPaymentRequest(queryId, null, flags, callback);
    }

    /**
     * Request to re-query pending payment request on the specified shop.
     *
     * @param queryId      Payment request ID.
     * @param shopIdentity Identity of the specified shop. <b>Not null</b>.
     * @param flags        Flags for request.
     * @param callback     The Object that implement an interface RequestCallback
     *                     {@literal <}UserPayment>. This object will be used for
     *                     sending callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<UserPayment> queryPaymentRequest(String queryId,
                       Identity shopIdentity, int flags, RequestCallback<UserPayment> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.queryPaymentRequest(queryId, shopIdentity, flags), callback)
                .execute();
        }
        return null;
    }

//-------------------------------------------------------------------------------------------

    /**
     * Request to get list of orders for current user by specified filter.
     *
     * @param filter       Filter for request.
     * @param shopIdentity Identity of the shop with orders.
     * @param flags        Flags for request.
     * @param callback     The Object that implement an interface RequestCallback
     *                 {@literal <}OrdersList>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     * <p/>
     */
    public static RequestTask<OrdersList> userGetOrders(OrderFilter filter, Identity shopIdentity,
                                                int flags, RequestCallback<OrdersList> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userGetOrders(filter, shopIdentity, flags), callback).execute();
        }
        return null;
    }

    /**
     * Request to get list of orders for current user and main Shop by specified filter.
     *
     * @param filter   Filter for request.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}OrdersList>. This object will be used to send
     *                 callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     * <p/>
     */
    public static RequestTask<OrdersList> userGetOrders(OrderFilter filter, int flags,
                                                        RequestCallback<OrdersList> callback) {
        return userGetOrders(filter, null, flags, callback);
    }

    // TODO write docs
    public static RequestTask<Order> userSetOrder(final Order order, final int flags,
                                                  RequestCallback<Order> callback) {
        Identity shopIdentity = getMainShopIdentity();
        if (order.getShopId() == null ||
                (shopIdentity != null && order.getShopId().equals(shopIdentity.getId())))
            return userSetOrder(order, null, flags, callback);

        final RequestTaskWrapper<Order> wrapper = new RequestTaskWrapper<>();
        wrapper.setCallback(callback, false);
        wrapper.set(shopFindById(order.getShopId(), 0, new RequestCallback<ShopFind>() {
            @Override
            public boolean onComplete(Response<ShopFind> response) {
                if (response.isStatusAccept()) {
                    ShopFind find = response.getResult();
                    if (find.getList().size() > 0) {
                        wrapper.set(userSetOrder(order, find.getList().get(0), flags, wrapper));
                        return true;
                    }
                }

                return onError(response.getAnyError());
            }

            @Override
            public boolean onError(RpError error) {
                return wrapper.onError(error);
            }
        }));

        return wrapper;
    }

    public static RequestTask<Order> userSetOrderApplication(String applicationId, String status, boolean isNewOrder,
                                                             Identity shopIdentity, int flags, RequestCallback<Order> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userSetOrderApplication(applicationId, status, isNewOrder, shopIdentity, flags), callback).execute();
        }
        return null;
    }

    // TODO write docs
    public static RequestTask<Order> userSetOrder(Order order, Identity shopIdentity, int flags,
                                                  RequestCallback<Order> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userSetOrder(order, shopIdentity, flags), callback).execute();
        }
        return null;
    }

    // TODO write docs
    public static RequestTask<Order> userSetOrderStatus(String orderStatus, String orderId,
                                Identity shopIdentity, int flags, RequestCallback<Order> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userSetOrderStatus(orderStatus, orderId, shopIdentity, flags),
                    callback).execute();
        }
        return null;
    }

    // TODO write docs
    public static RequestTask<UserRolesList> userGetRoles(String[] extensionIds, int flags,
                                                          RequestCallback<UserRolesList> callback) {
        return userGetRoles(extensionIds, null, flags, callback);
    }

    public static RequestTask<UserRolesList> userGetRoles(Identity shopIdentity, String[] extensionIds, int flags,
                                                          RequestCallback<UserRolesList> callback) {
        return userGetRoles(shopIdentity, extensionIds, null, flags, callback);
    }

    // TODO write docs
    public static RequestTask<UserRolesList> userGetRoles(String[] extensionIds,
                                                          String userId, int flags,
                                                          RequestCallback<UserRolesList> callback) {
        return new WebServiceAsync<>(getContext(),
                MessageBuilder.userGetRoles(null, extensionIds, userId, flags), callback)
                .execute();
    }

    // TODO write docs
    public static RequestTask<UserRolesList> userGetRoles(Identity shopIdentity, String[] extensionIds,
                                                          String userId, int flags,
                                                          RequestCallback<UserRolesList> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userGetRoles(shopIdentity, extensionIds, userId, flags), callback)
                    .execute();
        }
        return null;
    }

    /**
     * Request to get specified user role of the specified user in main shop.
     *
     * @param roleId Role ID.
     * @param userId User ID.
     * @param flags Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}UserRole>. This object will be used to send
     *                 callbacks in to UI thread.
     *
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<UserRole> userGetRole(String roleId, String userId, int flags,
                                                    RequestCallback<UserRole> callback) {
        return userGetRole(roleId, userId, null, flags, callback);
    }

    public static RequestTask<UserRole> userGetRoleById(String userRoleId, int flags,
                                                        RequestCallback<UserRole> callback) {
        return userGetRoleById(userRoleId,null, flags, callback);
    }

    /**
     * Request to get specified user role of the specified user.
     *
     * @param roleId Role ID.
     * @param userId User ID.
     * @param shopIdentity Shop Identity of role merchant.
     * @param flags Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}UserRole>. This object will be used to send
     *                 callbacks in to UI thread.
     *
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<UserRole> userGetRole(String roleId, String userId,
                            Identity shopIdentity, int flags, RequestCallback<UserRole> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userGetRole(roleId, userId, shopIdentity, flags), callback).execute();
        }
        return null;
    }

    public static RequestTask<UserRole> userGetRoleById(String userRoleId,
                                                            Identity shopIdentity, int flags, RequestCallback<UserRole> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userGetRoleById(userRoleId, shopIdentity, flags), callback).execute();
        }
        return null;
    }

    /**
     * Request to set specified user role of the current user.
     *
     * @param role Role.
     * @param flags Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}UserRole>. This object will be used to send
     *                 callbacks in to UI thread.
     *
     * @return RequestTask implementation. For understanding of the task behaviour see
     * {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<UserRole> userSetRole(UserRole role, int flags,
                                                    RequestCallback<UserRole> callback) {
        return userSetRole(role, null, flags, callback);
    }

    // TODO write docs
    public static RequestTask<UserRole> userSetRole(UserRole role, Identity shopIdentity, int flags,
                                                    RequestCallback<UserRole> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userSetRole(role, shopIdentity, flags), callback).execute();
        }
        return null;
    }

    // TODO docs
    public static RequestTask<UserRole> userDeleteRole(String roleId, int flags,
                                                   RequestCallback<UserRole> callback) {
        return userDeleteRole(roleId, null, flags, callback);
    }

    // TODO docs
    public static RequestTask<UserRole> userDeleteRole(String roleId, Identity shopIdentity,
                                                    int flags, RequestCallback<UserRole> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userDeleteRole(roleId, shopIdentity, flags), callback).execute();
        }
        return null;
    }

    /**
     * Request to get list of chat messages by specified filter.
     *
     * @param filter Filter for request.
     * @param flags Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}ChatMessages>. This object will be used to send
     *                 callbacks in to UI thread.
     *
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<ChatMessages> userGetChatMessages(ChatMessageFilter filter, int flags,
                                                        RequestCallback<ChatMessages> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userGetChatMessages(filter, flags), callback).execute();
        }
        return null;
    }

    /**
     * Request to send chat message.
     *
     * @param chatMessage Message to send.
     * @param flags Flags for request.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}UserChatMessages>. This object will be used to send
     *                 callbacks in to UI thread.
     *
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<Void> userSendChatMessages(ChatMessage chatMessage, int flags,
                                                         RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userSendChatMessages(chatMessage, flags), callback).execute();
        }
        return null;
    }

    // TODO write docs
    public static RequestTask<PushesList> userGetPushes(ListFilter filter, int flags,
                                                        RequestCallback<PushesList> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userGetPushes(filter, flags), callback).execute();
        }
        return null;
    }

    /**
     * XCLU command
     * @param getRegisteredUsers true to get number of registered users
     * @param getEvents true to get number of events
     * @param getInvitedUsers true to get number of invited users
     * @param flags
     * @param callback
     * @return
     */
    public static RequestTask<LaunchpadInfo> userGetLaunchpadInfo(boolean getRegisteredUsers, boolean getEvents, boolean getInvitedUsers,
                                                               int flags, RequestCallback<LaunchpadInfo> callback) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userGetLaunchpadInfo(getRegisteredUsers, getEvents, getInvitedUsers, flags), callback).execute();
    }

    /**
     * XCLU command usercreateinvitescopy
     * @param fromOrderId mandatory order id from which all invitations will be taken
     * @param toOrderId mandatory order id to which invitations will be linked
     * @param flags
     * @param callback
     * @return
     */
    public static RequestTask<Void> userCopyInvitations(String fromOrderId, String toOrderId, int flags, RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userCopyInvitees(fromOrderId, toOrderId, flags), callback).execute();
        }
        return null;
    }

    // TODO write docs
    public static RequestTask<Void> userSetPush(boolean isRead, PushFilter filter, int flags,
                                                RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userSetPush(isRead, filter, flags), callback).execute();
        }
        return null;
    }

    // FIXME lock for user API
    @Deprecated
    public static RequestTask<Void> userNotify(String messageString, Map<String, Object> payload,
                                   String[] userIds, int flags, RequestCallback<Void> callback) {

        Message<Void> message = new Message<>(getContext(), "notify", getUrl(),
                getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                newRequestBuilder(), newParser(Void.class), flags);

        message.addParameter("message", messageString);
        message.addParameter("payload", payload);
        if (userIds != null && userIds.length > 0)
            message.addParameter("user_ids", userIds);

        WebServiceAsync<Void> webService = new WebServiceAsync<>(getContext(), message, callback);
        webService.execute();
        return webService;
    }

    public static RequestTask<HkQrCode> qrInfo(String qrId, int flags,
                                               RequestCallback<HkQrCode> callback) {
        if (checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.qrInfo(qrId, flags), callback).execute();
        }

        return null;
    }

    public static RequestTask<Void> userSendAbuse(String merchant, String object, String objectType, String comment, int flags, RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            Message<Void> message = new Message<>(getContext(), Command.USER_SET_ABUSE, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(Void.class), flags);
            message.setSupportOffline(false);
            message.addParameter("merchant",  merchant);
            message.addParameter("object",  object);
            message.addParameter("object_type",  objectType);
            message.addParameter("comment",  comment);
            return (new WebServiceAsync<>(getContext(), message, callback).execute());
        }
        return null;
    }

    public static RequestTask<Void> userForgetMe(String userId, int flags, RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            Message<Void> message = new Message<>(getContext(), Command.USER_FORGET_ME, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(Void.class), flags);
            message.setSupportOffline(false);
            message.addParameter("user_id",  userId);
            return (new WebServiceAsync<>(getContext(), message, callback).execute());
        }
        return null;
    }

    public static RequestTask<InvitationsList> userSendInvitation(String parent_order_id, String role_id, List<String> user_ids, HashMap<String, String> contact_list, int flags, RequestCallback<InvitationsList> callback) {
        return userSendInvitation(null, parent_order_id, role_id, user_ids, contact_list, flags, callback);
    }

    /**
     * create invitation by phone list or by honkio user id
     * @param parent_order_id id of event order they were invited to
     * @param role_id invitee role id
     * @param contact_list list of the invitee's phones and corresponding names to invite
     * @param user_ids list of the honkio user ids to invite
     * @param flags
     * @param callback
     * @return list of created invitation with secret codes of users which NOT exist in honkio yet,
     *  if the user exists, or invitation already exists, that invitation will not be in result list
     */
    public static RequestTask<InvitationsList> userSendInvitation(Identity shopIdentity, String parent_order_id, String role_id, List<String> user_ids, HashMap<String, String> contact_list, int flags, RequestCallback<InvitationsList> callback) {
        Message<InvitationsList> message = new Message<>(getContext(), Command.USER_SEND_INVITES, getUrl(),
                getActiveConnection(), getDevice(), shopIdentity != null ? shopIdentity : getMainShopIdentity(), getAppIdentity(),
                newRequestBuilder(), newParser(InvitationsList.class), flags);
        message.setSupportOffline(false);
        message.addParameter(Param.QUERY_PARENT_ORDER_ID,  parent_order_id);
        message.addParameter(Param.QUERY_ROLE_ID,  role_id);
        if (user_ids != null && user_ids.size() > 0){
            message.addParameter("users_ids",  user_ids);
        }
        if (contact_list != null && contact_list.size() > 0){
            message.addParameter(Param.QUERY_CONTACTS_DICT, contact_list);
        }
        return new WebServiceAsync<>(getContext(), message, callback).execute();
    }

    public static RequestTask<InvitationsList> userInvitationsList(InvitationFilter filter, int flags, RequestCallback<InvitationsList> callback) {
        return userInvitationsList(null, filter, flags, callback);
    }

    /**
     * get all invitation according to filter
     * @param filter
     * @param flags
     * @param callback
     * @return invitation list
     */
    public static RequestTask<InvitationsList> userInvitationsList(Identity shopIdentity, InvitationFilter filter, int flags, RequestCallback<InvitationsList> callback) {
        Message<InvitationsList> message = new Message<>(getContext(), Command.USER_INVITE_LIST, getUrl(),
                getActiveConnection(), getDevice(), shopIdentity != null ? shopIdentity : getMainShopIdentity(), getAppIdentity(),
                newRequestBuilder(), newParser(InvitationsList.class), flags);
        message.setSupportOffline(false);
        if (filter != null) {
            filter.applyTo(message);
        }
        return new WebServiceAsync<>(getContext(), message, callback).execute();
    }

    /**
     * when user input invitation code server update receiver_id with user id and from that invitation belongs to this user
     * @param invitation_id not used now
     * @param invitation_code invitation "secret code"
     * @param flags
     * @param callback
     * @return invitation that contains the code with not empty receiver_id that equals user id now
     */
    public static RequestTask<Invitation> userSetInvitation(String invitation_id, String invitation_code, int flags, RequestCallback<Invitation> callback) {
        Message<Invitation> message = new Message<>(getContext(), Command.USER_SET_INVITE, getUrl(),
                getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                newRequestBuilder(), newParser(Invitation.class), flags);
        message.setSupportOffline(false);
        if (invitation_id != null) message.addParameter(Param.QUERY_ID, invitation_id);
        if (invitation_code != null) message.addParameter(Param.QUERY_SECRET_CODE, invitation_code);
        return new WebServiceAsync<>(getContext(), message, callback).execute();
    }

    /**
     * list of users "friends" list, aka list of user contacts (invitees), that accept invitation at least one time
     * @param roleId required, Invitee role
     * @param filter invitees filter
     * @param flags
     * @param callback
     * @return list of user roles
     */
    public static RequestTask<RolesList> userGetInvitees(String roleId, InviteesFilter filter, int flags, RequestCallback<RolesList> callback) {
        Message<RolesList> message = new Message<>(getContext(), Command.USER_GET_INVITEES, getUrl(),
                getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                newRequestBuilder(), newParser(RolesList.class), flags);
        message.setSupportOffline(false);
        message.addParameter(Param.QUERY_ROLE_ID, roleId);
        if (filter != null) {
            filter.applyTo(message);
        }
        return new WebServiceAsync<>(getContext(), message, callback).execute();
    }

    //--------POLLS------------------------------------------------------------------------------
    public static RequestTask<PollsList> userPollsList(Identity shopIdentity, PollsFilter filter, int flags, RequestCallback<PollsList> callback) {
        Message<PollsList> message = new Message<>(getContext(), Command.USER_POLLS_LIST, getUrl(),
                getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                newRequestBuilder(), newParser(PollsList.class), flags);
        message.setSupportOffline(false);
        if (filter != null) {
            filter.applyTo(message);
        }
        return new WebServiceAsync<>(getContext(), message, callback).execute();
    }

    public static RequestTask<Poll> userPollVote(Identity shopIdentity, String pollId, String optionId, String comment, int flags, RequestCallback<Poll> callback) {
        Message<Poll> message = new Message<>(getContext(), Command.USER_POLL_VOTE, getUrl(),
                getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                newRequestBuilder(), newParser(Poll.class), flags);
        message.setSupportOffline(false);
        message.addParameter("id", pollId);
        message.addParameter("option", optionId);
        message.addParameter("comment", comment);
        return new WebServiceAsync<>(getContext(), message, callback).execute();
    }

    //-------------------------------------------------------------------------------------------
    //-------- Getters and setters --------------------------------------------------------------
    //-------------------------------------------------------------------------------------------

    /**
     * Method to get last User data that was returned from the server
     *
     * @return Last User data that was returned from the server or null if cache doesn't contain
     * User object.
     */
    public static User getActiveUser() {
        return sApiState.getActiveUser();
    }

    public static String getUserPhotoUrl(String userId) {
        if (userId == null || getServerInfo() == null || getServerInfo().getConsumerUrl() == null)
            return null;
        return getServerInfo().getConsumerUrl() + (getServerInfo().getConsumerUrl().endsWith("/") ? "user_photo/" : "/user_photo/") + userId + ".jpg";
    }

    public static class MessageBuilder extends BaseHonkioApi.MessageBuilder {

        // TODO write docs
        public static Message<TagsList> userGetTags(Identity shopIdentity, int flags) {
            Message<TagsList> message = new Message<>(getContext(), Command.USER_GET_TAGS, getUrl(),
                    getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(TagsList.class), flags);

            return message;
        }

        // TODO write docs
        public static Message<UserAccessList> userAccessList(String parentMerchant, int flags) {
            Message<UserAccessList> message = new Message<>(getContext(), Command.USER_ACCESS_LIST, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(UserAccessList.class), flags);

            message.addParameter("query_parent", parentMerchant);

            return message;
        }

        // TODO write docs
        public static Message<UserAccessList> userMerchantAccessList(String merchant, int flags) {
            Message<UserAccessList> message = new Message<>(getContext(), Command.USER_ACCESS_LIST, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(UserAccessList.class), flags);

            message.addParameter("query_merchant", merchant);

            return message;
        }

        /**
         * The request to get personal information of the user.
         *
         * @param params   Additional params that will insert into request.
         * @param flags    Flags for request.
         */
        public static Message<User> userGet(HashMap<String, Object> params, int flags) {
            Message<User> message = new Message<>(getContext(), Command.USER_GET, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(User.class), flags);
            message.setCacheFileName(Command.USER_GET, false);
            message.addParameters(params);

            return message;
        }

        // TODO write docs
        public static Message<UserCommentsList> userGetComments(UserCommentsFilter filter,
                                                                Identity shopIdentity,
                                                                int flags) {
            Message<UserCommentsList> message = new Message<>(getContext(),
                    Message.Command.USER_GET_COMMENTS, getUrl(),
                    getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(UserCommentsList.class), flags);
            message.setCacheFileName(Message.Command.USER_GET_COMMENTS, true);

            if (filter != null)
                filter.applyTo(message);

            return message;
        }

        /**
         * The request to update personal information of the user.
         *
         * @param user     User object that contains information to change.
         * @param flags    Flags for request.
         */
        public static Message<User> userUpdate(User user, int flags) {
            return userUpdate(user.toParams(), flags);
        }


        public static Message<Void> userUpdateMedia(MediaUrl url, int flags) {
            Message<Void> message = new Message<>(getContext(), Command.USER_SET_MEDIA, url.getUploadUrl(),
                    null, getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(Void.class), flags);

            message.setSupportOffline(false);
            message.addParameter("upload_token", url.getUploadToken());
            message.addParameter("id", url.getId());
            message.addParameter("access", "public");
            return message;
        }

        /**
         * Request to add user payment account into accounts list. This method adds account
         * without any initialization, use {@link AccountCreationProcessModel}
         * to correct account adding.
         *
         * @param account  Account object with params to adding.
         * @param flags    Flags for request.
         */
        public static Message<User> userAddAccount(UserAccount account, int flags) {
            HashMap<String, Object> params = new HashMap<>();
            HashMap<String, Object> accountMap = new HashMap<>();
            params.put("consumer_account", accountMap);

            if (account.getType() != null) accountMap.put("type", account.getType());
            if (account.getNumber() != null) accountMap.put("number", account.getNumber());
            if (account.getCreationParams() != null)
                accountMap.put("creation_params", account.getCreationParams());
            if (account.getDescription() != null)
                accountMap.put("description", account.getDescription());
            accountMap.put("index", "+");

            return userUpdate(params, flags);
        }

        /**
         * Request to editing user accounts.
         *
         * @param account  The action with edited params.
         * @param flags    Flags for request.
         */
        public static Message<User> userEditAccount(UserAccount account, int flags) {
            HashMap<String, Object> params = new HashMap<>();
            HashMap<String, Object> accountMap = new HashMap<>();
            params.put("consumer_account", accountMap);

            if (account.getType() != null) accountMap.put("type", account.getType());
            if (account.getNumber() != null) accountMap.put("number", account.getNumber());
            accountMap.put("index", account.getIndex());

            return userUpdate(params, flags);
        }

        /**
         * Request to move user account.
         *
         * @param account     The account that will moved.
         * @param newPosition New position for the account.
         * @param flags       Flags for request.
         */
        public static Message<User> userMoveAccount(UserAccount account, int newPosition,
                                                    int flags) {
            HashMap<String, Object> params = new HashMap<>();
            HashMap<String, Object> accountMap = new HashMap<>();
            params.put("consumer_account", accountMap);

            accountMap.put("index", account.getIndex());
            accountMap.put("move", newPosition);

            return userUpdate(params, flags);
        }

        /**
         * Request to delete user accounts.
         *
         * @param account  The action that will deleted.
         * @param flags    Flags for request.
         */
        public static Message<User> userDeleteAccount(UserAccount account, int flags) {
            HashMap<String, Object> params = new HashMap<>();
            HashMap<String, Object> accountMap = new HashMap<>();
            params.put("consumer_account", accountMap);

            accountMap.put("index", account.getIndex());
            accountMap.put("move", "-");

            return userUpdate(params, flags);
        }

        /**
         * Private method that update user by specified params. Used in methods to work with User
         * Accounts.
         *
         * @param params   Params that will insert into request.
         * @param flags    Flags for request.
         */
        private static Message<User> userUpdate(Map<String, Object> params, int flags) {
            Message<User> message = new Message<>(getContext(), Command.USER_SET, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(User.class), flags);
            message.setCacheFileName(Command.USER_GET, false);
            message.setSupportOffline(false);

            message.addParameters(params);

            return message;
        }

        /**
         * Request to put or edit values form the special dictionary map of the User.
         *
         * @param dict     HashMap that contains a key value pairs to write in to user dictionary
         *                 map.
         * @param flags    Flags for request.
         */
        public static Message<User> userUpdateDictValues(HashMap<String, String> dict, int flags) {
            if (dict == null || dict.size() == 0)
                throw new InvalidParameterException("Dict must contain values.");

            Message<User> message = new Message<>(getContext(), Command.USER_SET, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(User.class), flags);
            message.setCacheFileName(Command.USER_GET, false);
            message.setSupportOffline(false);

            message.addParameter(Message.Param.USER_DICT_KEYSTORE, dict);

            return message;
        }

        /**
         * Request to get list of transactions for current user and specified shop by specified
         * filter.
         *
         * @param filter       Filter for request.
         * @param shopIdentity Identity of the specified shop.
         * @param flags        Flags for request.
         */
        public static Message<Transactions> userGetTransactions(TransactionFilter filter,
                                                                Identity shopIdentity, int flags) {
            Message<Transactions> message = new Message<>(getContext(),
                    Command.USER_TRANSACTION, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(Transactions.class), flags);

            boolean onlyValid = filter != null
                    && filter.getOnlyValid() != null
                    && filter.getOnlyValid();

            message.setCacheFileName(
                    Command.USER_TRANSACTION + (onlyValid ? "_onlyValid" : ""),
                    false);

            if (filter != null) {
                filter.applyTo(message);
            }

            return message;
        }

        public static Message<Invitation> userDonate(Identity shopIdentity, UserAccount account, double amount,
                                                     String userEmail, String desc, int flags) {
            Message<Invitation> message = new Message<>(getContext(), Message.Command.USER_DONATE, getUrl(),
                    getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(Invitation.class), flags);

            account.applyTo(message);
            message.addParameter("account_amount", AbsFilter.amountToString(amount));
            message.addParameter("email", userEmail);

            if (desc != null)
                message.addParameter("message", desc);

            return message;
        }

        public static Message<Invitation> userAcceptInvitation(String invitationId, String invitationStatus, int flags) {
            Message<Invitation> message = new Message<>(getContext(), Message.Command.USER_ACCEPT_INVITATION, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(Invitation.class), flags);

            message.addParameter("id", invitationId);
            if (invitationStatus != null) message.addParameter("status", invitationStatus);

            return message;
        }

        // TODO write docs
        public static Message<Void> userVote(UserComment userComment, int flags) {
            Message<Void> message = new Message<>(getContext(), Message.Command.USER_VOTE, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(Void.class), flags);

            userComment.applyTo(message);

            return message;
        }

        public static Message<RateList> getRate(RatingsFilter filter, int flags) {
            Message<RateList> message = new Message<>(getContext(), Message.Command.USER_GET_RATE, getUrl(),
                    null, getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(RateList.class), flags);
            filter.applyTo(message);
            return message;
        }
        @Deprecated
        public static Message<Void> userApplyForOrder(String orderId, String proposalTime,
                                                      int flags) {
            Message<Void> message = new Message<>(getContext(),
                    Message.Command.USER_APPLY_FOR_ORDER, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(Void.class), flags);
            message.addParameter("order_id", orderId);
            if (!TextUtils.isEmpty(proposalTime))
                message.addParameter("time_proposal", proposalTime);

            return message;
        }

        @Deprecated
        public static Message<Void> userDeleteApplyForOrder(String orderId, int flags) {
            Message<Void> message = new Message<>(getContext(),
                    Command.USER_APPLY_FOR_ORDER, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(Void.class), flags);
            message.addParameter("order_id", orderId);
            message.addParameter("delete", "true");

            return message;
        }

        public static Message<ApplicantsList> orderGetApplicants(ApplicantFilter filter,
                                                                 Identity shopIdentity, int flags) {
            Message<ApplicantsList> message = new Message<>(getContext(),
                    Command.USER_GET_APPLICANTS, getUrl(),
                    getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(ApplicantsList.class), flags);

            filter.applyTo(message);

            return message;
        }

        /**
         * Request for a payment in the specified shop.
         *
         * @param bookProducts List of the products that would to be buy.
         * @param account      User account that would be used for purchasing. <b>Not null</b>.
         * @param shop         Shop that will be used for request.
         * @param orderId      Order ID for which payment should processed.
         * @param params       Additional params that will insert into request.
         * @param flags        Flags for request.
         * @return RequestTask implementation. For understanding of the task behaviour see
         * {@link WebServiceAsync}. Task has priority: High Priority.
         */
        public static Message<UserPayment> paymentRequest(List<BookedProduct> bookProducts,
                                          UserAccount account, Shop shop,
                                          String orderId, Map<String, Object> params, int flags) {

            List<Object> productsArray = new ArrayList<>();
            if (bookProducts != null) {
                for (BookedProduct bookProduct : bookProducts) {
                    Map<String, Object> productMap = new HashMap<>(2);
                    productMap.put(Param.ID, bookProduct.getProduct().getId());
                    productMap.put("count", bookProduct.getCount());
                    productsArray.add(productMap);
                }
            }

            HashMap<String, Object> paymentParams = new HashMap<>();
            paymentParams.put(Param.PRODUCT, productsArray);

            if (params != null)
                paymentParams.putAll(params);

            return paymentRequest(account, shop, orderId, paymentParams, flags);
        }

        /**
         * Requesting a payment on the specified shop.
         *
         * @param amount   Amount of money that would be spend.
         * @param currency Currency of the payment.
         * @param account  User account that would be used for purchasing. <b>Not null</b>.
         * @param shop     Shop that will be used for request.
         * @param orderId      Order ID for which payment should processed.
         * @param params   Additional params that will insert into request.
         * @param flags    Flags for request.
         */
        public static Message<UserPayment> paymentRequest(double amount, String currency,
                                                    UserAccount account, Shop shop, String orderId,
                                                    Map<String, Object> params, int flags) {
            HashMap<String, Object> paymentParams = new HashMap<>();
            paymentParams.put(Param.ACCOUNT_AMOUNT, AbsFilter.amountToString(amount));
            if (currency != null)
                paymentParams.put(Param.ACCOUNT_CURRENCY, currency);

            if (params != null)
                paymentParams.putAll(params);

            return paymentRequest(account, shop, orderId, paymentParams, flags);
        }


        /**
         * The method that directly send the request for a payment.
         */
        public static Message<UserPayment> paymentRequest(UserAccount account, Shop shop,
                                          String orderId, Map<String, Object> params, int flags) {
            Message<UserPayment> message = new Message<>(getContext(),
                    Command.USER_PAYMENT, getUrl(), getActiveConnection(),
                    getDevice(), shop, getAppIdentity(),
                    newRequestBuilder(), newParser(UserPayment.class), flags);

            if (orderId != null)
                message.addParameter(Param.ORDER, orderId);

            account.applyTo(message);

            if (params != null)
                message.addParameters(params);

            return message;
        }

        /**
         * Request to ask server for new temporary password when user forgot his password.
         * Password will send on the User email.
         *
         * @param email    User mail that used for login.
         * @param flags    Flags for request.
         */
        public static Message<Void> userLostPassword(String email, int flags) {
            Message<Void> message = new Message<>(getContext(), Command.LOST_PASSWORD, getUrl(),
                    null, getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(Void.class), flags);
            message.addParameter(Param.LOGIN_IDENTITY, email);
            message.addParameter(Param.LOGIN_PASSWORD, "-");

            return message;
        }

        /**
         * Request to re-query pending payment request on the specified shop.
         *
         * @param shopIdentity Identity of the specified shop. <b>Not null</b>.
         * @param queryId      Payment request ID.
         * @param flags        Flags for request.
         */
        public static Message<UserPayment> queryPaymentRequest(String queryId,
                                                               Identity shopIdentity, int flags) {
            Message<UserPayment> message = new Message<>(getContext(),
                    Command.QUERY, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(UserPayment.class), flags);
            message.addParameter(Param.ID, queryId);
            message.setBroadcastCommand(Command.USER_PAYMENT);

            return message;
        }

        /**
         * Request to load some file from the server.
         *
         * @param fileName Name of file that will be loaded.
         * @param flags    Flags for request.
         */
        public static Message<ServerFile> userGetFile(String fileName, int flags) {
            Connection connection = getActiveConnection();
            if (connection != null && !connection.isValid())
                connection = null;

            Message<ServerFile> message = new Message<>(getContext(),
                    Command.USER_GET_FILE, getUrl(),
                    connection, getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(ServerFile.class), flags);
            message.addParameter(Param.ID, fileName);
            message.setCacheFileName(Command.USER_GET_FILE + "_" + fileName, false);
            if (connection == null) {
                message.addParameter(Param.LOGIN_IDENTITY, "-");
                message.addParameter(Param.LOGIN_PASSWORD, "-");
                message.addParameter(Param.QUERY_LANGUAGE, getSuitableLanguage());
            }

            return message;
        }

        /**
         * Request to agree current version of Terms of Use.
         *
         * @param touUid   UID of terms that should be accepted.
         * @param flags    Flags for request.
         */
        public static Message<Void> userTouAgree(String touUid, int flags) {
            Message<Void> message = new Message<>(getContext(), Command.USER_ACCEPT_TOU, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(Void.class), flags);

            message.setSupportOffline(false);

            if (touUid != null && !AppInfo.TouInfo.KEY_DEFAULT.equals(touUid))
                message.addParameter(Param.USER_TOU_KEY, touUid);

            return message;
        }

        /**
         * Request to get list of orders for current user and main Shop by specified filter.
         *
         * @param shopIdentity Identity of the shop with orders.
         * @param filter       Filter for request.
         * @param flags        Flags for request.
         */
        public static Message<OrdersList> userGetOrders(OrderFilter filter, Identity shopIdentity,
                                                        int flags) {
            Message<OrdersList> message = new Message<>(getContext(),
                    Command.USER_GET_ORDERS, getUrl(),
                    getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(OrdersList.class), flags);
            message.setCacheFileName(Command.USER_GET_ORDERS, true);

            if (filter != null) {
                filter.applyTo(message);
            }

            return message;
        }

        // TODO write docs
        public static Message<Order> userSetOrder(Order order, Identity shopIdentity, int flags) {
            Message<Order> message = new Message<>(getContext(),
                    Command.USER_SET_ORDER, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(Order.class), flags);

            order.applyTo(message);
            if (order.getAccount() != null
                    && HonkioApi.getActiveUser().getUserId().equals(order.getUserOwner())) {
                order.getAccount().applyTo(message);
            }

            return message;
        }

        /**
         *
         * @param applicationId id of application of applicant
         * @param status assept or reject
         * @param isNewOrder true, if need to create new order, if the user needs to add more applicants to order
         * @param shopIdentity
         * @param flags
         * @return
         */
        public static Message<Order> userSetOrderApplication(String applicationId, String status, boolean isNewOrder,
                                                             Identity shopIdentity, int flags) {
            Message<Order> message = new Message<>(getContext(),
                    Command.USER_SET_ORDER_APPLICATION, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(Order.class), flags);
            message.addParameter(Param.APPLICATION_ID, applicationId);
            message.addParameter(Param.STATUS, status);
            message.addParameter(Param.NEW_ORDER, isNewOrder);
            return message;
        }

        public static Message<Order> userSetOrderStatus(String orderStatus,
                                                String orderId, Identity shopIdentity, int flags) {
            Message<Order> message = new Message<>(getContext(),
                    Command.USER_SET_ORDER, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(Order.class), flags);

            message.addParameter(Param.ORDER_ID, orderId);
            message.addParameter(Param.ORDER_STATUS, orderStatus);

            return message;
        }

        /**
         * Request to get specified user role of the specified user.
         *
         * @param roleId Role ID.
         * @param userId User ID.
         * @param shopIdentity Shop Identity of role merchant.
         * @param flags Flags for request.
         */
        public static Message<UserRole> userGetRole(String roleId, String userId,
                                                    Identity shopIdentity, int flags) {
            Message<UserRole> message = new Message<>(getContext(), Command.USER_GET_ROLE, getUrl(),
                    getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(UserRole.class), flags);
            message.setSupportOffline(true);
            message.setCacheFileName(Command.USER_GET_ROLE
                            + "_" + roleId
                            + "_" + userId
                            + (shopIdentity != null ? "_" + shopIdentity.getId() : ""),
                    false);
            message.addParameter("role_id", roleId);
            if (userId != null && !userId.isEmpty()) {
                message.addParameter("user_id", userId);
            }

            return message;
        }

        public static Message<UserRole> userGetRoleById(String userRoleId,
                                                        Identity shopIdentity, int flags) {
            Message<UserRole> message = new Message<>(getContext(), Command.USER_GET_ROLE, getUrl(),
                    getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(UserRole.class), flags);
            message.setSupportOffline(true);
            message.setCacheFileName(Command.USER_GET_ROLE
                            + "_" + userRoleId
                            + (shopIdentity != null ? "_" + shopIdentity.getId() : ""),
                    false);
            message.addParameter("user_role_id", userRoleId);
            return message;
        }

        // TODO write docs
        public static Message<UserRolesList> userGetRoles(Identity shopIdentity, String[] roleIds,
                                                          String userId, int flags) {
            Message<UserRolesList> message = new Message<>(getContext(),
                    Command.USER_GET_ROLES, getUrl(),
                    getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(UserRolesList.class), flags);

            StringBuilder cacheName = new StringBuilder(Command.USER_GET_ROLES);
            if (roleIds != null)
                for (String extensionId : roleIds)
                    cacheName.append("_").append(extensionId);
            cacheName.append("_").append(userId);

            message.setSupportOffline(true);
            message.setCacheFileName(cacheName.toString(), false);

            if (roleIds != null)
                message.addParameter("roles", Arrays.asList(roleIds));

            if (userId != null && !userId.isEmpty())
                message.addParameter("user_id", userId);

            return message;
        }

        /**
         * Request to set specified user role of the current user.
         *
         * @param role Role.
         * @param flags Flags for request.
         */
        public static Message<UserRole> userSetRole(UserRole role, Identity shopIdentity, int flags) {
            Message<UserRole> message = new Message<>(getContext(), Command.USER_SET_ROLE, getUrl(),
                    getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(UserRole.class), flags);
            role.applyTo(message);
            message.setCacheFileName(
                    Command.USER_GET_ROLE + "_" + role.getRoleId() + "_" + role.getUserId(), false);

            return message;
        }

        // TODO docs
        public static Message<UserRole> userDeleteRole(String roleId, Identity shopIdentity, int flags) {
            Message<UserRole> message = new Message<>(getContext(), Command.USER_SET_ROLE, getUrl(),
                    getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(UserRole.class), flags);

            message.addParameter("role_id", roleId);
            message.addParameter("delete", "Yes");

            return message;
        }

        /**
         * Request to get list of chat messages by specified filter.
         *
         * @param filter Filter for request.
         * @param flags Flags for request.
         */
        public static Message<ChatMessages> userGetChatMessages(ChatMessageFilter filter,
                                                                int flags) {
            Message<ChatMessages> message = new Message<>(getContext(),
                    Command.USER_GET_CHAT, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(ChatMessages.class), flags);

            if (filter != null) {
                filter.applyTo(message);
            }

            return message;
        }

        /**
         * Request to send chat message.
         *
         * @param chatMessage Message to send.
         * @param flags Flags for request.
         */
        public static Message<Void> userSendChatMessages(ChatMessage chatMessage, int flags) {
            Message<Void> message = new Message<>(getContext(),
                    Command.USER_SEND_CHAT_MSG, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(Void.class), flags);

            chatMessage.applyTo(message);

            return message;
        }

        public static Message<PushesList> userGetPushes(ListFilter filter, int flags) {
            Message<PushesList> message = new Message<>(getContext(),
                    Command.USER_GET_NOTIFICATIONS, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(PushesList.class), flags);

            filter.applyTo(message);

            return message;
        }

        public static Message<LaunchpadInfo> userGetLaunchpadInfo(boolean getRegisteredUsers, boolean getEvents, boolean getInvitedUsers, int flags) {
            Message<LaunchpadInfo> message = new Message<>(getContext(),
                    Command.USER_GET_LAUNCHPAD_INFO, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(LaunchpadInfo.class), flags);

            message.addParameter("query_registered_users", getRegisteredUsers);
            message.addParameter("query_events", getEvents);
            message.addParameter("query_invited_users", getInvitedUsers);
            return message;
        }

        public static Message<Void> userCopyInvitees(String fromOrderId, String toOrderId, int flags) {
            Message<Void> message = new Message<>(getContext(),
                    Command.USER_CREATE_INVITES_COPY, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(Void.class), flags);
            message.addParameter("query_parent_order_id", fromOrderId);
            message.addParameter("query_new_order_id", toOrderId);
            return message;
        }

        public static Message<Void> userSetPush(boolean isRead, PushFilter filter, int flags) {
            Message<Void> message = new Message<>(getContext(),
                    Command.USER_SET_NOTIFICATION, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(Void.class), flags);

            if (filter != null)
                filter.applyTo(message);

            message.addParameter("readed", isRead);

            return message;
        }

        public static Message<Inventory> userGetInventory(Identity shopIdentity, int flags) {
            Message<Inventory> message = new Message<>(getContext(),
                    Command.USER_INVENTORY_LIST, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(Inventory.class), flags);
            return message;
        }

        public static Message<Inventory> userInventoryConvert(String productId, String targetId,
                                                              Identity shopIdentity, int flags) {
            Message<Inventory> message = new Message<>(getContext(),
                    Command.USER_INVENTORY_CONVERT, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(Inventory.class), flags);

            message.addParameter("product_id", productId);
            message.addParameter("target_product_id", targetId);

            return message;
        }

        public static Message<HkQrCode> qrInfo(String qrId, int flags) {
            Message<HkQrCode> message = new Message<>(getContext(),
                    Command.QR_INFO, getUrl(), null, getDevice(),
                    getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(HkQrCode.class), flags);

            message.addParameter("id", qrId);

            return message;
        }
    }

//-------------------------------------------------------------------------------------------
//-------- Private Methods ------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

    private static String getSmsVerifyUrl(String smsCode) {
        synchronized (STATIC_LOCK) {
            return getServerInfo().getVerifyUrl() + smsCode;
        }
    }

//-------------------------------------------------------------------------------------------
//-------- Private Classes ------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

    private static boolean setupUserLanguage(User user) {
        String suitableLanguage = getSuitableLanguage();
        if (user.getLanguage() == null || !user.getLanguage().equals(suitableLanguage)) {
            user.setLanguage(suitableLanguage);
            return true;
        }
        return false;
    }

    private static class UserWebServiceAsync extends WebServiceAsync<User> {

        public UserWebServiceAsync(Context context, Message<User> message, RequestCallback<User> callback) {
            super(context, message, callback);
        }

        @Override
        public boolean invokeOnComplete(Response<User> response) {
            if (response != null) {

                // User don't complete registration.
                if (!response.getResult().isActive() && !response.isOffline()) {
                    return handleRegistration(response);
                } else {
                    if (getActiveConnection() == null)
                        createConnection(response, null);

                    // setup language
                    if (setupUserLanguage(response.getResult())) {
                        User newUser = new User();
                        newUser.setLanguage(response.getResult().getLanguage());
                        userUpdate(newUser, Message.FLAG_NO_WARNING, null);
                    }
                    return UserWebServiceAsync.super.invokeOnComplete(response);
                }
            } else
                return invokeOnError(new RpError(RpError.Api.UNKNOWN));
        }

        protected void createConnection(Response<User> response, String pin) {
            synchronized (STATIC_LOCK) {
                initConnection(getApiPreferences(), response.getResult().getLogin(), response.getOtp());
            }
            BroadcastHelper.sendActionLogin(sApiState.getContext());
        }

        protected boolean handleRegistration(final Response<User> response) {
            if (response != null) {
                if (getActiveConnection() == null)
                    createConnection(response, null);
                if (response.isStatusPending() || response.isStatusAccept()) {
                    Message message = response.getMessage();
                    if (message != null) {
                        ApiCache.putWebResponse(getContext(), response, message.getCacheFileName(), true);
                    }
                }
                if (response.isStatusPending()) {
                    return super.invokeOnComplete(response);
                } else if (response.isStatusAccept() && !response.getResult().isActive()) {
                    // TODO remove ugly check of the activation.
                    RequestCallback<User> registrationQueryCallback = new RequestCallback<User>() {
                        @Override
                        public boolean onError(RpError error) {
                            return UserWebServiceAsync.super.invokeOnError(error);
                        }

                        @Override
                        public boolean onComplete(Response<User> queryResp) {
                            if (queryResp.isStatusPending()) {
                                Response.Editor.setStatus(response, Response.Status.PENDING);
                                Response.Editor.setPendings(response, queryResp.getPendings());
                                return UserWebServiceAsync.super.invokeOnComplete(response);
                            } else if (queryResp.isStatusReject()) {
                                userReRegister(response.getResult(), 0, this);
                                return true;
                            } else if (queryResp.isStatusAccept())
                                return UserWebServiceAsync.super.invokeOnComplete(response);
                            else {
                                return onError(queryResp.getAnyError());
                            }
                        }
                    };

                    String regQueryId = response.getResult().getRegistrationId();
                    if (regQueryId != null) {
                        queryUserRegister(regQueryId, 0, registrationQueryCallback);
                    } else
                        userReRegister(response.getResult(), 0, registrationQueryCallback);
                    return true;
                } else
                    return UserWebServiceAsync.super.invokeOnComplete(response);
            } else
                return invokeOnError(new RpError(RpError.Api.UNKNOWN));
        }
    }

    @Deprecated
    private static class RegistrationWebServiceAsync extends UserWebServiceAsync {

        public RegistrationWebServiceAsync(Context context, Message<User> message, RequestCallback<User> callback) {
            super(context, message, callback);
        }

        @Override
        public boolean invokeOnComplete(Response<User> response) {
            return handleRegistration(response);
        }
    }

    private static class ChangePasswordWebServiceAsync extends UserWebServiceAsync {

        private String mOldPassword;
        private String mNewPassword;

        public ChangePasswordWebServiceAsync(Context context, Message<User> message, RequestCallback<User> callback, String oldPassword, String newPassword) {
            super(context, message, callback);
            mOldPassword = oldPassword;
            mNewPassword = newPassword;
        }

        @Override
        public boolean invokeOnComplete(Response<User> response) {
            if (response.isStatusAccept()) {
                RequestCallback<User> callback = new RequestCallback<User>() {

                    @Override
                    public boolean onError(RpError error) {
                        return invokeOnError(error);
                    }

                    @Override
                    public boolean onComplete(Response<User> response) {
                        if (response.isStatusAccept()) {
                            return ChangePasswordWebServiceAsync.super.invokeOnComplete(response);
                        }
                        else
                            return invokeOnError(response.getAnyError());
                    }
                };

                Message<User> message = new Message<>(getContext(), Command.USER_SET, getUrl(),
                        getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                        newRequestBuilder(), newParser(User.class), 0);
                response.getResult().applyTo(message);
                if (mOldPassword != null)
                    message.addParameter(Param.LOGIN_PASSWORD, mOldPassword);
                message.addParameter(Param.USER_PASSWORD, mNewPassword);

                WebServiceAsync<User> webService = new WebServiceAsync<>(getContext(), message,
                        callback);
                webService.execute();

                return true;
            } else
                return invokeOnError(response.getAnyError());
        }
    }

    private static class PaymentCallbackWrapper extends RequestCallbackWrapper<UserPayment> {

        public PaymentCallbackWrapper(RequestCallback<UserPayment> callback) {
            super(callback);
        }

        @Override
        public boolean onComplete(Response<UserPayment> response) {
            if (response.isStatusAccept()) {
                userGet(Message.FLAG_ONLINE_MODE, new RequestCallback<User>() {
                    @Override
                    public boolean onError(RpError error) {
                        return false;
                    }

                    @Override
                    public boolean onComplete(Response<User> response) {
                        return true;
                    }
                });
            }
            return super.invokeOnComplete(response);
        }
    }

    private static class MobilePaymentCallbackWrapper extends RequestCallbackWrapper<UserPayment> {

        private static final long QUERY_TIMEOUT = ApiWebUtils.CONNECTION_TIMEOUT;

        private Identity mShopIdentity;
        private String mTransactionId;
        private long mQueryStartTime;
        private RequestCallback<UserPayment> paymentCheckCallback = new RequestCallback<UserPayment>() {

            @Override
            public boolean onError(RpError errorCode) {
                return invokeOnError(errorCode);
            }

            @Override
            public boolean onComplete(Response<UserPayment> response) {
                if (response.hasPending(Response.Pending.OPERATOR_VERIFY)) {
                    if (System.currentTimeMillis() - mQueryStartTime > QUERY_TIMEOUT)
                        return invokeOnError(new RpError(RpError.Api.UNKNOWN));
                    else {
                        queryPaymentRequest(mTransactionId, mShopIdentity, 0, this);
                        return true;
                    }
                }
                else {
                    return invokeOnComplete(response);
                }
            }
        };

        public MobilePaymentCallbackWrapper(RequestCallback<UserPayment> callback, Identity shopIdentity) {
            super(callback);
            mShopIdentity = shopIdentity;
        }

        @Override
        public boolean onComplete(final Response<UserPayment> response) {
            if (response.isStatusPending() && response.hasPending(Response.Pending.OPERATOR_VERIFY)) {
                String url = response.getUrl();
                if (url != null) {
                    mTransactionId = response.getId();
                    SimpleAsyncGet asyncRequest = new SimpleAsyncGet(getContext(), url) {

                        @Override
                        protected void onPostExecute(String result) {
                            query();
                        }

                        @Override
                        protected void onCancelled() {
                            query(); // Query payment anyway
                        }

                        private void query() {
                            mQueryStartTime = System.currentTimeMillis();
                            queryPaymentRequest(mTransactionId, mShopIdentity,
                                    0, paymentCheckCallback);
                        }
                    };
                    asyncRequest.execute();
                    return true;
                } else
                    return onError(response.getAnyError());
            } else
                return super.onComplete(response);
        }
    }

//-------------------------------------------------------------------------------------------
//-------- Deprecated      ------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

    /**
     * The request to repeated registration. Send this request only when previous request of
     * the registration has status "reject".
     *
     * @param user     Personal information of the user for registration.
     * @param flags    Flags for request.
     * @param callback The Object that implement an interface
     *                 RequestCallback{@literal <}User>. This object will be used to
     *                 send callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour see
     * {@link WebServiceAsync}. Task has priority: High Priority.
     */
    @Deprecated
    private static RequestTask<User> userReRegister(User user, int flags,
                                                   RequestCallback<User> callback) {
        if (checkApi(callback)) {
            Connection connection = getActiveConnection();
            String login = connection.getLogin();
            String password = connection.getOtp();
            HonkioApi.logout(Message.FLAG_HIGH_PRIORITY, null);

            @SuppressWarnings("ConstantConditions")
            Identity shopIdentity = getMainShopInfo().getMerchant().getRegistrationShopIdentity();

            if (shopIdentity == null || shopIdentity.isCorrupt()) {
                callback.onError(new RpError(RpError.Api.UNSUPPORTED_REQUEST));
                return null;
            }
            Message<User> message = new Message<>(getContext(), Command.USER_SET, getUrl(),
                    null, getDevice(), shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(User.class), flags);
            message.setCacheFileName(Command.USER_GET, false);
            message.setSupportOffline(false);
            message.addParameter(Param.LOGIN_IDENTITY, login);
            message.addParameter(Param.LOGIN_PASSWORD, password);
            message.addParameter(Param.USER_RE_REGISTRATION, true);

            user.applyTo(message);

            WebServiceAsync<User> webService
                    = new RegistrationWebServiceAsync(getContext(), message, callback);
            webService.execute();
            return webService;
        }
        return null;
    }

    /**
     * Request to re-query pending registration request.
     * <br><b>Deprecated:</b> Registration should be processed in web form
     *
     * @param queryId  Registration request ID.
     * @param callback The Object that implement an interface RequestCallback
     *                 {@literal <}User>. This object will be used to send
     *                 callbacks in to UI thread.
     * @param flags    Flags for request.
     * @return RequestTask implementation. For understanding of the task behaviour
     * see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    @Deprecated // Registration should be processed in web form
    private static RequestTask<User> queryUserRegister(String queryId, int flags,
                                                      RequestCallback<User> callback) {
        if (checkApi(callback)) {
            Identity shopIdentity = getMainShopInfo().getMerchant().getRegistrationShopIdentity();

            if (shopIdentity == null || shopIdentity.isCorrupt()) {
                callback.onError(new RpError(RpError.Api.UNSUPPORTED_REQUEST));
                return null;
            }

            Message<User> message = new Message<>(getContext(), Command.QUERY, getUrl(),
                    getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(User.class), flags);
            message.addParameter(Param.ID, queryId);

            WebServiceAsync<User> webService
                    = new WebServiceAsync<>(getContext(), message, callback);
            webService.execute();
            return webService;
        }
        return null;
    }

    @Deprecated
    public static RequestTask<UserPayment> paymentRequest(Invoice invoice, UserAccount account,
                                              int flags, RequestCallback<UserPayment> callback) {
        return paymentRequest(invoice, account, getMainShopInfo().getShop(), flags, callback);
    }

    @Deprecated
    public static RequestTask<UserPayment> paymentRequest(Invoice invoice, UserAccount account,
                                      Shop shop, int flags, RequestCallback<UserPayment> callback) {
        return paymentRequest(invoice, account, shop, null, flags, callback);
    }

    @Deprecated
    public static RequestTask<UserPayment> paymentRequest(Invoice invoice, UserAccount account,
                                              Shop shop, HashMap<String, Object> params,
                                              int flags, RequestCallback<UserPayment> callback) {

        HashMap<String, Object> paymentParams = invoice.toParams();

        if (params != null)
            paymentParams.putAll(params);

        return paymentRequest(account, shop, null, paymentParams, flags, callback);
    }

    /**
     * Request to dispute some transaction.
     *
     * @param text          Dispute message from the user.
     * @param transactionId Id of the transaction that will dispute.
     * @param flags         Flags for request.
     * @param callback      The Object that implement an interface RequestCallback
     *                      {@literal <}Void>. This object will be used to send
     *                      callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour see
     * {@link WebServiceAsync}. Task has priority: High Priority.
     */
    @Deprecated
    public static RequestTask<Void> userDispute(String text, String transactionId, int flags,
                                                RequestCallback<Void> callback) {
        if (checkApi(callback)) {
            Message<Void> message = new Message<>(getContext(), Command.USER_DISPUTE, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(Void.class), flags);
            message.addParameter(Param.ID, transactionId);
            message.addParameter(Param.DISPUTE_TEXT, text);

            WebServiceAsync<Void> webService = new WebServiceAsync<>(getContext(), message, callback);
            webService.execute();
            return webService;
        }
        return null;
    }

    @Deprecated
    public static RequestTask<InvoicesList> userGetInvoices(InvoiceFilter filter, int flags,
                                                        RequestCallback<InvoicesList> callback) {
        if (checkApi(callback)) {
            Message<InvoicesList> message = new Message<>(getContext(),
                    Command.USER_INVOICE, getUrl(),
                    getActiveConnection(), getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(InvoicesList.class), flags);
            message.setCacheFileName(Command.USER_INVOICE, true);

            if (filter != null) {
                filter.applyTo(message);
            }

            return new WebServiceAsync<>(getContext(), message, callback).execute();
        }
        return null;
    }

}
