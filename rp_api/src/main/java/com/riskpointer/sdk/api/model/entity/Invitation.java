package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Shurygin Denis
 */

public class Invitation implements Serializable {
    private String mId;
    private double mBonus;
    private String mMessage;
    private Map<String, Object> mCustomFields;
    private Map<String, Object> mInviter;
    private Boolean isUsed;
    private Order mOrder;
    private String mReceiverId;
    private String mStatus;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public double getBonus() {
        return mBonus;
    }

    public void setBonus(double bonus) {
        this.mBonus = bonus;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        this.mMessage = message;
    }

    public Boolean isUsed() {
        return isUsed;
    }

    public void setUsed(Boolean used) {
        isUsed = used;
    }

    public void setCustom(String name, Object value) {
        if (mCustomFields == null)
            mCustomFields = new HashMap<>();

        mCustomFields.put(name, value);
    }

    public void removeCustom(String name) {
        if (mCustomFields != null) {
            mCustomFields.remove(name);
        }
    }

    public Object getCustom(String name) {
        if (mCustomFields != null) {
            return mCustomFields.get(name);
        }
        return null;
    }

    public void setCustomFields(Map<String, Object> custom_fields) {
        this.mCustomFields = custom_fields;
    }

    public Map<String, Object> getCustomFields() {
        return mCustomFields;
    }

    public void setInviter(Map<String, Object> inviter) {
        this.mInviter = inviter;
    }

    public Map<String, Object> getInviter() {
        return mInviter;
    }

    public Order getOrder() {
        return mOrder;
    }

    public void setOrder(Order order) {
        mOrder = order;
    }

    public String getReceiverId() {
        return mReceiverId;
    }

    public void setReceiverId(String receiverId) {
        mReceiverId = receiverId;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }
}
