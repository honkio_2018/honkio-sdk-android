/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.message.filter;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by I.N. on 2017-02-16.
 */
public class AssetFilter extends ListFilter implements HKMapFilter {
    private static final long serialVersionUID = -8441914039864078460L;
    private Double mLatitude;
    private Double mLongitude;
    private String mRadius;
    private String[] mStructureIds;
    private String mMerchantId;
    private boolean mQuerySchedule;
    private PropertiesFilter mPropertiesFilter;
    private SortFilter mSortFilter;

    public AssetFilter() {
        super();
    }

    public AssetFilter(String id) {
        super(id);
    }

    public AssetFilter(AssetFilter filter) {
        super(filter);

        this.mLatitude = filter.mLatitude;
        this.mLongitude = filter.mLongitude;
        this.mRadius = filter.mRadius;
        this.mMerchantId = filter.mMerchantId;

        if (filter.mStructureIds != null)
            this.mStructureIds = Arrays.copyOf(filter.mStructureIds, filter.mStructureIds.length);

        if (filter.mPropertiesFilter != null)
            this.mPropertiesFilter = new PropertiesFilter(filter.mPropertiesFilter);

        if (filter.mSortFilter != null)
            this.mSortFilter = new SortFilter(filter.mSortFilter);
    }

    public Double getLatitude() {
        return mLatitude;
    }

    public AssetFilter setLatitude(Double mLatitude) {
        this.mLatitude = mLatitude;
        return this;
    }

    public Double getLongitude() {
        return mLongitude;
    }

    public AssetFilter setLongitude(Double mLongitude) {
        this.mLongitude = mLongitude;
        return this;
    }

    /**
     * Gets radius of the search.
     *
     * @return Radius of the search.
     */
    public String getRadius() {
        return mRadius;
    }

    /**
     * Sets radius of the search.
     *
     * @param radius Radius of the search.
     */
    public AssetFilter setRadius(String radius) {
        this.mRadius = radius;
        return this;
    }

    public boolean isQuerySchedule() {
        return mQuerySchedule;
    }

    public AssetFilter setQuerySchedule(boolean querySchedule) {
        mQuerySchedule = querySchedule;
        return this;
    }

    public String[] getStructureIds() {
        return mStructureIds;
    }

    public AssetFilter setStructureId(String structureId) {
        mStructureIds = new String[] { structureId };
        return this;
    }

    public AssetFilter setStructureIds(String... structureIds) {
        mStructureIds = structureIds;
        return this;
    }

    public PropertiesFilter getPropertiesFilter() {
        return mPropertiesFilter;
    }

    public AssetFilter setPropertiesFilter(PropertiesFilter filter) {
        this.mPropertiesFilter = filter;
        return this;
    }

    public SortFilter getSortFilter() {
        return mSortFilter;
    }

    public AssetFilter setSortFilter(SortFilter filter) {
        this.mSortFilter = filter;
        return this;
    }

    public String getMerchantId() {
        return mMerchantId;
    }

    public AssetFilter setMerchantId(String mMerchantId) {
        this.mMerchantId = mMerchantId;
        return this;
    }

    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = super.toParams();

        AbsFilter.applyIfNotNull(params, "query_search_latitude", doubleToString(getLatitude()));//TODO server "invalid message security in case of some decimal format"
        AbsFilter.applyIfNotNull(params, "query_search_longitude", doubleToString(getLongitude()));
        AbsFilter.applyIfNotNull(params, "query_distance", getRadius());
        AbsFilter.applyIfNotNull(params, "query_merchant", getMerchantId());
        if (isQuerySchedule()) params.put("query_schedule", true);

        if (mPropertiesFilter != null)
            AbsFilter.applyIfNotNull(params, "query_properties", mPropertiesFilter.toParams());

        if (mSortFilter != null)
            AbsFilter.applyIfNotNull(params, "query_sort", mSortFilter.toParams());

        String[] structures = getStructureIds();
        if (structures != null) {
            if (structures.length == 1)
                AbsFilter.applyIfNotNull(params, "query_structure", structures[0]);
            else
                AbsFilter.applyIfNotNull(params, "query_structure", structures);
        }
        return params;
    }

    private String doubleToString(Double value) {
        if (getLatitude() != null) {
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
            otherSymbols.setDecimalSeparator('.');
            DecimalFormat sf = new DecimalFormat("0.000000", otherSymbols);
            double result = (double) value;
            return sf.format(result);
        } else {
            return null;
        }
    }
}
