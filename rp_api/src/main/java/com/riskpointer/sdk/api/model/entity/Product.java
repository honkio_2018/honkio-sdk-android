/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.entity;

import android.text.TextUtils;

import com.riskpointer.sdk.api.web.message.filter.AbsFilter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Data object that hold information about a product.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 *
 * @author Shurygin Denis
 */
public class Product extends HKModelEntity {
	private static final long serialVersionUID = 1L;

	private String mId;
	private String mName;
	private String mDescription;
	private String mCurrency;
	private double mAmount;
	private double mAmountVat;
	private double mAmountNoVat;
	private double mAmountVatPercent;
	private HashSet<String> mDisallowedAccounts = new HashSet<>();

	private long mValidityTime;
	private String mValidityTimeType;

    private HashMap<String, Double> mSupplus;
    private ArrayList<Pic> mPics;
	private ArrayList<String> mTagIds;
	private ArrayList<Target> mTargetProducts;

	private Long mDuration;
	private String mIdType;
	private boolean mActive;
	private boolean mVisible;

	public Product() {
		super();
	}

	public Product(String id) {
		this();
		mId = id;
	}

	public Product(Map<String, Object> props) {
		super(props);
	}

	public Product(Product product) {
		super(product);

		mId = product.mId;
		mName = product.mName;
		mDescription = product.mDescription;
		mCurrency = product.mCurrency;
		mAmount = product.mAmount;
		mAmountVat = product.mAmountVat;
		mAmountNoVat = product.mAmountNoVat;
		mAmountVatPercent = product.mAmountVatPercent;

		mValidityTime = product.mValidityTime;
		mValidityTimeType = product.mValidityTimeType;

		if (product.mSupplus != null)
			mSupplus = new HashMap<>(product.mSupplus);

		if (product.mPics != null)
			mPics = new ArrayList<>(product.mPics);

		if (product.mTagIds != null)
			mTagIds = new ArrayList<>(product.mTagIds);

		if (product.mTargetProducts != null)
			mTargetProducts = new ArrayList<>(product.mTargetProducts);

		mDuration = product.mDuration;
	}

	public String getId() {
		return mId;
	}

	public void setId(String id) {
		this.mId = id;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		this.mName = name;
	}

	public String getDescription() {
		return mDescription;
	}

	public void setDescription(String description) {
		this.mDescription = description;
	}

	public String getCurrency() {
		return mCurrency;
	}

	public void setCurrency(String currency) {
		this.mCurrency = currency;
	}

	public double getAmount() {
		return mAmount;
	}

	public void setAmount(double amount) {
		this.mAmount = amount;
	}

	public double getAmountVat() {
		return mAmountVat;
	}

	public void setAmountVat(double amountVat) {
		this.mAmountVat = amountVat;
	}

	public double getAmountNoVat() {
		return mAmountNoVat;
	}

	public void setAmountNoVat(double amountNoVat) {
		this.mAmountNoVat = amountNoVat;
	}

	public double getAmountVatPercent() {
		return mAmountVatPercent;
	}

	public void setAmountVatPercent(double amountVatPercent) {
		this.mAmountVatPercent = amountVatPercent;
	}

	/**
	 * Gets validity time.
	 * @see #getValidityTimeType()
	 * @return Validity time.
	 */
	public long getValidityTime() {
		return mValidityTime;
	}

	/**
	 * Sets validity time.
	 * @see #setValidityTimeType(String)
	 * @param time Validity time.
	 */
	public void setValidityTime(long time) {
		this.mValidityTime = time;
	}

	/**
	 * Gets validity time type.
	 * @return Validity time.
	 */
	public String getValidityTimeType() {
		return mValidityTimeType;
	}

	/**
	 * Sets validity time type.
	 * @param type Validity time type.
	 */
	public void setValidityTimeType(String type) {
		this.mValidityTimeType = type;
	}

	public boolean isHasTimeValidity() {
		return !TextUtils.isEmpty(mValidityTimeType) && mValidityTime > 0;
	}

	/**
	 * Gets Supplus key/value map.<br>
	 * Key is the Payment Account Type, the value is amount of money which will added to original price when specified account will used for a payment.
	 * @return Supplus key/value map.
	 */
	@Deprecated
	public HashMap<String, Double> getSupplusMap() {
		return mSupplus;
	}

	/**
	 * Sets Supplus key/value map.<br>
	 * Key is the Payment Account Type, the value is amount of money which will added to original price when specified account will used for a payment.
	 * @param map Supplus key/value map.
	 */
	@Deprecated
	public void setSupplusMap(HashMap<String, Double> map) {
		this.mSupplus = map;
	}

	/**
	 * Gets Supplus for specified Payment Account Type. Returned value is amount of money which will added to original price when specified account will used for a payment.
	 * @param accountType Payment Account Type
	 * @return Supplus for specified Payment Account Type. Returned value is amount of money which will added to original price when specified account will used for a payment.
	 * @see UserAccount.Type
	 */
	@Deprecated
	public double getSupplusFor(String accountType) {
        if (mSupplus != null) {
            Double supplus = mSupplus.get(accountType);
            if (supplus != null)
                return supplus;
        }
		return 0;
	}

	/**
	 * Gets Pictures list of the Product.
	 * @return Pictures list of the Product.
	 */
    public ArrayList<Pic> getPicsList() {
        return mPics;
    }

	/**
	 * Sets Pictures list of the Product.
	 * @param pics Pictures list of the Product.
	 */
    public void setPicsList(ArrayList<Pic> pics) {
        mPics = pics;
    }

	public ArrayList<String> getTagIds() {
		return mTagIds;
	}

	public void setTagIds(ArrayList<String> tagIds) {
		this.mTagIds = tagIds;
	}

	public boolean isHasTag(String tag) {
		return mTagIds.contains(tag);
	}

	public Long getDuration() {
		return mDuration;
	}

	public void setDuration(Long duration) {
		this.mDuration = duration;
	}

	public String getIdType() {
		return mIdType;
	}

	public void setIdType(String id_type) {
		this.mIdType = id_type;
	}

	public boolean isActive() {
		return mActive;
	}

	public void setActive(boolean active) {
		this.mActive = active;
	}

	public boolean isVisible() {
		return mVisible;
	}

	public void setVisible(boolean visible) {
		this.mVisible = visible;
	}

	public ArrayList<Target> getTargetProducts() {
		return mTargetProducts;
	}

	public void setTargetProducts(ArrayList<Target> list) {
		mTargetProducts = list;
	}

	public Target findTarget(String productId) {
		if (productId == null)
			return null;

		if (mTargetProducts != null)
			for (Target target: mTargetProducts) {
				if (productId.equals(target.id))
					return target;
			}

		return null;
	}

	public boolean isCanBeExchanged() {
		return mTargetProducts != null && mTargetProducts.size() > 0;
	}

	public Set<String> getDisallowedAccounts() {
		return mDisallowedAccounts;
	}

	public void setDisallowedAccounts(Collection<String> accountsSet) {
		mDisallowedAccounts.clear();
		mDisallowedAccounts.addAll(accountsSet);
	}

	/**
	 * Data object that hold information about a picture of a product.
	 *
	 * @author Shurygin Denis
	 */
    public static class Pic implements Serializable {
        private static final long serialVersionUID = 1L;
		/** URL of the picture. */
        public final String url;
		/** Width of the picture. */
        public final int width;
		/** Height of the picture. */
        public final int height;

		/**
		 * Public Constructor.
		 * @param url URL of the picture.
		 * @param width Width of the picture.
		 * @param height Height of the picture.
		 */
        public Pic(String url, int width, int height) {
            this.url = url;
            this.width = width;
            this.height = height;
        }
    }

	public static class Target implements Serializable {

		public final String id;
		public final int count;

		public Target(String id, int count) {
			this.id = id;
			this.count = count;
		}
	}

    public HashMap<String, Object> toParams() {
        HashMap<String, Object> hashMap = new HashMap<>();

        AbsFilter.applyIfNotNull(hashMap, "id", getId());
        AbsFilter.applyIfNotNull(hashMap, "name", getName());
        AbsFilter.applyIfNotNull(hashMap, "description", getDescription());
        AbsFilter.applyIfNotNull(hashMap, "currency", getCurrency());
        AbsFilter.applyIfNotNull(hashMap, "amount", Double.toString(getAmount()));//todo temporary string, because of server
        AbsFilter.applyIfNotNull(hashMap, "amount_vat", Double.toString(getAmountVat()));//todo temporary string, because of server
        AbsFilter.applyIfNotNull(hashMap, "amount_no_vat", Double.toString(getAmountNoVat()));//todo temporary string, because of server
        AbsFilter.applyIfNotNull(hashMap, "amount_vat_percent", getAmountVatPercent());
        AbsFilter.applyIfNotNull(hashMap, "int_vat", getAmountVatPercent());
        AbsFilter.applyIfNotNull(hashMap, "int_validity_time", getValidityTime());
        AbsFilter.applyIfNotNull(hashMap, "str_validity_time_type", getValidityTimeType());
        AbsFilter.applyIfNotNull(hashMap, "id_type", getIdType());
        AbsFilter.applyIfNotNull(hashMap, "active", isActive());
        if (getDuration() != null && getDuration() > 0) AbsFilter.applyIfNotNull(hashMap, "int_duration", getDuration());
        if (getTargetProducts() != null) {
            List<Map<String, Object>> products = new ArrayList<>(getTargetProducts().size());
            for (Product.Target product_target : getTargetProducts()) {
                Map<String, Object> product = new HashMap<>();
                product.put("id", product_target.id);
                product.put("count", product_target.count);
                products.add(product);
            }
            AbsFilter.applyIfNotNull(hashMap, "target_products", products);
        }

        if (getTagIds() != null && getTagIds().size()>0) {
            AbsFilter.applyIfNotNull(hashMap, "list_tags", getTagIds());
        }
        if (getPicsList() != null) {
            List<Map<String, Object>> products = new ArrayList<>(getPicsList().size());
            for (Product.Pic product_pic : getPicsList()) {
                Map<String, Object> product = new HashMap<>();
                product.put("x", product_pic.width);
                product.put("y", product_pic.height);
                product.put("url", product_pic.url);
                products.add(product);
            }
            AbsFilter.applyIfNotNull(hashMap, "list_pics", products);
        }
        return hashMap;
    }
}

