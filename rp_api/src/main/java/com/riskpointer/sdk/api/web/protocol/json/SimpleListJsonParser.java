package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.SimpleListItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Parse List of SimpleListItem's from JSON.
 */
public abstract class SimpleListJsonParser extends BaseJsonResponseParser<ArrayList<SimpleListItem>> {

    protected ArrayList<SimpleListItem> parseList(JSONArray itemsList) {
        ArrayList<SimpleListItem> list = new ArrayList<SimpleListItem>();
        if (itemsList != null) {
            for (int i = 0; i < itemsList.length(); i++) {
                JSONObject itemJson = itemsList.optJSONObject(i);
                if (itemJson != null) {
                    String string = null;
                    try {
                        string = itemJson.toString(2);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        string = itemJson.toString();
                    }
                    list.add(new SimpleListItem(itemJson.optString("id", ""), string, itemJson.optString("str_name", "")));
                }
            }
        }
        return list;
    }
}
