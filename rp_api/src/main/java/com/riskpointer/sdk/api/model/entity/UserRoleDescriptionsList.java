package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.List;

/**
 * @author Shurygin Denis
 */

public class UserRoleDescriptionsList implements Serializable {

    private List<UserRoleDescription> mList;

    public void setList(List<UserRoleDescription> list) {
        mList = list;
    }

    public List<UserRoleDescription> getList() {
        return mList;
    }

}
