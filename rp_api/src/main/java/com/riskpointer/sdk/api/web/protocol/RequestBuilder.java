/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol;

import java.util.Map;

/**
 * The builder to build request string.
 */
public interface RequestBuilder {

	// TODO write docs
	String build(Map<String, Object> map);

	// TODO write docs
	String calculateHash(Map<String, Object> map, String hashKey);

}
