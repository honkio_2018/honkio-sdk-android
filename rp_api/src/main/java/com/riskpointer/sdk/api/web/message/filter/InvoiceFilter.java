package com.riskpointer.sdk.api.web.message.filter;

import android.content.Context;

import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.model.entity.Invoice;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.HonkioApi;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Filter for userGetInvoices request.
 *
 * @see Invoice
 * @see HonkioApi#userGetInvoices(Context, RequestCallback, InvoiceFilter, int)
 *
 * @author Denis Shurygin
 */
public class InvoiceFilter extends AbsFilter implements Serializable {

    private String mId;
    private Integer mCount;
    private Integer mSkip;
    private String mStatus;

    /**
     * Gets Id of Invoice that should be returned.
     * @return Id of Invoice that should be returned.
     */
    public String getId() {
        return mId;
    }

    /**
     * Sets Id of Invoice that should be returned.
     * @param id Id of Invoice that should be returned.
     */
    public void setId(String id) {
        this.mId = id;
    }

    /**
     * Gets Invoices count limit.
     * @return Invoices count limit.
     */
    public Integer getCount() {
        return mCount;
    }

    /**
     * Sets Invoices count limit for request.
     * @param count Invoices count limit. Set null to set default limit.
     */
    public void setCount(Integer count) {
        this.mCount = count;
    }

    /**
     * Gets Invoices count to skip.
     * @return Invoices count to skip.
     */
    public Integer getSkip() {
        return mSkip;
    }

    /**
     * Sets Invoices count to skip.
     * @param skip Invoices count to skip. Set null to set default value = 0.
     */
    public void setSkip(Integer skip) {
        this.mSkip = skip;
    }

    /**
     * Gets Invoice status specified for the filter.
     * @return Invoice status specified for the filter.
     * @see Invoice.Status
     */
    public String getStatus() {
        return mStatus;
    }

    /**
     * Sets Invoice status. Only Invoices with the specified status will be found.
     * @param status Invoice status.
     * @see Invoice.Status
     */
    public void setStatus(String status) {
        this.mStatus = status;
    }

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = new HashMap<>();

        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_COUNT, getCount());
        applyIfNotNull(params, Message.Param.QUERY_SKIP, getSkip());
        applyIfNotNull(params, Message.Param.QUERY_STATUS, getStatus());
        applyIfNotNull(params, Message.Param.QUERY_ID, getId());

        return params;
    }
}
