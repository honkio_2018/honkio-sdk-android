package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.UserRoleDescription;
import com.riskpointer.sdk.api.model.entity.UserRoleDescriptionsList;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shurygin Denis
 */

public class UserRoleDescriptionsListJsonParser extends BaseJsonResponseParser<UserRoleDescriptionsList> {

    @Override
    public UserRoleDescriptionsList parseJson(Message<?> message, JSONObject json) {
        UserRoleDescriptionsList result = new UserRoleDescriptionsList();

        List<UserRoleDescription> list = new ArrayList<>();
        result.setList(list);

        JSONArray rolesArray = json.optJSONArray("roles");
        if (rolesArray != null) {
            for (int i = 0; i < rolesArray.length(); i++) {
                JSONObject roleJson = rolesArray.optJSONObject(i);
                if (roleJson != null) {
                    list.add(UserRoleDescriptionJsonParser.parseRoleDescription(roleJson));
                }
            }
        }
        return result;
    }
}