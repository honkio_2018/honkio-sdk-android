/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.process.user;

import android.content.Context;
import android.os.Bundle;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.api.model.entity.UserPayment;
import com.riskpointer.sdk.api.model.process.BaseProcessModel;
import com.riskpointer.sdk.api.model.process.PinReason;
import com.riskpointer.sdk.api.model.task.RequestTask;
import com.riskpointer.sdk.api.model.task.TaskStorage;
import com.riskpointer.sdk.api.utils.ApiValuesUtils;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.callback.RequestCallbackWrapper;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Process model which implement base part of the Payment process.
 */
public abstract class BasePaymentProcessModel<PayParams extends BasePaymentProcessModel.BasePayParams> extends BaseProcessModel {

	private static final String SAVED_PAY_PARAMS = "BasePaymentProcessModel.SAVED_PAY_PARAMS";
	private static final String SAVED_PAY_TASK = "BasePaymentProcessModel.SAVED_PAY_TASK";
	private static final String SAVED_QUERY_PROCESS_MODEL = "BasePaymentProcessModel.SAVED_QUERY_PROCESS_MODEL";

	private PaymentCallbackWrapper mCallback;
	private RequestPinHandler mRequestPinHandler;
	private QueryPaymentProcessModel mQueryProcessModel;

	private PayParams mSavedPayParams;
	private RequestTask<UserPayment> mPaymentTask;

	/**
	 * Construct new Payment model.
	 */
	public BasePaymentProcessModel() {
		super();
	}

	/**
	 * Called when payment request should be executed.
	 * @param context Application context.
	 * @param params Payment params.
	 * @param additionalRequestParams Additional params which should be inserted into request.
	 */
	protected abstract RequestTask<UserPayment> doPayment(Context context, PayParams params, HashMap<String, Object> additionalRequestParams);

	@Override
	public void reset() {
		abort();
		super.reset();
		mQueryProcessModel = null;
	}

	@Override
	public boolean abort() {
		super.abort();
		if(mQueryProcessModel != null)
			mQueryProcessModel.abort();
		return true;
	}

	@Override
	public Bundle saveInstanceState() {
		Bundle savedState = super.saveInstanceState();
		savedState.putSerializable(SAVED_PAY_PARAMS, mSavedPayParams);
		savedState.putInt(SAVED_PAY_TASK, TaskStorage.pushIfNotCompleted(mPaymentTask));
		if (mQueryProcessModel != null)
			savedState.putBundle(SAVED_QUERY_PROCESS_MODEL, mQueryProcessModel.saveInstanceState());
		return savedState;
	}

	@Override
	public void restoreInstanceState(Context context, Bundle instanceState) {
		super.restoreInstanceState(context, instanceState);
		mSavedPayParams = (PayParams) instanceState.getSerializable(SAVED_PAY_PARAMS);
		if (instanceState.containsKey(SAVED_PAY_TASK)) {
			mPaymentTask = (RequestTask<UserPayment>) TaskStorage.pull(instanceState.getInt(SAVED_PAY_TASK));
			if (mPaymentTask != null)
				mPaymentTask.setCallback(mCallback, true);
		}
		if (instanceState.containsKey(SAVED_QUERY_PROCESS_MODEL))
			mQueryProcessModel = QueryPaymentProcessModel.restoreFromSavedState(mCallback, instanceState.getBundle(SAVED_QUERY_PROCESS_MODEL));
	}

	/**
	 * Makes query for pending payment transaction.
	 */
	public void query() {
		if(mQueryProcessModel != null)
			mQueryProcessModel.query();
	}

	/**
	 * Starts loop query for pending payment transaction with specified delay.
	 * @param loopDelay Delay for the next query.
	 */
	public void query(long loopDelay) {
		if(mQueryProcessModel != null)
			mQueryProcessModel.query(loopDelay);
	}

	/**
	 * Stops loop query.
	 */
	public void stopQuery() {
		if(mQueryProcessModel != null)
			mQueryProcessModel.stopQuery();
	}

	/**
	 * Gets Query process model.
	 * @return Query process model.
	 */
	public QueryPaymentProcessModel getQueryProcessModel() {
		return mQueryProcessModel;
	}

	/**
	 * Starts payment process.
	 * @param context Application context.
	 * @param params Payment params.
	 */
	public void pay(Context context, PayParams params) {
		start();
		if (params.getShop() == null)
			params.setShop(HonkioApi.getMainShopInfo().getShop());
		
		if(HonkioApi.checkPaymentAvailable(params.getAccount(), params.getShop(), mCallback)) {
			if(mRequestPinHandler != null) {
				mSavedPayParams = params;
				mRequestPinHandler.onRequestPin(context, PinReason.PURCHASE);
			}
			else {
				mSavedPayParams = null;
				mPaymentTask = doPayment(context, params, null);
			}
		}
	}

	/**
	 * Should be called when PIN entered.
	 * @param context Application context.
	 * @param pin Entered PIN code.
	 * @param delays Delays between PIN buttons clicks. May be null.
	 * @param reason Reason for the PIN request.
	 */
	public void onPinEntered(Context context, String pin, long[] delays, int reason) {
		if (HonkioApi.checkPin(pin)) {
			mPaymentTask = doPayment(context, mSavedPayParams, ApiValuesUtils.putPinDelayParams(null, pin, delays));
		} else if (mCallback != null)
			mCallback.onError(new RpError(RpError.Api.INVALID_PIN));
		mSavedPayParams = null;
	}

	/**
	 * Should be called when PIN pad canceled without entering a PIN.
	 * @param context Application context.
	 * @param reason Reason for the PIN request.
	 */
	public void onPinCanceled(Context context, int reason) {
		if (mCallback != null)
			mCallback.onError(new RpError(RpError.Api.CANCELED_BY_USER));
		mSavedPayParams = null;
	}

	/**
	 * Gets payment callback.
	 * @return Payment callback.
	 */
	public RequestCallback<UserPayment> getCallback() {
		return mCallback;
	}

	/**
	 * Sets payment callback.
	 * @param callback Payment callback.
	 */
	public void setCallback(RequestCallback<UserPayment> callback) {
		if (mCallback == null)
			mCallback = new PaymentCallbackWrapper(callback);
		else
			mCallback.set(callback, false);

		if (mPaymentTask != null)
			mPaymentTask.setCallback(mCallback, true);

		if (mQueryProcessModel != null)
			mQueryProcessModel.setCallback(mCallback);
	}

	/**
	 * Sets specified RequestPinHandler for the process model.
	 * @param requestPinHandler Should be define if PIN should be asked, null otherwise.
	 */
	public void setRequestPinHandler(RequestPinHandler requestPinHandler) {
		mRequestPinHandler = requestPinHandler;
	}

	/**
	 * Base Payment params. Other Payment params should extend this class.
	 */
	public abstract static class BasePayParams implements Serializable {

		private Shop shop;

		private String orderId;

		/** Payment account which will be used for the payment. */
		private UserAccount account;

		private HashSet<String> mDisallowedAccounts = new HashSet<>();

		/** Flags for requests. */
		public final int flags;

		/**
		 * Construct new payment params.
		 * @param shop Shop which will process payment.
		 * @param orderId Order which will process payment.
		 * @param account Payment account which will be used for the payment.
		 * @param flags Flags for requests.
		 */
		public BasePayParams(Shop shop, String orderId, UserAccount account, int flags) {
			this.shop = shop;
			this.orderId = orderId;
			this.account = account;
			this.flags = flags;
		}

		public abstract BasePaymentProcessModel<?> instantiateProcessModel();

		public abstract double getAmount();

		public abstract String getCurrency();

		public void setAccount(UserAccount account) {
			this.account = account;
		}

		public UserAccount getAccount() {
			return account;
		}

		public void setShop(Shop shop) {
			this.shop = shop;
		}

		public Shop getShop() {
			return shop;
		}

		public void setOrderId(String orderId) {
			this.orderId = orderId;
		}

		public String getOrderId() {
			return orderId;
		}

		public Set<String> getDisallowedAccounts() {
			return mDisallowedAccounts;
		}

		public void setDisallowedAccounts(Set<String> accountsSet) {
			mDisallowedAccounts.clear();
			mDisallowedAccounts.addAll(accountsSet);
		}

		public boolean isAccountValid() {
			return account != null && !mDisallowedAccounts.contains(account.getType());
		}
	}

	public interface RequestPinHandler {

		/**
		 * Called when PIN code required. In this case PIN pad should be shown.
		 * @param context Application context.
		 * @param reason Reason for the PIN request.
		 * @see PinReason
		 */
		void onRequestPin(Context context, int reason);
	}

	private class PaymentCallbackWrapper extends RequestCallbackWrapper<UserPayment> {

		public PaymentCallbackWrapper(RequestCallback<UserPayment> callback) {
			super(callback);
		}

		@Override
		public boolean onError(RpError error) {
			mPaymentTask = null;
			return super.onError(error);
		}

		@Override
		public boolean onComplete(Response<UserPayment> response) {
			mPaymentTask = null;
			if(response.isStatusPending()) {
				mQueryProcessModel = new QueryPaymentProcessModel(mCallback, response.getMessage().getShopIdentity(), response.getId());
				setStatus(Status.PENDING);
			}
			else {
				mQueryProcessModel = null;
				setStatus(Status.FINISHED);
			}
            return super.onComplete(response);
		}
	}
}
