package com.riskpointer.sdk.api.model.push.entity;

import com.riskpointer.sdk.api.model.push.Push;

import java.util.Map;

/**
 * @author I.N. 06.02.2019
 */
public class PollChangedPushEntity extends Push.PushEntity {
    private static final String POLL_ID = "poll_id";
    private static final String POLL_TITLE = "poll_title";
    private static final String POLL_TEXT = "poll_text";
    private static final String ACTION = "action";
    private static final String USER_ID = "user_id";
    private static final String USER_NAME = "user_name";

    public final class Action {
        private Action() {
        }
        public final static String CREATED = "created";
        public final static String UPDATED = "updated";
        public final static String REMOVED = "removed";
    }

    public PollChangedPushEntity(Map<String, Object> content) {
        super(Push.Type.POLL_CHANGED, content);
    }

    public void setPollId(String value) {
        setContent(POLL_ID, value);
    }

    public String getPollId() {
        return (String) getContent(POLL_ID);
    }

    public void setPollTitle(String value) {
        setContent(POLL_TITLE, value);
    }

    public String getPollTitle() {
        return (String) getContent(POLL_TITLE);
    }

    public void setPollText(String value) {
        setContent(POLL_TEXT, value);
    }

    public String getPollText() {
        return (String) getContent(POLL_TEXT);
    }

    public void setAction(String value) {
        setContent(ACTION, value);
    }

    public String getAction() {
        return (String) getContent(ACTION);
    }

    public boolean actionIs(String action) {
        String pushAction = getAction();
        return (action != null && action.equals(pushAction))
                || (pushAction == null && action == null);
    }

    public void setUserId(String value) {
        setContent(USER_ID, value);
    }

    public String getUserId() {
        return (String) getContent(USER_ID);
    }

    public void setUserName(String value) {
        setContent(USER_NAME, value);
    }

    public String getUserName() {
        return (String) getContent(USER_NAME);
    }

}
