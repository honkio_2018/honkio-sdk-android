package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.HKDevice;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONObject;

/**
 * @author Shurygin Denis
 */
public class HKDeviceJsonParser extends BaseJsonResponseParser<HKDevice> {

    @Override
    protected HKDevice parseJson(Message<?> message, JSONObject json) {

        HKDevice device = new HKDevice();

        device.setId(json.optString(Message.Param.ID));
        device.setApp(json.optString(Message.Param.DEVICE_APP));
        device.setToken(json.optString(Message.Param.DEVICE_TOKEN));
        device.setPlatform(json.optString(Message.Param.DEVICE_TYPE));
        device.setName(json.optString(Message.Param.DEVICE_NAME));
        device.setNotifyType(json.optString(Message.Param.DEVICE_NOTIFY));

        JSONObject properties = json.optJSONObject("properties");
        if (properties != null) {
            device.setManufacturer(properties.optString(Message.Param.DEVICE_MANUFACTURER));
            device.setModel(properties.optString(Message.Param.DEVICE_MODEL));
            device.setOsVersion(properties.optString(Message.Param.DEVICE_OS_VERSION));
        }

        return device;
    }
}
