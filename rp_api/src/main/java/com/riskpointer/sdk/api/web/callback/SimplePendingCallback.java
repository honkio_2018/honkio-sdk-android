/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.callback;

import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.response.Response;

/**
 * Simple callback that has additional method to handle pending responses.
 */
public interface SimplePendingCallback extends SimpleCallback {

    /**
     * This method will called when the server returned result with pending status.
     * 
     * @param response Response object with pending status.
     *                 
     * @return true if response was handled, false otherwise.
     * All unhandled responses will invoke Broadcast message {@link BroadcastHelper#BROADCAST_ACTION_UNHANDLED_RESPONSE}.
     */
    boolean handlePending(Response<?> response);
}
