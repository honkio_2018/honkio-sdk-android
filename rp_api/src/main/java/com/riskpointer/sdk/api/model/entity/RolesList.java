package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by I.N. on 2018-11-14.
 */

public class RolesList implements Serializable {

    private ArrayList<UserRole> mList;

    public ArrayList<UserRole> getList() {
        return mList;
    }

    public void setList(ArrayList<UserRole> list) {
        mList = list;
    }
}
