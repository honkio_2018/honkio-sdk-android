package com.riskpointer.sdk.api.utils;

import android.text.TextUtils;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.HKDevice;
import com.riskpointer.sdk.api.model.entity.ServerInfo;

/**
 * Created by Shurygin Denis on 2016-01-29.
 */
public class HonkioOAuth {

    public static final String DEFAULT_SCOPE = "email";
    public static final String DEFAULT_REDIRECT_URL = "http%3A%2F%2Flocalhost%3A5000%2Fauthorized";
    public static final String DEFAULT_REDIRECT_URL_FOR_CHECK = "http://localhost:5000/authorized";

    private static final String DEFAULT_AUTHORIZE_URL = "https://oauth.honkio.com/authorize";
    private static final String DEFAULT_TOKEN_URL = "https://oauth.honkio.com/token";
//    private static final String DEFAULT_AUTHORIZE_URL = "http://oauth.honkio2.webdev.softdev.com/authorize";
//    private static final String DEFAULT_TOKEN_URL = "http://oauth.honkio2.webdev.softdev.com/token";

//    private static final String DEFAULT_AUTHORIZE_URL = "http://oauth.app1.honkio.com/authorize";
//    private static final String DEFAULT_TOKEN_URL = "http://oauth.app1.honkio.com/token";

    public static String getAuthorisationUrl(HKDevice device, String clientId, String scope, String redirectUrl) {
        return getAuthorisationUrl()
                +"?platform=" + device.getPlatform()
                +"&device=" + device.getId()
                +"&response_type=code"
                +"&client_id=" + clientId
                +"&redirect_uri=" + redirectUrl
                +"&scope=" + scope;
    }

    public static String getAccessTokenUrl(HKDevice device, String clientId, String clientSecret, String redirectUrl, String authorizationToken) {
        return getAccessTokenUrl()
                +"?platform=" + device.getPlatform()
                +"&device=" + device.getId()
                +"&grant_type=authorization_code"
                +"&code=" + authorizationToken
                +"&client_id=" + clientId
                +"&client_secret=" + clientSecret
                +"&redirect_uri=" + redirectUrl;
    }

    private static String getAuthorisationUrl() {
        ServerInfo serverInfo = HonkioApi.getServerInfo();
        return (serverInfo != null && !TextUtils.isEmpty(serverInfo.getOAuthAuthorizeUrl()) ? serverInfo.getOAuthAuthorizeUrl() : DEFAULT_AUTHORIZE_URL);
    }

    private static String getAccessTokenUrl() {
        ServerInfo serverInfo = HonkioApi.getServerInfo();
        return (serverInfo != null && !TextUtils.isEmpty(serverInfo.getOAuthTokenUrl()) ? serverInfo.getOAuthTokenUrl() : DEFAULT_TOKEN_URL);
    }
}
