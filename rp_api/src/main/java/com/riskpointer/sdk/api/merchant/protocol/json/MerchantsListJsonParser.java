package com.riskpointer.sdk.api.merchant.protocol.json;

import com.riskpointer.sdk.api.model.entity.SimpleListItem;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.protocol.json.SimpleListJsonParser;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Parse Merchants list from JSON.
 */
public class MerchantsListJsonParser extends SimpleListJsonParser {

    @Override
    public ArrayList<SimpleListItem> parseJson(Message<?> message, JSONObject json) {
        return parseList(json.optJSONArray("merchants"));
    }
}
