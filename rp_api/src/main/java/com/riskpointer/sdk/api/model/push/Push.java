package com.riskpointer.sdk.api.model.push;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Data object that hold information about incoming Push message.
 * @param <Entity> Push entity type.
 * @author Shurygin Denis on 2015-04-07.
 */
public class Push<Entity extends Push.PushEntity> implements Serializable {

    /**
     * Types of the Push message.
     */
    public interface Type {
        /** Push doesn't contain entity. */
        String NONE = "";
        /** System message from server. */
        String SYSTEM = "system";
        /** Test push, doesn't contain entity. */
        String TEST = "test";
        /** Transaction push. Corresponded to {@link com.riskpointer.sdk.api.model.push.entity.TransactionPushEntity}. */
        String TRANSACTION = "transactionupdate";
        /** Order push. Corresponded to {@link com.riskpointer.sdk.api.model.push.entity.OrderStatusChangedPushEntity}. */
        String ORDER_STATUS_CHANGED = "orderstatuschanged";
        //TODO docs
        String CHAT_MESSAGE = "newchatmessage";
        /** Received when some user role changed/created/deleted */
        String ROLE_CHANGED = "userrolechanged";
        /** Invitation push, received when new invitation is created */
        String INVITATION = "newinvitation";
        /** Received when asset changed/created/deleted */
        String ASSET_CHANGED = "assetchanged";
        /** Received when poll changed/created/deleted */
        String POLL_CHANGED = "pollchanged";

    }

    private String mId;
    private Entity mEntity;
    private String mMessage;
    private String mBody;
    private long mDate;
    private boolean isRead;

    // TODO write docs
    public String getId() {
        return mId;
    }

    // TODO write docs
    public void setId(String id) {
        mId = id;
    }

    /**
     * Gets push body(JSON string).
     * @return Push body(JSON string).
     */
    public String getBody() {
        return mBody;
    }

    /**
     * Sets push body(JSON string).
     * @param body JSON string.
     */
    public void setBody(String body) {
        this.mBody = body;
    }

    /**
     * Sets push date in millis.
     * @return Push date in millis.
     */
    public long getDate() {
        return mDate;
    }

    /**
     * Sets push date in millis.
     * @param timeMillis Push date in millis.
     */
    public void setDate(long timeMillis) {
        this.mDate = timeMillis;
    }

    /**
     * Gets push message.
     * @return Push message.
     */
    public String getMessage() {
        return mMessage;
    }

    /**
     * Sets push message.
     * @param message Push message.
     */
    public void setMessage(String message) {
        this.mMessage = message;
    }

    /**
     * Gets push entity.
     * @return Push entity.
     * @see com.riskpointer.sdk.api.model.push.Push.Type
     */
    public Entity getEntity() {
        return mEntity;
    }

    /**
     * Sets push entity.
     * @param entity Push entity.
     */
    public void setEntity(Entity entity) {
        this.mEntity = entity;
    }

    /**
     * Gets entity type.
     * @return Entity type.
     */
    public String getType() {
        if (mEntity != null && mEntity.getType() != null)
            return mEntity.getType();
        return Type.NONE;
    }

    // TODO write docs
    public boolean isType(String type) {
        return getType().equals(type);
    }

    // TODO write docs
    public boolean isRead() {
        return isRead;
    }

    // TODO write docs
    public void setRead(boolean read) {
        isRead = read;
    }

    public static class PushEntity implements Serializable {

        private String mType;
        private HashMap<String, Object> mContent;

        public PushEntity(String type) {
            this(type, new HashMap<String, Object>());
        }

        public PushEntity(String type, Map<String, Object> content) {
            if (content == null)
                throw new IllegalArgumentException("Content must be not null");

            mType = type;

            if (content instanceof HashMap)
                mContent = (HashMap<String, Object>) content;
            else
                mContent = new HashMap<>(content);
        }

        /**
         * Gets Push type. Type should be corresponded to Push entity class.
         * @return Push type.
         * @see com.riskpointer.sdk.api.model.push.Push.Type
         */
        public String getType() {
            return mType;
        }

        public void setContent(String key, Serializable value) {
            mContent.put(key, value);
        }

        public Object getContent(String key) {
            return mContent.get(key);
        }

        public String getUid() {
            return "null";
        }
    }
}
