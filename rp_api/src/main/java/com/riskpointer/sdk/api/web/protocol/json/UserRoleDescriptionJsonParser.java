package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.UserRoleDescription;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONObject;

/**
 * @author Shurygin Denis
 */

public class UserRoleDescriptionJsonParser extends BaseJsonResponseParser<UserRoleDescription> {

    @Override
    protected UserRoleDescription parseJson(Message<?> message, JSONObject json) {
        return parseRoleDescription(json);
    }

    public static UserRoleDescription parseRoleDescription(JSONObject json) {
        UserRoleDescription description = new UserRoleDescription();
        description.setId(json.optString("_id"));
        description.setName(json.optString("name"));
        description.setType(json.optString("property"));
        description.setDisplayName(json.optString("display_name"));
        description.setDescription(json.optString("description"));
        description.setTotalUsers(json.optInt("users_count", 0));
        return description;
    }
}
