package com.riskpointer.sdk.api.model.push.entity;

import com.riskpointer.sdk.api.model.push.Push;

import java.io.Serializable;

/**
 * Transaction push entity that hold information about transaction push message.
 * @author Shurygin Denis on 2015-01-29.
 */
public class TransactionPushEntity extends Push.PushEntity implements Serializable{

    /** Available types of push messages. */
    public interface Type {
        /** User start new transaction. */
        String NEW_PAYMENT = "consumerpayment";
        /** Merchant chang exist transaction. */
        String TRANSACTION_UPDATE = "merchanttransactionupdate";
    }

    private String mShopId;
    private String mTransactionId;
    private String mStatus;

    public TransactionPushEntity() {
        super(Push.Type.TRANSACTION);
    }

    @Override
    public String getUid() {
        String shopId = getShopId();
        String transactionId = getTransactionId();
        return (shopId != null ? shopId : "null") + "|" + (transactionId != null ? transactionId : "null");
    }

    /**
     * Gets Id of the shop that contains transaction.
     * @return Id of the shop that contains transaction.
     */
    public String getShopId() {
        return mShopId;
    }

    /**
     * Sets Id of the shop that contains transaction.
     * @param shopId Id of the shop that contains transaction.
     */
    public void setShopId(String shopId) {
        this.mShopId = shopId;
    }

    /**
     * Gets Id of the transaction.
     * @return Id of the transaction.
     */
    public String getTransactionId() {
        return mTransactionId;
    }

    /**
     * Sets Id of the transaction.
     * @param transactionId Id of the transaction.
     */
    public void setTransactionId(String transactionId) {
        this.mTransactionId = transactionId;
    }

    /**
     * Gets Transaction status.
     * @return transaction status.
     * @see com.riskpointer.sdk.api.web.response.Response.Status
     */
    public String getStatus() {
        return mStatus;
    }

    /**
     * Sets Transaction status.
     * @param status Transaction status.
     * @see com.riskpointer.sdk.api.web.response.Response.Status
     */
    public void setStatus(String status) {
        this.mStatus = status;
    }
}
