/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;

/**
 * Data object that hold loaded file.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 *
 * @author Denis Shurygin
 */
public class ServerFile implements Serializable{
	private static final long serialVersionUID = 8124280782917333723L;

	/** License file name pattern. */
	public static final String LICENSE_PATTERN = "%0$s.license.html";

	private String mName;
	private byte[] mData;
	private String mMime;

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		mName = name;
	}

	public byte[] getData() {
		return mData;
	}

	public void setData(byte[] data) {
		this.mData = data;
	}

	public String getMime() {
		return mMime;
	}

	public void setMime(String mime) {
		this.mMime = mime;
	}
}
