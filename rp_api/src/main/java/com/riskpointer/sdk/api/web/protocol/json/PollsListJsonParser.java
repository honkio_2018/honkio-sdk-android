package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Poll;
import com.riskpointer.sdk.api.model.entity.PollAnswersList;
import com.riskpointer.sdk.api.model.entity.PollsList;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Parse Polls list from JSON
 */
public class PollsListJsonParser extends BaseJsonResponseParser<PollsList> {

    @Override
    public PollsList parseJson(Message<?> message, JSONObject json) {
        PollsList pollsList = new PollsList();
        ArrayList<Poll> list = new ArrayList<>();

        JSONArray consumerListJson = json.optJSONArray("polls");
        if (consumerListJson != null) {
            for (int i = 0; i < consumerListJson.length(); i++) {
                JSONObject pollJson = consumerListJson.optJSONObject(i);
                if (pollJson != null) {
                    Poll poll = PollJsonParser.parsePoll(pollJson);
                    if (poll != null) {
                        list.add(poll);
                    }
                }
            }
        }
        pollsList.setList(list);
        if (json.has("results")) {
            HashMap<String, PollAnswersList> results = new HashMap<>();
            JSONObject resultsJson = json.optJSONObject("results");
            if (resultsJson != null) {
                Iterator<String> keysResults = resultsJson.keys();
                while (keysResults.hasNext()) {
                    String pollId = keysResults.next();
                    JSONObject answersJson = resultsJson.optJSONObject(pollId);
                    if (answersJson != null) {
                        PollAnswersList answers = new PollAnswersList();
                        Iterator<?> keysOptions = answersJson.keys();
                        while (keysOptions.hasNext()) {
                            String option = (String) keysOptions.next();
                            answers.addAnswer(option, answersJson.optInt(option));
                        }
                        results.put(pollId, answers);
                    }
                }
            }
            pollsList.setPollResults(results);
        }
        if (json.has("votes")) {
            HashMap<String, String> myAnswers = new HashMap<>();
            JSONObject votesJson = json.optJSONObject("votes");
            if (votesJson != null) {
                Iterator<String> keysVotes = votesJson.keys();
                while (keysVotes.hasNext()) {
                    String pollId = keysVotes.next();//poll id
                    myAnswers.put(pollId, votesJson.optString(pollId));
                }
            }
            pollsList.setMyAnswers(myAnswers);
        }
        pollsList.arrangePollsAnswers();
        pollsList.arrangeMyAnswers();
        return pollsList;
    }

}
