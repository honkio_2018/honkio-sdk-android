/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Order;
import com.riskpointer.sdk.api.model.entity.Transaction;
import com.riskpointer.sdk.api.model.entity.Transactions;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Parse Transactions from JSON.
 */
public class TransactionsJsonParser extends BaseJsonResponseParser<Transactions> {

	@Override
	public Transactions parseJson(Message<?> message, JSONObject json) {
		long timeStamp = ApiFormatUtils.stringDateToLong(json.optString("timestamp", null));
		Transactions userTransactions = new Transactions(timeStamp);
		
		JSONArray transactionsJson = json.optJSONArray("transaction");
		if(transactionsJson != null) {
			ArrayList<Transaction> transactions = new ArrayList<>(transactionsJson.length());
			for(int i = 0; i < transactionsJson.length(); i++) {
				JSONObject transactionJson = transactionsJson.optJSONObject(i);
				if(transactionJson != null) {
					Transaction transaction = TransactionJsonParser.toTransaction(transactionJson);
					transactions.add(transaction);
				}
			}
			userTransactions.setTransactionsList(transactions);
		}
		JSONObject ordersObject = json.optJSONObject("orders");
		HashMap<String, Order> ordersMap = new HashMap<>();
		if (ordersMap != null) {
			Iterator<String> keys = ordersObject.keys();
			while (keys.hasNext()) {
				String orderId = keys.next();
				JSONObject orderJson = ordersObject.optJSONObject(orderId);
				if (orderJson != null) {
					ordersMap.put(orderId, OrderJsonParser.parseOrder(orderJson));
				}
			}
		}
		userTransactions.setOrdersMap(ordersMap);
		return userTransactions;
	}
}
