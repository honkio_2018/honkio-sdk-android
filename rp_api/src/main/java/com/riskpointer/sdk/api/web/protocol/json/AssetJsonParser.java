package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Asset;
import com.riskpointer.sdk.api.model.entity.HkStructure;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by I.N. on 2017-02-16.
 */

public class AssetJsonParser extends BaseJsonResponseParser<Asset> {
    @Override
    public Asset parseJson(Message<?> message, JSONObject json) {
        return parseAsset(json);
    }

    public static Asset parseAsset(JSONObject json) {
        if (json != null) {
            Asset asset = new Asset();
            asset.setId(json.optString("id"));
            asset.setLatitude(json.optDouble("latitude"));
            asset.setLongitude(json.optDouble("longitude"));
            asset.setName(json.optString("name"));
            asset.setVisible(json.optBoolean("visible", false));
            asset.setHasValidSchedule(json.optBoolean("has_valid_schedule", false));
            asset.setMerchantId(json.optString("merchant"));
            asset.setGroup(json.optString("group"));
            JSONObject properties = json.optJSONObject("properties");
            if (properties != null) {
                asset.setProperties((HashMap<String, Object>) jsonToMap(properties));
            }

            asset.setStructure(StructureJsonParser.toStructure(json.optJSONObject("structure")));
            if (asset.getStructure() == null) {
                String structureId = json.optString("structure");
                if (structureId != null) {
                    HkStructure structure = new HkStructure();
                    structure.setId(structureId);
                    structure.setMerchant(asset.getMerchantId());
                    asset.setStructure(structure);
                }
            }
            return asset;
        }
        return null;
    }
}
