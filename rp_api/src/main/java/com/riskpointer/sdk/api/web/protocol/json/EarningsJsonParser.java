package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Earnings;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONObject;

/**
 * @author I.N. at 21.11.2018
 */

public class EarningsJsonParser extends BaseJsonResponseParser<Earnings> {
    @Override
    public Earnings parseJson(Message<?> message, JSONObject json) {
        return parseEarnings(json);
    }

    public static Earnings parseEarnings(JSONObject jsonObject) {
        Earnings earnings = null;
        earnings = new Earnings();
        earnings.setAmount(jsonObject.optDouble("amount", 0));
        earnings.setVatAmount(jsonObject.optDouble("vat_amount", 0));
        earnings.setVat(jsonObject.optLong("vat", 0));
        earnings.setPeriodDateStart(ApiFormatUtils.stringDateToLong(jsonObject.optString("period_start")));
        earnings.setPeriodDateEnd(ApiFormatUtils.stringDateToLong(jsonObject.optString("period_end")));
        //todo currency
        return earnings;
    }
}
