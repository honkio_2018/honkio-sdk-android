package com.riskpointer.sdk.api.web.protocol.json;

import android.text.TextUtils;

import com.riskpointer.sdk.api.model.entity.BoughtProduct;
import com.riskpointer.sdk.api.model.entity.Field;
import com.riskpointer.sdk.api.model.entity.Transaction;
import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Parse Transaction from JSON.
 */
public class TransactionJsonParser extends BaseJsonResponseParser<Transaction> {

    @Override
    public Transaction parseJson(Message<?> message, JSONObject json) {
        return toTransaction(json.optJSONObject("transaction"));
    }

    /**
     * Parse Transaction object from the JSON object.
     * @param transactionJson JSON object which contains Transaction.
     * @return Transaction object.
     */
    public static Transaction toTransaction(JSONObject transactionJson) {
        Transaction transaction = null;
        if (transactionJson != null) {
            transaction = new Transaction();
            transaction.setType(transactionJson.optString("type"));
            transaction.setStatus(transactionJson.optString("status"));
            transaction.setId(transactionJson.optString("id"));
            transaction.setAccount(new UserAccount(
                    transactionJson.optString("account_type"),
                    transactionJson.optString("account_number")));
            transaction.setCurrency(transactionJson.optString("currency"));
            transaction.setAmount(transactionJson.optDouble("amount"));
            transaction.setTimeStamp(
                    ApiFormatUtils.stringDateToLong(transactionJson.optString("timestamp")));
            String orderId = transactionJson.optString("order");
            if (orderId!=null) transaction.setOrderId(orderId);
            JSONObject shopJson = transactionJson.optJSONObject("shop");
            if (shopJson != null) {
                transaction.setShopId(shopJson.optString("id"));
                transaction.setShopName(shopJson.optString("name"));
                transaction.setShopReceipt(shopJson.optString("receipt"));
                transaction.setShopReference(shopJson.optString("reference"));
            }

            JSONObject merchantJson = transactionJson.optJSONObject("merchant");
            if (merchantJson != null) {
                transaction.setMerchantName(merchantJson.optString("name"));
            }

            JSONArray productsJson = transactionJson.optJSONArray("product");
            if (productsJson != null) {
                ArrayList<BoughtProduct> products = new ArrayList<>(productsJson.length());
                for (int j = 0; j < productsJson.length(); j++) {
                    JSONObject productJson = productsJson.optJSONObject(j);
                    if (productJson != null) {
                        BoughtProduct product = new BoughtProduct();
                        ParseHelper.parseBoughtProduct(productJson, product);
                        product.setTransactionId(transaction.getId());

                        products.add(product);
                    }
                }
                transaction.setProductsList(products);
            }

            JSONArray messagesJson = transactionJson.optJSONArray("messages");
            if (messagesJson != null) {
                List<Transaction.TransactionMessage> messages
                        = new ArrayList<>(messagesJson.length());
                for (int j = 0; j < messagesJson.length(); j++) {
                    JSONObject messageJson = messagesJson.optJSONObject(j);
                    if (messageJson != null) {
                        String from = messageJson.optString("from", "");
                        String text = messageJson.optString("text", "");
                        long timeStamp = ApiFormatUtils.stringDateToLong(
                                messageJson.optString("timestamp"));
                        messages.add(new Transaction.TransactionMessage(from, text, timeStamp));
                    }
                }
                transaction.setMessagesList(messages);
            }

            JSONObject accountExtraJson = transactionJson.optJSONObject("account_extra");
            if (accountExtraJson != null) {
                transaction.setAccountExtra(parseFields(accountExtraJson));
            }
        }
        return transaction;
    }

    private static HashMap<String, Field<?>> parseFields(JSONObject json) {
        if (json == null)
            return null;

        HashMap<String, Field<?>> map = new HashMap<>();

        Iterator<String> keys = json.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            Field<?> field = parseField(key, json.optJSONObject(key));
            if (field != null)
                map.put(key, field);
        }

        return map;
    }

    private static Field<?> parseField(String name, JSONObject json) {
        if (json == null || name == null)
            return null;

        String type = json.optString("type");
        if (TextUtils.isEmpty(type)) {

            int sepIndex = name.indexOf('_');
            if (sepIndex > 0) {
                type = name.substring(0, sepIndex -1);
            }
        }

        if (TextUtils.isEmpty(type))
            type = Field.Type.STRING;

        Field<?> field = null;

        switch (type) {
            case Field.Type.BOOLEAN:
                Field<Boolean> boolField = new Field<>();
                boolField.setValue(json.optBoolean("value"));
                field = boolField;
                break;
            case Field.Type.STRING:
            default:
                Field<String> strField = new Field<>();
                strField.setValue(json.optString("value"));
                field = strField;
                break;
        }

        field.setType(type);
        field.setValueLabel(json.optString("value_label"));
        field.setLabel(json.optString("label"));
        field.setVisible(json.optBoolean("visible", true));

        return field;
    }
}
