package com.riskpointer.sdk.api.web.message.filter;

import java.util.HashMap;

/**
 * Created by I.N. on 2019-92-01
 */
public class ConsumerFilter extends ListFilter {
    public static final long serialVersionUID = 4401139216270903456L;
    private String mConsumerId;
    private String mRoleId;
    private Boolean mQueryRoles;
    private Boolean mQueryAccount;
    private String mQueryMerchant;

    public Boolean isQueryRoles() {
        return mQueryRoles;
    }

    public void setQueryRoles(Boolean queryRoles) {
        mQueryRoles = queryRoles;
    }

    public String getConsumerId() {
        return mConsumerId;
    }

    public void setConsumerId(String consumerId) {
        mConsumerId = consumerId;
    }

    public String getRoleId() {
        return mRoleId;
    }

    public void setRoleId(String roleId) {
        mRoleId = roleId;
    }

    public Boolean isQueryAccount() {
        return mQueryAccount;
    }

    public void setQueryAccount(Boolean queryAccount) {
        mQueryAccount = queryAccount;
    }

    public String getQueryMerchant() {
        return mQueryMerchant;
    }

    public void setQueryMerchant(String merchant) {
        mQueryMerchant = merchant;
    }

    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = super.toParams();
        AbsFilter.applyIfNotNull(params, "consumer_id", getConsumerId());
        AbsFilter.applyIfNotNull(params, "role_id", getRoleId());
        AbsFilter.applyIfNotNull(params, "query_merchant", getQueryMerchant());
        AbsFilter.applyIfNotNull(params, "query_roles", isQueryRoles());
        AbsFilter.applyIfNotNull(params, "'query_account'", isQueryAccount());
        return params;
    }
}
