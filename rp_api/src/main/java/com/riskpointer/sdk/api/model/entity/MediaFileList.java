package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by I.N. on 2018-02-16.
 */

public class MediaFileList implements Serializable {

    private ArrayList<MediaFile> mList;

    public ArrayList<MediaFile> getList() {
        return mList;
    }

    public void setList(ArrayList<MediaFile> list) {
        mList = list;
    }
}
