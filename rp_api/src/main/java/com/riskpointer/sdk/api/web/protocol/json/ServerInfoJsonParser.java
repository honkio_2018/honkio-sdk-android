/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.model.entity.ServerInfo;
import com.riskpointer.sdk.api.model.entity.ServerInfo.Country;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Parse ServerInfo from JSON.
 */
public class ServerInfoJsonParser extends BaseJsonResponseParser<ServerInfo> {

	@Override
	public ServerInfo parseJson(Message<?> message, JSONObject json) {
		ServerInfo serverInfo = new ServerInfo();
		JSONObject serverJson = json.optJSONObject("server");
		if(serverJson != null) {
            serverInfo.setVersion(serverJson.optInt("version"));
			serverInfo.setVerifyUrl(serverJson.optString("verify_url"));
			serverInfo.setConsumerUrl(serverJson.optString("consumer_url"));
			serverInfo.setOAuthAuthorizeUrl(serverJson.optString("oauth_authorize_url", null));
			serverInfo.setOAuthTokenUrl(serverJson.optString("oauth_token_url", null));
			serverInfo.setServiceFee(serverJson.optDouble("service_fee"));
            serverInfo.setAssetsUrl(serverJson.optString("assets_url", "http://consumer.deepmagic.io"));
			if (serverJson.optInt("creditcall_cc_device_register", 0) == 1) {
				serverInfo.setCreditCallInfo(new ServerInfo.CreditCallInfo(
						serverJson.optString("creditcall_url", ""),
						serverJson.optString("creditcall_terminalid", ""),
						serverJson.optString("creditcall_transactionkey", "")
				));
			}
			
			JSONArray timezoneJson = serverJson.optJSONArray("timezone");
			ArrayList<String> timezoneArray = new ArrayList<String>(timezoneJson.length());
			for(int i = 0; i < timezoneJson.length(); i++)
				timezoneArray.add(timezoneJson.optString(i, ""));
			serverInfo.setTimeZones(timezoneArray);
	
			JSONArray languageJson = serverJson.optJSONArray("language");
			ArrayList<String> languageArray = new ArrayList<String>(languageJson.length());
			for(int i = 0; i < languageJson.length(); i++)
				languageArray.add(languageJson.optString(i, ""));
			serverInfo.setLanguages(languageArray);
			
			JSONArray countrysJson = serverJson.optJSONArray("country");
			ArrayList<Country> countryArray = new ArrayList<Country>(countrysJson.length());
			for(int i = 0; i < countrysJson.length(); i++) {
				Country country = new Country();
				JSONObject countryJson = countrysJson.optJSONObject(i);
				country.setName(countryJson.optString("name"));
				country.setNameTranslated(countryJson.optString("name2"));
				country.setLocale(countryJson.optString("locale"));
				country.setPhonePrefix(countryJson.optString("telephone"));
				country.setIso(countryJson.optString("iso"));
				country.setTimeZone(countryJson.optString("timezone"));
				countryArray.add(country);
			}
			serverInfo.setCountries(countryArray);
		}
		
		return serverInfo;
	}

}
