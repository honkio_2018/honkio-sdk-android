package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Shurygin Denis on 2016-08-03.
 */
public class Rate implements Serializable {

    public final int value;
    public final int count;

    public Rate(int value, int count) {
        this.value = value;
        this.count = count;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (getClass() != obj.getClass())
            return false;

        Rate rate = (Rate) obj;
        return value == rate.value && count == rate.count;
    }

    public static int positiveRatesCount(List<Rate> ratings) {
        int total = 0;
        if (ratings != null) {
            for (Rate rate : ratings) {
                if (rate.value > 0)
                    total += rate.count;
            }
        }
        return total;
    }

    public static int negativeRatesCount(List<Rate> ratings) {
        int total = 0;
        if (ratings != null) {
            for (Rate rate : ratings) {
                if (rate.value < 0)
                    total += rate.count;
            }
        }
        return total;
    }
}
