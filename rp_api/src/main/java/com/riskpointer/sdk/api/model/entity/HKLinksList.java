package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class HKLinksList implements Serializable {

    private ArrayList<HKLink> mList;
    private HashMap<String, Asset> mAssetsMap;

    public ArrayList<HKLink> getList() {
        return mList;
    }

    public void setList(ArrayList<HKLink> list) {
        this.mList = list;
    }


    public HashMap<String, Asset> getAssetsMap() {
        return mAssetsMap;
    }

    public void setAssetsMap(HashMap<String, Asset> assetsMap) {
        this.mAssetsMap = assetsMap;
    }

    public Asset getAsset(String assetId) {
        if (mAssetsMap != null)
            return mAssetsMap.get(assetId);
        return null;
    }
}
