package com.riskpointer.sdk.api.model.entity;

import android.text.TextUtils;

import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.AbsFilter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by I.N. on 2017-02-16.
 */

public class Asset implements Serializable, HkQrCode.QrObject {

    private static final String FIELD_DEFAULT_PICTURE = "default_picture";
    private static final String FIELD_DEFAULT_THUMBNAIL_PICTURE = "default_thumbnail_picture";
    public static final String FIELD_PROPERTIES = "properties";
    public static final String FIELD_NAME = "name";

    private String mId = "";
    private String mName = "";
    private double mLatitude;
    private double mLongitude;
    private boolean isVisible;
    private String mMerchantId;
    private String mGroup;
    private HashMap<String, Object> mProperties = new HashMap<>();
    private HkStructure mStructure;
    private boolean isHasValidSchedule;

    public Asset() {}

    public Asset(Asset asset) {
        if (asset != null) {
            mId = asset.mId;
            mName = asset.mName;
            mLatitude = asset.mLatitude;
            mLongitude = asset.mLongitude;
            isVisible = asset.isVisible;
            mMerchantId = asset.mMerchantId;
            mGroup = asset.mGroup;
            if (asset.mProperties.size() > 0)
                mProperties.putAll(asset.mProperties);
            mStructure = asset.mStructure;
            isHasValidSchedule = asset.isHasValidSchedule;
        }
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public boolean isHasValidSchedule() {
        return isHasValidSchedule;
    }

    public void setHasValidSchedule(boolean hasValidSchedule) {
        isHasValidSchedule = hasValidSchedule;
    }

    public Object getProperty(String name) {
        if (mProperties != null) {
            return mProperties.get(name);
        }
        return null;
    }

    public Number getNumberProperty(String name) {
        if (mProperties != null) {
            Object prop = mProperties.get(name);
            if (prop instanceof Number)
                return (Number) prop;
        }
        return null;
    }

    public String getStringProperty(String name) {
        if (mProperties != null) {
            if (mProperties.get(name) != null)
                if ("null".equals(mProperties.get(name).toString())) {
                    return "";
                } else {
                    return mProperties.get(name).toString();
                }
        }
        return null;
    }

    public boolean getBooleanProperty(String name, boolean defa) {
        if (mProperties != null) {
            if (mProperties.get(name) != null) {
                return mProperties.get(name).toString().equals("true");
            } else {
                return defa;
            }
        }
        return defa;
    }

    public <T> List<T> getArrayProperty(String name) {
        if (mProperties != null) {
            Object propertyObject = mProperties.get(name);
            if (propertyObject != null && propertyObject instanceof List) {
                return (List<T>) propertyObject;
            }
        }
        return null;
    }

    public void setProperty(String name, Object value) {
        if (mProperties != null) {
            if (value != null)
                mProperties.put(name, value);
            else
                mProperties.remove(name);
        } else if (value != null) {
            mProperties = new HashMap<>();
            mProperties.put(name, value);
        }
    }

    public Map<String, Object> getProperties() {
        return mProperties;
    }

    public void setProperties(HashMap<String, Object> settings) {
        mProperties = settings;
    }

    public String getMerchantId() {
        return mMerchantId;
    }

    public void setMerchantId(String merchantId) {
        this.mMerchantId = merchantId;
    }
    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        this.mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        this.mLongitude = longitude;
    }

    public String getGroup() {
        return mGroup;
    }

    public void setGroup(String group) {
        mGroup = group;
    }

    public HkStructure getStructure() {
        return mStructure;
    }

    public boolean structureIs(String... structureIds) {
        for (String structureId: structureIds)
            if (structureIs(structureId))
                return true;
        return false;
    }

    public boolean structureIs(String structureId) {
        if (mStructure == null || mStructure.getId() == null) {
            return structureId == null;
        }
        return mStructure.getId().equals(structureId);
    }

    public void setStructure(HkStructure structure) {
        this.mStructure = structure;
    }

    public void setPictureMediaFile(MediaFile picture) {
        setProperty(FIELD_DEFAULT_PICTURE, picture.getUrl());
        setProperty(FIELD_DEFAULT_THUMBNAIL_PICTURE, picture.getThumbnailUrl());
    }

    public String getPictureUrl() {
        return getStringProperty(FIELD_DEFAULT_PICTURE);
    }

    public String getThumbnailPictureUrl() {
        String url = getStringProperty(FIELD_DEFAULT_THUMBNAIL_PICTURE);
        if (TextUtils.isEmpty(url))
            return getPictureUrl();
        return url;
    }

    public void applyTo(Message<?> message) {
        message.addParameters(toParams());
    }

    public void applyLocationTo(Message<?> message) {
        HashMap<String, Object> params = new HashMap<>();
        AbsFilter.applyIfNotNull(params, "id", getId());
        AbsFilter.applyIfNotNull(params, "latitude", mLatitude);
        AbsFilter.applyIfNotNull(params, "longitude", mLongitude);
        message.addParameters(params);
    }

    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = new HashMap<>();
        AbsFilter.applyIfNotNull(params, "id", getId());
        AbsFilter.applyIfNotNull(params, "merchant", mMerchantId);
        AbsFilter.applyIfNotNull(params, FIELD_NAME, mName);
        AbsFilter.applyIfNotNull(params, "group", mGroup);
        AbsFilter.applyIfNotNull(params, "latitude", mLatitude);
        AbsFilter.applyIfNotNull(params, "longitude", mLongitude);
        AbsFilter.applyIfNotNull(params, "visible", isVisible);
        if (mProperties != null && mProperties.size() > 0)
            params.put(FIELD_PROPERTIES, mProperties);
        if (mStructure != null)
            AbsFilter.applyIfNotNull(params, "structure", mStructure.getId());
        return params;
    }
}
