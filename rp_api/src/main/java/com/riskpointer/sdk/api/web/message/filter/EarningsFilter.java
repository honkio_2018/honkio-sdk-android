package com.riskpointer.sdk.api.web.message.filter;

import android.text.TextUtils;

import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by I.N. 20.11.2018
 */
public class EarningsFilter extends AbsFilter implements Serializable {
    public static class Period {
        public static final String MONTH = "month";
        public static final String YEAR = "year";
        public static final String WEEK = "week";
    }

    public static final String LAST_EVENT_EARNINGS = "last_event_earnings";

    private Long mStartDate;
    private Long mEndDate;
    private String mPeriod;
    private Boolean mLastEventEarnings;
    private String mQueryModel;
    private String[] mQueryStatuses;

    public Long getStartDate() {
        return mStartDate;
    }

    public void setStartDate(Long date) {
        this.mStartDate = date;
    }

    public Long getEndDate() {
        return mEndDate;
    }

    public void setEndDate(Long date) {
        this.mEndDate = date;
    }

    public String getPeriod() {
        return mPeriod;
    }

    public void setPeriod(String period) {
        mPeriod = period;
    }

    public Boolean getLastEventEarnings() {
        return mLastEventEarnings;
    }

    public void setLastEventEarnings(Boolean laseEventEarnings) {
        mLastEventEarnings = laseEventEarnings;
    }

    public String getQueryModel() {
        return mQueryModel;
    }

    public void setQueryModel(String queryModel) {
        mQueryModel = queryModel;
    }

    public String[] getQueryStatuses() {
        return mQueryStatuses;
    }

    public void setQueryStatuses(String[] queryStatuses) {
        mQueryStatuses = queryStatuses;
    }

    protected String dateToString(Long date) {
        if (date == null)
            return null;
        return ApiFormatUtils.dateToServerFormat(date);
    }

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = new HashMap<>();
        applyIfNotNull(params, Message.Param.QUERY_START_DATE, dateToString(getStartDate()));
        applyIfNotNull(params, Message.Param.QUERY_END_DATE, dateToString(getEndDate()));
        applyIfNotNull(params, Message.Param.QUERY_PERIOD, getPeriod());
        applyIfNotNull(params, LAST_EVENT_EARNINGS, getLastEventEarnings());
        applyIfNotNull(params, Message.Param.QUERY_MODEL, getQueryModel());
        if (getQueryStatuses() != null)
            applyIfNotNull(params, Message.Param.QUERY_STATUS, TextUtils.join("|", getQueryStatuses()));
        return params;
    }
}
