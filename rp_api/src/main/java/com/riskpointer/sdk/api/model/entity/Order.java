/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.entity;

import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.AbsFilter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

// TODO: Write correct methods description
/**
 * Data object that hold loaded order.
 * <br><br>Note:
 */
public class Order implements Serializable {

    private String mId;
    private String mModel;
    private String mStatus;
    private String mUserOwner;
    private String mThirdPerson;
    private String mTitle;
    private String mDescription;

    private long mCreationDate;
    private long mExpireDate;
    private long mStartDate;
    private long mEndDate;
    private long mCompletionDate;

    private double mLatitude;
    private double mLongitude;

    private String mCurrency;
    private double mAmount;
    private List<BookedProduct> mProducts;
    private List<UserRoleDescription> mLimitedRoles;

    private String mUserName;

    private Map<String, Object> mCustomFields;

    private UserAccount mAccount;

    /* Shop-Related */
    private String mShopId;
    private String mShopName;
    private String mShopReceipt;
    private String mShopReference;

    private int mUnreadChatMessagesCount;
    private String mAsset;
    private String mParentOrderId;

    public Order() {
        
    }

    public Order(Order order) {
        mId = order.mId;
        mModel = order.mModel;
        mStatus = order.mStatus;
        mUserOwner = order.mUserOwner;
        mThirdPerson = order.mThirdPerson;
        mTitle = order.mTitle;
        mDescription = order.mDescription;

        mCreationDate = order.mCreationDate;
        mExpireDate = order.mExpireDate;
        mStartDate = order.mStartDate;
        mEndDate = order.mEndDate;
        mCompletionDate = order.mCompletionDate;

        mLatitude = order.mLatitude;
        mLongitude = order.mLongitude;

        mCurrency = order.mCurrency;
        mAmount = order.mAmount;
        mAsset = order.mAsset;
        mParentOrderId = order.mParentOrderId;

        if (order.mProducts != null) {
            mProducts = new ArrayList<>(order.mProducts.size());
            for (BookedProduct product: order.mProducts)
                mProducts.add(new BookedProduct(product));
        }

        mUserName = order.mUserName;

        if (order.mCustomFields != null) mCustomFields = new HashMap<>(order.mCustomFields);

        if (order.mAccount != null)
            mAccount = new UserAccount(order.mAccount);

        mShopId = order.mShopId;
        mShopName = order.mShopName;
        mShopReceipt = order.mShopReceipt;
        mShopReference = order.mShopReference;

        if (order.mLimitedRoles != null)
            mLimitedRoles = new ArrayList<>(order.mLimitedRoles);

        mUnreadChatMessagesCount = order.mUnreadChatMessagesCount;
    }

    public void applyTo(Message<?> message) {
        message.addParameters(toParams());
    }

    public HashMap<String, Object> toParams() {
        HashMap<String, Object> hashMap = new HashMap<>();

        AbsFilter.applyIfNotNull(hashMap, "title", mTitle);
        AbsFilter.applyIfNotNull(hashMap, "model", mModel);
        AbsFilter.applyIfNotNull(hashMap, "currency", mCurrency);
        AbsFilter.applyIfNotNull(hashMap, Message.Param.ORDER_ID, mId);
        AbsFilter.applyIfNotNull(hashMap, Message.Param.ORDER_STATUS, mStatus); //TODO: rename to order_status as soon as it will be renamed on the server side
        AbsFilter.applyIfNotNull(hashMap, "description", mDescription);
        AbsFilter.applyIfNotNull(hashMap, "third_person", mThirdPerson);
        AbsFilter.applyIfNotNull(hashMap, "amount", ApiFormatUtils.formatAmount(mAmount));
        AbsFilter.applyIfNotNull(hashMap, "asset", mAsset);
        AbsFilter.applyIfNotNull(hashMap, "user_owner", mUserOwner);
        AbsFilter.applyIfNotNull(hashMap, Message.Param.PARENT_ORDER_ID, mParentOrderId);

        if (mCreationDate > 0)
            AbsFilter.applyIfNotNull(hashMap, "creation_date", ApiFormatUtils.dateToServerFormat(mCreationDate));

        if (mExpireDate > 0)
            AbsFilter.applyIfNotNull(hashMap, "expire_date", ApiFormatUtils.dateToServerFormat(mExpireDate));

        if (mCompletionDate > 0)
            AbsFilter.applyIfNotNull(hashMap, "completion_date",  ApiFormatUtils.dateToServerFormat(mCompletionDate));

        if (mStartDate > 0)
            AbsFilter.applyIfNotNull(hashMap, "start_date",  ApiFormatUtils.dateToServerFormat(mStartDate));

        if (mEndDate > 0)
            AbsFilter.applyIfNotNull(hashMap, "end_date",  ApiFormatUtils.dateToServerFormat(mEndDate));

        AbsFilter.applyIfNotNull(hashMap, "latitude", ApiFormatUtils.formatLocation(mLatitude));
        AbsFilter.applyIfNotNull(hashMap, "longitude", ApiFormatUtils.formatLocation(mLongitude));
        if (mCustomFields != null && mCustomFields.size() > 0)
            hashMap.put("custom_fields", mCustomFields);

        if (mProducts != null) {
            List<Map<String, Object>> products = new ArrayList<>(mProducts.size());
            for(BookedProduct bookedProduct: mProducts) {
                Map<String, Object> product = new HashMap<>();
                product.put(Message.Param.ID, bookedProduct.getProduct().getId());
                product.put("count", bookedProduct.getCount());
                products.add(product);
            }

            AbsFilter.applyIfNotNull(hashMap, "products", products);
        }

        if (mLimitedRoles != null) {
            List<String> rolesList = new ArrayList<>(mLimitedRoles.size());
            for (UserRoleDescription role: mLimitedRoles)
                rolesList.add(role.getId());

            AbsFilter.applyIfNotNull(hashMap, "roles", rolesList);
        }

        if (mAccount != null) {
            AbsFilter.applyIfNotNull(hashMap, "account_type", mAccount.getType());
            AbsFilter.applyIfNotNull(hashMap, "account_number", mAccount.getNumber());
        }

        return hashMap;
    }

    public void setCustom(String name, Object value) {
        if(mCustomFields == null)
            mCustomFields = new HashMap<>();

        mCustomFields.put(name, value);
    }

    public void removeCustom(String name) {
        if (mCustomFields != null) {
            mCustomFields.remove(name);
        }
    }

    public Object getCustom(String name) {
        if (mCustomFields != null) {
            return mCustomFields.get(name);
        }
        return null;
    }

	/* Methods */

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public long getCreationDate() {
        return mCreationDate;
    }

    public void setCreationDate(long creationDate) {
        this.mCreationDate = creationDate;
    }

    public long getCompletionDate() {
        return mCompletionDate;
    }

    public void setCompletionDate(long completionDate) {
        this.mCompletionDate = completionDate;
    }

    public long getExpireDate() {
        return mExpireDate;
    }

    public void setExpireDate(long expireDate) {
        this.mExpireDate = expireDate;
    }

    public long getStartDate() {
        return mStartDate;
    }

    public void setStartDate(long date) {
        this.mStartDate = date;
    }

    public long getEndDate() {
        return mEndDate;
    }

    public void setEndDate(long date) {
        this.mEndDate = date;
    }

    public String getThirdPerson() {
        return mThirdPerson;
    }

    public void setThirdPerson(String thirdPerson) {
        this.mThirdPerson = thirdPerson;
    }

    public Map<String, Object> getCustomFields() {
        return mCustomFields;
    }

    public void setCustomFields(Map<String, Object> custom_fields) {
        this.mCustomFields = custom_fields;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        this.mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double location) {
        this.mLongitude = location;
    }

    public String getUserOwner() {
        return mUserOwner;
    }

    public void setUserOwner(String userId) {
        this.mUserOwner = userId;
    }

    public String getModel() {
        return mModel;
    }

    public void setModel(String model) {
        this.mModel = model;
    }

    public String getOrderId() {
        return mId;
    }

    public void setOrderId(String orderId) {
        this.mId = orderId;
    }

    public double getAmount() {
        return mAmount;
    }

    public void setAmount(double amount) {
        this.mAmount = amount;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String currency) {
        this.mCurrency = currency;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        this.mStatus = status;
    }

    public boolean isStatus(String... statuses) {
        if (statuses == null || statuses.length == 0)
            return mStatus == null;

        if (mStatus == null)
            return false;

        for (String status: statuses)
            if (mStatus.equals(status))
                return true;

        return false;
    }

    /**
     * Gets Shop ID that is contain this transaction.
     *
     * @return Shop ID that is contain this transaction.
     */
    public String getShopId() {
        return mShopId;
    }

    /**
     * Sets Shop ID that is contain this transaction.
     *
     * @param id Shop ID that is contain this transaction.
     */
    public void setShopId(String id) {
        this.mShopId = id;
    }

    /**
     * Gets Shop Name that is contain this transaction.
     *
     * @return Shop Name that is contain this transaction.
     */
    public String getShopName() {
        return mShopName;
    }

    /**
     * Sets Shop Name that is contain this transaction.
     *
     * @param name Shop Name that is contain this transaction.
     */
    public void setShopName(String name) {
        this.mShopName = name;
    }

    /**
     * Gets Shop receipt for the Transaction.
     *
     * @return Shop receipt for the Transaction.
     */
    public String getShopReceipt() {
        return mShopReceipt;
    }

    /**
     * Sets Shop receipt for the Transaction.
     *
     * @param receipt Shop receipt for the Transaction.
     */
    public void setShopReceipt(String receipt) {
        this.mShopReceipt = receipt;
    }

    /**
     * Gets Reference string that was set in the payment request.
     *
     * @return Reference string that was set in the payment request.
     */
    public String getShopReference() {
        return mShopReference;
    }

    /**
     * Sets Reference string that was set in the payment request.
     *
     * @param reference Reference string that was set in the payment request.
     */
    public void setShopReference(String reference) {
        this.mShopReference = reference;
    }

    public List<BookedProduct> getProducts() {
        return mProducts;
    }

    public void setProducts(List<BookedProduct> products) {
        this.mProducts = products;

        if (products != null) {
            mAmount = 0;
            mCurrency = null;
            for (BookedProduct bookedProduct: products) {

                mAmount += bookedProduct.getPrice();
                if (mCurrency == null)
                    mCurrency = bookedProduct.getProduct().getCurrency();
            }
        }
    }

    public UserAccount getAccount() {
        return mAccount;
    }

    public void setAccount(UserAccount account) {
        this.mAccount = account;
    }

    public boolean isAccountValid() {
        if (mAccount != null && mProducts != null && mProducts.size() > 0) {
            Set<String> disallowedAccounts = new HashSet<>();
            for (BookedProduct bookedProduct: mProducts)
                disallowedAccounts.addAll(bookedProduct.getProduct().getDisallowedAccounts());

            return !disallowedAccounts.contains(mAccount.getType());
        }
        return mAccount != null && mAccount.enoughLeft(mAmount);
    }

    public List<UserRoleDescription> getLimitedRoles() {
        return mLimitedRoles;
    }

    public void setLimitedRoles(List<UserRoleDescription> limitedRoles) {
        this.mLimitedRoles = limitedRoles;
    }

    public int getUnreadChatMessagesCount() {
        return mUnreadChatMessagesCount;
    }

    public void setUnreadChatMessagesCount(int count) {
        this.mUnreadChatMessagesCount = count;
    }

    public String getAsset() {
        return mAsset;
    }

    public void setAsset(String asset) {
        this.mAsset = asset;
    }

    public String getParentOrderId() {
        return mParentOrderId;
    }

    public void setParentOrderId(String parentOrderId) {
        mParentOrderId = parentOrderId;
    }
}
