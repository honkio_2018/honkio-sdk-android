package com.riskpointer.sdk.api.model.entity;

import java.util.List;

/**
 * Created by I.N. on 2017-02-16.
 */

public class RateList {

    private List<Rate> mList;

    public List<Rate> getList() {
        return mList;
    }

    public void setList(List<Rate> list) {
        mList = list;
    }

    public int getAverageRate() {
        if (mList.size() > 0) {
            int sumCount = 0;
            int sumValue = 0;
            for (Rate rate : mList) {
                sumValue = sumValue + rate.value * rate.count;
                sumCount = sumCount + rate.count;
            }
            return Math.round(sumValue / sumCount);
        } else {
            return 0;
        }
    }
}
