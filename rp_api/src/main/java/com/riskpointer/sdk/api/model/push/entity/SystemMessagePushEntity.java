package com.riskpointer.sdk.api.model.push.entity;

import com.riskpointer.sdk.api.model.push.Push;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Shurygin Denis
 */
public class SystemMessagePushEntity extends Push.PushEntity implements Serializable {

    public SystemMessagePushEntity() {
        super(Push.Type.SYSTEM);
    }

    public SystemMessagePushEntity(Map<String, Object> content) {
        super(Push.Type.SYSTEM, content);
    }

    public String getMessage() {
        return (String) getContent("message");
    }

    public void setMessage(String value) {
        setContent("message", value);
    }

    public String getSubject() {
        return (String) getContent("subject");
    }

    public void setSubject(String value) {
        setContent("subject", value);
    }
}
