/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.callback;

import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.response.Response;

/**
 * Callback for receiving server responses.
 * 
 * @author Denis Shurygin
 * 
 * @param <Result> Type of the response.
 */
public interface RequestCallback<Result> extends ErrorCallback {
    /**
     * This method will called when the server return result and it's not error.
     * @param response Object that represent the server response.
     * @return True if response was handled, false otherwise.
     * All unhandled responses will invoke Broadcast message {@link BroadcastHelper#BROADCAST_ACTION_UNHANDLED_RESPONSE}.
     */
    public boolean onComplete(Response<Result> response);
}
