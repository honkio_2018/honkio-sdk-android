package com.riskpointer.sdk.api.model.entity;

public class HkObjectType {
    private HkObjectType() {
    }
    public static final String ASSET = "asset";
    public static final String SHOP = "shop";
    public static final String USER_ROLE = "user_role";
}
