package com.riskpointer.sdk.api.model.push.entity;

import com.riskpointer.sdk.api.model.push.Push;

// TODO docs. Rename!!!
/**
 * Created by Shurygin Denis on 2016-01-17.
 */
public class WorkerApplyPushEntity extends Push.PushEntity {
    public static final String TYPE = "workerapply";

    private String mOrderId;
    private String mOrderTitle;
    private String mUserId;
    private String mUserName;
    private String mProposalTime;

    public WorkerApplyPushEntity() {
        super(TYPE);
    }

    public String getOrderId() {
        return mOrderId;
    }

    public void setOrderId(String orderId) {
        this.mOrderId = orderId;
    }

    public String getOrderTitle() {
        return mOrderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.mOrderTitle = orderTitle;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        this.mUserId = userId;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        this.mUserName = userName;
    }

    public String getProposalTime() {
        return mProposalTime;
    }

    public void setProposalTime(String proposalTime) {
        this.mProposalTime = proposalTime;
    }
}