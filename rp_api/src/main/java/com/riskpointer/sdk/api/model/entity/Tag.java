package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;

/**
 * Created by asenko on 12/30/2015.
 * TODO DOCS
 */
public class Tag implements Serializable {

    private String mId;
    private String mName;
    private String mDescription;
    private String mCurrency;
    private float mPrice;
    private String mProductId;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String currency) {
        this.mCurrency = currency;
    }

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float price) {
        this.mPrice = price;
    }

    public String getProductId() {
        return mProductId;
    }

    public void setProductId(String productId) {
        this.mProductId = productId;
    }
}
