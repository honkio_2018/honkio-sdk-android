package com.riskpointer.sdk.api.web.message.filter;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by asenko on 1/13/2016.
 */
public class UserCommentsFilter extends ListFilter implements Serializable {

    public static final String QUERY_ORDER_ID = "order_id";
    public static final String QUERY_USER_ID = "user_id";
    public static final String QUERY_ROLE_ID = "role_id";
    public static final String QUERY_OBJECT = "object";
    public static final String QUERY_OBJECT_ID = "object_id";
    public static final String QUERY_OBJECT_TYPE = "object_type";

    private String mUserId;
    private String mOrderId;
    private String mRoleId;
    private String mObject;
    private String mObjectId;
    private String mObjectType;


    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        this.mUserId = userId;
    }

    public String getOrderId() {
        return mOrderId;
    }

    public void setOrderId(String orderId) {
        this.mOrderId = orderId;
    }

    public String getRoleId() {
        return mRoleId;
    }

    public void setRoleId(String roleId) {
        this.mRoleId = roleId;
    }

    public String getObject() {
        return mObject;
    }

    public void setObject(String object) {
        mObject = object;
    }

    public String getObjectId() {
        return mObjectId;
    }

    public void setObjectId(String objectId) {
        mObjectId = objectId;
    }

    public String getObjectType() {
        return mObjectType;
    }

    public void setObjectType(String objectType) {
        mObjectType = objectType;
    }

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = super.toParams();

        applyIfNotNull(params, QUERY_ORDER_ID, getOrderId());
        applyIfNotNull(params, QUERY_USER_ID, getUserId());
        applyIfNotNull(params, QUERY_ROLE_ID, getRoleId());
        applyIfNotNull(params, QUERY_OBJECT, getObject());
        applyIfNotNull(params, QUERY_OBJECT_ID, getObjectId());
        applyIfNotNull(params, QUERY_OBJECT_TYPE, getObjectType());

        return params;
    }
}