package com.riskpointer.sdk.api.model.entity;

import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.AbsFilter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created I.N. on 17.01.2019
 */
public class Poll implements Serializable {
    private String mId;
    private boolean mIsActive;
    private long mCreationDate;
    private long mExpireDate;
    private String mMerchantId;
    private HashMap<String, PollOptionsNamesList> mOptions;
    private HashMap<String, Object> mProperties;
    private String mText;
    private String mTitle;
    private boolean mIsVisible;
    private PollAnswersList mAnswersList;
    private String mMyAnswer; //id of selected option

    public Poll() {
    }

    public Poll(Poll poll) {
        this.mId = poll.mId;
        this.mIsActive = poll.mIsActive;
        this.mCreationDate = poll.mCreationDate;
        this.mExpireDate = poll.mExpireDate;
        this.mMerchantId = poll.mMerchantId;
        this.mText = poll.mText;
        this.mTitle = poll.mTitle;
        this.mIsVisible = poll.mIsVisible;

        if (poll.mProperties != null)
            this.mProperties = new HashMap<>(poll.mProperties);
        if (poll.mOptions != null)
            this.mOptions = new HashMap<>(poll.mOptions);
    }

    public void applyTo(Message<?> message) {
        message.addParameters(toParams());
    }

    public Map<String, Object> toParams() {
        Map<String, Object> map = new HashMap<>();

        AbsFilter.applyIfNotNull(map, "active", mIsActive);
        if (mCreationDate > 0)
            AbsFilter.applyIfNotNull(map, "creation_date", ApiFormatUtils.dateToServerFormat(mCreationDate));
        if (mExpireDate > 0)
            AbsFilter.applyIfNotNull(map, "expire_date", ApiFormatUtils.dateToServerFormat(mExpireDate));
        AbsFilter.applyIfNotNull(map, "id", mId);
        AbsFilter.applyIfNotNull(map, "merchant", mMerchantId);
        AbsFilter.applyIfNotNull(map, "text", mText);
        AbsFilter.applyIfNotNull(map, "title", mTitle);
        AbsFilter.applyIfNotNull(map, "visible", mIsVisible);
        if (mOptions != null && mOptions.size() > 0) {
            Map<String, Object> mapOptions = new HashMap<>();
            for (Map.Entry<String, PollOptionsNamesList> entryOptions : mOptions.entrySet()) {
                String keyOption = entryOptions.getKey();//yes, no
                Object mapNames = entryOptions.getValue().getNamesList();//name, name_sv
                mapOptions.put(keyOption, mapNames);
            }
            map.put("options", mapOptions);
        }
        if (mProperties != null && mProperties.size() > 0)
            map.put("properties", mProperties);
        return map;
    }

    public void removeProperty(String name) {
        if (mProperties != null && mProperties.containsKey(name)) {
            mProperties.remove(name);
        }
    }

    public void applyProperty(String name, Object value) {
        if (mProperties == null)
            mProperties = new HashMap<>();

        mProperties.put(name, value);
    }

    public String getId() {
        return mId;
    }

    public void setId(String userRoleId) {
        this.mId = userRoleId;
    }

    public Map<String, Object> getProperties() {
        return mProperties;
    }

    public String getMerchantId() {
        return mMerchantId;
    }

    public void setMerchantId(String merchantId) {
        this.mMerchantId = merchantId;
    }

    public boolean isActive() {
        return mIsActive;
    }

    public void setActive(boolean active) {
        mIsActive = active;
    }

    public long getCreationDate() {
        return mCreationDate;
    }

    public void setCreationDate(long creationDate) {
        mCreationDate = creationDate;
    }

    public long getExpireDate() {
        return mExpireDate;
    }

    public void setExpireDate(long expireDate) {
        mExpireDate = expireDate;
    }

    public Map<String, PollOptionsNamesList> getOptions() {
        return mOptions;
    }

    public void setOptions(HashMap<String, PollOptionsNamesList> options) {
        mOptions = options;
    }

    public void setProperties(HashMap<String, Object> properties) {
        mProperties = properties;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public boolean isVisible() {
        return mIsVisible;
    }

    public void setVisible(boolean visible) {
        mIsVisible = visible;
    }

    public PollAnswersList getAnswersList() {
        return mAnswersList;
    }

    public void setAnswersList(PollAnswersList answersList) {
        mAnswersList = answersList;
    }

    public String getMyAnswer() {
        return mMyAnswer;
    }

    public void setMyAnswer(String answer) {
        mMyAnswer = answer;
    }
}
