package com.riskpointer.sdk.api.model.entity.merchant;

import java.util.List;

/**
 * Created by Shurygin Denis on 2016-11-24.
 */

public class MerchantShopsList {

    private List<MerchantShop> mList;

    public List<MerchantShop> getList() {
        return mList;
    }

    public void setList(List<MerchantShop> list) {
        mList = list;
    }
}
