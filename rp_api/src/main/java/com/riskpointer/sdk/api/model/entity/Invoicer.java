package com.riskpointer.sdk.api.model.entity;

import android.text.TextUtils;

import java.io.Serializable;

/**
 * Data object that hold information about invoice initiator.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 *
 * @author Shurygin Denis on 2015-03-27.
 */
@Deprecated // Not tested
public class Invoicer implements Serializable{

    private String mId;
    private String mName;
    private String mNote;
    private String mMore;
    private String mRef;
    private String mBic;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        this.mNote = note;
    }

    public String getMore() {
        return mMore;
    }

    public void setMore(String more) {
        this.mMore = more;
    }

    public String getRef() {
        return mRef;
    }

    public void setRef(String ref) {
        this.mRef = ref;
    }

    public String getBic() {
        return mBic;
    }

    public void setBic(String bic) {
        this.mBic = bic;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (getClass() != obj.getClass())
            return false;

        Invoicer invoicer = (Invoicer) obj;
        return TextUtils.equals(mId, invoicer.mId) &&
                TextUtils.equals(mName, invoicer.mName) &&
                TextUtils.equals(mNote, invoicer.mNote) &&
                TextUtils.equals(mMore, invoicer.mMore) &&
                TextUtils.equals(mRef, invoicer.mRef) &&
                TextUtils.equals(mBic, invoicer.mBic);
    }
}
