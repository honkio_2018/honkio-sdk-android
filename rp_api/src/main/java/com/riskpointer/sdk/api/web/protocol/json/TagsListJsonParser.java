/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Tag;
import com.riskpointer.sdk.api.model.entity.TagsList;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Parses tags List from JSON.
 * TODO: write docs
 *
 * :             "product_id": "569d395aedc2ba6f6592b276",
 :             "description": "Child-Care",
 :             "tag_id": "568ba50384081331ff5ad8e9",
 :             "currency": "EUR",
 :             "amount": 15000,
 :             "name": "childcare"
 :         }
 ],
 */

public class TagsListJsonParser extends BaseJsonResponseParser<TagsList> {

    private float tryParseFloat (String f) {
        if ((f != null)&&(f.length()>0)) {
            try {
                return Float.parseFloat(f);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            return 0.0f;
        }else{
            return 0.0f;
        }
    }

    @Override
    public TagsList parseJson(Message<?> message, JSONObject json) {

        TagsList tagsList = new TagsList();

        HashMap<String, Tag> map = new HashMap<>();

        JSONArray tagsListJson = json.optJSONArray("tags");
        if (tagsListJson != null) {
            for (int i = 0; i < tagsListJson.length(); i++) {
                JSONObject tagJson = tagsListJson.optJSONObject(i);
                if (tagJson != null) {
                    Tag tag = new Tag();
                    tag.setName(tagJson.optString("name"));
                    tag.setId(tagJson.optString("tag_id"));
                    tag.setDescription(tagJson.optString("description"));
                    tag.setCurrency(tagJson.optString("currency"));
                    tag.setPrice(tryParseFloat(tagJson.optString("amount")));
                    tag.setProductId(tagJson.optString("product_id"));

                    map.put(tag.getId(), tag);
                }
            }
        }

        tagsList.setList(map);
        return tagsList;
    }

}
