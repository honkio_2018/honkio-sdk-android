package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Asset;
import com.riskpointer.sdk.api.model.entity.HkQrCode;
import com.riskpointer.sdk.api.model.entity.ShopInfo;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * @author Shurygin Denis
 */

public class QrCodeJsonParser extends BaseJsonResponseParser<HkQrCode> {

    @Override
    protected HkQrCode parseJson(Message<?> message, JSONObject json) {

        HkQrCode qrCode = new HkQrCode();

        if (json != null) {
            qrCode.setId(json.optString("id"));
            qrCode.setMerchantId(json.optString("merchant"));
            qrCode.setUrlBase(json.optString("url_base"));
            qrCode.setUrlParams(json.optString("url_params"));
            qrCode.setApplications(parseApplications(json.optJSONArray("applications")));
            qrCode.setObjectType(json.optString("object_type"));

            if ("null".equals(qrCode.getObjectType()))
                qrCode.setObjectType(null);

            if (qrCode.getObjectType() != null) {
                switch (qrCode.getObjectType()) {
                    case HkQrCode.Type.ASSET:
                        Asset asset = AssetJsonParser.parseAsset(json.optJSONObject("object"));
                        if (asset != null)
                            qrCode.setObject(asset);
                        break;
                    case HkQrCode.Type.SHOP:
                        ShopInfo shopInfo = new ShopInfo();
                        if (ShopInfoJsonParser.parseShopInfo(json.optJSONObject("object"), shopInfo))
                            qrCode.setObject(shopInfo);
                        break;
                    default:
                        // Do nothing
                        break;
                }
            }
        }

        return qrCode;
    }

    private ArrayList<HkQrCode.Application> parseApplications(JSONArray jsonArray) {
        ArrayList<HkQrCode.Application> list = new ArrayList<>();

        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.length(); i++) {
                HkQrCode.Application application = parseApplication(jsonArray.optJSONObject(i));
                if (application != null)
                    list.add(application);
            }
        }

        return list;
    }

    private HkQrCode.Application parseApplication(JSONObject json) {
        if (json != null) {
            HkQrCode.Application application = new HkQrCode.Application();

            return application;
        }
        return null;
    }
}
