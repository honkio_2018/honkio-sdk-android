/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.process.user;

import android.os.Bundle;
import android.os.Handler;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.UserPayment;
import com.riskpointer.sdk.api.model.task.RequestTask;
import com.riskpointer.sdk.api.model.task.TaskStorage;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.callback.RequestCallbackWrapper;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;

/**
 * Helper class that allow to schedule transaction status check.
 */
public class QueryPaymentProcessModel {
	private static final String SAVED_SHOP = "QueryPaymentProcessModel.SAVED_SHOP";
	private static final String SAVED_TRANSACTION_ID = "QueryPaymentProcessModel.SAVED_TRANSACTION_ID";
	private static final String SAVED_LOOP_DELAY = "QueryPaymentProcessModel.SAVED_LOOP_DELAY";
	private static final String SAVED_TASK = "QueryPaymentProcessModel.SAVED_TASK";

	private Handler mHandler;
	private Identity mShop;
	private String mPendingTransactionId;
	
	private long mLoopDelay;
	private Runnable mQueryRunnable;
	private CallbackWrapper mQueryCallback;

	private RequestTask<UserPayment> mTask;

	/**
	 * Construct new Query Model.
	 * @param callback Payment callback.
	 * @param shopIdentity Identity of the Shop which will process payment.
	 * @param transactionId Id of the payment transaction that should be queried.
	 */
	public QueryPaymentProcessModel(RequestCallback<UserPayment> callback, Identity shopIdentity, String transactionId) {
		mHandler = new Handler();
		mQueryCallback = new CallbackWrapper(callback);
		mShop = shopIdentity;
		mPendingTransactionId = transactionId;
	}

	/**
	 * Makes query for pending payment transaction.
	 */
	public void query() {
		mLoopDelay = 0;
		if(mPendingTransactionId != null)
			mTask = HonkioApi.queryPaymentRequest(mPendingTransactionId, getShopIdentity(), 0,
					mQueryCallback);
	}

	/**
	 * Starts loop query for pending payment transaction with specified delay.
	 * @param loopDelay Delay for the next query.
	 */
	public void query(long loopDelay) {
		stopQuery();
		mLoopDelay = loopDelay;
		mQueryRunnable = new Runnable() {
			@Override
			public void run() {
				if(mPendingTransactionId != null)
					mTask = HonkioApi.queryPaymentRequest(mPendingTransactionId, getShopIdentity(),
							0, mQueryCallback);
			}
		};
		mQueryRunnable.run();
	}

	/**
	 * Stops loop query.
	 */
	public void stopQuery() {
		if (mQueryRunnable != null)
			mHandler.removeCallbacks(mQueryRunnable);
	}

	/**
	 * Aborts query and sets payment transaction status to "reject".
	 * @return True if transaction aborted immediately, false otherwise.
	 */
	public boolean abort() {
		mHandler.removeCallbacks(mQueryRunnable);
		if(mPendingTransactionId != null) {
            HonkioApi.transactionSetStatus(Response.Status.REJECT, mPendingTransactionId,
					getShopIdentity(), 0, new RequestCallback<Void>() {

				@Override
				public boolean onError(RpError error) {
					query();
					return true;
				}

				@Override
				public boolean onComplete(Response<Void> response) {
					boolean result = true;
					if (!response.isStatusReject()) {
						result = mQueryCallback.onError(response.getAnyError());
					}
					query();
					return result;
				}
			});
            return false;
        }
        return true;
	}

	/**
	 * Gets Transaction Id.
	 * @return Transaction Id.
	 */
	public String getTransactionId() {
		return mPendingTransactionId;
	}

	/**
	 * Gets Identity of the Shop which will process payment.
	 * @return Identity of the Shop which will process payment.
	 */
	public Identity getShopIdentity() {
		if(mShop != null)
			return mShop;
		return HonkioApi.getMainShopInfo().getShop();
	}

	/**
	 * Sets payment callback.
	 * @param callback Payment callback.
	 */
	public void setCallback(RequestCallback<UserPayment> callback) {
		mQueryCallback.set(callback);
		if (mTask != null)
			mTask.setCallback(mQueryCallback, true);
	}

	public Bundle saveInstanceState() {
		Bundle savedState = new Bundle();
		savedState.putSerializable(SAVED_SHOP, mShop);
		savedState.putString(SAVED_TRANSACTION_ID, mPendingTransactionId);
		savedState.putLong(SAVED_LOOP_DELAY, mLoopDelay);
		savedState.putInt(SAVED_TASK, TaskStorage.pushIfNotCompleted(mTask));
		return savedState;
	}

	public static QueryPaymentProcessModel restoreFromSavedState(RequestCallback<UserPayment> callback, Bundle instanceState) {
		Identity shop = (Identity) instanceState.getSerializable(SAVED_SHOP);
		String transactionId = instanceState.getString(SAVED_TRANSACTION_ID);
		QueryPaymentProcessModel model = new QueryPaymentProcessModel(callback, shop, transactionId);

		long loopDelay = instanceState.getLong(SAVED_LOOP_DELAY);
		int taskId = instanceState.getInt(SAVED_TASK);

		if (taskId != 0)
			model.mTask = (RequestTask<UserPayment>) TaskStorage.pull(taskId);

		if (loopDelay > 0 && model.mTask == null)
			model.query(loopDelay);
		else {
			model.mLoopDelay = loopDelay;
			model.mTask.setCallback(model.mQueryCallback, true);
		}

		return model;
	}

	private class CallbackWrapper extends RequestCallbackWrapper<UserPayment> {

		/**
		 * Construct new Wrapper.
		 *
		 * @param callback Callback that will be wrapped.
		 */
		public CallbackWrapper(RequestCallback<UserPayment> callback) {
			super(callback);
		}

		@Override
		public boolean onComplete(Response<UserPayment> response) {
			if(response.isStatusPending()) {
				if (mLoopDelay > 0)
					mHandler.postDelayed(mQueryRunnable, mLoopDelay);
				return true;
			}
			else  {
				mLoopDelay = 0;
				if (response.isStatusAccept() || response.isStatusReject()) {
					return super.onComplete(response);
				}
				else {
					return onError(response.getAnyError());
				}
			}
		}
	}
	
}