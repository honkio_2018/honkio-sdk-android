package com.riskpointer.sdk.api.web.protocol.json.merchant;

import com.riskpointer.sdk.api.model.entity.SimpleListItem;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.protocol.json.SimpleListJsonParser;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Parse Merchant Transactions list from JSON.
 */
public class MerchantTransactionsListJsonParser extends SimpleListJsonParser {

    @Override
    public ArrayList<SimpleListItem> parseJson(Message<?> message, JSONObject json) {
        return parseList(json.optJSONArray("transaction"));
    }

}
