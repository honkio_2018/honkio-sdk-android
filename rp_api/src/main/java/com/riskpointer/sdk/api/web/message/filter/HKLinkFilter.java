package com.riskpointer.sdk.api.web.message.filter;

import java.util.HashMap;

public class HKLinkFilter extends ListFilter {

    private String type;

    private String fromObject;
    private String fromType;

    private String toObject;
    private String toType;

    private SortFilter mSortFilter;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setFrom(String object, String type) {
        this.fromObject = object;
        this.fromType = type;
    }

    public String getFromObject() {
        return fromObject;
    }

    public String getFromType() {
        return fromType;
    }

    public void setTo(String object, String type) {
        this.toObject = object;
        this.toType = type;
    }

    public String getToObject() {
        return toObject;
    }

    public String getToType() {
        return toType;
    }

    public SortFilter getSortFilter() {
        return mSortFilter;
    }

    public void setSortFilter(SortFilter filter) {
        this.mSortFilter = filter;
    }

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = super.toParams();

        applyIfNotNull(params, "query_link_type",type);
        applyIfNotNull(params, "query_from",fromObject);
        applyIfNotNull(params, "query_from_type",fromType);
        applyIfNotNull(params, "query_to",toObject);
        applyIfNotNull(params, "query_to_type",toType);

        if (mSortFilter != null)
            AbsFilter.applyIfNotNull(params, "query_sort", mSortFilter.toParams());

        return params;
    }
}
