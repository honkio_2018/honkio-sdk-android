/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.response;

import android.text.TextUtils;

import com.riskpointer.sdk.api.web.error.RpError;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Response of the API request.
 * @param <T> Type of the result object.
 * @author Denis Shurygin
 */
public final class Response<T> extends NoSerializableResponse<T> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * Available statuses of the response.
	 */
	public static final class Status {
		private Status() {}
		public static final String ACCEPT = "accept";
		public static final String ERROR = "error";
		public static final String PENDING = "pending";
		public static final String REJECT = "reject";
	}
	
	/**
	 * Available pending statuses.
	 */
	public static final class Pending {
		private Pending() {}
		/** Pending for the registration when the user don't confirm his email. */
		public static final String EMAIL_VERIFY = "emailverify";
		/** Pending for the registration when the user don't confirm his phone via SMS code. */
		public static final String SMS_VERIFY = "smsverify";
        /** Pending for credit card creation request.*/
		public static final String CREDITCARD_VERIFY = "creditcardverify";
        /** Pending when transaction should be verified by someone on the server.*/
		public static final String MANUAL_VERIFY = "manualverify";
		// TODO write docs
		public static final String OPERATOR_VERIFY = "operatorverify";
	}

	private T mResult;
	private String mStatus;
	private String mId;
	private String mDescription;
	private String mOtp;
	private String mShopId;
	private String mUrl;
	private List<String> mPendingsList;
	private boolean isOffline = false;
    private String mShopReference;
	private RpError mError;
	private RpError mAnyError;
	private String mCommand;

	/**
	 * Construct new response with specified result.
	 * @param result Result object.
	 */
    public Response(T result) {
		mResult = result;
    }

    /**
     * Construct new response based on template response object with specified result.
     * 
     * @param template Template object that will be copied.
	 * @param result Result object.
     */
    public Response(Response<?> template, T result) {
        this(result);
        mStatus = template.getStatus();
        mId = template.getId();
        mDescription = template.getDescription();
        mOtp = template.getOtp();
        mShopId = template.getShopId();
        mUrl = template.getUrl();
        if (template.getPendings() != null)
            mPendingsList = new ArrayList<>(template.getPendings());
        isOffline = template.isOffline();
		mShopReference = template.mShopReference;
		mError = template.mError;
		mCommand = template.mCommand;
    }

// ==========================================
// ========= Getters and Setters ============
// ==========================================
	
	/**
     * Gets object of the request.
	 * @return Object of the request.
	 */
	public T getResult() {
		return mResult;
	}

	/**
     * Status of the transaction.
     * 
	 * @return Status of the transaction.
	 * @see Status
	 */
	public String getStatus() {
		return mStatus;
	}
	
	/**
     * Check if status of response is {@link Status#ACCEPT}.
	 * @return True if status of the response is {@link Status#ACCEPT}, false otherwise.
	 */
	public boolean isStatusAccept() {
		return Status.ACCEPT.equals(mStatus);
	}
	
	/**
     * Check if status of response is {@link Status#ERROR}.
     * @return True if status of the response is {@link Status#ERROR}, false otherwise.
	 */
	public boolean isStatusError() {
		return Status.ERROR.equals(mStatus);
	}
	
	/**
     * Check if status of response is {@link Status#PENDING}.
     * @return True if status of the response is {@link Status#PENDING}, false otherwise.
	 */
	public boolean isStatusPending() {
		return Status.PENDING.equals(mStatus);
	}
	
	/**
     * Check if status of response is {@link Status#REJECT}.
     * @return True if status of the response is {@link Status#REJECT}, false otherwise.
	 */
	public boolean isStatusReject() {
		return Status.REJECT.equals(mStatus);
	}

	/**
     * Gets Transaction ID of the response.
     * @return Transaction ID of the response.
	 */
	public String getId() {
		return mId;
	}

	/**
     * Gets Description of the response.
	 * @return Description for the response.
	 */
	public String getDescription() {
		return mDescription;
	}

	/**
     * Gets New OTP(One time password) for the next request.
	 * @return New OTP(One time password) for the next request.
	 */
	public String getOtp() {
		return mOtp;
	}

	/**
     * Check if request was created in offline mode from the cache.
	 * @return True if this response was created in offline mode from the cache, false otherwise.
	 */
	public boolean isOffline() {
		return isOffline;
	}

	/**
	 * Gets URL provided from the response.
	 * @return URL string or null if the response doesn't contains URL.
	 */
	public String getUrl() {
		return mUrl;
	}

	/**
	 * Check that response contains specified pending.
	 * @param pending Pending for searching.
	 * @return True if the response has specified pending, false otherwise.
	 * @see Response.Pending
	 */
	public boolean hasPending(String pending) {
		if(mPendingsList != null && pending != null)
			return mPendingsList.contains(pending);
		return false;
	}

	/**
     * Gets Pending's list of the response.
	 * @return Pending's list of the response.
	 * @see Response.Pending
	 */
	public List<String> getPendings() {
		return mPendingsList;
	}

	/**
     * Gets Shop ID which return this response.
	 * @return Shop ID that return this response.
	 */
	public String getShopId() {
		return mShopId;
	}

	/**
	 * Gets Shop reference from request.
	 * @return Shop reference from request.
	 */
    public String getShopReference() {
        return mShopReference;
    }

	/**
	 * Gets error of the response.
	 * @return Error of the response or null if no error.
	 */
	public RpError getError() {
		return mError;
	}

	/**
	 * Gets error of the response or unknown error if response doesn't has a error.
	 * @return Error of the response or unknown error if response doesn't has a error.
	 */
	public RpError getAnyError() {
		if (mAnyError == null) {
			if (mError != null)
				mAnyError = mError;
			else
				mAnyError = new RpError(RpError.Group.API, RpError.subItemFromCode(RpError.Api.UNKNOWN), getMessage());
		}
		return mAnyError;
	}

	// TODO docs
	public String getCommand() {
		return mCommand;
	}

	/**
	 * Editor class to edit response.
	 */
	public static class Editor {

		/**
		 * Sets the status for response.
		 * @param response Response which will be edited.
		 * @param status The status.
		 * @see Status
		 */
		public static void setStatus(Response response, String status) {
			response.mStatus = status != null ? status.intern() : null;
		}

		/**
		 * Sets transaction ID of the response.
		 * @param response Response which will be edited.
		 * @param id Transaction ID.
		 */
		public static void setId(Response response, String id) {
			response.mId = id;
		}

		/**
		 * Sets description of the response.
		 * @param response Response which will be edited.
		 * @param description Description of the response.
		 */
		public static void setDescription(Response response, String description) {
			response.mDescription = description;
		}

		/**
		 * Sets OTP of response.
		 * @param response Response which will be edited.
		 * @param otp One time password.
		 */
		public static void setOtp(Response response, String otp) {
			response.mOtp = otp;
		}

		/**
		 * Sets that this response was created in the offline mode from the cache.
		 * @param response Response which will be edited.
		 * @param isOffline True if Offline mode used, false otherwise.
		 */
		public static void setOffline(Response response, boolean isOffline) {
			response.isOffline = isOffline;
		}

		/**
		 * Sets the URL provided from response.
		 * @param response Response which will be edited.
		 * @param url URL or null.
		 */
		public static void setUrl(Response response, String url) {
			response.mUrl = url;
		}

		/**
		 * Sets pending's for response.
		 * @param response Response which will be edited.
		 * @param pendings List of the pending's.
		 * @see Response.Pending
		 */
		public static void setPendings(Response response, List<String> pendings) {
			response.mPendingsList = pendings;
		}

		/**
		 * Sets Shop Id of response.
		 * @param response Response which will be edited.
		 * @param shopId Shop ID.
		 */
		public static void setShopId(Response response, String shopId) {
			response.mShopId = shopId;
		}

		/**
		 * Sets Shop reference of response
		 * @param response Response which will be edited.
		 * @param shopReference Shop reference.
		 */
		public static void setShopReference(Response response, String shopReference) {
			response.mShopReference = shopReference;
		}

		/**
		 * Sets error of the response.
		 * @param response Response which will be edited.
		 * @param error Error of the response or null if no error.
		 */
		public static void setError(Response response, RpError error) {
			response.mError = error;
			response.mAnyError = null;
		}

		public static void setCommand(Response response, String command) {
			response.mCommand = command;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (obj == null)
			return false;

		if (getClass() != obj.getClass())
			return false;

		Response response = (Response) obj;
		return TextUtils.equals(mStatus, response.mStatus) &&
				TextUtils.equals(mId, response.mId) &&
				TextUtils.equals(mDescription, response.mDescription) &&
				TextUtils.equals(mOtp, response.mOtp) &&
				TextUtils.equals(mShopId, response.mShopId) &&
				TextUtils.equals(mUrl, response.mUrl) &&
				TextUtils.equals(mShopReference, response.mShopReference) &&
				TextUtils.equals(mCommand, response.mCommand) &&
				isOffline == response.isOffline &&
				((mPendingsList != null && mPendingsList.equals(response.mPendingsList)) ||
						(mPendingsList == null && response.mPendingsList == null)) &&
				((mError != null && mError.equals(response.mError)) ||
					(mError == null && response.mError == null)) &&
				((mResult != null && mResult.equals(response.mResult)) ||
						(mResult == null && response.mResult == null));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("Response(");
		builder.append("status=").append(getStatus());
		builder.append("; id=").append(getId());
		if (mResult!= null)
			builder.append("; result_class=").append(mResult.getClass().getCanonicalName());
		if (mDescription != null)
			builder.append("; desc=\"").append(mDescription).append("\"");
		if (mShopId != null)
		builder.append("; shop=").append(mShopId);
		builder.append("; ").append(mOtp != null ? "has_otp" : "no_otp");
		if (mPendingsList != null && mPendingsList.size() > 0)
			builder.append("; pendings=").append(Arrays.toString(mPendingsList.toArray()));
		builder.append("; offline=").append(isOffline ? "true" : "false");
		if (getMessage() != null)
			builder.append("; command=").append(getMessage().getCommand());
		else
			builder.append("; message=null");
		builder.append(")");
		return builder.toString();
	}
}
