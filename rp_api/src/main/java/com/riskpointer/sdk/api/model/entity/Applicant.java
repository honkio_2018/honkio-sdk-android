package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;

/**
 * Created by asenko on 12/15/2015.
 * TODO write docs
 */
public class Applicant implements Serializable {

    private String mApplicationId;
    private String mWorkerId;
    private String mTimeProposal;
    private String mOrderId;
    private UserRole mRole;


    public String getApplicationId() {
        return mApplicationId;
    }

    public void setApplicationId(String applicationId) {
        this.mApplicationId = applicationId;
    }

    public String getUserId() {
        return mWorkerId;
    }

    public void setUserId(String workerId) {
        this.mWorkerId = workerId;
    }

    public String getTimeProposal() {
        return mTimeProposal;
    }

    public void setTimeProposal(String timeProposal) {
        this.mTimeProposal = timeProposal;
    }

    public String getOrderId() {
        return mOrderId;
    }

    public void setOrderId(String orderId) {
        this.mOrderId = orderId;
    }

    public UserRole getRole() {
        return mRole;
    }

    public void setRole(UserRole role) {
        this.mRole = role;
    }

}