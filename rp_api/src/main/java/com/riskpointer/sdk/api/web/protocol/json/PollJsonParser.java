package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Poll;
import com.riskpointer.sdk.api.model.entity.PollOptionsNamesList;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by I.N. on 2017-02-16.
 */

public class PollJsonParser extends BaseJsonResponseParser<Poll> {
    @Override
    public Poll parseJson(Message<?> message, JSONObject json) {
        return parsePoll(json);
    }

    public static Poll parsePoll(JSONObject json) {
        if (json != null) {
            Poll poll = new Poll();
            poll.setId(json.optString("id"));
            poll.setActive(json.optBoolean("active"));
            poll.setVisible(json.optBoolean("visible"));
            poll.setCreationDate(ApiFormatUtils.stringDateToLong(json.optString("creation_date")));
            poll.setExpireDate(ApiFormatUtils.stringDateToLong(json.optString("expire_date")));
            poll.setMerchantId(json.optString("merchant"));
            poll.setText(json.optString("text"));
            poll.setTitle(json.optString("title"));

            if (json.has("options")) {
                HashMap<String, PollOptionsNamesList> results = new HashMap<>();
                JSONObject resultsJson = json.optJSONObject("options");
                if (resultsJson != null) {
                    Iterator<String> keysResults = resultsJson.keys();
                    while (keysResults.hasNext()) {
                        String pollId = keysResults.next();//poll id
                        JSONObject answersJson = resultsJson.optJSONObject(pollId);
                        if (answersJson != null) {
                            PollOptionsNamesList answers = new PollOptionsNamesList();
                            Iterator<?> keysOptions = answersJson.keys();
                            while (keysOptions.hasNext()) {
                                String option = (String) keysOptions.next();
                                answers.addName(option, answersJson.optString(option));
                            }
                            results.put(pollId, answers);
                        }
                    }
                }
                poll.setOptions(results);
            }

            JSONObject properties = json.optJSONObject("properties");
            if (properties != null) {
                poll.setProperties((HashMap<String, Object>) jsonToMap(properties));
            }
            return poll;
        }
        return null;
    }
}
