package com.riskpointer.sdk.api.merchant.protocol.json;

import com.riskpointer.sdk.api.model.entity.merchant.MerchantShop;
import com.riskpointer.sdk.api.model.entity.merchant.MerchantShopsList;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.protocol.json.BaseJsonResponseParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Shurygin Denis on 2016-11-24.
 */

public class MerchantShopsListJsonParser extends BaseJsonResponseParser<MerchantShopsList> {

    @Override
    protected MerchantShopsList parseJson(Message<?> message, JSONObject json) {
        List<MerchantShop> list = new ArrayList<>();
        JSONArray jsonArray = json.optJSONArray("shops");
        if (jsonArray != null) {
            for (int i = 0; i< jsonArray.length(); i++) {
                JSONObject jsonShop = jsonArray.optJSONObject(i);
                if (jsonShop != null) {
                    MerchantShop merchantShop = new MerchantShop();
                    merchantShop.setId(jsonShop.optString("id"));
                    merchantShop.setPassword(jsonShop.optString("password"));

                    JSONObject settings = jsonShop.optJSONObject("settings");
                    if (settings != null) {
                        merchantShop.setSettings((HashMap<String, Object>) jsonToMap(settings));
                    }
                    list.add(merchantShop);
                }
            }
        }
        MerchantShopsList result = new MerchantShopsList();
        result.setList(list);
        return result;
    }
}
