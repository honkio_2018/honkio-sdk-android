package com.riskpointer.sdk.api.model.entity;

public class LaunchpadInfo {
    private int mEventsNumber;
    private int mRegisteredUsers;
    private int mInvitedUsers;

    public int getEventsNumber() {
        return mEventsNumber;
    }

    public void setEventsNumber(int eventsNumber) {
        mEventsNumber = eventsNumber;
    }

    public int getRegisteredUsers() {
        return mRegisteredUsers;
    }

    public void setRegisteredUsers(int registeredUsers) {
        mRegisteredUsers = registeredUsers;
    }

    public int getInvitedUsers() {
        return mInvitedUsers;
    }

    public void setInvitedUsers(int invitedUsers) {
        mInvitedUsers = invitedUsers;
    }
}
