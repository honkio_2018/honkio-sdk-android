/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.entity.merchant;

import com.riskpointer.sdk.api.model.entity.MerchantSimple;

import java.io.Serializable;

/**
 * Data object that holds information about shop and his merchant.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
public class MerchantShopInfo implements Serializable {

	private static final long serialVersionUID = -1332023438543275349L;
	private long mTimeStamp;
	private MerchantShop mShop;

	/**
	 * Gets Server time in millis when ShopInfo was sent.
	 * @return Server time in millis when ShopInfo was sent.
	 */
	public long getTimeStamp() {
		return mTimeStamp;
	}

	/**
	 * Sets Server time in millis when ShopInfo was sent.
	 * @param timeMillis Server time in millis when ShopInfo was sent.
	 */
	public void setTimeStamp(long timeMillis) {
		mTimeStamp = timeMillis;
	}

	public MerchantShop getMerchantShop() {
		return mShop;
	}

	public void setMerchantShop(MerchantShop shop) {
		this.mShop = shop;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (obj == null)
			return false;

		if (getClass() != obj.getClass())
			return false;

		MerchantShopInfo info = (MerchantShopInfo) obj;

		return mTimeStamp == info.mTimeStamp &&
				((mShop != null && mShop.equals(info.mShop)) ||
						(mShop == null && info.mShop == null)) ;
	}
}
