/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.process.user;

import android.content.Context;
import android.os.Bundle;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.BookedProduct;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.api.model.entity.UserPayment;
import com.riskpointer.sdk.api.model.process.BaseProcessModel;
import com.riskpointer.sdk.api.model.task.RequestTask;
import com.riskpointer.sdk.api.model.task.TaskStorage;
import com.riskpointer.sdk.api.web.callback.ErrorCallback;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.callback.SimplePendingCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.response.Response;

import java.util.List;

/**
 * Process model that implement logic of User payment account creation.
 */
public class AccountCreationProcessModel extends BaseProcessModel {

    public interface Callback extends ErrorCallback {

        /**
         * This method will called when process completes with success result.
         * @param account User account that did created.
         * @return True if response was handled, false otherwise.
         * All unhandled responses will invoke Broadcast message {@link
         * com.riskpointer.sdk.api.utils.BroadcastHelper#BROADCAST_ACTION_UNHANDLED_RESPONSE}.
         */
        boolean onComplete(UserAccount account);

        /**
         * This method will called when the server returned result with pending status.
         *
         * @param response Response object with pending status.
         *
         * @return true if response was handled, false otherwise.
         * All unhandled responses will invoke Broadcast message {@link
         * com.riskpointer.sdk.api.utils.BroadcastHelper#BROADCAST_ACTION_UNHANDLED_RESPONSE}.
         */
        boolean handlePending(Response<?> response);
    }

    private static final String SAVED_ACCOUNT_TYPE = "AccountCreationProcessModel.SAVED_ACCOUNT_TYPE";
    private static final String SAVED_ACCOUNT_NUMBER = "AccountCreationProcessModel.SAVED_ACCOUNT_NUMBER";
    private static final String SAVED_SHOP = "AccountCreationProcessModel.SAVED_SHOP";
    private static final String SAVED_FLAGS = "AccountCreationProcessModel.SAVED_FLAGS";
    private static final String SAVED_QUERY_ID = "AccountCreationProcessModel.SAVED_QUERY_ID";
    private static final String SAVED_PAY_CHECK_ENABLED = "AccountCreationProcessModel.SAVED_PAY_CHECK_ENABLED";
    private static final String SAVED_TASK_ADD_ACCOUNT = "UserLoginProcessModel.SAVED_TASK_ADD_ACCOUNT";
    private static final String SAVED_TASK_PAYMENT = "UserLoginProcessModel.SAVED_TASK_PAYMENT";
    private static final String SAVED_TASK_QUERY = "UserLoginProcessModel.SAVED_TASK_QUERY";
    private static final String SAVED_TASK_GET_USER = "UserLoginProcessModel.SAVED_TASK_GET_USER";

	private Callback mCallback;
    private UserAccount mAccount;
    private Shop mShop;
    private int mFlags;
	private String mQueryId;
    private boolean isPaymentCheckEnabled = true;

    private RequestTask<User> mTaskAddAccount;
    private RequestTask<UserPayment> mTaskPayment;
    private RequestTask<UserPayment> mTaskQuery;
    private RequestTask<User> mTaskGetUser;

    /**
     * Base constructor.
     * @param callback SimplePendingCallback implementation.
     * @param type Type of the account.
     * @param number Number of the account.
     */
	public AccountCreationProcessModel(Callback callback, String type, String number) {
		this(callback, new UserAccount(type, number));
	}

    /**
     * Base constructor.
     * @param callback SimplePendingCallback implementation.
     * @param account new UserAccount instance.
     */
    public AccountCreationProcessModel(Callback callback, UserAccount account) {
        this(callback, account, null);
    }

    /**
     * Base constructor.
     * @param callback SimplePendingCallback implementation.
     * @param account new UserAccount instance.
     * @param account shop that will be used in account creation.
     */
    public AccountCreationProcessModel(Callback callback, UserAccount account, Shop shop) {
        super();
        mCallback = callback;
        mAccount = account;
        mShop = shop;
    }

    @Override
    public Bundle saveInstanceState() {
        Bundle savedState = super.saveInstanceState();

        savedState.putString(SAVED_ACCOUNT_TYPE, mAccount.getType());
        savedState.putString(SAVED_ACCOUNT_NUMBER, mAccount.getNumber());
        savedState.putSerializable(SAVED_SHOP, mShop);
        savedState.putInt(SAVED_FLAGS, mFlags);
        savedState.putString(SAVED_QUERY_ID, mQueryId);
        savedState.putBoolean(SAVED_PAY_CHECK_ENABLED, isPaymentCheckEnabled);
        savedState.putInt(SAVED_TASK_ADD_ACCOUNT, TaskStorage.pushIfNotCompleted(mTaskAddAccount));
        savedState.putInt(SAVED_TASK_PAYMENT, TaskStorage.pushIfNotCompleted(mTaskPayment));
        savedState.putInt(SAVED_TASK_QUERY, TaskStorage.pushIfNotCompleted(mTaskQuery));
        savedState.putInt(SAVED_TASK_GET_USER, TaskStorage.pushIfNotCompleted(mTaskGetUser));

        return savedState;
    }

    @Override
    public void restoreInstanceState(Context context, Bundle instanceState) {
        super.restoreInstanceState(context, instanceState);

        if (instanceState != null) {
            mAccount.setType(instanceState.getString(SAVED_ACCOUNT_TYPE));
            mAccount.setNumber(instanceState.getString(SAVED_ACCOUNT_NUMBER));

            mShop = (Shop) instanceState.getSerializable(SAVED_SHOP);

            mFlags = instanceState.getInt(SAVED_FLAGS);
            mQueryId = instanceState.getString(SAVED_QUERY_ID);
            isPaymentCheckEnabled = instanceState.getBoolean(SAVED_PAY_CHECK_ENABLED, isPaymentCheckEnabled);

            mTaskAddAccount = (RequestTask<User>) TaskStorage.pull(instanceState.getInt(SAVED_TASK_ADD_ACCOUNT));
            if (mTaskAddAccount != null)
                mTaskAddAccount.setCallback(new AddEmptyAccount(), true);

            mTaskPayment = (RequestTask<UserPayment>) TaskStorage.pull(instanceState.getInt(SAVED_TASK_PAYMENT));
            if (mTaskPayment != null)
                mTaskPayment.setCallback(new PendingCallback(), true);

            mTaskQuery = (RequestTask<UserPayment>) TaskStorage.pull(instanceState.getInt(SAVED_TASK_QUERY));
            if (mTaskQuery != null)
                mTaskQuery.setCallback(new AcceptCheckCallback(), true);

            mTaskGetUser = (RequestTask<User>) TaskStorage.pull(instanceState.getInt(SAVED_TASK_GET_USER));
            if (mTaskGetUser != null)
                mTaskGetUser.setCallback(new GetUserCallback(), true);
        }
    }

    /**
     * Sets if account should be checked via payment request.
     * @param isEnabled True if payment request should be called, false otherwise.
     */
    public void setPaymentCheckEnabled(boolean isEnabled) {
        isPaymentCheckEnabled = isEnabled;
    }

    /**
     * Check if account should be checked via payment request.
     * @return True if payment request should be called, false otherwise. Default value is true.
     */
    public boolean isPaymentCheckEnabled() {
        return isPaymentCheckEnabled;
    }

    @Override
    public void start() {
        super.start();
        mTaskAddAccount = HonkioApi.userAddAccount(mAccount, mFlags, new AddEmptyAccount());
    }

    /**
     * Async check if transaction status changed from PENDING to another.<br>
     * Result will returned via {@link SimplePendingCallback}.
     */
	public void checkStatus() {
        if(mQueryId != null) {
            if(getStatus() == Status.STARTED && mTaskQuery != null)
                mTaskQuery.abort();

            setStatus(Status.STARTED);
            mTaskQuery = HonkioApi.queryPaymentRequest(mQueryId, mShop, mFlags,
                    new AcceptCheckCallback());
        }
        else
            removePendingCard();
	}

    /**
     * Gets pending transaction Id. If transaction status isn't Pending the null will be returned.
     * @return Pending transaction Id or null if transaction status isn't Pending.
     */
	public String getPendingQueryId() {
		return mQueryId;
	}
	
	@Override
    public boolean abort() {
		if(getStatus() != Status.FINISHED) {
			super.abort();
			removePendingCard();
		}
        return true;
	}

	@Override
	public void reset() {
		mQueryId = null;
		super.reset();
	}

    /**
     * Sets specified Flags for requests. Flags can be changed only if model status is {@link BaseProcessModel.Status#CREATED}.
     * @param flags Flags for requests.
     * @throws IllegalStateException if status of the model isn't {@link BaseProcessModel.Status#CREATED}
     */
    public void setFlags(int flags) {
        if (getStatus() == Status.CREATED)
            mFlags = flags;
        else
            throw new IllegalStateException("Flags must be set when status is CREATED.");
    }

    private class AddEmptyAccount implements RequestCallback<User> {

        @Override
		public boolean onError(RpError error) {
			setStatus(Status.ABORTED);
			return mCallback.onError(error);
		}
		
		@Override
        public boolean onComplete(Response<User> response) {
            if(!isAborted()) {
                if (response.isStatusAccept())
                    if (isPaymentCheckEnabled) {
                        Shop shop = mShop != null ? mShop : HonkioApi.getMainShopInfo().getShop();
                        String shopType = shop != null ? shop.getType() : null;

                        if (Shop.Type.PAYMENT.equals(shopType))
                            mTaskPayment = HonkioApi.paymentRequest(0, null,
                                    mAccount, shop, mFlags, new PendingCallback());
                        else
                            mTaskPayment = HonkioApi.paymentRequest((List<BookedProduct>) null,
                                    mAccount, shop, mFlags, new PendingCallback());
                    }
                    else {
                        setStatus(Status.FINISHED);
                        mCallback.onComplete(response.getResult()
                                .getAccount(mAccount.getType(), mAccount.getNumber()));
                    }

                else {
                    return onError(response.getAnyError());
                }
            }
            else
                removePendingCard();
            return true;
		}
	};

    private class PendingCallback implements RequestCallback<UserPayment> {

		@Override
		public boolean onError(RpError error) {
			removePendingCard();
			return mCallback.onError(error);
		}
		
		@Override
        public boolean onComplete(Response<UserPayment> response) {
            if(getStatus() != Status.ABORTED) {
                if(response.isStatusAccept()) {
                    HonkioApi.userGet(0, new GetUserCallback());
                    return true;
                }
                else if(response.isStatusPending()) {
                    setStatus(Status.PENDING);
                    mQueryId = response.getId();
                    return mCallback.handlePending(response);
                }
                else {
                    removePendingCard();
                    return mCallback.onError(response.getAnyError());
                }
            }
            else {
                removePendingCard();
            }
            return true;
        }
	};
	
	private class AcceptCheckCallback implements RequestCallback<UserPayment> {
		
		@Override
		public boolean onError(RpError error) {
			removePendingCard();
			return mCallback.onError(error);
		}
		
		@Override
        public boolean onComplete(Response<UserPayment> response) {
            if(response.isStatusAccept()) {
                HonkioApi.userGet(0, new GetUserCallback());
                return true;
            }
            else if(response.isStatusReject()) {
                removePendingCard();
                return mCallback.onError(response.getAnyError());
            }
            else if(getStatus() == Status.ABORTED)
                removePendingCard();
            else if(response.isStatusPending())
                setStatus(Status.PENDING);
            return true;
        }
	}

	private class GetUserCallback implements RequestCallback<User> {

        @Override
        public boolean onError(RpError error) {
            removePendingCard();
            return mCallback.onError(error);
        }

        @Override
        public boolean onComplete(Response<User> response) {
            if(response.isStatusAccept()) {
                setStatus(Status.FINISHED);

                List<UserAccount> accounts = response.getResult().getAccountsList();

                return mCallback.onComplete(accounts.get(accounts.size() - 1));
            }

            return onError(response.getAnyError());
        }
    }
	
	private void removePendingCard() {
		if(getStatus() != Status.FINISHED) {
			setStatus(Status.ABORTED);
            User user = HonkioApi.getActiveUser();
            if (user != null)
                for(int i = user.getAccountsList().size() - 1; i >= 0; i--) {
                    UserAccount account = user.getAccountsList().get(i);
                    if(mAccount.getType().equals(account.getType())
                            && mAccount.getNumber().equals(account.getNumber())) {
                        user.getAccountsList().remove(account);
                        HonkioApi.userDeleteAccount(account, Message.FLAG_NO_WARNING, null);
                        return;
                    }
                }
		}
	}
}
