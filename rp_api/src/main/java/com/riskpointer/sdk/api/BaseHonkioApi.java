package com.riskpointer.sdk.api;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;

import com.riskpointer.sdk.api.exception.WrongKeyException;
import com.riskpointer.sdk.api.model.entity.AppInfo;
import com.riskpointer.sdk.api.model.entity.Asset;
import com.riskpointer.sdk.api.model.entity.AssetList;
import com.riskpointer.sdk.api.model.entity.CalendarSchedule;
import com.riskpointer.sdk.api.model.entity.HKDevice;
import com.riskpointer.sdk.api.model.entity.HkStructureList;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.Schedule;
import com.riskpointer.sdk.api.model.entity.ServerFile;
import com.riskpointer.sdk.api.model.entity.ServerInfo;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.ShopFind;
import com.riskpointer.sdk.api.model.entity.ShopInfo;
import com.riskpointer.sdk.api.model.entity.ShopProducts;
import com.riskpointer.sdk.api.model.entity.Transaction;
import com.riskpointer.sdk.api.model.entity.Transactions;
import com.riskpointer.sdk.api.model.entity.UserCommentsList;
import com.riskpointer.sdk.api.model.entity.UserRoleDescription;
import com.riskpointer.sdk.api.model.entity.UserRoleDescriptionsList;
import com.riskpointer.sdk.api.model.task.RequestTask;
import com.riskpointer.sdk.api.model.task.SimpleRequestTask;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.model.task.TaskWrapper;
import com.riskpointer.sdk.api.support.BroadcastManager;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.utils.LocationFinder;
import com.riskpointer.sdk.api.web.ApiCache;
import com.riskpointer.sdk.api.web.Connection;
import com.riskpointer.sdk.api.web.WebServiceAsync;
import com.riskpointer.sdk.api.web.callback.ErrorCallback;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.callback.SimpleCallback;
import com.riskpointer.sdk.api.web.callback.SimplifiedRequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.Message.Command;
import com.riskpointer.sdk.api.web.message.filter.AssetFilter;
import com.riskpointer.sdk.api.web.message.filter.ScheduleFilter;
import com.riskpointer.sdk.api.web.message.filter.ShopFilter;
import com.riskpointer.sdk.api.web.message.filter.ShopProductsFilter;
import com.riskpointer.sdk.api.web.message.filter.SplitScheduleFilter;
import com.riskpointer.sdk.api.web.message.filter.TransactionFilter;
import com.riskpointer.sdk.api.web.message.filter.UserCommentsFilter;
import com.riskpointer.sdk.api.web.message.filter.UserRoleDescriptionsFilter;
import com.riskpointer.sdk.api.web.protocol.ProtocolFactory;
import com.riskpointer.sdk.api.web.protocol.RequestBuilder;
import com.riskpointer.sdk.api.web.protocol.ResponseParser;
import com.riskpointer.sdk.api.web.protocol.json.JsonProtocolFactory;
import com.riskpointer.sdk.api.web.response.Response;

import java.util.HashMap;
import java.util.Locale;

/**
 * This class contains a lot of static methods to work with
 * server. All methods for the requests has parameter "Callback".<br>
 * Callback is an implementation of the interface <b>RequestCallback{@literal <}T></b>.
 * This interface contains two methods:<br>
 * <b>onComplete(T response)</b> call when the server returns response and this
 * response is not error;<br>
 * <b>onError(ErrorDescription error)</b> call when server returns an error or not
 * all conditions are satisfied for sending the request.<br>
 * <br>
 * Requests execute in series but asynchronous for UI thread.<br>
 * <br>
 * This is helps don't lose OTP and don't freeze UI. The apps that used this
 * library don't care about OTP and other service parameters. OTP will saved and
 * used automatically.<br>
 * <br>
 * <b>Warning: </b><br>
 * Despite the fact that the tasks are executed sequentially, the order of
 * callbacks can be broken.<br>
 * Also, this class doesn't support work in the multiple tasks.
 *
 * @author Denis Shurygin
 */
@SuppressWarnings({
        "UnusedReturnValue",
        "WeakerAccess",
        "unqualified-field-access",
        "SameParameterValue"
})
public class BaseHonkioApi {
    /** Version of the server API. */
    public static final String VERSION = "1";
    /** Platform of the server API. PLATFORM = "android". */
    public static final String PLATFORM = "android";

    static final Object STATIC_LOCK = new Object();
    static final String CACHE_TIME_DIFFERENCE = "server_device_timeDifference";

    static final String PREFERENCE_FILE = "SessionCache";

    private static ProtocolFactory sProtocolFactory;

    @SuppressLint("StaticFieldLeak")
    private static LocationFinder sLocationFinder;

    private static SharedPreferences sApiPreferences;
    private static Connection sActiveConnection;

    @SuppressLint("StaticFieldLeak")
    private static BaseApiState sApiState;

    /* Hidden */
    static void reset() {
        sProtocolFactory = null;
        sLocationFinder = null;
        sApiPreferences = null;
        sActiveConnection = null;
        sApiState = null;
    }

    /**
     * Initialization of API.<br>
     * <p>
     * Without initialization all requests will return an error
     * <b>NO_INITIALIZED</b>.<br>
     * Also, this error can happen when application task was stopped.
     * In this case you should care about restarting application to loadServerData API again.<br>
     */
    protected static void initInstance(BaseApiState apiState, ProtocolFactory protocolFactory) {
        if (apiState == null)
            throw new IllegalArgumentException("ApiState must be not null.");
        if (sApiState != null)
            throw new IllegalStateException("Api must be initialized only once.");

        sApiState = apiState;
        sProtocolFactory = protocolFactory;

        if (getApiPreferences().contains(Connection.PASSWORD)){
            try {
                synchronized (STATIC_LOCK) {
                    sActiveConnection = new Connection(getApiPreferences(), null);
                }
            } catch (WrongKeyException e) {
                e.printStackTrace();
            }
        }

        // Check permissions
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            throwIfNoPermission(sApiState.getContext(), Manifest.permission.INTERNET);
            throwIfNoPermission(sApiState.getContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        }
        else {
            throwIfNoPermission(sApiState.getContext(), Manifest.permission.INTERNET);
            // Other permissions will checked in runtime
        }

        ApiCache.trim(sApiState.getContext());

        if (sLocationFinder == null)
            setLocationFinder(new LocationFinder(sApiState.getContext()));
    }

    /**
     * On this request the API will load <b>"serverinfo"</b> and <b>"shopinfo"</b>.
     * <p>
     * Without initialization all requests will return an error
     * <b>NO_INITIALIZED</b>.<br>
     * Also, this error can happen when application task was stopped.
     * In this case you should care about restarting application to loadServerData API again.<br>
     *
     * @param flags If contains FLAG_OFFLINE_MODE API will be used in offline mode.
     * @param initializationCallback
     *            The Object that implement an interface
     *            SimpleCallback. This object will be used to
     *            send callbacks in to UI thread.
     * 
     * @return SimpleRequestTask implementation. For understanding of the task behaviour see {@link WebServiceAsync}. Task has priority: No queue.
     */
    public static SimpleRequestTask loadServerData(final int flags, SimpleCallback initializationCallback) {
        if (!checkCriticalPermissions(initializationCallback)) {
            return null;
        }

        final SimpleRequestTaskWrapper taskWrapper = new SimpleRequestTaskWrapper(initializationCallback) ;

        loadServerData_StepDevice(flags, taskWrapper);

        return taskWrapper;
    }

    private static void loadServerData_StepDevice(final int flags,
                                                  final SimpleRequestTaskWrapper taskWrapper) {
        if (sApiState.getDevice() != null) {
            loadServerData_StepServerInfo(flags, taskWrapper);
        }
        else synchronized (taskWrapper) {
            if (!taskWrapper.isAborted()) {

                HKDevice device = new HKDevice();
                device.setApp(sApiState.getAppIdentity().getId());
                device.setName(Build.MANUFACTURER + " " + Build.MODEL);
                device.setPlatform(BaseHonkioApi.PLATFORM);
                device.setManufacturer(Build.MANUFACTURER);
                device.setModel(Build.MODEL);
                device.setOsVersion(Build.VERSION.RELEASE);

                taskWrapper.set(new WebServiceAsync<>(getContext(),
                        MessageBuilder.userSetDevice(device, flags),
                        new RequestCallback<HKDevice>() {

                    @Override
                    public boolean onComplete(Response<HKDevice> response) {
                        if (response.isStatusAccept()) {
                            sApiState.onDeviceGet(response.getResult());
                            loadServerData_StepServerInfo(flags, taskWrapper);
                            return true;
                        } else
                            return onError(response.getAnyError());
                    }

                    @Override
                    public boolean onError(RpError error) {
                        SimpleCallback initializationCallback = taskWrapper.getCallback();
                        return initializationCallback != null && initializationCallback.onError(error);
                    }
                }
                ).execute());
            }
        }
    }

    private static void loadServerData_StepServerInfo(final int flags,
                                                      final SimpleRequestTaskWrapper taskWrapper) {
        synchronized (taskWrapper) {
            if (!taskWrapper.isAborted()) {
                taskWrapper.set(new WebServiceAsync<>(getContext(),
                        MessageBuilder.serverGetInfo(flags), new RequestCallback<ServerInfo>() {

                            @Override
                            public boolean onComplete(Response<ServerInfo> response) {
                                if (response.isStatusAccept()) {
                                    loadServerData_StepAppInfo(flags, taskWrapper);
                                    return true;
                                } else
                                    return onError(response.getAnyError());
                            }

                            @Override
                            public boolean onError(RpError error) {
                                SimpleCallback initializationCallback = taskWrapper.getCallback();
                                return initializationCallback != null && initializationCallback.onError(error);
                            }
                        }
                ).execute());
            }
        }
    }

    private static void loadServerData_StepAppInfo(final int flags,
                                                   final SimpleRequestTaskWrapper taskWrapper) {
        synchronized (taskWrapper) {
            if (!taskWrapper.isAborted()) {
                taskWrapper.set(new WebServiceAsync<>(getContext(),
                        MessageBuilder.appGetInfo(flags), new RequestCallback<AppInfo>() {

                    @Override
                    public boolean onComplete(Response<AppInfo> response) {
                        if (response.isStatusAccept()) {
                            loadServerData_StepShopInfo(response.getResult().getShopIdentity(),
                                    flags, taskWrapper);
                            return true;
                        } else
                            return onError(response.getAnyError());
                    }

                    @Override
                    public boolean onError(RpError error) {
                        SimpleCallback initializationCallback = taskWrapper.getCallback();
                        return initializationCallback != null && initializationCallback.onError(error);
                    }

                }).execute());
            }
        }
    }

    private static void loadServerData_StepShopInfo(Identity shopIdentity, final int flags,
                                                    final SimpleRequestTaskWrapper taskWrapper) {

        synchronized (taskWrapper) {
            if (!taskWrapper.isAborted()) {
                taskWrapper.set(new WebServiceAsync<>(getContext(),
                        MessageBuilder.shopGetInfo(shopIdentity, flags), new RequestCallback<ShopInfo>() {
                    @Override
                    public boolean onComplete(Response<ShopInfo> response) {
                        if (response.isStatusAccept()) {
                            loadServerData_Complete(
                                    response.getResult().getTimeStamp() - System.currentTimeMillis(),
                                    flags, taskWrapper);
                            return true;
                        }
                        else
                            return onError(response.getAnyError());
                    }

                    @Override
                    public boolean onError(RpError error) {
                        SimpleCallback initializationCallback = taskWrapper.getCallback();
                        return initializationCallback != null && initializationCallback.onError(error);
                    }
                }
                ).execute());
            }
        }
    }

    private static void loadServerData_Complete(long timeDifference, final int flags,
                                                final SimpleRequestTaskWrapper taskWrapper) {
        synchronized (STATIC_LOCK) {
            getApiPreferences().edit()
                    .putLong(CACHE_TIME_DIFFERENCE, timeDifference)
                    .apply();
        }
        taskWrapper.complete();

        BroadcastManager.getInstance(getContext()).sendBroadcast(
                new Intent(BroadcastHelper.BROADCAST_ACTION_INITIALIZED));
    }

    /**
     * Checking opportunity for login with PIN code, using cached OTP.
     * @param callback Callback that will receive an error if ReLogin is impossible.
     * @return True if ReLogin is available, false otherwise.
     */
    public static boolean checkReLogin(ErrorCallback callback) {
        if(checkInitialisation(callback)) {
            if(getApiPreferences().contains(Connection.PASSWORD)) {
                return true;
            }
            else callback.onError(new RpError(RpError.Api.NO_LOGIN));
        }
        return false;
    }

    /**
     * Request Logout. Request will clear cache and OTP.
     */
    public static RequestTask<Void> logout(int flags, SimpleCallback callback) {
        if (!isConnected() || (flags & Message.FLAG_OFFLINE_MODE) == Message.FLAG_OFFLINE_MODE) {
            logout();

            if (callback != null)
                callback.onComplete();

            BroadcastManager.getInstance(getContext()).sendBroadcast(
                    new Intent(BroadcastHelper.getActionOnComplete(Command.LOGOUT)));

            return null;
        }
        else {
            Message<Void> message = new Message<>(getContext(), Command.LOGOUT,
                    getUrl(), getActiveConnection(), getDevice(), getMainShopIdentity(),
                    getAppIdentity(), newRequestBuilder(), newParser(Void.class), flags);

            return new WebServiceAsync<>(getContext(), message,
                    new SimplifiedRequestCallback<Void>(callback) {
                        @Override
                        public boolean onComplete(Response<Void> response) {
                            logout();
                            return super.onComplete(response);
                        }

                    }).execute();
        }
    }

    private static void logout() {
        synchronized (STATIC_LOCK) {
            if (sActiveConnection != null)
                sActiveConnection.exit();
            sActiveConnection = null;
        }
    }

    public static RequestTask<HKDevice> userSetDevice(HKDevice device, int flags,
                                                      RequestCallback<HKDevice> callback) {
        if (checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userSetDevice(device, flags), callback).execute();
        }
        return null;
    }

    /**
     * The request to load Shop info.
     *
     * @param shopIdentity Identity of the Shop that will loaded.
     * @param flags Flags for request.
     * @param callback
     *            The Object that implement an interface RequestCallback
     *            {@literal <}ShopInfo>. This object will be used to
     *            send callbacks in to UI thread.
     *
     * @return RequestTask implementation. For understanding of the task behaviour see
     * {@link WebServiceAsync}. Task has priority: No queue.
     */
    public static RequestTask<ShopInfo> shopGetInfo(final Identity shopIdentity, int flags,
                                                    RequestCallback<ShopInfo> callback) {
        if(checkInitialisation(callback)) {
            return new WebServiceAsync<ShopInfo>(getContext(), MessageBuilder.shopGetInfo(shopIdentity, flags), callback) {
                @Override
                public boolean invokeOnComplete(Response<ShopInfo> response) {
                    Shop shop = response.getResult().getShop();
                    if (shopIdentity != null)
                        shop.setPassword(shopIdentity.getPassword());
                    else if (shop.getId() != null && getMainShopIdentity() != null && shop.getId().equals(getMainShopIdentity().getId())){
                        shop.setPassword(getMainShopIdentity().getPassword());
                    }
                    return super.invokeOnComplete(response);
                }
            }.execute();
        }
        return null;
    }

    /**
     * The request to find shops by specified filter.
     *
     * @param filter Filter to query specified shops.
     * @param flags Flags for request.
     * @param callback
     *            The Object that implement an interface RequestCallback
     *            {@literal <}ShopFind>. This object will be used to
     *            send callbacks in to UI thread.
     *
     * @return RequestTask implementation. For understanding of the task behaviour see
     * {@link WebServiceAsync}. Task has priority: No queue.
     */
    public static RequestTask<ShopFind> shopFind(ShopFilter filter, int flags,
                                                 RequestCallback<ShopFind> callback) {
        return shopFind(filter, null, flags, callback);
    }

    /**
     * The request to find shops by id.
     *
     * @param id Id of the Shop.
     * @param flags Flags for request.
     * @param callback
     *            The Object that implement an interface RequestCallback
     *            {@literal <}ShopFind>. This object will be used to
     *            send callbacks in to UI thread.
     *
     * @return RequestTask implementation. For understanding of the task behaviour see
     * {@link WebServiceAsync}. Task has priority: No queue.
     */
    public static RequestTask<ShopFind> shopFindById(String id, int flags,
                                                     RequestCallback<ShopFind> callback) {
        if(checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.shopFindById(id, flags),
                    callback).execute();
        }
        return null;
    }

    /**
     * The request to find shops by QR-id.
     *
     * @param id QR-id of the Shop.
     * @param flags Flags for request.
     * @param callback
     *            The Object that implement an interface RequestCallback
     *            {@literal <}ShopFind>. This object will be used to
     *            send callbacks in to UI thread.
     *
     * @return RequestTask implementation. For understanding of the task behaviour see
     * {@link WebServiceAsync}. Task has priority: No queue.
     */
    public static RequestTask<ShopFind> shopFindByQrId(String id, int flags,
                                                       RequestCallback<ShopFind> callback) {
        if(checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.shopFindByQrId(id, flags),
                    callback).execute();
        }
        return null;
    }
    
    /**
     * The request to find shops by filter and specified params.
     *
     * @param filter Filter to query specified shops.
     * @param params Special params that will insert into request.
     * @param flags Flags for request.
     * @param callback
     *            The Object that implement an interface RequestCallback
     *            {@literal <}ShopFind>. This object will be used to
     *            send callbacks in to UI thread.
     *
     * @return RequestTask implementation. For understanding of the task behaviour see
     * {@link WebServiceAsync}. Task has priority: No queue.
     */
    public static RequestTask<ShopFind> shopFind(ShopFilter filter, HashMap<String, Object> params,
                                                 int flags, RequestCallback<ShopFind> callback) {
        if(checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.shopFind(filter, params, flags),
                    callback).execute();
        }
        return null;
    }

    /**
     * The request to load the list of products for main Shop.
     *
     * @param filter Filter to query specified products.
     * @param flags Flags for request.
     * @param callback
     *            The Object that implement an interface
     *            RequestCallback{@literal <}ShopProducts>. This object will be used to
     *            send callbacks in to UI thread.
     *
     * @return RequestTask implementation. For understanding of the task behaviour see {@link WebServiceAsync}. Task has priority: No queue.
     */
    public static RequestTask<ShopProducts> shopGetProducts(ShopProductsFilter filter, int flags,
                                                            RequestCallback<ShopProducts> callback) {
        return shopGetProducts(filter, getMainShopIdentity(), flags, callback);
    }

    /**
     * The request to load the list of products for specified Shop.
     *
     * @param filter Filter to query specified products.
     * @param shopIdentity Identity of the shop.
     * @param flags Flags for request.
     * @param callback
     *            The Object that implement an interface RequestCallback
     *            {@literal <}ShopProducts>. This object will be used to
     *            send callbacks in to UI thread.
     *
     * @return RequestTask implementation. For understanding of the task behaviour see {@link WebServiceAsync}. Task has priority: No queue.
     */
    public static RequestTask<ShopProducts> shopGetProducts(ShopProductsFilter filter, Identity shopIdentity, int flags,
                                                            RequestCallback<ShopProducts> callback) {
        if(checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.shopGetProducts(filter, shopIdentity, flags),
                    callback).execute();
        }
        return null;
    }

    /**
     * The request to load the list of transactions for specified Shop.
     *
     * @param filter Filter to query specified transactions.
     * @param shopIdentity  Identity of the shop.
     * @param flags Flags for request.
     * @param callback
     *            The Object that implement an interface RequestCallback
     *            {@literal <}Transactions>. This object will be used to
     *            send callbacks in to UI thread.
     *
     * @return RequestTask implementation. For understanding of the task behaviour see {@link WebServiceAsync}. Task has priority: No queue.
     */
    public static RequestTask<Transactions> shopGetTransactions(TransactionFilter filter,
                        Identity shopIdentity, int flags, RequestCallback<Transactions> callback) {
        if(checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.shopGetTransactions(filter, shopIdentity, flags),
                    callback).execute();
        }
        return null;
    }

    // TODO write docs
    public static RequestTask<ServerFile> serverGetToU(int flags,
                                                       RequestCallback<ServerFile> callback) {
        if(checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.serverGetToU(flags),
                    callback).execute();
        }
        return null;
    }

    /**
     * The request to set another status of transaction.
     *
     * @param status New status. {@link Response.Status}
     * @param transactionId Transaction ID which status would be changed.
     * @param shopIdentity Identity of the shop which contains transaction.
     * @param flags Flags for request.
     * @param callback
     *            The Object that implement an interface RequestCallback
     *            {@literal <}Void>. This object will be used to
     *            send callbacks in to UI thread.
     *
     * @return RequestTask implementation. For understanding of the task behaviour see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<Void> transactionSetStatus(String status, String transactionId, Identity shopIdentity, int flags, RequestCallback<Void> callback) {
        if(checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.transactionSetStatus(status, transactionId, shopIdentity, flags),
                    callback).execute();
        }
        return null;
    }

    /**
     * The request to send stamp message to check product validity.
     *
     * @param transactionId Transaction Id which would be checked.
     * @param shopIdentity Identity of the shop which contains transaction.
     * @param flags Flags for request.
     * @param callback
     *            The Object that implement an interface RequestCallback
     *            {@literal <}Transaction>. This object will be used to
     *            send callbacks in to UI thread.
     * @return RequestTask implementation. For understanding of the task behaviour see {@link WebServiceAsync}. Task has priority: High Priority.
     */
    public static RequestTask<Transaction> shopStamp(String transactionId, Identity shopIdentity,
                                                int flags, RequestCallback<Transaction> callback) {
        if(checkApi(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.shopStamp(transactionId, shopIdentity, flags),
                    callback).execute();
        }
        return null;
    }

    // TODO docs
    public static RequestTask<Schedule> getSplitSchedule(SplitScheduleFilter filter,
                                                             Identity shopIdentity, int flags, RequestCallback<Schedule> callback) {
        if(checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.getSplitSchedule(filter, shopIdentity, flags),
                    callback).execute();
        }
        return null;
    }
    /* Created for "Barber" application */
    // TODO docs
    public static RequestTask<UserCommentsList> shopGetRate(UserCommentsFilter filter,
                    Identity shopIdentity, int flags, RequestCallback<UserCommentsList> callback) {
        if(checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.shopGetRate(filter, shopIdentity, flags),
                    callback).execute();
        }
        return null;
    }

    public static RequestTask<Asset> userGetAsset(String assetId, int flags, RequestCallback<Asset> callback) {
        return userGetAsset(assetId, getMainShopIdentity(), false, flags, callback);
    }

    public static RequestTask<Asset> userGetAsset(String assetId, Identity shopIdentity, boolean searchInSchema, int flags, RequestCallback<Asset> callback) {
        if (checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(), MessageBuilder.userGetAsset(assetId, shopIdentity, searchInSchema, flags),
                    callback).execute();
        }
        return null;
    }

    // TODO docs
    public static RequestTask<AssetList> userAssetList(AssetFilter filter,
                                                           Identity shopIdentity, int flags, RequestCallback<AssetList> callback) {
        if(checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userAssetList(filter, shopIdentity, flags),
                    callback).execute();
        }
        return null;
    }

    // TODO docs
    public static RequestTask<UserRoleDescriptionsList> userRoleDescriptionsList(
            UserRoleDescriptionsFilter filter, Identity shopIdentity, int flags,
            RequestCallback<UserRoleDescriptionsList> callback) {
        if (checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userRoleDescriptionsList(filter, shopIdentity, flags),
                    callback).execute();
        }
        return null;
    }

    // TODO docs
    public static Task userRoleDescription(
            String code, Identity shopIdentity, int flags,
            final RequestCallback<UserRoleDescription> callback) {
        if (checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userRoleDescription(code, shopIdentity, flags),
                    new RequestCallback<UserRoleDescriptionsList>() {
                        @Override
                        public boolean onComplete(Response<UserRoleDescriptionsList> response) {
                            if (response.isStatusAccept()
                                    && response.getResult().getList().size() > 0)
                                return callback.onComplete(new Response<>(response,
                                        response.getResult().getList().get(0)));
                            return onError(response.getAnyError());
                        }

                        @Override
                        public boolean onError(RpError error) {
                            return callback.onError(error);
                        }
                    }).execute();
        }
        return null;
    }

    public static RequestTask<HkStructureList> userStructureDescriptionList(String roleId,
                                                                 Identity shopIdentity, int flags, RequestCallback<HkStructureList> callback) {
        return userStructureDescriptionList(new String[] {roleId}, shopIdentity, flags, callback);
    }

    public static RequestTask<HkStructureList> userStructureDescriptionList(String[] roleIds,
                                                                 Identity shopIdentity, int flags, RequestCallback<HkStructureList> callback) {
        if (checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.userStructureDescriptionList(roleIds, shopIdentity, flags), callback).execute();
        }
        return null;
    }

    @Deprecated
    public static RequestTask<HkStructureList> assetStructureList(String structureId, boolean searchInSchema,
                                                                  Identity shopIdentity, int flags, RequestCallback<HkStructureList> callback) {
        return assetStructureList(new String[] {structureId}, searchInSchema, shopIdentity, flags, callback);
    }

    @Deprecated
    public static RequestTask<HkStructureList> assetStructureList(String structureId,
                  Identity shopIdentity, int flags, RequestCallback<HkStructureList> callback) {
        return assetStructureList(new String[] {structureId}, shopIdentity, flags, callback);
    }

    @Deprecated
    public static RequestTask<HkStructureList> assetStructureList(String[] structureIds,
                  Identity shopIdentity, int flags, RequestCallback<HkStructureList> callback) {
        return assetStructureList(structureIds, false, shopIdentity, flags, callback);
    }

    @Deprecated
    public static RequestTask<HkStructureList> assetStructureList(String[] structureIds, boolean searchInSchema,
                                                                  Identity shopIdentity, int flags, RequestCallback<HkStructureList> callback) {
        if (checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.assetStructure(structureIds, searchInSchema, shopIdentity, flags), callback).execute();
        }
        return null;
    }

    public static RequestTask<CalendarSchedule> getSchedule(ScheduleFilter filter,
                                                             Identity shopIdentity, int flags, RequestCallback<CalendarSchedule> callback) {
        if(checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.getSchedule(filter, shopIdentity, flags),
                    callback).execute();
        }
        return null;
    }

    public static RequestTask<ShopProducts> getScheduleProducts(ScheduleFilter filter,
                                                            Identity shopIdentity, int flags, RequestCallback<ShopProducts> callback) {
        if(checkInitialisation(callback)) {
            return new WebServiceAsync<>(getContext(),
                    MessageBuilder.getScheduleProducts(filter, shopIdentity, flags),
                    callback).execute();
        }
        return null;
    }
//-------------------------------------------------------------------------------------------
//-------- Getters and setters --------------------------------------------------------------
//-------------------------------------------------------------------------------------------

    /**
     * Gets the factory that define format of the requests and responses.
     * @return Implementation of {@link ProtocolFactory} interface.
     * Default {@link JsonProtocolFactory}
     */
    public static ProtocolFactory getProtocolFactory() {
        if (sProtocolFactory == null) {
            synchronized (STATIC_LOCK) {
                if (sProtocolFactory == null)
                    sProtocolFactory = new JsonProtocolFactory();
            }
        }
        return sProtocolFactory;
    }

    /**
     * Check if API initialized in offline mode.
     * @return True if app is in offline mode, false otherwise.
     */
    @Deprecated
    public static boolean isOnOfflineMode() {
        return (0 & Message.FLAG_OFFLINE_MODE) == Message.FLAG_OFFLINE_MODE;
    }

    /**
     * Gets active Connection.
     * @return Active Connection.
     */
    public static Connection getActiveConnection() {
        synchronized (STATIC_LOCK) {
            return sActiveConnection;
        }
    }

    /**
     * Gets Device info.
     * @return Device info or null if API initialization was no completed.
     */
    public static HKDevice getDevice() {
        return sApiState != null ? sApiState.getDevice() : null;
    }

    /**
     * Gets server info.
     * @return Server info or null if API initialization was no completed.
     */
    public static ServerInfo getServerInfo() {
        return sApiState != null ? sApiState.getServerInfo() : null;
    }

    //TODO write docs
    public static AppInfo getAppInfo() {
        return sApiState != null ? sApiState.getAppInfo() : null;
    }

    /**
     * Gets Main shop info.
     * @return Shop info or null if API initialization was no completed.
     */
    public static ShopInfo getMainShopInfo() {
        return sApiState != null ? sApiState.getMainShopInfo() : null;
    }

    /**
     * Gets main shop Identity defined in the initialization.
     * @return Main shop Identity.
     */
    public static Identity getMainShopIdentity() {
        return sApiState != null ? sApiState.getMainShopIdentity() : null;
    }

    // TODO write docs
    public static Context getContext() {
        return sApiState != null ? sApiState.getContext() : null;
    }

    /**
     * Gets Url address of the server defined in the initialization.
     * @return Url address of the server.
     */
    public static String getUrl() {
        return sApiState != null ? sApiState.getUrl() : null;
    }

    /**
     * Ges App id defined in the initialization.
     * @return App id.
     */
    public static Identity getAppIdentity() {
        return sApiState != null ? sApiState.getAppIdentity() : null;
    }

    /**
     * Gets Shared preferences that used to store API values.
     * @return Shared preferences implementation.
     */
    public static SharedPreferences getApiPreferences() {
        synchronized (STATIC_LOCK) {
            if(sApiPreferences == null && getContext() != null)
                sApiPreferences = getContext().getSharedPreferences(PREFERENCE_FILE, Activity.MODE_PRIVATE);
            return sApiPreferences;
        }
    }

    /**
     * Check if API initialization is completed.
     * @return True if Shop info and server info not null, false otherwise.
     */
    public static boolean isInitialised() {
        return sApiState != null &&
                getDevice() != null &&
                getMainShopInfo() != null &&
                getServerInfo() != null &&
                getAppInfo() != null;
    }

    /**
     * Check if user is connected.
     * @return True if API initialized, Connection was created and valid.
     */
    public static boolean isConnected() {
        Connection connection = getActiveConnection();
        return connection != null && connection.isValid() && isInitialised();
    }

    /**
     * Sets custom location finder. Use it for debug.
     * @param finder custom location finder.
     */
    public static void setLocationFinder(LocationFinder finder) {
        if (sLocationFinder != null)
            sLocationFinder.stop();
        sLocationFinder = finder;
    }

    /**
     * Gets location finder.
     * @return Location finder.
     */
    public static LocationFinder getLocationFinder() {
        if (sLocationFinder == null)
            sLocationFinder = new LocationFinder(getContext());
        return sLocationFinder;
    }

    /**
     * Gets Location of Device from LocationFinder.
     * @return Location of Device.
     */
    public static Location getLocation() {
        return getLocationFinder().getLocation();
    }

    /**
     * Gets current time on server in millis for specified time on device.
     * @param deviceTime Time on device in millis.
     * @return Current time on server in millis.
     */
    public static long serverTimeMillis(long deviceTime) {
        return deviceTime + getApiPreferences().getLong(CACHE_TIME_DIFFERENCE, 0);
    }

    /**
     * Convert server time to device time.
     * @param serverTime Time on server in millis.
     * @return Device time converted from server time.
     */
    public static long deviceTimeMillis(long serverTime) {
        return serverTime - getApiPreferences().getLong(CACHE_TIME_DIFFERENCE, 0);
    }

    /**
     * Force change Connection PIN to another / reset pin
     * Note: You have to handle PinFailsCount & newPin validity manually
     * @param newPin New PIN code.
     */
    public static void changePin(String newPin) {
        getActiveConnection().changePin(newPin);
    }

    /**
     * Change Connection pin to another. PIN will be changed only if current PIN is correct.
     * @param currentPin Current PIN code.
     * @param newPin New PIN code.
     * @return True if check of current PIN is success, false otherwise.
     * @see #checkPin(String)
     */
    public static boolean changePin(String currentPin, String newPin) {
        if (isConnected()) {
            boolean result = getActiveConnection().changePin(currentPin, newPin);

            if (result) {

                // TODO Encode cache with new pin

            }

            return result;
        }
        return false;
    }

    /**
     * Check if given PIN code is the PIN code of the Connection. If check will be failed {@link Connection#increasePinFails()} will called.
     * @param pin PIN code.
     * @return True if Connection has the same PIN code or Connection PIN code is null, false otherwise.
     */
    public static boolean checkPin(String pin) {
        return isConnected() && getActiveConnection().checkPin(pin);
    }

    /**
     * Check if API initialized and connection is valid.
     * @param callback Callback that will receive error if condition is not quite.
     * @return True if API initialized and connection is valid, false otherwise.
     */
    public static boolean checkApi(ErrorCallback callback) {
        return checkInitialisation(callback)
                && checkConnection(callback);
    }

    /**
     * Check if API initialized.
     * @param callback Callback that will receive error if condition is not quite.
     * @return True if API initialized, false otherwise.
     */
    public static boolean checkInitialisation(ErrorCallback callback) {
        if(!isInitialised()) {
            if(callback != null)
                callback.onError(new RpError(RpError.Api.NO_INITIALIZED));
            return false;
        }

        return checkCriticalPermissions(callback);
    }

    /**
     * Check if action connection is valid.
     * @param callback Callback that will receive error if condition is not quite.
     * @return True if connection is valid and API is not in offline mode, false otherwise.
     */
    public static boolean checkConnection(ErrorCallback callback) {
        Connection connection = getActiveConnection();
        if(connection == null || !connection.isValid()) {
            if(callback != null) {
                if (connection != null && connection.isEncrypted())
                    callback.onError(new RpError(RpError.Api.ABORTED));
                else
                    callback.onError(new RpError(RpError.Api.NO_LOGIN));
            }
            return false;
        }
        return true;
    }

    public static String getSuitableLanguage() {
        ServerInfo serverInfo = getServerInfo();
        if (serverInfo != null)
            return serverInfo.getSuitableLanguage();
        return Locale.getDefault().getLanguage();
    }

    public static class MessageBuilder {

        public static Message<HKDevice> userSetDevice(HKDevice device, int flags) {
            Message<HKDevice> message = new Message<>(getContext(), Command.SET_DEVICE,
                    getUrl(), device.getId() != null ? getActiveConnection() : null, getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(HKDevice.class), flags);
            message.setCacheFileName(Command.SET_DEVICE, false);
            message.setSupportOffline(false);

            message.addParameters(device.toParams());

            return message;
        }

        /**
         * The request to load Server info.
         * @param flags Flags for request.
         */
        public static Message<ServerInfo> serverGetInfo(int flags) {
            Message<ServerInfo> message = new Message<>(getContext(), Command.SERVER_INFO,
                    getUrl(), null, getDevice(), new Identity("none", ""), getAppIdentity(),
                    newRequestBuilder(), newParser(ServerInfo.class), flags);
            message.setCacheFileName(Command.SERVER_INFO, false);
            message.addParameter(Message.Param.QUERY_LANGUAGE, getSuitableLanguage());
            message.setHashIgnore(true);
            return message;
        }

        /**
         * The request to load Application info.
         * @param flags Flags for request.
         */
        public static Message<AppInfo> appGetInfo(int flags) {
            Message<AppInfo> message = new Message<>(getContext(), Command.APP_INFO,
                    getUrl(), null, getDevice(), new Identity("none", ""), getAppIdentity(),
                    newRequestBuilder(), newParser(AppInfo.class), flags);
            message.setCacheFileName(Command.APP_INFO, false);
            message.setHashIgnore(true);
            return message;
        }

        /**
         * The request to load Shop info.
         *
         * @param shopIdentity Identity of the Shop that will loaded.
         * @param flags Flags for request.
         */
        public static Message<ShopInfo> shopGetInfo(Identity shopIdentity, int flags) {
            Message<ShopInfo> message = new Message<>(getContext(), Command.SHOP_INFO, getUrl(),
                    null, getDevice(), shopIdentity,
                    getAppIdentity(), newRequestBuilder(), newParser(ShopInfo.class), flags);
            message.setCacheFileName(Command.SHOP_INFO, true);
            message.addParameter(Message.Param.SHOP_QUERY_SHOW_RATINGS, true);
            return message;
        }

        /**
         * The request to find shops by id.
         *
         * @param id Id of the Shop.
         * @param flags Flags for request.
         */
        public static Message<ShopFind> shopFindById(String id, int flags) {
            HashMap<String, Object> params = new HashMap<>(1);
            params.put(Message.Param.SHOP_ID_FILTER, id);
            return shopFind(null, params, flags);
        }

        /**
         * The request to find shops by QR-id.
         *
         * @param id QR-id of the Shop.
         * @param flags Flags for request.
         */
        public static Message<ShopFind> shopFindByQrId(String id, int flags) {
            HashMap<String, Object> params = new HashMap<>(1);
            params.put(Message.Param.SHOP_QR_ID_FILTER, id);
            return shopFind(null, params, flags);
        }

        /**
         * The request to find shops by filter and specified params.
         *
         * @param filter Filter to query specified shops.
         * @param params Special params that will insert into request.
         * @param flags Flags for request.
         */
        public static Message<ShopFind> shopFind(ShopFilter filter, HashMap<String, Object> params,
                                                 int flags) {
            Message<ShopFind> message = new Message<>(getContext(),
                    Command.SHOP_FIND, getUrl(),
                    null, getDevice(), getMainShopIdentity(), getAppIdentity(),
                    newRequestBuilder(), newParser(ShopFind.class), flags);
            if(filter != null) {
                filter.applyTo(message);
            }
            if(params != null)
                message.addParameters(params);

            return message;
        }

        /**
         * The request to load the list of products for main Shop.
         * @param flags Flags for request.
         */
        public static Message<ShopProducts> shopGetProducts(ShopProductsFilter filter, int flags) {
            return shopGetProducts(filter, getMainShopIdentity(), flags);
        }

        /**
         * The request to load the list of products for specified Shop.
         *
         * @param shopIdentity Identity of the shop.
         * @param flags Flags for request.
         */
        public static Message<ShopProducts> shopGetProducts(ShopProductsFilter filter, Identity shopIdentity, int flags) {
                Message<ShopProducts> message = new Message<>(getContext(),
                        Command.SHOP_GET_PRODUCTS, getUrl(), null, getDevice(),
                        shopIdentity,
                        getAppIdentity(),
                        newRequestBuilder(), newParser(ShopProducts.class), flags);
                message.setCacheFileName(Command.SHOP_GET_PRODUCTS, true);
                message.addParameter(Message.Param.QUERY_LANGUAGE, getSuitableLanguage());

                if (filter != null)
                    filter.applyTo(message);

                return message;
        }

        /**
         * The request to load the list of transactions for specified Shop.
         *
         * @param filter Filter to query specified transactions.
         * @param shopIdentity  Identity of the shop.
         * @param flags Flags for request.
         */
        public static Message<Transactions> shopGetTransactions(TransactionFilter filter,
                                                                Identity shopIdentity, int flags) {
            Message<Transactions> message = new Message<>(getContext(),
                    Command.SHOP_TRANSACTIONS, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(Transactions.class), flags);

            boolean onlyValid = filter != null && filter.getOnlyValid() != null && filter.getOnlyValid();
            if (onlyValid)
                message.setCacheFileName(Command.SHOP_TRANSACTIONS + "_onlyValid", true);

            if(filter != null) {
                filter.applyTo(message);
            }
            message.addParameter(null, filter);

            return message;
        }

        // TODO write docs
        public static Message<ServerFile> serverGetToU(int flags) {
                Connection connection = getActiveConnection();
                if (connection != null && !connection.isValid())
                    connection = null;

                Message<ServerFile> message = new Message<>(getContext(),
                        Command.SERVER_GET_TOU, getUrl(),
                        connection, getDevice(), getMainShopIdentity(), getAppIdentity(),
                        newRequestBuilder(), newParser(ServerFile.class), flags);
                message.setCacheFileName(Command.SERVER_GET_TOU, false);
                message.addParameter(Message.Param.QUERY_LANGUAGE, getSuitableLanguage());

                return message;
        }

        /**
         * The request to set another status of transaction.
         *
         * @param status New status. {@link Response.Status}
         * @param transactionId Transaction ID which status would be changed.
         * @param shopIdentity Identity of the shop which contains transaction.
         * @param flags Flags for request.
         */
        public static Message<Void> transactionSetStatus(String status, String transactionId,
                                                         Identity shopIdentity, int flags) {
            Message<Void> message = new Message<>(getContext(),
                    Command.TRANSACTION_STATUS_SET, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(Void.class), flags);
            message.addParameter(Message.Param.ID, transactionId);
            message.addParameter(Message.Param.STATUS, status);

            return message;
        }

        /**
         * The request to send stamp message to check product validity.
         *
         * @param transactionId Transaction Id which would be checked.
         * @param shopIdentity Identity of the shop which contains transaction.
         * @param flags Flags for request.
         */
        public static Message<Transaction> shopStamp(String transactionId, Identity shopIdentity,
                                                     int flags) {
            Message<Transaction> message = new Message<>(getContext(),
                    Command.SHOP_STAMP, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(Transaction.class), flags);
            message.addParameter(Message.Param.ID, transactionId);

            return message;
        }

        public static Message<Schedule> getSplitSchedule(SplitScheduleFilter filter,
                                                             Identity shopIdentity, int flags) {
            Message<Schedule> message = new Message<>(getContext(),
                    Command.GET_SPLIT_SCHEDULE, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(Schedule.class), flags);

            if (filter != null)
                filter.applyTo(message);

            return message;
        }
        public static Message<UserCommentsList> shopGetRate(UserCommentsFilter filter,
                                                            Identity shopIdentity, int flags) {
            Message<UserCommentsList> message = new Message<>(getContext(),
                    Command.SHOP_GET_RATE, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(UserCommentsList.class), flags);

            if (filter != null)
                filter.applyTo(message);

            return message;
        }

        public static Message<Asset> userGetAsset(String assetId, int flags) {
            return userGetAsset(assetId, null,  false, flags);
        }

        public static Message<Asset> userGetAsset(String assetId, Identity shopIdentity, boolean searchInSchema, int flags) {
            Message<Asset> message = new Message<>(getContext(), Message.Command.USER_GET_ASSET, getUrl(),
                    getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(Asset.class), flags);

            message.addParameter("id", assetId);
            if (searchInSchema) {
                message.addParameter("schema_model", searchInSchema);
            }

            return message;
        }

        /**
         * The request to find user assets by filter and specified params.
         *
         * @param filter Filter to query specified shops.
         * @param flags Flags for request.
         */
        public static Message<AssetList> userAssetList(AssetFilter filter, Identity shopIdentity,
                                                           int flags) {
            Message<AssetList> message = new Message<>(getContext(),
                    Command.USER_ASSET_LIST, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(AssetList.class), flags);
            if(filter != null) {
                filter.applyTo(message);
            }
            return message;
        }

        public static Message<UserRoleDescriptionsList> userRoleDescriptionsList(
                UserRoleDescriptionsFilter filter, Identity shopIdentity, int flags) {
            Message<UserRoleDescriptionsList> message = new Message<>(getContext(),
                    Command.MERCHANT_ROLER, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(UserRoleDescriptionsList.class), flags);

            if (filter != null)
                filter.applyTo(message);

            return message;
        }

        public static Message<UserRoleDescriptionsList> userRoleDescription(
                String code, Identity shopIdentity, int flags) {
            Message<UserRoleDescriptionsList> message = new Message<>(getContext(),
                    Command.MERCHANT_ROLER, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(UserRoleDescriptionsList.class), flags);

            message.addParameter("query_code", code);

            return message;
        }

        @Deprecated
        private static Message<HkStructureList> assetStructure(String[] structureIds, boolean searchInSchema, Identity shopIdentity, int flags) {
            Message<HkStructureList> message = new Message<>(getContext(),
                    Command.USER_ASSET_STRUCTURE, getUrl(), null, getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(HkStructureList.class), flags);

            message.addParameter("query_id", structureIds);
            if (searchInSchema) {
                message.addParameter("schema_model", searchInSchema);
            }

            return message;
        }

        public static Message<HkStructureList> userStructureDescriptionList(String[] roleIds, Identity shopIdentity, int flags) {
            Message<HkStructureList> message = new Message<>(getContext(),
                    Command.USER_ROLES_DESCRIPTION, getUrl(), null, getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(HkStructureList.class), flags);

            message.addParameter("roles", roleIds);

            return message;
        }

        public static Message<CalendarSchedule> getSchedule(ScheduleFilter filter,
                                                            Identity shopIdentity, int flags) {
            Message<CalendarSchedule> message = new Message<>(getContext(),
                    Command.GET_SCHEDULE, getUrl(), getActiveConnection(), getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(CalendarSchedule.class), flags);

            if (filter != null)
                filter.applyTo(message);

            return message;
        }

        public static Message<ShopProducts> getScheduleProducts(ScheduleFilter filter,
                                                               Identity shopIdentity, int flags) {
            Message<ShopProducts> message = new Message<>(getContext(),
                    Command.GET_SCHEDULE_PRODUCTS, getUrl(), null, getDevice(),
                    shopIdentity, getAppIdentity(),
                    newRequestBuilder(), newParser(ShopProducts.class), flags);

            if (filter != null)
                filter.applyTo(message);

            return message;
        }
    }

//-------------------------------------------------------------------------------------------
//-------- Private Methods ------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

    static Connection initConnection(SharedPreferences preferences, String login,
                                               String otp) {
        synchronized (STATIC_LOCK) {
            sActiveConnection = new Connection(preferences, login, "", null);
            sActiveConnection.setOtp(otp);
            return sActiveConnection;
        }
    }

    /**
     * Creates Connection object from cache.
     * @param preferences Shared preferences that contains cached login and password.
     * @param pin PIN to decrypt OTP.
     * @return New Connection.
     * @throws WrongKeyException If PIN is wrong.
     */
    static Connection initConnectionFromCache(SharedPreferences preferences,
                                                        String pin) throws WrongKeyException {
        synchronized (STATIC_LOCK) {
            sActiveConnection = new Connection(preferences, pin);
            return sActiveConnection;
        }
    }

    static RequestBuilder newRequestBuilder() {
        return getProtocolFactory().newRequestBuilder();
    }

    static <T> ResponseParser<T> newParser(Class<T> clazz) {
        return getProtocolFactory().newParser(clazz);
    }

    private static boolean checkCriticalPermissions(ErrorCallback callback) {
        if (!checkPermission(getContext(), Manifest.permission.INTERNET)) {

            if(callback != null)
                callback.onError(new RpError(RpError.Api.NO_CRITICAL_PERMISSIONS,
                        "No critical permissions"));
            return false;
        }
        return true;
    }

    private static boolean checkPermission(Context context, String permission) {
        return context.checkCallingOrSelfPermission(permission) != PackageManager.PERMISSION_DENIED;
    }

    private static void throwIfNoPermission(Context context, String permission) {
        if(!checkPermission(context, permission))
            throw new SecurityException(
                    "Permission denied (missing " + permission + " permission?)");
    }

//-------------------------------------------------------------------------------------------
//-------- Private Classes ------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

    private static class SimpleRequestTaskWrapper extends TaskWrapper implements SimpleRequestTask {

        private volatile boolean isCompleted = false;
        private SimpleCallback mCallback;

        SimpleRequestTaskWrapper(SimpleCallback callback) {
            super();
            mCallback = callback;
        }

        @Override
        public void setCallback(SimpleCallback callback, boolean isDeliverResult) {
            mCallback = callback;
            if (isDeliverResult && isCompleted())
                callback.onComplete();
        }

        @Override
        public boolean isCompleted() {
            return isCompleted;
        }

        public SimpleCallback getCallback() {
            return mCallback;
        }

        public void complete() {
            isCompleted = true;
            if (mCallback != null)
                mCallback.onComplete();
        }
    }

}
