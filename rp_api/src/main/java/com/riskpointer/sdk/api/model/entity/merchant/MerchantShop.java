package com.riskpointer.sdk.api.model.entity.merchant;

import android.location.Location;

import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.AbsFilter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Shurygin Denis on 2016-11-24.
 */

public class MerchantShop extends Identity implements Serializable {

    private static final long serialVersionUID = -7203001353338172399L;

    public static final class Fields {
        public static final String NAME = "str_name";
        public static final String DESCRIPTION = "str_description";
        public static final String ADDRESS = "str_address";
        public static final String LOGO_SMALL = "str_logo_small";
        public static final String LOGO_LARGE = "str_logo_large";
        public static final String MAX_DISTANCE = "int_max_distance";
        public static final String PARENT = "parent";
        public static final String SHOP_TYPE = "shop_type";
        public static final String SERVICE_TYPE = "service_type";
        public static final String SHOP_MODE = "str_shop_mode";
        public static final String QR_CODE = "str_qrid";
        public static final String ACTIVE = "active";
        public static final String DAY_BREAK = "int_day_break";
        public static final String EMAIL = "str_email";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String TAG_LIST = "list_tags";
        public static final String COORDINATES = "coordinates";
        public static final String CREATE = "create";
    }

    public static final class ShopTypes {
        public static final String SHOP = "shop";
        public static final String PAYMENT = "payment";
        public static final String LOGIN = "login";
        public static final String REGISTRATION = "registration";
        public static final String LOST_PASSWORD = "lostpassword";
    }

    public static final class ServiceTypes {
        public static final String ECOMMERCE = "ecommerce";
        public static final String SYSTEM = "system";
        public static final String GAS_STATION = "gas_station";
        public static final String CAR_WASH = "car_wash";
        public static final String VENDING = "vending";
        public static final String GATE_PARKING = "gate_parking";
        public static final String OPEN_PARKING = "open_parking";
        public static final String TIME_RESERVATION = "time_reservation";
    }

    public static final class Modes {
        public static final String NORMAL = "Normal";
        public static final String HONKIO = "Honkio";
        public static final String MECSEL = "Mecsel";
    }

    public static String[] ShopTypeList = {ShopTypes.SHOP, ShopTypes.PAYMENT, ShopTypes.LOGIN,
            ShopTypes.REGISTRATION, ShopTypes.LOST_PASSWORD};
    public static String[] ServiceTypeList = {ServiceTypes.ECOMMERCE, ServiceTypes.SYSTEM,
            ServiceTypes.GAS_STATION, ServiceTypes.CAR_WASH, ServiceTypes.VENDING,
            ServiceTypes.GATE_PARKING, ServiceTypes.OPEN_PARKING, ServiceTypes.TIME_RESERVATION};
    public static String[] ModeList = new String[]{Modes.NORMAL, Modes.HONKIO, Modes.MECSEL};

    /**
     * GeneralFields used to upload merchant shop settings to server
     */
    private String[] mGeneralFields = {Fields.NAME, Fields.ADDRESS, Fields.LOGO_LARGE, Fields.LOGO_SMALL,
            Fields.MAX_DISTANCE, Fields.SHOP_TYPE, Fields.SERVICE_TYPE, Fields.SHOP_MODE,
            Fields.ACTIVE, Fields.DAY_BREAK, Fields.EMAIL, Fields.LATITUDE, Fields.LONGITUDE,
            Fields.TAG_LIST, Fields.DESCRIPTION};

    private HashMap<String, Object> mSettings = new HashMap<>();

    public MerchantShop() {
        super();
    }

    public MerchantShop(Identity identity) {
        super(identity);
    }

    public Object getField(String name) {
        if (mSettings != null) {
            return mSettings.get(name);
        }
        return null;
    }

    public String getStringField(String name) {
        if (mSettings != null) {
            if (mSettings.get(name) != null)
                if ("null".equals(mSettings.get(name).toString())) {
                    return "";
                } else {
                    return mSettings.get(name).toString();
                }
        }
        return null;
    }

    public boolean getBooleanField(String name, boolean defa) {
        if (mSettings != null) {
            if (mSettings.get(name) != null) {
                return mSettings.get(name).toString().equals("true");
            } else {
                return defa;
            }
        }
        return defa;
    }

    public void setField(String name, Object value) {
        if (mSettings != null) {
            mSettings.put(name, value);
        } else {
            mSettings = new HashMap<>();
            mSettings.put(name, value);
        }
    }

    public Map<String, Object> getSettings() {
        return mSettings;
    }

    public void setSettings(HashMap<String, Object> settings) {
        mSettings = settings;
    }

    public double getLatitude() {
        //return (getField(Fields.LATITUDE) == null ? 0.0 : (double) getField(Fields.LATITUDE));
        // TODO: 10.02.2017 uncomment when server will send latitude in the shops list
        if (getField(Fields.LATITUDE) != null) {
            return ((Number) getField(Fields.LATITUDE)).doubleValue();
        } else {
            return getCoordinate(this.getField(Fields.COORDINATES), 1);
        }
    }

    public double getLongitude() {
        //return (getField(Fields.LONGITUDE) == null ? 0.0 : (double) getField(Fields.LONGITUDE));
        // TODO: 10.02.2017 uncomment when server will send longitude in the shops list
        if (getField(Fields.LONGITUDE) != null) {
            return ((Number) getField(Fields.LONGITUDE)).doubleValue();
        } else {
            return getCoordinate(this.getField(Fields.COORDINATES), 0);
        }
    }

    public void applyTo(Message<?> message) {
        message.addParameters(toParams());
    }

    public HashMap<String, Object> toParams() {
        HashMap<String, Object> hashMap = new HashMap<>();
        for (String field : mGeneralFields) {
            AbsFilter.applyIfNotNull(hashMap, field, getField(field));
        }
        AbsFilter.applyIfNotNull(hashMap, Fields.CREATE, getField(Fields.CREATE));
        return hashMap;
    }

    public void setLocation(Location location) {
        if (location != null) {
            setField(Fields.LATITUDE, location.getLatitude());
            setField(Fields.LONGITUDE, location.getLongitude());
        }
    }

    private double getCoordinate(Object coordinates, int i) {
        double coord = 0.0;
        if ((coordinates != null) && (coordinates instanceof List)) {
            List coord_list = (List) coordinates;
            if (coord_list.size() > 0) {
                if ((coord_list.get(i) != null) && (coord_list.get(i) instanceof Double)) {
                    coord = (Double) coord_list.get(i);
                }
            }
        }
        return coord;
    }

    public ArrayList<String> getTagList() {
        Object tags = this.getField(Fields.TAG_LIST);
        if ((tags != null) && (tags instanceof ArrayList)) {
            ArrayList tags_list = (ArrayList) tags;
            return tags_list;
        } else {
            return new ArrayList<>();
        }
    }

    public void setTagList(ArrayList<String> list) {
        this.setField(Fields.TAG_LIST, list);
    }

    public void setGeneralFields(String[] mGeneralFields) {
        this.mGeneralFields = mGeneralFields;
    }

}
