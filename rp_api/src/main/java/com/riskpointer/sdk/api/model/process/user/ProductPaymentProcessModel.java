/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.process.user;

import android.content.Context;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.BookedProduct;
import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.api.model.entity.UserPayment;
import com.riskpointer.sdk.api.model.task.RequestTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Process model to buy a products.
 */
public class ProductPaymentProcessModel extends BasePaymentProcessModel<ProductPaymentProcessModel.ProductPayParams> {

	/**
	 * Construct new Product Payment model.
	 */
	public ProductPaymentProcessModel() {
		super();
	}

	/**
	 * Starts payment process.
	 * @param context Application context.
	 * @param shop Shop which will process payment.
	 * @param orderId Order which will process payment.
	 * @param account Payment account which will be used for the payment.
	 * @param bookProducts List of booked products.
	 */
	public void pay(Context context, Shop shop, String orderId, UserAccount account, ArrayList<BookedProduct> bookProducts) {
		pay(context, shop, orderId, account, bookProducts, 0);
	}

	/**
	 * Starts payment process.
	 * @param context Application context.
	 * @param shop Shop which will process payment.
	 * @param orderId Order which will process payment.
	 * @param account Payment account which will be used for the payment.
	 * @param bookProducts List of booked products.
	 * @param flags Flags for requests.
	 */
	public void pay(Context context, Shop shop, String orderId, UserAccount account, ArrayList<BookedProduct> bookProducts, final int flags) {
		pay(context, new ProductPayParams(shop, orderId, account, bookProducts, flags));
	}

	@Override
	protected RequestTask<UserPayment> doPayment(Context context, ProductPayParams payParams, HashMap<String, Object> additionalParams) {
		return HonkioApi.paymentRequest(
				payParams.bookProducts,
				payParams.getAccount(),
				payParams.getShop(),
				payParams.getOrderId(),
				additionalParams,
				payParams.flags,
				getCallback());
	}

	/**
	 * Product payment params.
	 */
	public static class ProductPayParams extends BasePaymentProcessModel.BasePayParams {

		/** List of booked products. */
		public final ArrayList<BookedProduct> bookProducts;

		private double mAmount;
		private String mCurrency;

		/**
		 * Construct new payment params.
		 * @param shop Shop which will process payment.
		 * @param orderId Order which will process payment.
		 * @param account Payment account which will be used for the payment.
		 * @param bookProducts List of booked products.
		 * @param flags Flags for requests.
		 */
		public ProductPayParams(Shop shop, String orderId, UserAccount account, ArrayList<BookedProduct> bookProducts, int flags) {
			super(shop, orderId, account, flags);
			this.bookProducts = bookProducts;

			Set<String> disallowedAccounts = new HashSet<>();
			for (BookedProduct bookedProduct: bookProducts) {
				disallowedAccounts.addAll(bookedProduct.getProduct().getDisallowedAccounts());

				mAmount += bookedProduct.getPrice();
				if (mCurrency == null)
					mCurrency = bookedProduct.getProduct().getCurrency();
			}
			setDisallowedAccounts(disallowedAccounts);
		}

		@Override
		public BasePaymentProcessModel<?> instantiateProcessModel() {
			return new ProductPaymentProcessModel();
		}

		@Override
		public double getAmount() {
			return mAmount;
		}

		@Override
		public String getCurrency() {
			return mCurrency;
		}
	}
}
