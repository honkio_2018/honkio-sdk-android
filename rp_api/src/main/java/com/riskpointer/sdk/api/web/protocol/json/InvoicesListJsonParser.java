package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Invoice;
import com.riskpointer.sdk.api.model.entity.InvoicesList;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.InvoiceFilter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Parse InvoicesList from JSON.
 */
public class InvoicesListJsonParser extends BaseJsonResponseParser<InvoicesList> {

    @Override
    public InvoicesList parseJson(Message message, JSONObject json) {
        InvoicesList invoicesList = new InvoicesList();

        Object nullParameter = message.getParameter(null);
        if(nullParameter instanceof InvoiceFilter)
            invoicesList.setFilter((InvoiceFilter) nullParameter);

        ArrayList<Invoice> list = new ArrayList<>();

        JSONArray invoicesListJson = json.optJSONArray("consumer_invoice");
        if (invoicesListJson != null) {
            for (int i = 0; i < invoicesListJson.length(); i++) {
                JSONObject invoiceJson = invoicesListJson.optJSONObject(i);
                if (invoiceJson != null) {
                    list.add(parseInvoice(invoiceJson));
                }
            }
        }

        invoicesList.setList(list);
        return invoicesList;
    }

    private Invoice parseInvoice(JSONObject json) {
        Invoice invoice = new Invoice();

        invoice.setId(json.optString("_id"));
        invoice.setUserId(json.optString("consumer"));
        invoice.setMerchantId(json.optString("merchant"));
        invoice.setMerchantName(json.optString("merchant_name"));
        invoice.setMerchantLogoUrl(json.optString("merchant_logo_url"));

        invoice.setInvoiceId(json.optString("invoice_id"));
        invoice.setReference(json.optString("invoice_reference"));
        invoice.setSubject(json.optString("invoice_subject"));

        invoice.setStatus(json.optString("status"));
        invoice.setDate(ApiFormatUtils.stringDateToLong(json.optString("invoice_date", null)));
        invoice.setDueDate(ApiFormatUtils.stringDateToLong(json.optString("due_date", null)));
        invoice.setPaidDate(ApiFormatUtils.stringDateToLong(json.optString("paid_date", null)));
        invoice.setAmount(json.optDouble("payable_amount"));
        invoice.setCurrency(json.optString("payable_currency"));

        JSONObject onlineLinkJson = json.optJSONObject("online-link");
        if (onlineLinkJson != null) {
            invoice.setOnlineLinkUrl(onlineLinkJson.optString("url"));
            invoice.setOnlineLinkTitle(onlineLinkJson.optString("title"));
        }

        ArrayList<Invoice.InternalContentItem> internalList = new ArrayList<>();
        ArrayList<Invoice.ExternalContentItem> externalList = new ArrayList<>();

        JSONObject comunicationsJson = json.optJSONObject("communications");
        if (comunicationsJson != null) {
            JSONArray internalContentsJson = comunicationsJson.optJSONArray("internal_contents");
            if (internalContentsJson != null) {
                for (int i = 0; i < internalContentsJson.length(); i++) {
                    JSONObject contentJson = internalContentsJson.optJSONObject(i);
                    if (contentJson != null) {
                        String title = contentJson.optString("title");
                        String content = contentJson.optString("content");
                        internalList.add(new Invoice.InternalContentItem(title, content));
                    }
                }
            }

            JSONArray externalContentsJson = comunicationsJson.optJSONArray("external_contents");
            if (externalContentsJson != null) {
                for (int i = 0; i < externalContentsJson.length(); i++) {
                    JSONObject contentJson = externalContentsJson.optJSONObject(i);
                    if (contentJson != null) {
                        String title = contentJson.optString("title");
                        String url = contentJson.optString("url");
                        externalList.add(new Invoice.ExternalContentItem(title, url));
                    }
                }
            }
        }

        invoice.setInternalContents(internalList);
        invoice.setExternalContents(externalList);

        return invoice;
    }
}
