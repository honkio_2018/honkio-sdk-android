package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.PushesList;
import com.riskpointer.sdk.api.model.push.Push;
import com.riskpointer.sdk.api.model.push.PushParser;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.ListFilter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * @author Shurygin Denis
 */

public class PushesListJsonParser extends BaseJsonResponseParser<PushesList> {

    @Override
    protected PushesList parseJson(Message<?> message, JSONObject json) {
        PushesList result = new PushesList();

        ArrayList<Push<?>> list = new ArrayList<>();

        JSONArray pushesJonArray = json.optJSONArray("notifications");
        if (pushesJonArray != null) {
            for (int i = 0; i < pushesJonArray.length(); i++) {
                JSONObject pushJson = pushesJonArray.optJSONObject(i);
                if (pushJson != null) {
                    JSONObject pushContentJson = pushJson.optJSONObject("content");
                    if (pushContentJson != null) {
                        Push<?> push = PushParser.getInstance().parse(pushContentJson);
                        if (push != null) {
                            push.setId(pushJson.optString("id"));
                            push.setMessage(pushJson.optString("subject"));
                            push.setRead(pushJson.optBoolean("read"));
                            push.setDate(ApiFormatUtils.stringDateToLong(
                                    pushJson.optString("timestamp")));
                            list.add(push);
                        }
                        else
                            list.add(dummyPush());
                    }
                    else
                        list.add(dummyPush());
                }
                else
                    list.add(dummyPush());
            }
        }

        result.setTotalCount(json.optInt("notifications_total"));
        result.setUnreadedCount(json.optInt("notifications_unread"));

        result.setList(list);
        result.setFilter((ListFilter) message.getParameter(null));
        return result;
    }

    private Push<?> dummyPush() {
        Push<?> push = new Push<>();

        push.setId("");
        push.setRead(true);
        push.setMessage("-");
        push.setDate(System.currentTimeMillis());
        push.setBody("{}");
        push.setEntity(null);

        return push;
    }
}
