package com.riskpointer.sdk.api.model.push;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class allow to parse String push message into {@code Push} object.
 *
 * @author Shurygin Denis on 2015-01-29.
 */
public abstract class PushParser {

    public static final String TYPE = "type";

    private static PushParser sInstance;

    public static void setInstance(PushParser instance) {
        if (sInstance != null)
            throw new IllegalStateException("PushParser instance already set.");
        sInstance = instance;
    }

    public static PushParser getInstance() {
        if (sInstance == null)
            sInstance = new PushParserImpl();
        return sInstance;
    }

    /**
     * Parse String message into Push object.
     *
     * @param pushString JSON string with push message.
     * @return Push object. Type of the push depend on the content of pushString.
     * @throws JSONException
     */
    public Push<?> parse(String pushString) throws JSONException {
        if (pushString == null)
            return null;

        JSONObject pushJson = new JSONObject(pushString);

        Push<?> result = parse(pushJson);

        if (result == null) {
            result = new Push<>();
        }

        result.setBody(pushString);
        parseBasePush(result, pushJson);

        return result;
    }

    public abstract Push<?> parse(JSONObject pushJson);

    private static void parseBasePush(Push push, JSONObject responseJson) {
        push.setMessage(responseJson.optString("message"));
    }
}
