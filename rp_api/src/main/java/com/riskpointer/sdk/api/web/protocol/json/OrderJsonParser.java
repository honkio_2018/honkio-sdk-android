/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.BookedProduct;
import com.riskpointer.sdk.api.model.entity.Order;
import com.riskpointer.sdk.api.model.entity.Product;
import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.api.model.entity.UserRoleDescription;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

// TODO docs
public class OrderJsonParser extends BaseJsonResponseParser<Order> {

    @Override
    public Order parseJson(Message<?> message, JSONObject json) {
        return parseOrder(json);
    }

    public static Order parseOrder(JSONObject json) {

        Order order = new Order();

        order.setStatus(json.optString("order_status"));
        order.setModel(json.optString("model"));

        order.setUserOwner(json.optString("user_owner"));
        order.setOrderId(json.optString("order_id"));

        order.setTitle(json.optString("title"));
        order.setDescription(json.optString("description"));

        order.setCreationDate(ApiFormatUtils.stringDateToLong(json.optString("creation_date")));
        order.setExpireDate(ApiFormatUtils.stringDateToLong(json.optString("expire_date")));
        order.setCompletionDate(ApiFormatUtils.stringDateToLong(json.optString("completion_date")));
        order.setStartDate(ApiFormatUtils.stringDateToLong(json.optString("start_date")));
        order.setEndDate(ApiFormatUtils.stringDateToLong(json.optString("end_date")));


        order.setAsset(json.optString("asset"));
        order.setParentOrderId(json.optString("parent_order_id"));

        JSONArray productsJsonArray = json.optJSONArray("products");
        if (productsJsonArray != null) {
            ArrayList<BookedProduct> products = new ArrayList<>(productsJsonArray.length());
            for (int i = 0; i < productsJsonArray.length(); i++) {
                JSONObject productJson = productsJsonArray.optJSONObject(i);
                if (productJson != null) {
                    Product product = new Product(jsonToMap(productJson));
                    ParseHelper.parseProduct(productJson, product);
                    products.add(new BookedProduct(product, productJson.optInt("count")));
                }
            }
            order.setProducts(products);
        }
        if (order.getAmount() == 0) //order amount can be taken from products above, if not, take from 'amount'
            order.setAmount(json.optDouble("amount"));
        if (order.getCurrency() == null || order.getCurrency().isEmpty())
            order.setCurrency(json.optString("currency"));

        JSONArray rolesJsonArray = json.optJSONArray("roles");
        if (rolesJsonArray != null) {
            List<UserRoleDescription> roles = new ArrayList<>(rolesJsonArray.length());
            for (int i = 0; i < rolesJsonArray.length(); i++) {
                JSONObject roleJson = rolesJsonArray.optJSONObject(i);
                if (roleJson != null) {
                    roles.add(UserRoleDescriptionJsonParser.parseRoleDescription(roleJson));
                }
            }
            order.setLimitedRoles(roles);
        }

        order.setLatitude(json.optDouble("latitude"));
        order.setLongitude(json.optDouble("longitude"));

        order.setThirdPerson(json.optString("third_person"));
        if ("None".equals(order.getThirdPerson()))
            order.setThirdPerson(null);

        JSONObject shopJson = json.optJSONObject("shop");
        if (shopJson != null) {
            order.setShopId(shopJson.optString("id"));
            order.setShopName(shopJson.optString("name"));
            order.setShopReceipt(shopJson.optString("receipt"));
            order.setShopReference(shopJson.optString("reference"));
        }
        else {
            order.setShopId(json.optString("shop_id"));
        }

        JSONObject customFields = json.optJSONObject("custom_fields");
        if (customFields != null) {
            order.setCustomFields(jsonToMap(customFields));
        }

        String accountType = json.optString("account_type");
        String accountNumber = json.optString("account_number");
        if (accountType != null && !"None".equals(accountType) && accountNumber != null) {
            UserAccount account = null;
            User user = HonkioApi.getActiveUser();
            if (user != null) {
                for (UserAccount userAccount: user.getAccountsList()) {
                    if (accountType.equals(userAccount.getType()) && accountNumber.equals(userAccount.getNumber())) {
                        account = userAccount;
                        break;
                    }
                }
            }
            if (account == null)
                account = new UserAccount(accountType, accountNumber);

            order.setAccount(account);
        }

        order.setUnreadChatMessagesCount(json.optInt("unread_chat_messages"));
        return order;
    }
}
