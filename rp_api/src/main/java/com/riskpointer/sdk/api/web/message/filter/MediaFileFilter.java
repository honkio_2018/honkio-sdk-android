package com.riskpointer.sdk.api.web.message.filter;

import android.text.TextUtils;

import com.riskpointer.sdk.api.web.message.Message;

import java.util.HashMap;

/**
 * Created by I.N. on 2017-02-16.
 */
public class MediaFileFilter extends ListFilter {
    private static final long serialVersionUID = -6268418508519948686L;
    private String mMediaType;
    private String mObjectId;
    private String mAccess;
    private PropertiesFilter mPropertiesFilter;
    private Boolean mIsParentOrder;
    private String[] mStatuses;
    private String mModel;

    /**
     * gets type of media files
     *
     * @return
     */
    public String getMediaType() {
        return mMediaType;
    }

    /**
     * sets type of media files, allows 'image' (png, jpg, jpeg); 'document' (txt, rtf, pdf); 'video' (mp4, flv)
     *
     * @param type
     */
    public void setMediaType(String type) {
        this.mMediaType = type;
    }

    public String getObjectId() {
        return mObjectId;
    }

    public void setObjectId(String objectId) {
        mObjectId = objectId;
    }

    public String getAccess() {
        return mAccess;
    }

    public void setAccess(String access) {
        mAccess = access;
    }

    public PropertiesFilter getPropertiesFilter() {
        return mPropertiesFilter;
    }

    public void setPropertiesFilter(PropertiesFilter filter) {
        this.mPropertiesFilter = filter;
    }

    public Boolean isParentOrder() {
        return mIsParentOrder;
    }

    /**
     * set this true to load all media including child orders media
     * and set object id = parent order id
     * @param parentOrder
     */
    public void setParentOrder(Boolean parentOrder) {
        mIsParentOrder = parentOrder;
    }

    public String getModel() {
        return mModel;
    }

    /**
     * used when loading all media from parent and child order
     * @param mModel order model of child orders
     */
    public void setModel(String mModel) {
        this.mModel = mModel;
    }

    public String[] getStatuses() {
        return mStatuses;
    }

    /**
     * used when loading all media from parent and child orders
     * @param statuses list of child orders statuses
     */
    public void setStatus(String[] statuses) {
        this.mStatuses = statuses;
    }

    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = super.toParams();
        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_TYPE, getMediaType());
        AbsFilter.applyIfNotNull(params, "object", getObjectId());
        AbsFilter.applyIfNotNull(params, "access", getAccess());
        AbsFilter.applyIfNotNull(params, "is_parent_order", isParentOrder());
        AbsFilter.applyIfNotNull(params, "query_model", getModel());
        if (mPropertiesFilter != null)
            AbsFilter.applyIfNotNull(params, "query_properties", mPropertiesFilter.toParams());
        if (getStatuses() != null)
            applyIfNotNull(params, Message.Param.QUERY_STATUS, TextUtils.join("|", getStatuses()));
        return params;
    }
}
