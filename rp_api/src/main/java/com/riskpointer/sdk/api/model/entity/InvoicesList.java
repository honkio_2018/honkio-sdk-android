package com.riskpointer.sdk.api.model.entity;

import com.riskpointer.sdk.api.web.message.filter.InvoiceFilter;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Data object that hold list of invoices. Also this class contain a Filter which was used to get the list.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 *
 * @author KamiSempai on 2015-03-27.
 */
public class InvoicesList implements Serializable{

    private InvoiceFilter mFilter;
    private ArrayList<Invoice> mList;

    /**
     * Gets list of Invoices.
     * @return List of Invoices.
     */
    public ArrayList<Invoice> getList() {
        return mList;
    }

    /**
     * Sets list of Invoices.
     * @param list List of Invoices.
     */
    public void setList(ArrayList<Invoice> list) {
        this.mList = list;
    }

    /**
     * Gets the Filter which was used to get Invoices list.
     * @return Filter which was used to get Invoices list.
     */
    public InvoiceFilter getFilter() {
        return mFilter;
    }

    /**
     * Sets the Filter which was used to get Invoices list.
     * @param filter Filter which was used to get Invoices list.
     */
    public void setFilter(InvoiceFilter filter) {
        this.mFilter = filter;
    }
}
