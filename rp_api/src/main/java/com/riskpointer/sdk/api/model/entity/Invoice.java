package com.riskpointer.sdk.api.model.entity;

import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.AbsFilter;
import com.riskpointer.sdk.api.utils.Currency;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Data object that hold information about invoice. Also this class extent {@code AbsFilter} and can be used like a Filter.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 *
 * @author Shurygin Denis on 2015-03-27.
 */
@Deprecated
public class Invoice extends AbsFilter implements Serializable {

    /**
     * Available statuses of Invoice.
     */
    public static final class Status {
        public static final String NEW = "new";
        public static final String PAID = "paid";
        public static final String OVERDUE = "overdue";
    }

    private String id;
    private String userId;
    private String merchantId;
    private String merchantName;
    private String merchantLogoUrl;

    private String invoiceId;
    private String reference;
    private String subject;

    private String status;
    private long date;
    private long dueDate;
    private long paidDate;
    private Double amount;
    private String currency;

    private String onlineLinkUrl;
    private String onlineLinkTitle;

    private ArrayList<InternalContentItem> internalContents;
    private ArrayList<ExternalContentItem> externalContents;

    /**
     * Getting RiskPointer Invoice ID. Use this ID to work with Invoice on RiskPointer Server.
     * @return RiskPointer Invoice ID.
     */
    public String getId() {
        return id;
    }

    /**
     * Set RiskPointer Invoice ID. This ID used to work with Invoice on RiskPointer Server.
     * @param id RiskPointer Invoice ID.
     */
    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantLogoUrl() {
        return merchantLogoUrl;
    }

    public void setMerchantLogoUrl(String merchantLogoUrl) {
        this.merchantLogoUrl = merchantLogoUrl;
    }

    /**
     * Getting real Invoice ID. This ID shouldn't be used to work with Invoice on RiskPointer Server, use ID from getId method instead.
     * @return Real Invoice ID.
     */
    public String getInvoiceId() {
        return invoiceId;
    }

    /**
     * Set real Invoice ID. This ID shouldn't be used to work with Invoice on RiskPointer Server, use ID from getId method instead.
     * @param invoiceId Real Invoice ID.
     */
    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public long getDueDate() {
        return dueDate;
    }

    public void setDueDate(long dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * Check if due date less current time.
     *
     * @return True if due date less current time, false otherwise.
     */
    public boolean isOverDue() {
        return isOverDue(System.currentTimeMillis());
    }

    /**
     * Check if due date less specified time.
     *
     * @param time Time millis.
     * @return True if due date less specified time, false otherwise.
     */
    public boolean isOverDue(long time) {
        return getDueDate() < time;
    }

    public long getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(long paidDate) {
        this.paidDate = paidDate;
    }

    /**
     * Getting amount of money that this Invoice require.
     * @return Amount of money that this Invoice require.
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * Set amount of money that this Invoice require.
     * @param amount Amount of money that this Invoice require.
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * Getting currency of the Invoice.
     * @return Currency of the Invoice.
     * @see Currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Set currency of the Invoice.
     * @param currency Currency of the Invoice.
     * @see Currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * Getting URL of the Invoice web page.
     * @return URL of the Invoice web page.
     */
    public String getOnlineLinkUrl() {
        return onlineLinkUrl;
    }

    /**
     * Set URL of the Invoice web page.
     * @param onlineLinkUrl URL of the Invoice web page.
     */
    public void setOnlineLinkUrl(String onlineLinkUrl) {
        this.onlineLinkUrl = onlineLinkUrl;
    }

    public String getOnlineLinkTitle() {
        return onlineLinkTitle;
    }

    public void setOnlineLinkTitle(String onlineLinkTitle) {
        this.onlineLinkTitle = onlineLinkTitle;
    }

    public ArrayList<InternalContentItem> getInternalContents() {
        return internalContents;
    }

    public void setInternalContents(ArrayList<InternalContentItem> internalContents) {
        this.internalContents = internalContents;
    }

    public ArrayList<ExternalContentItem> getExternalContents() {
        return externalContents;
    }

    public void setExternalContents(ArrayList<ExternalContentItem> externalContents) {
        this.externalContents = externalContents;
    }

    /**
     * Check if status of Invoice is NEW.
     * @return True if status of Invoice is NEW, false otherwise.
     */
    public boolean isStatusNew() {
        return Status.NEW.equals(status);
    }

    /**
     * Check if status of Invoice is PAID.
     * @return True if status of Invoice is PAID, false otherwise.
     */
    public boolean isStatusPaid() {
        return Status.PAID.equals(status);
    }

    /**
     * Check if status of Invoice is OVERDUE.
     * @return True if status of Invoice is OVERDUE, false otherwise.
     */
    public boolean isStatusOverdue() {
        return Status.OVERDUE.equals(status);
    }

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = new HashMap<>();

        applyIfNotNull(params, "invoice", getId());
        AbsFilter.applyIfNotNull(params, Message.Param.ACCOUNT_AMOUNT, amountToString(getAmount()));
        applyIfNotNull(params, Message.Param.ACCOUNT_CURRENCY, getCurrency());

        return params;
    }

    @Deprecated
    public static class InternalContentItem implements Serializable{
        public final String title;
        public final String content;

        public InternalContentItem(String title, String content) {
            this.title = title;
            this.content = content;
        }
    }

    @Deprecated
    public static class ExternalContentItem implements Serializable{
        public final String title;
        public final String url;

        public ExternalContentItem(String title, String url) {
            this.title = title;
            this.url = url;
        }
    }
}
