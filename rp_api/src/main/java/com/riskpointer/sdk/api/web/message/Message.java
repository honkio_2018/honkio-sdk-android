/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.message;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.text.TextUtils;

import com.riskpointer.sdk.api.BaseHonkioApi;
import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.exception.WrongHashException;
import com.riskpointer.sdk.api.model.entity.HKDevice;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.support.BroadcastManager;
import com.riskpointer.sdk.api.utils.LocationFinder;
import com.riskpointer.sdk.api.web.Connection;
import com.riskpointer.sdk.api.web.protocol.RequestBuilder;
import com.riskpointer.sdk.api.web.protocol.ResponseParser;
import com.riskpointer.sdk.api.web.response.Response;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * This class contains all requirement information to send requests and decode responses.
 *
 * @author Denis Shurygin
 *
 * @param <Resp> Type of the response.
 */
public class Message<Resp> {
    /** Sending offline request. Use this flag for getting last online response from the cache.*/
    public static final int FLAG_OFFLINE_MODE = 0x00000001;
    /** Sending online request. Use this flag when offline mode turned on. FLAG_OFFLINE_MODE will be ignored*/
    public static final int FLAG_ONLINE_MODE = 0x00000002;
//	/** This flag invoke offline and then online request. Note: If this flag used response will return twice.*/
//	public static final int FLAG_OFFLINE_FIRST = 0x00000080;
    /** Don't write response into cache.*/
    public static final int FLAG_NO_CACHE = 0x00000004;
    /** Don't send Broadcast messages for unhandled errors.*/
    public static final int FLAG_NO_ERROR_WARNING = 0x00000010;
    /** Don't send Broadcast messages for unhandled responses.*/
    public static final int FLAG_NO_RESPONSE_WARNING = 0x00000020;
	/** Don't send Broadcast messages for unhandled responses and errors.*/
	public static final int FLAG_NO_WARNING = FLAG_NO_ERROR_WARNING | FLAG_NO_RESPONSE_WARNING;
    /** Request will be executed in the Low priority queue.*/
    public static final int FLAG_LOW_PRIORITY = 0x00000040;
	/** Request will be executed in the High priority queue.*/
	public static final int FLAG_HIGH_PRIORITY = 0x00000080;


	/** Prefix for identity data. Specified for each platform. */
	private static final String IDENTITY_PREFIX = "dA";
	/** Length of the random string. */
	private static final int RANDOM_LENGTH = 40;

	/**
	 * Available commands of the message.
	 */
	public final class Command {
		public static final String SERVER_INFO = "serverinfo";
		public static final String SERVER_GET_TOU = "servergettou";
		public static final String APP_INFO = "appinfo";
		public static final String SHOP_FIND = "shopfind";
		public static final String SHOP_INFO = "shopinfo";
		public static final String SHOP_STAMP = "shopstamp";
		public static final String SHOP_GET_PRODUCTS = "shopgetproducts";
		public static final String SHOP_TRANSACTIONS = "shoptransaction";
		public static final String GET_SPLIT_SCHEDULE = "getsplitschedule";
		/** Created for Happs application */
		public static final String SHOP_GET_RATE = "shopgetrate";
		public static final String MERCHANT_ROLER = "getmerchantroles";
		public static final String GET_SCHEDULE = "getschedule";
		public static final String GET_SCHEDULE_PRODUCTS = "getscheduleproducts";
		public static final String SET_DEVICE = "setdevice";
		public static final String LOGOUT = "logout";


		public static final String TRANSACTION_STATUS_SET = "statusset";

		public static final String USER_GET = "consumerget";
		public static final String USER_SET = "consumerset";
		public static final String USER_TRANSACTION = "consumertransaction";
		public static final String USER_PAYMENT = "consumerpayment";
		public static final String USER_INVOICE = "consumerinvoice";
		public static final String USER_GET_FILE = "consumergetfile";
		public static final String USER_DISPUTE = "consumerdispute";
		public static final String USER_GET_ORDERS = "usergetorders";
		public static final String USER_SET_ORDER = "usersetorder";
		public static final String USER_SET_ORDER_APPLICATION = "usersetapplicant";
		public static final String USER_APPLY_FOR_ORDER = "userapplyfororder";
		public static final String USER_GET_APPLICANTS = "usergetapplicants";
		public static final String USER_GET_ROLES = "usergetroles";
		public static final String USER_GET_ROLE = "usergetrole";
		public static final String USER_SET_ROLE = "usersetrole";
		public static final String USER_SET_PHOTO = "usersetphoto";
		public static final String USER_SET_MEDIA = "userfileset";
		public static final String USER_GET_MEDIA_FILES_LIST = "userfileslist";
		public static final String USER_GET_CHAT = "usergetchat";
		public static final String USER_SEND_CHAT_MSG = "usersendchatmsg";
		public static final String USER_GET_COMMENTS = "usergetcomments";
		public static final String USER_VOTE = "uservote";
		public static final String USER_GET_RATE = "getrate";
		public static final String USER_GET_TAGS = "usergetproducttags";
		public static final String USER_INVENTORY_LIST = "userinventorylist";
		public static final String USER_INVENTORY_CONVERT = "userinventoryconvert";
		public static final String USER_GET_NOTIFICATIONS = "usergetnotifications";
		public static final String USER_GET_LAUNCHPAD_INFO = "usergetlaunchpadinfo";
		public static final String USER_CREATE_INVITES_COPY = "usercreateinvitescopy";
		public static final String USER_SET_NOTIFICATION =  "usersetnotification";
		public static final String USER_DONATE = "userdonate";
		public static final String USER_ACCEPT_INVITATION = "useracceptinvitation";
		public static final String USER_GET_ASSET = "usergetasset";
		public static final String USER_SET_ABUSE = "usersetabuse";
		public static final String USER_FORGET_ME = "userforgetme";
		public static final String USER_ROLES_DESCRIPTION = "usergetroledescriptions";
		public static final String USER_SEND_INVITES = "usersendinvites";
		public static final String USER_INVITE_LIST = "userinviteslist";
		public static final String USER_SET_INVITE = "usersetinvite";
		public static final String USER_GET_INVITEES = "usergetinvitees";
		public static final String USER_ACCEPT_TOU = "useraccepttou";

		public static final String USER_POLLS_LIST = "userpollslist";
		public static final String USER_POLL_VOTE = "userpollvote";

		@Deprecated
		public static final String USER_SHOP_PRODUCTS = "consumershopproduct";

		public static final String LOST_PASSWORD = "consumerlostpassword";
		public static final String QUERY = "query";
		public static final String BATCH = "batch";

		public static final String USER_ASSET_LIST = "userassetlist";

		public static final String USER_ACCESS_LIST = "useraccesslist";

		@Deprecated
		public static final String USER_ASSET_STRUCTURE = "userassetstructurelist";

		public static final String QR_INFO = "qrinfo";
	}

	/**
	 * Available parameters of the message.
	 */
	public final class Param {
		public static final String ID = "id";
		public static final String VERSION = "version";
		public static final String COMMAND = "command";
		public static final String RANDOM = "random";
		public static final String HASH = "hash";
		public static final String IDENTITY_PLUGIN = "identity_plugindata";
		public static final String IDENTITY_CLIENT = "identity_client";
		public static final String SHOP_ID = "shop";
		public static final String SHOP_ID_FILTER = "shopid";
		public static final String SHOP_QR_ID_FILTER = "qrid";
		public static final String SHOP_SERVICE_TYPE = "service_type";
		public static final String SHOP_QUERY_MERCHANT = "query_merchant";
		public static final String SHOP_QUERY_SUBMERCHANTS = "query_submerchants";
		public static final String QUERY_CHILD_MERCHANTS = "query_child_merchants";
		public static final String SHOP_QUERY_ALL_MERCHANTS = "query_all_merchants";
		public static final String SHOP_QUERY_SHOW_RATINGS = "query_show_ratings";
		public static final String SHOP_NETWORK_ID = "network";
		public static final String SHOP_REFERENCE = "shop_reference";
		public static final String LOGIN_IDENTITY = "login_identity";
		public static final String LOGIN_PASSWORD = "login_password";
		public static final String USER_LOGIN = "user_login";
		public static final String AUTH_TOKEN = "access_token";
		public static final String LOC_LATITUDE = "html_latitude";
		public static final String LOC_LONGITUDE = "html_longitude";
		public static final String LOC_ACCURACY = "html_accuracy";
		public static final String QUERY_VALID = "query_valid";
		public static final String QUERY_ID = "query_id";
		public static final String QUERY_COUNT = "query_count";
		public static final String QUERY_SKIP = "query_skip";
		public static final String QUERY_TIMESTAMP = "query_timestamp";
		public static final String QUERY_LANGUAGE = "query_language";
		public static final String QUERY_STATUS = "query_status";
		public static final String QUERY_ORDER = "query_order";
		public static final String QUERY_OWNER = "query_owner";
		public static final String QUERY_THIRD_PERSON = "query_third_person";
		public static final String QUERY_EXCLUDE_EXPIRED = "query_exclude_expired";
		public static final String QUERY_START_DATE = "query_start_date";
		public static final String QUERY_END_DATE = "query_end_date";
		public static final String QUERY_TYPE = "query_type";
		public static final String QUERY_USED = "query_used";
		public static final String QUERY_RECEIVER_ID = "query_receiver_id";
		public static final String QUERY_RECEIVER_PHONE = "query_receiver_phone";
		public static final String QUERY_RECEIVER_EMAIL = "query_receiver_email";
		public static final String QUERY_PARENT_ORDER_ID = "query_parent_order_id";
		public static final String QUERY_ROLE_ID = "query_role_id";
		public static final String QUERY_ORDER_DETAILS = "query_order_details";
		public static final String QUERY_PRODUCT_DETAILS = "query_product_details";
		public static final String QUERY_CONTACTS_DICT = "query_contacts_dict";
		public static final String QUERY_SECRET_CODE = "query_secret_code";
		public static final String QUERY_PERIOD = "query_period";
		public static final String QUERY_MODEL = "query_model";
		public static final String PRODUCT = "product";
		public static final String ORDER = "order";
		public static final String ORDER_ID = "order_id";
		public static final String PARENT_ORDER_ID = "parent_order_id";
		public static final String ORDER_STATUS = "status";
		public static final String APPLICATION_ID = "application_id";
		public static final String NEW_ORDER = "new_order";
		public static final String ACCOUNT_TYPE = "account_type";
		public static final String ACCOUNT_NUMBER = "account_number";
		public static final String ACCOUNT_AMOUNT = "account_amount";
		public static final String ACCOUNT_CURRENCY = "account_currency";
		public static final String USER_EMAIL = "consumer_str_email";
		public static final String USER_PASSWORD = "consumer_str_password";
		public static final String USER_DICT_KEYSTORE = "consumer_dict_keystore";
		public static final String USER_REGISTRATION = "consumer_registration";
		public static final String USER_RE_REGISTRATION = "consumer_reregistration";
		public static final String USER_TOU_KEY = "tou_key";
		public static final String USER_INVOICERS = "consumer_invoicers";
		public static final String DEVICE_APP = "app";
		public static final String DEVICE_TOKEN = "token";
		public static final String DEVICE_TYPE = "type";
		public static final String DEVICE_NAME = "name";
		public static final String DEVICE_NOTIFY = "notify";
		public static final String DEVICE_MANUFACTURER = "manufacturer";
		public static final String DEVICE_MODEL = "model";
		public static final String DEVICE_OS_VERSION = "os_version";
		public static final String DISPUTE_TEXT = "text";
		public static final String STATUS = "status";
		public static final String LANGUAGE = "language";
	}

	/** Static lock for asynchronous operations. */
	private static final String IDENTITY_CLIENT_SEPARATOR = ";";
	private static final String EMPTY_IDENTITY_PLUGIN = "NONE";

	private static String sIdentityClientCache;

	private final String mCommand;
    private final String mUrl;
    private final HKDevice mDevice;
	private final Connection mConnection;
	private final RequestBuilder mRequestBuilder;
	private final ResponseParser<Resp> mResponseParser;
	private final Map<String, Object> mParameters = new HashMap<>();
	private Identity mAppIdentity;
	private Identity mShopIdentity;
	private int mFlags;
	private String mBroadcastCommand;
	private String mCacheFileName;
    private boolean isCacheDependsOnShop = false;
	private boolean isSupportOffline = true;
	private boolean isIgnoreHash = false;

	private SoftReference<Context> mContext;

	/**
	 * Construct new Message.
	 *
	 * @param command        Command of the message. See {@link Command}.
	 * @param url            Server url.
	 * @param connection     User authentication data.
	 * @param device         Device info.
	 * @param shopIdentity   Shop that will used for the request.
	 * @param requestBuilder Builder for creating requests.
	 * @param responseParser Builder for decode response.
	 */
	public Message(Context context, String command, String url, Connection connection,
				   HKDevice device, Identity shopIdentity, Identity appIdentity,
				   RequestBuilder requestBuilder, ResponseParser<Resp> responseParser, int flags) {
		if (context == null)
			throw new IllegalArgumentException("Context must be not null");
		if (command == null)
			throw new IllegalArgumentException("Command must be not null");
		if (requestBuilder == null)
			throw new IllegalArgumentException("RequestBuilder must be not null");
		if (responseParser == null)
			throw new IllegalArgumentException("ResponseParser must be not null");

		mContext = new SoftReference<>(context.getApplicationContext());
		mCommand = command;
		mUrl = url;
		mConnection = connection;
		mDevice = device;
		mShopIdentity = shopIdentity;
		mAppIdentity = appIdentity;

		mRequestBuilder = requestBuilder;
		mResponseParser = responseParser;
		mFlags = flags;
	}

	static String generateRandomString(int length) {
		Random generator = new Random();
		StringBuilder randomStringBuilder = new StringBuilder();
		char tempChar;
		for (int i = 0; i < length; i++) {
			tempChar = (char) (generator.nextInt(93) + 35);
			randomStringBuilder.append(tempChar);
		}
		return randomStringBuilder.toString();
	}

	/**
	 * Add a parameter for message.
	 *
	 * @param name
	 *            Name of parameter. Can be null but in this case parameter will not used in the request.
	 * @param value
	 *            Value of parameter.
	 */
	public void addParameter(String name, Object value) {
		if(value == null)
			value = "";
		mParameters.put(name, value);
	}

	/**
	 * Add a map of parameters for message.
	 * @param params
	 *            Map of parameters.
	 */
	public void addParameters(Map<String, Object> params) {
		if(params != null)
			mParameters.putAll(params);
	}

	/**
	 * Gets parameter from message.
	 *
	 * @param name
	 *            Name of parameter.
	 * @return Value of parameter or {@code null} if parameter doesn't exists.
	 */
	public Object getParameter(String name) {
		return mParameters.get(name);
	}

	/**
	 * Remove parameter from message by name.
	 *
	 * @param name
	 *            Name of the removed parameter.
	 */
	public void removeParameter(String name) {
		mParameters.remove(name);
	}

	/**
	 * Parse response object from response string.
	 *
	 * @param responseString Response string.
	 * @return Response object.
	 * @throws WrongHashException If server return response with wrong hash.
	 */
	public Response<Resp> parseResponse(String responseString) throws WrongHashException {
		Response<Resp> response = mResponseParser.parse(this, responseString, isIgnoreHash ? null : getHashPassword());
		if (response != null) {
			Identity shopIdentity = getShopIdentity();
			if (shopIdentity != null)
				Response.Editor.setShopId(response, shopIdentity.getId());
			response.setMessage(this);
		}
		return response;
	}

	/**
	 * Gets message command.
	 * @return Command of the message. See {@link Command}.
	 */
	public String getCommand() {
		return mCommand;
	}

	/**
	 * Gets API url.
	 * @return API url set fot this message.
	 */
    public String getUrl() {
        return mUrl;
    }

	/**
	 * Gets Connection used in the message.
	 * @return Connection used in the message. Returns null if Connection is invalid.
	 */
	public Connection getConnection() {
		return (mConnection != null && mConnection.isValid()) ? mConnection : null;
	}

	/**
	 * Gets shop Identity of the message.
	 * @return Shop Identity that used for the request.
	 */
	public Identity getShopIdentity() {
		if (mShopIdentity == null)
			return BaseHonkioApi.getMainShopIdentity();//FIXME Remove dependency from BaseHonkioApi
		return mShopIdentity;
	}

	/**
	 * Sets specified shop Identity for the message.
	 * @param identity Shop Identity that will used for the request.
	 */
	public void setShopIdentity(Identity identity) {
		mShopIdentity = identity;
	}

	/**
	 * Gets app Identity of the message.
	 * @return App Identity that used for the request.
	 */
	public Identity getAppIdentity() {
		return mAppIdentity;
	}

	/**
	 * Gets name of the cache file without extension.
	 * @return Name of the file that used for caching response. If null cache for this message will not created.
	 */
	public String getCacheFileName() {
        if (mCacheFileName != null && isCacheDependsOnShop && getShopIdentity() != null && getShopIdentity().getId() != null)
		    return mCacheFileName + "_" + getShopIdentity().getId();
        return mCacheFileName;
	}

	/**
	 * Sets name of the cache file without extension.
	 * @param cacheFileName Name of the file that will used for caching response. If null cache for this message will not created.
     * @param cacheDependsOnShop If true, shop id will be attached to cache file name.
	 */
	public void setCacheFileName(String cacheFileName, boolean cacheDependsOnShop) {
		this.mCacheFileName = cacheFileName;
        this.isCacheDependsOnShop = cacheDependsOnShop;
	}

    /**
	 * Gets message flags.
     * @return Message flags.
     */
	public int getFlags() {
		return mFlags;
	}

    /**
     * Set Message flags.
     * @param flags Message flags.
     */
	public void setFlags(int flags) {
		this.mFlags = flags;
	}

    /**
     * Check if message has specified flag.
     * @param flag request flag.
     * @return true if message has flag, false otherwise;
     */
    public boolean isHasFlag(int flag) {
        return (mFlags & flag) == flag;
    }

    /**
     * Check if message should be executed offline.
     * @return true if is offline message, false otherwise.
	 *
	 * @see #FLAG_ONLINE_MODE
	 * @see #FLAG_OFFLINE_MODE
     */
	public boolean isOffline() {
		return ((mFlags & FLAG_ONLINE_MODE) != FLAG_ONLINE_MODE) &&
				((mFlags & FLAG_OFFLINE_MODE) == FLAG_OFFLINE_MODE);
	}

	/**
	 * Gets command name that will used for sending local broadcast. By default used command of the message.
	 * @return Command name. By default is command of the message.
	 */
	public String getBroadcastCommand() {
		if(mBroadcastCommand != null)
			return mBroadcastCommand;
		return mCommand;
	}

	/**
	 * Sets specified command name that will used for sending local broadcast. By default used command of the message.
	 * @param command Command name.
	 * @see BroadcastManager
	 */
	public void setBroadcastCommand(String command) {
		mBroadcastCommand = command;
	}

	/**
	 * Check if message support offline execution.
	 * @return True if message support offline, false otherwise. Default value is true.
	 */
	public boolean isSupportOffline() {
		return isSupportOffline;
	}

	/**
	 * Set if this message support offline requests.
	 * @param isSupportOffline True if message support offline, false otherwise.
	 */
	public void setSupportOffline(boolean isSupportOffline) {
		this.isSupportOffline = isSupportOffline;
	}

	/**
	 * Sets if hash should be ignored in this request.
	 * @param ignore True if should be ignored in this request, false otherwise.
	 */
	public void setHashIgnore(boolean ignore) {
		isIgnoreHash = ignore;
	}

	public Map<String, Object> buildRequestMap() {
		Map<String, Object> request = new HashMap<>();

		Identity shopIdentity = getShopIdentity();

		if(shopIdentity != null && shopIdentity.getId() != null)
			request.put(Param.SHOP_ID, shopIdentity.getId());

		if(mConnection != null) {
			request.put(Param.LOGIN_IDENTITY, mConnection.getLogin());
			request.put(Param.LOGIN_PASSWORD, mConnection.getOtp());
		}

		Location location = HonkioApi.getLocation();
		if (location != null) {
			request.put(Param.LOC_LATITUDE, LocationFinder.formatLocation(location.getLatitude()));
			request.put(Param.LOC_LONGITUDE, LocationFinder.formatLocation(location.getLongitude()));
			if (location.hasAccuracy())
				request.put(Param.LOC_ACCURACY, Integer.toString((int) location.getAccuracy()));
		}

		request.put(Param.LANGUAGE, HonkioApi.getSuitableLanguage());
		request.put(Param.VERSION, BaseHonkioApi.VERSION);
		request.put(Param.RANDOM, generateRandomString(RANDOM_LENGTH));
		request.put(Param.IDENTITY_CLIENT, generateIdentityClient(mContext.get()));

		if (!mCommand.equals(Command.SET_DEVICE) &&
				mDevice != null && !TextUtils.isEmpty(mDevice.getId()))
			if (!EMPTY_IDENTITY_PLUGIN.equals(mDevice.getId()))
				request.put(Param.IDENTITY_PLUGIN, mDevice.getId());

		request.putAll(mParameters);
		request.put(Param.COMMAND, mCommand);

		return request;
	}

	public Map<String, Object> buildRequestMapWithHash() {
		return putHash(buildRequestMap());
	}

	public Map<String, Object> putHash(Map<String, Object> map) {
		String hashPassword = getHashPassword();
		if (!TextUtils.isEmpty(hashPassword))
			map.put(Message.Param.HASH, mRequestBuilder.calculateHash(map, hashPassword));

		return map;
	}

	/**
	 * Build request string.
	 * @return String of the request.
	 */
	public String buildRequest() {
		return mRequestBuilder.build(buildRequestMapWithHash());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (obj == null)
			return false;

		if (getClass() != obj.getClass())
			return false;

		if (getClass().getGenericSuperclass() != obj.getClass().getGenericSuperclass())
			return false;

		Message<?> message = (Message<?>) obj;
		return getClass().getGenericSuperclass()
					.equals(message.getClass().getGenericSuperclass())
				&& TextUtils.equals(mCommand, message.mCommand)
				&& TextUtils.equals(mUrl, message.mUrl)
				&& TextUtils.equals(mBroadcastCommand, message.mBroadcastCommand)
				&& TextUtils.equals(mCacheFileName, message.mCacheFileName)
				&& (mShopIdentity != null && mShopIdentity.equals(message.mShopIdentity)
					|| mShopIdentity == null && message.mShopIdentity == null)
				&& (mAppIdentity != null && mAppIdentity.equals(message.mAppIdentity)
					|| mAppIdentity == null && message.mAppIdentity == null)
				&& mConnection == message.mConnection
				&& mFlags == message.mFlags
				&& isCacheDependsOnShop == message.isCacheDependsOnShop
				&& isSupportOffline == message.isSupportOffline
				&& isIgnoreHash == message.isIgnoreHash
				&& mRequestBuilder.equals(message.mRequestBuilder)
				&& mResponseParser.equals(message.mResponseParser)
				&& mParameters.equals(message.mParameters);
	}

	protected String getHashPassword() {

		Identity shopIdentity = getShopIdentity();

		StringBuilder passwordBuilder = new StringBuilder();

		if(shopIdentity != null && shopIdentity.getPassword() != null)
			passwordBuilder.append(shopIdentity.getPassword());

		if (mAppIdentity != null && mAppIdentity.getPassword() != null)
			passwordBuilder.append(mAppIdentity.getPassword());

		return passwordBuilder.toString();
	}

	private String generateIdentityClient(Context context) {
		if (sIdentityClientCache == null) {
			StringBuilder builder = new StringBuilder();
			builder.append(mAppIdentity.getId()).append(IDENTITY_CLIENT_SEPARATOR);
			builder.append(BaseHonkioApi.PLATFORM).append(IDENTITY_CLIENT_SEPARATOR);

			PackageInfo packageInfo = null;
			try {
				packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			} catch (PackageManager.NameNotFoundException e) {
				e.printStackTrace();
			}
			if (packageInfo != null)
				builder.append(packageInfo.versionName);
			else
				builder.append("0.00.0000");

			sIdentityClientCache = builder.toString();
		}
		return sIdentityClientCache;
	}
}
