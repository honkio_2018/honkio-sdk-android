package com.riskpointer.sdk.api.merchant;

import android.content.Context;

import com.riskpointer.sdk.api.BaseHonkioApi;
import com.riskpointer.sdk.api.merchant.protocol.json.MerchantShopsListJsonParserOld;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.SimpleListItem;
import com.riskpointer.sdk.api.web.WebServiceAsync;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.message.Message;

import java.util.ArrayList;

/**
 * Extension of BaseHonkioApi to work with the merchant.
 *
 * @author Shurygin Denis on 2014-12-04.
 */
@Deprecated
public class HonkioApiMerchantOld extends BaseHonkioApi {

    /**
     * The request to load shops list of the merchant.
     *
     * @param context
     *            The app context
     * @param callback
     *            The Object that implement an interface
     *            RequestCallback{@literal <}ArrayList{@literal <}SimpleListItem>>. This object will be used for
     *            sending callbacks in to UI thread.
     * @param merchantId
     *            Merchant id which shops would be loaded.
     * @param flags
     *            flags for request.
     */
    public static void merchantGetShopList(Context context, RequestCallback<ArrayList<SimpleListItem>> callback, String merchantId, int flags) {
        Message<ArrayList<SimpleListItem>> message = new Message<>(getContext(), MerchantCommand.MERCHANT_GET_SHOP_LIST, getUrl(),
                getActiveConnection(), getDevice(), null, getAppIdentity(), getProtocolFactory().newRequestBuilder(), new MerchantShopsListJsonParserOld(), flags);
        message.setShopIdentity(new Identity(merchantId, null));

        WebServiceAsync<ArrayList<SimpleListItem>> webService = new WebServiceAsync<>(getContext(), message, callback);
        webService.execute();
    }

    /**
     * The request to update transaction status. Also, this request allow to add some message into transaction messages list.
     *
     * @param context
     *            The app context
     * @param callback
     *            The Object that implement an interface
     *            RequestCallback{@literal <}Void>. This object will be used for
     *            sending callbacks in to UI thread.
     * @param shopIdentity
     *            Identity of the shop which transaction would be updated.
     * @param transactionId
     *            Id of the transaction that would be updated.
     * @param status
     *            New status of the transaction.
     * @param transactionMessage
     *            Message to add some message into transaction messages list.
     * @param flags
     *            flags for request.
     */
    public static void merchantUpdateTransaction(Context context, RequestCallback<Void> callback,
                                             Identity shopIdentity, String transactionId,
                                             String status, String transactionMessage, int flags) {
        Message<Void> message = new Message<>(getContext(),
                MerchantCommand.MERCHANT_UPDATE_TRANSACTION, getUrl(),
                getActiveConnection(), getDevice(), shopIdentity, getAppIdentity(),
                getProtocolFactory().newRequestBuilder(), getProtocolFactory().newParser(Void.class), flags);

        message.addParameter(Message.Param.ID, transactionId);
        if (status != null)
            message.addParameter(Message.Param.STATUS, status);
        if (transactionMessage != null && !transactionMessage.isEmpty()) {
            message.addParameter("message", transactionMessage);
        }

        WebServiceAsync<Void> webService = new WebServiceAsync<>(getContext(), message, callback);
        webService.execute();
    }

//-------------------------------------------------------------------------------------------
//-------- Getters and setters --------------------------------------------------------------
//-------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------
//-------- Private Classes ------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

}
