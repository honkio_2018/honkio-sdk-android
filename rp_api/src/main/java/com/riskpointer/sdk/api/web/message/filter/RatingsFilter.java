package com.riskpointer.sdk.api.web.message.filter;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by I.N. on 06.04.2018
 */
public class RatingsFilter extends ListFilter implements Serializable {
    public static final String QUERY_OBJECT = "query_object";
    public static final String QUERY_OBJECT_TYPE = "query_object_type";
    private String mObject;
    private String mObjectType;

    public String getObject() {
        return mObject;
    }

    public void setObject(String object) {
        mObject = object;
    }

    public String getObjectType() {
        return mObjectType;
    }

    public void setObjectType(String objectType) {
        mObjectType = objectType;
    }

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = super.toParams();

        applyIfNotNull(params, QUERY_OBJECT, getObject());
        applyIfNotNull(params, QUERY_OBJECT_TYPE, getObjectType());

        return params;
    }
}