package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Shurygin Denis
 */

public class HkStructure implements Serializable {

    public static class Property implements Serializable {

        public static final String PROP_TYPE = "type";
        public static final String PROP_NAME = "name";
        public static final String PROP_IS_REQUIRED = "is_required";
        public static final String PROP_TITLE = "title";

        public static class Type {

            private Type() {}

            public static final String BOOLEAN = "boolean";
            public static final String INT = "int";
            public static final String STRING = "string";
            public static final String URL = "url";
            public static final String TELEPHONE = "phone";
            public static final String EMAIL = "email";
            public static final String ENUM = "enum";
            public static final String ARRAY = "array";
        }

        private String mType;
        private String mName;
        private boolean isRequired;
        private final HKString mTitle = new HKString();

        public String getType() {
            return mType;
        }

        public void setType(String type) {
            this.mType = type;
        }

        public String getName() {
            return mName;
        }

        public void setName(String name) {
            this.mName = name;
        }

        public boolean isRequired() {
            return isRequired;
        }

        public void setRequired(boolean required) {
            isRequired = required;
        }

        public HKString getTitle() {
            return mTitle;
        }

        public Map<String, Object> toParams() {
            Map<String, Object> params = new HashMap<>();

            params.put(PROP_TYPE, mType);
            params.put(PROP_NAME, mName);
            params.put(PROP_IS_REQUIRED, isRequired);
            params.putAll(mTitle.toParams(PROP_TITLE));

            return params;
        }
    }

    public static class NumericProperty extends Property {

        public static final String PROP_MIN = "min";
        public static final String PROP_MAX = "max";

        private double mMin;
        private double mMax;

        public double getMin() {
            return mMin;
        }

        public void setMin(double min) {
            this.mMin = min;
        }

        public double getMax() {
            return mMax;
        }

        public void setMax(double max) {
            this.mMax = max;
        }

        @Override
        public Map<String, Object> toParams() {
            Map<String, Object> params = super.toParams();

            params.put(PROP_MIN, mMin);
            params.put(PROP_MAX, mMax);

            return params;
        }
    }

    public static class EnumProperty extends Property {

        public static final String PROP_VALUES = "values";

        public static class Value implements Serializable {

            public static final String PROP_NAME = "name";
            public static final String PROP_VALUE = "value";

            private String mName;
            private String mValue;

            private HashMap<String, Object> mProps;

            public Value(Map<String, Object> props) {
                if (props instanceof HashMap)
                    this.mProps = (HashMap<String, Object>) props;
                else
                    this.mProps = new HashMap<>(props);
            }

            public String getName() {
                return mName;
            }

            public void setName(String name) {
                this.mName = name;
            }

            public String getValue() {
                return mValue;
            }

            public void setValue(String value) {
                this.mValue = value;
            }

            public Object getProp(String key) {
                if (mProps == null) return null;
                return mProps.get(key);
            }

            public void setProp(String key, Object value) {
                if (mProps == null) mProps = new HashMap<>();
                mProps.put(key, value);
            }

            @Override
            public String toString() {
                return mName;
            }

            public Map<String, Object> toParams() {

                mProps.put(PROP_NAME, mName);
                mProps.put(PROP_VALUE, mValue);

                return mProps;
            }
        }

        private ArrayList<Value> mValues;

        public ArrayList<Value> getValues() {
            return mValues;
        }

        public void setValues(ArrayList<Value> mValues) {
            this.mValues = mValues;
        }

        public Value fund(String value) {
            if (mValues == null || value == null)
                return null;

            for (Value item: mValues) {
                if (value.equals(item.getValue()))
                    return item;
            }
            return null;
        }

        @Override
        public Map<String, Object> toParams() {
            Map<String, Object> params = super.toParams();

            if (mValues != null) {
                ArrayList<Object> list = new ArrayList<>(mValues.size());
                for (Value value : mValues)
                    list.add(value.toParams());

                params.put(PROP_VALUES, list);
            }

            return params;
        }
    }

    public static class ArrayProperty extends Property {

        public static final String PROP_SUBTYPE = "subtype";

        private Property mSubtype;

        public Property getSubtype() {
            return mSubtype;
        }

        public void setSubtype(Property type) {
            this.mSubtype = type;
        }

        @Override
        public Map<String, Object> toParams() {
            Map<String, Object> params = super.toParams();

            if (mSubtype != null)
                params.put(PROP_SUBTYPE, mSubtype.toParams());

            return params;
        }
    }

    public static final String PROP_ID = "_id";
    public static final String PROP_NAME = "name";
    public static final String PROP_PROPERTIES = "properties";

    private String mId;
    private String mName;
    private String mMerchant;

    private HashMap<String, Property> mProperties;

    public HkStructure() {}

    public HkStructure(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getMerchant() {
        return mMerchant;
    }

    public void setMerchant(String merchant) {
        this.mMerchant = merchant;
    }

    public HashMap<String, Property> getProperties() {
        return mProperties;
    }

    public Property getProperty(String name) {
        if (mProperties == null || name == null)
            return null;

        return mProperties.get(name);
    }

    public void setProperties(HashMap<String, Property> properties) {
        this.mProperties = properties;
    }

    public EnumProperty.Value findEnumPropertyValue(String propertyName, String value) {
        Property property = getProperty(propertyName);

        if (property instanceof EnumProperty) {
            return ((EnumProperty) property).fund(value);
        }
        else if (property instanceof ArrayProperty) {
            Property supProperty = ((ArrayProperty) property).getSubtype();
            if (supProperty instanceof EnumProperty)
                return ((EnumProperty) supProperty).fund(value);
        }

        return null;
    }

    public Map<String, Object> toParams() {
        Map<String, Object> params = new HashMap<>(mProperties.size());

        params.put("id", mId);
        params.put(PROP_NAME, mName);

        Map<String, Object> properties = new HashMap<>(mProperties.size());
        for (Property property: mProperties.values())
            properties.put(property.getName(), property.toParams());
        params.put(PROP_PROPERTIES, properties);

        return params;
    }
}
