package com.riskpointer.sdk.api.model.entity;

import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.AbsFilter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

// TODO: Write docs
/**
 * Created by asenko on 12/10/2015.
 */
public class UserRole implements Serializable {

    private String mId;
    private String mUserId;
    private String mMerchantId;
    private String mUserFirstName;
    private String mUserLastName;
    private String mRoleId;
    private String mRoleName;
    private String mRoleDisplayName;
    private String mRoleDesc;
    private int mJobsTotal;
    private int mUserRating;
    private HashMap<String, Object> mContactData;
    private HashMap<String, Object> mPrivateProperties;
    private HashMap<String, Object> mPublicProperties;
    private boolean isActive = true;

    public UserRole() {}

    public UserRole(String userId, String roleId) {
        this();
        this.mUserId = userId;
        this.mRoleId = roleId;
    }

    public UserRole(UserRole userRole) {
        this(userRole.mUserId, userRole.mRoleId);
        this.mId = userRole.mId;
        this.mUserFirstName = userRole.mUserFirstName;
        this.mUserLastName = userRole.mUserLastName;
        this.mJobsTotal = userRole.mJobsTotal;
        this.mUserRating = userRole.mUserRating;

        if (userRole.mContactData != null)
            this.mContactData = new HashMap<>(userRole.mContactData);

        if (userRole.mPrivateProperties != null)
            this.mPrivateProperties = new HashMap<>(userRole.mPrivateProperties);

        if (userRole.mPublicProperties != null)
            this.mPublicProperties = new HashMap<>(userRole.mPublicProperties);
    }


    public void applyTo(Message<?> message) {
        message.addParameters(toParams());
    }

    public Map<String, Object> toParams() {
        Map<String, Object> map = new HashMap<>();

        AbsFilter.applyIfNotNull(map, "role_id", mRoleId);

        if (mPrivateProperties != null && mPrivateProperties.size() > 0)
            map.put("private_properties", mPrivateProperties);

        if (mPublicProperties != null && mPublicProperties.size() > 0)
            map.put("public_properties", mPublicProperties);

        return map;
    }

    public void applyPrivateProperty(String name, Object value) {
        if (mPrivateProperties != null) {
            mPrivateProperties.put(name, value);
        } else {
            mPrivateProperties = new HashMap<>();
            mPrivateProperties.put(name, value);
        }
    }

    public void removePrivateProperty(String name) {
        if (mPrivateProperties != null && mPrivateProperties.containsKey(name)) {
            mPrivateProperties.remove(name);
        }
    }

    public void removePublicProperty(String name) {
        if (mPublicProperties != null && mPublicProperties.containsKey(name)) {
            mPublicProperties.remove(name);
        }
    }

    public void applyPublicProperty(String name, Object value) {
        if (mPublicProperties == null)
            mPublicProperties = new HashMap<>();

        mPublicProperties.put(name, value);
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        this.mUserId = userId;
    }

    public String getUserFirstName() {
        return mUserFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.mUserFirstName = userFirstName;
    }

    public String getUserLastName() {
        return mUserLastName;
    }

    public void setUserLastName(String userLastName) {
        this.mUserLastName = userLastName;
    }

    public String getUserName() {
        StringBuilder builder = new StringBuilder();

        if (mUserFirstName != null)
            builder.append(mUserFirstName);

        if (mUserLastName != null) {
            if (builder.length() > 0)
                builder.append(" ");

            builder.append(mUserLastName);
        }
        return builder.toString();
    }

    public String getRoleId() {
        return mRoleId;
    }

    public void setRoleId(String role) {
        this.mRoleId = role;
    }

    public String getId() {
        return mId;
    }

    public void setId(String userRoleId) {
        this.mId = userRoleId;
    }

    public int getJobsTotal() {
        return mJobsTotal;
    }

    public void setJobsTotal(int jobs) {
        this.mJobsTotal = jobs;
    }

    public int getUserRating() {
        return mUserRating;
    }

    public void setUserRating(int rating) {
        this.mUserRating = rating;
    }

    public Map<String, Object> getContactData() {
        return mContactData;
    }

    public void setContactData(HashMap<String, Object> contactData) {
        this.mContactData = contactData;
    }

    public Map<String, Object> getPrivateProperties() {
        return mPrivateProperties;
    }

    public Object getPrivateProperty(String key) {
        if (mPrivateProperties == null)
            return null;
        return mPrivateProperties.get(key);
    }

    public void setPrivateProperties(HashMap<String, Object> privateProperties) {
        this.mPrivateProperties = privateProperties;
    }

    public Map<String, Object> getPublicProperties() {
        return mPublicProperties;
    }

    public Object getPublicProperty(String key) {
        if (mPublicProperties == null)
            return null;
        return mPublicProperties.get(key);
    }

    public void setPublicProperties(HashMap<String, Object> publicProperties) {
        this.mPublicProperties = publicProperties;
    }

    public String getRoleName() {
        return mRoleName;
    }

    public void setRoleName(String mRoleName) {
        this.mRoleName = mRoleName;
    }

    public String getRoleDisplayName() {
        return mRoleDisplayName;
    }

    public void setRoleDisplayName(String mRoleDisplayName) {
        this.mRoleDisplayName = mRoleDisplayName;
    }

    public String getRoleDesc() {
        return mRoleDesc;
    }

    public void setRoleDesc(String mRoleDesc) {
        this.mRoleDesc = mRoleDesc;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getMerchantId() {
        return mMerchantId;
    }

    public void setMerchantId(String merchantId) {
        this.mMerchantId = merchantId;
    }
}
