package com.riskpointer.sdk.api.model.task;

import com.riskpointer.sdk.api.web.callback.RequestCallback;

/**
 * Interface that represent a some task that can be aborted. This task has a callback.
 * Method abort() doesn't guarantees immediately abortion of the task, some tasks doesn't allow
 * abortion after switching to special stage. For correct behaviour see concrete realisation.
 */
public interface RequestTask<Result> extends Task {

    /**
     *  Sets callback for the task.
     * @param callback The Object that implement an interface RequestCallback
     *            {@literal <}ResponseResult>. This object will be used
     *            for sending callbacks in to UI thread.
     * @param deliverResult If true callback will receive result if task is already finished or canceled.
     */
    public void setCallback(RequestCallback<Result> callback, boolean deliverResult);
}
