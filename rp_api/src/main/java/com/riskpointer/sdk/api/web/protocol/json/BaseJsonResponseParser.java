/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import android.text.TextUtils;

import com.riskpointer.sdk.api.exception.WrongHashException;
import com.riskpointer.sdk.api.utils.ApiHash;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.protocol.ResponseParser;
import com.riskpointer.sdk.api.web.response.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Base implementation of {@link ResponseParser}. This class parse server responses in JSON format.
 *
 * @param <BuildResult> Type of the response.
 */
public abstract class BaseJsonResponseParser<BuildResult> implements ResponseParser<BuildResult> {

    /**
     * Parse JSON object into Result object.
     *
     * @param message Message used for request.
     * @param json    JSON object to parse.
     * @return Result object.
     */
    protected abstract BuildResult parseJson(Message<?> message, JSONObject json);

    @Override
    public Response<BuildResult> parse(Message<?> message, String responseString, String password) throws WrongHashException {
        JSONObject json = null;
        try {
            json = new JSONObject(responseString);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        checkHash(json, password, true);
        Response<BuildResult> response = parseResponse(message, json);
        return response;
    }

    /**
     * Parse JSON object into Response object.
     *
     * @param message Message used for request.
     * @param json    JSON object to parse.
     * @return Response object.
     */
    public Response<BuildResult> parseResponse(Message<?> message, JSONObject json) {
        Response<BuildResult> response = new Response<>(parseJson(message, json));
        Response.Editor.setCommand(response, message.getCommand());
        Response.Editor.setStatus(response, json.optString("status", null));
        Response.Editor.setId(response, json.optString("id", null));
        Response.Editor.setOtp(response, json.optString("otp", null));
        Response.Editor.setDescription(response, json.optString("description", null));
        Response.Editor.setUrl(response, json.optString("url", null));
        Response.Editor.setShopReference(response, json.optString(Message.Param.SHOP_REFERENCE));
        if (json.has("pending")) {
            JSONArray pendingJson = json.optJSONArray("pending");
            if (pendingJson != null) {
                List<String> pending = new ArrayList<>(pendingJson.length());
                for (int i = 0; i < pendingJson.length(); i++)
                    pending.add(pendingJson.optString(i));
                Response.Editor.setPendings(response, pending);
            }
        }
        if (json.has("error")) {
            int group = 0;
            int subItem = 0;

            JSONArray errorJson = json.optJSONArray("error");
            if (errorJson != null) {
                if (errorJson.length() >= 2) {
                    try {
                        group = errorJson.getInt(0);
                        subItem = errorJson.getInt(1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        group = RpError.Group.API;
                        subItem = RpError.subItemFromCode(RpError.Api.UNKNOWN);
                    }
                } else {
                    group = RpError.Group.API;
                    subItem = RpError.subItemFromCode(RpError.Api.UNKNOWN);
                }
            }

            String description = response.getDescription();
            String[] fields = null;
            if(description != null) {
                StringBuilder buffer = new StringBuilder(description);
                int firstSpace = buffer.indexOf(" ");
                if(firstSpace > 0) {
                    buffer.delete(0, firstSpace); // Remove error code

                    int endOfFields = buffer.indexOf(":");
                    if(endOfFields > 0)
                        fields = buffer.substring(0, endOfFields).trim().split(" ");
                }
            }

            Response.Editor.setError(response, new RpError(group, subItem, message, fields, description));
        }
        return response;
    }

    /**
     * Check if JSON object has valid hash value.
     *
     * @param json           JSON object to check.
     * @param password       Password for the hash calculation.
     * @param throwException True if Exception should be thrown when hash is wrong, false otherwise.
     * @return True if hash is correct, false otherwise.
     * @throws WrongHashException If hash is wrong and throwException == true.
     */
    public boolean checkHash(JSONObject json, String password, boolean throwException) throws WrongHashException {
        if (TextUtils.isEmpty(password))
            return true;

        String expectedHash = json.optString("hash");
        if (TextUtils.isEmpty(expectedHash))
            return true;

        String calculatedHash = ApiHash.sha512(json, password);

        boolean hashIsOk = expectedHash.equals(calculatedHash);
        if (throwException && !hashIsOk)
            throw new WrongHashException("Wrong response hash." +
                    "\nResponse Hash = '" + expectedHash + "'" +
                    "\nCalculated    = '" + calculatedHash + "'" +
                    "\nSource str    = '" + ApiHash.findValues(json) + "'");

        return hashIsOk;
    }

    /**
     * Recursive conversion of JSONObject to a HashMap
     *
     * @param json JSON object to convert.
     * @return Map<String, Object> with
     */
    public static Map<String, Object> jsonToMap(JSONObject json) {

        HashMap<String, Object> map = new HashMap<>();
        Iterator<?> keys = json.keys();

        while (keys.hasNext()) {
            try {
                String key = (String) keys.next();
                map.put(key, jsonToObject(json.get(key)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    public static List<Object> jsonArrayToList(JSONArray jsonArray) {
        ArrayList<Object> result = new ArrayList<>(jsonArray.length());
        for (int i = 0; i < jsonArray.length(); i++) {
            result.add(jsonToObject(jsonArray.opt(i)));
        }
        return result;
    }

    public static Object jsonToObject(Object someJsonObject) {
        if (someJsonObject instanceof JSONObject) {
            return jsonToMap((JSONObject) someJsonObject);
        } else if (someJsonObject instanceof JSONArray) {
            return jsonArrayToList((JSONArray) someJsonObject);
        } else if (someJsonObject instanceof Serializable) {
            return someJsonObject; // normal serializable object
        } else {
            return null;  // unsupported object
        }
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj.getClass() == getClass()
                && obj.getClass().getGenericSuperclass() == getClass().getGenericSuperclass();
    }
}
