/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;

/**
 * Data object that holds information about shop and his merchant.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
public class ShopInfo implements Serializable, HkQrCode.QrObject {
	private static final long serialVersionUID = 3515791852494196328L;

	private long mTimeStamp;
	private Shop mShop;
	private MerchantSimple mMerchant;

	/**
	 * Gets Server time in millis when ShopInfo was sent.
	 * @return Server time in millis when ShopInfo was sent.
	 */
	public long getTimeStamp() {
		return mTimeStamp;
	}

	/**
	 * Sets Server time in millis when ShopInfo was sent.
	 * @param timeMillis Server time in millis when ShopInfo was sent.
	 */
	public void setTimeStamp(long timeMillis) {
		mTimeStamp = timeMillis;
	}

	public Shop getShop() {
		return mShop;
	}

	public void setShop(Shop shop) {
		this.mShop = shop;
	}

	public MerchantSimple getMerchant() {
		return mMerchant;
	}

	public void setMerchant(MerchantSimple merchant) {
		this.mMerchant = merchant;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (obj == null)
			return false;

		if (getClass() != obj.getClass())
			return false;

		ShopInfo info = (ShopInfo) obj;

		return mTimeStamp == info.mTimeStamp &&
				((mShop != null && mShop.equals(info.mShop)) ||
						(mShop == null && info.mShop == null)) &&
				((mMerchant != null && mMerchant.equals(info.mMerchant)) ||
						(mMerchant == null && info.mMerchant == null));
	}

	@Override
	public String getId() {
		if (mShop != null)
			return mShop.getId();
		return null;
	}
}
