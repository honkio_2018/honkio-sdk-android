package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by asenko on 12/15/2015.
 * TODO write docs
 */
public class UserCommentsList implements Serializable {

    private ArrayList<UserComment> mList;

    public ArrayList<UserComment> getList() {
        return mList;
    }

    public void setList(ArrayList<UserComment> list) {
        this.mList = list;
    }
}
