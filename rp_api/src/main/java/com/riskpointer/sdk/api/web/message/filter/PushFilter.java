package com.riskpointer.sdk.api.web.message.filter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Shurygin Denis
 */

// TODO write docs
public class PushFilter extends AbsFilter {

    public static final String QUERY_CONTENT_TYPE = "type";
    public static final String QUERY_CONTENT_ORDER = "order_id";
    public static final String QUERY_CONTENT_ASSET = "asset_id";
    public static final String QUERY_CONTENT_TRANSACTION = "id";

    private String mId;
    private HashMap<String, String> mContent;

    /**
     * Gets Id of item that should be returned.
     * @return Id of item that should be returned.
     */
    public String getId() {
        return mId;
    }

    /**
     * Sets Id of item that should be returned.
     * @param id Id of item that should be returned.
     */
    public PushFilter setId(String id) {
        this.mId = id;
        return this;
    }

    // TODO write docs
    public HashMap<String, String> getContent() {
        return mContent;
    }

    // TODO write docs
    public PushFilter setContent(HashMap<String, String> content) {
        this.mContent = content;
        return this;
    }

    // TODO write docs
    public PushFilter addContent(String key, String value) {
        if (mContent == null)
            mContent = new HashMap<>();
        mContent.put(key, value);
        return this;
    }

    @Override
    public Map<String, Object> toParams() {
        HashMap<String, Object> params = new HashMap<>();
        AbsFilter.applyIfNotNull(params, "query_id", getId());
        AbsFilter.applyIfNotNull(params, "query_content", getContent());

        return params;
    }
}
