package com.riskpointer.sdk.api.model.push;

import com.riskpointer.sdk.api.model.push.entity.AssetChangedPushEntity;
import com.riskpointer.sdk.api.model.push.entity.InvitationPushEntity;
import com.riskpointer.sdk.api.model.push.entity.OrderChatMessagePushEntity;
import com.riskpointer.sdk.api.model.push.entity.OrderStatusChangedPushEntity;
import com.riskpointer.sdk.api.model.push.entity.PollChangedPushEntity;
import com.riskpointer.sdk.api.model.push.entity.RoleChangedPushEntity;
import com.riskpointer.sdk.api.model.push.entity.SystemMessagePushEntity;
import com.riskpointer.sdk.api.model.push.entity.TransactionPushEntity;
import com.riskpointer.sdk.api.model.push.entity.WorkerApplyPushEntity;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.protocol.json.BaseJsonResponseParser;
import com.riskpointer.sdk.api.web.protocol.json.InvitationJsonParser;

import org.json.JSONObject;

/**
 * Created by Shurygin Denis on 2016-01-17.
 */
public class PushParserImpl extends PushParser {

    @Override
    public Push<?> parse(JSONObject pushJson) {
        Push<?> result = null;

        String type = pushJson.optString(TYPE);
        if (type != null) {
            if (TransactionPushEntity.Type.NEW_PAYMENT.equals(type) || TransactionPushEntity.Type.TRANSACTION_UPDATE.equals(type)) {
                Push<TransactionPushEntity> push = new Push<>();
                push.setEntity(parseTransactionPush(pushJson));
                result = push;

            } else if (Push.Type.ORDER_STATUS_CHANGED.equals(type)) {
                Push<OrderStatusChangedPushEntity> push = new Push<>();
                push.setEntity(parseOrderStatusChangedPush(pushJson));
                result = push;

            } else if (Push.Type.INVITATION.equals(type)) {
                Push<InvitationPushEntity> push = new Push<>();
                push.setEntity(parseInvitationPush(pushJson));
                result = push;

            } else if (Push.Type.CHAT_MESSAGE.equals(type)) {
                Push<OrderChatMessagePushEntity> push = new Push<>();
                push.setEntity(parseOrderChatMessagePush(pushJson));
                result = push;

            } else if (Push.Type.TEST.equals(type)) {
                Push<Push.PushEntity> push = new Push<>();
                push.setEntity(new Push.PushEntity(Push.Type.TEST));
                result = push;

            } else if (Push.Type.SYSTEM.equals(type)) {
                Push<Push.PushEntity> push = new Push<>();
                push.setEntity(new SystemMessagePushEntity(
                        BaseJsonResponseParser.jsonToMap(pushJson)));
                result = push;

            } else if (WorkerApplyPushEntity.TYPE.equals(type)) {
                Push<WorkerApplyPushEntity> push = new Push<>();
                push.setEntity(parseWorkerApply(pushJson));
                result = push;

            } else if (Push.Type.ROLE_CHANGED.equals(type)) {
                Push<RoleChangedPushEntity> push = new Push<>();
                push.setEntity(new RoleChangedPushEntity(
                        BaseJsonResponseParser.jsonToMap(pushJson)));
                result = push;

            } else if (Push.Type.ASSET_CHANGED.equals(type)) {
                Push<AssetChangedPushEntity> push = new Push<>();
                push.setEntity(new AssetChangedPushEntity(
                        BaseJsonResponseParser.jsonToMap(pushJson)));
                result = push;

            } else if (Push.Type.POLL_CHANGED.equals(type)) {
                Push<PollChangedPushEntity> push = new Push<>();
                push.setEntity(new PollChangedPushEntity(
                        BaseJsonResponseParser.jsonToMap(pushJson)));
                result = push;

            }
        }

        return result;
    }

    private static TransactionPushEntity parseTransactionPush(JSONObject responseJson) {
        TransactionPushEntity push = new TransactionPushEntity();
        push.setShopId(responseJson.optString("shop"));
        push.setTransactionId(responseJson.optString("id"));
        push.setStatus(responseJson.optString("status"));
        return push;
    }

    private static OrderChatMessagePushEntity parseOrderChatMessagePush(JSONObject responseJson) {
        OrderChatMessagePushEntity push = new OrderChatMessagePushEntity();

        push.setTime(ApiFormatUtils.stringDateToLong(responseJson.optString("timestamp")));
        push.setSenderId(responseJson.optString("sender_id"));
        push.setSenderRole(responseJson.optString("sender_role_id"));
        push.setReceiverId(responseJson.optString("receiver_id"));
        push.setReceiverRole(responseJson.optString("receiver_role_id"));
        push.setOrderId(responseJson.optString("order_id"));
        push.setText(responseJson.optString("text"));
        push.setOrderThirdPerson(responseJson.optString("third_person"));
        push.setSenderFirstName(responseJson.optString("sender_first_name"));
        push.setSenderLastName(responseJson.optString("sender_last_name"));
        push.setShopName(responseJson.optString("shop_name"));
        push.setShopId(responseJson.optString("shop_id"));
        return push;
    }

    private static OrderStatusChangedPushEntity parseOrderStatusChangedPush(JSONObject responseJson) {
        OrderStatusChangedPushEntity push = new OrderStatusChangedPushEntity();
        push.setOrderId(responseJson.optString("order_id"));
        push.setOrderTitle(responseJson.optString("order_title"));
        push.setOrderStatus(responseJson.optString("order_status"));
        push.setOrderOwnerId(responseJson.optString("order_owner_id"));
        push.setUserId(responseJson.optString("user_id"));
        push.setUserName(responseJson.optString("user_name"));
        return push;
    }

    private static InvitationPushEntity parseInvitationPush(JSONObject responseJson) {
        InvitationPushEntity push = new InvitationPushEntity(BaseJsonResponseParser.jsonToMap(responseJson));
        push.setInvitation(InvitationJsonParser.parseInvitation(responseJson));
        return push;
    }

    private WorkerApplyPushEntity parseWorkerApply(JSONObject responseJson) {
        WorkerApplyPushEntity push = new WorkerApplyPushEntity();
        push.setOrderId(responseJson.optString("order_id"));
        push.setOrderTitle(responseJson.optString("order_title"));
        push.setUserId(responseJson.optString("user_id"));
        push.setUserName(responseJson.optString("user_name"));
        push.setProposalTime(responseJson.optString("proposal_time"));
        return push;
    }
}
