package com.riskpointer.sdk.api.web.message.filter;

import com.riskpointer.sdk.api.web.message.Message;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Shurygin Denis
 */
public interface HKFilter extends Serializable {

    /**
     * Convert filter to HashMap that hold set of params.
     * @return HashMap that represent this filter.
     */
    public abstract Map<String, Object> toParams();

    /**
     * Apply filter for specified message. Also filter put self into message as null parameter.
     * @param message Message for apply filter.
     */
    public void applyTo(Message<?> message);

}
