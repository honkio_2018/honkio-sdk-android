/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.UserRole;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Parse UserRole from JSON.
 */
public class UserRoleJsonParser extends BaseJsonResponseParser<UserRole> {

    @Override
    public UserRole parseJson(Message<?> message, JSONObject json) {
        return UserRoleJsonParser.parseRole(json);
    }

    public static UserRole parseRole(JSONObject json) {
        UserRole role = new UserRole();
        role.setRoleId(json.optString("role_id"));
        role.setId(json.optString("id"));
        role.setMerchantId(json.optString("merchant"));
        role.setRoleName(json.optString("name"));
        role.setRoleDisplayName(json.optString("display_name"));
        role.setRoleDesc(json.optString("description"));

        role.setUserId(json.optString("user_id"));
        role.setUserFirstName(json.optString("user_first_name"));
        role.setUserLastName(json.optString("user_last_name"));

        role.setJobsTotal(json.optInt("user_jobs_total"));
        role.setUserRating(json.optInt("user_rating"));

        JSONObject contactData = json.optJSONObject("user_contact_data");
        if (contactData != null) {
            role.setContactData((HashMap<String, Object>) jsonToMap(contactData));
        }

        JSONObject privateData = json.optJSONObject("private_properties");
        if (privateData != null) {
            role.setPrivateProperties((HashMap<String, Object>) jsonToMap(privateData));
        }

        JSONObject publicData = json.optJSONObject("public_properties");
        if (publicData != null) {
            role.setPublicProperties((HashMap<String, Object>) jsonToMap(publicData));
        }

        if (json.has("active"))
            role.setActive(json.optBoolean("active"));

        return role;
    }
}