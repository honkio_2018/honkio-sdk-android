/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONObject;

/**
 * Parse Response without result from JSON.
 */
public class SimpleResponseJsonParser extends BaseJsonResponseParser<Void> {

	@Override
	public Void parseJson(Message<?> message, JSONObject json) {
		return null;
	}

}
