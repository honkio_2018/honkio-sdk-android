/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Applicant;
import com.riskpointer.sdk.api.model.entity.ApplicantsList;
import com.riskpointer.sdk.api.model.entity.UserRole;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Parses Applicants List from JSON.
 * TODO: write docs
 */

public class ApplicantsListJsonParser extends BaseJsonResponseParser<ApplicantsList> {

    private UserRoleJsonParser mRoleParser = new UserRoleJsonParser();

    @Override
    public ApplicantsList parseJson(Message<?> message, JSONObject json) {

        ApplicantsList applicantsList = new ApplicantsList();

        ArrayList<Applicant> list = new ArrayList<Applicant>();

        JSONArray applicantsListJson = json.optJSONArray("applicants");
        if (applicantsListJson != null) {
            for (int i = 0; i < applicantsListJson.length(); i++) {
                JSONObject applicantJson = applicantsListJson.optJSONObject(i);
                if (applicantJson != null) {
                    list.add(parseApplicant(applicantJson));
                }
            }
        }
        applicantsList.setList(list);
        applicantsList.setTotalCount(json.optInt("applicants_count", list.size()));
        return applicantsList;
    }

    private Applicant parseApplicant(JSONObject applicantJson) {
        Applicant applicant = new Applicant();
        applicant.setOrderId(applicantJson.optString("order_id"));
        applicant.setApplicationId(applicantJson.optString("id"));
        applicant.setUserId(applicantJson.optString("worker_id"));
        applicant.setTimeProposal(applicantJson.optString("time_proposal"));

        JSONObject roleJson = applicantJson.optJSONObject("role");
        if (roleJson != null) {
            UserRole role = mRoleParser.parseJson(null, roleJson);
            if (role != null) {
                applicant.setRole(role);
                applicant.setUserId(role.getUserId());
            }
        }

        return applicant;
    }
}
