package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.HashMap;

import static com.riskpointer.sdk.api.model.entity.AppInfo.TouInfo.KEY_DEFAULT;

/**
 * Created by Shurygin Denis on 2016-07-06.
 */
public class AppInfo extends Identity {

    public static class TouInfo implements Serializable {

        public static final String KEY_DEFAULT = "default";

        public final String key;
        public final String url;
        public final int version;

        public TouInfo(String url, int version) {
            this(KEY_DEFAULT, url, version);
        }

        public TouInfo(String key, String url, int version) {
            this.key = key;
            this.url = url;
            this.version = version;
        }
    }

    private Identity mShopIdentity;
    private Identity mOauthIdentity;
    private HashMap<String, TouInfo> mTouInfoMap = new HashMap<>();

    public AppInfo() {
        super();
    }

    public AppInfo(String id, String password) {
        super(id, password);
    }

    public AppInfo(Identity identity) {
        super(identity);
    }

    public Identity getShopIdentity() {
        return mShopIdentity;
    }

    public void setShopIdentity(Identity shopIdentity) {
        mShopIdentity = shopIdentity;
    }

    public Identity getOauthIdentity() {
        return mOauthIdentity;
    }

    public void setOauthIdentity(Identity oauthIdentity) {
        mOauthIdentity = oauthIdentity;
    }

    public TouInfo getTouInfo() {
        return getTouInfo(KEY_DEFAULT);
    }

    public TouInfo getTouInfo(String key) {
        return mTouInfoMap.get(key);
    }

    public void addTouInfo(TouInfo value) {
        mTouInfoMap.put(value.key, value);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (getClass() != obj.getClass())
            return false;

        AppInfo info = (AppInfo) obj;
        return ((mShopIdentity != null && mShopIdentity.equals(info.mShopIdentity)) ||
                        (mShopIdentity == null && info.mShopIdentity == null)) &&
                ((mOauthIdentity != null && mOauthIdentity.equals(info.mOauthIdentity)) ||
                        (mOauthIdentity == null && info.mOauthIdentity == null)) &&
                mTouInfoMap.equals(info.mTouInfoMap) &&
                super.equals(obj);
    }
}
