/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.entity;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * ArrayList that holds list of the products for concrete shop.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
public class ShopProducts extends ArrayList<Product> implements Serializable{
	private static final long serialVersionUID = -6357230785128556598L;

    private String mShopId;

    /**
     * Gets ID of the Shop that contains this Products.
     * @return ID of the Shop that contains this Products.
     */
    public String getShopId() {
        return mShopId;
    }

    /**
     * Sets ID of the Shop that contains this Products.
     * @param shopId ID of the Shop that contains this Products.
     */
    public void setShopId(String shopId) {
        mShopId = shopId;
    }

    public Product findProductById(String productId) {
        if (TextUtils.isEmpty(productId))
            return null;

        for (Product product : this) {
            if (productId.equals(product.getId()))
                return product;
        }
        return null;
    }
}
