package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.MediaFile;
import com.riskpointer.sdk.api.model.entity.MediaFileList;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Parse media file list from JSON.
 */
public class MediaFileListJsonParser extends BaseJsonResponseParser<MediaFileList> {

    @Override
    public MediaFileList parseJson(Message<?> message, JSONObject json) {
        MediaFileList mediaFileList = new MediaFileList();
        ArrayList<MediaFile> list = new ArrayList<>();

        JSONArray mediaFileListJson = json.optJSONArray("files");
        if (mediaFileListJson != null) {
            for (int i = 0; i < mediaFileListJson.length(); i++) {
                JSONObject mediaFileJson = mediaFileListJson.optJSONObject(i);
                if (mediaFileJson != null) {
                    MediaFile mediaFile = MediaFileJsonParser.parseMediaFile(mediaFileJson);
                    if (mediaFile != null)
                        list.add(mediaFile);
                }
            }
        }
        mediaFileList.setList(list);
        return mediaFileList;
    }

}
