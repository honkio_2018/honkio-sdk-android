/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.process.user;

import android.content.Context;
import android.os.Bundle;

import com.riskpointer.sdk.api.BaseHonkioApi;
import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.model.process.LoginProcessModel;
import com.riskpointer.sdk.api.model.process.PinReason;
import com.riskpointer.sdk.api.model.task.RequestTask;
import com.riskpointer.sdk.api.model.task.TaskStorage;
import com.riskpointer.sdk.api.utils.ApiValuesUtils;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.callback.SimpleCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.api.web.response.Response.Pending;

/**
 * Abstract class that implement base logic of User login.
 */
public abstract class UserLoginProcessModel extends LoginProcessModel<User> {

	private static final String SAVED_IS_PIN_REQUESTED = "UserLoginProcessModel.SAVED_IS_PIN_REQUESTED";
	private static final String SAVED_TASK_FOR_PASSWORD = "UserLoginProcessModel.SAVED_TASK_FOR_PASSWORD";
	private static final String SAVED_TASK_FOR_PIN = "UserLoginProcessModel.SAVED_TASK_FOR_PIN";
	private static final String SAVED_FLAGS = "UserLoginProcessModel.SAVED_FLAGS";

	private int mFlags;

	private Response<User> mSavedResponse;
	private RequestTask<User> mTaskForPassword;
	private RequestTask<User> mTaskForPin;
	private boolean isPinRequested;

	UserLoginProcessModel() {
		super();
	}

	/**
	 * Construct new Login process model with specified callback.
	 * @param callback Request callback.
	 */
	public UserLoginProcessModel(RequestCallback<User> callback) {
		super(callback);
	}

	@Override
	public Bundle saveInstanceState() {
		Bundle savedState = super.saveInstanceState();

		savedState.putBoolean(SAVED_IS_PIN_REQUESTED, isPinRequested);

		savedState.putInt(SAVED_TASK_FOR_PASSWORD, TaskStorage.push(mTaskForPassword));
		savedState.putInt(SAVED_TASK_FOR_PIN, TaskStorage.pushIfNotCompleted(mTaskForPin));
		savedState.putInt(SAVED_FLAGS, mFlags);

		return savedState;
	}

	@Override
	public void restoreInstanceState(Context context, Bundle instanceState) {
		super.restoreInstanceState(context, instanceState);

		isPinRequested = instanceState.getBoolean(SAVED_IS_PIN_REQUESTED, false);

		if (instanceState.containsKey(SAVED_TASK_FOR_PASSWORD)) {
			mTaskForPassword = (RequestTask<User>) TaskStorage.pull(instanceState.getInt(SAVED_TASK_FOR_PASSWORD, 0));
			if (mTaskForPassword != null)
				mTaskForPassword.setCallback(new LoginWithOauthTokenCallback(context), true);
		}

		if (instanceState.containsKey(SAVED_TASK_FOR_PIN)) {
			mTaskForPin = (RequestTask<User>) TaskStorage.pull(instanceState.getInt(SAVED_TASK_FOR_PIN, 0));
			if (mTaskForPin != null)
				mTaskForPin.setCallback(mLoginCallback, true);
		}

		mFlags = instanceState.getInt(SAVED_FLAGS, 0);
	}

	@Override
	public synchronized boolean abort() {
		if (!isCompleted() && !isAborted()) {
			if (mTaskForPassword != null)
				mTaskForPassword.abort();
			mTaskForPassword = null;

			if (mTaskForPin != null)
				mTaskForPin.abort();
			mTaskForPin = null;

			HonkioApi.logout(Message.FLAG_HIGH_PRIORITY, new SimpleCallback() {
				@Override
				public void onComplete() { }

				@Override
				public boolean onError(RpError error) {
					HonkioApi.logout(Message.FLAG_OFFLINE_MODE, null);
					return false;
				}
			});
		}
		super.abort();

		return true;
	}

	@Override
	public void loginWithOAuthToken(Context context, String token, int flags) {
		if (!isAborted()) {
			mTaskForPin = null;
			mFlags = flags;
			start();
			isPinRequested = false;
			mTaskForPassword = HonkioApi.loginWithOAuthToken(token, mFlags,
					new LoginWithOauthTokenCallback(context));
		}
		else
			onLoginError(RpError.ABORTED);
	}
	
	@Override
	public void login(Context context, final int flags) {
		if (!isAborted()) {
			mFlags = flags;
			start();
			if (HonkioApi.checkReLogin(mLoginCallback))
				onRequestPin(context, PinReason.REENTER);
		}
		else
			onLoginError(RpError.ABORTED);
	}
	
	@Override
	public void login(Context context, String pin, final int flags) {
		if (!isAborted()) {
			mTaskForPassword = null;
			start();
			if(HonkioApi.checkReLogin(mLoginCallback)) {
				mTaskForPin = HonkioApi.login(pin, mFlags, mLoginCallback);
			}
		}
		else
			onLoginError(RpError.ABORTED);
	}

	@Override
	public void login(Context context, String login, String password, int flags) {
		throw new UnsupportedOperationException("Login for user login and password not supported for this user type.");
	}

	@Override
	public void onPinEntered(Context context, String pin, long[] delays, int reason) {
		if (!isAborted()) {
			switch (reason) {
				case PinReason.REENTER:
					mTaskForPin = HonkioApi.login(pin,
							ApiValuesUtils.putPinDelayParams(null, pin, delays),
							mFlags, mLoginCallback);
					mTaskForPassword = null;
					break;
				case PinReason.NEW:
					BaseHonkioApi.changePin(null, pin);
					if (!onLoginComplete(mSavedResponse))
						BroadcastHelper.sendActionUnhandledResponse(context,
								Message.Command.USER_GET, mSavedResponse,
								Thread.getAllStackTraces().get(Thread.currentThread()));
					mSavedResponse = null;
					break;
			}
		}
		else
			onLoginError(RpError.ABORTED);
	}

	@Override
	public void onPinCanceled(Context context, int reason) {
		if (!isAborted()) {
			switch (reason) {
				case PinReason.REENTER:
					abort();
					onLoginError(new RpError(RpError.Api.CANCELED_BY_USER));
					break;
				case PinReason.NEW:
					abort();
					RpError error = new RpError(RpError.Api.CANCELED_BY_USER);
					if (!onLoginError(error))
						BroadcastHelper.sendActionUnhandledError(context,
								Message.Command.USER_GET, error,
								Thread.getAllStackTraces().get(Thread.currentThread()));
					mSavedResponse = null;
					break;
			}
		}
		else
			onLoginError(RpError.ABORTED);
	}

	@Override
	public void reset() {
		super.reset();
		mSavedResponse = null;
		mTaskForPassword = null;
		mTaskForPin = null;
		isPinRequested = false;
	}

	protected boolean onLoginComplete(Response<User> response) {
		setStatus(Status.FINISHED);
		return mCallback.onComplete(response);
	}

	protected boolean onLoginError(RpError error) {
		setStatus(Status.FINISHED);
		return mCallback.onError(error);
	}

	private RequestCallback<User> mLoginCallback = new RequestCallback<User>() {

		@Override
		public boolean onComplete(Response<User> response) {
			return onLoginComplete(response);
		}

		@Override
		public boolean onError(RpError error) {
			return onLoginError(error);
		}
	};

	private class LoginWithOauthTokenCallback implements RequestCallback<User> {
		private Context mContext;
		
		public LoginWithOauthTokenCallback(Context context) {
			mContext = context.getApplicationContext();
		}

		@Override
        public boolean onComplete(final Response<User> response) {
            if (response.isStatusAccept() || response.isStatusPending()) {
                if (!response.hasPending(Pending.SMS_VERIFY)) {
					mSavedResponse = response;
					if (!isPinRequested)
						onRequestPin(mContext, PinReason.NEW);
					isPinRequested = true;
                    return true;
                } else
                    return onLoginComplete(response);
            }
            return onError(response.getAnyError());
        }

		@Override
		public boolean onError(RpError error) {
			return onLoginError(error);
		}
	}

}
