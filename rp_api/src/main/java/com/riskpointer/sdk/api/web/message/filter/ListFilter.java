package com.riskpointer.sdk.api.web.message.filter;

import com.riskpointer.sdk.api.web.message.Message;

import java.util.HashMap;

/**
 * Created by Linch on 29.12.2015.
 */
public class ListFilter extends AbsFilter {

    private String mId;
    private Integer mCount;
    private Integer mSkip;

    public ListFilter() {}

    public ListFilter(ListFilter filter) {
        this.mId = filter.mId;
        this.mCount = filter.mCount;
        this.mSkip = filter.mSkip;
    }

    public ListFilter(String id) {
        this.mId = id;
    }

    /**
     * Gets Id of item that should be returned.
     * @return Id of item that should be returned.
     */
    public String getId() {
        return mId;
    }

    /**
     * Sets Id of item that should be returned.
     * @param id Id of item that should be returned.
     */
    public ListFilter setId(String id) {
        this.mId = id;
        return this;
    }

    /**
     * Gets Items count limit.
     * @return Items count limit.
     */
    public Integer getCount() {
        return mCount;
    }

    /**
     * Sets items count limit for request.
     * @param count Items count limit. Set null to set default limit.
     */
    public ListFilter setCount(Integer count) {
        this.mCount = count;
        return this;
    }

    /**
     * Gets Items count to skip.
     * @return Items count to skip.
     */
    public Integer getSkip() {
        return mSkip;
    }

    /**
     * Sets Items count to skip.
     * @param skip Items count to skip. Set null to set default value = 0.
     */
    public ListFilter setSkip(Integer skip) {
        this.mSkip = skip;
        return this;
    }

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = new HashMap<>();
        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_COUNT, getCount());
        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_SKIP, getSkip());
        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_ID, getId());

        return params;
    }
}
