/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.process.user;

import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.api.model.entity.UserAccount.Type;
import com.riskpointer.sdk.api.web.callback.SimpleCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;

/**
 * Process model for User payment account creation with type {@code OPERATOR}.
 */
public class OperatorAccountCreationProcessModel extends AccountCreationProcessModel {
	private static final String CARD_TYPE = Type.OPERATOR;
	private static final String CARD_NUMBER = UserAccount.ZERO_NUMBER;

	/**
	 * Base constructor.
	 * @param callback SimpleCallback implementation.
	 */
	public OperatorAccountCreationProcessModel(SimpleCallback callback) {
		this(new CallbackWrapper(callback));
	}

	/**
	 * Base constructor.
	 * @param callback SimplePendingCallback implementation.
	 */
	public OperatorAccountCreationProcessModel(Callback callback) {
		super(callback, CARD_TYPE, CARD_NUMBER);

		// Account doesn't require payment initialisation. No need to do test payment.
		setPaymentCheckEnabled(false);
	}

    private static class CallbackWrapper implements Callback {
		private SimpleCallback mCallback;

		public CallbackWrapper(SimpleCallback callback) {
			mCallback = callback;
		}
		
		@Override
		public boolean onComplete(UserAccount account) {
			mCallback.onComplete();
			return true;
		}

		@Override
		public boolean onError(RpError error) {
			return mCallback.onError(error);
		}

		@Override
		public boolean handlePending(Response<?> response) {
			// This method will not called.
			// Simple request on the URL will handled automatically.
            return mCallback.onError(response.getAnyError());
		}
		
	}

}
