/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.callback;

import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.error.RpError;

/**
 * This is callback for receiving errors from the server requests. All callbacks that
 * receive results from the server should extend this interface.
 * 
 * @author Denis Shurygin
 * 
 */
public interface ErrorCallback {

    /**
     * This method will called when an error is happened.
     * @param error Error object with error description.
     * @return True if error is handled, false otherwise.
     * All unhandled errors will invoke Broadcast message {@link BroadcastHelper#BROADCAST_ACTION_UNHANDLED_ERROR}.
     */
	public boolean onError(RpError error);
}
