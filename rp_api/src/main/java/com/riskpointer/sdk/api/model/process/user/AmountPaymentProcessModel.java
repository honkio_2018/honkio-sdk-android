/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.process.user;

import android.content.Context;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.api.model.entity.UserPayment;
import com.riskpointer.sdk.api.model.task.RequestTask;

import java.util.HashMap;

/**
 * Process model to do Amount payments.
 */
public class AmountPaymentProcessModel extends BasePaymentProcessModel<AmountPaymentProcessModel.AmountPayParams> {

	/**
	 * Construct new Amount Payment model.
	 */
	public AmountPaymentProcessModel() {
		super();
	}

	/**
	 * Starts payment process.
	 * @param context Application context.
	 * @param shop Shop which will process payment.
	 * @param orderId Order which will process payment.
	 * @param account Payment account which will be used for the payment.
	 * @param amount Amount of money that would be spend.
	 * @param currency Currency of the payment.
	 */
	public void pay(Context context, Shop shop, String orderId, UserAccount account, double amount, String currency) {
		pay(context, shop, orderId, account, amount, currency, 0);
	}

	/**
	 * Starts payment process.
	 * @param context Application context.
	 * @param shop Shop which will process payment.
	 * @param orderId Order which will process payment.
	 * @param account Payment account which will be used for the payment.
	 * @param amount Amount of money that would be spend.
	 * @param currency Currency of the payment.
	 * @param flags Flags for requests.
	 */
	public void pay(Context context, Shop shop, String orderId, UserAccount account, double amount, String currency, int flags) {
		pay(context, new AmountPayParams(shop, orderId, account, amount, currency, flags));
	}

	//// TODO: 2016-01-28 support orderId!!!!
	@Override
	protected RequestTask<UserPayment> doPayment(Context context, AmountPayParams payParams, HashMap<String, Object> additionalParams) {
		return HonkioApi.paymentRequest(
				payParams.amount,
				payParams.currency,
				payParams.getAccount(),
				payParams.getShop(),
				additionalParams,
				payParams.flags,
				getCallback());
	}

	/**
	 * Amount payment params.
	 */
	public static class AmountPayParams extends BasePaymentProcessModel.BasePayParams {

		/** Amount of money that would be spend. */
		public final double amount;
		/** Currency of the payment. */
		public final String currency;

		/**
		 * Construct new payment params.
		 * @param shop Shop which will process payment.
		 * @param orderId Order which will process payment.
		 * @param account Payment account which will be used for the payment.
		 * @param amount Amount of money that would be spend.
		 * @param currency Currency of the payment.
		 * @param flags Flags for requests.
		 */
		public AmountPayParams(Shop shop, String orderId, UserAccount account, double amount, String currency, int flags) {
			super(shop, orderId, account, flags);
			this.amount = amount;
			this.currency = currency;
		}

		@Override
		public BasePaymentProcessModel<?> instantiateProcessModel() {
			return new AmountPaymentProcessModel();
		}

		@Override
		public double getAmount() {
			return amount;
		}

		@Override
		public String getCurrency() {
			return currency;
		}
	}
}
