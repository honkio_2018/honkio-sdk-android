/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.entity;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

/**
 * Data object that hold information about server.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 *
 * @author Shurygin Denis
 */
public class ServerInfo implements Serializable{
	private static final long serialVersionUID = 1409954448845595206L;

	/** Language which should be used if server doesn't support device language. */
	public static final String DEFAULT_LANGUAGE = "en";
	
	private int mVersion;
	private List<String> mTimeZones;
	private List<Country> mCountries;
	private List<String> mLanguages;
	private String mVerifyUrl;
	private String mConsumerUrl;
	private double mServiceFee;
    private String mAssetsUrl;
	private CreditCallInfo mCreditCallInfo;
	private String mOAuthAuthorizeUrl;
	private String mOAuthTokenUrl;

	/**
	 * Gets version of the server.
	 * @return Version of the server.
	 */
	public int getVersion() {
		return mVersion;
	}

	/**
	 * Sets version of the server.
	 * @param version Version of the server.
	 */
	public void setVersion(int version) {
		this.mVersion = version;
	}

	public List<Country> getCountries() {
		return mCountries;
	}

	public void setCountries(List<Country> countries) {
		this.mCountries = countries;
	}

	public List<String> getTimeZones() {
		return mTimeZones;
	}

	public void setTimeZones(List<String> timeZones) {
		this.mTimeZones = timeZones;
	}

	/**
	 * Gets available server languages.
	 * @return Available server languages.
	 */
	public List<String> getLanguages() {
		return mLanguages;
	}

	/**
	 * Gets suitable language that server support.
	 * @return Suitable language that server support.
	 */
	public String getSuitableLanguage() {
		String deviceLanguage = Locale.getDefault().getLanguage();
		if (mLanguages != null && deviceLanguage != null)
			if (mLanguages.contains(deviceLanguage))
				return deviceLanguage;
		return DEFAULT_LANGUAGE;
	}

	/**
	 * Sets available server languages.
	 * @param languages Available server languages.
	 */
	public void setLanguages(List<String> languages) {
		this.mLanguages = languages;
	}

	/**
	 * Gets URL for SMS verification.
	 * @return URL for SMS verification.
	 */
	public String getVerifyUrl() {
		return mVerifyUrl;
	}


	/**
	 * Gets Service fee amount.
	 * @return Service fee amount.
	 */
	public double getServiceFee() {
		return mServiceFee;
	}

	/**
	 * Sets Service fee amount.
	 * @param fee Service fee amount.
	 */
	public void setServiceFee(double fee) {
		this.mServiceFee = fee;
	}

	//TODO DOCS!!!
	public String getConsumerUrl() {
		return mConsumerUrl;
	}

	//TODO DOCS!!!
	public void setConsumerUrl(String consumerUrl) {
		this.mConsumerUrl = consumerUrl;
	}


	/**
	 * Sets URL for SMS verification.
	 * @param verifyUrl URL for SMS verification.
	 */
	public void setVerifyUrl(String verifyUrl) {
		this.mVerifyUrl = verifyUrl;
	}

	/**
	 * Gets URL for Assets. This URl should be used to build url for concrete Asset file.
	 * @return URL for Assets. This URl should be used to build url for concrete Asset file.
	 */
    public String getAssetsUrl() {
        return mAssetsUrl;
    }

	/**
	 * Sets URL for Assets. This URl will be used to build url for concrete Asset file.
	 * @param assetsUrl URL for Assets.
	 */
    public void setAssetsUrl(String assetsUrl) {
        this.mAssetsUrl = assetsUrl;
    }

	/**
	 * Sets OAuth 2.0 authorize url.
	 * @param url OAuth 2.0 authorize url.
	 */
	public void setOAuthAuthorizeUrl(String url) {
		mOAuthAuthorizeUrl = url;
	}

	/**
	 * Gets OAuth 2.0 authorize url.
	 * @return OAuth 2.0 authorize url.
	 */
	public String getOAuthAuthorizeUrl() {
		return mOAuthAuthorizeUrl;
	}

	/**
	 * Sets OAuth 2.0 token url.
	 * @param url OAuth 2.0 token url.
	 */
	public void setOAuthTokenUrl(String url) {
		mOAuthTokenUrl = url;
	}

	/**
	 * Gets OAuth 2.0 token url.
	 * @return OAuth 2.0 token url.
	 */
	public String getOAuthTokenUrl() {
		return mOAuthTokenUrl;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;

		if (obj == null)
			return false;

		if (getClass() != obj.getClass())
			return false;

		ServerInfo info = (ServerInfo) obj;
		return mVersion == info.mVersion &&
				((mTimeZones != null && mTimeZones.equals(info.mTimeZones)) ||
						(mTimeZones == null && info.mTimeZones == null)) &&
				((mCountries != null && mCountries.equals(info.mCountries)) ||
						(mCountries == null && info.mCountries == null)) &&
				((mLanguages != null && mLanguages.equals(info.mLanguages)) ||
						(mLanguages == null && info.mLanguages == null)) &&
				TextUtils.equals(mVerifyUrl, info.mVerifyUrl) &&
				TextUtils.equals(mConsumerUrl, info.mConsumerUrl) &&
				mServiceFee == info.mServiceFee &&
				TextUtils.equals(mAssetsUrl, info.mAssetsUrl) &&
				TextUtils.equals(mOAuthAuthorizeUrl, info.mOAuthAuthorizeUrl) &&
				TextUtils.equals(mOAuthTokenUrl, info.mOAuthTokenUrl);
	}

	/**
	 * Data object that hold information about Country.
	 */
    public static class Country implements Serializable {
		private static final long serialVersionUID = 1L;
		
		private String mName;
		private String mNameTranslated;
		private String mLocale;
		private String mPhonePrefix;
		private String mIso;
		private String mTimeZone;

		/**
		 * Gets Name of the Country on English.
		 * @return Name of the Country on English.
		 */
		public String getName() {
			return mName;
		}

		/**
		 * Sets Name of the Country on English.
		 * @param name Name of the Country on English.
		 */
		public void setName(String name) {
			this.mName = name;
		}

		/**
		 * Gets Name of the Country on Phone Language.
		 * @return Name of the Country on Phone Language.
		 */
		public String getNameTranslated() {
			return mNameTranslated;
		}

		/**
		 * Sets Name of the Country on Phone Language.
		 * @param name Name of the Country on Phone Language.
		 */
		public void setNameTranslated(String name) {
			this.mNameTranslated = name;
		}

		public String getLocale() {
			return mLocale;
		}

		public void setLocale(String mLocale) {
			this.mLocale = mLocale;
		}

		public String getPhonePrefix() {
			return mPhonePrefix;
		}

		public void setPhonePrefix(String phonePrefix) {
			this.mPhonePrefix = phonePrefix;
		}

		public String getIso() {
			return mIso;
		}

		public void setIso(String mIso) {
			this.mIso = mIso;
		}

		public String getTimeZone() {
			return mTimeZone;
		}

		public void setTimeZone(String mTimeZone) {
			this.mTimeZone = mTimeZone;
		}

		/**
		 * Converts Country to the String.
		 * @return Name or Translated name if Name is null.
		 */
		@Override
		public String toString() {
			if(mName != null)
				return mName;
			return mNameTranslated;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof Country) {
				Country country = (Country) obj;
				return TextUtils.equals(mName, country.mName) &&
						TextUtils.equals(mNameTranslated, country.mNameTranslated) &&
						TextUtils.equals(mIso, country.mIso) &&
						TextUtils.equals(mLocale, country.mLocale) &&
						TextUtils.equals(mPhonePrefix, country.mPhonePrefix) &&
						TextUtils.equals(mTimeZone, country.mTimeZone);
			}
			return super.equals(obj);
		}
	}

	//==============================================================================================
	// Deprecated

	@Deprecated
	public void setCreditCallInfo(CreditCallInfo creditCallInfo) {
		mCreditCallInfo = creditCallInfo;
	}

	@Deprecated
	public CreditCallInfo getCreditCallInfo() {
		return mCreditCallInfo;
	}

	@Deprecated
	public static class CreditCallInfo {
		public final String url;
		public final String terminalId;
		public final String transactionKey;

		public CreditCallInfo(String url, String terminalId, String transactionKey) {
			this.url = url;
			this.terminalId = terminalId;
			this.transactionKey = transactionKey;
		}
	}
}
