package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.MediaFile;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author I.N.
 */

public class MediaFileJsonParser extends BaseJsonResponseParser<MediaFile> {
    @Override
    public MediaFile parseJson(Message<?> message, JSONObject json) {
        return parseMediaFile(json);
    }

    public static MediaFile parseMediaFile(JSONObject json) {
        MediaFile mediaFile = null;
        if (json != null) {
            mediaFile = new MediaFile();
            mediaFile.setUrl(json.optString("url"));
            mediaFile.setThumbnailUrl(json.optString("thumbnail_url"));
            mediaFile.setId(json.optString("id"));
            mediaFile.setTimestamp(json.optString("timestamp"));
            mediaFile.setAccess(json.optString("access"));
            mediaFile.setUserOwnerId(json.optString("user_owner"));
            mediaFile.setExtension(json.optString("extension"));
            if (json.has("metadata")) {
                Map<String, Object> metadata = new HashMap<>();
                JSONObject metadataJson = json.optJSONObject("metadata");
                if (metadataJson != null) {
                    Iterator<String> keysIterator = metadataJson.keys();
                    while (keysIterator.hasNext()) {
                        String key = keysIterator.next();
                        metadata.put(key, metadataJson.opt(key));
                    }
                }
                mediaFile.setMetaData(metadata);
            }
        }
        return mediaFile;
    }
}
