package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

// TODO write docs
/**
 * Created by Shurygin Denis on 2016-07-12.
 */
public class UserRolesList implements Serializable {

    private ArrayList<UserRole> mList;

    private HashMap<String, MerchantSimple> mMerchants;

    private String mRequestedUserId;
    private String[] mRequestedExtensionIds;

    public ArrayList<UserRole> getList() {
        return mList;
    }

    public void setList(ArrayList<UserRole> list) {
        mList = list;
    }

    public String getRequestedUserId() {
        return mRequestedUserId;
    }

    public void setRequestedUserId(String requestedUserId) {
        mRequestedUserId = requestedUserId;
    }

    public String[] getRequestedExtensionIds() {
        return mRequestedExtensionIds;
    }

    public void setRequestedExtensionIds(String[] requestedExtensionIds) {
        mRequestedExtensionIds = requestedExtensionIds;
    }

    public HashMap<String, MerchantSimple> getMerchants() {
        return mMerchants;
    }

    public void setMerchants(HashMap<String, MerchantSimple> merchants) {
        this.mMerchants = merchants;
    }
}
