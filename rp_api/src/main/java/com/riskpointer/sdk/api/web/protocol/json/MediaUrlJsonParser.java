package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.MediaUrl;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONObject;

/**
 * @author I.N.
 */

public class MediaUrlJsonParser extends BaseJsonResponseParser<MediaUrl> {
    @Override
    protected MediaUrl parseJson(Message<?> message, JSONObject json) {
        MediaUrl videoUrl = null;
        if (json != null) {
            videoUrl = new MediaUrl();
            videoUrl.setUploadUrl(json.optString("upload_url"));
            videoUrl.setUploadToken(json.optString("upload_token"));
            videoUrl.setId(json.optString("id"));
        }
        return videoUrl;
    }
}
