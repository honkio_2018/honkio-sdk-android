package com.riskpointer.sdk.api.model.entity;

/**
 * Created by I.N. on 2018-19-02.
 */

public class MediaUrl {
    private String mUploadUrl;
    private String mUploadToken;
    private String mId;

    public MediaUrl() {
    }

    public MediaUrl(String url, String token) {
        this.mUploadUrl = url;
        this.mUploadToken = token;
    }

    public String getUploadUrl() {
        return mUploadUrl;
    }

    public void setUploadUrl(String url) {
        this.mUploadUrl = url;
    }

    public void setUploadToken(String token) {
        mUploadToken = token;
    }

    public String getUploadToken() {
        return mUploadToken;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }
}
