package com.riskpointer.sdk.api.utils;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shurygin Denis on 2017-02-08.
 */

public class ApiLog {
    public static final String TAG = "HonkioApiDebug";

    public enum LogLevel implements Serializable {
        DEBUG("Debug"),
        ERROR("Error");

        private final String name;

        LogLevel(final String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public interface LogListener {
        void onLog(LogLevel level, String message);
    }

    private static final List<LogListener> sListeners = new ArrayList<>();

    private ApiLog() {}

    public static void d(String message) {
        log(LogLevel.DEBUG, message);
    }

    public static void e(String message) {
        log(LogLevel.ERROR, message);
    }

    public static void log(LogLevel level, String message) {
        switch (level) {
            case DEBUG:
                Log.d(ApiLog.TAG, message);
                break;
            case ERROR:
                Log.e(ApiLog.TAG, message);
                break;
            default:
                throw new IllegalArgumentException("Unknown log level");
        }
        synchronized (sListeners) {
            for (LogListener listener: sListeners) {
                listener.onLog(level, message);
            }
        }
    }

    public static void addListener(LogListener listener) {
        synchronized (sListeners) {
            sListeners.add(listener);
        }
    }

    public static void removeListener(LogListener listener) {
        synchronized (sListeners) {
            sListeners.remove(listener);
        }
    }
}
