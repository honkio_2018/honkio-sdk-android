/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Invoicer;
import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Parse User from JSON.
 */
public class UserJsonParser extends BaseJsonResponseParser<User> {

	@Override
	public User parseJson(Message<?> message, JSONObject json) {
		User result = new User();

		result.setLogin(json.optString(Message.Param.LOGIN_IDENTITY));

		result.setUserId(json.optString("id"));
		result.setFirstName(json.optString("consumer_str_firstname"));
		result.setLastName(json.optString("consumer_str_lastname"));
		result.setActive(json.optBoolean("consumer_active"));
		result.setRegistrationId(json.optString(Message.Param.USER_REGISTRATION));
		result.setTempPasswordUsed(json.optBoolean("lostpassword_used", false));
		result.setReceipt(json.optBoolean("consumer_bool_receipt"));
		result.setLanguage(json.optString("consumer_str_language"));
		result.setTimezone(json.optString("consumer_str_timezone"));
		result.setCountry(json.optString("consumer_str_country"));
		result.setState(json.optString("consumer_str_state"));
		result.setCity(json.optString("consumer_str_city"));
		result.setAddress1(json.optString("consumer_str_address1"));
		result.setAddress2(json.optString("consumer_str_address2"));
		result.setZip(json.optString("consumer_str_zip"));
		result.setPhone(json.optString("consumer_str_telephone"));
		result.setSsn(json.optString("consumer_str_ssn"));
        result.setOperatorBillingEnabled(json.optBoolean("operator_billing"));

		JSONObject touAppsJson = json.optJSONObject("consumer_dict_accepted_tou_version");
		if (touAppsJson != null) {
			HashMap<String, HashMap<String, User.TouVersion>> touApps = new HashMap<>(touAppsJson.length());
			Iterator<String> appsIterator = touAppsJson.keys();
			while (appsIterator.hasNext()) {
				HashMap<String, User.TouVersion> appTou = new HashMap<>();
				String appId = appsIterator.next();
				JSONObject touAppJson = touAppsJson.optJSONObject(appId);
				if (touAppJson != null) {
					Iterator<String> touKeys = touAppJson.keys();
					while (touKeys.hasNext()) {
						String touKey = touKeys.next();
						JSONObject touJson = touAppJson.optJSONObject(touKey);
						if (touJson != null) {
							appTou.put(touKey,
									new User.TouVersion(
											touJson.optInt("version", 0),
											ApiFormatUtils.stringDateToLong(touJson.optString("timestamp", null))
									)
							);
						}
					}
				}
				touApps.put(appId, appTou);
			}
			result.setTouVersions(touApps);
		}
		
		JSONArray accountsJson = json.optJSONArray("consumer_account");
		ArrayList<UserAccount> accounts = new ArrayList<>();
		if(accountsJson != null)
			for(int i = 0; i < accountsJson.length(); i++) {
				UserAccount account = new UserAccount();
				JSONObject accountJson = accountsJson.optJSONObject(i);
				account.setType(accountJson.optString("type"));
				account.setNumber(accountJson.optString("number"));
				account.setHasLeft(accountJson.has("left"));
				account.setLeft(accountJson.optDouble("left"));
				account.setLimit(accountJson.optDouble("limit"));
				account.setIsSigned(accountJson.optBoolean("sign", true));
				account.setDescription(accountJson.optString("description"));
				account.setLogoUrl(accountJson.optString("logo"));
				account.setIndex(i);
				accounts.add(account);
			}
		result.setAccountsList(accounts);
		
		JSONObject dictJson = json.optJSONObject("consumer_dict_keystore");
		if(dictJson != null) {
			@SuppressWarnings("unchecked")
			Iterator<String> keys = dictJson.keys();
			while(keys.hasNext()) {
				String key = keys.next();
				result.setDictValue(key, dictJson.optString(key));
			}
		}

        JSONObject invoicersJson = json.optJSONObject(Message.Param.USER_INVOICERS);
        if(invoicersJson != null) {
            @SuppressWarnings("unchecked")
            Iterator<String> keys = invoicersJson.keys();
            while(keys.hasNext()) {
                String key = keys.next();
                JSONObject invocerJson = invoicersJson.optJSONObject(key);
                if (invocerJson != null) {
                    Invoicer invoicer = new Invoicer();
                    invoicer.setId(key);
                    invoicer.setName(invocerJson.optString("name"));
                    invoicer.setNote(invocerJson.optString("note"));
                    invoicer.setMore(invocerJson.optString("more"));
                    invoicer.setRef(invocerJson.optString("ref"));
                    invoicer.setBic(invocerJson.optString("bic"));

                    result.addInvoicer(invoicer);
                }
            }
        }

		return result;
	}

}
