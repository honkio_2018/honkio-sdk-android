/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.callback;

/**
 * Callback for receiving info about success request.
 * 
 * @author Denis Shurygin
 *
 */
public interface SimpleCallback extends ErrorCallback {
	/**
	 * This method will called when the server returned result and it's not error.
	 */
	public void onComplete();
}
