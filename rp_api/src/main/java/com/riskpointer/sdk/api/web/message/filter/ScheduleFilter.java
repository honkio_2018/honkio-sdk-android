package com.riskpointer.sdk.api.web.message.filter;

import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;

import java.util.HashMap;

//TODO write docs

/**
 * Created by Shurygin Denis on 2016-05-31.
 */
public class ScheduleFilter extends AbsFilter {

    private Long mStartDate;
    private Long mEndDate;
    private String mObjectId;
    private String mObjectType;
    private String mQueryType;

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = new HashMap<>();
        applyIfNotNull(params, Message.Param.QUERY_START_DATE, dateToString(getStartDate()));
        applyIfNotNull(params, Message.Param.QUERY_END_DATE, dateToString(getEndDate()));
        applyIfNotNull(params, "query_object", mObjectId);
        applyIfNotNull(params, "query_object_type", mObjectType);
        applyIfNotNull(params, "query_type", mQueryType);
        return params;
    }

    public Long getStartDate() {
        return mStartDate;
    }

    public void setStartDate(Long date) {
        this.mStartDate = date;
    }

    public Long getEndDate() {
        return mEndDate;
    }

    public void setEndDate(Long date) {
        this.mEndDate = date;
    }

    protected String dateToString(Long date) {
        if (date == null)
            return null;

        return ApiFormatUtils.dateToServerFormat(date);
    }

    public String getObjectId() {
        return mObjectId;
    }

    public void setObjectId(String id) {
        this.mObjectId = id;
    }

    public String getObjectType() {
        return mObjectType;
    }

    public void setObjectType(String objectType) {
        this.mObjectType = objectType;
    }

    public String getQueryType() {
        return mQueryType;
    }

    public void setQueryType(String queryType) {
        mQueryType = queryType;
    }
}
