/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.entity;

import android.text.TextUtils;

import com.riskpointer.sdk.api.web.message.filter.AbsFilter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Data object that hold base information about merchant.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 *
 * @author Shurygin Denis
 */
public class MerchantSimple implements Serializable {
	private static final long serialVersionUID = 1L;

	private String mId;
	private String mName;
	private String mBusinessId;
	private String mCurrency;
	private String mLogoUrl;

	private String mSupportEmail;
	private String mSupportPhone;

	private String[] mSupportedAccounts;
    private Identity mLoginShopIdentity;
    private Identity mRegistrationShopIdentity;
	private ArrayList<Integer> mVatClasses;

	public String getId() {
		return mId;
	}

	public void setId(String id) {
		this.mId = id;
	}

	public String getName() {
		return mName;
	}

	public void setName(String name) {
		this.mName = name;
	}

	public String getBusinessId() {
		return mBusinessId;
	}

	public void setBusinessId(String businessId) {
		this.mBusinessId = businessId;
	}

	public String getCurrency() {
		return mCurrency;
	}

	public void setCurrency(String currency) {
		this.mCurrency = currency;
	}

	public String getLogoUrl() {
		return mLogoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		mLogoUrl = logoUrl;
	}

	public String getSupportEmail() {
		return mSupportEmail;
	}

	public void setSupportEmail(String mSupportEmail) {
		this.mSupportEmail = mSupportEmail;
	}

	public String getSupportPhone() {
		return mSupportPhone;
	}

	public void setSupportPhone(String mSupportPhone) {
		this.mSupportPhone = mSupportPhone;
	}

	/**
	 * Sets Login shop identity. This Identity should be used in the requests with login and password.
	 * @param identity Login shop identity. This Identity should be used in the requests with login and password.
	 */
    public void setLoginShopIdentity(Identity identity) {
        mLoginShopIdentity = identity;
    }

	/**
	 * Gets Login shop identity. This Identity should be used in the requests with login and password.
	 * @return Login shop identity. This Identity should be used in the requests with login and password.
	 */
    public Identity getLoginShopIdentity() {
        return mLoginShopIdentity;
    }

	/**
	 * Sets Registration shop identity. This Identity should be used in the registration requests.
	 * @param identity Registration shop identity. This Identity should be used in the registration requests.
	 */
    public void setRegistrationShopIdentity(Identity identity) {
        mRegistrationShopIdentity = identity;
    }

	/**
	 * Gets Registration shop identity. This Identity should be used in the registration requests.
	 * @return Registration shop identity. This Identity should be used in the registration requests.
	 */
	@Deprecated
    public Identity getRegistrationShopIdentity() {
        if(!isRegistrationShopIdentityExists())
            return getLoginShopIdentity();
        return mRegistrationShopIdentity;
    }

	/**
	 * Gets array of the Payment Account types which supported by the Merchant.
	 * @return Array of the Payment Account types which supported by the Merchant.
	 * @see UserAccount.Type
	 */
	public String[] getSupportedAccounts() {
		return mSupportedAccounts;
	}

	/**
	 * Sets array of the Payment Account types which supported by the Merchant.
	 * @param accountTypes Array of the Payment Account types.
	 * @see UserAccount.Type
	 */
	public void setSupportedAccounts(String[] accountTypes) {
		this.mSupportedAccounts = accountTypes;
	}

	/**
	 * Gets list of the VAT percent which supported by the Merchant (depending by country).
	 * @return List of the VAT percent which supported by the Merchant.
	 */
	public ArrayList<Integer> getVATClasses() {
		return mVatClasses;
	}

	/**
	 * Sets list of the VAT percent which supported by the Merchant (depending by country).
	 * @param vatClasses List of the VAT percents.
	 */
	public void setVatClasses(ArrayList<Integer> vatClasses) {
		this.mVatClasses = vatClasses;
	}

	/**
	 * Check if specified Payment Account type is supported by the Merchant.
	 * @param accountType Payment Account type.
	 * @return True if account supported by the Merchant, false otherwise.
	 * @see UserAccount.Type
	 */
	public boolean isAccountSupported(String accountType) {
		if (mSupportedAccounts != null)
			for(String supportedAccount: mSupportedAccounts)
				if(supportedAccount.equals(accountType))
					return true;
		return false;
	}

	public boolean isRegistrationShopIdentityExists() {
		return mRegistrationShopIdentity != null && !mRegistrationShopIdentity.isCorrupt();
	}

	public HashMap<String, Object> toParams() {
		HashMap<String, Object> hashMap = new HashMap<>();

		AbsFilter.applyIfNotNull(hashMap, "str_name", mName);
		AbsFilter.applyIfNotNull(hashMap, "str_businessid", mBusinessId);

		AbsFilter.applyIfNotNull(hashMap, "str_support_email", mSupportEmail);
		AbsFilter.applyIfNotNull(hashMap, "str_support_telephone", mSupportPhone);

		return hashMap;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;

		if (obj == null)
			return false;

		if (getClass() != obj.getClass())
			return false;

		MerchantSimple merchant = (MerchantSimple) obj;
		return TextUtils.equals(mId, merchant.mId) &&
				TextUtils.equals(mName, merchant.mName) &&
				TextUtils.equals(mBusinessId, merchant.mBusinessId) &&
				TextUtils.equals(mSupportEmail, merchant.mSupportEmail) &&
				TextUtils.equals(mSupportPhone, merchant.mSupportPhone) &&
				Arrays.equals(mSupportedAccounts, merchant.mSupportedAccounts) &&
				((mLoginShopIdentity != null && mLoginShopIdentity.equals(merchant.mLoginShopIdentity)) ||
						(mLoginShopIdentity == null && merchant.mLoginShopIdentity == null));
	}
}
