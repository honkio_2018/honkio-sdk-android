/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.utils.ApiHash;
import com.riskpointer.sdk.api.web.protocol.RequestBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

/**
 * The builder to create request string in JSON format.
 * 
 * @author Denis Shurygin
 * 
 */
public class JsonRequestBuilder implements RequestBuilder {

	@Override
	public String build(Map<String, Object> map) {
		JSONObject jsonObject = new JSONObject();
		put(jsonObject, map);
		return jsonObject.toString();
	}

	@Override
	public String calculateHash(Map<String, Object> map, String hashKey) {
		String jsonString = build(map);
		try {
			JSONObject jsonObjectForCalculation = new JSONObject(jsonString);
			String controlString = ApiHash.findValues(jsonObjectForCalculation).trim();
//			ApiLog.d("Control string: " + controlString);
			return ApiHash.sha512(controlString, hashKey);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void put(JSONObject json, String name, Object value) {
		if(json != null) try {
			json.put(name, wrap(value));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void put(JSONObject json, Map<String, Object> map) {
		if (map != null) {
			for (String key: map.keySet()) {
				if (key != null)
					put(json, key, map.get(key));
			}
		}
	}

	static Object wrapMap(Map map) {
		JSONObject json = new JSONObject();
		for(Object key : map.keySet()) {
			try {
				json.put((String) key, wrap(map.get(key)));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return json;
	}

	static JSONArray wrapCollection(Collection collection) {
		JSONArray jsonArray = new JSONArray();
		for(Object object : collection) {
				jsonArray.put(wrap(object));
		}
		return jsonArray;
	}

	static Object wrap(Object o) {
		if (o == null) {
			return JSONObject.NULL;
		}
		if (o instanceof JSONArray || o instanceof JSONObject) {
			return o;
		}
		if (o.equals(JSONObject.NULL)) {
			return o;
		}
		try {
			if (o instanceof Collection) {
				return wrapCollection((Collection) o);
			} else if (o.getClass().isArray()) {
				return new JSONArray(Arrays.asList((Object[]) o));
			}
			if (o instanceof Map) {
				return wrapMap((Map) o);
			}
			if (o instanceof Boolean ||
					o instanceof Byte ||
					o instanceof Character ||
					o instanceof Double ||
					o instanceof Float ||
					o instanceof Integer ||
					o instanceof Long ||
					o instanceof Short ||
					o instanceof CharSequence) {
				return o;
			}
			if (o.getClass().getPackage().getName().startsWith("java.")) {
				return o.toString();
			}
		} catch (Exception ignored) {
		}
		return null;
	}

	@Override
	public boolean equals(Object obj) {
		return obj != null && obj.getClass() == getClass();
	}
}
