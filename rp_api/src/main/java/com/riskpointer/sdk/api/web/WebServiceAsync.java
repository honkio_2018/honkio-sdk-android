/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web;

import android.content.Context;

import com.riskpointer.sdk.api.model.task.RequestTask;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.support.HoneycombAsyncTask;
import com.riskpointer.sdk.api.utils.ApiLog;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.callback.RequestCallbackWrapper;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.api.web.webtask.WebTask;
import com.riskpointer.sdk.api.web.webtask.WebTaskFactory;

import java.io.File;
import java.io.InputStream;
import java.util.concurrent.Executor;

import okhttp3.MediaType;

/**
 * Async task that send requests on the server.<br>
 *  WebServiceAsync has 3 queues for different priority messages.<br>
 *  Low Priority - for no important tasks, will execute only if no eny High Priority task. Set flag FLAG_LOW_PRIORITY into Message object to put task in to Low Priority queue.<br>
 *  High Priority - default queue. Next task will start to execute when previous task is finished.<br>
 *  No queue - When message doesn't has Connection object or it's offline request. Tasks are performed in parallel threads.<br>
 *  <br><br>
 *  WebServiceAsync implements {@link Task} interface, so you have opportunity to abort task. See abort().
 *
 * @param <ResponseResult> Type of Response.
 */
public class WebServiceAsync<ResponseResult>  extends HoneycombAsyncTask<Void, Void, Response<ResponseResult>> implements RequestTask<ResponseResult>, Cloneable {

	public static boolean PRINT_DEBUG_STACKTRACE = false;
	public static boolean PRINT_QUEUE_TRACKING = true;

	private Context mContext;
	private WebTask<ResponseResult> mWebWebTask;
	private RequestCallbackWrapper<ResponseResult> mCallback;
	private StackTraceElement[] mInitialCallStackTrace;

	/**
	 * Construct new WebServiceAsync.
	 *
	 * @param context Application Context.
	 * @param message Message object.
	 * @param callback The Object that implement an interface RequestCallback
	 *            {@literal <}ResponseResult>. This object will be used
	 *            for sending callbacks in to UI thread.
	 */
	public WebServiceAsync(Context context, Message<ResponseResult> message,
						   RequestCallback<ResponseResult> callback) {
		this(context, WebTaskFactory.getInstance().buildTask(context, message), callback, true);
	}

	/**
	 * Construct new WebServiceAsync.
	 *
	 * @param context  Application Context.
	 * @param message  Message object.
	 * @param callback The Object that implement an interface RequestCallback
	 *                 {@literal <}ResponseResult>. This object will be used
	 */
	public WebServiceAsync(Context context, Message<ResponseResult> message, File file,
						   RequestCallback<ResponseResult> callback) {
		this(context, WebTaskFactory.getInstance().buildTask(context, message, file),
				callback, true);
	}

	/**
	 * Construct new WebServiceAsync.
	 *
	 * @param context  Application Context.
	 * @param message  Message object.
	 * @param callback The Object that implement an interface RequestCallback
	 *                 {@literal <}ResponseResult>. This object will be used
	 */
	public WebServiceAsync(Context context, Message<ResponseResult> message,
						   InputStream inputStream, MediaType mediaType,
						   RequestCallback<ResponseResult> callback) {
		this(context, WebTaskFactory.getInstance()
				.buildTask(context, message, inputStream, mediaType), callback, true);
	}

	private WebServiceAsync(Context context, WebTask<ResponseResult> webwebTask,
							RequestCallback<ResponseResult> callback, boolean reWrap) {
		super();
		mContext = context;
		mWebWebTask = webwebTask;
		if (!reWrap && callback instanceof RequestCallbackWrapper)
			mCallback = (RequestCallbackWrapper<ResponseResult>) callback;
		else
			mCallback = new RequestCallbackWrapper<>(callback);
	}

	@Override
	public WebServiceAsync<ResponseResult> clone() {
		return new WebServiceAsync<>(mContext, (WebTask<ResponseResult>) mWebWebTask.clone(), mCallback, false);
	}

	@Override
	public WebServiceAsync<ResponseResult> execute(Void... params) {
		mInitialCallStackTrace = Thread.getAllStackTraces().get(Thread.currentThread());
		Executor executor = getSuitableExecutor(mWebWebTask.getMessage());

		if (PRINT_QUEUE_TRACKING) {
			ApiLog.d("Message '" + mWebWebTask.getMessage().getCommand()
					+ "' added to queue. Executor: " + executor.getClass().getSimpleName());
		}

		return (WebServiceAsync<ResponseResult>) executeOnExecutor(executor, params);
	}

	@Override
	protected Response<ResponseResult> doInBackground(Void... params) {
		if (PRINT_DEBUG_STACKTRACE && mInitialCallStackTrace != null) {
			StringBuilder builder = new StringBuilder("DEBUG STACKTRACE: ");
			builder.append(mWebWebTask.getMessage().getCommand());
			for (StackTraceElement element: mInitialCallStackTrace) {
				String elementString = element.toString();
				if (!elementString.startsWith("android.") &&
						!elementString.startsWith("com.android.") &&
						!elementString.startsWith("dalvik.system.") &&
						!elementString.startsWith("java.lang.")) {
					builder.append("\n");
					builder.append(elementString);
				}
			}
			ApiLog.d(builder.toString());
		}
		return mWebWebTask.execute();
	}

	@Override
	protected void onPostExecute(Response<ResponseResult> result) {
		if(result != null && result.isStatusError())
			performOnError(result.getAnyError());
		else if (mWebWebTask.getError().code != RpError.Api.NONE)
			performOnError(mWebWebTask.getError());
		else if (result != null)
			performOnComplete(result);
		else
			throw new IllegalStateException();
	}

	@Override
	protected void onCancelled() {
		if(mWebWebTask.getError().code != RpError.Api.NONE)
			performOnError(mWebWebTask.getError());
	}

	@Override
	public void setCallback(RequestCallback<ResponseResult> callback, boolean deliverResult) {
		mCallback.set(callback);
		if (mWebWebTask.isCompleted() && deliverResult) {
			if (isCancelled())
				onCancelled();
			else {
				onPostExecute(mWebWebTask.getResponse());
			}
		}
	}

	/**
	 *  Abortion of the task. Is different for different priority.<br>
	 *  Low Priority and High Priority - can be aborted immediately only if task is in queue and not executed, otherwise abortion callback will send when execution completed.<br>
	 *  No queue - Even the task already executed and HTTP connection is created, the task will be aborted.
	 *  <br><br>
	 *  Aborted task calls oError with the error code ABORTED.
	 */
	@Override
	public boolean abort() {
		if (mWebWebTask.abort())
			cancel(false);
		return mWebWebTask.isAborted();
	}

	@Override
	public boolean isAborted() {
		return mWebWebTask.isAborted();
	}

	@Override
	public boolean isCompleted() {
		return mWebWebTask.isCompleted();
	}

	/**
	 * Gets Application context which was passed into constructor.
	 * @return Application context.
	 */
	public Context getContext() {
		return mContext;
	}

	/**
	 * Invoke onComplete method of the Callback.
	 * @param response Object that represent the server response.
	 * @return Result of onComplete method. True if response was handled, false otherwise.
	 * All unhandled responses will invoke Broadcast message {@link BroadcastHelper#BROADCAST_ACTION_UNHANDLED_RESPONSE}.
	 */
	protected boolean invokeOnComplete(Response<ResponseResult> response) {
		return mCallback.onComplete(response);
	}

	/**
	 * Invoke onError method of the Callback.
	 * @param error Error object with error description.
	 * @return Result of onComplete method. True if error is handled, false otherwise.
	 * All unhandled errors will invoke Broadcast message {@link BroadcastHelper#BROADCAST_ACTION_UNHANDLED_ERROR}.
	 */
	protected boolean invokeOnError(RpError error) {
		return mCallback.onError(error);
	}

	private void performOnError(RpError error) {
		boolean isErrorHandled = invokeOnError(error);
		if (!isErrorHandled && !mWebWebTask.getMessage().isHasFlag(Message.FLAG_NO_ERROR_WARNING)) {
			BroadcastHelper.sendActionUnhandledError(mContext,
					mWebWebTask.getMessage().getCommand(), error, mInitialCallStackTrace);
		}
	}

	private void performOnComplete(Response<ResponseResult> response) {
		boolean isHandled = invokeOnComplete(response);

		if (!isHandled && !mWebWebTask.getMessage().isHasFlag(Message.FLAG_NO_RESPONSE_WARNING))
			BroadcastHelper.sendActionUnhandledResponse(mContext,
					mWebWebTask.getMessage().getCommand(), response, mInitialCallStackTrace);

		if (response != null && response.isStatusAccept())
			BroadcastHelper.sendActionOnComplete(getContext(), mWebWebTask.getMessage().getBroadcastCommand(), response);
	}

	public static Executor getSuitableExecutor(Message<?> message) {
		if (message.isHasFlag(Message.FLAG_LOW_PRIORITY))
			return HoneycombAsyncTask.LOW_PRIORITY_SERIAL_EXECUTOR;
		if (message.isHasFlag(Message.FLAG_HIGH_PRIORITY))
			return HoneycombAsyncTask.HIGH_PRIORITY_SERIAL_EXECUTOR;
		if (message.isOffline())
			return HoneycombAsyncTask.THREAD_POOL_EXECUTOR;
		if (message.getConnection() == null)
			return HoneycombAsyncTask.THREAD_POOL_EXECUTOR;
		return HoneycombAsyncTask.HIGH_PRIORITY_SERIAL_EXECUTOR;
	}
}