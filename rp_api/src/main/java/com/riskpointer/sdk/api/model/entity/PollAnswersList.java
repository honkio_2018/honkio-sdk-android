package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by I.N. on 18.02.2019
 */
public class PollAnswersList implements Serializable {

    private Map<String, Integer> mPollAnswers;

    public PollAnswersList() {
    }

    public Map<String, Integer> getList() {
        return mPollAnswers;
    }

    public void setList(Map<String, Integer> answerList) {
        mPollAnswers = answerList;
    }

    public int getAnswerForOption(String option) {
        return mPollAnswers.get(option);
    }

    public void addAnswer(String option, int value) {
        if (mPollAnswers == null)
            mPollAnswers = new HashMap<>();
        mPollAnswers.put(option, value);
    }
}
