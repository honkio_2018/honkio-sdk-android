/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.entity;

/**
 * Data object that hold information about one bought product position.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 *
 * @author Denis Shurygin
 */
public class BoughtProduct extends Product {
	private static final long serialVersionUID = 2L;
	private int mCount;
	private long mValidFrom;
	private long mValidTo;
	private String mSignature;
	private boolean isValid;
	private String mTransactionId;

	public BoughtProduct() {}

	public BoughtProduct(BoughtProduct product) {
		super(product);

		this.mCount = product.mCount;
		this.mValidFrom = product.mValidFrom;
		this.mValidTo = product.mValidTo;
		this.mSignature = product.mSignature;
		this.isValid = product.isValid;
		this.mTransactionId = product.mTransactionId;
	}

	/**
	 * Gets count of bought products.
	 * @return Count of bought products.
	 */
	public int getCount() {
		return mCount;
	}

	/**
	 * Sets count of bought products.
	 * @return Count of bought products.
	 */
	public void setCount(int cunt) {
		this.mCount = cunt;
	}

	/**
	 * Gets start of validity period in millis.
	 * @return Start of validity period in millis.
	 */
	public long getValidFrom() {
		return mValidFrom;
	}

	/**
	 * Sets start of validity period in millis.
	 * @param dateTime Start of validity period in millis.
	 */
	public void setValidFrom(long dateTime) {
		this.mValidFrom = dateTime;
	}

	/**
	 * Gets end of validity period in millis.
	 * @return End of validity period in millis.
	 */
	public long getValidTo() {
		return mValidTo;
	}

	/**
	 * Sets end of validity period in millis.
	 * @param dateTime End of validity period in millis.
	 */
	public void setValidTo(long dateTime) {
		this.mValidTo = dateTime;
	}

	/**
	 * Gets how many time in millis left until the validity period finished.
	 * @return Time in millis that left until the validity period finished.
	 */
	public long getValidLeft(long time) {
		return getValidTo() - time;
	}

	@Override
	public boolean isHasTimeValidity() {
		return (true || super.isHasTimeValidity()) && mValidFrom > 0 && mValidTo > 0;
	}

	public String getSignature() {
		return mSignature;
	}

	public void setSignature(String mSignature) {
		this.mSignature = mSignature;
	}

	/**
	 * Check validity value of this Product. Validity period ignored.
	 * @return Value of validity of this product.
	 */
	public boolean isValid() {
		return isValid;
	}

	/**
	 * Check validity value and validity period.
	 * @param time Time in millis for which there is a check.
	 * @return True if validity value is true and time is in validity period, false otherwise.
	 */
	public boolean isValid(long time) {
		return isValid && getValidLeft(time) > 0;
	}

	/**
	 * Sets validity value.
	 * @param isValid Validity value
	 */
	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	/**
	 * Gets Transaction ID of this Bought Product.
	 * @return Transaction ID of this Bought Product.
	 */
	public String getTransactionId() {
		return mTransactionId;
	}

	/**
	 * Sets Transaction ID of this Bought Product.
	 * @param transactionId Transaction ID of this Bought Product.
	 */
	public void setTransactionId(String transactionId) {
		this.mTransactionId = transactionId;
	}
}
