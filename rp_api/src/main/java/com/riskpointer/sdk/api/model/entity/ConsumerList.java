package com.riskpointer.sdk.api.model.entity;

import java.util.List;

/**
 * Created by I.N. on 2017-02-16.
 */

public class ConsumerList {

    private List<Consumer> mList;

    public List<Consumer> getList() {
        return mList;
    }

    public void setList(List<Consumer> list) {
        mList = list;
    }

    private List<UserRoleDescription> mRolesDescription;

    public List<UserRoleDescription> getRolesDescription() {
        return mRolesDescription;
    }

    public void setRolesDescription(List<UserRoleDescription> rolesDescription) {
        mRolesDescription = rolesDescription;
    }
}
