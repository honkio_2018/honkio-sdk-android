package com.riskpointer.sdk.api.web.protocol.json.merchant;

import com.riskpointer.sdk.api.model.entity.merchant.Merchant;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.protocol.json.BaseJsonResponseParser;
import com.riskpointer.sdk.api.web.protocol.json.ParseHelper;

import org.json.JSONObject;

/**
 * @author Shurygin Denis
 */

public class MerchantJsonParser extends BaseJsonResponseParser<Merchant> {

    @Override
    protected Merchant parseJson(Message<?> message, JSONObject json) {
        Merchant merchant = new Merchant();

        merchant.setId(json.optString("id", ""));
        JSONObject merchantJson = json.optJSONObject("merchant");

        if (merchantJson != null) {
            ParseHelper.parseMerchant(merchantJson, merchant);
        }

        return merchant;
    }
}
