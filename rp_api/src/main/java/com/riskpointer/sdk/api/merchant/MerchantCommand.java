package com.riskpointer.sdk.api.merchant;

/**
 * Created by Shurygin Denis on 2016-11-08.
 */

public class MerchantCommand {

    public static final String MERCHANT_GET = "merchantget";
    public static final String MERCHANT_SET = "merchantset";
    public static final String MERCHANT_GET_LIST = "merchantlist";
    public static final String MERCHANT_CONSUMER_LIST = "merchantconsumerlist";
    public static final String MERCHANT_CONSUMER_LIST_BY_ROLE = "merchantconsumerlistbyroles";
    public static final String MERCHANT_GET_PRODUCT = "merchantproductget";
    public static final String MERCHANT_GET_PRODUCT_LIST = "merchantproductlist";
    public static final String MERCHANT_PRODUCT_SET = "merchantproductset";
    public static final String MERCHANT_GET_ORDERS = "merchantgetorders";
    public static final String MERCHANT_SET_ORDER = "merchantorderset";
    public static final String MERCHANT_SET_ORDERS = "merchantmultipleordersset";
    public static final String MERCHANT_SET_SHOP = "merchantshopset";
    public static final String MERCHANT_QR_SET = "merchantqrset";
    public static final String MERCHANT_ASSET_SET = "merchantassetset";
    public static final String MERCHANT_ASSET_LIST = "merchantassetlist";
    public static final String MERCHANT_SET_EVENT = "merchanteventset";
    public static final String MERCHANT_USER_ROLES_LIST = "merchantlistuserroles";
    public static final String MERCHANT_USER_ROLE_SET = "merchantuserroleset";

    public static final String MERCHANT_UPDATE_TRANSACTION = "merchanttransactionupdate";
    public static final String MERCHANT_LIST_TRANSACTIONS = "merchantlisttransactions";

    public static final String MERCHANT_GET_SHOP_LIST = "merchantshoplist";
    public static final String MERCHANT_GET_SHOP = "merchantshopget";
    public static final String MERCHANT_INVENTORY_CONSUME = "merchantinventoryconsume";
    public static final String MERCHANT_ASSET_STRUCTURE_LIST = "merchantassetstructurelist";
    public static final String MERCHANT_ASSET_STRUCTURE_SET = "merchantassetstructureset";


    public static final String USER_MERCHANT_CREATE = "usermerchantcreate";

    public static final String MERCHANT_SEND_CHAT_MESSAGE = "merchantsendchatmsg";
    public static final String MERCHANT_GET_CHAT = "merchantgetchat";
    public static final String MERCHANT_VOTE = "merchantvote";
    public static final String MERCHANT_REPORT_RUN = "merchantreportrun";
    public static final String MERCHANT_GET_EARNINGS = "merchantgetearnings";

    public static final String MERCHANT_SET_LINK = "merchantlinkset";
    public static final String MERCHANT_GET_LINK = "merchantlinklist";

    public static final String MERCHANT_POLL_SET = "merchantpollset";
}
