/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;

import java.text.DecimalFormat;

import static android.R.attr.permission;

/**
 * Helper class which provide easy way to get device location.
 */
public class LocationFinder {
    private static final DecimalFormat sLocationFormat = new DecimalFormat("#.########");
    private static final long UPDATE_TIMEOUT = 60000;
    private static final long UPDATE_DISTANCE = 1000;
    private static final long STOP_TIMEOUT = 10000;
    private static final long STOP_FORCE_TIMEOUT = STOP_TIMEOUT + 5000;

    private Handler mHandler;
    private Context mContext;
    private LocationManager mLocationManager;
    private Location mLocation = null;
    private String mProvider;

    private volatile boolean isRunning = false;
    private volatile long mLastUsage;
    private StopThread mStopThread;

    /**
     * Construct new Location Finder.
     * @param context Application context.
     */
    public LocationFinder(Context context) {
        mHandler = new Handler();
        mContext = context;
        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        mProvider = getDefaultProvider(mLocationManager);

        if (mProvider != null && checkLocationPermission()) {
            mLocation = mLocationManager.getLastKnownLocation(mProvider);
            start(mProvider);
        }
    }

    /**
     * Gets Device location.
     * @return Device location.
     */
    public synchronized Location getLocation() {
        if (mProvider == null)
            mProvider = getDefaultProvider(mLocationManager);

        if (mProvider != null) {
            start(mProvider);
            restartStopThread();
        }
        return mLocation;
    }

    /**
     * Stops location updates.
     */
    public void stop() {
        isRunning = false;
        mLastUsage = 0;
        mProvider = getDefaultProvider(mLocationManager);
    }

    public void stopForce() {
        stop();
        removeLocationListener();
    }

    public boolean isLocationEnabled() {
        return mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
                || mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static String formatLocation(double value) {
        return sLocationFormat.format(value).replaceAll(",", ".");
    }

    private void start(String provider) {
        if(!isRunning && mLocationManager.isProviderEnabled(provider)) {
            isRunning = checkLocationPermission();
            mProvider = provider;
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (checkLocationPermission())
                        mLocationManager.requestLocationUpdates(mProvider, UPDATE_TIMEOUT,
                                UPDATE_DISTANCE, mLocationListener);
                }
            });
            mLastUsage = System.currentTimeMillis();
            restartStopThread();
        }
    }

    private void restart(String provider) {
        stop();
        if (provider != null)
            start(provider);
    }

    private void restartStopThread() {
        mLastUsage = System.currentTimeMillis();
        if (mStopThread != null) {
            mStopThread.restart();
        }
        else {
            mStopThread = new StopThread();
            mStopThread.start();
        }
    }

    private class StopThread extends Thread {
        @Override
        public void run() {
            do {
                do {
                    try {
                        Thread.sleep(STOP_TIMEOUT);
                    } catch (InterruptedException e) {
                        interrupted();
                    }
                } while (mLastUsage + STOP_TIMEOUT > System.currentTimeMillis());

                LocationFinder.this.stop();

                try {
                    Thread.sleep(STOP_FORCE_TIMEOUT - STOP_TIMEOUT);
                } catch (InterruptedException e) {
                    interrupted();
                }
            } while (mLastUsage + STOP_FORCE_TIMEOUT> System.currentTimeMillis());

            removeLocationListener();

            mStopThread = null;
        }

        public synchronized void restart() {
            interrupt();
        }
    }

    private String getDefaultProvider(LocationManager locationManager) {
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            return LocationManager.NETWORK_PROVIDER;
        }
        else {
            Criteria criteria = new Criteria();
            return locationManager.getBestProvider(criteria, true);
        }
    }

    private void removeLocationListener() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (checkLocationPermission())
                    mLocationManager.removeUpdates(mLocationListener);
            }
        });
    }

    private LocationListener mLocationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            if(location != null) {
                mLocation = location;
                BroadcastHelper.sendActionOnLocationChanged(mContext, location);
                if (!LocationManager.GPS_PROVIDER.equals(mProvider) && mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    mProvider = LocationManager.GPS_PROVIDER;
                    restart(mProvider);
                }
            }
            unregisterIfStopped();
        }

        @Override
        public synchronized void onProviderDisabled(String provider) {
            unregisterIfStopped();
        }

        @Override
        public void onProviderEnabled(String provider) {
            unregisterIfStopped();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            unregisterIfStopped();
        }

        private void unregisterIfStopped() {
            if (!isRunning) {
                removeLocationListener();
            }
        }
    };

    private boolean checkLocationPermission() {
        return mContext.checkCallingOrSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_DENIED;
    }
}
