package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.HKLink;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONObject;

import java.util.HashMap;

public class HKLinkJsonParser extends BaseJsonResponseParser<HKLink> {

    @Override
    protected HKLink parseJson(Message<?> message, JSONObject json) {

        HKLink link;

        JSONObject metaJson = json.optJSONObject("metadata");
        if (metaJson != null)
            link = new HKLink((HashMap<String, Object>) jsonToMap(metaJson));
        else
            link = new HKLink();

        link.setId(json.optString("id"));
        link.setType(json.optString("type_id"));

        link.setFrom(
                json.optString("from_object"),
                json.optString("from_object_type")
        );

        link.setTo(
                json.optString("to_object"),
                json.optString("to_object_type")
        );

        return link;
    }
}
