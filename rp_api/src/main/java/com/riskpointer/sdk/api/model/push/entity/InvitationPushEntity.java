package com.riskpointer.sdk.api.model.push.entity;

import com.riskpointer.sdk.api.model.entity.Invitation;
import com.riskpointer.sdk.api.model.push.Push;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by I.N. 29/10/2018
 */
public class InvitationPushEntity extends Push.PushEntity implements Serializable {

    private Invitation mInvitation;

    public InvitationPushEntity(Map<String, Object> content) {
        super(Push.Type.INVITATION, content);
    }

    public Invitation getInvitation() {
        return mInvitation;
    }

    public void setInvitation(Invitation invitation) {
        mInvitation = invitation;
    }

    public String getInvitationId() {
        return (String) getContent("invite_id");
    }

    public void setInvitationId(String invitationId) {
        setContent("invite_id", invitationId);
    }

}
