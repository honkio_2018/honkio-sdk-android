package com.riskpointer.sdk.api.model.process;

/**
 * The reasons of the PIN requests.
 * @author Shurygin Denis.
 */
public final class PinReason {

    /** Request for the new PIN code. */
    public static final int NEW = 1;
    /** Application restarted. */
    public static final int REENTER = 2;
    /** Purchase confirmation/ */
    public static final int PURCHASE = 3;
    /** Pin requested on timeout. */
    public static final int TIMEOUT = 4;

    private PinReason() {}

}
