/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol;

/**
 * A factory that creates a builders for requests and responses.
 */
public interface ProtocolFactory {

	/**
	 * Gets new {@link RequestBuilder}.
	 * @return New {@link RequestBuilder}.
	 */
	RequestBuilder newRequestBuilder();

	/**
	 * Gets new {@link ResponseParser} that parse responses.
	 * @return New {@link ResponseParser} that parse responses.
	 */
	<T> ResponseParser<T> newParser(Class<T> clazz);

}
