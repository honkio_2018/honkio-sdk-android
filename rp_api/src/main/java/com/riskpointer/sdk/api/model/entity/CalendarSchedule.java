package com.riskpointer.sdk.api.model.entity;

import com.riskpointer.sdk.api.web.message.filter.ScheduleFilter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by I.N. on 2018-02-13.
 */
public class CalendarSchedule implements Serializable {

    private ArrayList<CalendarEvent> mList;
    private ScheduleFilter mFilter;

    public List<CalendarEvent> getList() {
        return mList;
    }

    public void setList(ArrayList<CalendarEvent> list) {
        this.mList = list;
    }

    public ScheduleFilter getFilter() {
        return mFilter;
    }

    public void setFilter(ScheduleFilter filter) {
        this.mFilter = filter;
    }

}
