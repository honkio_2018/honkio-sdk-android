package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.CalendarEvent;
import com.riskpointer.sdk.api.model.entity.Product;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by I.N. on 2018-02-13.
 */
public class CalendarEventJsonParser extends BaseJsonResponseParser<CalendarEvent> {

    @Override
    protected CalendarEvent parseJson(Message<?> message, JSONObject json) {
        return parseCalendarEvent(json);
    }

    public static CalendarEvent parseCalendarEvent(JSONObject eventJson) {
        CalendarEvent event = new CalendarEvent();
        event.setId(eventJson.optString("id"));
        event.setType(eventJson.optString("type", CalendarEvent.Type.TENTATIVE));
        event.setStart(ApiFormatUtils.stringDateToLong(eventJson.optString("start")));
        event.setEnd(ApiFormatUtils.stringDateToLong(eventJson.optString("end")));
        event.setInitialStart(ApiFormatUtils.stringDateToLong(eventJson.optString("initial_start")));
        event.setInitialEnd(ApiFormatUtils.stringDateToLong(eventJson.optString("initial_end")));
        event.setRepeating(eventJson.optString("repeat"));

        ArrayList<Map<String, Object>> metadataArray = new ArrayList<>();
        event.setMetaData(metadataArray);

        JSONArray productsJsonArray = eventJson.optJSONArray("products");
        if (productsJsonArray != null) {
            ArrayList<Product> products = new ArrayList<>(productsJsonArray.length());
            for (int j = 0; j < productsJsonArray.length(); j++) {
                JSONObject productJson = productsJsonArray.optJSONObject(j);
                if (productJson != null) {
                    Product product = new Product(jsonToMap(productJson));
                    ParseHelper.parseProduct(productJson, product);
                    products.add(new Product(product));
                }
            }
            event.setProducts(products);
        }
        return event;
    }

}
