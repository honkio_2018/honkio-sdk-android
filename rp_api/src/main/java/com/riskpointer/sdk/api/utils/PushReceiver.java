package com.riskpointer.sdk.api.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.riskpointer.sdk.api.model.push.Push;

/**
 * @author Shurygin Denis
 */
public abstract class PushReceiver extends BroadcastReceiver {

    public abstract void onReceive(Context context, Intent intent, Push<?> push);

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.hasExtra(BroadcastHelper.BROADCAST_EXTRA_PUSH)) {
            Push<?> push = ((Push<?>)
                    intent.getSerializableExtra(BroadcastHelper.BROADCAST_EXTRA_PUSH));

            onReceive(context, intent, push);
        }
    }
}