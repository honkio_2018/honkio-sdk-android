/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import android.util.Base64;

import com.riskpointer.sdk.api.model.entity.ServerFile;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONObject;

/**
 * Parse ServerFile from JSON.
 */
public class ServerFileJsonParser extends BaseJsonResponseParser<ServerFile> {

	@Override
	public ServerFile parseJson(Message<?> message, JSONObject json) {
		ServerFile response = new ServerFile();
		response.setName((String) message.getParameter(Message.Param.ID));
		response.setMime(json.optString("mime"));
		String data = json.optString("data");
		response.setData(Base64.decode(data.getBytes(), Base64.DEFAULT));
		return response;
	}

}
