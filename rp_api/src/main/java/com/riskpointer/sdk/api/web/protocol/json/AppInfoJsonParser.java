package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.AppInfo;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by Shurygin Denis on 2016-07-15.
 */
public class AppInfoJsonParser extends BaseJsonResponseParser<AppInfo> {

    @Override
    protected AppInfo parseJson(Message<?> message, JSONObject json) {
        AppInfo result = new AppInfo(message.getAppIdentity());

        JSONObject jsonShop = json.optJSONObject("shop");
        if (jsonShop != null) {
            Identity shopIdentity = new Identity();
            shopIdentity.setId(jsonShop.optString("id"));
            shopIdentity.setPassword(jsonShop.optString("password"));
            result.setShopIdentity(shopIdentity);
        }

        result.setOauthIdentity(new Identity(
                json.optString("oauth_id"),
                json.optString("oauth_secret")
        ));

        result.addTouInfo(new AppInfo.TouInfo(
                json.optString("tou_url"),
                json.optInt("tou_version")
        ));
        JSONObject touCustom = json.optJSONObject("dict_tou_custom");
        if (touCustom != null) {
            Iterator<String> iterator = touCustom.keys();
            while (iterator.hasNext()) {
                String key = iterator.next();
                JSONObject object = touCustom.optJSONObject(key);
                if (object != null) {
                    result.addTouInfo(new AppInfo.TouInfo(
                            key, object.optString("str_tou_url"),
                        object.optInt("version")
                    ));
                }
            }
        }

        return result;
    }
}
