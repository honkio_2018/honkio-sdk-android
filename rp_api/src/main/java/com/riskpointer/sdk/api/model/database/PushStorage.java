package com.riskpointer.sdk.api.model.database;

import android.content.Context;
import android.net.Uri;

import com.riskpointer.sdk.api.model.push.Push;

/**
 * Provide easy work with push messages. All work with data base hidden in the methods of this class.
 * @author Shurygin Denis on 2015-05-15.
 */
@Deprecated
public class PushStorage {
    private static final String CONTENT_AUTHORITY = "com.riskpointer.api.model.database";
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    private static PushStorage sInstance;

    @Deprecated
    public static PushStorage getInstance(Context context) {
        if (sInstance == null)
            sInstance = new PushStorage(context);
        return sInstance;
    }


    private PushStorage(Context context) {
    }

    @Deprecated
    public long getInvoicePushesCount(String invoiceId) {
        return 0;
    }


    @Deprecated
    public Push<?> getPush(long id) {
        return null;
    }


    @Deprecated
    public int removePush(long id) {
        return 0;
    }

    @Deprecated
    public int removeInvoicePush(String invoiceId) {
        return -1;
    }
}
