/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Product;
import com.riskpointer.sdk.api.model.entity.ShopProducts;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Parse ShopProducts from JSON.
 */
public class ShopProductsJsonParser extends BaseJsonResponseParser<ShopProducts> {

	@Override
	public ShopProducts parseJson(Message<?> message, JSONObject json) {
		ShopProducts result = new ShopProducts();
		result.setShopId(message.getShopIdentity() != null ? message.getShopIdentity().getId() : null);
		
		JSONArray productsListJson = json.optJSONArray("product");
		if(productsListJson != null) {
			for(int i = 0; i < productsListJson.length(); i++) {
				JSONObject productJson = productsListJson.optJSONObject(i);
				// In the list will write only active products
				// Fix for some bug on the server 0_o
				if(productJson.optBoolean("active", true)) {
					/*Product product = new Product();
					ParseHelper.parseProduct(productJson, product);*/
					result.add(ProductJsonParser.parseProduct(productJson));
				}
			}
		}
		
		return result;
	}

}
