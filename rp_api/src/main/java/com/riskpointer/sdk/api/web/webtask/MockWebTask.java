package com.riskpointer.sdk.api.web.webtask;

import android.content.Context;

import com.riskpointer.sdk.api.exception.BadStatusCodeException;
import com.riskpointer.sdk.api.web.message.Message;

import java.io.IOException;

// TODO Write docs
/**
 * Created by Shurygin Denis on 2016-11-01.
 */
public class MockWebTask<ResponseResult> extends BaseWebTask<ResponseResult> {

    private final String mMockResponse;
    private final long mDelay;


    public MockWebTask(Context context, Message<ResponseResult> message, String mockResponse) {
        this(context, message, mockResponse, 0);
    }

    public MockWebTask(Context context, Message<ResponseResult> message, String mockResponse, long delay) {
        super(context, message);
        message.setHashIgnore(true);
        mMockResponse = mockResponse;
        mDelay = delay;
    }

    @Override
    public Object clone() {
        return new MockWebTask<>(mContext, getMessage(), mMockResponse, mDelay);
    }

    @Override
    protected String sendWebRequest(Message<ResponseResult> message) throws IOException, BadStatusCodeException {
        try {
            Thread.sleep(mDelay);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return mMockResponse;
    }
}
