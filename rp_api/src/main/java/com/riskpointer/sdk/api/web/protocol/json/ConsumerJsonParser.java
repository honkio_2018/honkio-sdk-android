package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Consumer;
import com.riskpointer.sdk.api.model.entity.UserRolesList;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONObject;

/**
 * Created by I.N. on 2017-02-16.
 */

public class ConsumerJsonParser extends BaseJsonResponseParser<Consumer> {
    @Override
    public Consumer parseJson(Message<?> message, JSONObject json) {
        return parseConsumer(message, json);
    }

    public static Consumer parseConsumer(Message<?> message, JSONObject json) {
        if (json != null) {
            Consumer consumer = new Consumer();
            consumer.setId(json.optString("user_id"));
            consumer.setActive(json.optBoolean("active", false));
            UserRolesList rolesList = UserRolesListJsonParser.parseUserRolesList(message, json);
            consumer.setRoles(rolesList);
            return consumer;
        }
        return null;
    }
}
