package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Shurygin Denis
 */

public class HKModelEntity implements Serializable {

    private final Map<String, Object> props;

    public HKModelEntity() {
        this(new HashMap<String, Object>());
    }

    public HKModelEntity(HKModelEntity entity) {
        this(new HashMap<>(entity.props));
    }

    public HKModelEntity(Map<String, Object> props) {
        this.props = props;
    }

    public Object getProperty(String key) {
        if (props != null)
            return this.props.get(key);
        return null;
    }

    public void setProperty(String key, Object value) {
        if (props != null)
            this.props.put(key, value);
    }
}
