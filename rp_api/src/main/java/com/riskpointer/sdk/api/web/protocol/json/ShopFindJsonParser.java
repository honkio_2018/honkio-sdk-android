/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.ShopFind;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.AbsFilter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Parse ShopFind from JSON.
 */
public class ShopFindJsonParser extends BaseJsonResponseParser<ShopFind> {

	@Override
	public ShopFind parseJson(Message<?> message, JSONObject json) {
		ShopFind response = new ShopFind();
		
		Object nullParameter = message.getParameter(null);
		if(nullParameter instanceof AbsFilter)
			response.setFilter((AbsFilter) nullParameter);
		
		ArrayList<Shop> shops = new ArrayList<Shop>();
		
		JSONArray productsJson = json.optJSONArray("product");
		if(productsJson != null)
			decodeItemsArray(shops, productsJson, Shop.Type.PRODUCT);
		
		JSONArray paymentsJson = json.optJSONArray("payment");
		if(paymentsJson != null)
			decodeItemsArray(shops, paymentsJson, Shop.Type.PAYMENT);

//		JSONArray otherJson = json.optJSONArray("shops");
//		if(otherJson != null)
//			decodeItemsArray(shops, otherJson, Shop.Type.PRODUCT);
		
		response.setList(shops);
		
		return response;
	}
	
	private void decodeItemsArray(ArrayList<Shop> list, JSONArray jsonArray, String type) {
		for(int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonItem = jsonArray.optJSONObject(i);

            Shop shop = new Shop(jsonToMap(jsonItem));
			ParseHelper.parseShop(jsonItem, shop);

            shop.setPassword(jsonItem.optString("password"));
			if (shop.getType() == null)
            	shop.setType(type);

			list.add(shop);
		}
	}

}
