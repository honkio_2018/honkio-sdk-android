package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.LaunchpadInfo;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONObject;

/**
 * @author I.N. 30.11.2018
 */

public class LaunchpadInfoJsonParser extends BaseJsonResponseParser<LaunchpadInfo> {
    @Override
    public LaunchpadInfo parseJson(Message<?> message, JSONObject json) {
        return parseInvitation(json);
    }

    public static LaunchpadInfo parseInvitation(JSONObject json) {
        LaunchpadInfo info = new LaunchpadInfo();
        info.setInvitedUsers(json.optInt("invited_users"));
        info.setRegisteredUsers(json.optInt("registered_users"));
        info.setEventsNumber(json.optInt("events_number"));
        return info;
    }
}
