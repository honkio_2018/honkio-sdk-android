/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;

import java.io.ByteArrayInputStream;
import java.lang.reflect.Field;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.security.auth.x500.X500Principal;

public class ApiDebugUtils {

    private static final X500Principal DEBUG_DN = new X500Principal(
            "CN=Android Debug,O=Android,C=US");
    private static final X500Principal DEBUG_DN_2 = new X500Principal("CN=android");

    private static Boolean mDebuggable;

	public static boolean isDebuggable(Context context) {
        // Try get debug info from gradle BuildConfig
        if (mDebuggable == null) {

            if (context == null)
                return false;

            String[] classNames = new String[] {
                    context.getPackageName() + ".BuildConfig",
                    context.getApplicationContext().getClass().getPackage().getName() + ".BuildConfig"
            };
            for (String className : classNames) {
                try {
                    Class configClass = Class.forName(className);
                    Field field = configClass.getField("DEBUG");
                    if (field.getType() == boolean.class) {
                        mDebuggable = field.getBoolean(null);
                    }
                } catch (ClassNotFoundException e) {
                    // do nothing
                } catch (IllegalAccessException e) {
                    // do nothing
                } catch (NoSuchFieldException e) {
                    // do nothing
                }
            }
        }

        // If BuildConfig not found.
        // Try to get debug info from sign certificate.
        if (mDebuggable == null) {
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(
                        context.getPackageName(), PackageManager.GET_SIGNATURES);
                Signature signatures[] = packageInfo.signatures;

                if (signatures != null) {
                    CertificateFactory cf = CertificateFactory.getInstance("X.509");

                    for (Signature signature : signatures) {
                        ByteArrayInputStream stream = new ByteArrayInputStream(
                                signature.toByteArray());
                        X509Certificate cert = (X509Certificate) cf
                                .generateCertificate(stream);
                        boolean debuggable = cert.getSubjectX500Principal().equals(DEBUG_DN) || cert.getSubjectX500Principal().equals(DEBUG_DN_2);
                        if (debuggable)
                            mDebuggable = true;
                    }
                }
            } catch (PackageManager.NameNotFoundException | CertificateException e) {
                // do nothing
            }
            if (mDebuggable == null)
                mDebuggable = false;
        }

        return mDebuggable;
	}
}
