/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.exception;

/**
 * Thrown when encryption key is not correct.
 * 
 * @author Denis Shurygin
 *
 */
public class WrongKeyException extends Exception {
	private static final long serialVersionUID = 910253653193138322L;

	/**
	 * Constructs a new {@code WrongPinException} that includes the current stack trace.
	 */
	public WrongKeyException() {
		super();
	}

	/**
	 * Constructs a new {@code WrongPinException} with the current stack trace and the
	 * specified detail message.
	 *
	 * @param message
	 *            the detail message for this exception.
	 */
	public WrongKeyException(String message) {
		super(message);
	}

}
