package com.riskpointer.sdk.api.model.entity;

import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.AbsFilter;
import com.riskpointer.sdk.api.web.message.filter.OrderFilter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by asenko on 10/30/2015.
 * <p>
 * Data object that hold list of orders. Also this class contain a Filter which was used to get the list.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 *
 * @author KamiSempai on 2015-03-27.
 */
public class OrdersList implements Serializable {

    private OrderFilter mFilter;
    private List<Order> mList;
    private int mTotalCount;

    private HashMap<String, Object> mPersons;

    /**
     * Gets list of Orders.
     *
     * @return List of Orders.
     */
    public List<Order> getList() {
        return mList;
    }

    /**
     * Sets list of Orders.
     *
     * @param list List of Orders.
     */
    public void setList(List<Order> list) {
        this.mList = list;
    }

    /**
     * Gets the Filter which was used to get Orders list.
     *
     * @return Filter which was used to get Orders list.
     */
    public OrderFilter getFilter() {
        return mFilter;
    }

    /**
     * Sets the Filter which was used to get Orders list.
     *
     * @param filter Filter which was used to get Orders list.
     */
    public void setFilter(OrderFilter filter) {
        this.mFilter = filter;
    }

    // TODO write docs
    public int getTotalCount() {
        return mTotalCount;
    }

    // TODO write docs
    public void setTotalCount(int total) {
        this.mTotalCount = total;
    }

    public HashMap<String, Object> getPersons() {
        return mPersons;
    }

    public void setPersons(HashMap<String, Object> persons) {
        mPersons = persons;
    }

    public void applyTo(Message<?> message) {
        message.addParameters(toParams());
    }

    public HashMap<String, Object> toParams() {
        HashMap<String, Object> hashMap = new HashMap<>();
        if (getList().size() > 0) {
            List<Map<String, Object>> orders = new ArrayList<>(getList().size());
            for (Order order : getList()) {
               /*HashMap<String, Object> orderMap = new HashMap<>();
               orderMap.put(order.toParams());
               product.put("count", product_target.count);
               products.add(product);*/
                orders.add(order.toParams());
            }
            AbsFilter.applyIfNotNull(hashMap, "orders", orders);
        }
        return hashMap;
    }
}