package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.HkStructure;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import static com.riskpointer.sdk.api.model.entity.HkStructure.ArrayProperty;
import static com.riskpointer.sdk.api.model.entity.HkStructure.EnumProperty;
import static com.riskpointer.sdk.api.model.entity.HkStructure.NumericProperty;
import static com.riskpointer.sdk.api.model.entity.HkStructure.Property;
import static com.riskpointer.sdk.api.model.entity.HkStructure.Property.Type;

/**
 * @author Shurygin Denis
 */

public class UserStructureJsonParser extends BaseJsonResponseParser<HkStructure> {

    @Override
    public HkStructure parseJson(Message<?> message, JSONObject json) {
        return toStructure(json);
    }

    public static HkStructure toStructure(JSONObject structureJson) {
        if (structureJson != null) {
            HkStructure structure = new HkStructure();

            structure.setId(structureJson.optString("_id"));
            structure.setName(structureJson.optString("name"));
            structure.setMerchant(structureJson.optString("merchant"));
            JSONObject extraFieldsJsonObject = structureJson.optJSONObject("extra_fields");
            if (extraFieldsJsonObject != null) {
                JSONObject propertiesJsonObject = extraFieldsJsonObject.optJSONObject("public_properties");
                if (propertiesJsonObject != null) {
                    HashMap<String, Property> propertiesMap = new HashMap<>();
                    Iterator<?> keys = propertiesJsonObject.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        propertiesMap.put(key, toProperty(key, propertiesJsonObject.optJSONObject(key)));
                    }
                    structure.setProperties(propertiesMap);
                }
            }

            return structure;
        }
        return null;
    }

    private static Property toProperty(String name, JSONObject propertyJson) {
        Property property;
        String type = propertyJson.optString("type", Type.STRING);
        switch (type) {
            case Type.INT:
                NumericProperty numProperty = new NumericProperty();
                numProperty.setMin(propertyJson.optDouble("min"));
                numProperty.setMax(propertyJson.optDouble("max"));
                property = numProperty;
                break;
            case Type.ENUM:
                EnumProperty enumProperty = new EnumProperty();
                JSONArray valuesJsonArray = propertyJson.optJSONArray("values");
                if (valuesJsonArray != null) {
                    ArrayList<EnumProperty.Value> valuesArray = new ArrayList<>();
                    for (int j = 0; j < valuesJsonArray.length(); j++) {
                        JSONObject valueJson = valuesJsonArray.optJSONObject(j);
                        if (valueJson != null) {
                            EnumProperty.Value value = new EnumProperty.Value(jsonToMap(valueJson));
                            value.setName(valueJson.optString("name"));
                            value.setValue(valueJson.optString("value"));
                            valuesArray.add(value);
                        }
                    }
                    enumProperty.setValues(valuesArray);
                }
                property = enumProperty;
                break;
            case Type.ARRAY:
                ArrayProperty arrayProperty = new ArrayProperty();
                JSONObject subtypeJson = propertyJson.optJSONObject("subtype");
                if (subtypeJson != null) {
                    arrayProperty.setSubtype(toProperty(null, subtypeJson));
                }
                property = arrayProperty;
                break;
            default:
                property = new Property();
        }
        property.setType(type);
//        property.setName(propertyJson.optString("name"));
        property.setName(name != null ? name : propertyJson.optString("name"));
        property.setRequired(propertyJson.optBoolean("is_required"));

        ParseHelper.parseHKString(
                propertyJson, property.getTitle(),
                "title", property.getName());

        return property;
    }

}
