package com.riskpointer.sdk.api.model.entity;

import com.riskpointer.sdk.api.utils.ApiFormatUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//TODO write docs
/**
 * Created by Shurygin Denis on 2016-05-31.
 */
public class Event implements Serializable {

    public static class Type {

        public static final String BUSY = "busy";
        public static final String TENTATIVE = "free";

    }

    public static class MetaData {

        public static final String SHOP = "shop";

    }


    private String mType;
    private long mStart;
    private long mEnd;

    private ArrayList<Map<String, Object>> mMetaData;

    public Event() {}

    public Event(Event event) {
        this.mType = event.mType;
        this.mStart = event.mStart;
        this.mEnd = event.mEnd;

        if (event.mMetaData != null) {
            this.mMetaData = new ArrayList<>(event.mMetaData.size());
            for (Map<String, Object> map: event.mMetaData)
                this.mMetaData.add(new HashMap<>(map));
        }
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public long getStart() {
        return mStart;
    }

    public void setStart(long date) {
        this.mStart = date;
    }

    public long getEnd() {
        return mEnd;
    }

    public void setEnd(long date) {
        this.mEnd = date;
    }

    public ArrayList<Map<String, Object>> getMetaData() {
        return mMetaData;
    }

    public void setMetaData(ArrayList<Map<String, Object>> metaData) {
        this.mMetaData = metaData;
    }

    public boolean isBusy() {
        return Type.BUSY.equals(mType);
    }

    @Override
    public String toString() {
        return "Event: start=" + ApiFormatUtils.dateToServerFormat(mStart)
                    + ", end=" + ApiFormatUtils.dateToServerFormat(mEnd)
                    + ", type=" + mType;
    }
}
