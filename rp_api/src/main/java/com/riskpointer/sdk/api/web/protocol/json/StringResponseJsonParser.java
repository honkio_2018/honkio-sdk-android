package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONObject;

/**
 * @author Shurygin Denis
 */

public class StringResponseJsonParser extends BaseJsonResponseParser<String> {

    @Override
    public String parseJson(Message<?> message, JSONObject json) {
        return json.toString();
    }

}
