package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by I.N. on 18.02.2019
 */
public class PollOptionsNamesList implements Serializable {
    private static final String NAME_EN = "name";
    private static final String NAME_SV = "name_sv";
    private HashMap<String, String> mNamesList;

    public PollOptionsNamesList() {
    }

    public PollOptionsNamesList(String name) {
        if (mNamesList == null)
            mNamesList = new HashMap<>();
            mNamesList.put(NAME_EN, name);
    }

    public Map<String, String> getNamesList() {
        return mNamesList;
    }

    public void setAnswerList(HashMap<String, String> list) {
        mNamesList = list;
    }

    public String getDefaultName() {
        return mNamesList.get(NAME_EN);
    }

    public void addDefaultName(String value) {
        if (mNamesList == null)
            mNamesList = new HashMap<>();
        mNamesList.put(NAME_EN, value);
    }

    public void addName(String option, String value) {
        if (mNamesList == null)
            mNamesList = new HashMap<>();
        mNamesList.put(option, value);
    }
}
