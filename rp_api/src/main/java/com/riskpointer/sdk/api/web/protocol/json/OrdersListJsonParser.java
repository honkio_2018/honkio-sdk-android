/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.Order;
import com.riskpointer.sdk.api.model.entity.OrdersList;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.OrderFilter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

// TODO docs
public class OrdersListJsonParser extends BaseJsonResponseParser<OrdersList> {

	@Override
	public OrdersList parseJson(Message<?> message, JSONObject json) {
        OrdersList ordersList = new OrdersList();


        Object nullParameter = message.getParameter(null);
        if(nullParameter instanceof OrderFilter)
            ordersList.setFilter((OrderFilter) nullParameter);

        ordersList.setTotalCount(json.optInt("orders_count"));

        ArrayList<Order> list = new ArrayList<>();

        JSONArray ordersListJson = json.optJSONArray("orders");
        if (ordersListJson != null) {
            for (int i = 0; i < ordersListJson.length(); i++) {
                JSONObject orderJson = ordersListJson.optJSONObject(i);
                if (orderJson != null) {
                    list.add(OrderJsonParser.parseOrder(orderJson));
                }
            }
        }

        JSONObject personsJson = json.optJSONObject("persons");
        if (personsJson != null) {
            ordersList.setPersons((HashMap<String, Object>) jsonToMap(personsJson));
        }

        ordersList.setList(list);
        return ordersList;
	}
}
