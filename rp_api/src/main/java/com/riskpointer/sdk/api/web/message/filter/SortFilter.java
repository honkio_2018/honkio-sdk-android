package com.riskpointer.sdk.api.web.message.filter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @author Shurygin Denis
 */
public class SortFilter implements Serializable {

    public enum Order {
        /** Ascending order */
        ASC,
        /** Descending order */
        DESC
    }

    public static final class Item implements Serializable {

        public final String field;
        public final Order order;

        public Item(String field, Order order) {
            if (field == null)
                throw new IllegalArgumentException("Parameter 'field' must be not null");
            if (order == null)
                throw new IllegalArgumentException("Parameter 'order' must be not null");

            this.field = field;
            this.order = order;
        }

    }

    public final List<Item> items = new ArrayList<>();

    public SortFilter() {}

    public SortFilter(Item... items) {
        Collections.addAll(this.items, items);
    }

    public SortFilter(SortFilter filter) {
        this.items.addAll(filter.items);
    }

    public ArrayList<HashMap<String, Integer>> toParams() {
        if (this.items.isEmpty())
            return null;

        ArrayList<HashMap<String, Integer>> params = new ArrayList<>(this.items.size());
        for (Item item: this.items) {
            if (item != null) {
                HashMap<String, Integer> map = new HashMap<>(1);
                map.put(item.field, item.order == Order.ASC ? 1 : -1);
                params.add(map);
            }
        }
        return params;
    }
}
