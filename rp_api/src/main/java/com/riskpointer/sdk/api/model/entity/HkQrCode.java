package com.riskpointer.sdk.api.model.entity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Shurygin Denis
 */

public class HkQrCode implements Serializable {

    public interface QrObject extends Serializable {
        String getId();
    }

    public static final class Type {

        private Type() {}

        public static final String ASSET = "asset";
        public static final String SHOP = "shop";
    }

    public static class Application implements Serializable {

        public Application() {}

        public Application(Application application) {}

    }

    private String mId;
    private String mMerchantId;

    private String mObjectType;
    private QrObject mObject;

    private String mUrlBase;
    private String mUrlParams;

    private ArrayList<Application> mApplications;

    public HkQrCode() {}

    public HkQrCode(HkQrCode qrCode) {
        this.mId = qrCode.mId;
        this.mMerchantId = qrCode.mMerchantId;
        this.mObjectType = qrCode.mObjectType;
        this.mObject = qrCode.mObject;
        this.mUrlBase = qrCode.mUrlBase;
        this.mUrlParams = qrCode.mUrlParams;

        if (qrCode.mApplications != null) {
            this.mApplications = new ArrayList<>(qrCode.mApplications.size());
            for (Application application: qrCode.mApplications)
                this.mApplications.add(new Application(application));
        }
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getMerchantId() {
        return mMerchantId;
    }

    public void setMerchantId(String merchantId) {
        this.mMerchantId = merchantId;
    }

    public String getObjectType() {
        return mObjectType;
    }

    public void setObjectType(String objectType) {
        this.mObjectType = objectType;
    }

    public <Type extends QrObject> Type getObject() {
        return (Type) mObject;
    }

    public void setObject(QrObject object) {
        this.mObject = object;
    }

    public String getUrlBase() {
        return mUrlBase;
    }

    public void setUrlBase(String urlBase) {
        this.mUrlBase = urlBase;
    }

    public String getUrlParams() {
        return mUrlParams;
    }

    public void setUrlParams(String urlParams) {
        this.mUrlParams = urlParams;
    }

    public ArrayList<Application> getApplications() {
        return mApplications;
    }

    public void setApplications(ArrayList<Application> applications) {
        this.mApplications = applications;
    }
}
