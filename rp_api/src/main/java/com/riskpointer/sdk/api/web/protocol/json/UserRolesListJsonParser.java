package com.riskpointer.sdk.api.web.protocol.json;

import android.text.TextUtils;

import com.riskpointer.sdk.api.model.entity.UserRolesList;
import com.riskpointer.sdk.api.model.entity.UserRole;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


// TODO write docs

/**
 * Created by Shurygin Denis on 2016-07-12.
 */
public class UserRolesListJsonParser extends BaseJsonResponseParser<UserRolesList> {
    @Override
    protected UserRolesList parseJson(Message<?> message, JSONObject json) {
        return parseUserRolesList(message, json);
    }

    public static UserRolesList parseUserRolesList(Message<?> message, JSONObject json) {
        if (json != null) {
            ArrayList<UserRole> list = new ArrayList<>();
            String userId = json.optString("user_id");
            String userFirstName = json.optString("user_first_name");
            String userLastName = json.optString("user_last_name");
            JSONObject contactData = json.optJSONObject("user_contact_data");

            JSONArray jsonArray = json.optJSONArray("roles");
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonExtension = jsonArray.optJSONObject(i);
                    if (jsonExtension != null) {

                        UserRole extension = UserRoleJsonParser.parseRole(jsonExtension);

                        if (TextUtils.isEmpty(extension.getUserId()))
                            extension.setUserId(userId);

                        if (TextUtils.isEmpty(extension.getUserFirstName()))
                            extension.setUserFirstName(userFirstName);
                        if (TextUtils.isEmpty(extension.getUserLastName()))
                            extension.setUserLastName(userLastName);

                        if (contactData != null) {
                            extension.setContactData((HashMap<String, Object>) jsonToMap(contactData));
                        }
                        list.add(extension);
                    }
                }
            }

            UserRolesList result = new UserRolesList();
            result.setList(list);
            result.setRequestedUserId((String) message.getParameter("user_id"));

            List<String> extensionIds = (List<String>) message.getParameter("roles");
            if (extensionIds != null)
                result.setRequestedExtensionIds(extensionIds.toArray(new String[extensionIds.size()]));

            result.setMerchants(ParseHelper.parseSimpleMerchantsMap(json.optJSONObject("merchants")));

            return result;
        } else {
            return null;
        }
    }
}
