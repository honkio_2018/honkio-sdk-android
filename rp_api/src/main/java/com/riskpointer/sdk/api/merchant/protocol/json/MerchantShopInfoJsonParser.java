package com.riskpointer.sdk.api.merchant.protocol.json;

import com.riskpointer.sdk.api.model.entity.merchant.MerchantShop;
import com.riskpointer.sdk.api.model.entity.merchant.MerchantShopInfo;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.protocol.json.BaseJsonResponseParser;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Shurygin Denis on 2016-11-24.
 */

public class MerchantShopInfoJsonParser extends BaseJsonResponseParser<MerchantShopInfo> {

    @Override
    protected MerchantShopInfo parseJson(Message<?> message, JSONObject json) {
        MerchantShopInfo merchantShopInfo = new MerchantShopInfo();
        merchantShopInfo.setTimeStamp(ApiFormatUtils.stringDateToLong(json.optString("timestamp", null)));

        JSONObject shopJson = json.optJSONObject("shop");
        if (shopJson != null) {
            MerchantShop merchantShop = new MerchantShop();
            merchantShop.setId(json.optString("id"));
            merchantShop.setPassword(shopJson.optString("password"));
            merchantShop.setSettings((HashMap<String, Object>) jsonToMap(shopJson));
            merchantShopInfo.setMerchantShop(merchantShop);
        }
        return merchantShopInfo;
    }
}
