/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web;

import android.content.Context;

import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.support.HoneycombAsyncTask;
import com.riskpointer.sdk.api.utils.ApiDebugUtils;
import com.riskpointer.sdk.api.utils.ApiLog;
import com.riskpointer.sdk.api.utils.ApiWebUtils;
import com.riskpointer.sdk.api.web.error.RpError;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import okhttp3.Call;
import okhttp3.Request;
import okhttp3.Response;

/**
 * AsyncTask that sends GET requests on the given URL.<br>
 * Returns true if request successful, false otherwise.
 *  <br><br>
 *  SimpleAsyncGet implements {@link Task} interface, so you have opportunity to abort task.
 */
public class SimpleAsyncGet extends HoneycombAsyncTask<Void, Void, String> implements Task {

	private int mErrorCode = RpError.Api.NONE;
	private final String mUrlString;
	private final boolean isWillLogged;

	private Call.Factory mClient;
	private Call mCallRequest;

	/**
	 * Construct new Task.
	 * @param urlString URL to send GET request.
	 */
	public SimpleAsyncGet(Context context, String urlString) {
		super();
		mClient = ApiWebUtils.getClient();
		mUrlString = urlString;
		isWillLogged = ApiDebugUtils.isDebuggable(context);
	}

    @Override
    public HoneycombAsyncTask<Void, Void, String> execute(Void... params) {
        return executeOnExecutor(THREAD_POOL_EXECUTOR, params);
    }

    @Override
	protected String doInBackground(Void... params) {
		String result = null;

		ApiLog.d("GET to URL = " + mUrlString);

		if(!isAborted()) {
			try {

				Request.Builder builder = new Request.Builder().url(mUrlString);
				builder.addHeader("Connection", "Keep-Alive");

				mCallRequest = mClient.newCall(builder.build());
				Response response = mCallRequest.execute();

				result = response.body().string();

				if (isWillLogged) {
					ApiLog.d("Response(" + response.code() +") from " + mUrlString + " <<= \n" + result);
				}

				if (response.code() != HttpURLConnection.HTTP_OK) {
					cancel(false);
					mErrorCode = RpError.Api.BAD_RESPONSE;
				}
			} catch (IOException e) {
				e.printStackTrace();
				cancel(false);
				mErrorCode = RpError.Api.NO_CONNECTION;
			}
		}

		if (result == null && mErrorCode == RpError.Api.NONE)
			mErrorCode = RpError.Api.UNKNOWN;

		return result;
	}

	@Override
	public boolean abort() {
		mErrorCode = RpError.Api.NONE;
		cancel(false);
		if(mCallRequest != null)
			mCallRequest.cancel();
		return true;
	}

	@Override
	public boolean isAborted() {
		if(mCallRequest != null)
			return mCallRequest.isCanceled();
		return false;
	}

    @Override
    public boolean isCompleted() {
        return getStatus() == Status.FINISHED;
    }

	/**
	 * Gets error code of the request.
	 * @return Error code of the request.
	 */
	public int getErrorCode() {
		return mErrorCode;
	}

	private String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line).append('\n');
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
}
