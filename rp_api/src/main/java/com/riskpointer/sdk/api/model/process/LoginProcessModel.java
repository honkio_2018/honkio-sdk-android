package com.riskpointer.sdk.api.model.process;

import android.content.Context;

import com.riskpointer.sdk.api.web.callback.RequestCallback;

/**
 * Interface which contains set of methods that represent different parts of the login process.
 *
 * @author Shurygin Denis.
 */
public abstract class LoginProcessModel<T> extends BaseProcessModel {

    protected RequestCallback<T> mCallback;

    protected LoginProcessModel() {
        throw new RuntimeException("Invalid constructor. Use constructor with arguments. Don't use BaseProcessModel.restoreFromSavedState.");
    }

    /**
     * Construct new Login process model with specified callback.
     * @param callback Request callback.
     */
    public LoginProcessModel(RequestCallback<T> callback) {
        super();
        mCallback = callback;
    }

    /**
     * Do login using OAuth2.0 token.
     * @param context Application context.
     * @param token OAuth token.
     * @param flags Flags for requests.
     */
    public abstract void loginWithOAuthToken(Context context, String token, int flags);

    /**
     * Do login using login and password.
     * @param context Application context.
     * @param login User login.
     * @param password User password.
     * @param flags Flags for requests.
     */
    @Deprecated
    public abstract void login(Context context, String login, String password, int flags);

    /**
     * Do login using PIN code.
     * @param context Application context.
     * @param pin PIN code.
     * @param flags Flags for requests.
     */
    public abstract void login(Context context, String pin, int flags);

    /**
     * Do login using saved PIN code.
     * @param context Application context.
     * @param flags Flags for requests.
     */
    public abstract void login(Context context, int flags);

    /**
     * Called when PIN code required. In this case PIN pad should be shown.
     * @param context Application context.
     * @param reason Reason for the PIN request.
     * @see PinReason
     */
    public abstract void onRequestPin(Context context, int reason);

    /**
     * Should be called when PIN entered.
     * @param context Application context.
     * @param pin Entered PIN code.
     * @param delays Delays between PIN buttons clicks. May be null.
     * @param reason Reason for the PIN request.
     */
    public abstract void onPinEntered(Context context, String pin, long[] delays, int reason);

    /**
     * Should be called when PIN pad canceled without entering a PIN.
     * @param context Application context.
     * @param reason Reason for the PIN request.
     */
    public abstract void onPinCanceled(Context context, int reason);
}
