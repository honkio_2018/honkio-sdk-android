package com.riskpointer.sdk.api.model.task;

import android.util.SparseArray;

/**
 * @author Shurygin Denis on 2015-05-06.
 */
public class TaskStorage {

    private static volatile int sLastTaskId = 0;

    private static final SparseArray<Task> sTasksSet = new SparseArray<>();

    public static synchronized int push(Task task) {
        if (task == null)
            return 0;

        int taskIndex = sTasksSet.indexOfValue(task);
        if (taskIndex >= 0) {
            return sTasksSet.keyAt(taskIndex);
        }
        int taskId = ++sLastTaskId;
        sTasksSet.put(taskId, task);

        return taskId;
    }

    public static synchronized int pushIfNotCompleted(Task task) {
        if (task == null || task.isCompleted())
            return 0;
        return TaskStorage.push(task);
    }

    public static synchronized Task pull(int taskId) {
        if (taskId == 0)
            return null;

        Task task = sTasksSet.get(taskId);
        if (task != null)
            sTasksSet.remove(taskId);
        return task;
    }
}
