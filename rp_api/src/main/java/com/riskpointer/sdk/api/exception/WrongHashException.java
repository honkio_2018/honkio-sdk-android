/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.exception;

/**
 * Thrown when server returned a request with wrong hash.
 * 
 * @author Denis Shurygin
 *
 */
public class WrongHashException extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new {@code WrongHashException} that includes the current stack trace.
	 */
	public WrongHashException() {
		super();
	}

	/**
	 * Constructs a new {@code WrongHashException} with the current stack trace and the
	 * specified detail message.
	 *
	 * @param message
	 *            the detail message for this exception.
	 */
	public WrongHashException(String message) {
		super(message);
	}
}
