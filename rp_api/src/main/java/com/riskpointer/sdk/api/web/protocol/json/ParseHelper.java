package com.riskpointer.sdk.api.web.protocol.json;

import android.text.TextUtils;

import com.riskpointer.sdk.api.BaseHonkioApi;
import com.riskpointer.sdk.api.model.entity.BoughtProduct;
import com.riskpointer.sdk.api.model.entity.HKString;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.InventoryProduct;
import com.riskpointer.sdk.api.model.entity.MerchantSimple;
import com.riskpointer.sdk.api.model.entity.Product;
import com.riskpointer.sdk.api.model.entity.Rate;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.Tag;
import com.riskpointer.sdk.api.model.entity.merchant.Merchant;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static com.riskpointer.sdk.api.web.protocol.json.BaseJsonResponseParser.jsonToMap;

/**
 * Created by Shurygin Denis on 2016-11-03.
 */

public class ParseHelper {

    /**
     * Parse Shop object from the JSON object.
     *
     * @param shopJson JSON object which contains shop params.
     * @param shop     Shop object that would be filled from JSON object.
     */
    public static void parseShop(JSONObject shopJson, Shop shop) {
        shop.setId(shopJson.optString("id"));
        shop.setType(shopJson.optString("type", null));
        shop.setName(shopJson.optString("name"));
        shop.setDescription(shopJson.optString("str_description"));
        shop.setLatitude(shopJson.optDouble("latitude"));
        shop.setLongitude(shopJson.optDouble("longitude"));
        shop.setMaxDistance(shopJson.optDouble("int_max_distance"));
        shop.setPublicKey(shopJson.optString("key"));
        shop.setType(shopJson.optString("type"));
        shop.setServiceType(shopJson.optString("service_type"));
        shop.setLogoSmall(shopJson.optString("logo_small"));
        shop.setLogoLarge(shopJson.optString("logo_large"));
        shop.setAddress(shopJson.optString("address"));
        if (TextUtils.isEmpty(shop.getAddress())) // TODO use single variant for all requests
            shop.setAddress(shopJson.optString("str_address"));

        JSONArray ratingsJson = shopJson.optJSONArray("ratings");
        if (ratingsJson != null) {
            List<Rate> ratings = new ArrayList<>(ratingsJson.length());
            for (int i = 0; i < ratingsJson.length(); i++) {
                JSONObject jsonItem = ratingsJson.optJSONObject(i);
                ratings.add(new Rate(jsonItem.optInt("value"), jsonItem.optInt("count")));
            }
            shop.setRatings(ratings);
        }
        ArrayList<Tag> tagsList = new ArrayList<>();
        JSONArray tagsListJson = shopJson.optJSONArray("list_tags");
        if (tagsListJson != null && tagsListJson.length() > 0) {
            for (int j = 0; j < tagsListJson.length(); j++) {
                JSONObject tagJson = tagsListJson.optJSONObject(j);
                String tagId = tagJson.optString("id");
                String tagName = tagJson.optString("name");
                if (tagId != null && tagName != null) {
                    Tag tag = new Tag();
                    tag.setId(tagId);
                    tag.setName(tagName);
                    tagsList.add(tag);
                }
            }
        }
        shop.setTags(tagsList);

        if (shopJson.has("opened_until")) {
            String timeString = shopJson.optString("opened_until");
            if (timeString != null && !timeString.equals("null")) {
                shop.setClosingTime(ApiFormatUtils.stringDateToLong(timeString));
            }
        }
        // Server send offset in minutes. Convert it to milliseconds
        shop.setClosingOffset(shopJson.optLong("int_closing_offset") * 1000 * 60);

        shop.setPreAuthorizationAmount(shopJson.optDouble("float_preauthorization_amount", 0));
        shop.setSettings((HashMap<String, Object>) jsonToMap(shopJson));
    }

    /**
     * Parse SimpleMerchant object from the JSON object.
     *
     * @param merchantJson JSON object which contains merchant params.
     * @param merchant     SimpleMerchant object that would be filled from JSON object.
     */
    public static void parseSimpleMerchant(JSONObject merchantJson, MerchantSimple merchant) {
        merchant.setId(merchantJson.optString("id"));
        merchant.setName(merchantJson.optString("name"));
        merchant.setSupportEmail(merchantJson.optString("support_email"));
        merchant.setSupportPhone(merchantJson.optString("support_telephone"));
        merchant.setBusinessId(merchantJson.optString("businessid"));
        merchant.setCurrency(merchantJson.optString("currency"));

        String loginShopId = merchantJson.optString("loginshop_id");
        if (loginShopId != null && loginShopId.length() > 0)
            merchant.setLoginShopIdentity(new Identity(
                    loginShopId,
                    merchantJson.optString("loginshop_password")));
        else
            merchant.setLoginShopIdentity(BaseHonkioApi.getMainShopIdentity());

        String registrationShopId = merchantJson.optString("registrationshop_id");
        if (registrationShopId != null && registrationShopId.length() > 0) {
            merchant.setRegistrationShopIdentity(new Identity(
                    registrationShopId,
                    merchantJson.optString("registrationshop_password")
            ));
        }

        String accounts = merchantJson.optString("account_type");
        if (accounts != null)
            merchant.setSupportedAccounts(accounts.split(" "));

        JSONArray vatJSON = merchantJson.optJSONArray("vat_classes");
        ArrayList<Integer> vatClasses = new ArrayList<>(vatJSON.length());
        for (int i = 0; i < vatJSON.length(); i++) {
            vatClasses.add(vatJSON.optInt(i));
        }
        merchant.setVatClasses(vatClasses);
    }

    public static HashMap<String, MerchantSimple> parseSimpleMerchantsMap(JSONObject merchantsJson) {
        HashMap<String, MerchantSimple> merchantsMap = new HashMap<>();
        if (merchantsJson != null) {
            Iterator<String> merchantKeys = merchantsJson.keys();

            while (merchantKeys.hasNext()) {
                String merchantId = merchantKeys.next();
                JSONObject merchantJson = merchantsJson.optJSONObject(merchantId);

                if (merchantJson != null) {
                    MerchantSimple merchant = new MerchantSimple();
                    merchant.setId(merchantId);

                    ParseHelper.parseSimpleMerchant(merchantJson, merchant);

                    merchantsMap.put(merchantId, merchant);
                }
            }
        }
        return merchantsMap;
    }

    public static void parseMerchant(JSONObject merchantJson, Merchant merchant) {
        merchant.setName(merchantJson.optString("str_name"));
        merchant.setCurrency(merchantJson.optString("currency"));
        merchant.setBusinessId(merchantJson.optString("str_businessid"));
        merchant.setBankIban(merchantJson.optString("str_sepa"));
        merchant.setBankName(merchantJson.optString("str_bankname"));
        merchant.setBankSwift(merchantJson.optString("str_swift"));
        merchant.setVatnr(merchantJson.optString("str_vatnumber"));

        merchant.setEmail(merchantJson.optString("str_email"));
        merchant.setPhone(merchantJson.optString("str_telephone"));

        merchant.setParentMerchant(merchantJson.optString("parent"));

        merchant.setVat(merchantJson.optDouble("int_vat"));

        merchant.setSupportEmail(merchantJson.optString("str_support_email"));
        merchant.setSupportPhone(merchantJson.optString("str_support_telephone"));
    }

    // TODO write docs
    public static void parseProduct(JSONObject productJson, Product product) {
        if (productJson != null) {
            product.setId(productJson.optString("id", ""));
            product.setName(productJson.optString("name", ""));
            product.setDescription(productJson.optString("description", ""));
            product.setCurrency(productJson.optString("currency", ""));
            product.setAmount(productJson.optDouble("amount", 0));
            product.setAmountVat(productJson.optDouble("amount_vat", 0));
            product.setAmountNoVat(productJson.optDouble("amount_no_vat", 0));
            product.setAmountVatPercent(productJson.optDouble("amount_vat_percent", 0));

            product.setValidityTime(productJson.optLong("int_validity_time", 0));
            product.setValidityTimeType(productJson.optString("str_validity_time_type"));

            if (productJson.has("int_duration"))
                product.setDuration(productJson.optLong("int_duration", 0));

            JSONArray targetProductsJson = productJson.optJSONArray("target_products");
            if (targetProductsJson != null) {
                ArrayList<Product.Target> targetsList
                        = new ArrayList<>(targetProductsJson.length());
                for (int i = 0; i < targetProductsJson.length(); i++) {
                    JSONObject targetJson = targetProductsJson.optJSONObject(i);
                    if (targetJson != null) {
                        String id = targetJson.optString("id");
                        int count = targetJson.optInt("required_count");
                        if (id != null && count > 0)
                            targetsList.add(new Product.Target(id, count));
                    }
                }

                product.setTargetProducts(targetsList);
            }

            if (productJson.has("amount_surplus")) {
                HashMap<String, Double> suppluses = new HashMap<>();
                JSONObject supplusesJson = productJson.optJSONObject("amount_surplus");
                if (supplusesJson != null) {
                    Iterator<String> keysIterator = supplusesJson.keys();
                    while (keysIterator.hasNext()) {
                        String key = keysIterator.next();
                        suppluses.put(key, supplusesJson.optDouble(key));
                    }
                }
                product.setSupplusMap(suppluses);
            }

            ArrayList<String> tagsList = new ArrayList<>();
            JSONArray tagsListJson = productJson.optJSONArray("list_tags");
            if (tagsListJson != null && tagsListJson.length() > 0) {
                for (int j = 0; j < tagsListJson.length(); j++) {
                    String tagId = tagsListJson.optString(j);
                    if (tagId != null)
                        tagsList.add(tagId);
                }
            }
            product.setTagIds(tagsList);

            JSONArray picsJson = productJson.optJSONArray("list_pics");
            if (picsJson != null && picsJson.length() > 0) {
                ArrayList<Product.Pic> pics = new ArrayList<Product.Pic>();
                for (int j = 0; j < picsJson.length(); j++) {
                    JSONObject picJson = picsJson.optJSONObject(j);
                    if (picJson != null) {
                        String url = picJson.optString("url");
                        int x = picJson.optInt("x");
                        int y = picJson.optInt("y");
                        if (url != null && !"".equals(url)) {
                            pics.add(new Product.Pic(url, x, y));
                        }
                    }
                }
                product.setPicsList(pics);
            }
        }
    }

    public static void parseBoughtProduct(JSONObject productJson, BoughtProduct product) {
        if (productJson != null) {
            parseProduct(productJson, product);

            product.setCount(productJson.optInt("count"));
            product.setValidFrom(ApiFormatUtils.stringDateToLong(productJson.optString("validfrom")));
            product.setValidTo(ApiFormatUtils.stringDateToLong(productJson.optString("validto")));
            product.setSignature(productJson.optString("signature"));
            product.setValid(productJson.optBoolean("valid"));
        }
    }

    public static void parseInventoryProduct(JSONObject productJson, InventoryProduct product) {
        if (productJson != null) {
            parseBoughtProduct(productJson, product);

            product.setStamp(ApiFormatUtils.stringDateToLong(productJson.optString("stamp")));
            product.setInventoryId(productJson.optString("item_id"));
        }
    }

    public static void parseHKString(JSONObject json, HKString string, String propertyName) {
        parseHKString(json, string, propertyName, null);
    }

    public static void parseHKString(JSONObject json, HKString string, String propertyName, String fallbackValue) {
        String value = json.optString(propertyName);
        if (value != null)
            string.setLocaleValue(null, value);
        else if (fallbackValue != null)
            string.setLocaleValue(null, fallbackValue);
    }
}
