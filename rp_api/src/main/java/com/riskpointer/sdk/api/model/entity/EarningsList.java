package com.riskpointer.sdk.api.model.entity;

import java.util.List;

/**
 * Created by I.N. on 2017-02-16.
 */

public class EarningsList {

    private List<Earnings> mList;
    private Earnings mLastEventEarnings;

    public List<Earnings> getList() {
        return mList;
    }

    public void setList(List<Earnings> list) {
        mList = list;
    }

    public Earnings getLastEventEarnings() {
        return mLastEventEarnings;
    }

    public void setLastEventEarnings(Earnings lastEventEarning) {
        mLastEventEarnings = lastEventEarning;
    }
}
