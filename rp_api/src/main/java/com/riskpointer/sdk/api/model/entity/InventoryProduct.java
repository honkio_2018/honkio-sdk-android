package com.riskpointer.sdk.api.model.entity;

/**
 * Created by Shurygin Denis on 2016-11-03.
 */

public class InventoryProduct extends BoughtProduct {

    private String mInventoryId;
    private long mStamp;

    public InventoryProduct() {}

    public InventoryProduct(InventoryProduct product) {
        super(product);

        this.mInventoryId = product.mInventoryId;
        this.mStamp = product.mStamp;
    }

    public String getInventoryId() {
        return mInventoryId;
    }

    public void setInventoryId(String id) {
        this.mInventoryId = id;
    }

    public void setStamp(long stamp) {
        mStamp = stamp;
    }

    public long getStamp() {
        return mStamp;
    }

}
