package com.riskpointer.sdk.api.web.message.filter;

/**
 * @author Shurygin Denis
 */
public interface HKMapFilter extends HKFilter {

    public Double getLatitude();
    public AssetFilter setLatitude(Double mLatitude);
    public Double getLongitude();
    public AssetFilter setLongitude(Double mLongitude);

    /**
     * Gets radius of the search.
     *
     * @return Radius of the search.
     */
    public String getRadius();

    /**
     * Sets radius of the search.
     *
     * @param radius Radius of the search.
     */
    public AssetFilter setRadius(String radius);
}
