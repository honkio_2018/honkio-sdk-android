package com.riskpointer.sdk.api.model.entity;

import android.text.TextUtils;

import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.AbsFilter;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Data object that hold information about device. Also this class has several methods which allow to used it like a Filter.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 *
 * @author Denis Shurygin on 2015-01-11.
 */
public class HKDevice implements Serializable {

    /** Default Notify type that will be set for each new Device object */
    public static final String DEFAULT_NOTIFY_TYPE = NotifyType.ALL;

    /**
     * Types of notifications that the Devise can receive.
     */
    public static final class NotifyType {
        private NotifyType() {}
        /** Device will receive all types of notifications. */
        public static final String ALL = "all";
        /** Device will not receive a notifications. */
        public static final String NONE = "none";
    }

    private String mId;
    private String mApp;
    private String mToken;
    private String mPlatform;
    private String mName;
    private String mNotifyType = DEFAULT_NOTIFY_TYPE;
    private String mManufacturer;
    private String mModel;
    private String mOsVersion;

    public HKDevice() {}

    public HKDevice(HKDevice device) {
        super();
        this.mId = device.mId;
        this.mApp = device.mApp;
        this.mToken = device.mToken;
        this.mPlatform = device.mPlatform;
        this.mName = device.mName;
        this.mNotifyType  = device.mNotifyType ;
        this.mManufacturer = device.mManufacturer;
        this.mModel = device.mModel;
        this.mOsVersion = device.mOsVersion;
    }

    /**
     * Gets unique Device ID.
     * @return Unique Device ID.
     */
    public String getId() {
        return mId;
    }

    /**
     * Sets unique Device ID.
     * @param id Unique Device ID.
     */
    public void setId(String id) {
        mId = id;
    }

    /**
     * Gets application ID for which is Device registered.
     * @return Application ID for which is Device registered.
     */
    public String getApp() {
        return mApp;
    }

    /**
     * Sets application ID for which is Device registered.
     * @param app Application ID for which is Device registered.
     */
    public void setApp(String app) {
        this.mApp = app;
    }

    /**
     * Gets token which used by server to send push notifications.
     * @return Token which used by server to send push notifications.
     */
    public String getToken() {
        return mToken;
    }

    /**
     * Sets token which used by server to send push notifications.
     * @param token Token which used by server to send push notifications.
     */
    public void setToken(String token) {
        mToken = token;
    }

    /**
     * Gets platform of the Device.
     * @return Platform of the Device. Default value "android".
     */
    public String getPlatform() {
        return mPlatform;
    }

    /**
     * Sets platform of the Device.
     * @param platform Platform of the Device.
     */
    public void setPlatform(String platform) {
        this.mPlatform = platform;
    }

    /**
     * Gets name of the Device.
     * @return Name of the Device.
     */
    public String getName() {
        return mName;
    }

    /**
     * Sets name of the Device.
     * @param name Name of the Device.
     */
    public void setName(String name) {
        mName = name;
    }

    /**
     * Gets notify type which used for Device.
     * @return Notify type which used for Device.
     * @see HKDevice.NotifyType
     */
    public String getNotifyType() {
        return mNotifyType;
    }

    /**
     * Sets notify type which used for Device.
     * @param type Notify type which used for Device.
     * @see HKDevice.NotifyType
     */
    public void setNotifyType(String type) {
        mNotifyType = type;
    }

    /**
     * Check if specified notify type used for Device.
     * @param type Notify type to check.
     * @return True if Device notify type equal specified type and not null, false otherwise.
     */
    public boolean isNotifyType(String type) {
        return mNotifyType != null && mNotifyType.equals(type);
    }

    /**
     * Gets name of device manufacturer.
     * @return Name of device manufacturer.
     */
    public String getManufacturer() {
        return mManufacturer;
    }

    /**
     * Sets name of device manufacturer.
     * @param manufacturer Name of device manufacturer.
     */
    public void setManufacturer(String manufacturer) {
        this.mManufacturer = manufacturer;
    }

    /**
     * Gets name of device model.
     * @return Name of device model.
     */
    public String getModel() {
        return mModel;
    }

    /**
     * Sets name of device model.
     * @param model Name of device model.
     */
    public void setModel(String model) {
        this.mModel = model;
    }

    /**
     * Gets Operation System version.
     * @return Operation System version.
     */
    public String getOsVersion() {
        return mOsVersion;
    }

    /**
     * Sets Operation System version.
     * @param osVersion Operation System version.
     */
    public void setOsVersion(String osVersion) {
        this.mOsVersion = osVersion;
    }

    /**
     * Convert Device to Map which can be used in the web requests.
     * @return Map which can be used in the web requests.
     */
    public HashMap<String, Object> toParams() {

        HashMap<String, Object> params = new HashMap<>(1);
        AbsFilter.applyIfNotNull(params, Message.Param.ID, mId);
        AbsFilter.applyIfNotNull(params, Message.Param.DEVICE_APP, mApp);
        AbsFilter.applyIfNotNull(params, Message.Param.DEVICE_TOKEN, mToken);
        AbsFilter.applyIfNotNull(params, Message.Param.DEVICE_TYPE, mPlatform);
        AbsFilter.applyIfNotNull(params, Message.Param.DEVICE_NAME, mName);
        AbsFilter.applyIfNotNull(params, Message.Param.DEVICE_NOTIFY, mNotifyType);

        HashMap<String, Object> properties = new HashMap<>();
        AbsFilter.applyIfNotNull(properties, Message.Param.DEVICE_MANUFACTURER, mManufacturer);
        AbsFilter.applyIfNotNull(properties, Message.Param.DEVICE_MODEL, mModel);
        AbsFilter.applyIfNotNull(properties, Message.Param.DEVICE_OS_VERSION, mOsVersion);

        params.put("properties", properties);

        return params;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (getClass() != obj.getClass())
            return false;

        HKDevice device = (HKDevice) obj;
        return TextUtils.equals(mId, device.mId) &&
                TextUtils.equals(mApp, device.mApp) &&
                TextUtils.equals(mToken, device.mToken) &&
                TextUtils.equals(mPlatform, device.mPlatform) &&
                TextUtils.equals(mName, device.mName) &&
                TextUtils.equals(mNotifyType, device.mNotifyType);
    }
}
