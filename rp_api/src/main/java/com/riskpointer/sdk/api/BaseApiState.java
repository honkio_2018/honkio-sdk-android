package com.riskpointer.sdk.api;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.riskpointer.sdk.api.model.entity.AppInfo;
import com.riskpointer.sdk.api.model.entity.HKDevice;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.ServerInfo;
import com.riskpointer.sdk.api.model.entity.ShopInfo;
import com.riskpointer.sdk.api.support.BroadcastManager;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.utils.CompleteActionReceiver;
import com.riskpointer.sdk.api.web.ApiCache;
import com.riskpointer.sdk.api.web.message.Message.Command;
import com.riskpointer.sdk.api.web.response.Response;

//TODO docs
/**
 * Created by Shurygin Denis on 2016-09-02.
 */
@SuppressWarnings({
        "WeakerAccess",
        "unchecked"
})
public class BaseApiState {

    private static final String SAVED_URL = "BaseApiState_SAVED_URL";

    private Context mContext;
    private String mAppUrl;
    private Identity mAppIdentity;

    private HKDevice mDevice;
    private ServerInfo mServerInfo;
    private AppInfo mAppInfo;
    private ShopInfo mMainShopInfo;

    public BaseApiState(Context context, String apiUrl, Identity appIdentity) {
        if (context == null)
            throw new IllegalArgumentException("Context must be not null");
        if (apiUrl == null)
            throw new IllegalArgumentException("API url must be not null");
        if (appIdentity == null)
            throw new IllegalArgumentException("App identity must be not null");

        mContext = extractAppContext(context);
        mAppUrl = apiUrl;
        mAppIdentity = appIdentity;

        String savedUrl = (String) ApiCache.getApi(mContext, SAVED_URL, false);
        if (savedUrl != null && !savedUrl.equals(mAppUrl))
            ApiCache.clearAll(mContext);

        ApiCache.putApi(mContext, mAppUrl, SAVED_URL, false);

        // Restore state from cache
        mDevice = (HKDevice)  ApiCache.getApi(mContext, Command.SET_DEVICE, false);
        mServerInfo = (ServerInfo) ApiCache.getApi(mContext, Command.SERVER_INFO, false);
        mAppInfo = (AppInfo) ApiCache.getApi(mContext, Command.APP_INFO, false);
        mMainShopInfo = (ShopInfo) ApiCache.getApi(mContext, Command.SHOP_INFO, false);

        // Register for changes
        BroadcastManager broadcastManager = BroadcastManager.getInstance(mContext);

        broadcastManager.registerReceiver(new CompleteActionReceiver<HKDevice>() {
            @Override
            public void onReceive(Context context, Intent intent, Response<HKDevice> response) {
                onDeviceGet(response.getResult());
            }
        }, new IntentFilter(BroadcastHelper.getActionOnComplete(Command.SET_DEVICE)));

        broadcastManager.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent != null && intent.hasExtra(BroadcastHelper.BROADCAST_EXTRA_RESPONSE)) {
                    mServerInfo = ((Response<ServerInfo>)
                            intent.getSerializableExtra(BroadcastHelper.BROADCAST_EXTRA_RESPONSE))
                            .getResult();
                    ApiCache.putApi(context, mServerInfo,  Command.SERVER_INFO, false);
                }
            }
        }, new IntentFilter(BroadcastHelper.getActionOnComplete(Command.SERVER_INFO)));

        broadcastManager.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent != null && intent.hasExtra(BroadcastHelper.BROADCAST_EXTRA_RESPONSE)) {
                    mAppInfo = ((Response<AppInfo>)
                            intent.getSerializableExtra(BroadcastHelper.BROADCAST_EXTRA_RESPONSE))
                            .getResult();
                    ApiCache.putApi(context, mAppInfo,  Command.APP_INFO, false);
                }
            }
        }, new IntentFilter(BroadcastHelper.getActionOnComplete(Command.APP_INFO)));


        broadcastManager.registerReceiver(new CompleteActionReceiver<ShopInfo>() {
            @Override
            public void onReceive(Context context, Intent intent, Response<ShopInfo> response) {
                ShopInfo shopInfo = response.getResult();

                boolean isMainShop =
                        mAppInfo != null &&
                        mAppInfo.getShopIdentity() != null &&
                        mAppInfo.getShopIdentity().getId() != null &&
                        shopInfo != null &&
                        shopInfo.getShop() != null &&
                        mAppInfo.getShopIdentity().getId().equals(shopInfo.getShop().getId());

                if (isMainShop) {
                    mMainShopInfo = shopInfo;
                    mMainShopInfo.getShop()
                            .setPassword(mAppInfo.getShopIdentity().getPassword());
                    ApiCache.putApi(context, mMainShopInfo,  Command.SHOP_INFO, false);
                }
            }
        }, new IntentFilter(BroadcastHelper.getActionOnComplete(Command.SHOP_INFO)));

        broadcastManager.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                logout();
            }
        }, new IntentFilter(BroadcastHelper.getActionOnComplete(Command.LOGOUT)));
    }

    public Context getContext() {
        return mContext;
    }

    /**
     * Gets Url address of the server defined in the initialization.
     * @return Url address of the server.
     */
    public String getUrl() {
        return mAppUrl;
    }

    /**
     * Ges App id defined in the initialization.
     * @return App id.
     */
    public Identity getAppIdentity() {
        return mAppIdentity;
    }

    /**
     * Gets Device info.
     * @return Device info or null if API initialization was no completed.
     */
    public HKDevice getDevice() {
        return mDevice;
    }

    /**
     * Gets server info.
     * @return Server info or null if API initialization was no completed.
     */
    public ServerInfo getServerInfo() {
        return mServerInfo;
    }

    //TODO write docs
    public AppInfo getAppInfo() {
        return mAppInfo;
    }

    /**
     * Gets Main shop info.
     * @return Shop info or null if API initialization was no completed.
     */
    public ShopInfo getMainShopInfo() {
        return mMainShopInfo;
    }

    /**
     * Gets main shop Identity defined in the initialization.
     * @return Main shop Identity.
     */
    public Identity getMainShopIdentity() {
        if (mAppInfo != null)
            return mAppInfo.getShopIdentity();
        return null;
    }

    public void logout() {
        ApiCache.clearAuthorized(getContext());
    }

    void onDeviceGet(HKDevice device) {
        mDevice = device;
        ApiCache.putApi(mContext, device, Command.SET_DEVICE, false);
    }

    /**
     * Extracts Application context from context.
     * @param context Context.
     * @return Application context.
     */
    private Context extractAppContext(Context context) {
        Context appContext = context.getApplicationContext();
        if(appContext == null) appContext = context;
        return appContext;
    }
}
