package com.riskpointer.sdk.api.model.entity;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by I.N. on 2018-19-02.
 */

public class MediaFile implements Serializable {
    public static final String MEDIA_TYPE_IMAGE = "image";
    public static final String MEDIA_TYPE_VIDEO = "video";
    private String mId;
    private String mUrl;
    private String mThumbnailUrl;
    private String mAccess;
    private String mExtension;
    private String mUserOwnerId;
    private String mTimestamp;
    private Map<String, Object> mMetaData;

    public static class MetaData {
        public static final String SIZE = "size";
        public static final String TYPE = "type";
    }
    public MediaFile() {
    }

    public MediaFile(String id, String url) {
        this.mUrl = url;
        this.mId = id;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        this.mUrl = url;
    }

    public String getThumbnailUrl() {
        if (TextUtils.isEmpty(mThumbnailUrl) || "null".equals(mThumbnailUrl))
            return getUrl();
        return mThumbnailUrl;
    }

    public void setThumbnailUrl(String mThumbnailUrl) {
        this.mThumbnailUrl = mThumbnailUrl;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }
    public String getAccess() {
        return mAccess;
    }

    public void setAccess(String access) {
        mAccess = access;
    }

    public String getUserOwnerId() {
        return mUserOwnerId;
    }

    public void setUserOwnerId(String userOwnerId) {
        mUserOwnerId = userOwnerId;
    }

    public String getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        mTimestamp = timestamp;
    }

    public Map<String, Object> getMetaData() {
        return mMetaData;
    }

    public void setMetaData(Map<String, Object> metaData) {
        mMetaData = metaData;
    }

    public String getExtension() {
        return mExtension;
    }

    public void setExtension(String extension) {
        mExtension = extension;
    }
    public String getType(){
        if (mMetaData!=null){
            return (String) mMetaData.get(MetaData.TYPE);
        }
        return null;
    }
    public int getSize(){
        if (mMetaData!=null){
            return (int) mMetaData.get(MetaData.SIZE);
        }
        return 0;
    }
}
