/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.message.filter;

import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Filter for transactions list.
 *
 * @author Denis Shurygin
 */
public class TransactionFilter extends ListFilter implements Serializable {
    public static final String QUERY_TYPE_ORDER_PAYMENTS = "order_payments";
    public static final String QUERY_STATUS_ACCEPT = "accept";

    private static final long serialVersionUID = 2059567217568822044L;
    private static final String QUERY_UNWIND_ORDER = "query_unwind_order";
    private static final String QUERY_ACCOUNT_TYPE = "query_account_type";

    private String mType;
    private Boolean mOnlyValid;
    private String mTimeStamp;
    private String mOrderId;
    private String mAccountType;
    private String mStatus;
    private ShopFilter mShopFilter = new ShopFilter();
    private Long mStartDate;
    private Long mEndDate;
    private Boolean mQueryUnwindOrder;

    public TransactionFilter() {
        super();
    }

    public TransactionFilter(String id) {
        super(id);
    }

    public TransactionFilter(TransactionFilter filter) {
        super(filter);
        this.mType = filter.mType;
        this.mOnlyValid = filter.mOnlyValid;
        this.mTimeStamp = filter.mTimeStamp;
        this.mOrderId = filter.mOrderId;
        this.mAccountType = filter.mAccountType;
        this.mStatus = filter.mStatus;
        this.mShopFilter = filter.mShopFilter;//todo
        this.mStartDate = filter.mStartDate;
        this.mEndDate = filter.mEndDate;
        this.mQueryUnwindOrder = filter.mQueryUnwindOrder;
    }

    // TODO docs
    public String getType() {
        return mType;
    }

    // TODO docs
    public void setType(String mType) {
        this.mType = mType;
    }

    /**
     * Gets if only valid products should be returned.
     *
     * @return True if only valid products should be returned, false otherwise.
     */
    public Boolean getOnlyValid() {
        return mOnlyValid;
    }

    /**
     * Sets that only valid products should be returned.
     *
     * @param onlyValid True if only valid products should be returned, false otherwise.
     */
    public void setOnlyValid(Boolean onlyValid) {
        this.mOnlyValid = onlyValid;
    }

    /**
     * Gets time stamp filter value.
     *
     * @return Time stamp filter.
     */
    public String getTimeStamp() {
        return mTimeStamp;
    }

    /**
     * Sets time stamp filter.
     *
     * @param timeStamp Time stamp filter.
     */
    public void setTimeStamp(String timeStamp) {
        this.mTimeStamp = timeStamp;
    }

    /**
     * Gets {@link ShopFilter} of the filter.
     *
     * @return Shop filter set for this filter.
     */
    public ShopFilter getShopFilter() {
        return mShopFilter;
    }

    /**
     * Sets specified {@link ShopFilter}s.
     *
     * @param shopFilter Shop filter.
     * @throws NullPointerException if shopFilter is null
     */
    public void setShopFilter(ShopFilter shopFilter) {
        this.mShopFilter = shopFilter;
        if (shopFilter == null)
            throw new NullPointerException("Shop filter must be not null.");
    }

    /**
     * Gets order id that should be returned in the list.
     *
     * @return Order id that should be returned in the list.
     */
    public String getOrderId() {
        return mOrderId;
    }

    /**
     * Sets order id that should be returned in the list.
     *
     * @param mOrderId Order ID
     */
    public void setOrderId(String mOrderId) {
        this.mOrderId = mOrderId;
    }

    @Override
    public void applyTo(Message<?> message) {
        message.addParameters(toParams());
    }

    public String getAccountType() {
        return mAccountType;
    }

    public void setAccountType(String mAccountType) {
        this.mAccountType = mAccountType;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public Long getStartDate() {
        return mStartDate;
    }

    public void setStartDate(Long startDate) {
        mStartDate = startDate;
    }

    public Long getEndDate() {
        return mEndDate;
    }

    public void setEndDate(Long endDate) {
        mEndDate = endDate;
    }

    public Boolean isQueryUnwindOrder() {
        return mQueryUnwindOrder;
    }

    public void setQueryUnwindOrder(Boolean queryUnwindOrder) {
        mQueryUnwindOrder = queryUnwindOrder;
    }

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = mShopFilter.toParams();
        params.putAll(super.toParams());
        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_VALID, getOnlyValid());
        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_ORDER, getOrderId());
        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_TIMESTAMP, getTimeStamp());
        AbsFilter.applyIfNotNull(params, QUERY_ACCOUNT_TYPE, getAccountType());
        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_STATUS, getStatus());
        AbsFilter.applyIfNotNull(params, Message.Param.QUERY_TYPE, getType());
        if (mStartDate != null)
            AbsFilter.applyIfNotNull(params, Message.Param.QUERY_START_DATE, ApiFormatUtils.dateToServerFormat(mStartDate));
        if (mEndDate != null)
            AbsFilter.applyIfNotNull(params, Message.Param.QUERY_END_DATE, ApiFormatUtils.dateToServerFormat(mEndDate));
        AbsFilter.applyIfNotNull(params, QUERY_UNWIND_ORDER, isQueryUnwindOrder());
        return params;
    }
}
