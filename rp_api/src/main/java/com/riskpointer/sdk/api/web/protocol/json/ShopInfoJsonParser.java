/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.exception.WrongHashException;
import com.riskpointer.sdk.api.model.entity.MerchantSimple;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.ShopInfo;
import com.riskpointer.sdk.api.utils.ApiFormatUtils;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.response.Response;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Parse ShopInfo from JSON.
 */
public class ShopInfoJsonParser extends BaseJsonResponseParser<ShopInfo> {

	@Override
	public ShopInfo parseJson(Message<?> message, JSONObject json) {
		ShopInfo result = new ShopInfo();
        parseShopInfo(json, result);
		
		return result;
	}
	
	@Override
	public Response<ShopInfo> parse(Message<?> message, String responseString, String password) throws WrongHashException {
		JSONObject json = null;
		try {
			json = new JSONObject(responseString);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		ShopInfo result = parseJson(message, json);
		Response<ShopInfo> response = parseResponse(message, json);
		return response;
	}

    /**
     * Parse ShopInfo object from the JSON object.
     * @param json JSON object which contains shop info params.
     * @param shopInfo ShopInfo object that would be filled from JSON object.
     */
    public static boolean parseShopInfo(JSONObject json, ShopInfo shopInfo) {
        if (json == null)
            return false;

        shopInfo.setTimeStamp(ApiFormatUtils.stringDateToLong(json.optString("timestamp", null)));

        JSONObject shopJson = json.optJSONObject("shop");
        if(shopJson != null) {
            Shop shop = new Shop(jsonToMap(shopJson));
            ParseHelper.parseShop(shopJson, shop);
            shopInfo.setShop(shop);
        }

        JSONObject merchantJson = json.optJSONObject("merchant");
        if(merchantJson != null) {
            MerchantSimple merchant = new MerchantSimple();
            ParseHelper.parseSimpleMerchant(merchantJson, merchant);
            shopInfo.setMerchant(merchant);
        }

        if (shopInfo.getShop() != null)
            shopInfo.getShop().setMerchant(shopInfo.getMerchant());

        return true;
    }

}
