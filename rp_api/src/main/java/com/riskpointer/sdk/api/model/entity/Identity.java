package com.riskpointer.sdk.api.model.entity;

import android.text.TextUtils;

import java.util.Map;

/**
 * Data object that hold id/password pair.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 *
 * @author Shurygin Denis on 2015-01-28.
 */
public class Identity extends HKModelEntity {
    private static final long serialVersionUID = 1L;

    private String mId = "";
    private String mPassword = null;

    /**
     * Simple constructor without parameters.
     */
    public Identity() {
    }

    /**
     * Constructor from HKModelEntity.
     */
    public Identity(Map<String, Object> props) {
        super(props);
    }

    /**
     * Identity by coping another identity.
     *
     * @param identity Identity to copy.
     */
    public Identity(Identity identity) {
        super(identity);
        mId = identity.getId();
        mPassword = identity.getPassword();
    }

    /**
     * Identity with specified id and password.
     * 
     * @param id Identity Id.
     * @param password Identity Password.
     */
    public Identity(String id, String password) {
        mId = id;
        mPassword = password;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        this.mPassword = password;
    }

    /**
     * Check if ID or Password is Empty.
     * @return True if ID or Password is Empty, false otherwise.
     */
    public boolean isCorrupt() {
        return TextUtils.isEmpty(mId) || mPassword == null;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (getClass() != obj.getClass())
            return false;

        return TextUtils.equals(mId, ((Identity) obj).mId)
            && TextUtils.equals(mPassword, ((Identity) obj).mPassword);
    }
}
