package com.riskpointer.sdk.api.web.message.filter;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by I.N. on 06.04.2018
 */
public class PollsFilter extends ListFilter implements Serializable {
    public static final String QUERY_VOTES = "query_votes";
    public static final String QUERY_RESULTS = "query_results";
    public static final String QUERY_ACTIVE = "query_active";
    public static final String QUERY_VISIBLE = "query_visible";
    private Boolean mQueryVotes;
    private Boolean mQueryResults;
    private Boolean mQueryActive;
    private Boolean mQueryVisible;

    public Boolean isQueryVotes() {
        return mQueryVotes;
    }

    public void setQueryVotes(boolean queryVotes) {
        mQueryVotes = queryVotes;
    }

    public Boolean isQueryResults() {
        return mQueryResults;
    }

    public void setQueryResults(boolean queryResults) {
        mQueryResults = queryResults;
    }

    public Boolean isQueryActive() {
        return mQueryActive;
    }

    public void setQueryActive(boolean queryActive) {
        mQueryActive = queryActive;
    }

    public Boolean isQueryVisible() {
        return mQueryVisible;
    }

    public void setQueryVisible(boolean queryVisible) {
        mQueryVisible = queryVisible;
    }

    @Override
    public HashMap<String, Object> toParams() {
        HashMap<String, Object> params = super.toParams();

        applyIfNotNull(params, QUERY_VOTES, isQueryVotes());
        applyIfNotNull(params, QUERY_RESULTS, isQueryResults());
        applyIfNotNull(params, QUERY_ACTIVE, isQueryActive());
        applyIfNotNull(params, QUERY_VISIBLE, isQueryVisible());

        return params;
    }
}