package com.riskpointer.sdk.api.web.protocol.json;

import com.riskpointer.sdk.api.model.entity.InventoryProduct;
import com.riskpointer.sdk.api.web.message.Message;

import org.json.JSONObject;

/**
 * Created by Shurygin Denis on 2016-11-07.
 */

public class InventoryProductJsonParser extends BaseJsonResponseParser<InventoryProduct> {

    @Override
    protected InventoryProduct parseJson(Message<?> message, JSONObject json) {
        InventoryProduct product = new InventoryProduct();
        JSONObject productJson = json.optJSONObject("product");

        if (productJson != null) {
            ParseHelper.parseInventoryProduct(productJson, product);
        }

        return product;
    }


}
