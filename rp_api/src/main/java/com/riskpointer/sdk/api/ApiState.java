package com.riskpointer.sdk.api;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.support.BroadcastManager;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.utils.CompleteActionReceiver;
import com.riskpointer.sdk.api.web.ApiCache;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.response.Response;

//TODO docs
/**
 * Created by Shurygin Denis on 2016-09-05.
 */
public class ApiState extends BaseApiState {

    private User sUser;

    public ApiState(Context context, String apiUrl, Identity appIdentity) {
        super(context, apiUrl, appIdentity);

        // Restore state from cache
        sUser = (User) ApiCache.getApi(context, Message.Command.USER_GET, true);

        // Register for changes
        BroadcastManager broadcastManager = BroadcastManager.getInstance(context);
        BroadcastReceiver broadcastReceiver = new CompleteActionReceiver<User>() {
            @Override
            public void onReceive(Context context, Intent intent, Response<User> response) {
                onUserGet(response.getResult());
            }
        };
        broadcastManager.registerReceiver(broadcastReceiver,
                new IntentFilter(BroadcastHelper.getActionOnComplete(Message.Command.USER_GET)));
        broadcastManager.registerReceiver(broadcastReceiver,
                new IntentFilter(BroadcastHelper.getActionOnComplete(Message.Command.USER_SET)));
    }

    public User getActiveUser() {
        return sUser;
    }

    public void onUserGet(User user) {
        sUser = user;
        ApiCache.putApi(getContext(), sUser, Message.Command.USER_GET, true);
    }

    @Override
    public void logout() {
        super.logout();
        sUser = null;
    }
}
