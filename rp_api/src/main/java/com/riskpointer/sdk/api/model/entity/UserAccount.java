/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.api.model.entity;

import android.text.TextUtils;

import com.riskpointer.sdk.api.web.message.filter.AbsFilter;
import com.riskpointer.sdk.api.web.message.Message;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Data object that hold information about user payment account.
 * <br>Also this class extent {@code AbsFilter} and can be used like a Filter.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 *
 * @author Shurygin Denis on 2015-01-28.
 */
public class UserAccount extends AbsFilter implements Serializable {
	private static final long serialVersionUID = 1L;

	/** Number of account with zero value. */
	public static final String ZERO_NUMBER = "0";

	/**
	 * Types of the User Account.
	 */
	public static final class Type {
		public static final String VISA_TEST = "VISA";
		public static final String X_BILLING = "X_BILLING";
		public static final String BONUS = "BONUS";
		public static final String OPERATOR = "OPERATOR";
		public static final String SEITATECH = "SEITATECH";
		public static final String NETS = "CREDITCARD";
		public static final String INVOICE = "MERCHANTINVOICE";

		public static final String CREDITCARD = NETS;
	}
	
	private int mIndex;
	private String mType;
	private String mNumber;
	private double mLeft;
	private boolean mHasLeft;
	private double mLimit;
	private boolean isSigned = true;
	private String mTitle;
	private String mDescription;
	private String mLogoUrl;
	private String mCreationParams;

	private boolean isEnabled = true;

	/**
	 * Public constructor without parameters. All fields equal null.
	 */
	public UserAccount() {
	}

	/**
	 * Account with specified type and number.
	 *
	 * @param type Type of account.
	 * @param number Number of account.
	 */
	public UserAccount(String type, String number) {
		mType = type;
		mNumber = number;
	}

	public UserAccount(UserAccount userAccount) {
		mIndex = userAccount.mIndex;
		mType = userAccount.mType;
		mNumber = userAccount.mNumber;
		mLeft = userAccount.mLeft;
		mHasLeft = userAccount.mHasLeft;
		mLimit = userAccount.mLimit;
		isSigned = userAccount.isSigned;
		mDescription = userAccount.mDescription;
		mCreationParams = userAccount.mCreationParams;
	}

	public int getIndex() {
		return mIndex;
	}

	public void setIndex(int index) {
		this.mIndex = index;
	}

	public String getType() {
		return mType;
	}

	public void setType(String type) {
		this.mType = type;
	}

	/**
	 * Check if type of the account equal specified type from parameter.
	 * @param types Specified type.
	 * @return True if type of the account equal specified type from parameter, false otherwise.
	 */
    public boolean typeIs(String... types) {
        if (mType != null) {
        	for (String type: types)
        		if (mType.equals(type))
        			return true;
		}
        return types == null || types.length == 0 || types[0] == null;
    }

	public String getNumber() {
		return mNumber;
	}

	public void setNumber(String number) {
		this.mNumber = number;
	}

	public double getLeft() {
		return mLeft;
	}

	public void setLeft(double left) {
		if(isHasLeft())
			this.mLeft = left;
	}

	/**
	 * Check if account has left.
	 * @return True if account has left, false otherwise.
	 */
	public boolean isHasLeft() {
		return mHasLeft;
	}

	/**
	 * Sets if account has left.
	 * @param isHasLeft True if account has left, false otherwise.
	 */
	public void setHasLeft(boolean isHasLeft) {
		this.mHasLeft = isHasLeft;
	}

	/**
	 * Check if account has enough left.
	 * @param value Minimum left value.
	 * @return True if account has enough left or account doesn't has left value, false otherwise.
	 */
	public boolean enoughLeft(double value) {
		if(value != 0 && isHasLeft()) {
			return mLeft >= value;
		}
		return true;
	}

	public double getLimit() {
		return mLimit;
	}

	public void setLimit(double mLimit) {
		this.mLimit = mLimit;
	}

	public boolean isSigned() {
		return isSigned;
	}

	public void setIsSigned(boolean isSigned) {
		this.isSigned = isSigned;
	}

	public String getTitle() {
		return mTitle;
	}

	public void setTitle(String title) {
		this.mTitle = title;
	}

	public String getDescription() {
		return mDescription;
	}

	public void setDescription(String mDescription) {
		this.mDescription = mDescription;
	}

	public String getLogoUrl() {
		return mLogoUrl;
	}

	public void setLogoUrl(String url) {
		this.mLogoUrl = url;
	}

	/**
	 * Check if account shouldn't be shown for user.
	 * @return True if account shouldn't be shown for user, false otherwise.
	 */
    public boolean isHidden() {
        return isDummy()
				|| typeIs(Type.INVOICE)
                || typeIs(Type.BONUS);
    }

	public boolean isDummy() {
		return mType == null
				|| mNumber == null
				|| (typeIs(Type.NETS) && ZERO_NUMBER.equals(mNumber))
				|| (typeIs(Type.SEITATECH) && ZERO_NUMBER.equals(mNumber))
				|| (typeIs(Type.CREDITCARD) && ZERO_NUMBER.equals(mNumber));
	}

	public boolean isEnabled() {
		return isEnabled;
	}

	public void setEnabled(boolean enabled) {
		isEnabled = enabled;
	}

	/**
	 * Method for CreditCall. Deprecated!!!
	 */
	@Deprecated
	public String getCreationParams() {
		return mCreationParams;
	}

	/**
	 * Method for CreditCall. Deprecated!!!
	 */
	@Deprecated
	public void setCreationParams(String creationParams) {
		mCreationParams = creationParams;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;

		if (obj == null)
			return false;

		if (getClass() != obj.getClass())
			return false;

		UserAccount account = (UserAccount) obj;
		return TextUtils.equals(mType, account.mType) &&
				TextUtils.equals(mNumber, account.mNumber);
	}
	
	@Override
	public String toString() {
		if(mDescription != null && !"".equals(mDescription)) {
			return mDescription;
		}
		return mType;
	}
	
	@Override
	public void applyTo(Message<?> message) {
		AbsFilter.applyIfNotNull(message, Message.Param.ACCOUNT_TYPE, getType());
		AbsFilter.applyIfNotNull(message, Message.Param.ACCOUNT_NUMBER, getNumber());
	}
	
	@Override
	public HashMap<String, Object> toParams() {
		HashMap<String, Object> params = new HashMap<String, Object>();
		AbsFilter.applyIfNotNull(params, Message.Param.ACCOUNT_TYPE, getType());
		AbsFilter.applyIfNotNull(params, Message.Param.ACCOUNT_NUMBER, getNumber());

		return params;
	}

}
