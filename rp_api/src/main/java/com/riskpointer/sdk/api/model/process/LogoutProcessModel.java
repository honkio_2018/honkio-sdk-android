package com.riskpointer.sdk.api.model.process;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.web.callback.SimpleCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;

/**
 * @author Shurygin Denis
 */
public class LogoutProcessModel extends BaseProcessModel {

    private SimpleCallback mCallback;
    private boolean isForced = false;

    private Task mTask;

    /**
     * Construct new Logout process model.
     */
    public LogoutProcessModel() {
        super();
    }

    @Override
    public synchronized boolean abort() {
        super.abort();

        if (mTask != null)
            mTask.abort();
        mTask = null;

        return true;
    }

    public LogoutProcessModel setForced(boolean value) {
        if (getStatus() != Status.CREATED)
            throw new IllegalStateException("Process model can be forced only on status CREATED");

        this.isForced = value;

        return this;
    }

    public void logout(SimpleCallback callback) {
        if (getStatus() != Status.CREATED)
            throw new IllegalStateException("Process model can't be started twice");
        setStatus(Status.STARTED);

        mCallback = callback;

        mTask = HonkioApi.logout(Message.FLAG_HIGH_PRIORITY, new SimpleCallback() {
            @Override
            public void onComplete() {
                LogoutProcessModel.this.onComplete();
            }

            @Override
            public boolean onError(RpError error) {
                if (isForced || error.code == RpError.Common.INVALID_USER_LOGIN) {
                    HonkioApi.logout(Message.FLAG_OFFLINE_MODE, null);
                    LogoutProcessModel.this.onComplete();
                    return true;
                }
                return LogoutProcessModel.this.onError(error);
            }
        });
    }

    private void onComplete() {
        setStatus(Status.FINISHED);
        if (mCallback != null)
            mCallback.onComplete();
    }

    private boolean onError(RpError error) {
        setStatus(Status.FINISHED);
        return mCallback != null && mCallback.onError(error);
    }
}
