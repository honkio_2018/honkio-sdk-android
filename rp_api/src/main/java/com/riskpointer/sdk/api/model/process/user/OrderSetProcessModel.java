package com.riskpointer.sdk.api.model.process.user;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.merchant.HonkioMerchantApi;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.Order;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.ShopFind;
import com.riskpointer.sdk.api.model.entity.ShopInfo;
import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.api.model.process.BaseProcessModel;
import com.riskpointer.sdk.api.model.task.RequestTask;
import com.riskpointer.sdk.api.model.task.TaskStorage;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;

import java.util.List;

//TODO write docs

/**
 * Created by Shurygin Denis on 2016-07-14.
 */
public abstract class OrderSetProcessModel extends BaseProcessModel implements RequestCallback<Order> {

    private static final String SAVED_STEP = "OrderSetProcessModel.SAVED_STEP";
    private static final String SAVED_PIN_REQUIRED = "OrderSetProcessModel.SAVED_PIN_REQUIRED";
    private static final String SAVED_USE_MERCHANT_API = "OrderSetProcessModel.SAVED_USE_MERCHANT_API";
    private static final String SAVED_ACCOUNT_REQUIRED = "OrderSetProcessModel.SAVED_ACCOUNT_REQUIRED";
    private static final String SAVED_SHOP_IDENTITY = "OrderSetProcessModel.SAVED_SHOP_IDENTITY";
    private static final String SAVED_ORDER = "OrderSetProcessModel.SAVED_ORDER";
    private static final String SAVED_FLAGS = "OrderSetProcessModel.SAVED_FLAGS";
    private static final String SAVED_ORDER_SET_TASK = "OrderSetProcessModel.SAVED_ORDER_SET_TASK";
    private static final String SAVED_SHOP_FIND_TASK = "OrderSetProcessModel.SAVED_SHOP_FIND_TASK";
    private static final String SAVED_SHOP_INFO_TASK = "OrderSetProcessModel.SAVED_SHOP_INFO_TASK";

    // Order of items is important.
    // DON'T MIX THEM!
    public enum Step {
        NONE,
        STARTED,
        LOGIN_CHECK,
        SHOP_REQUEST,
        ACCOUNT_CHECK,
        PIN_CHECK,
        REQUEST
    }

    private boolean isUseMerchantApi = false;
    private boolean isPinRequired;
    private boolean isAccountRequired;

    private Identity mShopIdentity;
    private Order mOrder;
    private int mFlags;

    private Step mStep = Step.NONE;

    private RequestTask<Order> mOrderSetTask;
    private RequestTask<ShopFind> mShopFindTask;
    private RequestTask<ShopInfo> mShopInfoTask;

    public abstract void onAccountRequired();

    public abstract void onLoginRequired();

    public abstract void onPinRequired();

    protected OrderSetProcessModel() {
        super();
    }

    @Override
    public Bundle saveInstanceState() {
        Bundle instanceState = super.saveInstanceState();

        instanceState.putSerializable(SAVED_STEP, mStep);
        instanceState.putBoolean(SAVED_USE_MERCHANT_API, isUseMerchantApi);
        instanceState.putBoolean(SAVED_PIN_REQUIRED, isPinRequired);
        instanceState.putBoolean(SAVED_ACCOUNT_REQUIRED, isAccountRequired);
        instanceState.putSerializable(SAVED_SHOP_IDENTITY, mShopIdentity);
        instanceState.putSerializable(SAVED_ORDER, mOrder);
        instanceState.putInt(SAVED_FLAGS, mFlags);

        if (mOrderSetTask != null) {
            instanceState.putInt(SAVED_ORDER_SET_TASK, TaskStorage.push(mOrderSetTask));
            mOrderSetTask.setCallback(null, false);
        }

        if (mShopFindTask != null) {
            instanceState.putInt(SAVED_SHOP_FIND_TASK, TaskStorage.push(mShopFindTask));
            mShopFindTask.setCallback(null, false);
        }

        if (mShopInfoTask != null) {
            instanceState.putInt(SAVED_SHOP_INFO_TASK, TaskStorage.push(mShopInfoTask));
            mShopInfoTask.setCallback(null, false);
        }

        return instanceState;
    }

    @Override
    public void restoreInstanceState(Context context, Bundle instanceState) {
        super.restoreInstanceState(context, instanceState);

        mStep = (Step) instanceState.getSerializable(SAVED_STEP);
        isPinRequired = instanceState.getBoolean(SAVED_PIN_REQUIRED);
        isUseMerchantApi = instanceState.getBoolean(SAVED_USE_MERCHANT_API);
        isAccountRequired = instanceState.getBoolean(SAVED_ACCOUNT_REQUIRED);
        mShopIdentity = (Identity) instanceState.getSerializable(SAVED_SHOP_IDENTITY);
        mOrder = (Order) instanceState.getSerializable(SAVED_ORDER);
        mFlags = instanceState.getInt(SAVED_FLAGS);

        mOrderSetTask = (RequestTask<Order>) TaskStorage.pull(instanceState.getInt(SAVED_ORDER_SET_TASK));
        if (mOrderSetTask != null)
            mOrderSetTask.setCallback(mOrderUpdateCallback, true);

        mShopFindTask = (RequestTask<ShopFind>) TaskStorage.pull(instanceState.getInt(SAVED_SHOP_FIND_TASK));
        if (mShopFindTask != null)
            mShopFindTask.setCallback(mShopFindCallback, true);

        mShopInfoTask = (RequestTask<ShopInfo>) TaskStorage.pull(instanceState.getInt(SAVED_SHOP_INFO_TASK));
        if (mShopInfoTask != null)
            mShopInfoTask.setCallback(mShopInfoCallback, true);
    }

    public void userSetOrder(Identity shopIdentity, Order order, int flags) {
        if (HonkioApi.checkInitialisation(this)) {
            start();
            mStep = Step.STARTED;
            mShopIdentity = shopIdentity;
            mOrder = order;
            mFlags = flags;
            nextStep();
        }
    }

    public void setUseMerchantApi(boolean use) {
        isUseMerchantApi = use;
    }

    public void setPinRequired(boolean pinRequired) {
        isPinRequired = pinRequired;
    }

    public void setAccountRequired(boolean accountRequired) {
        isAccountRequired = accountRequired;
    }

    public Order getOrder() {
        return mOrder;
    }

    public Identity getShopIdentity() {
        return mShopIdentity;
    }

    protected void stepLoginCheck() {
        mStep = Step.LOGIN_CHECK;
        if (!HonkioApi.isConnected()) {
            setStatus(Status.PENDING);
            onLoginRequired();
            return;
        }

        nextStep();
    }

    protected void stepShopRequest() {
        setStatus(Status.STARTED);
        mStep = Step.SHOP_REQUEST;

        String orderShopId = mOrder.getShopId();

        if (TextUtils.isEmpty(orderShopId)) {
            nextStep();
            return;
        }

        if (mShopIdentity instanceof Shop) {
            Shop shop = (Shop) mShopIdentity;
            if (shop.getMerchant() != null) {
                nextStep();
                return;
            }
        }

        if (mShopIdentity != null && orderShopId.equals(mShopIdentity.getId())) {
            loadShopByIdentity(mShopIdentity);
            return;
        }

        mShopFindTask = HonkioApi.shopFindById(orderShopId, 0, mShopFindCallback);
    }

    protected void stepAccountCheck() {
        mStep = Step.ACCOUNT_CHECK;
        if (isAccountRequired && !mOrder.isAccountValid()) {
            setStatus(Status.PENDING);
            onAccountRequired();
            return;
        }

        nextStep();
    }

    protected void stepPinCheck() {
        mStep = Step.PIN_CHECK;
        if (isPinRequired) {
            setStatus(Status.PENDING);
            onPinRequired();
            return;
        }
        nextStep();
    }

    protected void stepRequest() {
        setStatus(Status.STARTED);
        mStep = Step.REQUEST;
        mOrderSetTask = sendRequest(mShopIdentity, mOrder, mOrderUpdateCallback, mFlags);
    }

    protected RequestTask<Order> sendRequest(Identity shopIdentity, Order order,
                                             RequestCallback<Order> callback, int flags) {
        RequestTask<Order> requestTask;
        if (isUseMerchantApi) {
            if (shopIdentity == null) {
                requestTask = HonkioMerchantApi.merchantSetOrder(order, flags, callback);
            } else {
                requestTask = HonkioMerchantApi.merchantSetOrder(order, shopIdentity, flags, callback);
            }
        } else {
            if (shopIdentity == null) {
                requestTask = HonkioApi.userSetOrder(order, flags, callback);
            } else {
                requestTask = HonkioApi.userSetOrder(order, shopIdentity, flags, callback);
            }
        }
        return requestTask;
    }

    public void onPinEntered(String pin) {
        if (mStep != Step.PIN_CHECK || getStatus() != Status.PENDING)
            throw new IllegalStateException("Method onPinEntered must be called only on status PENDING and step PIN_CHECK.");
        if (!HonkioApi.checkPin(pin)) {
            setStatus(Status.ABORTED);
            onError(new RpError(RpError.Api.INVALID_PIN));
        } else {
            nextStep();
        }
    }

    public void onAccountSelected(UserAccount account) {
        if (mStep != Step.ACCOUNT_CHECK || getStatus() != Status.PENDING)
            throw new IllegalStateException("Method onAccountSelected must be called only on status PENDING and step ACCOUNT_CHECK.");
        if (mOrder != null)
            mOrder.setAccount(account);
        resume();
    }

    @Override
    public boolean onError(RpError error) {
        return false;
    }

    public boolean onPending(Response<Order> response) {
        return onError(response.getAnyError());
    }

    public void resume() {
        if (getStatus() == Status.PENDING) {
            switch (mStep) {
                case LOGIN_CHECK:
                    stepLoginCheck();
                    break;
                case ACCOUNT_CHECK:
                    stepAccountCheck();
                    break;
                default:
                    throw new IllegalStateException("Current step can't be resumed via resume().");
            }
        } else
            throw new IllegalStateException("Process model should be on status PENDING to be resumed.");
    }

    private void nextStep() {
        if (mStep.ordinal() < Step.values().length - 1) {
            Step nextStep = Step.values()[mStep.ordinal() + 1];

            switch (nextStep) {
                case LOGIN_CHECK:
                    stepLoginCheck();
                    break;
                case SHOP_REQUEST:
                    stepShopRequest();
                    break;
                case ACCOUNT_CHECK:
                    stepAccountCheck();
                    break;
                case PIN_CHECK:
                    stepPinCheck();
                    break;
                case REQUEST:
                    stepRequest();
                    break;
                default:
                    throw new IllegalStateException("Current step can't be moved to the next step.");
            }
        }
        else
            throw new IllegalStateException("Current step can't be moved to the next step.");

    }

    private void loadShopByIdentity(Identity identity) {
        mShopInfoTask = HonkioApi.shopGetInfo(identity, 0, mShopInfoCallback);
    }

    private RequestCallback<ShopFind> mShopFindCallback = new RequestCallback<ShopFind>() {
        @Override
        public boolean onComplete(Response<ShopFind> response) {
            mShopFindTask = null;
            if (response.isStatusAccept()) {
                List<Shop> list = response.getResult().getList();
                if (list.size() > 0) {
                    mShopIdentity = list.get(0);
                    loadShopByIdentity(mShopIdentity);
                    return true;
                }
                return onError(new RpError(
                        RpError.Group.SHOP_FIND,
                        RpError.subItemFromCode(RpError.ShopFind.NO_SHOP),
                        response.getMessage(), null,
                        "Shop '" + mOrder.getShopId() + "' not found"));
            }
            return onError(response.getAnyError());
        }

        @Override
        public boolean onError(RpError error) {
            mShopFindTask = null;
            setStatus(Status.FINISHED);
            return OrderSetProcessModel.this.onError(error);
        }
    };

    private RequestCallback<ShopInfo> mShopInfoCallback = new RequestCallback<ShopInfo>() {
        @Override
        public boolean onComplete(Response<ShopInfo> response) {
            mShopInfoTask = null;
            if (response.isStatusAccept()) {
                mShopIdentity = response.getResult().getShop();
                nextStep();
                return true;
            }
            return onError(response.getAnyError());
        }

        @Override
        public boolean onError(RpError error) {
            mShopInfoTask = null;
            setStatus(Status.FINISHED);
            return OrderSetProcessModel.this.onError(error);
        }
    };

    private RequestCallback<Order> mOrderUpdateCallback = new RequestCallback<Order>() {
        @Override
        public boolean onComplete(Response<Order> response) {
            mOrderSetTask = null;
            if (!isAborted()) {
                if (response.isStatusAccept()) {
                    setStatus(Status.FINISHED);
                    return OrderSetProcessModel.this.onComplete(response);
                } else if (response.isStatusPending()) {
                    setStatus(Status.FINISHED);
                    return OrderSetProcessModel.this.onPending(response);
                } else
                    return onError(response.getAnyError());
            }
            return true;
        }

        @Override
        public boolean onError(RpError error) {
            mOrderSetTask = null;
            setStatus(Status.FINISHED);
            return OrderSetProcessModel.this.onError(error);
        }
    };
}
