package com.riskpointer.sdk.app.ui.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.app.map.R;
import com.riskpointer.sdk.app.ui.view.AddressAutoCompleteTextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * Created by I.N. on 09.06.2016.
 */
public abstract class BaseSelectOnMapFragment extends BaseMapFragment {
    public static final String EXTRA_LOCALE = "MapsFragment.EXTRA_LOCALE";
    public static final String EXTRA_LOCATION = "MapsFragment.EXTRA_LOCATION";
    public static final String EXTRA_LOCATION_DESCRIPTION = "MapsFragment.EXTRA_LOCATION_DESCRIPTION";
    public static final String EXTRA_SEARCH_LOCATION = "MapsFragment.EXTRA_SEARCH_LOCATION";
    public static final String EXTRA_LOCATION_PICK = "MapsFragment.EXTRA_LOCATION_PICK";
    public static final String EXTRA_LOCATION_INFO = "MapsFragment.EXTRA_LOCATION_INFO";
    public static final String EXTRA_LOCATION_ADDRESS = "MapsFragment.EXTRA_LOCATION_ADDRESS";
    public static final String EXTRA_SEARCH_COUNTRY = "MapsFragment.EXTRA_SEARCH_COUNTRY";

    private static final float ZOOM_DEFAULT = 15;

    private static final int DRAWABLE_RIGHT = 2;

    private Location mLocation;
    private String mLocationDescription;

    private String mSearchLocation;
    private AddressAutoCompleteTextView mSearchEdit;

    private MapAnnotation mMarker;
    private Geocoder geocoder;

    private boolean isHasSetLocation = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        geocoder = new Geocoder(getContext(), getLocale());
    }

    @Override
    public String getScreenName() {
        return "Map";
    }

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_select_on_map;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        Bundle args = getArguments();
        if (args != null) {
            mLocation = args.getParcelable(EXTRA_LOCATION);
            mLocationDescription = args.getString(EXTRA_LOCATION_DESCRIPTION);
            mSearchLocation = args.getString(EXTRA_SEARCH_LOCATION);
        }
        mSearchEdit = view.findViewById(R.id.fragment_maps_search);

        view.findViewById(R.id.fragment_maps_search_panel).setVisibility(isPickingLocation() ? View.VISIBLE : View.GONE);

        View searchButton = view.findViewById(R.id.maps_search_address_btn);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mSearchEdit.getText().toString())) {
                    performSearch(mSearchEdit.getText().toString());
                }
            }

        });
        mSearchEdit.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!TextUtils.isEmpty(v.getText().toString())) {
                        performSearch(v.getText().toString());
                    }
                    return true;
                }
                return false;
            }
        });

        mSearchEdit.setCountry(searchCountry());
        mSearchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) { }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(s.toString())) {
                    mSearchEdit.setCompoundDrawablesWithIntrinsicBounds(
                            0, 0, 0, 0);
                } else {
                    mSearchEdit.setCompoundDrawablesWithIntrinsicBounds(
                            0, 0, R.mipmap.ic_close_small, 0);
                }
            }
        });

        mSearchEdit.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    Drawable[] drawables = mSearchEdit.getCompoundDrawables();
                    if (drawables.length > DRAWABLE_RIGHT && drawables[DRAWABLE_RIGHT] != null) {
                        if (event.getRawX() >= (mSearchEdit.getRight() - drawables[DRAWABLE_RIGHT].getBounds().width())) {
                            mSearchEdit.setText("");
                            return true;
                        }
                    }
                }
                return false;
            }
        });

        super.onViewCreated(view, savedInstanceState);

        if (checkRuntimePermission(ACCESS_FINE_LOCATION))
            getMapView().setShowUserLocation(true);

        if (mLocation != null) {
            replaceMarker(mLocation, mLocationDescription, true);
        } else if (!TextUtils.isEmpty(mSearchLocation)) {
            performSearch(mSearchLocation);
        } else {
            Location myLocation = HonkioApi.getLocation();
            if (myLocation != null)
                getMapView().moveToLocation(myLocation, ZOOM_DEFAULT, false);
        }

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private Locale getLocale() {
        Bundle args = getArguments();
        if (args != null && args.containsKey(EXTRA_LOCALE)) {
            return (Locale) args.get(EXTRA_LOCALE);
        } else {
            return Locale.getDefault();
        }

    }

    public void moveToUserLocation() {
        Location myLocation = HonkioApi.getLocation();
        if (myLocation != null)
            getMapView().moveToLocation(myLocation, ZOOM_DEFAULT, true);
    }

    public void moveToMarkerLocation() {
        if (mMarker != null) {
            getMapView().moveToLocation(mMarker.location, ZOOM_DEFAULT, true);
        }
    }

    public void onOkClick() {
        if (mMarker != null) {
            try {
                List<Address> addresses = geocoder.getFromLocation(mMarker.location.getLatitude(),
                        mMarker.location.getLongitude(), 1);
                if (addresses != null && !addresses.isEmpty())
                    onAddressSelected(mMarker.location, addresses.get(0));
                else
                    onAddressSelected(mMarker.location, null);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            Toast.makeText(getContext(), getString(R.string.screen_select_on_map_msg_address_not_selected), Toast.LENGTH_SHORT).show();
        }
    }

    public void onAddressSelected(Location location, Address address) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_LOCATION, location);

        if (address != null) {
            intent.putExtra(EXTRA_LOCATION_ADDRESS, address);

            StringBuilder addressLine = new StringBuilder();
            for (int i = 4; i >= 0; i--) {
                String subAddress = address.getAddressLine(i);
                if (!TextUtils.isEmpty(subAddress)) {
                    if (addressLine.length() > 0)
                        addressLine.append(", ");
                    addressLine.append(subAddress);
                }
            }

            if (addressLine.length() > 0)
                intent.putExtra(EXTRA_LOCATION_INFO, addressLine.toString());
        }

        finish(Activity.RESULT_OK, intent);
    }

    public void setSelection(Location location, String desc) {
        mLocation = location;
        mLocationDescription = desc;

        replaceMarker(mLocation, mLocationDescription, true);
    }

    @Override
    public void onCameraMove() {
        super.onCameraMove();
        if (mSearchEdit != null && getMapView().isMapReady()) {
            Location location = getMapView().getCenter();
            mSearchEdit.setLocation(location.getLatitude(), location.getLongitude());
        }
    }

    @Override
    public boolean onMapClick(Location location) {
        if (isPickingLocation()) {
            List<Address> addresses;
            StringBuilder addressLine = new StringBuilder();
            try {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                if (addresses != null && !addresses.isEmpty()) {
                    Address address = addresses.get(0);
                    for (int i = 4; i >= 0; i--) {
                        String subAddress = address.getAddressLine(i);
                        if (!TextUtils.isEmpty(subAddress)) {
                            if (addressLine.length() > 0)
                                addressLine.append(", ");
                            addressLine.append(subAddress);
                        }
                    }

                    String postalCode = address.getPostalCode() != null ? address.getPostalCode() : "";
                    String address1 =
                            address.getThoroughfare() == null
                                    ? "" : address.getThoroughfare()
                                    + (address.getFeatureName() == null
                                    ? "" : ", " + address.getFeatureName());

                    mSearchEdit.setText(String.format("%s %s", postalCode, address1));
                }
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            replaceMarker(location, addressLine.toString(), true);
            return true;
        }

        return super.onMapClick(location);
    }

    protected void onMarkerPlaced() {

    }

    protected boolean isPickingLocation() {
        Bundle args = getArguments();
        return args != null && args.getBoolean(EXTRA_LOCATION_PICK);
    }

    protected String searchCountry() {
        Bundle args = getArguments();
        if (args != null)
            return args.getString(EXTRA_SEARCH_COUNTRY);
        return null;
    }

    private void performSearch(String query) {
        if (query != null && !query.isEmpty()) {
            new SyncSearchTask(this).execute(query);
        }
    }

    private void replaceMarker(Location location, String address, boolean animateCamera) {
        getMapView().removeAllAnnotations();

        if (address == null)
            address = "";

        mMarker = new MapAnnotation(location);
        mMarker.title = address;

        getMapView().addAnnotation(mMarker);

        if (animateCamera)
            getMapView().moveToLocation(location, ZOOM_DEFAULT, true);

        onMarkerPlaced();
    }

    private static class SyncSearchTask extends AsyncTask<String, Void, Address> {

        private BaseSelectOnMapFragment mFragment;

        SyncSearchTask(BaseSelectOnMapFragment fragment) {
            super();
            mFragment = fragment;
        }

        @Override
        protected Address doInBackground(String... params) {
            try {
                List<Address> addresses = mFragment.geocoder.getFromLocationName(params[0], 1);
                if (addresses != null && !addresses.isEmpty()) {
                    return addresses.get(0);
                } else {
                    return null;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Address address) {
            if (address == null)
                Toast.makeText(mFragment.getContext(), mFragment.getString(R.string.screen_select_on_map_msg_address_not_found), Toast.LENGTH_SHORT).show();
            else {
                String addressLine = (
                    address.getLocality() != null ?
                    address.getLocality() : "") + ' ' + (address.getAddressLine(0) != null ?
                    (address.getAddressLine(0)) : ""
                );


                Location location = new Location("");
                location.setLatitude(address.getLatitude());
                location.setLongitude(address.getLongitude());

                mFragment.replaceMarker(location, addressLine, true);
            }
        }
    }

}
