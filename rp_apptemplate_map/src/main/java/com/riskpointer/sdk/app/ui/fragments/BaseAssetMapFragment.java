package com.riskpointer.sdk.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Asset;
import com.riskpointer.sdk.api.model.entity.AssetList;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.message.filter.AssetFilter;
import com.riskpointer.sdk.app.ui.strategy.MapLoader;
import com.riskpointer.sdk.app.utils.FragmentUtils;

import java.util.List;

/**
 * Created by I.N. on 26.03.2018.
 */
public abstract class BaseAssetMapFragment extends BaseLazyMapFragment<Asset> {

    public static final String ARG_FILTER = "BaseAssetsMapFragment.ARG_FILTER";

    private AssetFilter mFilter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mFilter = (AssetFilter) savedInstanceState.getSerializable(ARG_FILTER);
        } else if (mFilter == null) {
            Bundle args = getArguments();
            if (args != null)
                mFilter = (AssetFilter) args.getSerializable(ARG_FILTER);
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(ARG_FILTER, mFilter);
    }

    @Override
    protected MapAnnotation buildMapAnnotation(Asset item) {
        MapAnnotation annotation = new MapAnnotation(item.getLatitude(), item.getLongitude());
        annotation.setItem(item);
        annotation.title = item.getName();
        return annotation;
    }

    @Override
    protected MapLoader<Asset, ?> buildMapLoader() {
        return new UserAssetsMapLoader(getFilter(),
                FragmentUtils.getShopIdentity(this, null));
    }

    public AssetFilter getFilter() {
        if (mFilter == null) {
            Bundle args = getArguments();
            if (args != null)
                mFilter = (AssetFilter) args.getSerializable(ARG_FILTER);

            if (mFilter == null) {
                mFilter = new AssetFilter();
            }

        }
        return mFilter;
    }

    public void setFilter(AssetFilter filter) {
        mFilter = filter;
    }

    public static abstract class BaseAssetsMapLoader<Result> extends MapLoader<Asset, Result> {

        protected AssetFilter mFilter;

        public BaseAssetsMapLoader(AssetFilter filter) {
            mFilter = filter;
        }

        protected AssetFilter getFilter(double latitude, double longitude, long radius) {
            mFilter.setLatitude(latitude);
            mFilter.setLongitude(longitude);
            mFilter.setRadius(Long.toString(radius));
            return mFilter;
        }

    }

    public static class UserAssetsMapLoader extends BaseAssetsMapLoader<AssetList> {

        private Identity mShopIdentity;

        public UserAssetsMapLoader(AssetFilter filter, Identity shopIdentity) {
            super(filter);
            mShopIdentity = shopIdentity;
        }

        @Override
        protected List<Asset> toList(AssetList assetList) {
            return assetList.getList();
        }

        @Override
        protected Task queryData(double latitude, double longitude, long radius, RequestCallback<AssetList> requestCallback) {
            return HonkioApi.userAssetList(getFilter(latitude, longitude, radius),
                    mShopIdentity, 0, requestCallback);
        }
    }
}
