package com.riskpointer.sdk.app.ui.fragments;

import android.content.Intent;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.ShopFind;
import com.riskpointer.sdk.api.model.entity.ShopInfo;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.utils.LocationFinder;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.filter.ShopFilter;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.strategy.MapLoader;

import java.util.List;

/**
 * Created by Shurygin Denis on 2016-05-23.
 */
public abstract class ShopsMapFragment extends BaseShopsMapFragment<Shop> {

    public static class ShopsMapLoader extends BaseShopsMapLoader<Shop, ShopFind> {

        public ShopsMapLoader(ShopFilter filter) {
            super(filter);
        }

        @Override
        protected List<Shop> toList(ShopFind shopFind) {
            return shopFind.getList();
        }

        @Override
        protected Task queryData(double latitude, double longitude, long radius,
                                 RequestCallback<ShopFind> requestCallback) {
            return HonkioApi.shopFind(getFilterFor(latitude, longitude, radius),
                    0, requestCallback);
        }

        protected ShopFilter getFilterFor(double latitude, double longitude, long radius) {
            mFilter.setLatitude(LocationFinder.formatLocation(latitude));
            mFilter.setLongitude(LocationFinder.formatLocation(longitude));
            mFilter.setRadius(Long.toString(radius));
            mFilter.setCount(50);
            return mFilter;
        }
    }

    @Override
    protected MapLoader<Shop, ?> buildMapLoader() {
        return new ShopsMapLoader(getFilter());
    }

    @Override
    protected MapAnnotation buildMapAnnotation(Shop item) {
        MapAnnotation annotation = new MapAnnotation(item.getLatitude(), item.getLongitude());
        annotation.title = item.getName();
        return annotation;
    }

    @Override
    protected void onMapItemClick(Shop item, MapAnnotation annotation) {
        onShopClick(item);
    }

    protected void onShopClick(Shop shop) {
        if (shop != null) {
            openShopPage(shop);
        }
    }

    protected void openShopPage(Shop shop) {
        showProgressDialog();
        HonkioApi.shopGetInfo(shop, 0, mShopOpenCallback);
    }

    private RequestCallback<ShopInfo> mShopOpenCallback = new RequestCallback<ShopInfo>() {

        @Override
        public boolean onError(RpError error) {
            dismissProgressDialog();
            return isAdded() && ShopsMapFragment.this.onError(error);
        }

        @Override
        public boolean onComplete(Response<ShopInfo> response) {
            dismissProgressDialog();
            if (response.isStatusAccept()) {
                Intent intent = AppController.getIntent(AppController.SCREEN_SHOP, response.getResult().getShop());
                startActivity(intent);
                return true;
            } else
                return onError(response.getAnyError());
        }
    };
}
