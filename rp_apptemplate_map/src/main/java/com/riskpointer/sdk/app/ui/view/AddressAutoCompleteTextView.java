package com.riskpointer.sdk.app.ui.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import com.riskpointer.sdk.api.utils.ApiWebUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @author Shurygin Denis
 */

public class AddressAutoCompleteTextView extends AppCompatAutoCompleteTextView {

    private double mLatitude;
    private double mLongitude;
    private long mRadius = 50000;
    private String mCountry;

    public AddressAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setThreshold(2);
        setAdapter(new AddressAdapter(getContext(), new ArrayList<String>()));
    }

    public void setLocation(double latitude, double longitude) {
        mLatitude = latitude;
        mLongitude = longitude;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    private class AddressAdapter extends ArrayAdapter<String> {

        private final String mApiKey;
        private final List<String> mList;
        private final AddressFilter mFilter = new AddressFilter();

        public AddressAdapter(@NonNull Context context, List<String> list) {
            super(context, android.R.layout.simple_list_item_1, android.R.id.text1, list);
            mApiKey = getApiKey(context);
            mList = list;
        }

        @NonNull
        @Override
        public Filter getFilter() {
            return mFilter;
        }

        private class AddressFilter extends Filter {

            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                Response response;
                try {
                    Call callRequest = ApiWebUtils.getClient().newCall(
                            new Request.Builder().url(buildUrl(charSequence.toString()))
                                    .addHeader("Connection", "Keep-Alive").build());

                    response = callRequest.execute();
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }

                JSONObject json;

                try {
                    json = new JSONObject(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                } catch (JSONException e) {
                    e.printStackTrace();
                    return null;
                }

                mList.clear();

                List<String> list = parse(json);
                if (list != null)
                    mList.addAll(list);


                FilterResults result = new FilterResults();
                result.values = mList;
                result.count = mList.size();
                return result;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                notifyDataSetChanged();
            }
        }

        private String buildUrl(String text) {
            StringBuilder builder = new StringBuilder(
                    "https://maps.googleapis.com/maps/api/place/autocomplete/json?");

            builder.append("types=geocode&sensor=false&key=").append(mApiKey);

            try {
                String input = "&input=" + URLEncoder.encode(text, "utf-8");
                builder.append(input);
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            if (mLatitude != 0 || mLongitude != 0) {
                builder.append("&location=")
                        .append(String.format("%.6f,%.6f", mLatitude, mLongitude));

                if ( mRadius != 0)
                    builder.append("&radius=").append(Long.toString(mRadius));
            }

            if (!TextUtils.isEmpty(mCountry))
                builder.append("&components=country:")
                    .append(mCountry);

            return builder.toString();
        }

        private List<String> parse(JSONObject jObject) {
            if ("OK".equals(jObject.optString("status"))) {
                JSONArray jPlaces;
                try {
                    jPlaces = jObject.getJSONArray("predictions");
                } catch (JSONException e) {
                    e.printStackTrace();
                    return null;
                }

                List<String> placesList = new ArrayList<>();
                for (int i = 0; i < jPlaces.length(); i++) {
                    JSONObject placeJson = jPlaces.optJSONObject(i);
                    if (placeJson != null) {
                        String place = placeJson.optString("description");
                        if (place != null)
                            placesList.add(place);
                    }
                }

                return placesList;
            }
            else {
                try {
                    Log.d("AddressTextView", jObject.toString(2));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }

        private String getApiKey(Context context) {
            return "AIzaSyAqNq30DT13uPY50dS77tq7oLAMrPNK9I4";
        }
    }
}
