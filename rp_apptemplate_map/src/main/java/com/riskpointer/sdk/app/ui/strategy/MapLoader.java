package com.riskpointer.sdk.app.ui.strategy;

import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.web.callback.ErrorCallback;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;

import java.util.List;

/**
 * @author Shurygin Denis
 */
public abstract class MapLoader<Item, Result> {

    public interface Callback<Item> extends ErrorCallback {

        void onDataLoad(List<Item> data);

    }

    private static class FeatureUpdate {
        final double latitude;
        final double longitude;
        final long radius;

        FeatureUpdate(double latitude, double longitude, long radius) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.radius = radius;
        }
    }

    private Callback<Item> mCallback;

    private Task mRefreshTask;
    private FeatureUpdate mFeatureUpdate;

    private boolean isRecycled = false;

    protected abstract List<Item> toList(Result result);
    protected abstract Task queryData(double latitude, double longitude, long radius,
                                      RequestCallback<Result> requestCallback);

    public void setCallback(Callback<Item> callback) {
        mCallback = callback;
    }

    public void load(double latitude, double longitude, long radius) {
        if (isRecycled)
            return;

        if (mRefreshTask == null) {
            mFeatureUpdate = null;
            mRefreshTask = queryData(latitude, longitude, radius, mRefreshDataCallback);
        } else {
            mFeatureUpdate = new FeatureUpdate(latitude, longitude, radius);
        }
    }

    private void notifyCallback(List<Item> data) {
        Callback<Item> callback = mCallback;
        if (callback != null)
            callback.onDataLoad(data);
    }

    private void notifyCallback(RpError error) {
        Callback<Item> callback = mCallback;
        if (callback != null)
            callback.onError(error);
    }

    private void recycle() {
        mFeatureUpdate = null;
        isRecycled = true;
        Task task = mRefreshTask;
        if (task != null)
            task.abort();
        mRefreshTask = null;
    }

    private RequestCallback<Result> mRefreshDataCallback = new RequestCallback<Result>() {
        @Override
        public boolean onComplete(Response<Result> response) {
            mRefreshTask = null;
            if (!isRecycled) {

                if (mFeatureUpdate != null)
                    load(mFeatureUpdate.latitude, mFeatureUpdate.longitude, mFeatureUpdate.radius);

                if (response.isStatusAccept()) {
                    List<Item> list = toList(response.getResult());
                    notifyCallback(list);
                    return true;
                } else
                    return onError(response.getAnyError());
            }
            return false;
        }

        @Override
        public boolean onError(RpError error) {
            mRefreshTask = null;
            if (!isRecycled) {

                if (mFeatureUpdate != null)
                    load(mFeatureUpdate.latitude, mFeatureUpdate.longitude, mFeatureUpdate.radius);

                notifyCallback(error);
            }
            return false;
        }
    };
}
