package com.riskpointer.sdk.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.web.message.filter.ShopFilter;
import com.riskpointer.sdk.app.ui.strategy.MapLoader;

/**
 * Created by Shurygin Denis on 2016-05-19.
 */
public abstract class BaseShopsMapFragment<T extends Identity> extends BaseLazyMapFragment<T> {

    public static final String ARG_FILTER = "BaseShopsMapFragment.ARG_FILTER";

    private ShopFilter mFilter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mFilter = (ShopFilter) savedInstanceState.getSerializable(ARG_FILTER);
        } else if (mFilter == null) {
            Bundle args = getArguments();
            if (args != null)
                mFilter = (ShopFilter) args.getSerializable(ARG_FILTER);
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(ARG_FILTER, mFilter);
    }

    public ShopFilter getFilter() {
        if (mFilter == null) {
            Bundle args = getArguments();
            if (args != null)
                mFilter = (ShopFilter) args.getSerializable(ARG_FILTER);

            if (mFilter == null) {
                mFilter = new ShopFilter();
            }

        }
        return mFilter;
    }

    public static abstract class BaseShopsMapLoader<Item extends Identity, Result>
            extends MapLoader<Item, Result> {

        protected ShopFilter mFilter;

        public BaseShopsMapLoader(ShopFilter filter) {
            mFilter = filter;
        }

    }
}
