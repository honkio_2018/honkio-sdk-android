package com.riskpointer.sdk.app.ui.fragments;

import com.riskpointer.sdk.api.merchant.HonkioMerchantApi;
import com.riskpointer.sdk.api.model.entity.merchant.MerchantShop;
import com.riskpointer.sdk.api.model.entity.merchant.MerchantShopsList;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.utils.LocationFinder;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.message.filter.ShopFilter;
import com.riskpointer.sdk.app.ui.strategy.MapLoader;

import java.util.List;

/**
 * Created by I.N. on 2017-02-01.
 */
public abstract class MerchantShopsMapFragment extends BaseShopsMapFragment<MerchantShop> {

    public static class ShopsMapLoader extends BaseShopsMapLoader<MerchantShop, MerchantShopsList> {

        public ShopsMapLoader(ShopFilter filter) {
            super(filter);
        }

        @Override
        protected List<MerchantShop> toList(MerchantShopsList shopFind) {
            return shopFind.getList();
        }

        @Override
        protected Task queryData(double latitude, double longitude, long radius,
                                 RequestCallback<MerchantShopsList> requestCallback) {
            return HonkioMerchantApi.merchantShopsList(getFilterFor(latitude, longitude, radius),
                    0, requestCallback);
        }

        protected ShopFilter getFilterFor(double latitude, double longitude, long radius) {
            mFilter.setLatitude(LocationFinder.formatLocation(latitude));
            mFilter.setLongitude(LocationFinder.formatLocation(longitude));
            mFilter.setRadius(Long.toString(radius));
            mFilter.setCount(50);
            return mFilter;
        }
    }

    @Override
    protected MapLoader<MerchantShop, ?> buildMapLoader() {
        return new ShopsMapLoader(getFilter());
    }

    @Override
    protected MapAnnotation buildMapAnnotation(MerchantShop item) {
        MapAnnotation annotation = new MapAnnotation(item.getLatitude(), item.getLongitude());
        return annotation;
    }

    @Override
    protected void onMapItemClick(MerchantShop item, MapAnnotation annotation) {
        onShopClick(item);
    }

    protected void onShopClick(MerchantShop shop) {
        if (shop != null) {
            openShopPage(shop);
        }
    }

    protected void openShopPage(MerchantShop shop) {

    }

}
