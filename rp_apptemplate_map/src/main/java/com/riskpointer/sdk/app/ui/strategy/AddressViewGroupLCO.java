package com.riskpointer.sdk.app.ui.strategy;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.view.View;

import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.map.R;
import com.riskpointer.sdk.app.ui.fragments.BaseSelectOnMapFragment;
import com.riskpointer.sdk.app.utils.RequestCodes;

/**
 * @author Shurygin Denis
 */
public class AddressViewGroupLCO extends BaseAddressViewGroupLCO {

    public AddressViewGroupLCO() {
        super();
    }

    public AddressViewGroupLCO(String tag) {
        super(tag);
    }

    @Override
    public void onViewCreated(ViewController viewController, Bundle savedInstanceState) {
        super.onViewCreated(viewController, savedInstanceState);

        viewController.findViewById(R.id.hk_btnSelectLocationOnMap)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestFocus();
                        showMapSelection();
                    }
                });
    }

    @Override
    public void onActivityResult(ViewController viewController, int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCodes.REQUEST_PICK_LOCATION && resultCode == Activity.RESULT_OK)
            onAddressSelected(data);
        else
            super.onActivityResult(viewController, requestCode, resultCode, data);
    }

    @Override
    protected void onAddressNotFound() {
        showMessage(null, 0, R.string.dlg_failed_to_get_location);
    }

    private void showMapSelection() {
        getController().startActivityForResult(AppController.getIntent(AppController.SCREEN_MAP_SELECT_ADDRESS,
                getLocation(), null, true, getAddressCountry().getIso()),
                RequestCodes.REQUEST_PICK_LOCATION);
    }

    private void onAddressSelected(Intent intent) {
        if (intent != null) {
            if (intent.hasExtra(BaseSelectOnMapFragment.EXTRA_LOCATION)) {
                if (intent.hasExtra(BaseSelectOnMapFragment.EXTRA_LOCATION_INFO)) {
                    onAddressSelected(
                            (Address) intent.getParcelableExtra(BaseSelectOnMapFragment.EXTRA_LOCATION_ADDRESS),
                            (Location) intent.getParcelableExtra(BaseSelectOnMapFragment.EXTRA_LOCATION)
                    );
                } else {
                    onAddressSelected(null,
                            (Location) intent.getParcelableExtra(BaseSelectOnMapFragment.EXTRA_LOCATION)
                    );
                }
            }
        }
    }

}
