package com.riskpointer.sdk.app.ui.fragments;

import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.riskpointer.sdk.app.map.R;

/**
 * Created by I.N. on 10.06.2016.
 */
public abstract class BaseMapFragment extends BaseFragment {

    public static final class MapAnnotation {

        public MapAnnotation(double latitude, double longitude) {
            location = new Location("");
            location.setLatitude(latitude);
            location.setLongitude(longitude);
        }

        public MapAnnotation(Location location) {
            this.location = location;
        }

        public final Location location;

        public String title;
        public String subtitle;

        private Object item;

        private int iconId;
        private Bitmap iconBitmap;

        public void setIcon(int resId) {
            this.iconId = resId;

            Bitmap bitmap = this.iconBitmap;
            if (bitmap != null)
                bitmap.recycle();

            this.iconBitmap = null;
        }

        public int getIconResId() {
            return iconId;
        }

        public void setIcon(Bitmap bitmap) {
            if (bitmap == this.iconBitmap)
                return;

            Bitmap oldBitmap = this.iconBitmap;
            if (oldBitmap != null)
                oldBitmap.recycle();

            this.iconBitmap = bitmap;
            this.iconId = 0;
        }

        public Bitmap getIconBitmap() {
            return iconBitmap;
        }

        public boolean isHasIcon() {
            return this.iconId != 0 || this.iconBitmap != null;
        }

        public void setItem(Object item) {
            this.item = item;
        }

        @SuppressWarnings("unchecked")
        public <T> T getItem() {
            return (T) item;
        }

    }

    public interface MapView {

        boolean isMapReady();

        void setShowUserLocation(boolean value);
        boolean isShowUserLocation();

        void addAnnotation(MapAnnotation annotation);
        void removeAnnotation(MapAnnotation annotation);
        void removeAllAnnotations();

        void moveToLocation(Location location, float zoom, boolean animated);

        Location getCenter();
        double getRadius();

    }

    private MapView mMapView;

    protected abstract Fragment buildMapFragment();

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Fragment mapFragment = getChildFragmentManager().findFragmentById(R.id.hk_content);

        if (mapFragment == null) {
            mapFragment = buildMapFragment();
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(R.id.hk_content, mapFragment)
                    .commit();
        }

        if (!(mapFragment instanceof MapView))
            throw new IllegalArgumentException(
                    "Fragment returned by buildMapFragment must implement MapView interface");

        mMapView = (MapView) mapFragment;
    }

    protected MapView getMapView() {
        return mMapView;
    }

    public void onCameraMove() {

    }

    public boolean onMapClick(Location location) {
        return false;
    }

    public boolean onMapAnnotationClick(MapAnnotation annotation) {
        return false;
    }

    public void onUserLocationChange(Location location) {

    }

}
