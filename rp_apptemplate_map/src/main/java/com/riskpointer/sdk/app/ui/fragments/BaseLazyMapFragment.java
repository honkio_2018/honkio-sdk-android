package com.riskpointer.sdk.app.ui.fragments;

import android.location.Location;
import android.os.Bundle;
import android.view.View;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.app.map.R;
import com.riskpointer.sdk.app.ui.strategy.MapLoader;

import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * @author Shurygin Denis
 */
public abstract class BaseLazyMapFragment<T> extends BaseMapFragment implements MapLoader.Callback<T> {

    private static final float DEFAULT_ZOOM = 15;
    private static final long DEFAULT_SEARCH_RADIUS = 5000;

    private List<T> mList;

    protected abstract void onMapItemClick(T item, MapAnnotation annotation);

    protected abstract MapAnnotation buildMapAnnotation(T item);
    protected abstract MapLoader<T, ?> buildMapLoader();

    private MapLoader<T, ?> mLoader;

    /**
     * True if any location was set for the map camera.
     */
    private boolean isHasSetLocation = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLoader = buildMapLoader();
        mLoader.setCallback(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        onMapShouldRefresh();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (checkRuntimePermission(ACCESS_FINE_LOCATION))
            getMapView().setShowUserLocation(true);

        Location myLocation = HonkioApi.getLocation();
        if (myLocation != null)
            moveCameraTo(myLocation, false);

        if (mList != null)
            onDataLoad(mList);

        onMapShouldRefresh();
    }

    @Override
    public void onCameraMove() {
        super.onCameraMove();
        onMapShouldRefresh();
    }

    @Override
    public void onUserLocationChange(Location location) {
        if (!isHasSetLocation) {
            moveCameraTo(location, true);
        }
    }

    @Override
    public boolean onMapAnnotationClick(MapAnnotation annotation) {
        T item = annotation.getItem();
        if (item != null) {
            onMapItemClick(item, annotation);
            return true;
        }
        return super.onMapAnnotationClick(annotation);
    }

    public void onMapShouldRefresh() {
        Location location;
        double radius = DEFAULT_SEARCH_RADIUS;

        MapView mapView = getMapView();
        if (mapView.isMapReady()) {
            location = mapView.getCenter();
            radius = mapView.getRadius();
        } else {
            location = HonkioApi.getLocation();
        }

        if (location != null) {
            mLoader.load(location.getLatitude(), location.getLongitude(), (long) radius);
        }
    }

    public void onDataLoad(List<T> data) {
        mList = data;
        getMapView().removeAllAnnotations();

        for (T item : mList) {
            getMapView().addAnnotation(buildMapAnnotation(item));
        }
    }

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_assets_map;
    }

    protected void moveCameraTo(Location location, boolean animated) {
        isHasSetLocation = true;
        getMapView().moveToLocation(location, DEFAULT_ZOOM, animated);
    }
}
