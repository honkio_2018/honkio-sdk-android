/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.dialogs;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;

import com.riskpointer.sdk.app.R;

public class MessageDialog extends BaseDialogFragment {
    public static final String ARG_TITLE = "MessageDialog.ARG_TITLE";
    public static final String ARG_MESSAGE = "MessageDialog.ARG_MESSAGE";
    public static final String ARG_BUTTONS = "MessageDialog.ARG_BUTTONS";
    public static final String ARG_BUTTON_POSITIVE = "MessageDialog.ARG_BUTTON_POSITIVE";
    public static final String ARG_BUTTON_NEGATIVE = "MessageDialog.ARG_BUTTON_NEGATIVE";
    public static final String ARG_BUTTON_NEUTRAL = "MessageDialog.ARG_BUTTON_NEUTRAL";

    public static final int BTN_NOT_SET         = 0;
    public static final int BTN_OK              = 1;
    public static final int BTN_OK_CANCEL       = 2;
    public static final int BTN_YES_NO          = 3;
    public static final int BTN_YES_NO_CANCEL   = 4;

// =================================================================================================
// Methods of Super Class
// =================================================================================================

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new Builder(getActivity());

        String mTitleString = getTitleString();
        String mMessageString = getMessageString();

        if(mTitleString != null)
            builder.setTitle(mTitleString);
        builder.setMessage(mMessageString);

        int buttonsVariant = getButtons();

        switch (buttonsVariant) {
            case BTN_NOT_SET:
                String positiveButton = null;
                String negativeButton = null;
                String neutralButton = null;
                Bundle args = getArguments();
                if (args != null) {
                    positiveButton = getArgsString(args, ARG_BUTTON_POSITIVE);
                    negativeButton = getArgsString(args, ARG_BUTTON_NEGATIVE);
                    neutralButton = getArgsString(args, ARG_BUTTON_NEUTRAL);
                }
                if (positiveButton != null || negativeButton != null || neutralButton != null) {
                    if (positiveButton != null)
                        builder.setPositiveButton(positiveButton, this);
                    if (negativeButton != null)
                        builder.setNegativeButton(negativeButton, this);
                    if (neutralButton != null)
                        builder.setNeutralButton(neutralButton, this);
                }
                else {
                    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dismiss();
                        }
                    });
                }
                break;
            case BTN_OK:
                builder.setPositiveButton(android.R.string.ok, this);
                break;
            case BTN_OK_CANCEL:
                builder.setPositiveButton(android.R.string.ok, this);
                builder.setNegativeButton(android.R.string.cancel, this);
                break;
            case BTN_YES_NO:
                builder.setPositiveButton(R.string.dlg_common_button_yes, this);
                builder.setNegativeButton(R.string.dlg_common_button_no, this);
                break;
            case BTN_YES_NO_CANCEL:
                builder.setPositiveButton(R.string.dlg_common_button_yes, this);
                builder.setNegativeButton(R.string.dlg_common_button_no, this);
                builder.setNeutralButton(R.string.dlg_common_button_cancel, this);
                break;

        }

        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP)
                    onClick(dialog, DialogInterface.BUTTON_NEUTRAL);
                return false;
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

// =================================================================================================
// Protected methods and classes
// =================================================================================================

    protected String getTitleString() {
        return getArgsString(getArguments(), ARG_TITLE);
    }

    protected String getMessageString() {
        return getArgsString(getArguments(), ARG_MESSAGE);
    }

    protected int getButtons() {
        Bundle args = getArguments();
        if (args != null)
            return args.getInt(ARG_BUTTONS, 0);
        return 0;
    }
}
