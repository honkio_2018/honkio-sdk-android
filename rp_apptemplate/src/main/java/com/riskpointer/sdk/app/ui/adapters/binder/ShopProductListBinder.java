package com.riskpointer.sdk.app.ui.adapters.binder;

import android.view.View;

import com.riskpointer.sdk.api.model.entity.Product;

/**
 * Created by Shurygin Denis on 2016-07-01.
 */
public class ShopProductListBinder extends AbsShopProductListBinder<Product,
        AbsShopProductListBinder.ViewHolder> {

    @Override
    public ViewHolder newHolder(View view) {
        return new ViewHolder(view);
    }
}
