package com.riskpointer.sdk.app;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;

/**
 * Parcelable object that hold all required information to instantiate a new Fragment.
 *
 * @author Shurygin Denis on 2015-06-09.
 */
public class FragmentComponents implements Parcelable {

    public static final Creator<FragmentComponents> CREATOR = new Creator<FragmentComponents>() {

        @Override
        public FragmentComponents createFromParcel(Parcel in) {
            return new FragmentComponents(in);
        }

        @Override
        public FragmentComponents[] newArray(int size) {
            return new FragmentComponents[size];
        }
    };

    private Class<?> mFragmentClass;
    private Bundle mArguments;

    public FragmentComponents() {   }

    public FragmentComponents(Class<?> fragmentClass) {
        this(fragmentClass, null);
    }

    public FragmentComponents(Class<?> fragmentClass, Bundle arguments) {
        mFragmentClass = fragmentClass;
        mArguments = arguments;
    }

    private FragmentComponents(Parcel parcel) {
        mFragmentClass = (Class<?>) parcel.readSerializable();
        mArguments = parcel.readBundle();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeSerializable(mFragmentClass);
        parcel.writeBundle(mArguments);
    }

    public Fragment newFragment() {
        Fragment fragment;

        Class<?> clazz = mFragmentClass;
        if (clazz == null)
            throw new NullPointerException("Fragment class is null");

        try {
            fragment = (Fragment) clazz.newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        if (fragment != null)
            fragment.setArguments(mArguments);

        return fragment;
    }

    public DialogFragment newDialog() {
        return (DialogFragment) newFragment();
    }

    public Class<?> getFragmentClass() {
        return mFragmentClass;
    }

    public Bundle getArguments() {
        return mArguments;
    }

    public void setFragmentClass(Class<?> fragmentClass) {
        mFragmentClass = fragmentClass;
    }

    public void setArguments(Bundle arguments) {
        mArguments = arguments;
    }

    public boolean isFragment() {
        return mFragmentClass != null;
    }

    public boolean isDialog() {
        return mFragmentClass != null;
    }
}
