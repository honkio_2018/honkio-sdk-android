/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.textvalidator;

import java.util.regex.Pattern;

import android.util.Patterns;

public class PhoneTextValidator extends BaseTextValidator {
	
    private static final Pattern mPattern = Patterns.PHONE;

	public PhoneTextValidator(String errorMessage) {
		super(errorMessage);
	}

	@Override
	public boolean validate(String string) {
		return mPattern.matcher(string).matches();
	}

}
