package com.riskpointer.sdk.app.ui.fragments.list;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Event;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.Schedule;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.filter.SplitScheduleFilter;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.ui.adapters.BaseRecyclerListAdapter;
import com.riskpointer.sdk.app.utils.FragmentUtils;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Shurygin Denis on 2016-05-31.
 */
public abstract class BaseEventsListFragment extends BaseRecycleListFragment implements RequestCallback<Schedule> {

    public static final String ARG_FILTER = "EventsListFragment.ARG_FILTER";

    private SplitScheduleFilter mFilter;
    private Task mRefreshTask;

    private BaseRecyclerListAdapter<Event, ?> mAdapter;

    protected abstract BaseRecyclerListAdapter<Event, ?> newAdapter(Context context, List<Event> list);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mFilter = (SplitScheduleFilter) savedInstanceState.getSerializable(ARG_FILTER);
        }
        else {
            Bundle args = getArguments();
            if (args != null && args.containsKey(ARG_FILTER))
                mFilter = (SplitScheduleFilter) args.getSerializable(ARG_FILTER);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mFilter != null)
            outState.putSerializable(ARG_FILTER, mFilter);
    }

    @Override
    public boolean onError(RpError error) {
        if (isAdded()) {
            setRefreshing(false);
            setListShown(true);
        }
        return super.onError(error);
    }

    @Override
    public boolean onComplete(Response<Schedule> response) {
        Activity activity = getActivity();
        if(activity != null) {
            setRefreshing(false);
            if (response.isStatusAccept()) {
                List<Event> list = toList(response.getResult());

                if (mAdapter == null) {
                    mAdapter = newAdapter(activity, list);
                    setListAdapter(mAdapter);
                } else {
                    mAdapter.changeList(list);
                }

                onListChanged(list);

                if (getView() != null)
                    setListShown(true);

                return true;
            }
            else
                return onError(response.getAnyError());
        }
        return false;
    }

    @Override
    protected void reloadList() {
        setRefreshing(true);
        if (mRefreshTask != null)
            mRefreshTask.abort();
        mRefreshTask = doRequest(getShopIdentity(), getFilter(), this, 0);
    }

    public SplitScheduleFilter getFilter() {
        return mFilter;
    }

    public void setFilter(SplitScheduleFilter filter) {
        mFilter = filter;
    }

    protected List<Event> toList(Schedule schedule) {
        List<Event> list = schedule.getList();
        Collections.sort(list, new Comparator<Event>() {
            @Override
            public int compare(Event lhs, Event rhs) {
                return (int) (lhs.getStart() - rhs.getStart());
            }
        });
        return list;
    }

    protected Task doRequest(Identity shopIdentity, SplitScheduleFilter filter, RequestCallback<Schedule> callback, int flags) {
        return HonkioApi.getSplitSchedule(filter, shopIdentity, flags, callback);
    }

    protected void onListChanged(List<Event> list) {

    }

    private Identity getShopIdentity() {
        return FragmentUtils.getShopIdentity(this, HonkioApi.getMainShopInfo().getShop());
    }
}
