package com.riskpointer.sdk.app.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.ChatMessage;
import com.riskpointer.sdk.api.model.entity.ChatMessages;
import com.riskpointer.sdk.api.model.entity.Order;
import com.riskpointer.sdk.api.model.push.Push;
import com.riskpointer.sdk.api.model.push.entity.OrderChatMessagePushEntity;
import com.riskpointer.sdk.api.model.task.RequestTask;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.support.BroadcastManager;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.ChatMessageFilter;
import com.riskpointer.sdk.api.web.message.filter.PushFilter;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.receivers.HonkioGcmBroadcastReceiver;
import com.riskpointer.sdk.app.ui.fragments.list.BaseRecycleLazyListFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shurygin Denis on 2016-09-01.
 */
public abstract class BaseOrderChatFragment extends BaseRecycleLazyListFragment<ChatMessage> {
    public static final String ARG_ORDER = "BaseChatFragment.ARG_ORDER";
    private static final String SAVED_FILTER = "BaseChatFragment.ARG_FILTER";

    private ChatMessageFilter mFilter;
    private Order mOrder;

    private List<ChatMessage> mPendingMessages;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        BroadcastManager.getInstance(getActivity()).registerReceiver(mPushReceiver,
                new IntentFilter(BroadcastHelper.BROADCAST_ACTION_ON_PUSH_RECEIVED));

        if (savedInstanceState != null)
            mFilter = (ChatMessageFilter) savedInstanceState.getSerializable(SAVED_FILTER);
        else {
            mFilter = new ChatMessageFilter();

            Order order = getOrder();
            if (order != null)
                mFilter.setOrderId(order.getOrderId());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BroadcastManager.getInstance(getActivity()).unregisterReceiver(mPushReceiver);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mFilter != null)
            outState.putSerializable(SAVED_FILTER, mFilter);
    }

    @Override
    public ListLoader<ChatMessage, ChatMessages> onCreateLoader(int id, Bundle args) {
        return new ChatLoader(getContext(), mFilter);
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager(Context context) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setReverseLayout(true);
        return layoutManager;
    }

    @Override
    public void onLoadFinished(Loader<ListLoaderResult<ChatMessage>> loader, ListLoaderResult<ChatMessage> data) {
        super.onLoadFinished(loader, data);
        if (data.list != null) {
            removeChatPushes();
        }

        if (mPendingMessages != null && mAdapter != null) {
            for (ChatMessage message: mPendingMessages) {
                mAdapter.insert(message, 0);
            }
            mPendingMessages = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        HonkioGcmBroadcastReceiver.addPushToIgnore(Push.Type.CHAT_MESSAGE,
                "null|" + mOrder.getOrderId());
    }

    @Override
    public void onPause() {
        super.onPause();
        HonkioGcmBroadcastReceiver.removePushFromIgnore(Push.Type.CHAT_MESSAGE,
                "null|" + mOrder.getOrderId());
    }

    public void setFilter(ChatMessageFilter filter) {
        mFilter = filter;
    }

    public ChatMessageFilter getFilter() {
        return mFilter;
    }

    public Order getOrder() {
        if (mOrder == null) {
            Bundle args = getArguments();
            if (args != null && args.containsKey(ARG_ORDER)) {
                mOrder = (Order) args.getSerializable(ARG_ORDER);
            }
        }
        return mOrder;
    }

    public void sendMessage(ChatMessage message) {
        message.setTime(System.currentTimeMillis());
        addMessageToList(message);
        HonkioApi.userSendChatMessages(message, 0, new RequestCallback<Void>() {
            @Override
            public boolean onComplete(Response<Void> response) {
                if (response.isStatusAccept()) {
                    // mark message as sent
                    return true;
                }
                return onError(response.getAnyError());
            }

            @Override
            public boolean onError(RpError error) {
                return BaseOrderChatFragment.this.onError(error);
            }
        });
    }

    public static class ChatLoader extends ListLoader<ChatMessage, ChatMessages> {
        private ChatMessageFilter mFilter;

        public ChatLoader(Context context, ChatMessageFilter filter) {
            super(context);
            mFilter = filter;
        }

        @Override
        protected List<ChatMessage> toList(Response<ChatMessages> response) {
            return response.getResult().getList();
        }

        public ChatMessageFilter getFilter() {
            return mFilter;
        }

        @Override
        protected Task loadList(Context context, RequestCallback<ChatMessages> callback, int count, int skip) {
            if (mFilter == null) {
                mFilter = new ChatMessageFilter();
            }
            mFilter.setCount(count);
            mFilter.setSkip(skip);
            return doRequest(mFilter, callback);
        }

        protected RequestTask<ChatMessages> doRequest(ChatMessageFilter filter, RequestCallback<ChatMessages> callback){
            return HonkioApi.userGetChatMessages(filter, 0, callback);
        }
    }

    protected void addMessageToList(ChatMessage message) {
        if (mAdapter != null) {
            mAdapter.insert(message, 0);
            getListView().scrollToPosition(0);
        }
        else {
            if (mPendingMessages == null)
                mPendingMessages = new ArrayList<>();
            mPendingMessages.add(message);
        }
    }

    protected void removeChatPushes() {
        Context context = getContext();
        if (context != null && mOrder != null && mOrder.getOrderId() != null) {
            HonkioApi.userSetPush(true,
                    new PushFilter()
                            .addContent(PushFilter.QUERY_CONTENT_ORDER, mOrder.getOrderId())
                            .addContent(PushFilter.QUERY_CONTENT_TYPE, Push.Type.CHAT_MESSAGE),
                    Message.FLAG_NO_WARNING, null);
        }
    }

    private BroadcastReceiver mPushReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent!= null && intent.hasExtra(BroadcastHelper.BROADCAST_EXTRA_PUSH)) {
                Push<?> push = (Push<?>) intent.getSerializableExtra(BroadcastHelper.BROADCAST_EXTRA_PUSH);
               if (push.getEntity() instanceof OrderChatMessagePushEntity) {
                   OrderChatMessagePushEntity pushEntity = (OrderChatMessagePushEntity) push.getEntity();
                   if (!pushEntity.getSenderId().equals(HonkioApi.getActiveUser().getUserId()) && mOrder.getOrderId().equals(pushEntity.getOrderId())) {
                       ChatMessage message = new ChatMessage();
                       message.setTime(pushEntity.getTime());
                       message.setSenderId(pushEntity.getSenderId());
                       message.setSenderRole(pushEntity.getSenderRole());
                       message.setReceiverId(pushEntity.getReceiverId());
                       message.setReceiverRole(pushEntity.getReceiverRole());
                       message.setOrderId(pushEntity.getOrderId());
                       message.setText(pushEntity.getText());
                       message.setOrderThirdPerson(pushEntity.getThirdPerson());
                       message.setSenderFullName(pushEntity.getSenderFirstName(),pushEntity.getSenderLastName());
                       message.setShopName(pushEntity.getShopName());
                       addMessageToList(message);
                   }
               }
            }
        }
    };
}
