package com.riskpointer.sdk.app.ui.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.google.zxing.WriterException;
import com.riskpointer.sdk.app.utils.BarcodeUtils;

/**
 * @author Shurygin Denis on 2015-08-20.
 */
public class BarcodeView extends ImageView {

    public interface GeneratorCallback {
        void onBarCodeGenerated(Bitmap bitmap);
    }

    private String mBarcodeString;
    private GeneratorCallback mGeneratorCallback;
    private AsyncBarcodeGenerator mAsyncBarcodeGenerator;
    private boolean isMeasured;

    public BarcodeView(Context context) {
        super(context);
    }

    public BarcodeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BarcodeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public BarcodeView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        isMeasured = true;

        String generatedCode = mAsyncBarcodeGenerator != null ?
                mAsyncBarcodeGenerator.mBarcodeString : null;
        if (mBarcodeString != null && !mBarcodeString.equals(generatedCode))
            generateBarcode();
    }

    public void setGeneratorCallback(GeneratorCallback callback) {
        mGeneratorCallback = callback;
    }

    public void setBarCodeString(String barCode) {
        mBarcodeString = barCode;
        if (isMeasured)
            generateBarcode();
    }

    private void generateBarcode() {
        if (mAsyncBarcodeGenerator != null)
            mAsyncBarcodeGenerator.cancel(true);

        if (mBarcodeString != null) {
            mAsyncBarcodeGenerator = new AsyncBarcodeGenerator(mBarcodeString, getMeasuredWidth(), getMeasuredHeight()) {

                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    setImageBitmap(bitmap);
                    if (mGeneratorCallback != null)
                        mGeneratorCallback.onBarCodeGenerated(bitmap);
                }
            };
            mAsyncBarcodeGenerator.execute();
        }
    }

    private class AsyncBarcodeGenerator extends AsyncTask<Void, Void, Bitmap> {

        private String mBarcodeString;
        private int mImgWidth;
        private int mImgHeight;

        public AsyncBarcodeGenerator(String barCode, int imgWidth, int imgHeight) {
            super();
            mBarcodeString = barCode;
            mImgWidth = imgWidth;
            mImgHeight = imgHeight;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                return BarcodeUtils.build(mBarcodeString, mImgWidth, mImgHeight);
            } catch (WriterException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
