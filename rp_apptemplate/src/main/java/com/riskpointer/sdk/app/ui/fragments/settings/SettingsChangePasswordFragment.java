/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.fragments.settings;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.web.callback.ErrorCallback;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.textvalidator.PasswordTextValidator;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;
import com.riskpointer.sdk.app.ui.activities.SingleFragmentActivity;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.ui.fragments.BaseFragment;
import com.riskpointer.sdk.app.utils.DoneClickListener;

public class SettingsChangePasswordFragment extends BaseFragment implements BaseDialogFragment.OnDismissListener, SingleFragmentActivity.SingleFragment {

    public static final String ARG_IGNORE_OLD = "SettingsChangePasswordFragment.ARG_IGNORE_OLD";

    private PasswordTextValidator mPasswordTextValidator;
    private EditText mOldPasswordText;
    private EditText mNewPasswordText;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mPasswordTextValidator = new PasswordTextValidator(activity.getResources());
    }

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_dialog_fragment_change_password;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupView(view);
        view.findViewById(R.id.hk_btnNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPositiveButtonClick();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getDefaultLayoutId(), container, false);
    }

    private void setupView(View view) {
        mOldPasswordText = (EditText) view.findViewById(R.id.hk_edPassword);
        mNewPasswordText = (EditText) view.findViewById(R.id.hk_edNewPassword);
        CheckBox mShowPasswordCheckBox = (CheckBox) view.findViewById(R.id.hk_cbShow);


        mNewPasswordText.setOnKeyListener(new DoneClickListener() {
            @Override
            public void onDoneClick(View view) {
                onPositiveButtonClick();
            }
        });

        mShowPasswordCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int inputType = 0;
                if (isChecked)
                    inputType = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD;
                else
                    inputType = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD;
                mOldPasswordText.setInputType(inputType);
                mOldPasswordText.setTypeface(Typeface.MONOSPACE);
                mNewPasswordText.setInputType(inputType);
                mNewPasswordText.setTypeface(Typeface.MONOSPACE);
            }
        });

        if (isOldPasswordIgnore()) {
            mOldPasswordText.setText("********");
            mOldPasswordText.setEnabled(false);
        }

    }

    public boolean isOldPasswordIgnore() {
        Bundle args = getArguments();
        return args != null && args.getBoolean(ARG_IGNORE_OLD, false);
    }

    private void changePassword(String oldPassword, String newPassword) {
        showProgressDialog();
        RequestCallback<User> callback = new RequestCallback<User>() {

            @Override
            public boolean onError(RpError errorCode) {
                dismissProgressDialog();
                if (isAdded()) {
                    if (errorCode.code == RpError.Common.INVALID_USER_LOGIN) {
                        showMessage(null,
                                getString(R.string.change_password_dlg_invalid_title),
                                getString(R.string.change_password_dlg_invalid_message));
                    } else {
                        if (!((ErrorCallback) getActivity()).onError(errorCode)) {
                            showMessage(null, getString(R.string.change_password_dlg_failed_title),
                                (errorCode.description != null
                                    ? String.format(
                                        getString(R.string.change_password_dlg_failed_message_format),
                                        getString(R.string.change_password_dlg_failed_message),
                                        errorCode.description)
                                    : ""));
                        }
                    }
                }
                return true;
            }

            @Override
            public boolean onComplete(Response<User> response) {
                dismissProgressDialog();
                if (response.isStatusAccept())
                        showMessage(BaseActivity.DLG_TAG_SUCCESS,
                                getString(R.string.change_password_dlg_accept_title),
                                getString(R.string.change_password_dlg_accept_message));
                else
                    return onError(response.getAnyError());
                return true;
            }
        };
        HonkioApi.userUpdatePassword(oldPassword, newPassword, 0, callback);
    }

    private void onPositiveButtonClick() {
        if (mPasswordTextValidator.validate(mOldPasswordText)
                && mPasswordTextValidator.validate(mNewPasswordText)) {
            String oldPassword = null;
            if (!isOldPasswordIgnore())
                oldPassword = mOldPasswordText.getText().toString();
            String newPassword = mNewPasswordText.getText().toString();
            changePassword(oldPassword, newPassword);
        }
    }

    @Override
    public String getScreenName() {
        if (isAdded()) {
            return getString(R.string.change_password_title);
        } else return "";
    }

    @Override
    public String getTitle(Context context) {
        return context.getString(R.string.change_password_title);
    }
}
