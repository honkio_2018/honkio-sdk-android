package com.riskpointer.sdk.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Asset;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;
import com.riskpointer.sdk.app.ui.strategy.ProgressContainerManager;

/**
 * Created by IAm on 19.01.2018.
 */
@Deprecated  //use BaseAssetDetailsFragment instead
public abstract class BaseAssetFragment extends BaseFragment {

    public static final String ARG_ASSET = "BaseAssetFragment.ARG_ASSET";

    public static final String SAVED_ASSET = "BaseAssetFragment.SAVED_ASSET";

    private static final String FLO_TAG_PROGRESS = "BaseAssetFragment.ProgressContainerManager";

    private ProgressContainerManager mProgressManager;
    private Asset mAsset;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mProgressManager = getLifeCycleObserver(FLO_TAG_PROGRESS);
        if (mProgressManager == null) {
            mProgressManager = new ProgressContainerManager(FLO_TAG_PROGRESS,
                    R.id.hk_progress, R.id.hk_content);
            addLifeCycleObserver(mProgressManager);
        }

        if (mAsset == null) {
            if (savedInstanceState == null) {
                Object assetObject = getArguments().get(ARG_ASSET);
                if (assetObject instanceof String) {
                    hideContent(false);
                    loadAssetById((String) assetObject);
                }
                else if (assetObject instanceof Asset) {
                    onAssetGet((Asset) assetObject);
                }
                else  {
                    throw new IllegalArgumentException("wrong type of ARG_ASSET");
                }
            }
            else {
                mAsset = (Asset) savedInstanceState.getSerializable(SAVED_ASSET);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (isAssetLoad()){
            showContent(false);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mAsset != null)
            outState.putSerializable(SAVED_ASSET, mAsset);
    }

    public Asset getAsset() {
        return mAsset;
    }

    protected void onAssetGet(Asset asset) {
        mAsset = asset;
        updateUi();
    }

    protected void updateUi() {

    }

    protected boolean isAssetLoad() {
        return mAsset != null;
    }

    protected void hideContent(boolean animation) {
        mProgressManager.hideContent(animation);
    }

    public void showContent(boolean animation) {
        mProgressManager.showContent(animation);
    }

    private void loadAssetById(String id) {
        hideContent(false);
        HonkioApi.userGetAsset(id, 0, new RequestCallback<Asset>() {
            @Override
            public boolean onComplete(Response<Asset> response) {
                if (response.isStatusAccept()) {
                    showContent(true);
                    onAssetGet(response.getResult());
                    return true;
                }
                return onError(response.getAnyError());
            }

            @Override
            public boolean onError(RpError error) {
                showErrorMessage(BaseActivity.DLG_TAG_FINISH, error);
                return true;
            }
        });
    }
}
