package com.riskpointer.sdk.app.ui.dialogs;

import android.app.Activity;
import android.support.v7.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.riskpointer.sdk.app.R;

/**
 * Created by Shurygin Denis on 2017-01-10.
 */
public class InputDialogFragment extends BaseDialogFragment {

    public static final String ARG_TEXT = "InputDialogFragment.ARG_TEXT";
    public static final String ARG_TEXT_HINT = "InputDialogFragment.ARG_TEXT_HINT";
    public static final String ARG_TITLE = "InputDialogFragment.ARG_TITLE";
    public static final String ARG_MESSAGE = "InputDialogFragment.ARG_MESSAGE";
    public static final String ARG_INPUT_TYPE = "InputDialogFragment.ARG_INPUT_TYPE";
    public static final String ARG_SINGLE_LINE = "InputDialogFragment.ARG_SINGLE_LINE";
    public static final String ARG_SHOW_KEYBOARD = "InputDialogFragment.ARG_SHOW_KEYBOARD";

    private EditText mEditText;

// =================================================================================================
// Public static
// =================================================================================================

    public static InputDialogFragment build(String text, int textHint, int title, int message,
                             int inputType, boolean singleLine,  boolean showKeyboard) {
        Bundle args = new Bundle();

        args.putString(ARG_TEXT, text);
        args.putInt(ARG_TEXT_HINT, textHint);
        args.putInt(ARG_TITLE, title);
        args.putInt(ARG_MESSAGE, message);
        args.putInt(ARG_INPUT_TYPE, inputType);
        args.putBoolean(ARG_SINGLE_LINE, singleLine);
        args.putBoolean(ARG_SHOW_KEYBOARD, showKeyboard);

        InputDialogFragment fragment = new InputDialogFragment();
        fragment.setArguments(args);

        return fragment;
    }

// =================================================================================================
// Methods of Super Class
// =================================================================================================

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.hk_dialog_fragment_input, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(view);
        builder.setTitle(getTitleString());
        builder.setPositiveButton(android.R.string.ok, this);

        mEditText = (EditText) view.findViewById(R.id.hk_edText);
        TextView txtMessage = (TextView) view.findViewById(R.id.hk_txtMessage);

        String testString = getTextString();
        if (testString != null)
            mEditText.setText(testString);
        mEditText.setHint(getTextHintString());

        applyInputType(mEditText);

        mEditText.setSingleLine(isSingleLine());

        String messageString = getMessageString();
        if (messageString != null)
            txtMessage.setText(messageString);
        else
            txtMessage.setVisibility(View.GONE);

        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP)
                    onClick(dialog, DialogInterface.BUTTON_NEUTRAL);
                return false;
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        if (isShowKeyboard()) {
            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Activity activity = getActivity();
                    if (activity != null) {
                        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (imm != null && mEditText != null) {
                            imm.showSoftInput(mEditText, 0);
                        }
                    }
                }
            });
        }

        return dialog;
    }

// =================================================================================================
// Public methods
// =================================================================================================

    public String getText() {
        if (mEditText != null)
            return mEditText.getText().toString();
        return null;
    }

    protected void setText(String text){
        mEditText.setText(text);
    }

// =================================================================================================
// Protected methods and classes
// =================================================================================================

    protected String getTextString() {
        return getArgsString(getArguments(), ARG_TEXT);
    }

    protected String getTextHintString() {
        return getArgsString(getArguments(), ARG_TEXT_HINT);
    }

    protected String getTitleString() {
        return getArgsString(getArguments(), ARG_TITLE);
    }

    protected String getMessageString() {
        return getArgsString(getArguments(), ARG_MESSAGE);
    }

    protected void applyInputType(EditText editText) {
        Bundle args = getArguments();
        if (args != null && args.containsKey(ARG_INPUT_TYPE)) {
            int inputType = args.getInt(ARG_INPUT_TYPE, EditorInfo.TYPE_TEXT_VARIATION_PERSON_NAME);
            editText.setInputType(inputType);
        }
    }

    protected boolean isShowKeyboard() {
        Bundle args = getArguments();
        return args != null && args.getBoolean(ARG_SHOW_KEYBOARD, false);
    }

    protected boolean isSingleLine() {
        Bundle args = getArguments();
        return args != null && args.getBoolean(ARG_SINGLE_LINE, true);
    }

}
