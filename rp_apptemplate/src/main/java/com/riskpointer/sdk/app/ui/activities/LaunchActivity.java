/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.fragments.LoginFragment;

public class LaunchActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = AppController.getIntent(AppController.ACTIVITY_SPLASH);

        if (Intent.ACTION_VIEW.equals(getIntent().getAction()) && getIntent().getData() != null) {
            intent.putExtra(LoginFragment.EXTRA_ACTION_COMMAND, LoginFragment.COMMAND_URI);
            intent.putExtra(LoginFragment.EXTRA_COMMAND_PAYLOAD, getIntent().getData());
        }

        startActivity(intent);
		overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }
}
