/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.model.entity.UserAccount;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AppPreferences {
	public static final String PREFERENCES_FILE = "HonkioPreferences";

	private static final String PREFERRED_ACCOUNTS = "PREFERRED_ACCOUNTS";

	private static final String DEFAULT_USER_ACCOUNT_TYPE = "DefUserAccountType";
	private static final String DEFAULT_USER_ACCOUNT_NUMBER = "DefUserAccountNumber";
	private static final String USE_MERCHANT_INVOICE = "UseMerchantInvoice";

    public static final String PREF_SAVE_PIN = "savePin";
    public static final String PREF_PIN = "pin";
    public static final boolean PREF_SAVE_PIN_DEFAULT = true;
	
	private Context mContext;
	public final SharedPreferences pref;

	private List<UserAccount> mPreferredAccounts;
	
	public AppPreferences(Context context) {
		mContext = context;
		mPreferredAccounts = new ArrayList<>();
		pref = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);

		String preferredAccountsString = pref.getString(PREFERRED_ACCOUNTS, null);
		if (preferredAccountsString != null && HonkioApi.getActiveUser() != null) {
			try {
				JSONArray jsonArray = new JSONArray(preferredAccountsString);
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject json = jsonArray.optJSONObject(i);
					if (json != null) {
						String type = json.optString("type");
						String number = json.optString("number");

						if (type != null && number != null) {
							UserAccount account = HonkioApi.getActiveUser().getAccount(type, number);
							if (account == null && UserAccount.Type.OPERATOR.equals(type))
								account = new UserAccount(type, number);

							if (account != null)
								mPreferredAccounts.add(account);
						}
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	public List<UserAccount> getPreferredAccounts() {
		return mPreferredAccounts;
	}

	public void addPreferredAccount(UserAccount account) {
		addPreferredAccount(account.getType(), account.getNumber());
	}

	public void addPreferredAccount(String type, String number) {
		if (type == null || number == null)
			return;

		if (isPreferredAccount(type, number))
			return;

		UserAccount account = HonkioApi.getActiveUser().getAccount(type, number);
		if (account == null)
			return;

		mPreferredAccounts.add(account);
		savePreferredAccounts();
	}

	public void removePreferredAccount(UserAccount account) {
		removePreferredAccount(account.getType(), account.getNumber());
	}

	public void removePreferredAccount(String type, String number) {
		if (type == null || number == null)
			return;

		for (int i = 0; i < mPreferredAccounts.size(); i++) {
			UserAccount account = mPreferredAccounts.get(i);
			if (account.typeIs(type) && number.equals(account.getNumber())) {
				mPreferredAccounts.remove(i);
				savePreferredAccounts();
				return;
			}
		}
	}

	public boolean isPreferredAccount(UserAccount account) {
		return isPreferredAccount(account.getType(), account.getNumber());
	}

	public boolean isPreferredAccount(String type, String number) {
		if (type == null || number == null)
			return false;

		for (UserAccount account: mPreferredAccounts)
			if (account.typeIs(type) && number.equals(account.getNumber()))
				return true;

		return false;
	}

	private void savePreferredAccounts() {
		final List<UserAccount> list = new ArrayList<>(mPreferredAccounts);
		new Thread() {
			@Override
			public void run() {
				JSONArray jsonArray = new JSONArray();

				for(UserAccount account: list) {
					JSONObject json = new JSONObject();
					try {
						json.put("type", account.getType());
						json.put("number", account.getNumber());
						jsonArray.put(json);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

				pref.edit().putString(PREFERRED_ACCOUNTS, jsonArray.toString()).apply();
			}
		}.start();
	}
// =================================================================================================
// ========  DEPRECATED  ===========================================================================
// =================================================================================================

	@Deprecated
	public UserAccount getDefUserAccount() {
		String type = pref.getString(DEFAULT_USER_ACCOUNT_TYPE, null);
		String number = pref.getString(DEFAULT_USER_ACCOUNT_NUMBER, null);
		User user = HonkioApi.getActiveUser();
		if(user != null) {

			if (isUseMerchantInvoice() && HonkioApi.isInitialised()) {
				UserAccount account = user.getAccount(UserAccount.Type.INVOICE, HonkioApi.getMainShopInfo().getMerchant().getId());
				if (account != null)
					return account;
			}

			UserAccount account = user.getAccount(type, number);
			if (account != null && account.isHidden())
				account = null;
			if (account == null) {
				for (UserAccount firstAccount : user.getAccountsList()) {
					if (!firstAccount.isHidden())
						return firstAccount;
				}
			}
			return account;
		}

		return null;
	}

	@Deprecated
	public boolean isUseMerchantInvoice() {
		return pref.getBoolean(USE_MERCHANT_INVOICE, true);
	}

	@Deprecated
	private void setUseMerchantInvoice(boolean useMerchantInvoice) {
		pref.edit().putBoolean(USE_MERCHANT_INVOICE, useMerchantInvoice)
				.apply();
	}
}
