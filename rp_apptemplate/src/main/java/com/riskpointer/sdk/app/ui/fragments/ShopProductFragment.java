/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.riskpointer.sdk.api.model.entity.Product;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.utils.Currency;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.utils.FormatUtils;
import com.riskpointer.sdk.app.utils.FragmentUtils;
import com.riskpointer.sdk.app.utils.ImageUtils;

public class ShopProductFragment extends BaseFragment {
	
	private TextView mTextName;
	private TextView mTextCount;
	private TextView mTextCurrency;
	private TextView mTextVatCount;
	private TextView mTextVatCurrency;
	private TextView mTextDesc;
	private ImageView mImgLogo;
	private View mLogoProgress;

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_product_detail;
    }

	@Override
	public String getTitle(Context context) {
		return context.getString(R.string.product_details_title);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);

		mTextName = (TextView) view.findViewById(R.id.hk_txtName);
		mTextCount = (TextView) view.findViewById(R.id.hk_txtCount);
		mTextCurrency = (TextView) view.findViewById(R.id.hk_txtCurrency);
		mTextVatCount = (TextView) view.findViewById(R.id.hk_txtVatCount);
		mTextVatCurrency = (TextView) view.findViewById(R.id.hk_txtVatCurrency);
		mTextDesc = (TextView) view.findViewById(R.id.hk_txtDesc);
		mImgLogo = (ImageView) view.findViewById(R.id.hk_imgProductLargeLogo);
		mLogoProgress = view.findViewById(R.id.hk_progressContainer);
		
		return view;
	}
	
	@Override
	public void onResume() {
		updateView();
		super.onResume();
	}

	@Override
	public void onDetach() {
		ImageUtils.stopLoadWithPicasso(getActivity(), mImgLogo);
		super.onDetach();
	}

	private void updateView() {
		Shop shop = FragmentUtils.getAnyShop(this);
		Product product = FragmentUtils.getProduct(this);
		String currency = Currency.getSymbol(product.getCurrency());

		if (mTextName != null)
			mTextName.setText(product.getName());
		mTextCount.setText(FormatUtils.format(product.getAmount()));
		mTextCurrency.setText(currency);
		mTextVatCount.setText(FormatUtils.format(product.getAmountVat()));
		mTextVatCurrency.setText(currency);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
			mTextDesc.setText(Html.fromHtml(product.getDescription(), Html.FROM_HTML_MODE_COMPACT));
		else
			mTextDesc.setText(Html.fromHtml(product.getDescription()));

		if (product.getPicsList() == null || product.getPicsList().size() == 0) {
			ImageUtils.loadImage(getActivity(),
					mImgLogo, mLogoProgress, shop.getLogoLarge(),
					getDummyLogo(), true, null);
		}
		else {
			ImageUtils.loadImage(getActivity(),
					mImgLogo, mLogoProgress, product.getPicsList().get(0).url,
					getDummyLogo(), true, null);
		}
	}

	protected int getDummyLogo() {
		return 0;
	}
}
