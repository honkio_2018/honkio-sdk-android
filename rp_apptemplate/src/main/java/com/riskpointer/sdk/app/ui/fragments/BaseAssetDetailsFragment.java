package com.riskpointer.sdk.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Asset;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.FragmentComponents;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;
import com.riskpointer.sdk.app.ui.strategy.ProgressContainerManager;
import com.riskpointer.sdk.app.utils.FragmentUtils;

/**
 * Created by I.N. on 11.12.2018
 */

public abstract class BaseAssetDetailsFragment extends BaseFragment {
    public static final String ARG_ASSET = "BaseAssetDetailsFragment.ARG_ASSET";
    public static final String ARG_SEARCH_IN_SCHEMA = "BaseAssetDetailsFragment.ARG_SEARCH_IN_SCHEMA";
    public static final String SAVED_ASSET = "BaseAssetDetailsFragment.SAVED_ASSET";
    private static final String FLO_TAG_PROGRESS = "BaseAssetDetailsFragment.ProgressContainerManager";
    private ProgressContainerManager mProgressManager;
    private Asset mAsset;
    private Fragment mContentFragment;

    protected abstract FragmentComponents buildContentFragmentComponents(Asset asset);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProgressManager = getLifeCycleObserver(FLO_TAG_PROGRESS);
        if (mProgressManager == null) {
            mProgressManager = new ProgressContainerManager(FLO_TAG_PROGRESS,
                    R.id.hk_progress, R.id.hk_content);
            addLifeCycleObserver(mProgressManager);
        }
        if (savedInstanceState != null)
            mAsset = (Asset) savedInstanceState.getSerializable(SAVED_ASSET);

        if (mAsset == null) {
            if (savedInstanceState == null) {
                Object assetObject = getArguments().get(ARG_ASSET);
                if (assetObject instanceof String) {
                    hideContent(false);
                    loadAssetById((String) assetObject);
                } else if (assetObject instanceof Asset) {
                    mAsset = (Asset) assetObject;
                } else {
                    throw new IllegalArgumentException("wrong type of ARG_ASSET");
                }
            } else {
                mAsset = (Asset) savedInstanceState.getSerializable(SAVED_ASSET);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isAssetLoad()) {
            showContent(false);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mAsset != null) {
            updateContent(mAsset);
            mProgressManager.showContent(false);
        } else
            mProgressManager.hideContent(false);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mAsset != null)
            outState.putSerializable(SAVED_ASSET, mAsset);
    }

    public Asset getAsset() {
        return mAsset;
    }

    public void setAsset(Asset asset) {
        mAsset = asset;
    }

    protected boolean isAssetLoad() {
        return mAsset != null;
    }

    protected void hideContent(boolean animation) {
        mProgressManager.hideContent(animation);
    }

    public void showContent(boolean animation) {
        mProgressManager.showContent(animation);
    }

    private void loadAssetById(String id) {
        hideContent(false);
        HonkioApi.userGetAsset(id, FragmentUtils.getShopIdentity(this, null), getSchemaParameter(), 0, new RequestCallback<Asset>() {
            @Override
            public boolean onComplete(Response<Asset> response) {
                if (response.isStatusAccept()) {
                    updateContent(response.getResult());
                    showContent(true);
                    return true;
                }
                return onError(response.getAnyError());
            }

            @Override
            public boolean onError(RpError error) {
                showErrorMessage(BaseActivity.DLG_TAG_FINISH, error);
                return true;
            }
        });
    }

    private boolean getSchemaParameter() {
        if (getArguments() != null && getArguments().containsKey(ARG_SEARCH_IN_SCHEMA)) {
            return (boolean) getArguments().get(ARG_SEARCH_IN_SCHEMA);
        } else {
            return false;
        }
    }

    protected void updateUI(Asset asset) {
        //update UI of this container fragment
    }

    private void updateContent(Asset asset) {
        updateUI(asset);
        if (isAdded() && getFragmentManager().findFragmentById(R.id.hk_content) == null) {
            FragmentComponents fragmentComponents = buildContentFragmentComponents(asset);
            /*if (fragmentComponents == null)
                fragmentComponents = new FragmentComponents(
                        ShopDetailsUnsupportedFragment.class, null);*/
            mContentFragment = fragmentComponents.newFragment();
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.hk_content, mContentFragment)
                    .commit();
        }
    }

    public Fragment getContentFragment() {
        return mContentFragment;
    }
}
