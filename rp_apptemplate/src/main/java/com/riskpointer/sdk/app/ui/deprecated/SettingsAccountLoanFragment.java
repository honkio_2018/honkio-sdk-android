package com.riskpointer.sdk.app.ui.deprecated;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.ServerFile;
import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.fragments.BaseFragment;

import java.util.HashMap;

/**
 * @author Shurygin Denis on 2015-04-08.
 */
@Deprecated
public class SettingsAccountLoanFragment extends BaseFragment implements RequestCallback<User> {
    public static final String EXTRA_USER_FILE = "UserFile";
    public static final String EXTRA_LICENSE_KEY = "LicenseKey";

    private String mLicenseDictKey;

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_settings_account_loan;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLicenseDictKey = getArguments().getString(EXTRA_LICENSE_KEY);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final View buttonOk = view.findViewById(R.id.hk_btnOk);
        buttonOk.setEnabled(false);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog();
                if (isAdded()) {
                    HashMap<String, String> dict = new HashMap<>(1);
                    dict.put(mLicenseDictKey, User.Dict.TIMESTAMP_TEMPLATE);
                    HonkioApi.userUpdateDictValues(dict, 0, SettingsAccountLoanFragment.this);
                }
            }
        });

        View btnCancel = view.findViewById(R.id.hk_btnCancel);
        if (btnCancel != null)
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onCancelClick(view);
                }
            });

        CheckBox checkBoxAgree = (CheckBox) view.findViewById(R.id.hk_cbAgree);

        checkBoxAgree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                buttonOk.setEnabled(isChecked);
            }
        });

        ServerFile userFile = (ServerFile) getArguments().getSerializable(EXTRA_USER_FILE);
        WebView webView = (WebView) view.findViewById(R.id.hk_webView);
        webView.setBackgroundColor(0);
        webView.loadDataWithBaseURL("", new String(userFile.getData()), userFile.getMime(), "UTF-8", "");
    }

    @Override
    public boolean onBackPressed() {
        super.onBackPressed();
        onCancelClick(null);
        return true;
    }

    public void onCancelClick(View view) {
        finish(Activity.RESULT_CANCELED);
    }

    @Override
    public boolean onError(RpError error) {
        dismissProgressDialog();
        if(!super.onError(error))
            showMessage(null, R.string.loan_dlg_error_title, R.string.loan_dlg_error_message);
        return true;
    }

    @Override
    public boolean onComplete(Response<User> response) {
        dismissProgressDialog();
        if (isAdded()) {
            if (response.isStatusAccept() && response.getResult().getDictValue(mLicenseDictKey) != null) {
                finish(Activity.RESULT_OK);
            } else {
                return onError(response.getAnyError());
            }
        }
        return true;
    }
}
