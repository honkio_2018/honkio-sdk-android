package com.riskpointer.sdk.app.ui.adapters.binder;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.riskpointer.sdk.api.model.entity.BoughtProduct;
import com.riskpointer.sdk.app.R;

/**
 * Created by Shurygin Denis on 2016-11-28.
 */

public abstract class AbsBoughtProductListBinder
        <ListItem extends BoughtProduct, Holder extends AbsBoughtProductListBinder.ViewHolder>
        extends AbsShopProductListBinder<ListItem, Holder> {

    @Override
    public void bind(Context context, Holder holder, ListItem product) {
        super.bind(context, holder, product);

        if (holder.count != null)
            holder.count.setText(Integer.toString(product.getCount()));
    }

    public static class ViewHolder extends ShopProductListBinder.ViewHolder {
        public TextView count;

        public ViewHolder(View view) {
            super(view);
            count = (TextView) view.findViewById(R.id.hk_txtCount);
        }
    }

}
