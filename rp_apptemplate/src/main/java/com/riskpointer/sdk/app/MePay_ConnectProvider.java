/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Messenger;

import java.net.MalformedURLException;
import java.net.URL;

@Deprecated
public class MePay_ConnectProvider {
	public static final String PACKAGE_NAME = "com.riskpointer.mepay";

	public static final String ACTION_HANDLE_QR_STRING = "com.riskpointer.mepay.action.HANDLE_QR_STRING";
	public static final String EXTRA_QR_STRING = "com.riskpointer.mepay.intent.extra.QR_STRING";

	public static final String URI_AUTORITY = "api.bbbs.net";
	public static final String URI_QUERY_SHOP = "shop";
	
	/** 
	 * Check that app is installed on the phone.
	 * 
	 * @param context A context.
	 * @return true if app is installed on the phone, false otherwise.
	 */
	public static boolean isAppInstalled(Context context) {
		PackageManager pm = context.getPackageManager();
	    try {
	        pm.getPackageInfo(PACKAGE_NAME, PackageManager.GET_ACTIVITIES);
	        return true; 
	    } catch (NameNotFoundException e) {
	        return false; 
	    }
	}

	/** 
	 * Check that code is code of mePay app. <b>FOR DEMO VERSION!!!</b>
	 * 
	 * @param qrString String readed from QR-code.
	 * @return true if qrCodeString is string of the mePay QR-code, false otherwise.
	 */
	public static boolean isMePayCode(String qrString) {
		Uri uri = null;
		try {
			URL url = new URL(qrString);
			Uri.Builder builder =  new Uri.Builder()
            .scheme(url.getProtocol())
            .authority(url.getAuthority())
            .appendPath(url.getPath())
            .encodedQuery(url.getQuery());
			uri = builder.build();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		if(uri == null)
			return false;
		
		String shopId = uri.getQueryParameter(URI_QUERY_SHOP);
		if(URI_AUTORITY.equals(uri.getAuthority()) && shopId != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * Start mePay app and handle QR-code string.
	 * 
	 * @param context A context.
	 * @param qrString String readed from QR-code.
	 * @return true if mePay app is started, false otherwise.
	 */
	public static boolean startApp(Context context, String qrString) {
		//TODO implementation
		return false;
	}

	/**
	 * Check that code is code of mePay app. <b>FOR RELEASE. Will be implemented later.</b>
	 * 
	 * @param context A context.
	 * @param messenger Messenger for receiving messages from mePay app.
	 * @param qrString String readed from QR-code.
	 */
	public static void isMePayCode(Context context, Messenger messenger, String qrString) {
		//TODO implementation
	}
}