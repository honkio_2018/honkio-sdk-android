/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.utils;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.riskpointer.sdk.api.BaseHonkioApi;
import com.riskpointer.sdk.api.model.entity.ServerInfo;
import com.riskpointer.sdk.api.model.entity.Shop.ServiceType;
import com.riskpointer.sdk.app.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Transformation;

import java.net.MalformedURLException;
import java.net.URL;

public class ImageUtils {

    public final static Transformation USER_PICTURE_TRANSFORMATION
            = new RoundedTransformationBuilder().oval(true).build();

	public static int getImageResourceIdForService(String serviceType) {
		if(ServiceType.CAR_WASH.equals(serviceType))
			return R.drawable.hk_ic_wash;
		if(ServiceType.GAS_STATION.equals(serviceType))
			return R.drawable.hk_ic_gas;
		if(ServiceType.VENDING.equals(serviceType))
			return R.drawable.hk_ic_vending;
		return R.drawable.hk_ic_all_services;
	}

    public static void loadImage(Context context, ImageView imageView, View progressView, String url, int defaultImage) {
        loadImage(context, imageView, progressView, url, defaultImage, null);
    }

    public static void loadImage(Context context, final ImageView imageView, final View progressView, String url, int defaultImage, final Callback callback) {
        loadImage(context, imageView, progressView, url, defaultImage, false, callback);
    }

    public static void loadImage(Context context, final ImageView imageView,
                                 final View progressView, String url, int defaultImage,
                                 boolean cropToImageView, final Callback callback) {
        Uri uri = getAssetsUri(url);
        if(uri != null) {
            RequestCreator requestCreator = Picasso.with(context).load(uri);
            if (defaultImage != 0)
                requestCreator.error(defaultImage);

            showProgress(imageView, progressView);

            if (cropToImageView) {
                requestCreator = requestCreator
                        .fit()
                        .centerCrop();
            }

            requestCreator.into(imageView, new Callback() {
                @Override
                public void onSuccess() {
                    showImage(imageView, progressView);
                    if (callback != null)
                        callback.onSuccess();
                }

                @Override
                public void onError() {
                    showImage(imageView, progressView);
                    if (callback != null)
                        callback.onError();
                }
            });
        }
        else {
            if (defaultImage != 0) {
                imageView.setImageResource(defaultImage);
                showImage(imageView, progressView);
            }
            else {
                imageView.setVisibility(View.GONE);
                progressView.setVisibility(View.GONE);
            }
        }
    }

    public static void stopLoadWithPicasso(Context context, ImageView imageView) {
        Picasso.with(context).cancelRequest(imageView);
    }

    public static Uri getAssetsUri(String urlString) {
        if (urlString == null || TextUtils.isEmpty(urlString))
            return null;

        URL url = null;
        try {
            url = new URL(urlString);
        } catch (MalformedURLException e) {
            // Do nothing
        }
        if (url == null) {
            ServerInfo serverInfo = BaseHonkioApi.getServerInfo();
            if (serverInfo != null) {
                String assetsUrlString = serverInfo.getAssetsUrl();
                if (assetsUrlString != null) {
                    StringBuilder urlStringBuilder = new StringBuilder(assetsUrlString);
                    if (urlStringBuilder.charAt(urlStringBuilder.length() - 1) == '/') {
                        if (urlString.charAt(0) == '/')
                            urlStringBuilder.deleteCharAt(urlStringBuilder.length() - 1);
                    }
                    else if (urlString.charAt(0) != '/')
                        urlStringBuilder.insert(0, '/');

                    urlStringBuilder.append(urlString);
                    try {
                        url = new URL(urlStringBuilder.toString());
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if (url != null)
            return Uri.parse(url.toString());
        return null;
    }
	
	private static void showProgress(ImageView imageView, View progressView) {
		imageView.setImageDrawable(null);
        if (progressView != null)
		    progressView.setVisibility(View.VISIBLE);
	}
	
	private static void showImage(ImageView imageView, View progressView) {
		imageView.setVisibility(View.VISIBLE);
        if (progressView != null)
		    progressView.setVisibility(View.GONE);
	}
}
