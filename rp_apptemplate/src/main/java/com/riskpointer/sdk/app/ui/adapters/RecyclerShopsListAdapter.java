package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.app.ui.adapters.binder.BaseShopsListBinder;

import java.util.List;

/**
 * @author Shurygin Denis
 */

public class RecyclerShopsListAdapter<VH extends RecyclerView.ViewHolder> extends BaseRecyclerListAdapter<Shop, VH> {

    private BaseShopsListBinder<Shop, VH> mBinder;

    public RecyclerShopsListAdapter(Context context, List<Shop> list, int layout, BaseShopsListBinder<Shop, VH> binder) {
        super(context, list, layout);
        mBinder = binder;
    }

    @Override
    public VH onCreateItemViewHolder(ViewGroup parent, View view, int viewType) {
        return mBinder.newHolder(view);
    }

    @Override
    public void onBindItemViewHolder(VH holder, int position) {
        mBinder.bind(mContext, holder, getItem(position));
    }

    public void setLocation(Location location) {
        mBinder.setLocation(location);
    }

    public Location getLocation() {
        return mBinder.getLocation();
    }

    public void setServiceTypeVisible(boolean visible) {
        mBinder.setServiceTypeVisible(visible);
    }
}
