package com.riskpointer.sdk.app.ui.adapters.binder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Shurygin Denis on 2016-05-26.
 */
public interface ListBinder<ListItem, ViewHolder extends RecyclerView.ViewHolder>
        extends SimpleListBinder<ListItem, ViewHolder> {

    ViewHolder newHolder(View view);

}
