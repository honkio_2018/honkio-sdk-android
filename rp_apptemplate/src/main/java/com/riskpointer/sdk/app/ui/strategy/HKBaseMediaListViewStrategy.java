package com.riskpointer.sdk.app.ui.strategy;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.MediaFile;
import com.riskpointer.sdk.api.model.entity.MediaFileList;
import com.riskpointer.sdk.api.model.entity.MediaUrl;
import com.riskpointer.sdk.api.web.callback.ErrorCallback;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.callback.SimpleCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.filter.MediaFileFilter;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.adapters.BaseRecyclerListAdapter;
import com.riskpointer.sdk.app.ui.adapters.BaseRecyclerListAdapter.OnItemClickListener;
import com.riskpointer.sdk.app.ui.adapters.binder.HKMediaFilesListBinder;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import okhttp3.MediaType;

/**
 * @author Shurygin Denis
 */
public abstract class HKBaseMediaListViewStrategy extends BaseViewLifeCycleObserver
        implements MediaPickerViewStrategy.Delegate {

    public interface ImageFilePreProcessor extends Serializable {

        interface Callback {
            void onComplete(File originalFile, ByteArrayInputStream processedStream, MediaType mediaType);
        }

        void process(File file, Callback callback);

    }

    public static final String TAG = "HKBaseMediaListViewStrategy.TAG";

    private static final String LCO_TAG_PROGRESS
            = "HKBaseMediaListViewStrategy.LCO_TAG_PROGRESS";
    private static final String LCO_TAG_PICTURE_PICKER
            = "HKBaseMediaListViewStrategy.LCO_TAG_PICTURE_PICKER";


    private static final String SAVED_OBJECT_ID
            = "HKAssetMediaListViewStrategy.SAVED_DEFERRED_LIST";
    private static final String SAVED_OBJECT_TYPE
            = "HKAssetMediaListViewStrategy.SAVED_DEFERRED_LIST";

    private static final String SAVED_DEFERRED_LIST
            = "HKAssetMediaListViewStrategy.SAVED_DEFERRED_LIST";
    private static final String SAVED_IMAGE_PRE_PROCESSOR
            = "HKAssetMediaListViewStrategy.SAVED_IMAGE_PRE_PROCESSOR";
    private static final String SAVED_IS_READ_ONLY
            = "HKAssetMediaListViewStrategy.SAVED_IS_READ_ONLY";

    private String mObjectId;
    private String mObjectType;

    private RecyclerView mRecyclerView;
    private Adapter<?> mAdapter;

    private ArrayList<MediaFile> mMediaList;

    private boolean isReadOnly = false;
    private boolean isDeferredList = false;

    private LinkedList<MediaPickerViewStrategy.PickedFile> mDeferredMediaFiles = new LinkedList<>();

    private ProgressContainerManager mProgressManager;

    private ImageFilePreProcessor mImageFilePreProcessor;

    protected abstract int getListViewId();
    protected abstract int getProgressViewId();
    protected abstract int getListItemViewId();
    protected abstract int getListAddViewId();

    protected abstract HKMediaFilesListBinder<? extends RecyclerView.ViewHolder> geyListBinder();

    protected abstract void inflateContextMenu(Menu menu, MenuInflater menuInflater);

    public HKBaseMediaListViewStrategy(String tag) {
        super(tag);
    }

    @Override
    public void onViewCreated(ViewController viewController, Bundle savedInstanceState) {
        mProgressManager = viewController.getLifeCycleObserver(LCO_TAG_PROGRESS);
        if (mProgressManager == null) {
            mProgressManager = new ProgressContainerManager(
                    LCO_TAG_PROGRESS,
                    getProgressViewId(),
                    getListViewId()
            );
            viewController.addLifeCycleObserver(mProgressManager);
            mProgressManager.onViewCreated(viewController, savedInstanceState);
        }

        super.onViewCreated(viewController, savedInstanceState);

        mRecyclerView = viewController.findViewById(getListViewId());
        if (mRecyclerView != null) {
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setHorizontalScrollBarEnabled(true);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(
                    getContext(), LinearLayoutManager.HORIZONTAL, false)
            );
            new PagerSnapHelper().attachToRecyclerView(mRecyclerView);

            mAdapter = new Adapter<>(getContext(), mMediaList, getListItemViewId(), geyListBinder());
            mAdapter.setEditable(isEditable());
            mAdapter.addInnerClickListener(new OnItemClickListener<MediaFile, RecyclerView.ViewHolder>() {
                @Override
                public void onItemClick(RecyclerView.ViewHolder itemHolder, View view, MediaFile mediaFile) {
                    showMediaFile(mediaFile);
                }
            });
            mAdapter.addInnerClickListener(new OnItemClickListener<MediaFile, RecyclerView.ViewHolder>() {
                @Override
                public void onItemClick(RecyclerView.ViewHolder itemHolder, View view, MediaFile mediaFile) {
                    showMediaFileMenu(mediaFile, view);
                }
            }, R.id.hk_btnMenu);

            mRecyclerView.setAdapter(mAdapter);

            addFooterIfNeeded();

            if (mMediaList != null)
                mProgressManager.showContent(false);
            else
                mProgressManager.hideContent(false);
        }
    }

    @Override
    public void saveState(ViewController viewController, Bundle outState) {
        super.saveState(viewController, outState);

        outState.putBoolean(SAVED_IS_READ_ONLY, isReadOnly);
        outState.putSerializable(SAVED_DEFERRED_LIST, mDeferredMediaFiles);
        outState.putSerializable(SAVED_IMAGE_PRE_PROCESSOR, mImageFilePreProcessor);

        outState.putString(SAVED_OBJECT_ID, mObjectId);
        outState.putString(SAVED_OBJECT_TYPE, mObjectType);
    }

    @Override
    public void restoreState(ViewController viewController, Bundle savedInstanceState) {
        super.restoreState(viewController, savedInstanceState);

        isReadOnly = savedInstanceState.getBoolean(SAVED_IS_READ_ONLY, isReadOnly);

        mImageFilePreProcessor = (ImageFilePreProcessor)
                savedInstanceState.getSerializable(SAVED_IMAGE_PRE_PROCESSOR);

        if (mDeferredMediaFiles.size() == 0)
            mDeferredMediaFiles = (LinkedList<MediaPickerViewStrategy.PickedFile>)
                    savedInstanceState.getSerializable(SAVED_DEFERRED_LIST);

        String objectId = savedInstanceState.getString(SAVED_OBJECT_ID);
        String objectType = savedInstanceState.getString(SAVED_OBJECT_TYPE);

        if (objectType != null) {
            setObject(objectId, objectType);
        }
    }


    @Override
    public void onFilePicked(MediaPickerViewStrategy.PickedFile pickedFile, int request) {
        if (isDeferredList) {
            mDeferredMediaFiles.add(pickedFile);
            if (mMediaList == null)
                mMediaList = new ArrayList<>();
            mMediaList.add(new MediaFile(null, pickedFile.file.toURI().toASCIIString()));

            if (mAdapter != null)
                mAdapter.notifyDataSetChanged();
        }
        else {
            showProgressDialog();
            createMediaUrl(mObjectId, mObjectType, pickedFile, new SimpleCallback() {
                @Override
                public void onComplete() {
                    loadMediaList(mObjectId);
                }

                @Override
                public boolean onError(RpError error) {
                    dismissProgressDialog();
                    if (!onParentError(error))
                        showErrorMessage(null, error);
                    return true;
                }
            });
        }
    }

    @Override
    public void onFileProcessing(int request) {
        showProgressDialog();
    }

    @Override
    public void onFilePickError(int request) {
        dismissProgressDialog();
        // TODO show error
    }

    public void setObject(String objectId, String objectType) {
        isDeferredList = TextUtils.isEmpty(objectId);

        if (objectType != null) {
            mObjectId = objectId;
            mObjectType = objectType;

            if (mAdapter != null)
                mAdapter.setEditable(isEditable());
            addFooterIfNeeded();
            if (isDeferredList) {
                ArrayList<MediaFile> list = new ArrayList<>();
                for (MediaPickerViewStrategy.PickedFile deferred: mDeferredMediaFiles)
                    list.add(new MediaFile(null, deferred.file.toURI().toASCIIString()));
                onListLoad(list);
                if (mProgressManager != null)
                    mProgressManager.showContent(false);
            }
            else {
                if (mProgressManager != null)
                    mProgressManager.hideContent(false);
                loadMediaList(mObjectId);
            }
        }
    }

    public void setReadOnly(boolean value) {
        isReadOnly = value;
    }

    public boolean isReadOnly() {
        return isReadOnly;
    }

    public void setImageFileProcessor(ImageFilePreProcessor processor) {
        mImageFilePreProcessor = processor;
    }

    public void addDeferredImage(MediaPickerViewStrategy.PickedFile pickedFile) {
        mDeferredMediaFiles.add(pickedFile);
        if (mMediaList != null) {
            mMediaList.add(new MediaFile(null, pickedFile.file.toURI().toASCIIString()));
            if (mAdapter != null)
                mAdapter.notifyDataSetChanged();
        }
    }

    protected void saveDeferred(String objectId, final SimpleCallback callback) {
        saveDeferredRecursive(objectId, mObjectType, callback);
    }

    protected void onListLoad(ArrayList<MediaFile> list) {
        mMediaList = list;

        if (mMediaList == null)
            mMediaList = new ArrayList<>();


        if (isReadOnly && mMediaList.size() == 0)
            mMediaList.add(new MediaFile("", ""));

        if (mAdapter != null) {
            mAdapter.setEditable(isEditable());
            mAdapter.changeList(mMediaList);
        }
    }

    protected void showMediaFile(MediaFile mediaFile) {

    }

    protected boolean isDeferredList() {
        return isDeferredList;
    }

    protected boolean isEditable() {
        return !isReadOnly;
    }

    protected boolean onContextMenuClicked(MenuItem item, MediaFile mediaFile) {
        int itemId = item.getItemId();
        if (itemId == R.id.hk_action_remove) {
            deleteMediaFile(mediaFile);
            return true;
        }
        return false;
    }

    private void showMediaFileMenu(final MediaFile mediaFile, View view) {
        PopupMenu popup = new PopupMenu(getContext(), view, Gravity.RIGHT | Gravity.BOTTOM);
        inflateContextMenu(popup.getMenu(), popup.getMenuInflater());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return onContextMenuClicked(item, mediaFile);
            }
        });
        popup.show();
    }

    private void loadMediaList(String objectId) {
        MediaFileFilter fileFilter = new MediaFileFilter();
        fileFilter.setObjectId(objectId);
        fileFilter.setMediaType(MediaFile.MEDIA_TYPE_IMAGE);

        HonkioApi.userMediaFilesList(fileFilter, 0, new RequestCallback<MediaFileList>() {
            @Override
            public boolean onComplete(Response<MediaFileList> response) {
                mProgressManager.showContent(true);
                if (response.isStatusAccept()) {
                    onListLoad(response.getResult().getList());
                    return true;
                }
                return onError(response.getAnyError());
            }

            @Override
            public boolean onError(RpError error) {
                mProgressManager.showContent(true);
                return onParentError(error);
            }
        });
    }

    private void addFooterIfNeeded() {
        if (mRecyclerView != null && mAdapter != null && mAdapter.getFooterViewCount() == 0 && isEditable()) {
            View footerView = LayoutInflater.from(getContext())
                    .inflate(getListAddViewId(), mRecyclerView, false);
            footerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showUploadMenu();
                }
            });
            mAdapter.addFooterView(footerView);
        }
    }

    private void showUploadMenu() {
        MediaPickerViewStrategy picturePicker = getController().getLifeCycleObserver(LCO_TAG_PICTURE_PICKER);
        if (picturePicker == null) {
            picturePicker = new MediaPickerViewStrategy();
            picturePicker.setParentLifeCycleObserverTag(TAG);
            picturePicker.setMediaType(MediaPickerViewStrategy.MEDIA_TYPE_IMAGE);
            getController().addLifeCycleObserver(picturePicker);
        }
        picturePicker.pick();
    }

    private void createMediaUrl(String objectId, String objectType, final MediaPickerViewStrategy.PickedFile pickedFile, final SimpleCallback callback) {
        HonkioApi.userGetMediaUrl(objectId, objectType, 0,
                new RequestCallback<MediaUrl>() {
                    @Override
                    public boolean onComplete(Response<MediaUrl> response) {
                        if (response.isStatusAccept()) {
                            sendMediaFile(pickedFile, response.getResult(), callback);
                            return true;
                        }
                        return onError(response.getAnyError());
                    }

                    @Override
                    public boolean onError(RpError error) {
                        dismissProgressDialog();
                        if (!onParentError(error))
                            showErrorMessage(null, error);
                        return true;
                    }
                });
    }

    private void saveDeferredRecursive(final String objectId, final String objectType, final SimpleCallback callback) {
        if (mDeferredMediaFiles.size() == 0) {
            dismissProgressDialog();
            callback.onComplete();
        }
        else {
            showProgressDialog();
            MediaPickerViewStrategy.PickedFile pickedFile = mDeferredMediaFiles.removeFirst();
            createMediaUrl(objectId, objectType, pickedFile, new SimpleCallback() {
                @Override
                public void onComplete() {
                    saveDeferredRecursive(objectId, objectType, callback);
                }

                @Override
                public boolean onError(RpError error) {
                    dismissProgressDialog();
                    return callback.onError(error);
                }
            });
        }
    }

    private void sendMediaFile(final MediaPickerViewStrategy.PickedFile pickedFile, final MediaUrl url,
                               final SimpleCallback callback) {

        final RequestCallback<Void> uploadCallback = new RequestCallback<Void>() {
            @Override
            public boolean onComplete(Response<Void> response) {
                if (response.isStatusAccept()) {
                    dismissProgressDialog();

                    if (pickedFile.isTemp)
                        pickedFile.file.delete();

                    callback.onComplete();

                    return true;
                }
                return onError(response.getAnyError());
            }

            @Override
            public boolean onError(RpError error) {
                return callback.onError(error);
            }
        };

        if (mImageFilePreProcessor == null)
            HonkioApi.userUpdateMedia(pickedFile.file, url, 0, uploadCallback);
        else {
            mImageFilePreProcessor.process(pickedFile.file, new ImageFilePreProcessor.Callback() {
                @Override
                public void onComplete(File originalFile, ByteArrayInputStream processedStream,
                                       MediaType mediaType) {
                    if (processedStream != null) {
                        HonkioApi.userUpdateMedia(processedStream, mediaType, url, 0, uploadCallback);
                    }
                    else
                        uploadCallback.onError(RpError.UNKNOWN);
                }
            });
        }
    }

    private void deleteMediaFile(final MediaFile mediaFile) {
        if (isDeferredList) {
            if (mMediaList != null) {
                int index = mMediaList.indexOf(mediaFile);
                mMediaList.remove(index);
                mDeferredMediaFiles.remove(index);

                if (mAdapter != null)
                    mAdapter.notifyDataSetChanged();
            }
        }
        else {
            showProgressDialog();
            HonkioApi.userClearMediaUrl(mediaFile.getId(), 0, new RequestCallback<MediaUrl>() {
                @Override
                public boolean onComplete(Response<MediaUrl> response) {
                    if (response.isStatusAccept()) {
                        dismissProgressDialog();

                        if (mAdapter != null && mMediaList != null)
                            mAdapter.remove(mMediaList.indexOf(mediaFile));

                        return true;
                    }
                    return onError(response.getAnyError());
                }

                @Override
                public boolean onError(RpError error) {
                    dismissProgressDialog();
                    if (!onParentError(error))
                        showErrorMessage(null, error);
                    return true;
                }
            });
        }
    }

    private boolean onParentError(RpError error) {
        Object parent = getParentLifeCycleObserver();
        if (parent instanceof ErrorCallback) {
            if (((ErrorCallback) parent).onError(error))
                return true;
        }

        parent = getController();
        if (parent instanceof ErrorCallback) {
            if (((ErrorCallback) parent).onError(error))
                return true;
        }

        return false;
    }

    public static class Adapter<ViewHolder extends HKMediaFilesListBinder.ViewHolder>
            extends BaseRecyclerListAdapter<MediaFile, ViewHolder> {

        private boolean isEditable = false;
        private HKMediaFilesListBinder<ViewHolder> mBinder;

        public Adapter(Context context, List<MediaFile> list, int layout, HKMediaFilesListBinder<ViewHolder> binder) {
            super(context, list, layout);
            setHasStableIds(true);
            mBinder = binder;
        }

        @Override
        public ViewHolder onCreateItemViewHolder(ViewGroup parent, View view, int viewType) {
            return mBinder.newHolder(view);
        }

        @Override
        public void onBindItemViewHolder(ViewHolder holder, int position) {
            mBinder.bind(mContext, holder, getItem(position));
            holder.btnMenu.setVisibility(isEditable ? View.VISIBLE : View.GONE);
        }

        @Override
        public long getItemId(int position) {
            MediaFile file = getItem(position);
            if (file != null && file.getId() != null)
                return file.getId().hashCode();
            else
                return 0;
        }

        protected void addInnerClickListener(OnItemClickListener<MediaFile, RecyclerView.ViewHolder> listener) {
            addInnerClickListener(listener, 0);
        }

        protected void addInnerClickListener(final OnItemClickListener<MediaFile, RecyclerView.ViewHolder> listener, int viewId) {
            addOnItemClickListener(new OnItemClickListener<MediaFile, ViewHolder>() {
                @Override
                public void onItemClick(ViewHolder itemHolder, View view, MediaFile mediaFile) {
                    listener.onItemClick(itemHolder, view, mediaFile);
                }
            }, viewId);
        }

        public void setEditable(boolean isEditable) {
            this.isEditable = isEditable;
        }
    }
}
