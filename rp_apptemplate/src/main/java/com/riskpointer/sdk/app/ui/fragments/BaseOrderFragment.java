package com.riskpointer.sdk.app.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Order;
import com.riskpointer.sdk.api.model.entity.OrdersList;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.OrderFilter;
import com.riskpointer.sdk.api.web.message.filter.PushFilter;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;

/**
 * Created by Shurygin Denis on 2016-01-16.
 */
@Deprecated
public abstract class BaseOrderFragment extends BaseFragment {
    public final static String ARG_ORDER = "BaseOrderFragment.ARG_ORDER";
    public final static String ARG_ORDER_ID = "BaseOrderFragment.ARG_ORDER_ID";

    public final static String DLG_TAG_ORDER_LOAD_ERROR = "BaseOrderFragment.DLG_TAG_ORDER_LOAD_ERROR";

    private Order mOrder;

    private Task mOrderLoadTask;

    protected abstract void updateUi();

    public void setOrder(Order order) {
        mOrder = order;
        if (getView() != null) {
            updateUi();
            removeOrderPushes();
        }
    }

    public Order getOrder() {
        return mOrder;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            setOrder((Order) savedInstanceState.getSerializable(ARG_ORDER));
        } else {
            Bundle args = getArguments();
            if (args != null) {
                if (args.containsKey(ARG_ORDER))
                    setOrder((Order) args.getSerializable(ARG_ORDER));
            }
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(ARG_ORDER, mOrder);
    }

    @Override
    public void onDestroy() {
        if (mOrderLoadTask != null)
            mOrderLoadTask.abort();
        super.onDestroy();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(mOrder != null) {
            updateUi();
            removeOrderPushes();
        }
        else {

            String orderId = getArguments().getString(ARG_ORDER_ID);
            if (orderId != null) {
                loadOrder(getActivity().getApplicationContext(), orderId);
            }
            else
                finish();
        }
    }

    @Override
    public boolean onDismissDialog(BaseDialogFragment dialogFragment) {
        if (DLG_TAG_ORDER_LOAD_ERROR.equals(dialogFragment.getTag())) {
            finish();
            return true;
        }
        return super.onDismissDialog(dialogFragment);
    }

    protected OrderFilter getFilter(String id) {
        final OrderFilter filter = new OrderFilter();
        filter.setId(id);
        filter.setProductDetails(true);
        filter.setQueryUnreadMessages(true);
        filter.setChildMerchants(true);
        return filter;
    }

    protected void loadOrder(Context context, String id) {
        mOrderLoadTask = doOrderGetRequest(getFilter(id), new RequestCallback<OrdersList>() {
            @Override
            public boolean onComplete(Response<OrdersList> response) {
                if (response.isStatusAccept()) {
                    if (response.getResult().getList().size() > 0) {
                        setOrder(response.getResult().getList().get(0));
                    } else {
                        showMessage(
                                DLG_TAG_ORDER_LOAD_ERROR,
                                R.string.dlg_load_order_error_title,
                                R.string.dlg_load_order_error_message);
                    }

                    removeOrderPushes();
                    return true;
                }
                return onError(response.getAnyError());
            }

            @Override
            public boolean onError(RpError error) {
                if (!BaseOrderFragment.this.onError(error)) {
                    showErrorMessage(DLG_TAG_ORDER_LOAD_ERROR, error);
                }
                removeOrderPushes();
                return true;
            }
        });
    }

    protected Task doOrderGetRequest(OrderFilter filter, RequestCallback<OrdersList> callback) {
        return HonkioApi.userGetOrders(filter, 0, callback);
    }

    protected void removeOrderPushes() {
        if (mOrder != null && mOrder.getOrderId() != null) {
            HonkioApi.userSetPush(true, new PushFilter()
                    .addContent(PushFilter.QUERY_CONTENT_ORDER, mOrder.getOrderId()),
                    Message.FLAG_NO_WARNING, null);
        }
    }
}
