/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;
import android.view.View;

import com.riskpointer.sdk.api.model.entity.Product;
import com.riskpointer.sdk.app.ui.adapters.binder.ShopProductListBinder;

import java.util.List;

public class ShopProductsListAdapter extends BaseArrayListAdapter<Product, ShopProductListBinder.ViewHolder> {

	private ShopProductListBinder mBinder;

	public ShopProductsListAdapter(Context context, List<Product> list, int layout) {
		super(context, list, layout);
		mBinder = new ShopProductListBinder();
	}

	@Override
	public ShopProductListBinder.ViewHolder newHolder(View view) {
		return new ShopProductListBinder.ViewHolder(view);
	}

	@Override
	public void bindHolder(ShopProductListBinder.ViewHolder holder, int position) {
		Product product = getItem(position);
		mBinder.bind(mContext, holder, product);
	}

}
