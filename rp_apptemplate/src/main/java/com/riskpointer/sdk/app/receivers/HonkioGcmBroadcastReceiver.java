package com.riskpointer.sdk.app.receivers;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.riskpointer.sdk.api.model.push.Push;
import com.riskpointer.sdk.api.model.push.PushParser;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.fragments.LoginFragment;

import org.json.JSONException;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author Shurygin Denis on 2015-01-26.
 */
public class HonkioGcmBroadcastReceiver extends BroadcastReceiver {
    private static int NOTIFICATION_ID = 100;
    static final String TAG = "GCMMessage";
    private static final String CHANNEL_ID = "honkio_channel";
    private static final String CHANNEL_NAME = "Honkio chanel";
    private static final String CHANNEL_DESCRIPTION = "Honkio notifications";

    private final static Set<IgnoredPush> sIgnoredPushes = new HashSet<>();
    private NotificationChannel mChannel;

    public static void addPushToIgnore(String type, String uid) {
        synchronized (sIgnoredPushes) {
            sIgnoredPushes.add(new IgnoredPush(type, uid));
        }
    }

    public static void removePushFromIgnore(String type, String uid) {
        synchronized (sIgnoredPushes) {
            Iterator<IgnoredPush> iterator = sIgnoredPushes.iterator();
            while (iterator.hasNext()) {
                IgnoredPush ignoredPush = iterator.next();
                if (ignoredPush.isEqual(type, uid)) {
                    iterator.remove();
                    return;
                }
            }
        }
    }

    public static boolean isPushIgnored(Push<?> push) {
        synchronized (sIgnoredPushes) {
            for (IgnoredPush ignoredPush: sIgnoredPushes) {
                if (ignoredPush.isEqual(push.getEntity()))
                    return true;
            }
        }
        return false;
    }

// =================================================================================================
// Methods of Super Class
// =================================================================================================

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (extras != null && !extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                Toast.makeText(context, "Server sent error: " + extras.toString(),Toast.LENGTH_SHORT).show();
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                Toast.makeText(context, "Deleted messages on Server: " + extras.toString(),Toast.LENGTH_SHORT).show();
            } else if (GoogleCloudMessaging.
                MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                try {
                    String paymoad = extras.getString("payload");
                    Log.d(TAG, "Push payload: " + paymoad);
                    Push<?> push = PushParser.getInstance().parse(paymoad);
                    if (push != null)
                        onPushReceived(context, push);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.i(TAG, "Received: " + extras.toString());
            }
        }
    }

// =================================================================================================
// Protected methods and classes
// =================================================================================================

    protected void onPushReceived(Context context, Push<?> push) {
        if (!HonkioGcmBroadcastReceiver.isPushIgnored(push)) {
            showNotification(context, push);
        }
        BroadcastHelper.sendActionOnPushReceived(context, push);
    }

    protected void showNotification(Context context, Push<?> push) {
        Context appContext = context.getApplicationContext();
        if (AppController.isInitialized()) {
            NotificationManager mNotificationManager = (NotificationManager)
                    context.getSystemService(Context.NOTIFICATION_SERVICE);

            mNotificationManager.notify(NOTIFICATION_ID, buildNotification(appContext, push));
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void createChannel(Context context) {
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        String id = CHANNEL_ID;
        CharSequence name = CHANNEL_NAME;
        String description = CHANNEL_DESCRIPTION;
        int importance = NotificationManager.IMPORTANCE_DEFAULT;
        NotificationChannel mChannel = new NotificationChannel(id, name, importance);
        mChannel.setDescription(description);
        mChannel.setShowBadge(true);
        mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        mNotificationManager.createNotificationChannel(mChannel);
    }

    protected Notification buildNotification(Context context, Push<?> push) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel(context);
        }
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context, CHANNEL_ID)
                        .setSound(Uri.parse(Settings.System.DEFAULT_NOTIFICATION_URI.toString()), AudioManager.STREAM_NOTIFICATION)
                        .setVibrate(new long[]{100, 500, 100, 1000})
                        .setSmallIcon(getNotificationSmallIcon(context, push))
                        .setLargeIcon(getNotificationLargeIcon(context, push))
                        .setColor(getNotificationColor(context, push))
                        .setContentTitle(getNotificationTitle(context, push))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(getNotificationBigText(context, push)))
                        .setContentText(getNotificationText(context, push))
                        .setAutoCancel(true);

        mBuilder.setContentIntent(getNotificationContentIntent(context, push));

        return mBuilder.build();
    }

    protected PendingIntent getNotificationContentIntent(Context context, Push<?> push) {
        if (AppController.isInitialized()) {
            Intent intent = AppController.getIntent(AppController.ACTIVITY_SPLASH);
            intent.putExtra(LoginFragment.EXTRA_ACTION_COMMAND, LoginFragment.COMMAND_PUSH);
            intent.putExtra(LoginFragment.EXTRA_COMMAND_PAYLOAD, push);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        }
        return null;
    }

    protected String getNotificationTitle(Context context, Push<?> push) {
        return push.getMessage();
    }

    protected String getNotificationText(Context context, Push<?> push) {
        return null;
    }

    protected String getNotificationBigText(Context context, Push<?> push) {
        return getNotificationText(context, push);
    }

    protected int getNotificationSmallIcon(Context context, Push<?> push) {
        return R.drawable.hk_ic_account_invoice;
    }

    protected Bitmap getNotificationLargeIcon(Context context, Push<?> push) {
        return null;
    }

    protected int getNotificationColor(Context context, Push<?> push) {
        return Color.GRAY;
    }

// =================================================================================================
// Private methods and classes
// =================================================================================================

    private void playSoundAndVibrate(Context context, Push<?> push) {
        Uri soundUri = Uri.parse(Settings.System.DEFAULT_NOTIFICATION_URI.toString());
        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
        mediaPlayer.setLooping(false);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.release();
            }
        });
        try {
            mediaPlayer.setDataSource(context, soundUri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
            mediaPlayer.release();
        }

        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(new long[]{100, 500, 100, 1000}, -1);
    }

    private static class IgnoredPush {

        final String type;
        final String uid;

        IgnoredPush(String type, String uid) {
            this.type = type;
            this.uid = uid;
        }

        public boolean equals(IgnoredPush ignoredPush) {
            return ignoredPush != null && isEqual(ignoredPush.type, ignoredPush.uid);
        }

        boolean isEqual(Push.PushEntity entity) {
            return entity != null && isEqual(entity.getType(), entity.getUid());
        }

        boolean isEqual(String type, String uid) {
            boolean isEqual;

            if (this.type != null)
                isEqual = this.type.equals(type);
            else
                isEqual = type == null;

            if (isEqual) {
                if (this.uid != null)
                    isEqual = this.uid.equals(uid);
                else
                    isEqual = uid == null;
            }
            return isEqual;
        }
    }

}