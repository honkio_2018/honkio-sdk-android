/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.riskpointer.sdk.api.BaseHonkioApi;
import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.process.LogoutProcessModel;
import com.riskpointer.sdk.api.utils.ApiDebugUtils;
import com.riskpointer.sdk.api.utils.LocationFinder;
import com.riskpointer.sdk.api.web.callback.ErrorCallback;
import com.riskpointer.sdk.api.web.callback.SimpleCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.app.FragmentComponents;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.services.HonkioNfcHostService;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.ui.dialogs.MessageDialog;
import com.riskpointer.sdk.app.ui.fragments.QrScannerFragment;
import com.riskpointer.sdk.app.ui.strategy.ViewLifeCycleObserver;
import com.riskpointer.sdk.app.ui.strategy.ViewLifeCycleObserversSet;
import com.riskpointer.sdk.app.utils.AppPreferences;
import com.riskpointer.sdk.app.utils.NfcUriRecord;
import com.riskpointer.sdk.app.utils.RequestCodes;
import com.riskpointer.sdk.app.utils.uri.UriHandler;

public abstract class BaseActivity extends AppCompatActivity implements ErrorCallback,
        BaseDialogFragment.OnDismissListener, BaseDialogFragment.OnClickListener,
        ViewLifeCycleObserver.ViewController {
	public static final String ACTION_CLEAR_STACK = "com.honkio.sdk.action.CLEAR_STACK";
    public static final String EXTRA_ACTION_MASK = "com.honkio.sdk.EXTRA_ACTION_MASK";

    public static final String EXTRA_INTENT_ID = "BaseActivity.EXTRA_INTENT_ID";
    public static final String EXTRA_LAYOUT_ID = "BaseActivity.EXTRA_LAYOUT_ID";
    public static final String EXTRA_LCO = "BaseActivity.EXTRA_LCO";
    public static final String EXTRA_IGNORE_INITIALIZATION = "BaseActivity.EXTRA_IGNORE_INITIALIZATION";
	
	private static final long APP_BACKGROUND_LIVE_TIME = 0;
    public static final int REQUEST_CAPTURE_URI = 101;

    public static final String DLG_TAG_NO_CONNECTION = "BaseActivity.DLG_TAG_NO_CONNECTION";
    public static final String DLG_TAG_BAD_RESPONSE = "BaseActivity.DLG_TAG_BAD_RESPONSE";
    public static final String DLG_TAG_FINISH = "BaseActivity.DLG_TAG_FINISH";
    public static final String DLG_TAG_LOGOUT = "BaseActivity.DLG_TAG_LOGOUT";
    public static final String DLG_TAG_INVALID_PIN = "BaseActivity.DLG_TAG_INVALID_PIN";
    public static final String DLG_TAG_INVALID_SECURITY = "BaseActivity.DLG_TAG_INVALID_SECURITY";
    public static final String DLG_TAG_PROGRESS = "BaseActivity.DLG_TAG_PROGRESS";
    public static final String DLG_TAG_UNSUPPORTED_APP = "BaseActivity.DLG_TAG_UNSUPPORTED_APP";
    public static final String DLG_TAG_TEMP_PASSWORD = "BaseActivity.DLG_TAG_TEMP_PASSWORD";
    public static final String DLG_TAG_UNSUPPORTED_ACCOUNT = "BaseActivity.DLG_TAG_UNSUPPORTED_ACCOUNT";
    public static final String DLG_TAG_USER_NOT_ACTIVE = "BaseActivity.DLG_TAG_USER_NOT_ACTIVE";
    public static final String DLG_TAG_UNSUPPORTED_REQUEST = "BaseActivity.DLG_TAG_UNSUPPORTED_REQUEST";
    public static final String DLG_TAG_WRONG_HASH = "BaseActivity.DLG_TAG_WRONG_HASH";
    public static final String DLG_TAG_SUCCESS = "BaseActivity.DLG_TAG_SUCCESS";

    private static final String SAVED_LCO_SET = "BaseActivity.SAVED_LCO_SET";

    private static int sActivitiesCount = 0;
	
	private BroadcastReceiver mClearStackReceiver;
	private BroadcastReceiver mScreenReceiver;

    private final ViewLifeCycleObserversSet mLcoSet;

    private boolean isFinishedAfterOnStop = false;
    private boolean isStateSaved = false;
    private boolean isDestroyed = false;

    private NfcAdapter mNfcAdapter;
    private PendingIntent mNfcPendingIntent;

	// Variables for detecting when the App goes to background or foreground.
	private static boolean isAppWentToBg = false;
	private static boolean isWindowFocused = false;
	private static boolean isBackPressed = false;
	private static PendingIntent sAppClosingIntent;
	
    // Debug value. If not 0 the app will closed after going into background.
	private static final long SYSTEM_EXIT_DELAY = 0;
	private Thread mSystemExitThread;

    public BaseActivity() {
        super();
        mLcoSet = new ViewLifeCycleObserversSet(this);
    }

// =================================================================================================
// Abstract methods
// =================================================================================================

    protected abstract int getDefaultLayoutId();

// =================================================================================================
// Methods of Super Class
// =================================================================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
        sActivitiesCount++;

        if (getIntent().hasExtra(EXTRA_LCO)) {
            Class<?> observerClass = (Class<?>) getIntent().getSerializableExtra(EXTRA_LCO);
            if (observerClass != null
                    && ViewLifeCycleObserver.class.isAssignableFrom(observerClass))
                try {
                    ViewLifeCycleObserver observer = (ViewLifeCycleObserver)
                            observerClass.newInstance();
                    addLifeCycleObserver(observer);
                    observer.onWillCreate(this, savedInstanceState);
                } catch (java.lang.InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
        }

		super.onCreate(savedInstanceState);

        int layoutId = getLayoutId();
        if (layoutId != 0)
            setContentView(layoutId);

        onTrackScreen();
		
		IntentFilter intentFilter = new IntentFilter(ACTION_CLEAR_STACK);
        intentFilter.addCategory(getPackageName());
        final int intentId = getIntent().getIntExtra(EXTRA_INTENT_ID, 0);
	    mClearStackReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
                int mask = intent != null ? intent.getIntExtra(EXTRA_ACTION_MASK, 0) : 0;
                if ((intentId & mask) == mask)
				    finish();
			}
		};
		registerReceiver(mClearStackReceiver, intentFilter);
		
		IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addCategory(getPackageName());
        mScreenReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
		        	onAppGoToBackground();
		        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
		        	onAppGoToForeground();
		        }
			}
        	
        };
        registerReceiver(mScreenReceiver, filter);
        checkInitialisation();
        if (getUriHandler() != null && checkPermission(Manifest.permission.NFC)) {
            mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
            if(mNfcAdapter != null) {
                mNfcPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                        getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            }
        }

        if (savedInstanceState != null) {
            Bundle lcoBundle = savedInstanceState.getBundle(SAVED_LCO_SET);
            if (lcoBundle != null)
                mLcoSet.restoreState(savedInstanceState);
        }

        mLcoSet.onCreate(savedInstanceState);
        mLcoSet.onViewCreated(savedInstanceState);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu)
                & mLcoSet.onCreateOptionsMenu(menu, getMenuInflater());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item) || mLcoSet.onOptionsItemSelected(item);
    }

    @Override
	protected void onStart() {
		super.onStart();
		applicationWillEnterForeground();
		mLcoSet.onStart();
	}

    @Override
    protected void onResume() {
        super.onResume();
        if (mNfcAdapter != null)
            mNfcAdapter.enableForegroundDispatch(this, mNfcPendingIntent, null, null);

        mLcoSet.onResume();

        isStateSaved = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mNfcAdapter != null)
            mNfcAdapter.disableForegroundDispatch(this);

        mLcoSet.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle(SAVED_LCO_SET, mLcoSet.saveState());
        isStateSaved = true;
    }

    @Override
	protected void onStop() {
		super.onStop();
		if(isFinishedAfterOnStop)
			finish();
		applicationDidEnterBackground();

        mLcoSet.onStop();
	}
	
	@Override
	protected void onDestroy() {
        mLcoSet.onDestroy();

		isDestroyed = true;
		if(mSystemExitThread != null) {
			mSystemExitThread.interrupt();
			mSystemExitThread = null;
		}
		if(mClearStackReceiver != null)
			unregisterReceiver(mClearStackReceiver);
		if(mScreenReceiver != null)
			unregisterReceiver(mScreenReceiver);

        sActivitiesCount--;
        if (sActivitiesCount <= 0) {
            LocationFinder locationFinder = HonkioApi.getLocationFinder();
            locationFinder.stopForce();
        }

		super.onDestroy();
	}

    @Override
    public void onBackPressed() {
        isBackPressed = true;
	    if (mLcoSet.onBackPressed())
	        return;
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == REQUEST_CAPTURE_URI) {
            if (resultCode == RESULT_OK && intent != null) {
                String text = intent.getExtras().getString(QrScannerFragment.EXTRA_CODE_TEXT);
                if (text != null) {
                    handleUri(text);
                }
            }
        } else
            super.onActivityResult(requestCode, resultCode, intent);

        mLcoSet.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        isWindowFocused = hasFocus;
        if (isBackPressed && !hasFocus) {
            isBackPressed = false;
            isWindowFocused = true;
        }
        super.onWindowFocusChanged(hasFocus);
    }

    @Override
    public void onNewIntent(Intent intent) {
        if(NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())
                ||NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction()))
            handleNfc(intent);
    }

// =================================================================================================
// Methods from Interfaces
// =================================================================================================

    @Override
    public boolean onDismissDialog(BaseDialogFragment dialogFragment) {
	    if (mLcoSet.onDismissDialog(dialogFragment))
	        return true;

        if (dialogFragment != null && dialogFragment.getTag() != null) switch (dialogFragment.getTag()) {
            case DLG_TAG_LOGOUT:
                showProgressDialog();
                new LogoutProcessModel()
                        .setForced(true)
                        .logout(new SimpleCallback() {
                            @Override
                            public void onComplete() {
                                restart(BaseActivity.this);
                            }

                            @Override
                            public boolean onError(RpError error) {
                                onComplete();
                                return true;
                            }
                        });
                break;
            case DLG_TAG_SUCCESS:
                setResult(RESULT_OK);
                finish();
                break;
            case DLG_TAG_FINISH:
                finish();
                break;
            case DLG_TAG_UNSUPPORTED_APP:
                clearStack(BaseActivity.this, 0);
                break;
            case DLG_TAG_TEMP_PASSWORD:
                startActivity(AppController.getIntent(AppController.SCREEN_SETTINGS_CHANGE_PASSWORD, true));
                break;
            default:
                return false;
        }
        return true;
    }

    @Override
    public boolean onClickDialog(BaseDialogFragment dialogFragment, int which) {
	    if (mLcoSet.onClickDialog(dialogFragment, which))
	        return true;

        if (DLG_TAG_UNSUPPORTED_APP.equals(dialogFragment.getTag())) {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (ActivityNotFoundException e) {
                    // When market app isn't installed.
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        }
        else
            return false;
        return true;
    }

    @Override
    public boolean onError(RpError error) {
        return isDestroyed || handleError(error);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (permissions.length > 0 && requestCode == RequestCodes.REQUEST_PERMISSION) {
            if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED)
                onPermissionDenied(permissions[0], requestCode);
            else
                onPermissionGranted(permissions[0], requestCode);
        }
        else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    // =================================================================================================
// Public methods and classes
// =================================================================================================

    public void addLifeCycleObserver(ViewLifeCycleObserver observer) {
	    mLcoSet.addLifeCycleObserver(observer);
    }

    public <T extends ViewLifeCycleObserver> T getLifeCycleObserver(String tag) {
        return mLcoSet.getLifeCycleObserver(tag);
    }

    public void removeLifeCycleObserver(String tag) {
	    mLcoSet.removeLifeCycleObserver(tag);
    }

    public void removeLifeCycleObserver(ViewLifeCycleObserver observer) {
        mLcoSet.removeLifeCycleObserver(observer);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    public void finishAfterOnStop() {
        isFinishedAfterOnStop = true;
    }

    public boolean isActivityDestroyed() {
        return isDestroyed;
    }

    public boolean isStateSaved() {
        return isStateSaved;
    }

    public void onActionClick(View view) {
        mLcoSet.onActionClick(view.getId());
    }

    public boolean handleError(RpError error) {
        switch (error.code) {
            case RpError.Api.NO_CONNECTION:
                showMessage(DLG_TAG_NO_CONNECTION, R.string.dlg_bad_connection_title, R.string.dlg_bad_connection_message);
                return true;
            case RpError.Api.BAD_RESPONSE:
                showMessage(DLG_TAG_BAD_RESPONSE, R.string.dlg_bad_status_code_title, R.string.dlg_bad_status_code_message);
                return true;
            case RpError.Common.INVALID_SECURITY:
                if (error.isHasField(Message.Param.SHOP_ID) && error.isHasField(Message.Param.HASH)) {
                    showMessage(DLG_TAG_INVALID_SECURITY, R.string.dlg_invalid_shop_security_title, R.string.dlg_invalid_shop_security_message);
                    return true;
                }
                break;
            case RpError.System.SERVICE_WORKS:
                showMessage(DLG_TAG_NO_CONNECTION,
                        getString(R.string.dlg_service_work_title),
                        TextUtils.isEmpty(error.description) ?
                                getString(R.string.dlg_service_work_message) :
                                error.description);
                return true;
            case RpError.Api.NO_INITIALIZED:
                restart(this);
                return true;
            case RpError.Api.CANCELED_BY_USER:
            case RpError.Api.ABORTED:
                return true;
            case RpError.UserPayment.USER_NOT_ACTIVE:
                showMessage(DLG_TAG_USER_NOT_ACTIVE, R.string.dlg_need_verify_email_title, R.string.dlg_need_verify_email_message);
                return true;
            case RpError.Api.NO_LOGIN:
            case RpError.Common.INVALID_USER_LOGIN:
                if (!AppController.getInstance().isGuestModeAvailable()
                        || (HonkioApi.getActiveConnection() != null && HonkioApi.getActiveConnection().isValid())) {
                    showMessage(DLG_TAG_LOGOUT, 0, R.string.dlg_otp_lost_message);
                    return true;
                }
                break;
            case RpError.Api.INVALID_PIN:
                if(!BaseHonkioApi.isConnected()) {
                    showMessage(DLG_TAG_LOGOUT, 0, R.string.dlg_pin_logout_message);
                }
                else {
                    showMessage(DLG_TAG_INVALID_PIN, 0, R.string.dlg_pin_invalid_message);
                }
                return true;
            case RpError.Api.ACCOUNT_NOT_SUPPORTED:
                showMessage(DLG_TAG_UNSUPPORTED_ACCOUNT, R.string.dlg_unsuported_account_title, R.string.dlg_unsuported_account_message);
                return true;
            case RpError.Api.UNSUPPORTED_REQUEST:
                showMessage(DLG_TAG_UNSUPPORTED_REQUEST, R.string.dlg_unsupported_request_title, R.string.dlg_unsupported_request_message);
                return true;
            case RpError.Api.WRONG_HASH:
                showMessage(DLG_TAG_WRONG_HASH, "Bad server response", "Wrong hash!");
                return true;
            case RpError.Common.INVALID_VALUE_FOR_PARAMETER:
                if (error.isHasField(Message.Param.IDENTITY_CLIENT)) {
                    showMessage(DLG_TAG_UNSUPPORTED_APP, R.string.dlg_unsupported_app_title, R.string.dlg_unsupported_app_message, MessageDialog.BTN_OK_CANCEL);
                    return true;
                }
                break;
        }
        return false;
    }

    public void showProgressDialog() {
        showProgressDialog(null, null);
    }

    public void showProgressDialog(String tag) {
        showProgressDialog(null, null, tag);
    }

    public void showProgressDialog(int titleId, int messageId) {
        showProgressDialog(getString(titleId), getString(messageId));
    }

    public void showProgressDialog(int titleId, int messageId, String tag) {
	    showProgressDialog(getString(titleId), getString(messageId), tag);
    }

    public void showProgressDialog(String title, String message) {
        showProgressDialog(title, message, DLG_TAG_PROGRESS);
    }

    public void showProgressDialog(String title, String message, String tag) {
        if(!isDestroyed && getSupportFragmentManager().findFragmentByTag(tag) == null)
            showDialog(AppController.DIALOG_FRAGMENT_PROGRESS, tag, title, message);
    }

    public void dismissProgressDialog() {
        if(!isDestroyed) {
            dismissDialog(DLG_TAG_PROGRESS);
        }
    }

    @Override
    public void dismissDialog(String tag) {
        if(tag != null && !isDestroyed) {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
            if (fragment instanceof DialogFragment)
                ((DialogFragment) fragment).dismiss();
        }
    }

    public DialogFragment showMessage(String tag, int titleId, int messageId) {
        return showMessage(tag, titleId, messageId, MessageDialog.BTN_NOT_SET);
    }

    public DialogFragment showMessage(String tag, int titleId, int messageId, int buttonsVariant) {
        return showMessage(tag, titleId != 0 ? getString(titleId) : null, getString(messageId), buttonsVariant);
    }

    public DialogFragment showMessage(String tag, int titleId, int messageId, int buttonsVariant, Bundle bundle) {
        return showMessage(tag, titleId != 0 ? getString(titleId) : null, getString(messageId), buttonsVariant, bundle);
    }

    public DialogFragment showMessage(String tag, String title, String message) {
        return showMessage(tag, title, message, MessageDialog.BTN_NOT_SET);
    }

    public DialogFragment showMessage(String tag, String title, String message, int buttonsVariant) {
        return showMessage(tag, title, message, buttonsVariant, null);
    }

    public DialogFragment showMessage(String tag, String title, String message, int buttonsCount, Bundle bundle) {
        return showDialog(AppController.DIALOG_FRAGMENT_MESSAGE, tag, title, message, buttonsCount, bundle);
    }

    public DialogFragment showErrorMessage(String tag, RpError error) {
        String title = getString(R.string.dlg_unknown_error_title);
        String message = error.description != null ? error.description.trim() : null;
        if (TextUtils.isEmpty(message))
            message = getString(R.string.dlg_unknown_error_message);
        return showMessage(tag, title, message);
    }

    public DialogFragment showDialog(int dialogId, String tag, Object... params) {
        FragmentComponents components = AppController.getFragmentComponents(dialogId, params);
        if (components != null && components.isDialog()) {
            return showDialog(components.newDialog(), tag);
        }
        return null;
    }

    public DialogFragment showDialog(DialogFragment dialog, String tag) {
        getSupportFragmentManager().beginTransaction().add(dialog, tag).commitAllowingStateLoss();
        return dialog;
    }

    public void setNfcMessage(String message) {
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            SharedPreferences preferences = getSharedPreferences(AppPreferences.PREFERENCES_FILE, MODE_PRIVATE);
            preferences.edit().putString(HonkioNfcHostService.PREF_NFC_MESSAGE, message).apply();
        }
    }

    @Deprecated
    public boolean handleUri(String uriString) {
        if(getUriHandler() != null)
            return getUriHandler().handle(uriString);
        return false;
    }

    @Deprecated
    public void onUriHandled() {

    }

// =================================================================================================
// Protected methods and classes
// =================================================================================================

    protected void onPermissionDenied(String permission, int requestCode) {
	    mLcoSet.onPermissionDenied(permission, requestCode);
    }

    protected void onPermissionGranted(String permission, int requestCode) {
	    mLcoSet.onPermissionGranted(permission, requestCode);
    }

    @Deprecated
    protected UriHandler getUriHandler() {
	    ViewLifeCycleObserver lco = getLifeCycleObserver(null);
        if (lco != null)
            return lco.getUriHandler(this);
        return null;
    }

    protected void setTextIfExists(int textViewId, String text) {
        if (text != null) {
            TextView textView = (TextView) findViewById(textViewId);
            if (textView != null)
                textView.setText(text);
        }
    }

    protected void setVisibilityIfExists(int viewId, int visibility) {
        View view = findViewById(viewId);
        if (view != null)
            view.setVisibility(visibility);
    }

    protected void onTrackScreen() {
        AppController.trackScreen(getClass().getSimpleName());
    }

    protected boolean checkInitialisation() {
        if(!getIntent().getBooleanExtra(EXTRA_IGNORE_INITIALIZATION, false)
                && !BaseHonkioApi.isInitialised()) {
            restart(this);
            finish();
            return false;
        }
        return true;
    }

    protected boolean checkPermission(String permission) {
        return checkCallingOrSelfPermission(permission) != PackageManager.PERMISSION_DENIED;
    }

    @SuppressWarnings("unused")
    protected void onAppGoToBackground() {
        if(APP_BACKGROUND_LIVE_TIME > 0) {
            AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + APP_BACKGROUND_LIVE_TIME, getAppClosingIntent());
        }
        if(SYSTEM_EXIT_DELAY > 0 && ApiDebugUtils.isDebuggable(this)) {
            mSystemExitThread = new Thread() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(SYSTEM_EXIT_DELAY);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        return;
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("DEBUG", "System.exit(0)");
                            System.exit(0);
                        }
                    });
                }
            };
            mSystemExitThread.start();
        }
    }

    @SuppressWarnings("unused")
    protected void onAppGoToForeground() {
        if(APP_BACKGROUND_LIVE_TIME > 0) {
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(getAppClosingIntent());
        }
        if(mSystemExitThread != null) {
            mSystemExitThread.interrupt();
            mSystemExitThread = null;
        }
    }

// =================================================================================================
// Public Static methods and classes
// =================================================================================================

	public static Context clearStack(Context context, int mask) {
		Context appContext = context.getApplicationContext();
		if(appContext == null) appContext = context;
		
		Intent broadcastIntent = new Intent();
		broadcastIntent.setAction(ACTION_CLEAR_STACK);
        broadcastIntent.putExtra(EXTRA_ACTION_MASK, mask);
        broadcastIntent.addCategory(context.getPackageName());
		appContext.sendBroadcast(broadcastIntent);
		
		return appContext;
	}
	
	public static void startMainActivity(Activity activity) {
        Context appContext = activity.getApplicationContext();
        Intent intent = AppController.getIntent(AppController.ACTIVITY_MAIN);
		clearStack(appContext, 0);
	    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    appContext.startActivity(intent);
	}

    public static void restart(Context context) {
        Intent intent = AppController.getIntent(AppController.ACTIVITY_SPLASH);
        Context appContext = clearStack(context, 0);

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        appContext.startActivity(intent);
    }

    public static void startPinActivity(Activity activity, int reason, int requestCode) {
        activity.startActivityForResult(buildPinActivityIntent(reason), requestCode);
    }

    public static void startPinActivity(Fragment fragment, int reason, int requestCode) {
        fragment.startActivityForResult(buildPinActivityIntent(reason), requestCode);
    }

    private static Intent buildPinActivityIntent(int reason) {
        return AppController.getIntent(AppController.ACTIVITY_PIN, reason);
    }

    public static final class AppCloseAlarmReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            BaseActivity.clearStack(context, 0);
        }
    }

// =================================================================================================
// Private methods and classes
// =================================================================================================

    private int getLayoutId() {
        if (getIntent().hasExtra(EXTRA_LAYOUT_ID))
            return getIntent().getIntExtra(EXTRA_LAYOUT_ID, getDefaultLayoutId());
        return getDefaultLayoutId();
    }

    private void handleNfc(Intent intent) {
        if(BaseHonkioApi.isConnected()) {
            Parcelable[] messages = intent
                    .getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if(messages != null) {
                NdefRecord firstRecord = ((NdefMessage) messages[0]).getRecords()[0];
                NfcUriRecord uriRecord = NfcUriRecord.parse(firstRecord);
                if (uriRecord != null)
                    handleUri(uriRecord.getUri().toString());
            }
        }
    }

    private void applicationDidEnterBackground() {
        if (!isWindowFocused) {
            isAppWentToBg = true;
            onAppGoToBackground();
        }
    }

    private void applicationWillEnterForeground() {
        if (isAppWentToBg) {
            isAppWentToBg = false;
            onAppGoToForeground();
        }
    }

    private synchronized PendingIntent getAppClosingIntent() {
        if(sAppClosingIntent == null) {
            Intent intent = new Intent(this.getApplicationContext(), AppCloseAlarmReceiver.class);
            sAppClosingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), 0, intent, 0);
        }
        return sAppClosingIntent;
    }

}
