/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.applications;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.Toast;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.support.BroadcastManager;
import com.riskpointer.sdk.api.utils.ApiHash;
import com.riskpointer.sdk.api.utils.ApiLog;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public abstract class BaseApplication extends Application {

    protected abstract AppController getAppController();

    @Override
    public void onCreate() {
        super.onCreate();

        AppController.initInstance(getAppController());

        UnhandledMessagesReceiver receiver = new UnhandledMessagesReceiver();
        BroadcastManager broadcastManager = BroadcastManager.getInstance(this);
        broadcastManager.registerReceiver(receiver, new IntentFilter(BroadcastHelper.BROADCAST_ACTION_UNHANDLED_ERROR));
        broadcastManager.registerReceiver(receiver, new IntentFilter(BroadcastHelper.BROADCAST_ACTION_UNHANDLED_RESPONSE));
        broadcastManager.registerReceiver(photoUpdateReceiver,
                new IntentFilter(BroadcastHelper.getActionOnComplete(Message.Command.USER_SET_PHOTO)));

        ApiLog.d("Certificate SHA1 fingerprint: " + ApiHash.getCertificateSHA1Fingerprint(this));
    }
    private BroadcastReceiver photoUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && BroadcastHelper.getActionOnComplete(Message.Command.USER_SET_PHOTO).equals(intent.getAction())) {
                Picasso.with(getApplicationContext()).invalidate(HonkioApi.getUserPhotoUrl(HonkioApi.getActiveUser().getUserId()));
            }
        }
    };

    protected void onUnhandledResponse(Response<?> response, ArrayList<StackTraceElement> stackTrace) {
        showErrorToast(this, response.getAnyError());
        ApiLog.e("Unhandled response:" + response.toString());
        ApiLog.e(stacktraceToString(stackTrace));
    }

    protected void onUnhandledError(RpError error, ArrayList<StackTraceElement> stackTrace) {
        showErrorToast(this, error);
        ApiLog.e("Unhandled error:" + error.toString());
        ApiLog.e(stacktraceToString(stackTrace));
    }

    protected void showErrorToast(Context context, RpError error) {
        if (error != null && error.description != null && !"".equals(error.description)) {
            Toast.makeText(context, error.description, Toast.LENGTH_SHORT).show();
        }
    }

    private class UnhandledMessagesReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (BroadcastHelper.BROADCAST_ACTION_UNHANDLED_ERROR.equals(intent.getAction())) {
                RpError error = (RpError) intent.getSerializableExtra(BroadcastHelper.BROADCAST_EXTRA_ERROR);
                ArrayList<StackTraceElement> stackTrace = (ArrayList<StackTraceElement>)
                        intent.getSerializableExtra(BroadcastHelper.BROADCAST_EXTRA_STACKTRACE);
                onUnhandledError(error, stackTrace);
            }
            else if (BroadcastHelper.BROADCAST_ACTION_UNHANDLED_RESPONSE.equals(intent.getAction())) {
                Response<?> response = (Response<?>) intent.getSerializableExtra(BroadcastHelper.BROADCAST_EXTRA_RESPONSE);
                if (response != null) {
                    ArrayList<StackTraceElement> stackTrace = (ArrayList<StackTraceElement>)
                            intent.getSerializableExtra(BroadcastHelper.BROADCAST_EXTRA_STACKTRACE);
                    onUnhandledResponse(response, stackTrace);
                }
            }
        }
    }

    private String stacktraceToString(ArrayList<StackTraceElement> stackTrace) {
        if (stackTrace != null && stackTrace.size() > 0) {
            StringBuilder builder = new StringBuilder();
            for (StackTraceElement element: stackTrace) {
                if (builder.length() > 0)
                    builder.append("\n");
                builder.append(element.toString());
            }
            return builder.toString();
        }
        else {
            return "No stacktrace";
        }
    }
}