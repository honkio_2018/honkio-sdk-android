package com.riskpointer.sdk.app.ui.adapters.binder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Shurygin Denis on 2017-02-08.
 */

public interface SimpleListBinder<ListItem, ViewHolder extends RecyclerView.ViewHolder> {

    void bind(Context context, ViewHolder holder, ListItem item);

}
