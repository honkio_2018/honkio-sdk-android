package com.riskpointer.sdk.app.textvalidator;

/**
 * @author Shurygin Denis on 2015-02-25.
 */
public class AmountTextValidator extends NoEmptyTextValidator {
    
    public AmountTextValidator(String errorMessage) {
        super(errorMessage);
    }

    @Override
    public boolean validate(String string) {
        if (super.validate(string)) {
            try {
                return Float.parseFloat(string) > 0;
            }
            catch (NumberFormatException e) {
                // do nothing
            }
        }
        return false;
    }
}
