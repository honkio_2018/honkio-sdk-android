package com.riskpointer.sdk.app.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebView;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.AppInfo;
import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;

/**
 * @author Shurygin Denis on 2015-04-08.
 */
public class TermsOfUseFragment extends BaseFragment {
    public static final String ARG_DIALOG_TITLE = "TermsOfUseFragment.ARG_DIALOG_TITLE";
    public static final String ARG_DIALOG_MESSAGE = "TermsOfUseFragment.ARG_DIALOG_MESSAGE";
    public static final String ARG_REASON = "TermsOfUseFragment.ARG_REASON";
    public static final String ARG_TOU = "TermsOfUseFragment.ARG_TOU";

    public static final int REASON_NO = 0;
    public static final int REASON_NEW_VERSION = 1;

    private static final String SAVED_IS_DIALOG_SHOWN = "TermsOfUseFragment.SAVED_IS_DIALOG_SHOWN";

    private View mAcceptBtn;
    private WebView mWebView;

    private boolean isDialogShown = false;

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_terms_of_use;
    }

    @Override
    public String getTitle(Context context) {
        return context.getString(R.string.terms_of_use_title);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            isDialogShown = savedInstanceState.getBoolean(SAVED_IS_DIALOG_SHOWN, isDialogShown);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAcceptBtn = view.findViewById(R.id.hk_btnAccept);
        mWebView = view.findViewById(R.id.hk_webView);

        mAcceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAdded()) {
                    if (HonkioApi.getActiveUser() != null) {
                        showProgressDialog();
                        HonkioApi.userTouAgree(getTouInfo().key, 0, new RequestCallback<Void>() {
                            @Override
                            public boolean onComplete(Response<Void> response) {
                                if (isAdded()) {
                                    if (response.isStatusAccept()) {
                                        User user = HonkioApi.getActiveUser();
                                        AppInfo.TouInfo touInfo = getTouInfo();
                                        user.setTouVersion(
                                                HonkioApi.getAppIdentity().getId(),
                                                touInfo.key,
                                                new User.TouVersion(touInfo.version, System.currentTimeMillis())
                                        );
                                        onTouAccept();
                                        return true;
                                    } else {
                                        return onError(response.getAnyError());
                                    }
                                }
                                return true;
                            }

                            @Override
                            public boolean onError(RpError error) {
                                dismissProgressDialog();
                                if (isAdded() && !TermsOfUseFragment.this.onError(error)) {
                                    showMessage(null, R.string.terms_of_use_title, R.string.terms_of_use_version_update_failed);
                                }
                                return true;
                            }
                        });
                    } else {
                        onTouAccept();
                    }
                }
            }
        });


        AppInfo.TouInfo touInfo = getTouInfo();
        if (touInfo != null && touInfo.url != null) {
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.loadUrl(touInfo.url);
        }

        switch (getReason()) {
            case REASON_NEW_VERSION:
                mAcceptBtn.setVisibility(View.VISIBLE);
                break;
            default:
                mAcceptBtn.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!isDialogShown) {
            Bundle args = getArguments();
            if (args != null) {
                String message = args.getString(ARG_DIALOG_MESSAGE);
                if (message != null)
                    showMessage(null, args.getString(ARG_DIALOG_TITLE), message);
            }
            isDialogShown = true;
        }
    }

    private void onTouAccept() {
        Activity activity = getActivity();
        if (isAdded() && activity != null) {
            if (activity.getCallingActivity() != null)
                finish(Activity.RESULT_OK);
            else {
                BaseActivity.startMainActivity(activity);
            }
        }
    }

    private int getReason() {
        Bundle extras = getArguments();
        int args = REASON_NO;
        if(extras != null)
            args = extras.getInt(ARG_REASON, REASON_NO);
        return args;
    }

    private AppInfo.TouInfo getTouInfo() {
        AppInfo.TouInfo touInfo = null;
        Bundle args = getArguments();
        if (args != null) {
            touInfo = (AppInfo.TouInfo) args.getSerializable(ARG_TOU);
        }

        if (touInfo == null)
            touInfo = HonkioApi.getAppInfo().getTouInfo();

        return touInfo;
    }
}
