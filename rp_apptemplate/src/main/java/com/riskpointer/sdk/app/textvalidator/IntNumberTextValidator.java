package com.riskpointer.sdk.app.textvalidator;

import android.text.TextUtils;

/**
 * @author Shurygin Denis on 2015-08-17.
 */
public class IntNumberTextValidator extends BaseTextValidator {

    private boolean isAllowEmpty = false;

    public IntNumberTextValidator(String errorMessage) {
        super(errorMessage);
    }

    @Override
    public boolean validate(String string) {
        if (TextUtils.isEmpty(string))
            return isAllowEmpty;

        int number;
        try {
            number = Integer.parseInt(string);
        } catch (NumberFormatException e) {
            return false;
        }
        return validateNumber(number);
    }

    public void setAllowEmpty(boolean value) {
        isAllowEmpty = value;
    }

    protected boolean validateNumber(int number) {
        return true;
    }
}
