package com.riskpointer.sdk.app.qrscaner.android;

import com.google.zxing.ResultPoint;
import com.riskpointer.sdk.app.qrscaner.android.camera.CameraManager;

/**
 * @author Shurygin Denis
 */

public interface ViewfinderView {

    void setCameraManager(CameraManager cameraManager);
    void addPossibleResultPoint(ResultPoint point);
    void drawViewfinder();

    void setVisibility(int visible);
}
