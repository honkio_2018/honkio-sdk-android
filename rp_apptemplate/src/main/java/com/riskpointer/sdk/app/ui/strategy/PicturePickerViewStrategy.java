package com.riskpointer.sdk.app.ui.strategy;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.utils.HonkioFileProvider;
import com.riskpointer.sdk.app.utils.RequestCodes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

@Deprecated
public class PicturePickerViewStrategy extends BaseViewLifeCycleObserver {

    public interface Listener {
        void onFilePicked(File file, boolean isTemp, int request);
        void onFileProcessing(int request);
        void onFilePickError(int request);
    }

    public static final String DLG_OPEN_PERMISSION_SETTING = "PicturePickerViewStrategy.DLG_TAG_OPEN_PERMISSION_SETTING";
    public static final String DLG_SELECT_RESOURCE = "PicturePickerViewStrategy.DLG_SELECT_RESOURCE";

    public static final int RESOURCE_GALLERY = 0;
    public static final int RESOURCE_CAMERA = 1;

    private static final String SAVED_CAMERA_FILE = PicturePickerViewStrategy.class.getName() + ".SAVED_CAMERA_FILE";

    private File mCameraOutFile;
    private Listener mListener;

    @Override
    public void onAttachToController(ViewController viewController) {
        if (!(viewController instanceof Listener))
            throw new IllegalArgumentException("Fragment must implement PicturePickerViewStrategy.Delegate");

        mListener = (Listener) viewController;

        super.onAttachToController(viewController);
    }

    @Override
    public void saveState(ViewController viewController, Bundle outState) {
        super.saveState(viewController, outState);
        if (mCameraOutFile != null && outState != null)
            outState.putSerializable(SAVED_CAMERA_FILE, mCameraOutFile);
    }

    @Override
    public void restoreState(ViewController viewController, Bundle savedInstanceState) {
        super.restoreState(viewController, savedInstanceState);
        mListener = (Listener) viewController;
        if (savedInstanceState != null && savedInstanceState.containsKey(SAVED_CAMERA_FILE))
            mCameraOutFile = (File) savedInstanceState.getSerializable(SAVED_CAMERA_FILE);
    }

    @Override
    public void onActivityResult(ViewController viewController, final int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK
                && ((requestCode == RequestCodes.REQUEST_PICK_IMAGE && data != null)
                    || requestCode == RequestCodes.REQUEST_PICK_CAMERA)) {

            File pictureFile = null;
            if (requestCode == RequestCodes.REQUEST_PICK_IMAGE) {
                Uri selectedImage = data.getData();
                String[] column = {MediaStore.Images.Media.DATA};
                Cursor cursor = null;
                try {
                    cursor = viewController.getContext().getContentResolver()
                            .query(selectedImage, column, null, null, null);
                }
                catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
                String fileName = null;
                if (cursor != null) {
                    if(cursor.moveToFirst()) {
                        fileName = cursor.getString(cursor.getColumnIndex(column[0]));
                    }
                    cursor.close();

                    if (fileName == null
                            && android.os.Build.VERSION.SDK_INT >= 19
                            && DocumentsContract.isDocumentUri(viewController.getContext(),
                                                                selectedImage)) {

                        String wholeID = DocumentsContract
                                .getDocumentId(selectedImage);

                        String[] ids = wholeID.split(":");
                        if (ids.length >= 2) {
                            String id = ids[1];

                            String sel = MediaStore.Images.Media._ID + "=?";

                            cursor = viewController.getContext().getContentResolver().query(
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                    column, sel, new String[]{id}, null);

                            if (cursor != null) {
                                if (cursor.moveToFirst())
                                    fileName = cursor.getString(cursor.getColumnIndex(column[0]));
                                cursor.close();
                            }
                        }
                    }
                }
                if (fileName != null)
                    pictureFile = new File(fileName);
            } else {
                mListener.onFilePicked(mCameraOutFile, true, requestCode);
                return;
            }

            if (pictureFile != null) {
                mListener.onFilePicked(pictureFile, false, requestCode);
            }
            else {
                mListener.onFileProcessing(requestCode);
                final Context context = viewController.getContext().getApplicationContext();
                new AsyncTask<Uri, Void, File>() {

                    @Override
                    protected File doInBackground(Uri... params) {
                        try {
                            File pictureFile = getTempImageFile();
                            InputStream inputStream = context.getContentResolver()
                                    .openInputStream(params[0]);
                            copy(inputStream, pictureFile);
                            return pictureFile;
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            return null;
                        }
                    }

                    @Override
                    protected void onPostExecute(File file) {
                        if (file != null)
                            mListener.onFilePicked(file, true, requestCode);
                        else
                            mListener.onFilePickError(requestCode);
                    }
                }.execute(data.getData());
            }
        }
    }

    @Override
    public boolean onClickDialog(ViewController viewController, BaseDialogFragment dialogFragment, int which) {
        if (DLG_OPEN_PERMISSION_SETTING.equals(dialogFragment.getTag())) {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + viewController.getContext().getPackageName()));
                viewController.startActivity(appSettingsIntent);
            }
            return true;
        }
        else if (DLG_SELECT_RESOURCE.equals(dialogFragment.getTag())){
            switch (which) {
                case RESOURCE_GALLERY:
                    pickFromGallery();
                    break;
                case RESOURCE_CAMERA:
                    pickFromCamera();
                    break;
            }
            return true;
        }

        return super.onClickDialog(viewController, dialogFragment, which);
    }

    @Override
    public void onPermissionGranted(ViewController viewController, String permission, int requestCode) {
        super.onPermissionGranted(viewController, permission, requestCode);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            if (Manifest.permission.READ_EXTERNAL_STORAGE.equals(permission))
                pickFromGalleryInner();
            else if (Manifest.permission.CAMERA.equals(permission))
                pickFromCameraInner();
        }
    }

    @Override
    public void onPermissionDenied(ViewController viewController, String permission, int requestCode) {
        super.onPermissionDenied(viewController, permission, requestCode);
        if (Manifest.permission.READ_EXTERNAL_STORAGE.equals(permission)
                || Manifest.permission.CAMERA.equals(permission)) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(viewController.getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                String messageText = Manifest.permission.READ_EXTERNAL_STORAGE.equals(permission)
                    ? viewController.getContext()
                        .getString(R.string.dlg_request_permission_external_storage_in_settings)
                    : viewController.getContext()
                        .getString(R.string.dlg_request_permission_camera_in_settings);

                viewController.showDialog(
                        AppController.DIALOG_FRAGMENT_MESSAGE,
                        DLG_OPEN_PERMISSION_SETTING,
                        viewController.getContext()
                                .getString(R.string.dlg_request_permission_title),
                        messageText,
                        viewController.getContext()
                                .getString(R.string.dlg_request_permission_btn_settings),
                        viewController.getContext().getString(android.R.string.cancel)
                );
            }
        }
    }

    public void pick() {
        getController().showDialog(new SelectResourceDialog(), DLG_SELECT_RESOURCE);
    }

    public void pickFromGallery() {
        if (checkPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
            pickFromGalleryInner();
        }
        else {
            getController().requestPermissions(
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    RequestCodes.REQUEST_PERMISSION);
        }
    }

    public void pickFromCamera() {
        if (checkPermission(getContext(), Manifest.permission.CAMERA)) {
            pickFromCameraInner();
        }
        else {
            getController().requestPermissions(new String[]{Manifest.permission.CAMERA},
                    RequestCodes.REQUEST_PERMISSION);
        }
    }

    public static class SelectResourceDialog extends BaseDialogFragment {

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            dialogBuilder.setTitle(R.string.dlg_pick_image_title);
            dialogBuilder.setItems(
                new String[]{
                    getActivity().getString(R.string.dlg_pick_image_option_folder),
                    getActivity().getString(R.string.dlg_pick_image_option_camera)
                },
                this);
            return dialogBuilder.create();
        }
    }


    private void pickFromGalleryInner() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        getController().startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                RequestCodes.REQUEST_PICK_IMAGE); //TODO: to Strings.xml
    }


    private void pickFromCameraInner() {
        mCameraOutFile = getTempImageFile();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (mCameraOutFile != null) {
            Uri outFileUri = HonkioFileProvider.getUriForFile(getContext(), mCameraOutFile);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, outFileUri);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                    | Intent.FLAG_GRANT_READ_URI_PERMISSION);

        }

        getController().startActivityForResult(intent, RequestCodes.REQUEST_PICK_CAMERA);
    }

    /**
     * Create a File for saving an image or video
     */
    private File getTempImageFile() {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(
                getContext().getFilesDir(),
                "temp_images/");

        storageDir.mkdirs();

        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (IOException ex) {
            Log.d("MyCameraApp", "failed to create temp file");
            ex.printStackTrace();
        }

        return image;
    }

    private static boolean copy(InputStream in, File fTo) {
        boolean result = true;
        OutputStream out = null;
        try {
            out = new FileOutputStream(fTo);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        }
        finally {
            try {
                if (in != null)
                    in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (out != null)
                    out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }



    protected boolean checkPermission(Context context, String permission) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
            return true;

        boolean result = ContextCompat.checkSelfPermission(context, permission)
                == PackageManager.PERMISSION_GRANTED;

        // Throw for SDK < VERSION_CODES.M
        // For this versions permission must be requested in manifest
        if (!result && Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            throw new SecurityException("Permission denied (missing "
                                            + permission + " permission?)");

        return result;
    }
}
