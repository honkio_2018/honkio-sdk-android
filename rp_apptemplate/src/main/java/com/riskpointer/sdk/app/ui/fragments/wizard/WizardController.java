package com.riskpointer.sdk.app.ui.fragments.wizard;

public interface WizardController {

    boolean goToNext();
    boolean goToPrevious();

}