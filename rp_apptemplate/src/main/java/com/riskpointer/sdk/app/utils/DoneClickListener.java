/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.utils;

import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;

public abstract class DoneClickListener implements OnKeyListener {

	@Override
	public boolean onKey(View view, int keyCode, KeyEvent event) {
		if ((event.getAction() == KeyEvent.ACTION_DOWN)
				&& (keyCode == KeyEvent.KEYCODE_ENTER)) {
			onDoneClick(view);
			return true;
		}
		return false;
	}

	public abstract void onDoneClick(View view);
}
