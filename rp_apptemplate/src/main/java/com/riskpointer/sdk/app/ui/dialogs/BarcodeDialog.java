package com.riskpointer.sdk.app.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.riskpointer.sdk.app.ui.view.BarcodeView;
import com.riskpointer.sdk.app.R;

/**
 * @author Shurygin Denis on 2015-08-20.
 */
public class BarcodeDialog extends BaseDialogFragment {
    public static final String ARG_TITLE = "BarcodeDialog.ARG_TITLE";
    public static final String ARG_BARCODE = "BarcodeDialog.ARG_BARCODE";

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), getTheme());
        builder.setTitle(getArguments().getString(ARG_TITLE));

        View view = LayoutInflater.from(getContext())
                .inflate(R.layout.hk_dialog_fragment_barcode, null, false);

        builder.setView(view);

        BarcodeView barcodeView = (BarcodeView) view.findViewById(R.id.hk_imgBarcode);
        barcodeView.setBarCodeString(getArguments().getString(ARG_BARCODE));

        return builder.create();
    }

    public static BarcodeDialog build(String title, String barcodeString) {
        Bundle args = new Bundle(2);
        args.putString(ARG_TITLE, title);
        args.putString(ARG_BARCODE, barcodeString);
        BarcodeDialog dialog = new BarcodeDialog();
        dialog.setArguments(args);
        return dialog;
    }
}
