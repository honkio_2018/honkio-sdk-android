package com.riskpointer.sdk.app.ui.deprecated;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.api.model.process.user.AccountCreationProcessModel;
import com.riskpointer.sdk.api.web.callback.SimplePendingCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.textvalidator.BaseTextValidator;
import com.riskpointer.sdk.app.textvalidator.IntNumberTextValidator;
import com.riskpointer.sdk.app.textvalidator.NoEmptyTextValidator;
import com.riskpointer.sdk.app.textvalidator.ValidatorsSet;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;
import com.riskpointer.sdk.app.ui.fragments.BaseFragment;

/**
 * @author dskopa on 2015-07-16.
 */
@Deprecated
public class SettingsAccountAddCreditCardFormFragment extends BaseFragment {

    private static final String TAG = SettingsAccountAddCreditCardFormFragment.class
            .getSimpleName();

    private static final String CARD_TYPE = UserAccount.Type.CREDITCARD;

    private AccountCreationProcessModel mModel;
    private EditText editCardNumber;
    private EditText editCardExpirationMonth;
    private EditText editCardExpirationYear;
    private EditText editCardCVC;

    private BaseTextValidator mCardNumberValidator;
    private BaseTextValidator mCardCvcValidator;
    private ValidatorsSet mMonthValidator;
    private ValidatorsSet mYearValidator;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        final View buttonOk = view.findViewById(R.id.hk_btnOk);
        buttonOk.setOnClickListener(this);

        editCardNumber = (EditText) view.findViewById(R.id.hk_edCardNumber);
        editCardExpirationMonth = (EditText) view.findViewById(R.id.hk_edCardExpirationMonth);
        editCardExpirationYear = (EditText) view.findViewById(R.id.hk_edCardExpirationYear);
        editCardCVC = (EditText) view.findViewById(R.id.hk_edCardCVC);

        NoEmptyTextValidator noEmptyTextValidator = new NoEmptyTextValidator(getString(R.string.text_validator_field_empty));
        mCardNumberValidator = noEmptyTextValidator;
        mMonthValidator = new ValidatorsSet();
        mMonthValidator.add(noEmptyTextValidator);
        mMonthValidator.add(new IntNumberTextValidator(getString(R.string.pay_creditcard_add_dlg_bad_month)) {
            @Override
            protected boolean validateNumber(int number) {
                return number >= 1 && number <= 12 ;
            }
        });

        mYearValidator = new ValidatorsSet();
        mYearValidator.add(noEmptyTextValidator);
        mYearValidator.add(new IntNumberTextValidator(getString(R.string.pay_creditcard_add_dlg_bad_year)));

        mCardCvcValidator = noEmptyTextValidator;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.hk_btnOk) {
//            if (mCardNumberValidator.validate(editCardNumber) &
//                    mMonthValidator.validate(editCardExpirationMonth) &
//                    mYearValidator.validate(editCardExpirationYear) &
//                    mCardCvcValidator.validate(editCardCVC)) {
//                new CreditCallTask().execute();
//            }
        }
    }

    protected String getCardNumber() {
        return editCardNumber.getText().toString();
    }

    protected String getExpirationDate() {
        int month = 0;
        try {
            month = Integer.parseInt(editCardExpirationMonth.getText().toString());
        } catch (NumberFormatException e) {
            return null;
        }
        if (month < 1 || month > 12) {
            return null;
        }

        int year = 0;
        try {
            year = Integer.parseInt(editCardExpirationYear.getText().toString());
        } catch (NumberFormatException e) {
            return null;
        }
        return String.format("%02d", year) + String.format("%02d", month);
    }

    protected String getCardCvc() {
        return editCardCVC.getText().toString();
    }

//    private class CreditCallTask extends AsyncTask<Void, Void, com.creditcall.Response> {
//
//        @Override
//        protected void onPreExecute() {
//            showProgressDialog();
//        }
//
//        @Override
//        protected com.creditcall.Response doInBackground(Void... params) {
//            String cardNumber = getCardNumber();
//            String cardExpiration = getExpirationDate();
//            String cardCVC = getCardCvc();
//
//            // Setup the request
//            Request request = new Request();
//
//            request.setSoftwareName("SoftwareName");
//            request.setSoftwareVersion("SoftwareVersion");
//            request.setTerminalID(RpApi.getServerInfo().getCreditCallInfo().terminalId);
//            request.setTransactionKey(RpApi.getServerInfo().getCreditCallInfo().transactionKey);
//
//            // Setup the request detail
//            request.setRequestType(RequestType.Auth);
//            request.setAmount("0");
//            request.setPAN(cardNumber);
//            request.setExpiryDate(cardExpiration);
//            request.setCSC(cardCVC);
//
//            Log.d(TAG, request.toString());
//
//            // Setup the client
//            Client client = new Client();
//            client.setRequest(request);
//
//            try {
//                client.addServerURL(RpApi.getServerInfo().getCreditCallInfo().url, 45000);
//
//                // Process the request
//                client.processRequest();
//
//            } catch (final CardEaseXMLCommunicationException e) {
//                // There is something wrong with communication
//                Log.e(TAG, "There is something wrong with communication", e);
//                showMessage(null, R.string.dlg_bad_connection_title, R.string.dlg_bad_connection_message);
//                return null;
//            } catch (final CardEaseXMLRequestException e) {
//                // There is something wrong with the request
//                Log.e(TAG, "There is something wrong with the request", e);
//                if (e.getMessage() == "PAN has an invalid format") {
//                    showMessage(null, R.string.dlg_common_title_error, R.string.pay_creditcard_add_dlg_bad_pan);
//                } else {
//                    showMessage(null, getString(R.string.dlg_common_title_error), e.getMessage());
//                }
//                return null;
//            } catch (final CardEaseXMLResponseException e) {
//                // There is something wrong with the response
//                Log.e(TAG, "There is something wrong with the response", e);
//                showMessage(null, getString(R.string.dlg_common_title_error), e.getMessage());
//                showErrorMessageAndFinish();
//                return null;
//            }
//
//            // Get the response
//            com.creditcall.Response response = client.getResponse();
//            Log.d(TAG, response.toString());
//
//            return response;
//        }
//
//        @Override
//        protected void onPostExecute(com.creditcall.Response result) {
//            if(result == null) {
//                dismissProgressDialog();
//                // an error happened: nothing to do
//                return;
//            }
//
//            UserAccount account = new UserAccount(CARD_TYPE, UserAccount.ZERO_NUMBER);
//            account.setCreationParams(result.getCardHash() + ";" + result.getCardReference());
//            account.setDescription(result.getPAN());
//            mModel = new AccountCreationProcessModel(mCardCreationCallback, account);
//            mModel.start(getActivity());
//        }
//
//    }

    private SimplePendingCallback mCardCreationCallback = new SimplePendingCallback() {

        @Override
        public boolean onError(RpError error) {
            return SettingsAccountAddCreditCardFormFragment.this.onError(error);
        }

        @Override
        public void onComplete() {
            if (isAdded()) {
                getActivity().setResult(Activity.RESULT_OK);
                showMessage(BaseActivity.DLG_TAG_FINISH,
                        R.string.pay_creditcard_add_dlg_accept_title,
                        R.string.pay_creditcard_add_dlg_accept_message);
            }
        }

        @SuppressWarnings("rawtypes")
        @Override
        public boolean handlePending(Response response) {
            showErrorMessageAndFinish();
            return true;
        }
    };

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_add_account_credit_card_form;
    }

    @Override
    public void onDestroy() {
        if (mModel != null)
            mModel.abort();
        super.onDestroy();
    }

    @Override
    public boolean onError(RpError error) {
        if (isAdded()) {
            switch (error.code) {
                case RpError.Api.CANCELED_BY_USER:
                    getActivity().finish();
                    return true;
                case RpError.UserPayment.USER_NOT_ACTIVE:
                    showMessage(BaseActivity.DLG_TAG_FINISH,
                            R.string.dlg_need_verify_email_title,
                            R.string.dlg_need_verify_email_message);
                    return true;
                case RpError.Api.INVALID_PIN:
                    if (HonkioApi.isConnected())
                        showMessage(BaseActivity.DLG_TAG_FINISH, 0, R.string.dlg_pin_invalid_message);
                    else
                        super.onError(error);
                    return true;
                default:
                    if (!super.onError(error))
                        showErrorMessageAndFinish();
                    break;
            }
        }
        return true;
    }

    private void showErrorMessageAndFinish() {
        showMessage(BaseActivity.DLG_TAG_FINISH,
                R.string.pay_creditcard_add_dlg_failed_title,
                R.string.pay_creditcard_add_dlg_failed_message);
    }
}
