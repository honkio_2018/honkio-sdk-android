package com.riskpointer.sdk.app.ui.fragments.list;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.ShopFind;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.support.BroadcastManager;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.filter.ShopFilter;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.adapters.BaseRecyclerListAdapter;
import com.riskpointer.sdk.app.ui.adapters.RecyclerShopsListAdapter;
import com.riskpointer.sdk.app.ui.adapters.binder.BaseShopsListBinder;
import com.riskpointer.sdk.app.ui.adapters.binder.ShopsListBinder;

import java.util.List;

/**
 * @author Shurygin Denis
 */

public class BaseShopsListFragment extends BaseRecycleLazyListFragment<Shop> {

    public static final String ARG_FILTER = "ShopsListFragment.ARG_FILTER";

    private static final long DEF_SEARCH_RADIUS = 5000;

    private RecyclerShopsListAdapter mShopAdapter;
    private ShopFilter mFilter;

    private BroadcastReceiver mLocationFindListener;

    @Override
    protected int getDefaultListItemLayoutId(int type) {
        return R.layout.hk_list_item_shop;
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager(Context context) {
        return new LinearLayoutManager(context);
    }

    @Override
    public String getTitle(Context context) {
        return context.getString(R.string.screen_title_shops);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mFilter = (ShopFilter) savedInstanceState.getSerializable(ARG_FILTER);
        } else if (mFilter == null) {
            Bundle args = getArguments();
            if (args != null)
                mFilter = (ShopFilter) args.getSerializable(ARG_FILTER);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setEmptyText(getString(R.string.shops_list_shop_not_found));
    }

    @Override
    protected void reloadList() {
        ShopFilter filter = getFilter();
        if ((filter.getLatitude() != null && filter.getLongitude() != null) || HonkioApi.getLocation() != null) {
            super.reloadList();
        } else {
            onError(new RpError(RpError.Api.ABORTED));
            Toast.makeText(getContext(), "Failed to define location.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(ARG_FILTER, mFilter);
    }

    @Override
    public void onAttach(Context context) {
        BroadcastManager.getInstance(context).registerReceiver(mLocationChangeListener, new IntentFilter(BroadcastHelper.BROADCAST_ACTION_ON_LOCATION_CHANGED));
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        if (mLocationFindListener != null)
            BroadcastManager.getInstance(getActivity()).unregisterReceiver(mLocationFindListener);
        BroadcastManager.getInstance(getActivity()).unregisterReceiver(mLocationChangeListener);
        super.onDetach();
    }

    public ShopFilter getFilter() {
        if (mFilter == null) {
            Bundle args = getArguments();
            if (args != null)
                mFilter = (ShopFilter) args.getSerializable(ARG_FILTER);

            if (mFilter == null) {
                mFilter = new ShopFilter();
                mFilter.setRadius(Long.toString(DEF_SEARCH_RADIUS));
            }

        }
        return mFilter;
    }

    @Override
    public ShopListLoader onCreateLoader(int id, Bundle args) {
        return new ShopListLoader(getActivity().getApplicationContext(), getFilter());
    }

    @Override
    protected BaseRecyclerListAdapter<Shop, ?> newAdapter(List<Shop> list) {
        RecyclerShopsListAdapter adapter = new RecyclerShopsListAdapter<>(getActivity(), list,
                getListItemLayoutId(0), newItemBinder());
        adapter.addOnItemClickListener(new BaseRecyclerListAdapter.OnItemClickListener<Shop, RecyclerView.ViewHolder>() {
            @Override
            public void onItemClick(RecyclerView.ViewHolder itemHolder, View view, Shop shop) {
                openShop(new Identity(shop));
            }
        });

        return adapter;
    }

    protected BaseShopsListBinder<Shop, ?> newItemBinder() {
        return new ShopsListBinder();
    }

    protected void openShop(Identity shopIdentity) {
        startActivity(AppController.getIntent(AppController.SCREEN_SHOP, shopIdentity));
    }

    private BroadcastReceiver mLocationChangeListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mShopAdapter != null) {
                Location location = intent.getParcelableExtra(BroadcastHelper.BROADCAST_EXTRA_LOCATION);
                if (location != null) {
                    mShopAdapter.setLocation(location);
                    mShopAdapter.notifyDataSetChanged();
                }
            }
        }
    };

    @Override
    public void onLoadFinished(Loader<ListLoaderResult<Shop>> loader, ListLoaderResult<Shop> data) {
        super.onLoadFinished(loader, data);
        mShopAdapter = (RecyclerShopsListAdapter) mAdapter;
        if (mShopAdapter != null) {
            mShopAdapter.setLocation(HonkioApi.getLocation());
            mShopAdapter.setServiceTypeVisible(TextUtils.isEmpty(getFilter().getServiceType()));
            mShopAdapter.notifyDataSetChanged();
        }
    }

    public static class ShopListLoader extends ListLoader<Shop, ShopFind> {
        private ShopFilter mFilter;

        public ShopListLoader(Context context, ShopFilter filter) {
            super(context);
            mFilter = filter;
        }

        @Override
        protected List<Shop> toList(Response<ShopFind> response) {
            return response.getResult().getList();
        }

        @Override
        protected Task loadList(Context context, RequestCallback<ShopFind> callback, int count, int skip) {
            if (mFilter == null) {
                mFilter = new ShopFilter();
            }
            mFilter.setCount(count);
            mFilter.setSkip(skip);
            return HonkioApi.shopFind(mFilter, 0, callback);
        }
    }
}
