package com.riskpointer.sdk.app.ui.fragments.list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.PushesList;
import com.riskpointer.sdk.api.model.push.Push;
import com.riskpointer.sdk.api.model.push.entity.SystemMessagePushEntity;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.ListFilter;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.adapters.BaseRecyclerListAdapter;
import com.riskpointer.sdk.app.ui.adapters.PushesListAdapter;

import java.util.List;

/**
 * @author Shurygin Denis on 2015-03-02.
 */
public class PushesListFragment extends BaseRecycleLazyListFragment<Push<?>> {

    @Override
    protected int getDefaultListItemLayoutId(int type) {
        return R.layout.hk_list_item_push;
    }

    @Override
    public String getTitle(Context context) {
        return context.getString(R.string.screen_title_notifications);
    }

    @Override
    public ListLoader<Push<?>, PushesList> onCreateLoader(int id, Bundle args) {
        return new PushesListLoader(getContext(), new ListFilter());
    }

    @Override
    protected BaseRecyclerListAdapter<Push<?>, ?> newAdapter(List<Push<?>> list) {
        PushesListAdapter adapter = new PushesListAdapter(getContext(), list, getListItemLayoutId(0));

        adapter.addOnItemClickListener(new BaseRecyclerListAdapter.OnItemClickListener<Push<?>, PushesListAdapter.ViewHolder>() {
            @Override
            public void onItemClick(PushesListAdapter.ViewHolder itemHolder, View view, Push<?> push) {
                onListItemClick(itemHolder, view, push);
            }
        });

        return adapter;
    }

    protected void onListItemClick(PushesListAdapter.ViewHolder itemHolder, View view, Push<?> push) {
        if (push != null) {
            switch (push.getType()) {
                case Push.Type.SYSTEM:
                    SystemMessagePushEntity entity = (SystemMessagePushEntity) push.getEntity();
                    showMessage(null, entity.getSubject(), entity.getMessage());
                    break;
                case Push.Type.TEST:
                    showMessage(null, "Test push", push.getMessage() != null ? push.getMessage() : "Message is null!");
                    break;
                case Push.Type.NONE:
                    showMessage(null, "Unknown push",
                            (push.getMessage() != null ? "Message:" + push.getMessage() : "Message is null!")+
                            "\n\nBody:\n" + push.getBody());
                    break;
                default:
                    Intent intent = AppController.getIntent(AppController.SCREEN_PUSH, push);
                    if (intent != null)
                        startActivity(intent);
                    break;
            }
        }
    }

    protected static class PushesListLoader extends ListLoader<Push<?>, PushesList> {
        private ListFilter mFilter;

        public PushesListLoader(Context context, ListFilter filter) {
            super(context);
            mFilter = filter;
        }

        @Override
        protected List<Push<?>> toList(Response<PushesList> response) {
            return response.getResult().getList();
        }

        @Override
        protected Task loadList(Context context, RequestCallback<PushesList> callback, int count, int skip) {
            if (mFilter == null) {
                mFilter = new ListFilter();
            }
            mFilter.setCount(count);
            mFilter.setSkip(skip);
            return HonkioApi.userGetPushes(mFilter, 0, callback);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.hk_push_list_options_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.hk_action_mark_all_as_read) {
            markAllAsRead();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void markAllAsRead() {
        List<Push<?>> list = getList();
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setRead(true);
        }
        this.getListAdapter().notifyDataSetChanged();
        HonkioApi.userSetPush(true, null, Message.FLAG_NO_WARNING, null);
    }

}
