/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;

import com.riskpointer.sdk.api.model.entity.ServerInfo.Country;
import com.riskpointer.sdk.app.utils.Countries;

import java.util.List;

public class CountriesSpinAdapter extends BaseSpinAdapter<Country> {

	public CountriesSpinAdapter(Context context, List<Country> list, int layoutId) {
		super(context, list, layoutId);
	}

	public int findCountryByIso(String iso) {
		if (iso == null)
			return -1;

		String iso3 = iso.length() == 3 ? iso : Countries.ISO2_TO_ISO3_MAP.get(iso);
		if (iso3 != null) {
			for (int i = 0; i < getCount(); i++) {
				if (getItem(i).getIso().equals(iso3)) {
					return i;
				}
			}
		}
		else if (iso.length() == 2) {
			for(int i = 0; i < getCount(); i++) {
				String itemIso = getItem(i).getIso();
				if (itemIso.length() >= 2
						&& itemIso.charAt(0) == iso.charAt(0)
						&& itemIso.charAt(1) == iso.charAt(1)) {
					return i;
				}
			}
		}
		return -1;
	}
	
	public int findCountryByLocale(String locale) {
		for(int i = 0; i < getCount(); i++) {
			if(getItem(i).getLocale().equals(locale))
				return i;
		}
		return -1;
	}

	@Override
	protected String objectToString(Country country) {
		return country.getName();
	}
}
