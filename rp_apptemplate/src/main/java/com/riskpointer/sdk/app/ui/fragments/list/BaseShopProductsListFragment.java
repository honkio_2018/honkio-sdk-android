package com.riskpointer.sdk.app.ui.fragments.list;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Product;
import com.riskpointer.sdk.api.model.entity.ShopProducts;
import com.riskpointer.sdk.api.web.callback.ErrorCallback;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.adapters.BaseArrayListAdapter;
import com.riskpointer.sdk.app.ui.adapters.ShopProductsListAdapter;
import com.riskpointer.sdk.app.utils.FragmentUtils;

/**
 * Created by Shurygin Denis on 2016-04-04.
 */
public abstract class BaseShopProductsListFragment extends BaseListFragment implements RequestCallback<ShopProducts> {
    private BaseArrayListAdapter<Product, ?> mAdapter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setEmptyText(getString(R.string.product_list_empty));
        refresh(getActivity());
    }

    @Override
    public boolean onError(RpError error) {
        Activity activity = getActivity();
        if(activity != null) {
            setListShown(true);
            if(activity instanceof ErrorCallback)
                return ((ErrorCallback) activity).onError(error);
        }
        return false;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public boolean onComplete(Response<ShopProducts> response) {
        if(getActivity() != null) {
            if (response.isStatusAccept()) {
                mAdapter = createAdapter(response.getResult());
                setListAdapter(mAdapter);
                setListShown(true);
            }
            else
                return onError(response.getAnyError());
        }
        return false;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        if(mAdapter != null) {
            onProductClick(mAdapter.getItem(position - listView.getHeaderViewsCount()), position);
        }
    }

    public void refresh(Context context) {
        setListShown(false);

        HonkioApi.shopGetProducts(null,
                FragmentUtils.getShopIdentity(this, null), 0, this);
    }

    protected void onProductClick(Product product, int position) {

    }

    protected ShopProductsListAdapter createAdapter(ShopProducts list) {
        return new ShopProductsListAdapter(getActivity(), list, getListItemLayoutId(0));
    }
}
