package com.riskpointer.sdk.app.ui.fragments.list;

import android.content.Context;
import android.os.Bundle;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.Transactions;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.message.filter.TransactionFilter;
import com.riskpointer.sdk.app.utils.FragmentUtils;

/**
 * @author Shurygin Denis on 2015-04-10.
 */
public class TransactionsListUserFragment extends TransactionsListFragment {

    @Override
    public TransactionsListLoader onCreateLoader(int id, Bundle args) {
        return new UserTransactionsListLoader(getActivity().getApplicationContext(), FragmentUtils.getShop(this), getFilter());
    }

    public static class UserTransactionsListLoader extends TransactionsListLoader {

        public UserTransactionsListLoader(Context context, Shop shop, TransactionFilter filter) {
            super(context, shop, filter);
        }

        @Override
        protected Task doRequest(Context context, RequestCallback<Transactions> callback, Shop shop, TransactionFilter filter) {
            if (shop == null)
                return HonkioApi.userGetTransactions(filter, 0, callback);
            else
                return HonkioApi.userGetTransactions(filter, shop, 0, callback);
        }

    }
}
