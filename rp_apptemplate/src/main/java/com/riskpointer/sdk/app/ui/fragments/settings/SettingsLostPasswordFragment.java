/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.fragments.settings;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.textvalidator.EmailTextValidator;
import com.riskpointer.sdk.app.textvalidator.NoEmptyTextValidator;
import com.riskpointer.sdk.app.textvalidator.ValidatorsSet;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;
import com.riskpointer.sdk.app.ui.activities.SingleFragmentActivity;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.ui.fragments.BaseFragment;
import com.riskpointer.sdk.app.utils.DoneClickListener;

public class SettingsLostPasswordFragment extends BaseFragment implements BaseDialogFragment.OnDismissListener, SingleFragmentActivity.SingleFragment {
    public static final String ARG_EMAIL = "SettingsLostPasswordFragment.ARG_EMAIL";

    private EditText mEditEmail;
    private ValidatorsSet mEmailValidator;

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_dialog_fragment_lost_password;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView(view);
        view.findViewById(R.id.hk_btnNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPositiveButtonClick();
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mEmailValidator = new ValidatorsSet();
        mEmailValidator.add(new EmailTextValidator(getResources().getString(R.string.text_validator_invalid_email)));
        mEmailValidator.add(new NoEmptyTextValidator(getResources().getString(R.string.text_validator_field_empty)));
    }

    private void setupView(View view) {
        mEditEmail = (EditText) view.findViewById(R.id.hk_edEmail);
        mEditEmail.setKeyListener(null);
        mEditEmail.setOnKeyListener(new DoneClickListener() {

            @Override
            public void onDoneClick(View view) {
                onPositiveButtonClick();
            }
        });

        readArguments();
    }


    private void onPositiveButtonClick() {
        User user = HonkioApi.getActiveUser();
        if (user != null)
            requestNewPassword(user.getLogin());
        else {
            if (mEmailValidator.validate(mEditEmail))
                requestNewPassword(mEditEmail.getText().toString());
        }
    }

    private void requestNewPassword(String email) {
        showProgressDialog();
        RequestCallback<Void> callback = new RequestCallback<Void>() {

            @Override
            public boolean onError(final RpError error) {
                BaseActivity activity = (BaseActivity) getActivity();
                dismissProgressDialog();
                if (error.code == RpError.Common.INVALID_USER_LOGIN) {
                    showMessage(null, 0, R.string.dlg_account_not_found_message);
                } else if (activity == null || !(activity).onError(error)) {
                    showMessage(null, 0, R.string.lost_password_dlg_failed_message);
                }
                return true;
            }

            @Override
            public boolean onComplete(Response<Void> response) {
                if (response.isStatusAccept()) {
                    dismissProgressDialog();
                    showMessage(BaseActivity.DLG_TAG_SUCCESS, 0, R.string.lost_password_dlg_success_message);
                } else
                    return onError(response.getAnyError());
                return true;
            }
        };
        HonkioApi.userLostPassword(email, 0, callback);
    }

    private void readArguments() {
        Bundle args = getArguments();
        if (args != null) {
            String emailString = args.getString(ARG_EMAIL);
            if (emailString != null && mEditEmail != null)
                mEditEmail.setText(emailString);
        }
    }

    @Override
    public String getTitle(Context context) {
        return context.getString(R.string.lost_password_title);
    }
}
