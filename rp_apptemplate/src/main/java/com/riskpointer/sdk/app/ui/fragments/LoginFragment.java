package com.riskpointer.sdk.app.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.model.process.LoginProcessModel;
import com.riskpointer.sdk.api.model.process.user.UserLoginProcessModel;
import com.riskpointer.sdk.api.model.push.Push;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.model.task.TaskWrapper;
import com.riskpointer.sdk.api.support.HoneycombAsyncTask;
import com.riskpointer.sdk.api.utils.ApiDebugUtils;
import com.riskpointer.sdk.api.web.Connection;
import com.riskpointer.sdk.api.web.callback.ErrorCallback;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.callback.SimpleCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.utils.AppPreferences;
import com.riskpointer.sdk.app.utils.RequestCodes;
import com.riskpointer.sdk.app.utils.uri.UriHandler;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.riskpointer.sdk.app.ui.activities.BaseActivity.DLG_TAG_BAD_RESPONSE;
import static com.riskpointer.sdk.app.ui.activities.BaseActivity.DLG_TAG_FINISH;
import static com.riskpointer.sdk.app.ui.activities.BaseActivity.DLG_TAG_INVALID_PIN;
import static com.riskpointer.sdk.app.ui.activities.BaseActivity.DLG_TAG_INVALID_SECURITY;
import static com.riskpointer.sdk.app.ui.activities.BaseActivity.DLG_TAG_NO_CONNECTION;

/**
 * Created by Shurygin Denis on 2017-02-20.
 */

public class LoginFragment extends BaseFragment implements ErrorCallback, OAuthWebFragment.OAuth2Callbacks {

    public static final String EXTRA_ACTION_COMMAND = "LoginFragment.ACTION_COMMAND";
    public static final String EXTRA_COMMAND_PAYLOAD = "LoginFragment.COMMAND_PAYLOAD";
    public static final String EXTRA_SPLASH_DELAY = "LoginFragment.SPLASH_DELAY";
    public static final String EXTRA_FORCE_LOGIN_SCREEN = "LoginFragment.EXTRA_FORCE_LOGIN_SCREEN";

    public static final String COMMAND_PUSH = "command_push";
    public static final String COMMAND_URI = "command_uri";

    public static final String DLG_TAG_GO_TO_LOGIN = "LoginFragment.DLG_TAG_GO_TO_LOGIN";
    public static final String DLG_TAG_ERROR = "LoginFragment.DLG_TAG_ERROR";
    public static final String DLG_TAG_EXIT = "LoginFragment.DLG_TAG_EXIT";
    private static final String DLG_TAG_INACTIVE_USER = "LoginFragment.DLG_TAG_INACTIVE_USER";

    private static final String SAVED_LOGIN_MODEL = "LoginFragment.SAVED_LOGIN_MODEL";

    private static final int REQUEST_PIN = 1;

    private LoginProcessModel<User> mLoginProcessModel;

    private SharedPreferences mAppPreferences;
    private TaskWrapper mAbortableProcess;

    private volatile int mNextScreenCountDown = 2;
    private DelayedLoginScreenShow mDelayedLoginScreenShow;

    private boolean isDestroyed = false;
    private boolean isInitializationStarted = false;
    private boolean isLoginScreenShow = false;

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_login;
    }

    @Override
    public String getTitle(Context context) {
        return null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAppPreferences = getActivity().getSharedPreferences(AppPreferences.PREFERENCES_FILE, MODE_PRIVATE);

        mLoginProcessModel = buildLoginProcessModel(mLoginCallback);
        if (savedInstanceState != null) {
            mLoginProcessModel.restoreInstanceState(getContext(), savedInstanceState.getBundle(SAVED_LOGIN_MODEL));
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setTextIfExists(R.id.hk_txtAppInfo, AppController.getInstance().appUrl);
        setVisibilityIfExists(R.id.hk_txtAppInfo, ApiDebugUtils.isDebuggable(getContext()) ? View.VISIBLE : View.GONE);

        if (checkLocationPermissions())
            startInitialization();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle(SAVED_LOGIN_MODEL, mLoginProcessModel.saveInstanceState());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == REQUEST_PIN) {
            if (resultCode == RESULT_OK) {
                String pin = intent.getStringExtra(PinFragment.EXTRA_PIN);
                if((mAppPreferences.getBoolean(AppPreferences.PREF_SAVE_PIN, AppPreferences.PREF_SAVE_PIN_DEFAULT)) && pin != null) {
                    HonkioApi.getApiPreferences().edit()
                            .putString(AppPreferences.PREF_PIN, pin).apply();
                }
                mLoginProcessModel.onPinEntered(
                        getContext(), pin,
                        intent.getLongArrayExtra(PinFragment.EXTRA_DELAYS),
                        intent.getIntExtra(PinFragment.EXTRA_REASON, 0));
            }
            else {
                mLoginProcessModel.onPinCanceled(
                        getContext(), intent.getIntExtra(PinFragment.EXTRA_REASON, 0));
            }
        }
        else if (requestCode == RequestCodes.TOU_UPDATE) {
            if (resultCode == Activity.RESULT_OK) {
                showMainScreen();
            } else {
                showProgressDialog();
                HonkioApi.logout(0, new SimpleCallback() {
                    @Override
                    public void onComplete() {
                        dismissProgressDialog();
                        onError(new RpError(RpError.Api.CANCELED_BY_USER));
                    }

                    @Override
                    public boolean onError(RpError error) {
                        dismissProgressDialog();
                        if (!LoginFragment.this.onError(error))
                            showErrorMessage(null, error);
                        return true;
                    }
                });
            }
        }
    }

    @Override
    public void onDestroy() {
        isDestroyed = true;
        if (mDelayedLoginScreenShow != null)
            mDelayedLoginScreenShow.interrupt();
        if (getActivity().isFinishing()) {
            if (mAbortableProcess != null)
                mAbortableProcess.abort();
        }
        super.onDestroy();
    }

    @Override
    public boolean onBackPressed() {
        Fragment childFragment = getChildFragmentManager().findFragmentById(R.id.hk_content);
        if (childFragment instanceof BaseFragment && ((BaseFragment) childFragment).onBackPressed())
            return true;
        return super.onBackPressed();
    }

    @Override
    public boolean onClickDialog(BaseDialogFragment dialogFragment, int which) {
        String tag = dialogFragment.getTag();
        if (tag != null) {
            switch (tag) {
                case DLG_TAG_EXIT:
                    if (which == DialogInterface.BUTTON_POSITIVE)
                        finish();
                    return true;
                default:
                    return super.onClickDialog(dialogFragment, which);

            }
        }
        return true;
    }

    @Override
    public boolean onDismissDialog(BaseDialogFragment dialogFragment) {
        String tag = dialogFragment.getTag();
        if (tag != null)
            switch (tag) {
                case DLG_TAG_GO_TO_LOGIN:
                    tryShowNextScreen();
                    return true;
                case DLG_TAG_INVALID_PIN:
                    mLoginProcessModel.reset();
                    mAbortableProcess.set(new LoginTask());
                    mLoginProcessModel.login(getContext(), 0);
                    return true;
                case DLG_TAG_NO_CONNECTION:
                case DLG_TAG_BAD_RESPONSE:
                case DLG_TAG_INVALID_SECURITY:
                case DLG_TAG_ERROR:
                    if (!HonkioApi.isInitialised()) {
                        finish();
                        startActivity(getActivity().getIntent());
                    }
                    else {
                        tryShowNextScreen();
                    }
                    return true;
                case DLG_TAG_INACTIVE_USER:
                    onLoginComplete(HonkioApi.getActiveUser());
                    return true;

            }
        return super.onDismissDialog(dialogFragment);
    }

    @Override
    public boolean handleError(RpError error) {
        dismissProgressDialog();
        switch (error.code) {
            case RpError.Api.CACHE_NOT_FOUND:
            case RpError.Api.CACHE_NOT_SUPPORTED:
                showMessage(DLG_TAG_FINISH, 0, R.string.dlg_offline_mode_fail_message);
                break;
            case RpError.Api.ABORTED:
                finish();
                break;
            case RpError.Common.INVALID_USER_LOGIN:
                if (isLoginScreenShow)
                    showMessage(null, R.string.dlg_login_failed_title, R.string.dlg_login_failed_message);
                else
                    tryShowNextScreen();
                break;
            case RpError.Api.CANCELED_BY_USER:
            case RpError.Api.NO_LOGIN:
                tryShowNextScreen();
                break;
            case RpError.Api.INVALID_PIN:
                if(mAppPreferences.getBoolean(AppPreferences.PREF_SAVE_PIN, AppPreferences.PREF_SAVE_PIN_DEFAULT)) {
                    HonkioApi.getActiveConnection().setToInvalid();
                    tryShowNextScreen();
                }
                else {
                    long pinFails = HonkioApi.getApiPreferences().getLong(Connection.PIN_FAILS, 0);
                    if(pinFails >= Connection.MAX_PIN_FAIL_COUNT) {
                        if(HonkioApi.isOnOfflineMode())
                            // FIXME API Offline mode not supported
                            showMessage(DLG_TAG_FINISH, 0, R.string.dlg_pin_offline_logout_message);
                        else
                            showMessage(DLG_TAG_GO_TO_LOGIN, 0, R.string.dlg_pin_logout_message);
                    }
                    else {
                        showMessage(DLG_TAG_INVALID_PIN, 0, R.string.dlg_pin_error_message);
                    }
                }
                break;
            default:
                if (!super.handleError(error)) {
                    showErrorMessage(DLG_TAG_ERROR, error);
                    return false;
                }
        }
        return true;
    }

    @Override
    public void onAuthorisationNotAllowed() {
        // Do nothing
    }

    @Override
    public void onTokenGet(String token) {
        showProgressDialog();
        mLoginProcessModel.reset();
        mLoginProcessModel.loginWithOAuthToken(getContext(), token, 0);
    }

    @Override
    protected void onPermissionDenied(String permission, int requestCode) {
        startInitialization();
    }

    @Override
    protected void onPermissionGranted(String permission, int requestCode) {
        startInitialization();
    }

    public void onRequestPin(int reason) {
        BaseActivity.startPinActivity(this, reason, REQUEST_PIN);
    }

    protected boolean handleIntentActions(Intent intent) {
        final Bundle extras = intent.getExtras();
        if (extras != null) {
            String command = extras.getString(EXTRA_ACTION_COMMAND);
            if (command != null) {
                if (COMMAND_PUSH.equals(command)) {
                    Object payload = extras.getSerializable(EXTRA_COMMAND_PAYLOAD);
                    if (payload instanceof Push<?>) {
                        Push<?> push = (Push<?>) payload;
                        switch (push.getType()) {
                            case Push.Type.TEST:
                                startActivity(AppController.getIntent(AppController.ACTIVITY_MAIN));
                                finish();
                                break;
                            default:
                                startActivity(AppController.getIntent(AppController.ACTIVITY_MAIN));
                                startActivity(AppController.getIntent(AppController.SCREEN_PUSH, push));
                                finish();
                                break;
                        }
                        return true;
                    }
                } else if (COMMAND_URI.equals(command)) {
                    Uri payload = extras.getParcelable(EXTRA_COMMAND_PAYLOAD);
                    if (payload != null) {
                        UriHandler uriHandler = buildUriHandler();
                        if (uriHandler != null)
                            return uriHandler.handle(payload);
                    }
                    return false;
                }

                throw new IllegalArgumentException("Unknown command: " + command
                        + ", payload=" + extras.getSerializable(EXTRA_COMMAND_PAYLOAD));
            }
        }
        return false;
    }

    protected LoginProcessModel<User> buildLoginProcessModel(RequestCallback<User> callback) {
        return new UserLoginProcessModel(callback) {
            @Override
            public void onRequestPin(Context context, int reason) {
                LoginFragment.this.onRequestPin(reason);
            }
        };
    }

    protected void onInitializationComplete() {
        if (isAdded()&& !mAbortableProcess.isAborted())
        if(!mAbortableProcess.isAborted()) {
            if (getActivity().getIntent().getBooleanExtra(EXTRA_FORCE_LOGIN_SCREEN, false)) {
                showLoginScreen();
            }
            else {
                mLoginProcessModel.reset();
                SharedPreferences apiPrefs = HonkioApi.getApiPreferences();
                if (mAppPreferences.getBoolean(AppPreferences.PREF_SAVE_PIN, AppPreferences.PREF_SAVE_PIN_DEFAULT)
                        && apiPrefs.contains(AppPreferences.PREF_PIN) && apiPrefs.contains(Connection.PASSWORD)) {
                    mLoginProcessModel.login(getContext(), HonkioApi.getApiPreferences().getString(AppPreferences.PREF_PIN, ""), 0);
                } else {
                    mLoginProcessModel.login(getContext(), 0);
                }
                mAbortableProcess.set(new LoginTask());
            }
        }
    }

    protected void onLoginComplete(User user) {
        Activity activity = getActivity();
        if (activity != null) {
            if (user != null && user.isTouVersionObsolete(HonkioApi.getAppInfo())) {
                Intent intent = AppController.getIntent(
                        AppController.ACTIVITY_TOU,
                        TermsOfUseFragment.REASON_NEW_VERSION, null,
                        getString(R.string.terms_of_use_title),
                        getString(R.string.terms_of_use_new_version_message)
                );
                startActivityForResult(intent, RequestCodes.TOU_UPDATE);
            } else {
                showMainScreen();
            }
        }
    }

    protected void showNextScreen() {
        if (getActivity().getIntent().getBooleanExtra(EXTRA_FORCE_LOGIN_SCREEN, false)
                || !AppController.getInstance().isGuestModeAvailable())
            showLoginScreen();
        else
            showMainScreen();
    }

    protected void showMainScreen() {
        Activity activity = getActivity();
        if (activity != null && !handleIntentActions(activity.getIntent())) {
            if (activity.getCallingActivity() == null)
                BaseActivity.startMainActivity(activity);
            finish(Activity.RESULT_OK);
        }
    }

    protected void showLoginScreen() {
        if (!isLoginScreenShow) {
            isLoginScreenShow = true;
            Fragment loginFragment = new OAuthWebFragment();

            Bundle args = new Bundle();
            args.putString(OAuthWebFragment.CLIENT_ID,
                    getArguments().getString(OAuthWebFragment.CLIENT_ID));
            args.putString(OAuthWebFragment.CLIENT_SECRET,
                    getArguments().getString(OAuthWebFragment.CLIENT_SECRET));

            loginFragment.setArguments(args);
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(R.id.hk_content, loginFragment, OAuthWebFragment.class.getName())
                    .commitAllowingStateLoss();

            setVisibilityIfExists(R.id.hk_splash, View.GONE);
        }
    }

    protected void showInactiveMessage() {
        showMessage(DLG_TAG_INACTIVE_USER, R.string.dlg_inactive_user_title,
                R.string.dlg_inactive_user_message);
    }

    protected UriHandler buildUriHandler() {
        return null;
    }

    protected long getSplashDelay() {
        return getActivity().getIntent().getLongExtra(EXTRA_SPLASH_DELAY, 2000L);
    }

    private RequestCallback<User> mLoginCallback = new RequestCallback<User>() {

        @Override
        public boolean onError (RpError error){
            return LoginFragment.this.onError(error);
        }

        @Override
        public boolean onComplete ( final Response<User> response){
            if (response.isStatusAccept()) {
                if (!response.getResult().isActive())
                    showInactiveMessage();
                else
                    onLoginComplete(response.getResult());
                return true;
            } else if (response.isStatusPending()) {
                if (response.hasPending(Response.Pending.EMAIL_VERIFY)) {
                    showMessage(DLG_TAG_INACTIVE_USER, R.string.dlg_need_verify_email_title,
                            R.string.dlg_need_verify_email_message);
                    return true;
                } else if (!response.getResult().isActive())
                    showInactiveMessage();
            }
            return onError(response.getAnyError());
        }
    };

    private boolean checkLocationPermissions() {
        if (!checkRuntimePermission(ACCESS_FINE_LOCATION)) {
                requestPermissions(new String[]{ACCESS_FINE_LOCATION},
                        RequestCodes.REQUEST_PERMISSION);
            return false;
        }
        return true;
    }

    private void startInitialization() {
        if (!isInitializationStarted) {
            isInitializationStarted = true;
            if (HonkioApi.isConnected() && handleIntentActions(getActivity().getIntent())) {
                // Do nothing
            } else {
                mDelayedLoginScreenShow = new DelayedLoginScreenShow(getSplashDelay());
                mDelayedLoginScreenShow.start();

                mAbortableProcess = new TaskWrapper();
                if (!HonkioApi.isInitialised()) {
                    Task process = HonkioApi.loadServerData(0, new SimpleCallback() {
                        @Override
                        public void onComplete() {
                            onInitializationComplete();
                        }

                        @Override
                        public boolean onError(RpError error) {
                            return LoginFragment.this.onError(error);
                        }
                    });

                    if (process != null)
                        mAbortableProcess.set(process);
                } else {
                    onInitializationComplete();


                    // Bug workaround.
                    // Execute empty HoneycombAsyncTask to initialize static variables.
                    new HoneycombAsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) { return null; }
                    }.execute();

                    new Thread() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(5000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            HonkioApi.loadServerData(Message.FLAG_LOW_PRIORITY
                                    + Message.FLAG_NO_WARNING, null);
                        }
                    }.start();
                }
            }
        }
    }

    private void runOnUiThread(Runnable action) {
        Activity activity = getActivity();
        if (activity != null)
            activity.runOnUiThread(action);
    }

    private void tryShowNextScreen() {
        if (!isDestroyed) {
            synchronized (mDelayedLoginScreenShow) {
                --mNextScreenCountDown;
                if (mNextScreenCountDown == 0) {
                    showNextScreen();
                }
            }
        }
    }

    private class DelayedLoginScreenShow extends Thread {

        private long mDelay;

        public DelayedLoginScreenShow(long delay) {
            mDelay = delay;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(mDelay);
            } catch (InterruptedException e) {
                return;
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tryShowNextScreen();
                }
            });
        }
    }


    private class LoginTask implements Task {

        @Override
        public boolean abort() {
            return mLoginProcessModel != null && mLoginProcessModel.abort();
        }

        @Override
        public boolean isAborted() {
            return mLoginProcessModel == null || mLoginProcessModel.isAborted();
        }

        @Override
        public boolean isCompleted() {
            return mLoginProcessModel != null && mLoginProcessModel.isCompleted();
        }
    }
}