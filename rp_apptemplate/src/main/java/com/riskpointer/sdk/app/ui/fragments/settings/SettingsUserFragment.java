package com.riskpointer.sdk.app.ui.fragments.settings;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.ServerInfo;
import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.api.web.webtask.MultiPartWebTask;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.textvalidator.NoEmptyTextValidator;
import com.riskpointer.sdk.app.ui.adapters.CountriesSpinAdapter;
import com.riskpointer.sdk.app.ui.fragments.BaseFragment;
import com.riskpointer.sdk.app.ui.strategy.PicturePickerViewStrategy;
import com.riskpointer.sdk.app.utils.ImageUtils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

/**
 * @author Shurygin Denis on 2015-04-08.
 */
public class SettingsUserFragment extends BaseFragment implements PicturePickerViewStrategy.Listener {
    private static final String COUNTRY_FINLAND = "FIN";
    private static final String DEFAULT_COUNTRY = COUNTRY_FINLAND;

    private static final String LCO_TAG_PICTURE_PICKER = "SettingsUserFragment.LCO_TAG_PICTURE_PICKER";

    private static final int PICTURE_SIZE = 200;

    private Spinner mCountry;
    private EditText mAddress;
    private EditText mPostCode;
    private EditText mCity;
    private EditText mPhone;
    private EditText mEmail;
    private EditText mFirstName;
    private EditText mLastName;
    private ImageView mPhotoImage;

    private NoEmptyTextValidator mNoEmptyTextValidator;

    private PicturePickerViewStrategy mPicturePicker;

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_settings_address;
    }

    @Override
    public String getTitle(Context context) {
        return context.getString(R.string.screen_title_settings_user);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPicturePicker = getLifeCycleObserver(LCO_TAG_PICTURE_PICKER);
        if (mPicturePicker == null) {
            mPicturePicker = new PicturePickerViewStrategy();
            addLifeCycleObserver(mPicturePicker);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(HonkioApi.isConnected()) {

            mCountry = (Spinner) view.findViewById(R.id.hk_spinCountry);
            mAddress = (EditText) view.findViewById(R.id.hk_edAddress);
            mPostCode = (EditText) view.findViewById(R.id.hk_edPostNumber);
            mCity = (EditText) view.findViewById(R.id.hk_edCity);
            mPhone = (EditText) view.findViewById(R.id.hk_edPhone);
            mEmail = (EditText) view.findViewById(R.id.hk_edEmail);
            mFirstName = (EditText) view.findViewById(R.id.hk_edFirstName);
            mLastName = (EditText) view.findViewById(R.id.hk_edLastName);
            mPhotoImage = (ImageView) view.findViewById(R.id.hk_imgUserPhoto);

            mNoEmptyTextValidator = new NoEmptyTextValidator(getString(R.string.text_validator_field_empty));

            CountriesSpinAdapter countriesAdapter = new CountriesSpinAdapter(getActivity(),
                    HonkioApi.getServerInfo().getCountries(), R.layout.hk_list_item_spinner);
            mCountry.setAdapter(countriesAdapter);

            User user = HonkioApi.getActiveUser();
            if(user != null) {
                mAddress.setText(user.getAddress1());
                mPostCode.setText(user.getZip());
                mCity.setText(user.getCity());
                mEmail.setText(user.getLogin());
                mFirstName.setText(user.getFirstName());
                mLastName.setText(user.getLastName());
                mPhone.setText(user.getPhone());

                loadUserPhoto(mPhotoImage);

                int countryIndex = countriesAdapter.findCountryByIso(user.getCountry());
                if(countryIndex >= 0)
                    mCountry.setSelection(countryIndex);
                else {
                    Locale current = getResources().getConfiguration().locale;
                    countryIndex = countriesAdapter.findCountryByLocale(current.toString());
                    if(countryIndex >= 0)
                        mCountry.setSelection(countryIndex);
                    else {
                        countryIndex = countriesAdapter.findCountryByIso(DEFAULT_COUNTRY);
                        if(countryIndex >= 0)
                            mCountry.setSelection(countryIndex);
                        else
                            mCountry.setSelection(0);
                    }
                }
            }

            View btnOk = view.findViewById(R.id.hk_btnOk);
            if (btnOk != null)
                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String country = ((ServerInfo.Country) mCountry.getSelectedItem()).getIso();
                        if(mNoEmptyTextValidator.validate(mFirstName)
                                & mNoEmptyTextValidator.validate(mLastName)
                                & mNoEmptyTextValidator.validate(mPhone)) {
                            User user = new User();
                            user.setCountry(country);
                            user.setAddress1(mAddress.getText().toString());
                            user.setZip(mPostCode.getText().toString());
                            user.setCity(mCity.getText().toString());
                            user.setPhone(mPhone.getText().toString());
                            user.setFirstName(mFirstName.getText().toString());
                            user.setLastName(mLastName.getText().toString());
                            user.setPhone(mPhone.getText().toString());


                            HonkioApi.userUpdate(user, 0, mUserSetCallback);
                            showProgressDialog();
                        }
                    }
                });

            View btnPickImage = view.findViewById(R.id.hk_btnSetPhoto);
            if (btnPickImage != null) {
                btnPickImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPicturePicker.pick();
                    }
                });
            }
        }
    }

    private RequestCallback<User> mUserSetCallback = new RequestCallback<User>() {

        @Override
        public boolean onError(RpError error) {
            dismissProgressDialog();
            return SettingsUserFragment.this.onError(error);
        }

        @Override
        public boolean onComplete(Response<User> response) {
            if (response.isStatusAccept()) {
                finish();
                return true;
            }
            else
                return onError(response.getAnyError());
        }
    };

    @Override
    public boolean onError(RpError error) {
        dismissProgressDialog();
        if(!super.onError(error))
            showErrorMessage(null, error);
        return true;
    }



    @Override
    public void onFilePicked(final File file, final boolean isTemp, int request) {
        dismissProgressDialog();
        Picasso.with(getContext())
                .load(file)
                .fit()
                .centerCrop()
                .transform(ImageUtils.USER_PICTURE_TRANSFORMATION)
                .into(mPhotoImage);

        // Scale picture before uploading in parallel thread
        new AsyncTask<File, Void, InputStream>() {

            RequestCallback<Void> mCallback = new RequestCallback<Void>() {
                @Override
                public boolean onComplete(Response<Void> response) {
                    if (isTemp)
                        file.delete();
                    return response.isStatusAccept();
                }

                @Override
                public boolean onError(RpError error) {
                    if (isTemp)
                        file.delete();
                    return false;
                }
            };

            @Override
            protected InputStream doInBackground(File... params) {
                InputStream inputStream = null;
                try {
                    inputStream = scaledBitmapInputStream(getContext(), params[0], PICTURE_SIZE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return inputStream;
            }

            @Override
            protected void onPostExecute(InputStream inputStream) {
                if (inputStream != null)
                    HonkioApi.userUpdatePhoto(inputStream, MultiPartWebTask.MEDIA_TYPE_IMAGE_JPEG,
                            0, mCallback);
            }

            private InputStream scaledBitmapInputStream(Context pContext, File file, int pSize) throws IOException {

                ExifInterface exif = new ExifInterface(file.getAbsolutePath());
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

                BitmapFactory.Options opts = new BitmapFactory.Options();
                opts.inSampleSize = 4; // reduce memory consumption
                Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), opts);

                DisplayMetrics metrics = pContext.getResources().getDisplayMetrics();
                pSize = (int) (pSize * (metrics.densityDpi / 160f));

                int scaledWidth = 0;
                int scaledHeight = 0;
                if (bitmap.getWidth() < bitmap.getHeight()) {
                    scaledWidth = pSize;
                    scaledHeight = bitmap.getHeight() * pSize / bitmap.getWidth();
                } else {
                    scaledHeight = pSize;
                    scaledWidth = bitmap.getWidth() * pSize / bitmap.getHeight();
                }
                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, scaledWidth, scaledHeight, false);
                bitmap.recycle();

                int trimX = 0;
                int trimY = 0;
                if (scaledWidth > pSize) trimX = (scaledWidth - pSize) / 2;
                if (scaledHeight > pSize) trimY = (scaledHeight - pSize) / 2;
                if (trimX + pSize > scaledWidth)
                    trimX = 0;
                if (trimY + pSize > scaledHeight)
                    trimY = 0;

                //File Rotation
                Matrix matrix = new Matrix();
                switch (orientation) {
                    case 3:
                        matrix.postRotate(180);
                        break;
                    case 6:
                        matrix.postRotate(90);
                        break;
                    case 8:
                        matrix.postRotate(-90);
                        break;
                }

                Bitmap trimBitmap = Bitmap.createBitmap(scaledBitmap, trimX, trimY, pSize, pSize, matrix, true);
                scaledBitmap.recycle();

                try {
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    trimBitmap.compress(Bitmap.CompressFormat.JPEG, 85, out);
                    out.flush();
                    out.close();
                    byte[] bitmapdata = out.toByteArray();
                    return new ByteArrayInputStream(bitmapdata);

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    trimBitmap.recycle();
                }

                return null;
            }
        }.execute(file);
    }

    @Override
    public void onFileProcessing(int request) {
        showProgressDialog();
    }

    @Override
    public void onFilePickError(int request) {
        dismissProgressDialog();
        loadUserPhoto(mPhotoImage);
    }

    private void loadUserPhoto(final ImageView imageView) {
        User user = HonkioApi.getActiveUser();
        if(user != null) {
            Picasso.with(getContext())
                    .load(HonkioApi.getUserPhotoUrl(user.getUserId()))
                    .fit()
                    .centerCrop()
                    .transform(ImageUtils.USER_PICTURE_TRANSFORMATION)
                    .into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                            imageView.setImageResource(R.drawable.hk_ic_profile);
                        }
                    });
        }
        else {
            imageView.setImageResource(R.drawable.hk_ic_profile);
        }
    }
}
