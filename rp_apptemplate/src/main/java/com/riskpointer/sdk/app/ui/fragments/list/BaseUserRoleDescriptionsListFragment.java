package com.riskpointer.sdk.app.ui.fragments.list;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.UserRoleDescription;
import com.riskpointer.sdk.api.model.entity.UserRoleDescriptionsList;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.message.filter.UserRoleDescriptionsFilter;
import com.riskpointer.sdk.api.web.response.Response;

import java.util.List;

/**
 * @author Shurygin Denis
 */

public abstract class BaseUserRoleDescriptionsListFragment
        extends BaseRecycleLazyListFragment<UserRoleDescription> {

    public static final String ARG_FILTER = "BaseUserRoleDescriptionsListFragment.ARG_FILTER";

    private UserRoleDescriptionsFilter mFilter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null)
            mFilter = (UserRoleDescriptionsFilter) savedInstanceState.getSerializable(ARG_FILTER);
        else {
            Bundle args = getArguments();
            if (args != null && args.containsKey(ARG_FILTER))
                mFilter = (UserRoleDescriptionsFilter) args.getSerializable(ARG_FILTER);
            else
                mFilter = new UserRoleDescriptionsFilter();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mFilter != null)
            outState.putSerializable(ARG_FILTER, mFilter);
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager(Context context) {
        return new LinearLayoutManager(context);
    }

    @Override
    public UserRoleDescriptionsListLoader onCreateLoader(int id, Bundle args) {
        return new UserRoleDescriptionsListLoader(getActivity().getApplicationContext(),
                mFilter, null);
    }

    public void setFilter(UserRoleDescriptionsFilter filter) {
        mFilter = filter;
    }

    public UserRoleDescriptionsFilter getFilter() {
        return mFilter;
    }

    public static class UserRoleDescriptionsListLoader extends ListLoader<UserRoleDescription, UserRoleDescriptionsList> {

        private Identity mShopIdentity;
        private UserRoleDescriptionsFilter mFilter;

        public UserRoleDescriptionsListLoader(Context context, UserRoleDescriptionsFilter filter,
                                              Identity shopIdentity) {
            super(context);
            mFilter = filter;
            mShopIdentity = shopIdentity;
        }

        @Override
        protected List<UserRoleDescription> toList(Response<UserRoleDescriptionsList> response) {
            return response.getResult().getList();
        }

        @Override
        protected Task loadList(Context context, RequestCallback<UserRoleDescriptionsList> callback,
                                int count, int skip) {
            if (mFilter == null) {
                mFilter = new UserRoleDescriptionsFilter();
            }
            setupFilter(mFilter, count, skip);

            return HonkioApi.userRoleDescriptionsList(mFilter, mShopIdentity, 0, callback);
        }

        protected void setupFilter(UserRoleDescriptionsFilter filter, int count, int skip) {
            filter.setCount(count);
            filter.setSkip(skip);
        }
    }
}
