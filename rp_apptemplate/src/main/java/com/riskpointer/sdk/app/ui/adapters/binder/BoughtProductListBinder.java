package com.riskpointer.sdk.app.ui.adapters.binder;

import android.content.Context;
import android.view.View;

import com.riskpointer.sdk.api.model.entity.BoughtProduct;

/**
 * Created by Shurygin Denis on 2016-11-28.
 */

public class BoughtProductListBinder extends AbsBoughtProductListBinder<BoughtProduct,
        AbsBoughtProductListBinder.ViewHolder> {

    @Override
    public void bind(Context context, ViewHolder holder, BoughtProduct product) {
        super.bind(context, holder, product);
    }

    @Override
    public ViewHolder newHolder(View view) {
        return new ViewHolder(view);
    }
}