/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.riskpointer.sdk.api.model.entity.Transaction;
import com.riskpointer.sdk.api.utils.Currency;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.utils.FormatUtils;

import java.util.ArrayList;
import java.util.List;

public class TransactionsListAdapter extends BaseArrayListAdapter<Transaction, TransactionsListAdapter.ViewHolder> {

	public TransactionsListAdapter(Context context, List<Transaction> list) {
		this(context, trimTransactions(list), R.layout.hk_list_item_history_1);
	}

    public TransactionsListAdapter(Context context, List<Transaction> list, int layout) {
        super(context, trimTransactions(list), layout);
    }

	@Override
	public ViewHolder newHolder(View view) {
		return new ViewHolder(view);
	}

	@Override
	public void bindHolder(ViewHolder holder, int position) {
		Transaction transaction = getItem(position);
        if(transaction.getProductsCount() == 1) {
            holder.name.setText(transaction.getProduct(0).getName());
        }
		else if(transaction.getProductsCount() > 1) {
			StringBuilder builder = new StringBuilder();
			for(int i = 0; i < transaction.getProductsCount(); i++) {
				if(i > 0)
					builder.append(", ");
				builder.append(transaction.getProduct(i).getName());
			}
			holder.name.setText(builder.toString());
		}
		else {
			holder.name.setText(transaction.getMerchantName());
		}

        if (transaction.getShopName() == null) {
            holder.shopName.setVisibility(View.GONE);
        }
        else {
            holder.shopName.setText(transaction.getShopName());
            holder.shopName.setVisibility(View.VISIBLE);
        }
		holder.date.setText(FormatUtils.formatDate(mContext, transaction.getTimeStamp()));
		holder.amount.setText(FormatUtils.format(transaction.getAmount()));
		holder.currency.setText(Currency.getSymbol(transaction.getCurrency()));

        if (transaction.isStatusAccept())
            holder.status.setVisibility(View.GONE);
        else {
            if (transaction.isStatusPending())
                holder.status.setText(R.string.purchase_history_item_status_pending);
            else if (transaction.isStatusReject())
                holder.status.setText(R.string.purchase_history_item_status_reject);
            else
                holder.status.setText(transaction.getStatus());
            holder.status.setVisibility(View.VISIBLE);
        }

        if (holder.messageLayout != null && holder.message != null) {
            List<Transaction.TransactionMessage> messagesList = transaction.getMessagesList();
            if (messagesList != null && messagesList.size() > 0) {
                Transaction.TransactionMessage message = messagesList.get(messagesList.size() - 1);
                holder.message.setText(message.text);
                holder.messageLayout.setVisibility(View.VISIBLE);
            } else
                holder.messageLayout.setVisibility(View.GONE);
        }
	}
	
	@Override
	public void changeList(List<Transaction> list) {
		super.changeList(trimTransactions(list));
	}
	
	public class ViewHolder {
		final TextView name;
        final TextView status;
		final TextView shopName;
		final TextView date;
        final View messageLayout;
        final TextView message;
		final TextView amount;
		final TextView currency;
		
		public ViewHolder(View view) {
            name = (TextView) view.findViewById(R.id.hk_txtName);
            status = (TextView) view.findViewById(R.id.hk_txtStatus);
			shopName = (TextView) view.findViewById(R.id.hk_txtShopName);
            date = (TextView) view.findViewById(R.id.hk_txtDate);
            messageLayout = view.findViewById(R.id.hk_layoutMessages);
            message = (TextView) view.findViewById(R.id.hk_txtMessage);
			amount = (TextView) view.findViewById(R.id.hk_txtAmount);
            currency = (TextView) view.findViewById(R.id.hk_txtCurrency);
		}
	}
	
	private static ArrayList<Transaction> trimTransactions(List<Transaction> transactions) {
        if (transactions != null) {
            ArrayList<Transaction> result = new ArrayList<>();

            for (Transaction transaction : transactions)
                if (transaction.getAmount() > 0 || (transaction.getProductsList() != null && transaction.getProductsList().size() > 0))
                    result.add(transaction);

            return result;
        }
        return null;
	}
}
