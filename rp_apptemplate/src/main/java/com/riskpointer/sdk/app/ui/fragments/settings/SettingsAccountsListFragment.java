package com.riskpointer.sdk.app.ui.fragments.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.MerchantSimple;
import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.api.support.BroadcastManager;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.adapters.BaseRecyclerListAdapter;
import com.riskpointer.sdk.app.ui.adapters.BaseRecyclerListAdapter.OnItemClickListener;
import com.riskpointer.sdk.app.ui.adapters.binder.ListBinder;
import com.riskpointer.sdk.app.ui.adapters.binder.UserAccountItemBinder;
import com.riskpointer.sdk.app.ui.adapters.recyclerdecorations.RecyclerDividerDefaultDecoration;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.ui.dialogs.MessageDialog;
import com.riskpointer.sdk.app.ui.fragments.list.BaseRecycleListFragment;
import com.riskpointer.sdk.app.utils.AppPreferences;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shurygin Denis
 */

public class SettingsAccountsListFragment extends BaseRecycleListFragment {

    protected static final String DLG_TAG_REMOVE_ACCOUNT = "SettingsAccountsListFragment.DLG_TAG_REMOVE_ACCOUNT";
    protected static final String DLG_BUNDLE_ACCOUNT = "SettingsAccountsListFragment.DLG_BUNDLE_ACCOUNT";

    private AppPreferences mPreferences;

    private BaseRecyclerListAdapter<UserAccount, ?> mAdapter;

    protected static final int TYPE_ITEM = 0;
    protected static final int TYPE_FOOTER = 1;
    protected static final int TYPE_HEADER = 2;

    @Override
    public String getTitle(Context context) {
        return context.getString(R.string.screen_title_pay_accounts_list);
    }

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_recycler_list_no_swipe;
    }

    @Override
    protected int getDefaultListItemLayoutId(int type) {
        switch (type) {
            case TYPE_HEADER:
                return R.layout.hk_list_item_user_accounts_header;
            case TYPE_FOOTER:
                return R.layout.hk_list_item_user_accounts_footer;
        }
        return R.layout.hk_list_item_user_account_checkable;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPreferences = new AppPreferences(getActivity());

        HonkioApi.userGet(Message.FLAG_LOW_PRIORITY | Message.FLAG_NO_WARNING, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAdapter = newAdapter(null);

        LayoutInflater inflater = LayoutInflater.from(getContext());
        setupHeaders(mAdapter, inflater);

        getListView().setAdapter(mAdapter);
        getListView().addItemDecoration(new RecyclerDividerDefaultDecoration());
        setListShownNoAnimation(true);
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager(Context context) {
        return new LinearLayoutManager(context);
    }

    protected BaseRecyclerListAdapter<UserAccount, ?> newAdapter(List<UserAccount> list) {
        CheckableUserAccountsAdapter adapter = new CheckableUserAccountsAdapter(getContext(), list,
                new int[] {
                        getListItemLayoutId(TYPE_ITEM),
                        getListItemLayoutId(TYPE_FOOTER)
                });

        adapter.addOnItemClickListener(new OnItemClickListener<UserAccount, ViewHolder>() {
            @Override
            public void onItemClick(ViewHolder itemHolder, View view, UserAccount account) {
                itemHolder.checkView.setChecked(!itemHolder.checkView.isChecked());
            }
        });
        adapter.addOnItemClickListener(new OnItemClickListener<UserAccount, ViewHolder>() {
            @Override
            public void onItemClick(ViewHolder itemHolder, View view, UserAccount account) {
                onDeleteClick(account);
            }
        }, R.id.hk_btnDelete);
        return adapter;
    }

    @Override
    public boolean onClickDialog(BaseDialogFragment dialogFragment, int which) {
        if (DLG_TAG_REMOVE_ACCOUNT.equals(dialogFragment.getTag())) {
            if (isAdded()) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    showProgressDialog();
                    UserAccount delAccount = (UserAccount) dialogFragment.getBundle().getSerializable(DLG_BUNDLE_ACCOUNT);
                    mPreferences.removePreferredAccount(delAccount);
                    HonkioApi.userDeleteAccount(delAccount, 0, mAccountDeleteCallback);
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (HonkioApi.isConnected()) {
            BroadcastManager.getInstance(getActivity()).registerReceiver(
                    mBroadcastReceiver,
                    new IntentFilter(BroadcastHelper.getActionOnComplete(
                            Message.Command.USER_GET)));
            BroadcastManager.getInstance(getActivity()).registerReceiver(
                    mBroadcastReceiver,
                    new IntentFilter(BroadcastHelper.getActionOnComplete(
                            Message.Command.USER_SET)));
            User user = HonkioApi.getActiveUser();
            if (user != null) {
                mAdapter.changeList(filter(user.getAccountsList()));
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mBroadcastReceiver != null)
            BroadcastManager.getInstance(getActivity()).unregisterReceiver(mBroadcastReceiver);
    }

    protected void onDeleteClick(UserAccount account) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(DLG_BUNDLE_ACCOUNT, account);

        showMessage(DLG_TAG_REMOVE_ACCOUNT,
                R.string.pay_accounts_list_dlg_remowe_title,
                R.string.pay_accounts_list_dlg_remowe_message,
                MessageDialog.BTN_OK_CANCEL, bundle);
    }

    protected void onAccountChecked(UserAccount account, boolean isChecked) {
        if (isChecked)
            mPreferences.addPreferredAccount(account);
        else
            mPreferences.removePreferredAccount(account);
    }

    @Override
    protected void reloadList() {
        if (mAdapter != null)
            mAdapter.changeList(filter(HonkioApi.getActiveUser().getAccountsList()));
        setRefreshing(false);
    }

    protected void setupHeaders(BaseRecyclerListAdapter<UserAccount, ?> adapter, LayoutInflater inflater) {
        View view = inflater.inflate(getListItemLayoutId(TYPE_HEADER), getListView(), false);
        adapter.addHeaderView(view);
    }

    protected String[] getSupportedMerchants() {
        return new String[] {
                HonkioApi.getMainShopInfo().getMerchant().getId()
        };
    }

    protected class ViewHolder extends UserAccountItemBinder.ViewHolder {

        final CompoundButton checkView;

        public ViewHolder(View view) {
            super(view);

            checkView = (CompoundButton) view.findViewById(R.id.hk_cbActive);
        }

        public void setEnabled(boolean isEnabled) {
            super.setEnabled(isEnabled);
            if (isEnabled) {
                if (this.checkView != null) {
                    setViewAlpha(this.checkView, 1);
                    this.checkView.setEnabled(true);
                }
            }
            else {
                if (this.checkView != null) {
                    setViewAlpha(this.checkView, DISABLE_ALPHA);
                    this.checkView.setEnabled(false);
                }
            }
        }
    }

    protected class AddButtonItemBinder implements ListBinder<Void, ViewHolder> {

        @Override
        public void bind(Context context, ViewHolder holder, Void aVoid) {
            // Do nothing
        }

        @Override
        public ViewHolder newHolder(View view) {
            view.findViewById(R.id.hk_btnAdd).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(AppController.getIntent(AppController.SCREEN_SETTINGS_USER_ADD_ACCOUNT));
                }
            });
            return new ViewHolder(view);
        }
    }

    protected class CheckableUserAccountItemBinder extends UserAccountItemBinder {

        @Override
        public SettingsAccountsListFragment.ViewHolder newHolder(View view) {
            return new SettingsAccountsListFragment.ViewHolder(view);
        }

        @Override
        public void bind(Context context, UserAccountItemBinder.ViewHolder holder, UserAccount account) {
            super.bind(context, holder, account);

            SettingsAccountsListFragment.ViewHolder checkableHolder =
                    (SettingsAccountsListFragment.ViewHolder) holder;

            if (checkableHolder.checkView != null)
                checkableHolder.checkView.setChecked(mPreferences.isPreferredAccount(account));

            View btnDelete = checkableHolder.itemView.findViewById(R.id.hk_btnDelete);
            if (btnDelete != null)
                btnDelete.setVisibility(isCanBeDeleted(account) ? View.VISIBLE : View.GONE);
        }

        private boolean isCanBeDeleted(UserAccount account) {
            return account != null && account.typeIs(UserAccount.Type.CREDITCARD);
        }
    }

    private class CheckableUserAccountsAdapter extends BaseRecyclerListAdapter<UserAccount, ViewHolder> {

        private CheckableUserAccountItemBinder mBinder = new CheckableUserAccountItemBinder();
        private AddButtonItemBinder mAddButtonBinder = new AddButtonItemBinder();

        private static final int TYPE_ITEM = 0;
        private static final int TYPE_ADD_BUTTON = 1;

        private CheckableUserAccountsAdapter(Context context, List<UserAccount> list, int[] layouts) {
            super(context, list, layouts);
        }

        @Override
        protected int getListItemViewType(int position) {
            return getItem(position) != null ? TYPE_ITEM : TYPE_ADD_BUTTON;
        }

        @Override
        public ViewHolder onCreateItemViewHolder(ViewGroup parent, View view, int viewType) {
            switch (viewType) {
                case TYPE_ITEM:
                    final ViewHolder holder = mBinder.newHolder(view);
                    holder.checkView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            onAccountChecked(getItem(holder.getAdapterPosition()), isChecked);
                        }
                    });
                    return holder;
                case TYPE_ADD_BUTTON:
                    return mAddButtonBinder.newHolder(view);
            }
            throw new IllegalArgumentException("Unsupported view type");
        }

        @Override
        public void onBindItemViewHolder(ViewHolder holder, int position) {
            UserAccount account =  getItem(position);
            if (account != null)
                mBinder.bind(mContext, holder, account);
            else
                mAddButtonBinder.bind(mContext, holder, null);
        }
    }

    private List<UserAccount> filter(List<UserAccount> list) {
        String[] supportedMerchants = getSupportedMerchants();
        ArrayList<UserAccount> newList = new ArrayList<>();

        for (UserAccount account: list) {
            if (UserAccount.Type.CREDITCARD.equals(account.getType()))
                newList.add(account);
        }

        newList.add(null); // null is separator with "Add creditcard" button

        UserAccount operatorAccount = null;
        for (UserAccount account: list) {
            if (UserAccount.Type.OPERATOR.equals(account.getType())) {
                operatorAccount = account;
            }
        }

        if (operatorAccount == null) {
            operatorAccount = new UserAccount(UserAccount.Type.OPERATOR, UserAccount.ZERO_NUMBER);
            operatorAccount.setEnabled(false);
        }

        newList.add(operatorAccount);

        boolean isHasBonuses = false;
        for (UserAccount account: list) {
            if (UserAccount.Type.BONUS.equals(account.getType())) {
                for (String merchantId: supportedMerchants) {
                    if (merchantId.equals(account.getNumber())) {
                        newList.add(account);
                        isHasBonuses = true;
                        break;
                    }
                }
            }
        }

        if (!isHasBonuses) {
            MerchantSimple merchant = HonkioApi.getMainShopInfo().getMerchant();

            UserAccount account = new UserAccount(UserAccount.Type.BONUS, merchant.getId());
            account.setHasLeft(true);
            account.setDescription(merchant.getName());

            newList.add(account);
        }

        boolean isHasInvoices = false;
        for (UserAccount account: list) {
            if (UserAccount.Type.INVOICE.equals(account.getType())) {
                for (String merchantId: supportedMerchants) {
                    if (merchantId.equals(account.getNumber())) {
                        newList.add(account);
                        isHasInvoices = true;
                        break;
                    }
                }
            }
        }

        if (!isHasInvoices) {
            MerchantSimple merchant = HonkioApi.getMainShopInfo().getMerchant();

            UserAccount account = new UserAccount(UserAccount.Type.INVOICE, merchant.getId());
            account.setDescription(merchant.getName());

            newList.add(account);
        }

        return newList;
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @SuppressWarnings("unchecked")
        @Override
        public void onReceive(Context context, Intent intent) {
            reloadList();
        }
    };

    private RequestCallback<User> mAccountDeleteCallback = new RequestCallback<User>() {
        @Override
        public boolean onComplete(Response<User> response) {
            dismissProgressDialog();
            return response.isStatusAccept() || onError(response.getAnyError());
        }

        @Override
        public boolean onError(RpError error) {
            dismissProgressDialog();
            return SettingsAccountsListFragment.this.onError(error);
        }
    };
}
