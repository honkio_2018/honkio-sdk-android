package com.riskpointer.sdk.app.ui.strategy;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.Toast;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.merchant.HonkioMerchantApi;
import com.riskpointer.sdk.api.model.entity.Asset;
import com.riskpointer.sdk.api.model.entity.HkObjectType;
import com.riskpointer.sdk.api.model.entity.MediaFile;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.callback.SimpleCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;

import java.util.ArrayList;

/**
 * @author Shurygin Denis
 */

public abstract class HKAssetMediaListViewStrategy extends HKBaseMediaListViewStrategy {

    private static final String SAVED_ASSET
            = "HKAssetMediaListViewStrategy.SAVED_ASSET";
    private static final String SAVED_DEFERRED_MAIN_IMAGE
            = "HKAssetMediaListViewStrategy.SAVED_DEFERRED_MAIN_IMAGE";

    private Asset mAsset;

    private MediaFile mDeferredMainImage;

    public HKAssetMediaListViewStrategy() {
        this(TAG);
    }

    public HKAssetMediaListViewStrategy(String tag) {
        super(tag);
    }

    @Override
    public void saveState(ViewController viewController, Bundle outState) {
        super.saveState(viewController, outState);

        outState.putSerializable(SAVED_ASSET, mAsset);
        outState.putSerializable(SAVED_DEFERRED_MAIN_IMAGE, mDeferredMainImage);
    }

    @Override
    public void restoreState(ViewController viewController, Bundle savedInstanceState) {
        if (mDeferredMainImage == null)
            mDeferredMainImage = (MediaFile)
                    savedInstanceState.getSerializable(SAVED_DEFERRED_MAIN_IMAGE);

        mAsset = (Asset) savedInstanceState.getSerializable(SAVED_ASSET);

        super.restoreState(viewController, savedInstanceState);;
    }

    public void onAssetSet(Asset asset) {
        mAsset = asset;
        if (asset != null) {
            setObject(asset.getId(), HkObjectType.ASSET);
        }
    }

    @Override
    public void addDeferredImage(MediaPickerViewStrategy.PickedFile pickedFile) {
        super.addDeferredImage(pickedFile);
        if (mDeferredMainImage == null)
            mDeferredMainImage = new MediaFile(null, pickedFile.file.toURI().toASCIIString());
    }

    public void saveDeferred(final Asset asset, final SimpleCallback callback) {
        super.saveDeferred(asset.getId(), new SimpleCallback() {
                @Override
                public void onComplete() {
                    saveDeferredMain(asset, callback);
                }

                @Override
                public boolean onError(RpError error) {
                    return callback.onError(error);
                }
            }
        );
    }

    protected void saveDeferredMain(final Asset asset, final SimpleCallback callback) {
        if (mDeferredMainImage == null) {
            callback.onComplete();
        }
        else {
            showProgressDialog();

            Asset newAsset = new Asset(asset);
            newAsset.setPictureMediaFile(mDeferredMainImage);

            HonkioMerchantApi.merchantAssetSet(newAsset, HonkioApi.getMainShopIdentity(), 0,
                    new RequestCallback<Asset>() {
                        @Override
                        public boolean onComplete(Response<Asset> response) {
                            if (response.isStatusAccept()) {
                                dismissProgressDialog();
                                mDeferredMainImage = null;
                                callback.onComplete();
                                return true;
                            }
                            return onError(response.getAnyError());
                        }

                        @Override
                        public boolean onError(RpError error) {
                            dismissProgressDialog();
                            return callback.onError(error);
                        }
                    });
        }
    }

    @Override
    protected void onListLoad(ArrayList<MediaFile> list) {
        super.onListLoad(list);

        if (list != null && list.size() > 0 && mAsset != null && TextUtils.isEmpty(mAsset.getPictureUrl())) {
            setImageAsMain(list.get(0));
        }
    }

    @Override
    protected boolean onContextMenuClicked(MenuItem item, MediaFile mediaFile) {
        int itemId = item.getItemId();
        if (itemId == R.id.hk_action_set_as_main) {
            setImageAsMain(mediaFile);
            return true;
        }
        return super.onContextMenuClicked(item, mediaFile);
    }

    private void setImageAsMain(final MediaFile mediaFile) {
        if (isDeferredList()) {
            mDeferredMainImage = mediaFile;
        }
        else if (mAsset != null && mAsset.getId() != null) {
            Asset asset = new Asset(mAsset);
            asset.setPictureMediaFile(mediaFile);

            HonkioMerchantApi.merchantAssetSet(asset, HonkioApi.getMainShopIdentity(), 0,
                    new RequestCallback<Asset>() {
                        @Override
                        public boolean onComplete(Response<Asset> response) {
                            return response.isStatusAccept() || onError(response.getAnyError());
                        }

                        @Override
                        public boolean onError(RpError error) {
                            Context context = getContext();
                            if (context != null)
                                Toast.makeText(context, error.description, Toast.LENGTH_SHORT).show();
                            return true;
                        }
                    });
        }
    }

    protected Asset getAsset() {
        return mAsset;
    }

    @Override
    protected boolean isEditable() {
        return super.isEditable() && mAsset != null;
    }
}
