package com.riskpointer.sdk.app.ui.fragments.list;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.support.SwipeRefreshLayoutBottom;
import com.riskpointer.sdk.app.ui.fragments.BaseFragment;

/**
 * Created by I.N. on 2016-05-16.
 */
public abstract class BaseRecycleListFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    final private Handler mHandler = new Handler();

    final private Runnable mRequestFocus = new Runnable() {
        @Override
        public void run() {
            mListRecycler.focusableViewAvailable(mListRecycler);
        }
    };

    private RecyclerView.AdapterDataObserver mEmptyObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            if (mViewEmpty != null && mListRecycler != null) {
                mViewEmpty.setVisibility(isListEmpty() ? View.VISIBLE : View.GONE);
                mListRecycler.setVisibility(isListEmpty() ? View.GONE : View.VISIBLE);
            }
        }
    };

    private RecyclerView.Adapter<? extends RecyclerView.ViewHolder> mAdapter;
    private RecyclerView mListRecycler;

    private TextView mTxtEmpty;
    private View mViewEmpty;

    private View mProgressContainer;
    private View mListContainer;
    private SwipeRefreshLayout mLayoutSwipeRefresh;
    private SwipeRefreshLayoutBottom mLayoutSwipeRefreshBottom;

    private boolean mListShown;

// =================================================================================================
// Abstract methods
// =================================================================================================

    protected abstract int getDefaultListItemLayoutId(int type);

// =================================================================================================
// Methods of Super Class
// =================================================================================================

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), container, false);
    }

    /**
     * Attach to list view once the view hierarchy has been created.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ensureList();
    }

    /**
     * Detach from list view.
     */
    @Override
    public void onDestroyView() {
        mHandler.removeCallbacks(mRequestFocus);
        mListRecycler = null;
        mListShown = false;
        mTxtEmpty = null;
        mViewEmpty = null;
        mProgressContainer = mListContainer = null;
        if (mAdapter != null)
            mAdapter.unregisterAdapterDataObserver(mEmptyObserver);
        mAdapter = null;
        super.onDestroyView();
    }

// =================================================================================================
// Methods from Interfaces
// =================================================================================================

// =================================================================================================
// Public methods and classes
// =================================================================================================

    /**
     * This method will be called when an item in the list is selected.
     * Subclasses should override. Subclasses can call
     * getListView().getItemAtPosition(position) if they need to access the
     * data associated with the selected item.
     *
     * @param l The ListView where the click happened
     * @param v The view that was clicked within the ListView
     * @param position The position of the view in the list
     * @param id The row id of the item that was clicked
     */
    public void onListItemClick(RecyclerView l, View v, int position, long id) {
    }

    /**
     * Provide the cursor for the list view.
     */
    public void setListAdapter(RecyclerView.Adapter<? extends RecyclerView.ViewHolder> adapter) {
        boolean hadAdapter = mAdapter != null;

        if (mAdapter != null)
            mAdapter.unregisterAdapterDataObserver(mEmptyObserver);

        mAdapter = adapter;
        if (mListRecycler != null) {
            mListRecycler.setAdapter(adapter);
            if (!mListShown && !hadAdapter) {
                // The list was hidden, and previously didn't have an
                // adapter.  It is now time to show it.
                setListShown(true, getView().getWindowToken() != null);
            }
        }

        if (mAdapter != null)
            mAdapter.registerAdapterDataObserver(mEmptyObserver);

        mEmptyObserver.onChanged();
    }



    /**
     * Get the activity's RecyclerView widget.
     */
    public RecyclerView getListView() {
        ensureList();
        return mListRecycler;
    }

    /**
     * The default content for a ListFragment has a TextView that can
     * be shown when the list is empty.  If you would like to have it
     * shown, call this method to supply the text it should use.
     */
    public void setEmptyText(CharSequence text) {
        ensureList();
        if (mTxtEmpty !=null) {
            mTxtEmpty.setText(text);
        }
    }

    /**
     * Control whether the list is being displayed.  You can make it not
     * displayed if you are waiting for the initial data to show in it.  During
     * this time an indeterminant progress indicator will be shown instead.
     *
     * <p>Applications do not normally need to use this themselves.  The default
     * behavior of ListFragment is to start with the list not being shown, only
     * showing it once an adapter is given with {@link #setListAdapter(RecyclerView.Adapter)}.
     * If the list at that point had not been shown, when it does get shown
     * it will be do without the user ever seeing the hidden state.
     *
     * @param shown If true, the list view is shown; if false, the progress
     * indicator.  The initial value is true.
     */
    public void setListShown(boolean shown) {
        setListShown(shown, true);
    }

    /**
     * Like {@link #setListShown(boolean)}, but no animation is used when
     * transitioning from the previous state.
     */
    public void setListShownNoAnimation(boolean shown) {
        setListShown(shown, false);
    }

    @Override
    public void onRefresh() {
        setRefreshing(true);
        if (isAdded())
            reloadList();
    }

    protected void reloadList() {
        throw new UnsupportedOperationException("You should override reloadList method to define way of list reloading.");
    }

    public void setRefreshing(boolean refreshing) {
        if (mProgressContainer != null && mProgressContainer.getVisibility() == View.VISIBLE)
            return;

        if (mLayoutSwipeRefresh != null) {
            if (refreshing) {
                if(!mLayoutSwipeRefresh.isRefreshing())
                    mLayoutSwipeRefresh.setRefreshing(true);
            }
            else
                mLayoutSwipeRefresh.setRefreshing(false);
        }
        else if (mLayoutSwipeRefreshBottom != null) {
            if (refreshing) {
                if(!mLayoutSwipeRefreshBottom.isRefreshing())
                    mLayoutSwipeRefreshBottom.setRefreshing(true);
            }
            else
                mLayoutSwipeRefreshBottom.setRefreshing(false);
        }
    }

    /**
     * Get the ListAdapter associated with this activity's ListView.
     */
    public RecyclerView.Adapter<? extends RecyclerView.ViewHolder> getListAdapter() {
        return mAdapter;
    }

// =================================================================================================
// Protected methods and classes
// =================================================================================================

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_recycler_list;
    }

    protected int getListItemLayoutId(int type) {
        Bundle args = getArguments();
        if (args != null && args.containsKey(ARG_LIST_ITEM_LAYOUT_ID)) {
            int[] layouts = args.getIntArray(ARG_LIST_ITEM_LAYOUT_ID);
            if (layouts != null && layouts.length > type && layouts[type] > 0)
                return layouts[type];
        }
        return getDefaultListItemLayoutId(type);
    }

    protected int[] getListItemLayoutIds() {
        Bundle args = getArguments();
        if (args != null && args.containsKey(ARG_LIST_ITEM_LAYOUT_ID)) {
            int[] layouts = args.getIntArray(ARG_LIST_ITEM_LAYOUT_ID);
            if (layouts != null)
                return layouts;
        }
        return new int[] {getDefaultListItemLayoutId(0)};
    }

    protected RecyclerView.LayoutManager getLayoutManager(Context context) {
        return new LinearLayoutManager(context);
    }

    protected boolean isListEmpty() {
        RecyclerView.Adapter<?> adapter = getListAdapter();
        if (adapter != null) {
            if (adapter.getItemCount() == 0) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

// =================================================================================================
// Private methods and classes
// =================================================================================================

    /**
     * Control whether the list is being displayed.  You can make it not
     * displayed if you are waiting for the initial data to show in it.  During
     * this time an indeterminant progress indicator will be shown instead.
     *
     * @param shown If true, the list view is shown; if false, the progress
     * indicator.  The initial value is true.
     * @param animate If true, an animation will be used to transition to the
     * new state.
     */
    private void setListShown(boolean shown, boolean animate) {
        ensureList();
        if (mProgressContainer == null) {
            throw new IllegalStateException("Can't be used with a custom content view");
        }
        if (mListShown == shown) {
            return;
        }
        mListShown = shown;
        if (shown) {
            if (animate) {
                mProgressContainer.startAnimation(AnimationUtils.loadAnimation(
                        getActivity(), android.R.anim.fade_out));
                mListContainer.startAnimation(AnimationUtils.loadAnimation(
                        getActivity(), android.R.anim.fade_in));
            } else {
                mProgressContainer.clearAnimation();
                mListContainer.clearAnimation();
            }
            mProgressContainer.setVisibility(View.GONE);
            mListContainer.setVisibility(View.VISIBLE);
        } else {
            if (animate) {
                mProgressContainer.startAnimation(AnimationUtils.loadAnimation(
                        getActivity(), android.R.anim.fade_in));
                mListContainer.startAnimation(AnimationUtils.loadAnimation(
                        getActivity(), android.R.anim.fade_out));
            } else {
                mProgressContainer.clearAnimation();
                mListContainer.clearAnimation();
            }
            mProgressContainer.setVisibility(View.VISIBLE);
            mListContainer.setVisibility(View.INVISIBLE);
        }
    }

    private void ensureList() {
        if (mListRecycler != null) {
            return;
        }
        View root = getView();
        if (root == null) {
            throw new IllegalStateException("Content view not yet created");
        }
        if (root instanceof RecyclerView) {
            mListRecycler = (RecyclerView)root;
        } else {
            mTxtEmpty = root.findViewById(R.id.hk_emptyText);
            mViewEmpty = root.findViewById(R.id.hk_emptyView);
            if (mTxtEmpty == null && mViewEmpty instanceof TextView) {
                mTxtEmpty = (TextView) mViewEmpty;
            }
            else if (mViewEmpty == null){
                mViewEmpty = mTxtEmpty;
            }
            mProgressContainer = root.findViewById(R.id.hk_progressContainer);
            mListContainer = root.findViewById(R.id.hk_listContainer);
            mListRecycler = root.findViewById(R.id.hk_listRecycler);
            View layoutSwipeRefresh =  root.findViewById(R.id.hk_layoutSwipeRefresh);

            if (layoutSwipeRefresh instanceof SwipeRefreshLayout) {
                mLayoutSwipeRefresh = (SwipeRefreshLayout) layoutSwipeRefresh;
                mLayoutSwipeRefresh.setOnRefreshListener(this);
            }
            else if (layoutSwipeRefresh instanceof SwipeRefreshLayoutBottom){
                mLayoutSwipeRefreshBottom = (SwipeRefreshLayoutBottom) layoutSwipeRefresh;
                mLayoutSwipeRefreshBottom.setOnRefreshListener(this);
            }

            if (mListRecycler == null) {
                throw new RuntimeException(
                        "Your content must have a RecycleView whose id attribute is " +
                                "'hk_listRecycler'");
            }

            mListRecycler.setLayoutManager(getLayoutManager(getContext()));
        }
        mListShown = true;
        //mListRecycler.addOnItemClickListener(mOnClickListener);
        if (mAdapter != null) {
            RecyclerView.Adapter<? extends RecyclerView.ViewHolder> adapter = mAdapter;
            mAdapter = null;
            setListAdapter(adapter);
        } else {
            // We are starting without an adapter, so assume we won't
            // have our data right away and start with the progress indicator.
            if (mProgressContainer != null) {
                setListShown(false, false);
            }
        }
        mHandler.post(mRequestFocus);
    }
}
