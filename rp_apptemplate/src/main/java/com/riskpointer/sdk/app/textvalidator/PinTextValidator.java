/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.textvalidator;

public class PinTextValidator extends BaseTextValidator {
	public static final int PIN_SIZE = 4;

	public PinTextValidator(String errorMessage) {
		super(errorMessage);
	}

	@Override
	public boolean validate(String string) {
		return string.length() == PIN_SIZE;
	}

}
