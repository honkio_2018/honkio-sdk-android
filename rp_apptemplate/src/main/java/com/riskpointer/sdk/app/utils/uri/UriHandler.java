package com.riskpointer.sdk.app.utils.uri;

import android.net.Uri;

/**
 * @author Shurygin Denis on 2015-04-17.
 */
public interface UriHandler {

    boolean handle(String uriString);
    boolean handle(Uri uri);

}
