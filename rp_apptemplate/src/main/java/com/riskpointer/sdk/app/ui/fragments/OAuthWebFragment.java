package com.riskpointer.sdk.app.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.HKDevice;
import com.riskpointer.sdk.api.model.task.RequestTask;
import com.riskpointer.sdk.api.model.task.TaskStorage;
import com.riskpointer.sdk.api.utils.HonkioOAuth;
import com.riskpointer.sdk.api.web.SimpleAsyncGet;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Shurygin Denis on 2016-01-29.
 */
public class OAuthWebFragment extends WebViewFragment {

    public static final String CLIENT_ID = "OAuth2Fragment.CLIENT_ID";
    public static final String CLIENT_SECRET = "OAuth2Fragment.CLIENT_SECRET";

    public final static String SAVED_TASK = "OAuth2Fragment.SAVED_TASK";

    private static final String RESPONSE_TYPE_VALUE ="code";

    private GetOAuthTokenTask mTask;

    @Override
    public String getTitle(Context context) {
        return context.getString(R.string.login_screen_title);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState == null) {
            loadLoginPage();
        }
        else {
            mTask = (GetOAuthTokenTask) TaskStorage.pull(savedInstanceState.getInt(SAVED_TASK));
            if (mTask != null)
                mTask.setCallback(mGetOAuthTokenCallback, true);
        }
    }

    @Override
    protected boolean onPageWillLoaded(String url) {
        if(url.startsWith(HonkioOAuth.DEFAULT_REDIRECT_URL_FOR_CHECK)){
            Uri uri = Uri.parse(url);

            //If the user doesn't allow authorization to our application, the authorizationToken Will be null.
            String authorizationToken = uri.getQueryParameter(RESPONSE_TYPE_VALUE);
            if(authorizationToken==null){
                Log.i("Authorize", "The user doesn't allow authorization.");
                onAuthorisationNotAllowed();
                return true;
            }
            Log.i("Authorize", "Auth token received: "+authorizationToken);


            HKDevice device = HonkioApi.getDevice();
            if (device == null) {
                BaseActivity.restart(getContext());
                return true;
            }
            //Generate URL for requesting Access Token
            String accessTokenUrl = HonkioOAuth.getAccessTokenUrl(device, getClientId(),
                    getClientSecret(), HonkioOAuth.DEFAULT_REDIRECT_URL, authorizationToken);
            mTask = new GetOAuthTokenTask(getContext(), accessTokenUrl, mGetOAuthTokenCallback);
            mTask.execute();

            showProgressDialog();

            return true;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mTask != null)
            outState.putInt(SAVED_TASK, TaskStorage.pushIfNotCompleted(mTask));
    }

    protected void onAuthorisationNotAllowed() {
        Fragment fragment = getParentFragment();
        if (fragment instanceof OAuth2Callbacks) {
            ((OAuth2Callbacks) fragment).onAuthorisationNotAllowed();
        }
        else {
            Activity activity = getActivity();
            if (activity instanceof OAuth2Callbacks)
                ((OAuth2Callbacks) activity).onAuthorisationNotAllowed();
        }
        loadLoginPage();
    }

    private RequestCallback<String> mGetOAuthTokenCallback = new RequestCallback<String>() {
        @Override
        public boolean onComplete(Response<String> response) {
            if (!isFragmentStateSaved()) {
                dismissProgressDialog();
                Fragment fragment = getParentFragment();
                if (fragment instanceof OAuth2Callbacks) {
                    ((OAuth2Callbacks) fragment).onTokenGet(response.getResult());
                }
                else {
                    Activity activity = getActivity();
                    if (activity instanceof OAuth2Callbacks)
                        ((OAuth2Callbacks) activity).onTokenGet(response.getResult());
                }
            }
            return true;
        }

        @Override
        public boolean onError(RpError error) {
            if (!isFragmentStateSaved()) {
                dismissProgressDialog();
                if (!OAuthWebFragment.this.onError(error))
                    onAuthorisationNotAllowed();
            }
            return true;
        }
    };

    private void loadLoginPage() {
        if (getActivity() != null) {
            CookieSyncManager.createInstance(getActivity());
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();

            HKDevice device = HonkioApi.getDevice();
            if (device == null) {
                BaseActivity.restart(getContext());
                return;
            }

            loadUrl(HonkioOAuth.getAuthorisationUrl(device, getClientId(),
                    HonkioOAuth.DEFAULT_SCOPE, HonkioOAuth.DEFAULT_REDIRECT_URL));
        }
    }

    private String getClientId() {
        Bundle args = getArguments();
        if (args != null && args.containsKey(CLIENT_ID)) {
            String value = args.getString(CLIENT_ID);
            if (value != null)
                return value;
        }
        throw new IllegalStateException("Please provide OAuth Client ID for the fragment in the arguments.");
    }

    private String getClientSecret() {
        Bundle args = getArguments();
        if (args != null && args.containsKey(CLIENT_SECRET)) {
            String value = args.getString(CLIENT_SECRET);
            if (value != null)
                return value;
        }
        throw new IllegalStateException("Please provide OAuth Client Secret for the fragment in the arguments.");
    }

    public interface OAuth2Callbacks {
        void onAuthorisationNotAllowed();
        void onTokenGet(String token);
    }

    public static class GetOAuthTokenTask extends SimpleAsyncGet implements RequestTask<String> {

        private Response<String> mResult;
        private RpError mError = RpError.NONE;
        private RequestCallback<String> mCallback;

        /**
         * Construct new Task.
         *
         * @param urlString URL to send GET request.
         */
        public GetOAuthTokenTask(Context context, String urlString, RequestCallback<String> callback) {
            super(context, urlString);
            mCallback = callback;
        }

        @Override
        protected void onPostExecute(String string) {
            if (string == null) {
                mResult = null;
                mError = new RpError(RpError.Api.BAD_RESPONSE);
                if (mCallback != null)
                    mCallback.onError(mError);
            }
            else {
                mResult = new Response<>(string);
                Response.Editor.setStatus(mResult, Response.Status.ACCEPT);
                if (mCallback != null) {
                    mCallback.onComplete(mResult);
                }
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = super.doInBackground(params);

            if (result == null) {
                mError = new RpError(getErrorCode());
                return null;
            }

            try {
                JSONObject json = new JSONObject(result);
                return json.optString("access_token");
            } catch (JSONException e) {
                e.printStackTrace();
                mError = new RpError(RpError.Api.BAD_RESPONSE);
                return null;
            }
        }

        @Override
        protected void onCancelled() {
            if (mError == RpError.NONE)
                mError = new RpError(RpError.Api.UNKNOWN);
            if (mCallback != null)
                mCallback.onError(mError);
        }

        @Override
        public void setCallback(RequestCallback<String> callback, boolean deliverResult) {
            mCallback = callback;
            if (isCompleted() && deliverResult && callback != null) {
                if (mError != null && mError.code != RpError.Api.NONE)
                    mCallback.onError(mError);
                else
                    mCallback.onComplete(mResult);
            }
        }
    }
}
