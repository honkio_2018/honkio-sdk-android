package com.riskpointer.sdk.app.ui.strategy;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.dialogs.AccountChooseDialog;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.utils.AppPreferences;
import com.riskpointer.sdk.app.utils.RequestCodes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Shurygin Denis
 */

public class UserAccountSelectionViewStrategy extends BaseViewLifeCycleObserver {

    public static final String TAG = "UserAccountSelectionViewStrategy.TAG";

    public interface Delegate {
        void onAccountSelect(UserAccountSelectionViewStrategy strategy, UserAccount account);
        void onAccountSelectionCancel(UserAccountSelectionViewStrategy strategy);
    }

    private static final String DLG_TAG_CHOOSE_ACCOUNT = "OrderSetViewStrategy.DLG_TAG_CHOOSE_ACCOUNT";

    private static final String SAVED_DISALLOWED_ACCOUNTS = "OrderSetViewStrategy.SAVED_DISALLOWED_ACCOUNTS";

    private AppPreferences mPreferences;

    private HashSet<String> mDisallowedAccounts = new HashSet<>();

    public UserAccountSelectionViewStrategy() {
        this(TAG);
    }

    public UserAccountSelectionViewStrategy(String tag) {
        super(tag);
    }

    public void selectAccount(Shop shop, double amount, String currency) {
        List<UserAccount> list = filter(getPreferences().getPreferredAccounts(), shop, amount, currency);
        if (list.size() == 1) {
            onAccountSelected(list.get(0));
        }
        else {
            showDialog(AppController.DIALOG_FRAGMENT_ACCOUNT_CHOOSE,
                    DLG_TAG_CHOOSE_ACCOUNT,
                    shop, amount, currency, mDisallowedAccounts);
        }
    }

    @Override
    public void onActivityResult(ViewController viewController, int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCodes.ORDER_SET_FE_ADD_ACCOUNT) {
            if (resultCode == Activity.RESULT_OK) {
                onAccountSelected(
                        (UserAccount) data.getSerializableExtra(AppController.Extra.USER_ACCOUNT));
            }
            else
                onAccountSelectionCancel();
        }
        else
            super.onActivityResult(viewController, requestCode, resultCode, data);
    }

    @Override
    public boolean onClickDialog(ViewController viewController, BaseDialogFragment dialogFragment, int which) {
        if (isAttached()) {
            if (DLG_TAG_CHOOSE_ACCOUNT.equals(dialogFragment.getTag())) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    onAccountSelected(((AccountChooseDialog) dialogFragment).getSelectedAccount());
                }
                else {
                    onAccountSelectionCancel();
                }
                return true;
            }
        }
        return super.onClickDialog(viewController, dialogFragment, which);
    }

    @Override
    public void saveState(ViewController viewController, Bundle outState) {
        super.saveState(viewController, outState);
        outState.putStringArray(SAVED_DISALLOWED_ACCOUNTS,
                mDisallowedAccounts.toArray(new String[mDisallowedAccounts.size()]));
    }

    @Override
    public void restoreState(ViewController viewController, Bundle savedInstanceState) {
        super.restoreState(viewController, savedInstanceState);

        String[] disallowedAccounts = savedInstanceState.getStringArray(SAVED_DISALLOWED_ACCOUNTS);
        if (disallowedAccounts != null) {
            mDisallowedAccounts.clear();
            mDisallowedAccounts.addAll(Arrays.asList(disallowedAccounts));
        }
    }

    public Set<String> getDisallowedAccounts() {
        return mDisallowedAccounts;
    }

    public void setDisallowedAccounts(Set<String> accountsSet) {
        mDisallowedAccounts.clear();
        mDisallowedAccounts.addAll(accountsSet);
    }

    protected void onAccountSelected(UserAccount account) {
        Delegate delegate = getDelegate();
        if (delegate != null)
            delegate.onAccountSelect(this, account);
    }

    protected void onAccountSelectionCancel() {
        Delegate delegate = getDelegate();
        if (delegate != null)
            delegate.onAccountSelectionCancel(this);
    }

    private List<UserAccount> filter(List<UserAccount> list, Shop shop, double amount, String currency) {
        if (shop == null && HonkioApi.getMainShopInfo() != null)
            shop = HonkioApi.getMainShopInfo().getShop();

        if (shop != null && shop.getMerchant() != null) {
            ArrayList<UserAccount> newList = new ArrayList<>();

            for (UserAccount account: list) {
                if (!mDisallowedAccounts.contains(account.getType()))
                    if (shop.getMerchant().isAccountSupported(account.getType())) {
                        if (account.typeIs(UserAccount.Type.INVOICE)) {
                            if (shop.getMerchant().getId().equals(account.getNumber()))
                                newList.add(account);
                            // Else don't add
                        } else if (!account.isDummy() && account.enoughLeft(amount))
                            newList.add(account);
                    }
            }

            return newList;
        }
        return list;
    }

    private AppPreferences getPreferences() {
        if (mPreferences == null)
            mPreferences = new AppPreferences(getContext());
        return mPreferences;
    }

    private Delegate getDelegate() {
        ViewLifeCycleObserver parent = getParentLifeCycleObserver();
        if (parent instanceof Delegate)
            return (Delegate) parent;

        ViewController viewController = getController();
        if (viewController instanceof Delegate)
            return (Delegate) viewController;
        return null;
    }
}
