package com.riskpointer.sdk.app.textvalidator;

import android.text.TextUtils;

/**
 * Created by Shurygin Denis on 2016-10-12.
 */
public class FloatNumberTextValidator extends BaseTextValidator {

    private boolean isAllowEmpty = false;

    public FloatNumberTextValidator(String errorMessage) {
        super(errorMessage);
    }

    @Override
    public boolean validate(String string) {
        if (TextUtils.isEmpty(string))
            return isAllowEmpty;

        float number;
        try {
            number = Float.parseFloat(string);
        } catch (NumberFormatException e) {
            return false;
        }
        return validateNumber(number);
    }

    public void setAllowEmpty(boolean value) {
        isAllowEmpty = value;
    }

    protected boolean validateNumber(float number) {
        return true;
    }
}
