/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.dialogs;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.applications.BaseApplication;

public class ProgressDialog extends BaseDialogFragment implements DialogInterface.OnClickListener {

	public static final String ARG_MESSAGE = "ProgressDialog.ARG_MESSAGE";
	public static final String ARG_TITLE = "ProgressDialog.ARG_TITLE";

	private static final String SAVED_MESSAGE = "ProgressDialog.SAVED_MESSAGE";

	private boolean isWaitingForDismiss = false;

	private String mMessage;
	private String mTitle;

	private TextView mMessageTextView;

// =================================================================================================
// Methods of Super Class
// =================================================================================================

	@Override
	protected int getDefaultLayoutId() {
		return R.layout.hk_dialog_fragment_progress;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setCancelable(false);

		if (mMessage == null && savedInstanceState != null)
			mMessage = savedInstanceState.getString(SAVED_MESSAGE);

		Bundle args = getArguments();
		if (args != null) {
			mTitle = args.getString(ARG_TITLE);
			if (mMessage == null)
				mMessage = args.getString(ARG_MESSAGE);
		}

		if (mTitle == null)
			mTitle = getString(R.string.dlg_loading_title);
		if (mMessage == null)
			mMessage = getString(R.string.dlg_loading_message);
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		LayoutInflater inflater = LayoutInflater.from(getActivity());
		
		AlertDialog.Builder builder = new Builder(getActivity());
		View view = inflater.inflate(getLayoutId(), null);
		mMessageTextView = (TextView) view.findViewById(R.id.hk_txtMessage);
		if (mMessage != null)
			mMessageTextView.setText(mMessage);

		if (mTitle != null)
			builder.setTitle(mTitle);

		builder.setView(view);
		
		return builder.create();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(SAVED_MESSAGE, mMessage);
	}

	@Override
	public void onResume() {
		super.onResume();
		if(isWaitingForDismiss)
			dismiss();
	}
	
	@Override
	public void dismiss() {
		if(isResumed())
			super.dismiss();
		else
			isWaitingForDismiss = true;
	}

// =================================================================================================
// Public methods and classes
// =================================================================================================

	public void setMessage(String messageString) {
		mMessage = messageString;
		if (mMessage != null)
			mMessageTextView.setText(mMessage);
	}

}
