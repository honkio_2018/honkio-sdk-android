/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class BaseSpinAdapter<T> extends BaseAdapter {
	
	private LayoutInflater mInflater;
	private List<T> mList;
	private int mLayoutId;
	
	public BaseSpinAdapter(Context context, List<T> list, int layoutId) {
		mInflater = LayoutInflater.from(context);
		mList = list;
		mLayoutId = layoutId;
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public T getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressWarnings("unchecked")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		View view = convertView;
		if(view == null) {
			view = mInflater.inflate(mLayoutId, null);
			holder = new ViewHolder(view);
			view.setTag(holder);
		}
		else
			holder = (ViewHolder) view.getTag();
		
		T object = getItem(position);
		holder.text.setText(objectToString(object));
		return view;
	}
	
	protected String objectToString(T object) {
		return object.toString();
	}
	
	private class ViewHolder {
		public final TextView text;
		
		public ViewHolder(View view) {
			text = (TextView) view.findViewById(android.R.id.text1);
		}
	}

}
