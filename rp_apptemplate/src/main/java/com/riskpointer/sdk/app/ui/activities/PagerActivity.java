package com.riskpointer.sdk.app.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.riskpointer.sdk.app.FragmentComponents;
import com.riskpointer.sdk.app.R;

/**
 * @author Shurygin Denis on 2015-06-03.
 */
public class PagerActivity extends BaseActivity {
    public static final String EXTRA_FRAGMENT_COMPONENTS = "PagerActivity.EXTRA_FRAGMENT_COMPONENTS";
    public static final String EXTRA_ACTIVITY_TITLE = "PagerActivity.EXTRA_ACTIVITY_TITLE";
    public static final String EXTRA_PAGE_TITLES = "PagerActivity.EXTRA_PAGE_TITLES";

    private ViewPager mViewPager;
    private PagerAdapter mAdapter;

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_activity_pager_fragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String titleString = getIntent().getStringExtra(EXTRA_ACTIVITY_TITLE);
        if (titleString != null && getSupportActionBar() != null)
            getSupportActionBar().setTitle(titleString);
        FragmentComponents[] fragmentComponents = getFragmentComponents();
        mAdapter = new PagerAdapter(this, fragmentComponents, getTitles());
        mViewPager = (ViewPager) findViewById(R.id.hk_pager);
        mViewPager.setAdapter(mAdapter);
    }

    public FragmentPagerAdapter getAdapter() {
        return mAdapter;
    }

    public ViewPager getViewPager() {
        return mViewPager;
    }

    public int findFragment(Class<?> fragmentClass, int offset) {
        return mAdapter.findFragment(fragmentClass, offset);
    }

    public void setCurrentItem(int position, boolean smoothScroll) {
        if (mAdapter.getCount() < position)
            mViewPager.setCurrentItem(position, smoothScroll);
        else
            mViewPager.setCurrentItem(mAdapter.getCount() - 1, smoothScroll);
    }

    protected FragmentComponents[] getFragmentComponents() {
        Parcelable[] parcelables = getIntent().getParcelableArrayExtra(EXTRA_FRAGMENT_COMPONENTS);

        if (parcelables == null) {
            StringBuilder messageBuilder = new StringBuilder("No FRAGMENT_COMPONENTS. ");
            messageBuilder.append("| Title=").append(getIntent().getStringExtra(EXTRA_ACTIVITY_TITLE));
            messageBuilder.append("| intentId=").append(getIntent().getIntExtra(EXTRA_INTENT_ID, -1));
            throw new RuntimeException(messageBuilder.toString());
        }

        FragmentComponents[] fragmentComponents = new FragmentComponents[parcelables.length];
        for (int i = 0; i < parcelables.length; i++)
            fragmentComponents[i] = (FragmentComponents) parcelables[i];
        return fragmentComponents;
    }

    protected String[] getTitles() {
        return getIntent().getStringArrayExtra(EXTRA_PAGE_TITLES);
    }

    private class PagerAdapter extends FragmentPagerAdapter {
        private BaseActivity mActivity;
        private FragmentComponents[] mFragmentComponents;
        private Fragment[] mList;
        private String[] mTitles;

        public PagerAdapter(BaseActivity activity, FragmentComponents[] fragmentComponents, String[] titles) {
            super(activity.getSupportFragmentManager());
            mActivity = activity;
            mFragmentComponents = fragmentComponents;
            mList = new Fragment[mFragmentComponents.length];
            mTitles = titles;
            if (mTitles == null)
                mTitles = new String[mFragmentComponents.length];
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = mList[position];
            if (fragment == null) {
                fragment = mFragmentComponents[position].newFragment();
                mList[position] = fragment;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return mList.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = mTitles[position];
            if (title == null) {
                Fragment fragment = getItem(position);
                if (fragment instanceof SingleFragmentActivity.SingleFragment)
                    title = ((SingleFragmentActivity.SingleFragment) fragment).getTitle(mActivity);
                else
                    title = "";
                mTitles[position] = title;
            }
            return title;
        }

        public int findFragment(Class<?> fragmentClass, int offset) {
            if (fragmentClass != null)
                for (int position = offset; position < mFragmentComponents.length; position++) {
                    if (fragmentClass.isAssignableFrom(mFragmentComponents[position].getFragmentClass()))
                        return position;
                }
            return -1;
        }
    }

    public static Intent newIntent(Context context, String activityTitle, FragmentComponents[] fragmentComponents, String[] pageTitles) {
        Intent intent = new Intent(context, PagerActivity.class);
        intent.putExtra(EXTRA_ACTIVITY_TITLE, activityTitle);
        intent.putExtra(EXTRA_FRAGMENT_COMPONENTS, fragmentComponents);
        intent.putExtra(EXTRA_PAGE_TITLES, pageTitles);
        return intent;
    }
}
