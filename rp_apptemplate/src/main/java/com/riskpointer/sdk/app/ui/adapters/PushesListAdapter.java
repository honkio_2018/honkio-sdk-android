package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.riskpointer.sdk.api.model.push.Push;
import com.riskpointer.sdk.app.R;

import java.util.List;

import static android.text.format.DateUtils.FORMAT_ABBREV_RELATIVE;
import static android.text.format.DateUtils.MINUTE_IN_MILLIS;
import static android.text.format.DateUtils.getRelativeTimeSpanString;

/**
 * @author Shurygin Denis on 2015-03-02.
 */
public class PushesListAdapter extends BaseRecyclerListAdapter<Push<?>, PushesListAdapter.ViewHolder> {

    public PushesListAdapter(Context context, List<Push<?>> list, int layout) {
        super(context, list, layout);
    }

    @Override
    public ViewHolder onCreateItemViewHolder(ViewGroup parent, View view, int viewType) {
        return new ViewHolder(view);
    }

    @Override
    public void onBindItemViewHolder(ViewHolder holder, int position) {
        Push<?> push = getItem(position);

        holder.message.setText(push.getMessage());
        holder.date.setText(
                getRelativeTimeSpanString(push.getDate(),
                        System.currentTimeMillis(),MINUTE_IN_MILLIS, FORMAT_ABBREV_RELATIVE));
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView message;
        public final TextView date;
        
        public ViewHolder(View view) {
            super(view);

            message = (TextView) view.findViewById(R.id.hk_txtMessage);
            date = (TextView) view.findViewById(R.id.hk_txtDate);
        }
    }
}
