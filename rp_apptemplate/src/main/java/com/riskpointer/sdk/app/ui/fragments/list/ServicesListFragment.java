/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.fragments.list;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.riskpointer.sdk.api.web.message.filter.ShopFilter;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.adapters.ServiceListAdapter;
import com.riskpointer.sdk.app.R;

public abstract class ServicesListFragment extends BaseListFragment {
	private ServiceListAdapter mAdapter;

    protected abstract ServiceListAdapter.ListItem[] createServicesList();

    @Override
    protected int getDefaultListItemLayoutId(int type) {
        return R.layout.hk_list_item_shop_type;
    }

    @Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		mAdapter = createAdapter(createServicesList());
		setListAdapter(mAdapter);
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		ShopFilter filter = new ShopFilter();
		filter.setServiceType(mAdapter.getItem(position).service_type);
		Intent intent = AppController.getIntent(AppController.SCREEN_SHOPS_LIST, filter);
		startActivity(intent);
	}

    protected ServiceListAdapter createAdapter(ServiceListAdapter.ListItem[] list) {
        return new ServiceListAdapter(getActivity(), getListItemLayoutId(0), list);
    }
}
