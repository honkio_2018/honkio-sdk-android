package com.riskpointer.sdk.app.utils;

import android.content.Context;
import android.net.Uri;

import java.io.File;

/**
 * @author Shurygin Denis
 */

public class HonkioFileProvider extends android.support.v4.content.FileProvider {

    private static final String AUTHORITY_POSTFIX = ".fileprovider";

    public static String authority(Context context) {
        return context.getPackageName() + AUTHORITY_POSTFIX;
    }

    public static Uri getUriForFile(Context context, File file) {
        return getUriForFile(context, authority(context), file);
    }

}
