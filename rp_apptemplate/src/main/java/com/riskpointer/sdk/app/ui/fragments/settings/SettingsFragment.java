package com.riskpointer.sdk.app.ui.fragments.settings;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.riskpointer.sdk.api.BaseHonkioApi;
import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.HKDevice;
import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.model.process.LogoutProcessModel;
import com.riskpointer.sdk.api.model.process.PinReason;
import com.riskpointer.sdk.api.support.BroadcastManager;
import com.riskpointer.sdk.api.utils.ApiDebugUtils;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.callback.SimpleCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;
import com.riskpointer.sdk.app.ui.activities.SingleFragmentActivity;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.ui.dialogs.MessageDialog;
import com.riskpointer.sdk.app.ui.fragments.BaseFragment;
import com.riskpointer.sdk.app.ui.fragments.PinFragment;
import com.riskpointer.sdk.app.utils.AppPreferences;
import com.riskpointer.sdk.app.utils.ImageUtils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * @author Shurygin Denis on 2015-04-01.
 */
public class SettingsFragment extends BaseFragment implements BaseDialogFragment.OnClickListener {

    public static final String DLG_TAG_LOGOUT = "SettingsFragment.DLG_TAG_LOGOUT";

    public static final int USER_SETTINGS_PIN_REQUEST = 1;

    final int[] mViewIds = {R.id.hk_btnPayMethod, R.id.hk_btnChangePin, R.id.hk_btnGdpr,
            R.id.hk_btnChangePass, R.id.hk_btnSetAddress, R.id.hk_btnLogout, R.id.hk_btnLog,
            R.id.hk_btnTou, R.id.hk_btnAbout, R.id.hk_btnSetPhoto, R.id.hk_btnForgotPass};

    private CheckBox mCbNotifications;
    protected AppPreferences mPreferences;

    private ImageView mProfileImage;

    private BroadcastReceiver mProfileImageChangeREceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateProfileImage();
        }
    };

    @Override
    public String getTitle(Context context) {
        return context.getString(R.string.settings_title);
    }

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_settings;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        if (BaseHonkioApi.isConnected()) {

            mPreferences = new AppPreferences(getActivity());
            mProfileImage = (ImageView) rootView.findViewById(R.id.hk_imgUserPhoto);
            mCbNotifications = (CheckBox) rootView.findViewById(R.id.hk_cbNotifications);
            if (mCbNotifications != null && AppController.getInstance().getGcmSenderId() != null) {
                mCbNotifications.setOnCheckedChangeListener(getCbNotificationCheckListener());
            }

            final CheckBox savePinCheckBox = (CheckBox) rootView.findViewById(R.id.hk_cbSavePin);

            if (savePinCheckBox != null) {
                savePinCheckBox.setChecked(mPreferences.pref.getBoolean(AppPreferences.PREF_SAVE_PIN, AppPreferences.PREF_SAVE_PIN_DEFAULT));
                savePinCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        Toast.makeText(getActivity(), "Unsupported option!!!!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            updateUi();
            setupListeners(rootView);
        }

        TextView txtAppInfo = (TextView) rootView.findViewById(R.id.hk_txtAppInfo);
        setTextIfExists(txtAppInfo, AppController.getInstance().appUrl);
        setVisibilityIfExists(txtAppInfo,
                ApiDebugUtils.isDebuggable(getActivity()) ? View.VISIBLE : View.GONE);
        setVisibilityIfExists(rootView.findViewById(R.id.hk_btnLog),
                ApiDebugUtils.isDebuggable(getActivity()) ? View.VISIBLE : View.GONE);

        if (mProfileImage != null)
            BroadcastManager.getInstance(getContext()).registerReceiver(mProfileImageChangeREceiver,
                    new IntentFilter(BroadcastHelper
                            .getActionOnComplete(Message.Command.USER_SET_PHOTO)));

        return rootView;
    }

    @Override
    public void onDestroyView() {
        BroadcastManager.getInstance(getContext()).unregisterReceiver(mProfileImageChangeREceiver);
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == USER_SETTINGS_PIN_REQUEST) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                if (HonkioApi.checkPin(data.getStringExtra(PinFragment.EXTRA_PIN))) {
                    startActivity(AppController.getIntent(AppController.SCREEN_SETTINGS_USER_PROFILE));
                } else
                    onError(new RpError(RpError.Api.INVALID_PIN));
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
            updateUi();
        }
    }

    private void setupListeners(View rootView) {
        for (int viewId : mViewIds) {

            View view = rootView.findViewById(viewId);
            if (view != null)
                view.setOnClickListener(mClickListener);
        }
    }

    View.OnClickListener mClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            int viewId = view.getId();
            if (viewId == R.id.hk_btnPayMethod)
                onSetPayMethodClick();
            else if (viewId == R.id.hk_btnChangePin)
                onChangePinClick();
            else if (viewId == R.id.hk_btnChangePass)
                onChangePassClick();
            else if (viewId == R.id.hk_btnSetAddress)
                onUserSettingsClick();
            else if (viewId == R.id.hk_btnLogout)
                onLogoutClick();
            else if (viewId == R.id.hk_btnTou)
                onTouClick();
            else if (viewId == R.id.hk_btnGdpr)
                onGdprClick();
            else if (viewId == R.id.hk_btnAbout)
                onAboutClick();
            else if (viewId == R.id.hk_btnForgotPass)
                onLostPasswordClick();
            else if (viewId == R.id.hk_btnLog)
                onLogsListClick();
        }
    };

    protected void onChangePinClick() {
        startActivity(AppController.getIntent(AppController.SCREEN_SETTINGS_CHANGE_PIN));
    }

    protected void onLostPasswordClick() {
        startActivity(AppController.getIntent(AppController.SCREEN_SETTINGS_LOST_PASSWORD, HonkioApi.getActiveUser().getLogin()));
    }

    protected void onChangePassClick() {
        startActivity(AppController.getIntent(AppController.SCREEN_SETTINGS_CHANGE_PASSWORD));
    }

    protected void onSetPayMethodClick() {
        User user = HonkioApi.getActiveUser();
        if (user != null && user.getAccountsList().size() > 0)
            startActivityForResult(AppController.getIntent(AppController.SCREEN_SETTINGS_USER_ACCOUNTS), 0);
        else
            startActivityForResult(AppController.getIntent(AppController.SCREEN_SETTINGS_USER_ADD_ACCOUNT), 0);
    }

    protected void onUserSettingsClick() {
        BaseActivity.startPinActivity(this, PinReason.REENTER, USER_SETTINGS_PIN_REQUEST);
    }

    protected void onLogoutClick() {
        showMessage(DLG_TAG_LOGOUT, 0, R.string.settings_dlg_logout_message, MessageDialog.BTN_OK_CANCEL);
    }

    protected void onLogsListClick() {
        startActivity(SingleFragmentActivity.newIntent(getContext(), "Logs List",
                SettingsLogsListFragment.class, null));
    }

    protected void onTouClick() {
        startActivity(AppController.getIntent(AppController.ACTIVITY_TOU));
    }

    protected void onGdprClick() {
        startActivity(AppController.getIntent(AppController.ACTIVITY_GDPR));
    }

    protected void onAboutClick() {
        startActivity(AppController.getIntent(AppController.ACTIVITY_ABOUT));
    }

    protected CompoundButton.OnCheckedChangeListener getCbNotificationCheckListener() {
        return new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // FIXME
            }
        };
    }

    private void updateUi() {
        Context context = getActivity();
        if (context != null) {
            if (mCbNotifications != null) {
                AppController appModel = AppController.getInstance();
                if (appModel.getGcmSenderId() == null || appModel.getGcmRegistrationId() == null)
                    mCbNotifications.setVisibility(View.GONE);
                else {
                    User user = HonkioApi.getActiveUser();
                    if (user != null) {
                        HKDevice device = HonkioApi.getDevice();
                        if (device != null) {
                            mCbNotifications.setChecked(HKDevice.NotifyType.ALL.equals(device.getNotifyType()));
                        } else
                            mCbNotifications.setVisibility(View.GONE);
                    } else
                        mCbNotifications.setVisibility(View.GONE);
                }
            }

            updateProfileImage();
        }
    }

    private void updateProfileImage() {
        if (mProfileImage != null) {
            User user = HonkioApi.getActiveUser();
            if (user != null) {
                Picasso.with(getContext())
                        .load(HonkioApi.getUserPhotoUrl(user.getUserId()))
                        .fit()
                        .centerCrop()
                        .transform(ImageUtils.USER_PICTURE_TRANSFORMATION)
                        .into(mProfileImage, new Callback() {
                            @Override
                            public void onSuccess() {
                                mProfileImage.clearColorFilter();
                                mProfileImage.setPadding(0, 0, 0, 0);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    mProfileImage.setImageTintMode(null);
                                }
                            }

                            @Override
                            public void onError() {
                                Context context = getContext();
                                if (context != null) {
                                    TypedValue typedValue = new TypedValue();
                                    context.getTheme().resolveAttribute(
                                            R.attr.hk_settingsIconTintColor, typedValue, true);

                                    mProfileImage.setColorFilter(typedValue.data);
                                    mProfileImage.setImageResource(R.drawable.hk_ic_profile);
                                }
                            }
                        });
            }
        }
    }

    @Override
    public boolean onClickDialog(BaseDialogFragment dialogFragment, int which) {
        if (DLG_TAG_LOGOUT.equals(dialogFragment.getTag())) {
            final Activity activity = getActivity();
            if (activity != null) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    showProgressDialog();
                    new LogoutProcessModel()
                        .logout(new SimpleCallback() {
                            @Override
                            public void onComplete() {
                                HonkioApi.getApiPreferences().edit()
                                        .remove(AppPreferences.PREF_PIN)
                                        .apply();
                                BaseActivity.restart(activity);
                            }

                            @Override
                            public boolean onError(RpError error) {
                                dismissProgressDialog();
                                if (isAdded() && !SettingsFragment.this.onError(error)) {
                                    showMessage(null, 0,
                                            R.string.settings_dlg_logout_fail_message);
                                }
                                return true;
                            }
                        });
                }
            }
        } else
            return false;
        return true;
    }
}
