package com.riskpointer.sdk.app.ui.adapters.binder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.riskpointer.sdk.api.model.entity.MediaFile;
import com.riskpointer.sdk.app.R;
import com.squareup.picasso.Picasso;

/**
 * @author Shurygin Denis
 */

public abstract class HKMediaFilesListBinder<ViewHolder extends HKMediaFilesListBinder.ViewHolder>
        implements ListBinder<MediaFile, ViewHolder> {

    private int mNoImageResId;
    private int mErrorResId;

    public HKMediaFilesListBinder(int noImageResId, int errorResId) {
        super();

        mNoImageResId = noImageResId;
        mErrorResId = errorResId;
    }

    @Override
    public void bind(Context context, ViewHolder holder, MediaFile mediaFile) {
        if (TextUtils.isEmpty(mediaFile.getUrl())) {
            holder.image.setImageResource(mNoImageResId);
        }
        else {
            Picasso.with(context)
                    .load(mediaFile.getUrl())
                    .fit()
                    .centerCrop()
                    .error(mErrorResId)
                    .into(holder.image);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final ImageView image;
        public final ImageView btnMenu;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.hk_imgPicture);
            btnMenu = itemView.findViewById(R.id.hk_btnMenu);

            if (btnMenu != null)
                btnMenu.setVisibility(View.GONE);
        }
    }
}
