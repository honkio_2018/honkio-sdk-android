package com.riskpointer.sdk.app.ui.fragments.list;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.riskpointer.sdk.app.R;

/**
 * @author Shurygin Denis on 2014-11-28.
 */
@Deprecated // Use BaseRecycleListFragment instead
public abstract class BaseListRefFragment extends BaseListFragment implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout mSwipeLayout;

// =================================================================================================
// Abstract methods
// =================================================================================================

    protected abstract void reloadList(Context context);

// =================================================================================================
// Methods of Super Class
// =================================================================================================

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_list_refr;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(),
                container, false);
        mSwipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.hk_swipe_container);
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setColorSchemeResources(R.color.hk_pin_dot_selected,
                R.color.hk_pin_dot_checked,
                R.color.hk_pin_dot_selected,
                android.R.color.white);
        return view;
    }

// =================================================================================================
// Methods from Interfaces
// =================================================================================================

    @Override
    public void onRefresh() {
        setRefreshing(true);
        if (isAdded())
            reloadList(getActivity());
    }

// =================================================================================================
// Public methods and classes
// =================================================================================================

    public void setRefreshing(boolean refreshing) {
        if (mSwipeLayout != null) {
            if (refreshing) {
                if(!mSwipeLayout.isRefreshing())
                    mSwipeLayout.setRefreshing(true);
            }
            else
                mSwipeLayout.setRefreshing(false);
        }
    }
    
    public boolean isRefreshing() {
        return mSwipeLayout != null && mSwipeLayout.isRefreshing();
    }

// =================================================================================================
// Protected methods and classes
// =================================================================================================

// =================================================================================================
// Private methods and classes
// =================================================================================================

}
