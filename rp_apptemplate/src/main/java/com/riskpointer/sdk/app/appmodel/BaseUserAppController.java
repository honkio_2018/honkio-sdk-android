package com.riskpointer.sdk.app.appmodel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.AppInfo;
import com.riskpointer.sdk.api.model.entity.HKDevice;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.Product;
import com.riskpointer.sdk.api.model.entity.ServerFile;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.Transaction;
import com.riskpointer.sdk.api.model.push.entity.TransactionPushEntity;
import com.riskpointer.sdk.api.support.BroadcastManager;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.OrderFilter;
import com.riskpointer.sdk.api.web.message.filter.TransactionFilter;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.FragmentComponents;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;
import com.riskpointer.sdk.app.ui.deprecated.SettingsAccountAddCreditCardFormFragment;
import com.riskpointer.sdk.app.ui.deprecated.SettingsAccountLoanFragment;
import com.riskpointer.sdk.app.ui.dialogs.AccountChooseDialog;
import com.riskpointer.sdk.app.ui.fragments.LoginFragment;
import com.riskpointer.sdk.app.ui.fragments.OAuthWebFragment;
import com.riskpointer.sdk.app.ui.fragments.ShopDetailsFragment;
import com.riskpointer.sdk.app.ui.fragments.ShopProductFragment;
import com.riskpointer.sdk.app.ui.fragments.TransactionDetailsFragment;
import com.riskpointer.sdk.app.ui.fragments.list.BaseOrdersListFragment;
import com.riskpointer.sdk.app.ui.fragments.list.TransactionsListFragment;
import com.riskpointer.sdk.app.ui.fragments.list.TransactionsListUserFragment;
import com.riskpointer.sdk.app.ui.fragments.settings.SettingsAccountAddCreditCardFragment;
import com.riskpointer.sdk.app.ui.fragments.settings.SettingsAccountsListFragment;
import com.riskpointer.sdk.app.ui.fragments.settings.SettingsChangePasswordFragment;
import com.riskpointer.sdk.app.ui.fragments.settings.SettingsChangePinFragment;
import com.riskpointer.sdk.app.ui.fragments.settings.SettingsFragment;
import com.riskpointer.sdk.app.ui.fragments.settings.SettingsLostPasswordFragment;
import com.riskpointer.sdk.app.ui.fragments.settings.SettingsUserFragment;
import com.riskpointer.sdk.app.utils.AppPreferences;
import com.riskpointer.sdk.app.utils.FragmentUtils;

import java.util.HashSet;

/**
 * @author Shurygin Denis on 2015-09-08.
 */
public class BaseUserAppController extends BaseAppController {

    public BaseUserAppController(Context applicationContext, String appUrl, Identity appIdentity) {
        super(applicationContext, appUrl, appIdentity);

        BroadcastManager.getInstance(mAppContext).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                HKDevice device = new HKDevice(HonkioApi.getDevice());
                device.setToken(getGcmRegistrationId());
                updateDevice(device);
            }
        }, new IntentFilter(BroadcastHelper.BROADCAST_ACTION_LOGIN));
    }

    @Override
    public Intent buildNewIntent(int intentId, Object... params) {
        switch (intentId) {
            case ACTIVITY_SPLASH:
                Intent intent = super.buildNewIntent(ACTIVITY_LOGIN, params);
                intent.putExtra(BaseActivity.EXTRA_IGNORE_INITIALIZATION, true);
                return intent;
            case ACTIVITY_LOGIN:
                intent = super.buildNewIntent(intentId, params);
                if (getParam(params, 0) instanceof Boolean)
                    intent.putExtra(LoginFragment.EXTRA_FORCE_LOGIN_SCREEN,
                            (Boolean) getParam(params, 0));
                else
                    intent.putExtra(LoginFragment.EXTRA_FORCE_LOGIN_SCREEN, true);
                return intent;
            case SCREEN_SHOPS_LIST:
                String serviceType = null;
                if (assertClass(params, 0, String.class))
                    serviceType = (String) params[0];

                TransactionFilter filter = new TransactionFilter();
                filter.getShopFilter().setServiceType(serviceType != null ? serviceType : "");

                return buildNewIntent(ACTIVITY_PAGER, serviceType,
                        new FragmentComponents[]{
                                getFragmentComponents(SCREEN_SHOPS_LIST, filter.getShopFilter()),
                                getFragmentComponents(SCREEN_TRANSACTIONS_LIST, null, filter)},
                        new String[]{
                                mAppContext.getString(R.string.main_page_shops),
                                mAppContext.getString(R.string.main_page_history)});

            default:
                return super.buildNewIntent(intentId, params);
        }
    }

    @Override
    public void onGcmRegistrationComplete(String regId) {
        updateDeviceToken(HonkioApi.getDevice(), regId);
    }

    @Override
    public FragmentComponents buildNewFragmentComponents(int fragmentId, Object... params) {
        FragmentComponents components = super.buildNewFragmentComponents(fragmentId, params);
        Bundle args = components.getArguments();
        switch (fragmentId) {
            case ACTIVITY_LOGIN:
                components.setFragmentClass(LoginFragment.class);
                AppInfo appInfo = HonkioApi.getAppInfo();
                if (appInfo != null) {
                    Identity oauthIdentity = appInfo.getOauthIdentity();
                    if (oauthIdentity != null) {
                        args.putString(OAuthWebFragment.CLIENT_ID, oauthIdentity.getId());
                        args.putString(OAuthWebFragment.CLIENT_SECRET, oauthIdentity.getPassword());
                    }
                }
                break;
            case SCREEN_SHOP_TRANSACTION:
                components.setFragmentClass(TransactionDetailsFragment.class);
                if (isHasParam(params, 0)) {
                    Object param = params[0];
                    if (param != null) {
                        if (param instanceof Transaction) {
                            args.putSerializable(TransactionDetailsFragment.EXTRA_TRANSACTION, (Transaction) param);
                        } else if (param instanceof TransactionPushEntity) {
                            args.putSerializable(TransactionDetailsFragment.EXTRA_TRANSACTION_PUSH, (TransactionPushEntity) param);
                        }
                    }
                }
                break;
            case SCREEN_SETTINGS_MAIN:
                components.setFragmentClass(SettingsFragment.class);
                break;
            case SCREEN_SETTINGS_USER_ACCOUNTS:
                components.setFragmentClass(SettingsAccountsListFragment.class);
                break;
            case SCREEN_SETTINGS_CHANGE_PASSWORD:
                components.setFragmentClass(SettingsChangePasswordFragment.class);
                putBoolean(args, SettingsChangePasswordFragment.ARG_IGNORE_OLD, params, 0);
                break;
            case SCREEN_SETTINGS_LOST_PASSWORD:
                components.setFragmentClass(SettingsLostPasswordFragment.class);
                putString(args, SettingsLostPasswordFragment.ARG_EMAIL, params, 0);
                break;
            case SCREEN_SETTINGS_CHANGE_PIN:
                components.setFragmentClass(SettingsChangePinFragment.class);
                break;
            case SCREEN_SETTINGS_USER_ACCOUNT_LOAN:
                components.setFragmentClass(SettingsAccountLoanFragment.class);
                if (assertClass(params, 0, ServerFile.class, false) && assertClass(params, 1, String.class, false)) {
                    putSerializable(args, SettingsAccountLoanFragment.EXTRA_USER_FILE, params, 0, ServerFile.class);
                    putString(args, SettingsAccountLoanFragment.EXTRA_LICENSE_KEY, params, 1);
                }
                break;
            case SCREEN_SETTINGS_USER_ADD_ACCOUNT:
                if (HonkioApi.getServerInfo().getCreditCallInfo() != null) {
                    components.setFragmentClass(SettingsAccountAddCreditCardFormFragment.class);
                } else {
                    components.setFragmentClass(SettingsAccountAddCreditCardFragment.class);
                }
                break;
            case SCREEN_SETTINGS_USER_PROFILE:
                components.setFragmentClass(SettingsUserFragment.class);
                break;
            case SCREEN_TRANSACTIONS_LIST:
                components.setFragmentClass(TransactionsListUserFragment.class);
                putSerializable(args, FragmentUtils.ARG_SHOP, params, 0, Shop.class);
                putSerializable(args, TransactionsListFragment.ARG_FILTER, params, 1, TransactionFilter.class);
                break;
            case SCREEN_ORDERS_LIST:
                putSerializable(args, FragmentUtils.ARG_SHOP, params, 0, Shop.class);
                putSerializable(args, BaseOrdersListFragment.ARG_FILTER, params, 1, OrderFilter.class);
                break;
            case FRAGMENT_SHOP_DETAILS:
                components.setFragmentClass(ShopDetailsFragment.class);
                putSerializable(args, FragmentUtils.ARG_SHOP, params, 0, Shop.class);
                break;
            case SCREEN_SHOP_PRODUCT:
                components.setFragmentClass(ShopProductFragment.class);
                if (assertClass(params, 0, Shop.class, false) && assertClass(params, 1, Product.class)) {
                    putSerializable(args, FragmentUtils.ARG_SHOP, params, 0, Shop.class);
                    putSerializable(args, FragmentUtils.ARG_PRODUCT, params, 1, Product.class);
                }
                break;
            case DIALOG_FRAGMENT_ACCOUNT_CHOOSE:
                components.setFragmentClass(AccountChooseDialog.class);
                putSerializable(args, AccountChooseDialog.ARG_SHOP, params, 0, Shop.class);
                putDouble(args, AccountChooseDialog.ARG_AMOUNT, params, 1);
                putString(args, AccountChooseDialog.ARG_CURRENCY, params, 2);
                putSerializable(args, AccountChooseDialog.ARG_DISALLOWED_ACCOUNTS, params, 3, HashSet.class);
                break;
        }
        return components;
    }

    @Override
    protected void onInitialized() {
        super.onInitialized();

        if (HonkioApi.getActiveConnection() != null && HonkioApi.getActiveConnection().isEncrypted()) {
            SharedPreferences prefs = HonkioApi.getApiPreferences();
            if(prefs.getBoolean(AppPreferences.PREF_SAVE_PIN, AppPreferences.PREF_SAVE_PIN_DEFAULT)
                    && prefs.contains(AppPreferences.PREF_PIN)) {
                HonkioApi.changePin(prefs.getString(AppPreferences.PREF_PIN, ""));
            }
        }

        updateDeviceToken(HonkioApi.getDevice(), getGcmRegistrationId());
    }

    private void updateDeviceToken(HKDevice device, String token) {
        if (token != null && device != null && !token.equals(device.getToken())) {
            final HKDevice newDevice = new HKDevice(device);
            newDevice.setToken(token);
            updateDevice(newDevice);
        }
    }

    private void updateDevice(final HKDevice device) {
        if (!HonkioApi.isConnected())
            return;

        if (device != null) {
            HonkioApi.userSetDevice(device, Message.FLAG_LOW_PRIORITY,
                    new RequestCallback<HKDevice>() {

                        private int mCounter = 3;

                        @Override
                        public boolean onComplete(Response<HKDevice> response) {
                            return response.isStatusAccept() || onError(response.getAnyError());
                        }

                        @Override
                        public boolean onError(RpError error) {
                            switch (error.code) {
                                case RpError.Api.ABORTED:
                                case RpError.Api.BAD_RESPONSE:
                                    return true;
                                case RpError.Api.NO_CONNECTION:
                                    if (!repeatUpdate())
                                        Toast.makeText(mAppContext,
                                                R.string.dlg_setup_notifications_failure_message,
                                                Toast.LENGTH_SHORT).show();
                                    return true;
                                case RpError.Common.INVALID_USER_LOGIN:
                                    reRegisterDevice(device);
                                    return true;
                            }
                            Toast.makeText(mAppContext,
                                    R.string.dlg_setup_notifications_failure_message,
                                    Toast.LENGTH_SHORT).show();

                            return true;
                        }

                        private boolean repeatUpdate() {
                            if (mCounter > 0) {
                                mCounter--;
                                final RequestCallback<HKDevice> callback = this;
                                new Thread() {
                                    @Override
                                    public void run() {
                                        try {
                                            Thread.sleep(10000);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                            return;
                                        }
                                        HonkioApi.userSetDevice(device,
                                                Message.FLAG_LOW_PRIORITY, callback);
                                    }
                                }.start();
                                return true;
                            }
                            return false;
                        }
                    });
        }
    }

    private void reRegisterDevice(HKDevice device) {
        HKDevice newDevice = new HKDevice(device);
        newDevice.setId(null);
        HonkioApi.userSetDevice(newDevice, Message.FLAG_HIGH_PRIORITY, new RequestCallback<HKDevice>() {
            @Override
            public boolean onComplete(Response<HKDevice> response) {
                if (response.isStatusAccept())
                    return true;
                return onError(response.getAnyError());
            }

            @Override
            public boolean onError(RpError error) {
                Toast.makeText(mAppContext,
                        R.string.dlg_setup_notifications_failure_message,
                        Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }
}
