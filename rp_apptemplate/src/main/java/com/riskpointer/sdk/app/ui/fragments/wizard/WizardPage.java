package com.riskpointer.sdk.app.ui.fragments.wizard;

/**
 * @author Shurygin Denis
 */

public interface WizardPage<WizardObject> {
    boolean isValid();

    WizardObject getWizardObject();

    void setWizardObject(WizardObject object);
}
