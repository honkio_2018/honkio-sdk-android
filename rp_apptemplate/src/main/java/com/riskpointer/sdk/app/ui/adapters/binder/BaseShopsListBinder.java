package com.riskpointer.sdk.app.ui.adapters.binder;

import android.location.Location;
import android.support.v7.widget.RecyclerView;

/**
 * Created by I.N. on 20.07.2016.
 */
public interface BaseShopsListBinder<ListItem, ViewHolder extends RecyclerView.ViewHolder> extends ListBinder<ListItem, ViewHolder> {

    void setLocation(Location location);
    Location getLocation();

    void setServiceTypeVisible(boolean visible);
}
