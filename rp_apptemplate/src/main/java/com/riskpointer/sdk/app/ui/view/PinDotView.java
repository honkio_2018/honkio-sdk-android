package com.riskpointer.sdk.app.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author Shurygin Denis on 2015-04-23.
 */
public class PinDotView extends View {

    public static final int[] STATE_DISABLE = new int[] {-android.R.attr.state_checked};
    public static final int[] STATE_ACTIVE = new int[] {android.R.attr.state_selected};
    public static final int[] STATE_DONE = new int[] {android.R.attr.state_checked};

    private int[] mAdditionalState;

    public PinDotView(Context context) {
        super(context);
    }

    public PinDotView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PinDotView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public PinDotView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        if (mAdditionalState != null && mAdditionalState.length > 0) {
            final int[] drawableState = super.onCreateDrawableState(extraSpace + mAdditionalState.length);

            mergeDrawableStates(drawableState, mAdditionalState);
            return drawableState;
        } else {
            return super.onCreateDrawableState(extraSpace);
        }
    }

    public void setState(int[] state) {
        mAdditionalState = state;
        refreshDrawableState();
    }
}
