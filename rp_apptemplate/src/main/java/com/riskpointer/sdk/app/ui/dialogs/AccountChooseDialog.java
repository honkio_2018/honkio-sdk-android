package com.riskpointer.sdk.app.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.MerchantSimple;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.adapters.BaseRecyclerListAdapter;
import com.riskpointer.sdk.app.ui.adapters.UserAccountsAdapter;
import com.riskpointer.sdk.app.ui.adapters.binder.UserAccountItemBinder;
import com.riskpointer.sdk.app.ui.adapters.recyclerdecorations.RecyclerDividerDefaultDecoration;
import com.riskpointer.sdk.app.utils.AppPreferences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

/**
 * @author Shurygin Denis
 */

public class AccountChooseDialog extends BaseDialogFragment {
    public static final String ARG_SHOP = "AccountChooseDialog.ARG_SHOP";
    public static final String ARG_AMOUNT = "AccountChooseDialog.ARG_AMOUNT";
    public static final String ARG_CURRENCY = "AccountChooseDialog.ARG_CURRENCY";
    public static final String ARG_DISALLOWED_ACCOUNTS = "AccountChooseDialog.ARG_DISALLOWED_ACCOUNTS";

    private static final int REQUEST_CREATE_CARD = 1;

    private UserAccount mSelectedAccount;

    private HashSet<String> mDisallowedAccounts = null;
    private MerchantSimple mMerchant;
    private double mAmount = 0;
    private String mCurrency = null;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mMerchant = HonkioApi.getMainShopInfo().getMerchant();
        Bundle args = getArguments();
        if (args != null) {
            if (args.containsKey(ARG_SHOP)) {
                Shop argsShop = (Shop) args.getSerializable(ARG_SHOP);
                if (argsShop != null && argsShop.getMerchant() != null) {
                    mMerchant = argsShop.getMerchant();
                }
            }
            if (args.containsKey(ARG_AMOUNT)) mAmount = args.getDouble(ARG_AMOUNT, 0);
            if (args.containsKey(ARG_CURRENCY)) mCurrency = args.getString(ARG_CURRENCY);
            mDisallowedAccounts = (HashSet<String>) args.getSerializable(ARG_DISALLOWED_ACCOUNTS);
        }

        if (mDisallowedAccounts == null)
            mDisallowedAccounts = new HashSet<>();

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), getTheme());

        UserAccountItemBinder binder = new UserAccountItemBinder() {
            @Override
            public void bind(Context context, ViewHolder holder, UserAccount account) {
                holder.desc.setVisibility(View.VISIBLE);

                super.bind(context, holder, account);

                if (isDummyCard(account)) {
                    holder.name.setText(R.string.pay_accounts_list_btn_card_add);
                    holder.desc.setVisibility(View.GONE);
                }

                if (isEnabled(account)) {
                    holder.setEnabled(true);
                    holder.warn.setVisibility(View.GONE);
                }
                else {
                    holder.setEnabled(false);
                    holder.warn.setVisibility(View.VISIBLE);

                    if (mDisallowedAccounts.contains(account.getType()))
                        holder.warn.setText(R.string.pay_accounts_list_warn_disallowed);
                    else if (!account.enoughLeft(mAmount))
                        holder.warn.setText(R.string.pay_accounts_list_warn_not_enough_left);
                    else if (!mMerchant.isAccountSupported(account.getType())) {
                        holder.warn.setText(R.string.pay_accounts_list_warn_not_supported_by_merchant);
                    }
                    else
                        holder.warn.setVisibility(View.GONE);
                }
            }
        };
        UserAccountsAdapter adapter = new UserAccountsAdapter(getContext(), buildList(),
                R.layout.hk_list_item_user_account, binder);
        adapter.addOnItemClickListener(new BaseRecyclerListAdapter
                .OnItemClickListener<UserAccount, UserAccountItemBinder.ViewHolder>() {
            @Override
            public void onItemClick(UserAccountItemBinder.ViewHolder itemHolder, View view, UserAccount account) {
                if (isEnabled(account)) {
                    if (isDummyCard(account)) {
                        startActivityForResult(AppController.getIntent(
                                AppController.SCREEN_SETTINGS_USER_ADD_ACCOUNT), REQUEST_CREATE_CARD);
                    } else {
                        onAccountSelected(account);
                    }
                }
            }
        });

        View view = LayoutInflater.from(getContext())
                .inflate(R.layout.hk_dialog_fragment_account_choose, null, false);

        RecyclerView list = view.findViewById(R.id.hk_listRecycler);
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        list.addItemDecoration(new RecyclerDividerDefaultDecoration());
        list.setAdapter(adapter);

        builder.setView(view);
        builder.setTitle(R.string.pay_accounts_list_title);

        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP)
                    onClick(dialog, DialogInterface.BUTTON_NEUTRAL);
                return false;
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CREATE_CARD) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                UserAccount account = (UserAccount)
                        data.getSerializableExtra(AppController.Extra.USER_ACCOUNT);

                if (account != null)
                    onAccountSelected(account);
            }
        }
        else
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mSelectedAccount != null)
            onAccountSelected(mSelectedAccount);
    }

    public UserAccount getSelectedAccount() {
        return mSelectedAccount;
    }

    protected void onAccountSelected(UserAccount account) {
        mSelectedAccount = account;
        if (isResumed()) {
            onClick(DialogInterface.BUTTON_POSITIVE);
            dismiss();
        }
    }

    private List<UserAccount> buildList() {
        if (HonkioApi.isConnected()) {

            List<UserAccount> list = new AppPreferences(getContext()).getPreferredAccounts();
            if (list != null && list.size() > 0)
                list = filter(list);

            if (list == null || list.size() == 0)
                list = filter(HonkioApi.getActiveUser().getAccountsList());

            if (list != null) {
                sort(list);
                list.add(0, new UserAccount(UserAccount.Type.CREDITCARD, UserAccount.ZERO_NUMBER));
            }

            return list;
        }
        return null;
    }

    private List<UserAccount> filter(List<UserAccount> list) {
        List<UserAccount> newList = new ArrayList<>();
        for (UserAccount account: list) {
            if (!account.isDummy()) {
                if (account.typeIs(UserAccount.Type.INVOICE, UserAccount.Type.BONUS)) {
                    if (mMerchant.getId().equals(account.getNumber()))
                        newList.add(account);
                }
                else {
                    newList.add(account);
                }
            }
        }

        return newList;
    }

    private void sort(List<UserAccount> list) {
        Collections.sort(list, new Comparator<UserAccount>() {
            @Override
            public int compare(UserAccount o1, UserAccount o2) {
                return value(o1) - value(o2);
            }

            private int value(UserAccount account) {
                if (account.typeIs(UserAccount.Type.CREDITCARD))
                    return 1;
                return 2;
            }
        });
    }

    private boolean isEnabled(UserAccount account) {
        if (!mDisallowedAccounts.contains(account.getType())) {
            if (mMerchant.isAccountSupported(account.getType())) {
                if (account.enoughLeft(mAmount)) {
                    return true;
                }
            }
        }

        return false;
    }

    private boolean isDummyCard(UserAccount account) {
        return account.typeIs(UserAccount.Type.CREDITCARD) &&
                UserAccount.ZERO_NUMBER.equals(account.getNumber());
    }
}
