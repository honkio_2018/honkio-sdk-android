package com.riskpointer.sdk.app.ui.strategy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.ui.strategy.ViewLifeCycleObserver.ViewController;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Shurygin Denis
 */

public class ViewLifeCycleObserversSet {

    private static final String SAVED_LCO_TAGS = "ViewLifeCycleObserversSet.SAVED_LCO_TAGS";
    private static final String SAVED_LCO_CLASSES = "ViewLifeCycleObserversSet.SAVED_LCO_CLASSES";
    private static final String SAVED_LCO_BUNDLES = "ViewLifeCycleObserversSet.SAVED_LCO_BUNDLES";

    private final Map<String, ViewLifeCycleObserver> mLifeCycleObservers = new HashMap<>();
    private final List<ViewLifeCycleObserver> mObserversToAdd = new ArrayList<>();

    private volatile boolean isMapProcessing = false;

    private final ViewController viewController;

    public ViewLifeCycleObserversSet(ViewController viewController) {
        this.viewController = viewController;
    }

    public Bundle saveState() {
        Bundle outState = new Bundle();
        synchronized (mLifeCycleObservers) {
            ArrayList<String> lcoTags = new ArrayList<>(mLifeCycleObservers.size());
            ArrayList<Class> lcoClasses = new ArrayList<>(mLifeCycleObservers.size());
            ArrayList<Bundle> lcoBundles = new ArrayList<>(mLifeCycleObservers.size());
            for (ViewLifeCycleObserver observer : mLifeCycleObservers.values()) {
                // Save observer into bundle
                lcoTags.add(observer.getTag());

                lcoClasses.add(observer.getClass());

                Bundle bundle = new Bundle();
                observer.saveState(this.viewController, bundle);
                lcoBundles.add(bundle);
            }

            outState.putSerializable(SAVED_LCO_TAGS, lcoTags);
            outState.putSerializable(SAVED_LCO_CLASSES, lcoClasses);
            outState.putSerializable(SAVED_LCO_BUNDLES, lcoBundles);
        }
        return outState;
    }

    public void restoreState(Bundle savedInstanceState) {
        @SuppressWarnings("unchecked")
        ArrayList<String> lcoTags = (ArrayList<String>)
                savedInstanceState.getSerializable(SAVED_LCO_TAGS);

        @SuppressWarnings("unchecked")
        ArrayList<Class> lcoClasses = (ArrayList<Class>)
                savedInstanceState.getSerializable(SAVED_LCO_CLASSES);

        @SuppressWarnings("unchecked")
        ArrayList<Bundle> lcoBundles = (ArrayList<Bundle>)
                savedInstanceState.getSerializable(SAVED_LCO_BUNDLES);

        synchronized (mLifeCycleObservers) {
            if (lcoTags != null && lcoClasses != null && lcoBundles != null) {
                for (int i = 0; i < lcoTags.size(); i++) {
                    try {
                        ViewLifeCycleObserver lifeCycleObserver = mLifeCycleObservers.get(lcoTags.get(i));
                        boolean isNewInstance = false;
                        if (lifeCycleObserver == null) {
                            lifeCycleObserver = (ViewLifeCycleObserver) lcoClasses.get(i).newInstance();
                            isNewInstance = true;
                        }
                        lifeCycleObserver.restoreState(this.viewController, lcoBundles.get(i));
                        if (isNewInstance)
                            synchronized (mLifeCycleObservers) {
                                mLifeCycleObservers.put(lifeCycleObserver.getTag(), lifeCycleObserver);
                            }
                    } catch (java.lang.InstantiationException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void addLifeCycleObserver(ViewLifeCycleObserver observer) {
        // Check observer class
        Class<?> observerClass = observer.getClass();
        if (observerClass.isMemberClass() && (!Modifier.isStatic(observerClass.getModifiers()) ||
                !Modifier.isPublic(observerClass.getModifiers()))) {
            throw new IllegalArgumentException("Observer class must be public static if it's member class.");
        }
        else if (!Modifier.isPublic(observerClass.getModifiers())) {
            throw new IllegalArgumentException("Observer class must be public.");
        }

        synchronized (mLifeCycleObservers) {
            if (!isMapProcessing)
                mLifeCycleObservers.put(observer.getTag(), observer);
            else
                mObserversToAdd.add(observer);
        }

        observer.onAttachToController(this.viewController);
    }

    public <T extends ViewLifeCycleObserver> T getLifeCycleObserver(String tag) {
        synchronized (mLifeCycleObservers) {
            //noinspection unchecked
            return (T) mLifeCycleObservers.get(tag);
        }
    }

    public void removeLifeCycleObserver(final String tag) {
        new Thread() {
            @Override
            public void run() {
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        synchronized (mLifeCycleObservers) {
                            ViewLifeCycleObserver observer = mLifeCycleObservers.remove(tag);
                            observer.onDetachFromController(null);
                        }
                    }
                };
                Activity activity = viewController.getActivity();
                if (activity != null)
                    activity.runOnUiThread(runnable);
                else
                    runnable.run();
            }
        }.start();
    }

    public void removeLifeCycleObserver(ViewLifeCycleObserver observer) {
        removeLifeCycleObserver(observer.getTag());
    }

    public void onWillCreate(final Bundle savedInstanceState) {
        visitAll(new Visitor1() {
            @Override
            public void visit(ViewLifeCycleObserver observer) {
                observer.onWillCreate(viewController, savedInstanceState);
            }
        });
    }

    public void onCreate(final Bundle savedInstanceState) {
        visitAll(new Visitor1() {
            @Override
            public void visit(ViewLifeCycleObserver observer) {
                observer.onCreate(viewController, savedInstanceState);
            }
        });
    }

    public void onViewCreated(final Bundle savedInstanceState) {
        visitAll(new Visitor1() {
            @Override
            public void visit(ViewLifeCycleObserver observer) {
                observer.onViewCreated(viewController, savedInstanceState);
            }
        });
    }

    public void onStart() {
        visitAll(new Visitor1() {
            @Override
            public void visit(ViewLifeCycleObserver observer) {
                observer.onStart(viewController);
            }
        });
    }

    public void onResume() {
        visitAll(new Visitor1() {
            @Override
            public void visit(ViewLifeCycleObserver observer) {
                observer.onResume(viewController);
            }
        });
    }

    public void onPause() {
        visitAll(new Visitor1() {
            @Override
            public void visit(ViewLifeCycleObserver observer) {
                observer.onPause(viewController);
            }
        });
    }

    public void onStop() {
        visitAll(new Visitor1() {
            @Override
            public void visit(ViewLifeCycleObserver observer) {
                observer.onStop(viewController);
            }
        });
    }

    public void onDestroy() {
        visitAll(new Visitor1() {
            @Override
            public void visit(ViewLifeCycleObserver observer) {
                observer.onDestroy(viewController);
            }
        });
    }

    public void onActivityResult(final int requestCode, final int resultCode, final Intent data){
        visitAll(new Visitor1() {
            @Override
            public void visit(ViewLifeCycleObserver observer) {
                observer.onActivityResult(viewController, requestCode, resultCode, data);
            }
        });
    }

    public boolean onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
        return visitAll(new Visitor2() {
            @Override
            public boolean visit(ViewLifeCycleObserver observer) {
                return observer.onCreateOptionsMenu(viewController, menu, inflater);
            }
        });
    }

    public boolean onOptionsItemSelected(final MenuItem item) {
        return visitAny(new Visitor2() {
            @Override
            public boolean visit(ViewLifeCycleObserver observer) {
                return observer.onOptionsItemSelected(viewController, item);
            }
        });
    }

    public boolean onBackPressed() {
        return visitAny(new Visitor2() {
            @Override
            public boolean visit(ViewLifeCycleObserver observer) {
                return observer.onBackPressed(viewController);
            }
        });
    }

    public boolean onDismissDialog(final BaseDialogFragment dialogFragment) {
        return visitAny(new Visitor2() {
            @Override
            public boolean visit(ViewLifeCycleObserver observer) {
                return observer.onDismissDialog(viewController, dialogFragment);
            }
        });
    }

    public boolean onClickDialog(final BaseDialogFragment dialogFragment, final int which) {
        return visitAny(new Visitor2() {
            @Override
            public boolean visit(ViewLifeCycleObserver observer) {
                return observer.onClickDialog(viewController, dialogFragment, which);
            }
        });
    }

    public void onPermissionDenied(final String permission, final int requestCode) {
        visitAll(new Visitor1() {
            @Override
            public void visit(ViewLifeCycleObserver observer) {
                observer.onPermissionDenied(viewController, permission, requestCode);
            }
        });
    }

    public void onPermissionGranted(final String permission, final int requestCode) {
        visitAll(new Visitor1() {
            @Override
            public void visit(ViewLifeCycleObserver observer) {
                observer.onPermissionGranted(viewController, permission, requestCode);
            }
        });
    }


    public boolean onActionClick(final int actionId) {
        return visitAny(new Visitor2() {
            @Override
            public boolean visit(ViewLifeCycleObserver observer) {
                return observer.onActionClick(viewController, actionId);
            }
        });
    }

    private interface Visitor1 {
        void visit(ViewLifeCycleObserver observer);
    }

    private interface Visitor2 {
        boolean visit(ViewLifeCycleObserver observer);
    }

    private void visitAll(Visitor1 visitor) {
        synchronized (mLifeCycleObservers) {
            isMapProcessing = true;

            for (ViewLifeCycleObserver observer : mLifeCycleObservers.values())
                visitor.visit(observer);

            addAll(mLifeCycleObservers, mObserversToAdd);
            isMapProcessing = false;

            for (ViewLifeCycleObserver observer : mObserversToAdd)
                visitor.visit(observer);

            mObserversToAdd.clear();
        }
    }

    private boolean visitAll(Visitor2 visitor) {
        boolean result = false;
        synchronized (mLifeCycleObservers) {
            isMapProcessing = true;

            for (ViewLifeCycleObserver observer : mLifeCycleObservers.values())
                result = result | visitor.visit(observer);

            addAll(mLifeCycleObservers, mObserversToAdd);
            isMapProcessing = false;

            for (ViewLifeCycleObserver observer : mObserversToAdd)
                result = result | visitor.visit(observer);

            mObserversToAdd.clear();
        }
        return result;
    }

    private boolean visitAny(Visitor2 visitor) {
        synchronized (mLifeCycleObservers) {
            isMapProcessing = true;

            for (ViewLifeCycleObserver observer : mLifeCycleObservers.values())
                if (visitor.visit(observer)) {
                    isMapProcessing = false;
                    return true;
                }

            addAll(mLifeCycleObservers, mObserversToAdd);
            isMapProcessing = false;

            for (ViewLifeCycleObserver observer : mObserversToAdd)
                if (visitor.visit(observer)) {
                    mObserversToAdd.clear();
                    return true;
                }

            mObserversToAdd.clear();
        }
        return false;
    }

    private void addAll(Map<String, ViewLifeCycleObserver> map, List<ViewLifeCycleObserver> list) {
        for (ViewLifeCycleObserver observer : list)
            map.put(observer.getTag(), observer);
    }
}
