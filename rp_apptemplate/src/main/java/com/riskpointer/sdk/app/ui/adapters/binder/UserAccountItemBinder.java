package com.riskpointer.sdk.app.ui.adapters.binder;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.utils.ImageUtils;
import com.riskpointer.sdk.app.utils.UserAccountFormatter;

/**
 * @author Shurygin Denis
 */

public class UserAccountItemBinder implements ListBinder<UserAccount, UserAccountItemBinder.ViewHolder> {

    @Override
    public void bind(Context context, ViewHolder holder, UserAccount account) {
        if (account != null) {

            if (holder.name != null)
                holder.name.setText(UserAccountFormatter.getAccountName(context, account));

            if (holder.desc != null)
                holder.desc.setText(UserAccountFormatter.getAccountDesc(context, account));

            if (holder.logo != null)
                holder.logo.setImageResource(UserAccountFormatter.getAccountLogo(context, account));

            if (holder.logo2 != null) {
                if (!TextUtils.isEmpty(account.getLogoUrl())) {
                    holder.logo2.setImageDrawable(null);
                    holder.logo2.setVisibility(View.VISIBLE);
                    ImageUtils.loadImage(context, holder.logo2, null, account.getLogoUrl(), 0);
                } else {
                    holder.logo2.setVisibility(View.GONE);
                }
            }

            holder.setEnabled(account.isEnabled());
        }
    }

    @Override
    public ViewHolder newHolder(View view) {
        return new ViewHolder(view);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        protected static final float DISABLE_ALPHA = 0.5f;

        private boolean isEnabled;

        public final TextView name;
        public final TextView desc;
        public final TextView warn;
        public final ImageView logo;
        public final ImageView logo2;

        public ViewHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.hk_txtName);
            desc = (TextView) view.findViewById(R.id.hk_txtDesc);
            warn = (TextView) view.findViewById(R.id.hk_txtWarning);
            logo = (ImageView) view.findViewById(R.id.hk_imgLogo);
            logo2 = (ImageView) view.findViewById(R.id.hk_imgLogo2);
        }

        public void setEnabled(boolean isEnabled) {
            this.isEnabled = isEnabled;
            if (isEnabled) {
                setViewAlpha(this.name, 1);
                setViewAlpha(this.desc, 1);
                setViewAlpha(this.warn, 1);
                setViewAlpha(this.logo, 1);
                this.itemView.setEnabled(true);
            }
            else {
                setViewAlpha(this.name, DISABLE_ALPHA);
                setViewAlpha(this.desc, DISABLE_ALPHA);
                setViewAlpha(this.warn, DISABLE_ALPHA);
                setViewAlpha(this.logo, DISABLE_ALPHA);
                this.itemView.setEnabled(false);
            }
        }

        public boolean isEnabled() {
            return isEnabled;
        }

        protected void setViewAlpha(View view, float alpha) {
            if (view != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                view.setAlpha(alpha);
        }
    }
}
