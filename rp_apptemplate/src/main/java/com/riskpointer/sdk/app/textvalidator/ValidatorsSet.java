/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.textvalidator;

import android.view.View;
import android.view.View.OnFocusChangeListener;

import java.util.ArrayList;
import java.util.List;

public class ValidatorsSet implements OnFocusChangeListener  {

	List<BaseTextValidator> mValidators = new ArrayList<>();

	public boolean validate(View view) {
		for(BaseTextValidator validator: mValidators) {
			if(!validator.validate(view))
				return false;
		}
		return true;
	}

	@Override
	public void onFocusChange(View view, boolean hasFocus) {
		validate(view);
	}

	public void add(BaseTextValidator validator) {
		if (!mValidators.contains(validator))
			mValidators.add(validator);
	}
	
	public void remove(BaseTextValidator validator) {
		mValidators.remove(validator);
	}
	
	public void clear() {
		mValidators.clear();
	}

}
