package com.riskpointer.sdk.app.ui.adapters.recyclerdecorations;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by Shurygin Denis on 2016-07-25.
 */
public class RecyclerHeaderDecoration extends RecyclerView.ItemDecoration {

    private View mView;

    public RecyclerHeaderDecoration(Context context, RecyclerView parent, int resId) {
        this(LayoutInflater.from(context).inflate(resId, parent, false));
    }

    public RecyclerHeaderDecoration(View view) {
        mView = view;
        mView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
    }

    @Override
    public void onDraw(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
        super.onDraw(canvas, parent, state);
        // layout basically just gets drawn on the reserved space on top of the first view
        mView.layout(parent.getLeft(), 0, parent.getRight(), mView.getMeasuredHeight());
        for (int i = 0; i < parent.getChildCount(); i++) {
            View view = parent.getChildAt(i);
            if (parent.getChildAdapterPosition(view) == 0) {
                canvas.save();
                final int height = mView.getMeasuredHeight();
                final int top = view.getTop() - height;
                canvas.translate(0, top);
                mView.draw(canvas);
                canvas.restore();
                break;
            }
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.set(0, mView.getMeasuredHeight(), 0, 0);
        } else {
            outRect.setEmpty();
        }
    }
}