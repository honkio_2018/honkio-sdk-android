package com.riskpointer.sdk.app.ui.fragments.list;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Inventory;
import com.riskpointer.sdk.api.model.entity.InventoryProduct;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.support.BroadcastManager;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.adapters.BaseRecyclerListAdapter;
import com.riskpointer.sdk.app.ui.adapters.InventoryProductsListAdapter;
import com.riskpointer.sdk.app.ui.adapters.binder.BoughtProductListBinder;

import java.util.List;

/**
 * Created by Shurygin Denis on 2016-11-03.
 */

public abstract class BaseInventoryListFragment extends BaseRecycleLazyListFragment<InventoryProduct> {

    public abstract void onItemClick(InventoryProduct item);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        BroadcastManager broadcastManager = BroadcastManager.getInstance(getContext());
        broadcastManager.registerReceiver(mInventoryUpdateListener, new IntentFilter(
                BroadcastHelper.getActionOnComplete(Message.Command.USER_PAYMENT)));
        broadcastManager.registerReceiver(mInventoryUpdateListener, new IntentFilter(
                BroadcastHelper.getActionOnComplete(Message.Command.USER_INVENTORY_CONVERT)));
    }

    @Override
    public void onDestroy() {
        BroadcastManager.getInstance(getActivity()).unregisterReceiver(mInventoryUpdateListener);
        super.onDestroy();
    }

    @Override
    public String getTitle(Context context) {
        return context.getString(R.string.screen_inventory_title);
    }

    @Override
    protected int getDefaultListItemLayoutId(int type) {
        return R.layout.hk_list_item_inventory;
    }

    @Override
    public ListLoader<InventoryProduct, Inventory> onCreateLoader(int id, Bundle args) {
        return new InventoryListLoader(getContext());
    }

    @Override
    protected BaseRecyclerListAdapter<InventoryProduct, ?> newAdapter(List<InventoryProduct> list) {
        InventoryProductsListAdapter adapter
                = new InventoryProductsListAdapter(getContext(), list, getListItemLayoutId(0));
        adapter.addOnItemClickListener(new BaseRecyclerListAdapter
                .OnItemClickListener<InventoryProduct, BoughtProductListBinder.ViewHolder>() {
            @Override
            public void onItemClick(BoughtProductListBinder.ViewHolder itemHolder,
                                    View view, InventoryProduct product) {
                BaseInventoryListFragment.this.onItemClick(product);
            }
        });
        return adapter;
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager(Context context) {
        return new LinearLayoutManager(context);
    }

    public static class InventoryListLoader extends ListLoader<InventoryProduct, Inventory> {

        public InventoryListLoader(Context context) {
            super(context);
        }

        @Override
        protected List<InventoryProduct> toList(Response<Inventory> response) {
            return response.getResult().groupByProduct();
        }

        @Override
        protected Task loadList(Context context, RequestCallback<Inventory> callback,
                                int count, int skip) {
            if (skip == 0)
                return HonkioApi.userGetInventory(0, callback);

            Response<Inventory> response = new Response<>(new Inventory());
            Response.Editor.setStatus(response, Response.Status.ACCEPT);
            callback.onComplete(response);
            return null;
        }
    }

    private BroadcastReceiver mInventoryUpdateListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            reloadList();
        }
    };

}
