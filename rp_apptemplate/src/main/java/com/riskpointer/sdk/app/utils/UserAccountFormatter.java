package com.riskpointer.sdk.app.utils;

import android.annotation.SuppressLint;
import android.content.Context;

import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.api.model.entity.UserAccount.Type;
import com.riskpointer.sdk.app.R;

/**
 * @author Shurygin Denis
 */

public class UserAccountFormatter {

    public interface Format {

        String getAccountName(Context context, UserAccount account);
        String getAccountDesc(Context context, UserAccount account);
        int getAccountLogo(Context context, UserAccount account);

        String formatAmount(Context context, UserAccount account);

    }

    public static class FormatImpl implements Format {

        @Override
        public String getAccountName(Context context, UserAccount account) {
            if (account.getType() != null) {
                if (account.typeIs(Type.X_BILLING))
                    return context.getResources().getString(
                            R.string.account_name_xbilling);
                else if (account.typeIs(Type.BONUS))
                    return context.getResources().getString(
                            R.string.account_name_bonus);
                else if (account.typeIs(Type.OPERATOR))
                    return context.getResources().getString(
                            R.string.account_name_operator);
                else if (account.typeIs(Type.CREDITCARD))
                    return context.getResources().getString(
                            R.string.account_name_creditcard);
                else if (account.typeIs(Type.INVOICE))
                    return context.getResources().getString(
                            R.string.account_name_merchant_invoice);
                return account.getType();
            }
            return "unknown";
        }

        @Override
        public int getAccountLogo(Context context, UserAccount account) {
            if (account.typeIs(Type.CREDITCARD)) {
                String card = account.getDescription();
                if (card != null) {
                    if (card.startsWith("3")) {
                        return R.drawable.hk_ic_card_american;
                    } else if (card.startsWith("4")) {
                        return R.drawable.hk_ic_card_visa;
                    } else if (card.startsWith("5")) {
                        return R.drawable.hk_ic_card_master;
                    } else if (card.startsWith("6")) {
                        return R.drawable.hk_ic_card_discover;
                    }
                }
                return R.drawable.hk_ic_account_card;
            }
            else if (account.typeIs(Type.INVOICE))
                return R.drawable.hk_ic_account_invoice;
            else if (account.typeIs(Type.OPERATOR))
                return R.drawable.hk_ic_account_operator;
            else if (account.typeIs(Type.BONUS))
                return R.drawable.hk_ic_account_bonus;
            return 0;
        }

        @Override
        public String getAccountDesc(Context context, UserAccount account) {
            if (account.typeIs(Type.OPERATOR))
                return context.getString(R.string.account_desc_operator);
            else if (account.isHasLeft())
                return context.getString(R.string.account_left, formatAmount(context, account));
            else
                return account.getDescription();
        }


        @SuppressLint("DefaultLocale")
        @Override
        public String formatAmount(Context context, UserAccount account) {
            return String.format("%.2f", account.getLeft());
        }
    }

    private static Format sInstance;

    public static void initInstance(Format instance) {
        if (sInstance != null)
            throw new IllegalStateException("UserAccountFormatter instance already initialized");

        sInstance = instance;
    }

    private static synchronized Format getInstance() {
        if (sInstance == null) {
            sInstance = new FormatImpl();
        }
        return sInstance;
    }


    public static String getAccountName(Context context, UserAccount account) {
        return getInstance().getAccountName(context, account);
    }

    public static String getAccountDesc(Context context, UserAccount account) {
        return getInstance().getAccountDesc(context, account);
    }

    public static int getAccountLogo(Context context, UserAccount account) {
        return getInstance().getAccountLogo(context, account);
    }

    public static String formatAmount(Context context, UserAccount account) {
        return getInstance().formatAmount(context, account);
    }
}
