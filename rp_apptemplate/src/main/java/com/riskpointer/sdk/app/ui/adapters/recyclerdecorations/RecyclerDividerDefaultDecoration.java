package com.riskpointer.sdk.app.ui.adapters.recyclerdecorations;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

/**
 * Created by Shurygin Denis on 2016-07-25.
 */
public class RecyclerDividerDefaultDecoration extends RecyclerDividerDecoration {

    private static final int DIVIDER_COLOR = Color.parseColor("#DCDCDC");
    private static final int DIVIDER_HEIGHT = 2;

    public RecyclerDividerDefaultDecoration() {
        super(new ColorDrawable(DIVIDER_COLOR) {
            @Override
            public int getIntrinsicHeight() {
                return DIVIDER_HEIGHT;
            }
        });
    }
}
