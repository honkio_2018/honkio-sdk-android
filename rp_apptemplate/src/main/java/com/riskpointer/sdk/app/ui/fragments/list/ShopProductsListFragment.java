/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.fragments.list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Product;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.FragmentComponents;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.utils.FragmentUtils;

@Deprecated
public class ShopProductsListFragment extends BaseShopProductsListFragment {

    @Override
    protected int getDefaultListItemLayoutId(int type) {
        return R.layout.hk_list_item_product;
    }

    @Override
    public String getTitle(Context context) {
        return context.getString(R.string.shop_details_products_title);
    }

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = super.onCreateView(inflater, container, savedInstanceState);
        if (rootView != null) {
            ListView listView = (ListView) rootView.findViewById(android.R.id.list);
            listView.setDivider(null);

            View header = getLayoutInflater(savedInstanceState).inflate(R.layout.hk_list_item_shop_product_details, listView, false);
            listView.addHeaderView(header, null, false);
        }
		return rootView;
	}

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Shop shop = FragmentUtils.getShop(this, HonkioApi.getMainShopInfo().getShop());

        FragmentComponents components = AppController.getFragmentComponents(AppController.FRAGMENT_SHOP_DETAILS, shop);
        if (components != null && components.isFragment())
            getChildFragmentManager().beginTransaction().replace(R.id.hk_fragment_content, components.newFragment()).commitAllowingStateLoss();
    }

    @Override
    protected void onProductClick(Product product, int position) {
        Intent intent = AppController.getIntent(AppController.SCREEN_SHOP_PRODUCT,
                FragmentUtils.getShop(this, HonkioApi.getMainShopInfo().getShop()), getListView().getAdapter().getItem(position));
        startActivity(intent);
    }
}
