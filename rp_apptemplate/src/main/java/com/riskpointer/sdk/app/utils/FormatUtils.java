/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.utils;

import android.content.Context;
import android.text.format.DateUtils;

import com.riskpointer.sdk.app.R;

import java.text.DecimalFormat;
import java.util.Formatter;
import java.util.TimeZone;

public class FormatUtils {

	public static final DecimalFormat sDoubleFormat = new DecimalFormat("#0.00");
	public static final DecimalFormat sDistanceFormat = new DecimalFormat("#0.0");
	public static final DecimalFormat sTimeFormat = new DecimalFormat("#00");

	public static String format(double value) {
		return sDoubleFormat.format(value);
	}

	public static String format(double value, boolean cropZeros) {
		if (cropZeros && value == ((int) value))
			return Integer.toString((int) value);
		return sDoubleFormat.format(value);
	}

    public static String formatDate(Context context, long timeMillis) {
        return formatDate(context, timeMillis, true, "UTC");
    }

    public static String formatDate(Context context, long timeMillis, boolean showTime) {
		return formatDate(context, timeMillis, showTime, "UTC");
	}

	public static String formatDate(Context context, long timeMillis, boolean showTime, String timeZoneId) {
		Formatter formatter = DateUtils.formatDateRange(context,
				new Formatter(new StringBuilder()),
				timeMillis, timeMillis,
				DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_YEAR,
				timeZoneId);

		if (showTime) {
			StringBuilder stringBuilder = new StringBuilder(formatter.toString());
			stringBuilder.append(' ');
			formatter = DateUtils.formatDateRange(context,
					new Formatter(new StringBuilder()),
					timeMillis, timeMillis,
					DateUtils.FORMAT_SHOW_TIME,
					timeZoneId);
			stringBuilder.append(formatter.toString());
			return stringBuilder.toString();
		}

		return formatter.toString();
	}

	public static String formatTime(long timeMillis) {
		int seconds = (int) (timeMillis / 1000) % 60;
		int minutes = (int) ((timeMillis / (1000 * 60)) % 60);
		int hours = (int) ((timeMillis / 1000 - minutes * 60 - seconds) / 3600);
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(sTimeFormat.format(hours)).append(':');
		stringBuilder.append(sTimeFormat.format(minutes)).append(':');
		stringBuilder.append(sTimeFormat.format(seconds));
		return stringBuilder.toString();
	}

	public static String formatTime(Context context, long timeMillis) {
		int seconds = (int) (timeMillis / 1000) % 60;
		int minutes = (int) ((timeMillis / (1000 * 60)) % 60);
		int hours = (int) ((timeMillis / (1000 * 60 * 60)) % 24);
		int days = (int) ((timeMillis / 1000 - hours * 3600 - minutes * 60 - seconds) / 86400);
		StringBuilder stringBuilder = new StringBuilder();
		if (days > 0) {
			stringBuilder
					.append(Integer.toString(days))
					.append(' ')
					.append(context.getResources().getString(
							R.string.capt_days_abr));
		}
		if (hours > 0) {
			if (stringBuilder.length() > 0)
				stringBuilder.append(' ');

			stringBuilder
					.append(Integer.toString(hours))
					.append(' ')
					.append(context.getResources().getString(
							R.string.capt_hours_abr));
		}
		if (minutes > 0) {
			if (stringBuilder.length() > 0)
				stringBuilder.append(' ');

			stringBuilder
					.append(Integer.toString(minutes))
					.append(' ')
					.append(context.getResources().getString(
							R.string.capt_minuts_abr));
		}
		if (stringBuilder.length() == 0)
			stringBuilder.append('0');
		return stringBuilder.toString();
	}

	public static String formatDistance(Context context, double value) {
		if (value < 1000)
			return "" + ((int) value) + " m";
		return sDistanceFormat.format(value / 1000) + " km";
	}
}
