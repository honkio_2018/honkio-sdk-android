package com.riskpointer.sdk.app.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Poll;
import com.riskpointer.sdk.api.model.entity.PollsList;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.filter.PollsFilter;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.utils.FragmentUtils;

/**
 * Created by I.N. 20.01.2019
 */
public abstract class BasePollFragment extends BaseFragment {
    public final static String ARG_POLL = "BasePollFragment.ARG_POLL";
    public final static String ARG_POLL_ID = "BasePollFragment.ARG_POLL_ID";

    public final static String DLG_TAG_POLL_LOAD_ERROR = "BasePollFragment.DLG_TAG_POLL_LOAD_ERROR";

    private Poll mPoll;

    private Task mPollLoadTask;

    protected abstract void updateUi();

    public void setPoll(Poll poll) {
        mPoll = poll;
        if (getView() != null) {
            updateUi();
            //removePollPushes();
        }
    }

    public Poll getPoll() {
        return mPoll;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            setPoll((Poll) savedInstanceState.getSerializable(ARG_POLL));
        } else {
            Bundle args = getArguments();
            if (args != null) {
                if (args.containsKey(ARG_POLL))
                    setPoll((Poll) args.getSerializable(ARG_POLL));
            }
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(ARG_POLL, mPoll);
    }

    @Override
    public void onDestroy() {
        if (mPollLoadTask != null)
            mPollLoadTask.abort();
        super.onDestroy();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(mPoll != null) {
            updateUi();
            removePollPushes();
        }
        else {

            String pollId = getArguments().getString(ARG_POLL_ID);
            if (pollId != null) {
                loadPoll(getActivity().getApplicationContext(), pollId);
            }
            else
                finish();
        }
    }

    @Override
    public boolean onDismissDialog(BaseDialogFragment dialogFragment) {
        if (DLG_TAG_POLL_LOAD_ERROR.equals(dialogFragment.getTag())) {
            finish();
            return true;
        }
        return super.onDismissDialog(dialogFragment);
    }

    protected PollsFilter getFilter(String id) {
        final PollsFilter filter = new PollsFilter();
        filter.setId(id);
        return filter;
    }

    protected void loadPoll(Context context, String id) {
        mPollLoadTask = doPollGetRequest(getFilter(id), new RequestCallback<PollsList>() {
            @Override
            public boolean onComplete(Response<PollsList> response) {
                if (response.isStatusAccept()) {
                    if (response.getResult().getList().size() > 0) {
                        setPoll(response.getResult().getList().get(0));
                    } else {
                        showMessage(
                                DLG_TAG_POLL_LOAD_ERROR,
                                R.string.dlg_load_order_error_title,//todo
                                R.string.dlg_load_order_error_message);
                    }

                    removePollPushes();
                    return true;
                }
                return onError(response.getAnyError());
            }

            @Override
            public boolean onError(RpError error) {
                if (!BasePollFragment.this.onError(error)) {
                    showErrorMessage(DLG_TAG_POLL_LOAD_ERROR, error);
                }
                removePollPushes();
                return true;
            }
        });
    }

    protected Task doPollGetRequest(PollsFilter filter, RequestCallback<PollsList> callback) {
        return HonkioApi.userPollsList(FragmentUtils.getShop(this), filter, 0, callback);
    }

    protected void removePollPushes() {
       /* if (mPoll != null && mPoll.getId() != null) {
            HonkioApi.userSetPush(true, new PushFilter()
                    .addContent(PushFilter.QUERY_CONTENT_POLL, mPoll.getId()),
                    Message.FLAG_NO_WARNING, null);
        }*/
    }
}
