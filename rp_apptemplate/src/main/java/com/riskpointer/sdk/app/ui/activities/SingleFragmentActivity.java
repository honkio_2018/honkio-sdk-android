package com.riskpointer.sdk.app.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.riskpointer.sdk.app.FragmentComponents;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.R;

/**
 * @author Shurygin Denis on 2015-03-02.
 */
public class SingleFragmentActivity extends BaseActivity {
    public static final String EXTRA_FRAGMENT_COMPONENTS = "SingleFragmentActivity.EXTRA_FRAGMENT_COMPONENTS";
    public static final String EXTRA_ACTIVITY_TITLE = "SingleFragmentActivity.EXTRA_ACTIVITY_TITLE";

    public static final String FRAGMENT_TAG = "SingleFragmentActivity.FRAGMENT_TAG";

    private FragmentComponents mComponents;
    private SingleFragment mSingleFragmentInterface;

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_activity_simple_fragment;
    }

    @Override
    protected void onTrackScreen() {
        if (mSingleFragmentInterface != null && mSingleFragmentInterface.getScreenName() != null)
            AppController.trackScreen(mSingleFragmentInterface.getScreenName());
        else {
            if (mComponents != null && mComponents.getFragmentClass() != null)
                AppController.trackScreen(mComponents.getFragmentClass().getSimpleName());
            else
                super.onTrackScreen();
        }
    }

    @SuppressWarnings("rawtypes")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mComponents = getIntent().getParcelableExtra(EXTRA_FRAGMENT_COMPONENTS);

        Fragment fragment = getFragment();
        if(fragment == null && mComponents != null && mComponents.isFragment()) {
            fragment = mComponents.newFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.hk_fragment_content, fragment, FRAGMENT_TAG).commit();
        }

        if(fragment instanceof SingleFragment)
            mSingleFragmentInterface = (SingleFragment) fragment;

        refreshTitle();
    }

    @Override
    public boolean onNavigateUp() {
        onBackPressed();
        return false;
    }

    @Override
    public void onBackPressed() {
        if (mSingleFragmentInterface != null && mSingleFragmentInterface.onBackPressed())
            return;
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    public void onClick(View view) {
        Fragment fragment = getFragment();
        if (fragment != null && fragment instanceof View.OnClickListener)
            ((View.OnClickListener) fragment).onClick(view);
    }

    public void refreshTitle() {
        String titleString = null;

        if (getIntent().hasExtra(EXTRA_ACTIVITY_TITLE))
            titleString = getIntent().getStringExtra(EXTRA_ACTIVITY_TITLE);
        else if(mSingleFragmentInterface != null)
            titleString = mSingleFragmentInterface.getTitle(this);

        if (getSupportActionBar() != null) {
            if (titleString != null) {
                getSupportActionBar().setTitle(titleString);
                getSupportActionBar().show();
            }
            else
                getSupportActionBar().hide();
        }
    }

    protected Fragment getFragment() {
        return getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);
    }

    public static Intent newIntent(Context context, String title, Class<?> fragmentClass, Bundle arguments) {
        return newIntent(context, title, new FragmentComponents(fragmentClass, arguments));
    }

    public static Intent newIntent(Context context, FragmentComponents fragmentComponents) {
        return newIntent(context, null, fragmentComponents);
    }

    public static Intent newIntent(Context context, String activityTitle, FragmentComponents fragmentComponents) {
        if (fragmentComponents == null)
            throw new NullPointerException("FragmentComponents is null");

        if (fragmentComponents.getFragmentClass() == null)
            throw new NullPointerException("FragmentComponents class is null");

        Intent intent = new Intent(context, SingleFragmentActivity.class);

        if (activityTitle != null)
            intent.putExtra(EXTRA_ACTIVITY_TITLE, activityTitle);
        intent.putExtra(EXTRA_FRAGMENT_COMPONENTS, fragmentComponents);
        return intent;
    }
    
    public interface SingleFragment {
        String getScreenName();
        String getTitle(Context context);
        boolean onBackPressed();
    }
}
