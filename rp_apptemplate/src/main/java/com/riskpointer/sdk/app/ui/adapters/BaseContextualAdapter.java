package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

/**
 * Created by Shurygin Denis on 2016-10-21.
 */
public abstract class BaseContextualAdapter<Holder> extends BaseAdapter {

    protected final Context mContext;
    private LayoutInflater mInflater;
    private int[] mContextualButtons;

    public BaseContextualAdapter(Context context) {
        super();
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    protected abstract Holder newHolder(View view);
    protected abstract void bindHolder(Holder holder, int position);
    protected abstract int getViewLayoutId(int position);

    public void setContextualButtons(int[] contextualButtons) {
        mContextualButtons = contextualButtons;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null)
            view = newView(getViewLayoutId(position), parent);

        bindView(view, position);
        return view;
    }

    protected View newView(int layoutId, ViewGroup parent) {
        View view = mInflater.inflate(layoutId, parent, false);
        ContextHolder<Holder> contextHolder = new ContextHolder<>();
        contextHolder.viewHolder = newHolder(view);
        if (parent instanceof AdapterView<?>)
            contextHolder.list = (AdapterView<?>) parent;
        view.setTag(contextHolder);
        if (mContextualButtons != null)
            for (int id : mContextualButtons) {
                View contextualButton = view.findViewById(id);
                if (contextualButton != null) {
                    contextualButton.setOnClickListener(mContextButtonClickListener);
                    contextualButton.setTag(contextHolder);
                    contextualButton.setSoundEffectsEnabled(false);
                    contextualButton.setFocusable(false);
                }
            }
        return view;
    }

    protected void bindView(View view, int position) {
        @SuppressWarnings("unchecked")
        ContextHolder<Holder> contextHolder = (ContextHolder<Holder>) view
                .getTag();
        contextHolder.position = position;
        contextHolder.id = getItemId(position);

        bindHolder(contextHolder.viewHolder, position);
    }

    protected void handleContextButtonClick(AdapterView<?> list, View view, int position, long id) {
        if (list instanceof ListView)
            position += ((ListView) list).getHeaderViewsCount();

        list.performItemClick(view, position, id);
    }

    private View.OnClickListener mContextButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            @SuppressWarnings("unchecked")
            ContextHolder<Holder> contextHolder = (ContextHolder<Holder>) view
                    .getTag();
            if (contextHolder != null && contextHolder.list != null)
                handleContextButtonClick(contextHolder.list, view,
                        contextHolder.position, contextHolder.id);
        }
    };


    private static class ContextHolder<T> {
        int position;
        long id;
        AdapterView<?> list;
        T viewHolder;
    }
}
