package com.riskpointer.sdk.app.ui.fragments.list;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ListView;

import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.Transaction;
import com.riskpointer.sdk.api.model.entity.Transactions;
import com.riskpointer.sdk.api.model.push.Push;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.support.BroadcastManager;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.TransactionFilter;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.adapters.BaseArrayListAdapter;
import com.riskpointer.sdk.app.ui.adapters.TransactionsListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shurygin Denis on 2015-04-10.
 */
public abstract class TransactionsListFragment extends BaseListRefLazyFragment<Transaction, Transactions> {
    public static final String ARG_FILTER = "Filter";

    private TransactionFilter mFilter;

    @Override
    public abstract TransactionsListLoader onCreateLoader(int id, Bundle args);

    public void setFilter(TransactionFilter filter) {
        mFilter = filter;
    }

    public TransactionFilter getFilter() {
        if (mFilter == null)
            mFilter = new TransactionFilter();
        return mFilter;
    }

    @Override
    protected int getDefaultListItemLayoutId(int type) {
        return R.layout.hk_list_item_history_1;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null)
            mFilter = (TransactionFilter) savedInstanceState.getSerializable(ARG_FILTER);
        else {
            Bundle args = getArguments();
            if (args != null)
                mFilter = (TransactionFilter) args.getSerializable(ARG_FILTER);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(ARG_FILTER, mFilter);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setEmptyText(getString(R.string.main_history_not_found));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        BroadcastManager.getInstance(getActivity()).registerReceiver(mPaymentListener,
                new IntentFilter(BroadcastHelper.getActionOnComplete(Message.Command.USER_PAYMENT)));

        BroadcastManager.getInstance(getActivity()).registerReceiver(mPushListener,
                new IntentFilter(BroadcastHelper.BROADCAST_ACTION_ON_PUSH_RECEIVED));
    }

    @Override
    public void onDetach() {
        BroadcastManager.getInstance(getActivity()).unregisterReceiver(mPaymentListener);
        BroadcastManager.getInstance(getActivity()).unregisterReceiver(mPushListener);
        super.onDetach();
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        if(getListAdapter() != null && position >= listView.getHeaderViewsCount()) {
            Transaction transaction = (Transaction)
                    getListAdapter().getItem(position - listView.getHeaderViewsCount());
            if(transaction != null) {
                Intent intent = AppController.getIntent(AppController.SCREEN_SHOP_TRANSACTION, transaction);
                startActivity(intent);
            }
        }
    }

    @Override
    protected BaseArrayListAdapter<Transaction, ?> newAdapter(List<Transaction> list) {
        return new TransactionsListAdapter(getActivity(), list, getListItemLayoutId(0));
    }

    private BroadcastReceiver mPaymentListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            reloadList(context);
        }
    };

    private BroadcastReceiver mPushListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.hasExtra(BroadcastHelper.BROADCAST_EXTRA_PUSH)) {
                Push<?> push = (Push<?>) intent.getSerializableExtra(BroadcastHelper.BROADCAST_EXTRA_PUSH);
                if (push.isType(Push.Type.TRANSACTION))
                    reloadList(context);
            }
        }
    };

    public static abstract class TransactionsListLoader extends ListLoader<Transaction, Transactions> {

        private Shop mShop;
        private TransactionFilter mFilter;

        public TransactionsListLoader(Context context, Shop shop, TransactionFilter filter) {
            super(context);
            mShop = shop;
            mFilter = filter;
        }

        protected abstract Task doRequest(Context context, RequestCallback<Transactions> callback, Shop shop, TransactionFilter filter);

        @Override
        protected Task loadList(Context context, RequestCallback<Transactions> callback, int count, int skip) {
            if (mFilter == null) {
                mFilter = new TransactionFilter();
            }
            mFilter.setCount(count);
            mFilter.setSkip(skip);

            if(mShop == null)
                return doRequest(context, callback, null, mFilter);
            else {
                if(mFilter != null) {
                    if (mShop.getMerchant() != null)
                        mFilter.getShopFilter().setMerchantId(mShop.getMerchant().getId());
                    mFilter.getShopFilter().setServiceType(mShop.getServiceType());
                }
                return doRequest(context, callback, mShop, mFilter);
            }
        }

        @Override
        protected ArrayList<Transaction> toList(Response<Transactions> response) {
            return response.getResult().getTransactionsList();
        }
    }
}
