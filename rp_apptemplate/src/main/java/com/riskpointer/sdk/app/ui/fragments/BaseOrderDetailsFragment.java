package com.riskpointer.sdk.app.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.merchant.HonkioMerchantApi;
import com.riskpointer.sdk.api.merchant.MerchantCommand;
import com.riskpointer.sdk.api.model.entity.Order;
import com.riskpointer.sdk.api.model.entity.OrdersList;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.support.BroadcastManager;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.utils.CompleteActionReceiver;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.OrderFilter;
import com.riskpointer.sdk.api.web.message.filter.PushFilter;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.ui.strategy.ProgressContainerManager;

public abstract class BaseOrderDetailsFragment extends BaseFragment {

    public final static String ARG_ORDER = "BaseOrderFragment.ARG_ORDER";
    public final static String ARG_ORDER_ID = "BaseOrderFragment.ARG_ORDER_ID";
    public final static String ARG_USE_MERCHANT_API = "BaseOrderFragment.ARG_USE_MERCHANT_API";

    public final static String DLG_TAG_ORDER_LOAD_ERROR = "BaseOrderFragment.DLG_TAG_ORDER_LOAD_ERROR";

    private Order mOrder;

    private Task mOrderLoadTask;

    private ProgressContainerManager mProgressManager;

    public interface ContentFragment {
        void setOrder(Order order);
    }

    protected abstract Fragment buildContentFragment(Order order);

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_lazy_container;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mOrder == null) {
            if (savedInstanceState != null) {
                mOrder = (Order) savedInstanceState.getSerializable(ARG_ORDER);
            } else {
                Bundle args = getArguments();
                if (args != null) {
                    if (args.containsKey(ARG_ORDER))
                        mOrder = (Order) args.getSerializable(ARG_ORDER);
                }
            }
        }

        mProgressManager = getLifeCycleObserver(ProgressContainerManager.TAG);
        if (mProgressManager == null) {
            mProgressManager = new ProgressContainerManager(ProgressContainerManager.TAG,
                    R.id.hk_progress, R.id.hk_content);
            addLifeCycleObserver(mProgressManager);
        }
        if (mOrder == null)
            mProgressManager.hideContent(false);
        else
            mProgressManager.showContent(false);

        BroadcastManager.getInstance(getActivity()).registerReceiver(mOrderUpdateListener,
                new IntentFilter(BroadcastHelper.getActionOnComplete(Message.Command.USER_SET_ORDER)));
        BroadcastManager.getInstance(getActivity()).registerReceiver(mOrderUpdateListener,
                new IntentFilter(BroadcastHelper.getActionOnComplete(MerchantCommand.MERCHANT_SET_ORDER)));
    }

    @Override
    public void onDestroy() {
        if (mOrderLoadTask != null)
            mOrderLoadTask.abort();

        BroadcastManager.getInstance(getActivity()).unregisterReceiver(mOrderUpdateListener);

        super.onDestroy();
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(ARG_ORDER, mOrder);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mOrder != null) {
            onOrderSet(mOrder);
        } else {
            Bundle args = getArguments();
            if (args != null) {
                String orderId = args.getString(ARG_ORDER_ID);
                if (orderId != null) {
                    loadOrder(getContext(), orderId);
                } else {
                    showMessage(DLG_TAG_ORDER_LOAD_ERROR,
                            R.string.dlg_load_order_error_title,
                            R.string.dlg_load_order_error_message);
                }
            } else {
                showMessage(DLG_TAG_ORDER_LOAD_ERROR,
                        R.string.dlg_load_order_error_title,
                        R.string.dlg_load_order_error_message);
            }
        }
    }

    @Override
    public boolean onDismissDialog(BaseDialogFragment dialogFragment) {
        if (DLG_TAG_ORDER_LOAD_ERROR.equals(dialogFragment.getTag())) {
            finish();
            return true;
        }
        return super.onDismissDialog(dialogFragment);
    }

    public void setOrder(Order order) {
        mOrder = order;
        if (getView() != null) {
            onOrderSet(order);
            removeOrderPushes();
        }
    }

    public Order getOrder() {
        return mOrder;
    }

    protected OrderFilter getFilter(String id) {
        final OrderFilter filter = new OrderFilter();
        filter.setId(id);
        filter.setProductDetails(true);
        filter.setQueryUnreadMessages(true);
        filter.setChildMerchants(true);
        return filter;
    }

    protected void onOrderSet(Order order) {
        if (isAdded()) {
            Fragment contentFragment = getFragmentManager().findFragmentById(R.id.hk_content);
            if (contentFragment == null) {
                Fragment fragment = buildContentFragment(order);
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.hk_content, fragment)
                        .commit();
            } else {
                if (contentFragment instanceof ContentFragment) {
                    ((ContentFragment) contentFragment).setOrder(order);
                }
            }

            mProgressManager.showContent(true);
        } else
            mProgressManager.showContent(false);
    }

    protected void loadOrder(Context context, String id) {
        mOrderLoadTask = doOrderGetRequest(getFilter(id), new RequestCallback<OrdersList>() {
            @Override
            public boolean onComplete(Response<OrdersList> response) {
                if (response.isStatusAccept()) {
                    if (response.getResult().getList().size() > 0) {
                        setOrder(response.getResult().getList().get(0));
                    } else {
                        showMessage(DLG_TAG_ORDER_LOAD_ERROR,
                                R.string.dlg_load_order_error_title,
                                R.string.dlg_load_order_error_message);
                    }

                    removeOrderPushes();
                    return true;
                }
                return onError(response.getAnyError());
            }

            @Override
            public boolean onError(RpError error) {
                if (!BaseOrderDetailsFragment.this.onError(error)) {
                    showErrorMessage(DLG_TAG_ORDER_LOAD_ERROR, error);
                }
                removeOrderPushes();
                return true;
            }
        });
    }

    protected boolean isUseMerchantApi() {
        if (getArguments() != null && getArguments().containsKey(ARG_USE_MERCHANT_API)) {
            Object args = getArguments().get(ARG_USE_MERCHANT_API);
            if (args instanceof Boolean) {
                return (Boolean) args;
            }
        }
        return false;
    }

    protected Task doOrderGetRequest(OrderFilter filter, RequestCallback<OrdersList> callback) {
        if (isUseMerchantApi()) {
            return HonkioMerchantApi.merchantGetOrders(filter, 0, callback);
        } else {
            return HonkioApi.userGetOrders(filter, 0, callback);
        }
    }

    protected void removeOrderPushes() {
        if (mOrder != null && mOrder.getOrderId() != null) {
            HonkioApi.userSetPush(true, new PushFilter()
                            .addContent(PushFilter.QUERY_CONTENT_ORDER, mOrder.getOrderId()),
                    Message.FLAG_NO_WARNING, null);
        }
    }

    private BroadcastReceiver mOrderUpdateListener = new CompleteActionReceiver<Order>() {
        @Override
        public void onReceive(Context context, Intent intent, Response<Order> response) {
            Order order = response.getResult();
            if (mOrder != null &&
                    mOrder.getOrderId() != null &&
                    mOrder.getOrderId().equals(order.getOrderId())) {
                setOrder(order);
            }
        }
    };
}
