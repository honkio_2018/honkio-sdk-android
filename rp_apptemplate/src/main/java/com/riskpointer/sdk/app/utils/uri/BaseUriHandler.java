package com.riskpointer.sdk.app.utils.uri;

import android.net.Uri;

import java.util.List;

/**
 * @author Shurygin Denis
 */

public class BaseUriHandler implements UriHandler {

    public static final String PATH_LOGIN = "login";

    public static final String PATH_QR = "qr";
    public static final String PATH_ORDER = "order";
    public static final String PATH_INVITE = "invite";
    public static final String PATH_ASSET = "asset";
    public static final String PATH_ROLE = "role";

    @Override
    public boolean handle(String uriString) {
        if (uriString == null)
            return false;
        return handle(Uri.parse(uriString));
    }

    public boolean handle(Uri uri) {
        if (uri == null)
            return false;

        if (!checkAuthority(uri.getAuthority()))
            return false;

        List<String> pathSegments = uri.getPathSegments();

        // Screen
        if (pathSegments.size() >= 1) {
            switch (pathSegments.get(pathSegments.size() - 1)) {
                case PATH_LOGIN:
                    return handleLogin(uri);
            }
        }

        // Item
        if (pathSegments.size() >= 2) {
            String id = pathSegments.get(pathSegments.size() - 1);
            switch (pathSegments.get(pathSegments.size() - 2)) {
                case PATH_QR:
                    return handleQrId(id, uri);
                case PATH_ORDER:
                    return handleOrder(id, uri);
                case PATH_INVITE:
                    return handleInvite(id, uri);
                case PATH_ASSET:
                    return handleAsset(id, uri);
                case PATH_ROLE:
                    return handleRole(id, uri);
            }
        }

        // Deprecated share url
        if (pathSegments.size() == 3
                && "share".equals(pathSegments.get(0))
                && PATH_ORDER.equals(pathSegments.get(1))) {
            return handleOrder(pathSegments.get(2), uri);
        }

        return false;
    }

    protected boolean checkAuthority(String authority) {
        return true;
    }

    protected boolean handleLogin(Uri uri) {
        // Do nothing
        return false;
    }

    protected boolean handleQrId(String id, Uri uri) {
        return false;
    }

    protected boolean handleOrder(String id, Uri uri) {
        return false;
    }

    protected boolean handleInvite(String id, Uri uri) {
        return false;
    }

    protected boolean handleAsset(String id, Uri uri) {
        return false;
    }

    protected boolean handleRole(String id, Uri uri) {
        return false;
    }
}
