package com.riskpointer.sdk.app.ui.strategy;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.BookedProduct;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.api.model.entity.UserPayment;
import com.riskpointer.sdk.api.model.process.BaseProcessModel;
import com.riskpointer.sdk.api.model.process.user.AmountPaymentProcessModel;
import com.riskpointer.sdk.api.model.process.user.BasePaymentProcessModel;
import com.riskpointer.sdk.api.model.process.user.ProductPaymentProcessModel;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.callback.RequestCallbackWrapper;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.ui.dialogs.MessageDialog;
import com.riskpointer.sdk.app.ui.fragments.PinFragment;

import java.util.ArrayList;


/**
 * Created by Shurygin Denis on 2016-11-24.
 */

public class PaymentViewStrategy extends BaseViewLifeCycleObserver
        implements RequestCallback<UserPayment>, BasePaymentProcessModel.RequestPinHandler, UserAccountSelectionViewStrategy.Delegate {

    public interface Delegate {
        void onStart(PaymentViewStrategy paymentStrategy);
        boolean onComplete(PaymentViewStrategy paymentStrategy, Response<UserPayment> response);
        boolean onError(PaymentViewStrategy paymentStrategy, RpError error);
    }

    public enum State {
        STARTED,

        LOGIN_ASKED,
        LOGIN_COMPLETED,

        PAYMENT_ACCOUNT_ASKED,
        PAYMENT_ACCOUNT_SELECTED,

        WEB_REQUEST_WILL_START,

        PIN_ASKED,
        PIN_ENTERED,

        COMPLETED,
        ABORTED,
        ERROR,
    }

    public interface PaymentStateCallback {

        void onPaymentStateChanged(PaymentViewStrategy paymentStrategy, State state);

    }

    protected static final int REQUEST_PIN = 3;
    protected static final int REQUEST_LOGIN = 4;

    private static final String SAVED_PAY_PARAMS = "PaymentViewStrategy.SAVED_PAY_PARAMS";
    private static final String SAVED_PAY_PROCESS_MODEL = "PaymentViewStrategy.SAVED_PAY_PROCESS_MODEL";
    private static final String SAVED_REMOVE_ON_COMPLETION = "PaymentViewStrategy.SAVED_REMOVE_ON_COMPLETION";
    private static final String SAVED_ASK_PIN = "PaymentViewStrategy.SAVED_ASK_PIN";

    private static final String DLG_TAG_NEED_LOGIN = "PaymentViewStrategy.DLG_TAG_NEED_LOGIN";

    private static final String LCO_TAG_ACCOUNT_SELECTION = "PaymentViewStrategy.LCO_TAG_ACCOUNT_SELECTION";

    private BasePaymentProcessModel mPaymentProcessModel;
    private BasePaymentProcessModel.BasePayParams mPayParams;

    private boolean isRemoveOnCompletion = false;
    private boolean isPinRequired = true;

    public PaymentViewStrategy() {
        this(null);
    }

    public PaymentViewStrategy(String tag) {
        super(tag);
    }

    @Override
    public void onAttachToController(ViewController viewController) {
        if (viewController != null && !(viewController instanceof Delegate))
            throw new IllegalArgumentException(
                    "Parent fragment must implement Delegate");

        super.onAttachToController(viewController);
    }

    @Override
    public void saveState(ViewController viewController, Bundle outState) {
        super.saveState(viewController, outState);

        outState.putBoolean(SAVED_ASK_PIN, isPinRequired);
        outState.putBoolean(SAVED_REMOVE_ON_COMPLETION, isRemoveOnCompletion);

        if (mPayParams != null)
            outState.putSerializable(SAVED_PAY_PARAMS, mPayParams);
        if (mPaymentProcessModel != null)
            outState.putBundle(SAVED_PAY_PROCESS_MODEL, mPaymentProcessModel.saveInstanceState());
    }

    @Override
    public void restoreState(ViewController viewController, Bundle savedInstanceState) {
        super.restoreState(viewController, savedInstanceState);

        isPinRequired = savedInstanceState.getBoolean(SAVED_ASK_PIN, true);
        isRemoveOnCompletion = savedInstanceState.getBoolean(SAVED_REMOVE_ON_COMPLETION, false);

        if (savedInstanceState.containsKey(SAVED_PAY_PARAMS))
            mPayParams = (BasePaymentProcessModel.BasePayParams) savedInstanceState.getSerializable(SAVED_PAY_PARAMS);
        if (savedInstanceState.containsKey(SAVED_PAY_PROCESS_MODEL)) {
            mPaymentProcessModel = (BasePaymentProcessModel) BaseProcessModel
                    .restoreFromSavedState(viewController.getContext(),
                            savedInstanceState.getBundle(SAVED_PAY_PROCESS_MODEL));
            if (mPaymentProcessModel != null) {
                mPaymentProcessModel.setCallback(new PaymentCallbackWrapper(this));
                mPaymentProcessModel.setRequestPinHandler(isPinRequired ? this : null);
            }
        }
    }

    @Override
    public void onActivityResult(ViewController viewController, int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PIN) {
            if (resultCode == Activity.RESULT_OK) {
                if (mPaymentProcessModel != null)
                    notifyStateChanged(State.PIN_ENTERED);

                    mPaymentProcessModel.onPinEntered(
                            viewController.getContext(),
                            data.getStringExtra(PinFragment.EXTRA_PIN),
                            data.getLongArrayExtra(PinFragment.EXTRA_DELAYS),
                            data.getIntExtra(PinFragment.EXTRA_REASON, 0));
            }
            else {
                if (mPaymentProcessModel != null)
                    mPaymentProcessModel.onPinCanceled(viewController.getContext(),
                            data.getIntExtra(PinFragment.EXTRA_REASON, 0));
            }
        }
        else if (requestCode == REQUEST_LOGIN) {
            if (HonkioApi.isConnected()) {
                notifyStateChanged(State.LOGIN_COMPLETED);
                runPaymentIfOk();
            }
            else {
                onError(new RpError(RpError.Api.CANCELED_BY_USER));
            }
        }
        else
            onError(viewController, new RpError(RpError.Api.CANCELED_BY_USER));
    }

    @Override
    public boolean onClickDialog(ViewController viewController, BaseDialogFragment dialogFragment, int which) {
        if (DLG_TAG_NEED_LOGIN.equals(dialogFragment.getTag())) {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                getController().startActivityForResult(
                        AppController.getIntent(AppController.ACTIVITY_LOGIN), REQUEST_LOGIN);
            }
            else{
                onError(new RpError(RpError.Api.CANCELED_BY_USER));
            }
            return true;
        }
        return super.onClickDialog(viewController, dialogFragment, which);
    }

    @Override
    public boolean onError(ViewController viewController, RpError error) {
        return onError(error);
    }

    @Override
    public boolean onError(RpError error) {
        notifyStateChanged(State.ERROR);
        Delegate callback = getCompletionCallback();

        if (isRemoveOnCompletion) {
            ViewController viewController = getController();
            if (viewController != null)
                viewController.removeLifeCycleObserver(this);
        }

        if (callback != null)
            return callback.onError(this, error);
        return false;
    }

    @Override
    public boolean onComplete(Response<UserPayment> response) {
        notifyStateChanged(State.COMPLETED);
        Delegate callback = getCompletionCallback();

        if (isRemoveOnCompletion && !Response.Status.PENDING.equals(response.getStatus())) {
            ViewController viewController = getController();
            if (viewController != null)
                viewController.removeLifeCycleObserver(this);
        }

        if (response.isStatusPending() || response.isStatusAccept()) {
            if (callback != null)
                return callback.onComplete(this, response);
            return false;
        }
        return onError(response.getAnyError());
    }

    @Override
    public void onRequestPin(Context context, int reason) {
        notifyStateChanged(State.PIN_ASKED);

        getController().startActivityForResult(
                AppController.getIntent(AppController.ACTIVITY_PIN, reason), REQUEST_PIN);
    }

    @Override
    public void onAccountSelect(UserAccountSelectionViewStrategy strategy, UserAccount account) {
        notifyStateChanged(State.PAYMENT_ACCOUNT_SELECTED);

        mPayParams.setAccount(account);
        runPaymentIfOk();
    }

    @Override
    public void onAccountSelectionCancel(UserAccountSelectionViewStrategy strategy) {
        onError(new RpError(RpError.Api.CANCELED_BY_USER));
    }

    public PaymentViewStrategy setRemoveOnCompletion(boolean remove) {
        isRemoveOnCompletion = remove;
        return this;
    }

    public PaymentViewStrategy setPinRequired(boolean pinRequired) {
        isPinRequired = pinRequired;
        if (mPaymentProcessModel != null)
            mPaymentProcessModel.setRequestPinHandler(isPinRequired ? this : null);
        return this;
    }

    /**
     * Makes query for pending payment transaction.
     */
    public void query() {
        if(mPaymentProcessModel != null)
            mPaymentProcessModel.query();
    }

    /**
     * Starts loop query for pending payment transaction with specified delay.
     * @param loopDelay Delay for the next query.
     */
    public void query(long loopDelay) {
        if(mPaymentProcessModel != null)
            mPaymentProcessModel.query(loopDelay);
    }

    /**
     * Stops loop query.
     */
    public void stopQuery() {
        if(mPaymentProcessModel != null)
            mPaymentProcessModel.stopQuery();
    }

    public void abort() {
        notifyStateChanged(State.ABORTED);

        if (mPaymentProcessModel != null)
            mPaymentProcessModel.abort();
    }

    public void pay(Shop shop, String orderId, UserAccount account,
                    ArrayList<BookedProduct> bookProducts, int flags) {
        pay(new ProductPaymentProcessModel.ProductPayParams(shop, orderId, account, bookProducts,
                flags));
    }

    public void pay(Shop shop, String orderId, UserAccount account,
                    double amount, String currency, int flags) {
        pay(new AmountPaymentProcessModel.AmountPayParams(shop, orderId, account, amount, currency,
                flags));
    }

    protected void pay(BasePaymentProcessModel.BasePayParams payParams) {
        mPayParams = payParams;
        mPaymentProcessModel = null;
        notifyStateChanged(State.STARTED);
        runPaymentIfOk();
    }

    private void onPaymentWillStart() {
        notifyStateChanged(State.WEB_REQUEST_WILL_START);

        Delegate callback = getCompletionCallback();
        if (callback != null)
            callback.onStart(this);
    }

    private void notifyStateChanged(State state) {
        if (getController() instanceof PaymentStateCallback)
            ((PaymentStateCallback) getController()).onPaymentStateChanged(this, state);
    }

    private void runPaymentIfOk() {
        if (!HonkioApi.isConnected()) {
            onLoginRequired();
        }
        else if(!mPayParams.isAccountValid()) {
            onAccountRequired();
        }
        else {
            onPaymentWillStart();
            mPaymentProcessModel = mPayParams.instantiateProcessModel();
            mPaymentProcessModel.setCallback(new PaymentCallbackWrapper(this));
            mPaymentProcessModel.setRequestPinHandler(isPinRequired ? this : null);
            //noinspection unchecked
            mPaymentProcessModel.pay(getContext(), mPayParams);
            mPayParams = null;
        }
    }

    private void onLoginRequired() {
        notifyStateChanged(State.LOGIN_ASKED);

        showMessage(DLG_TAG_NEED_LOGIN,
                R.string.dlg_login_needed_title,
                R.string.dlg_login_needed_message,
                MessageDialog.BTN_OK_CANCEL);
    }

    private void onAccountRequired() {
        notifyStateChanged(State.PAYMENT_ACCOUNT_ASKED);

        UserAccountSelectionViewStrategy strategy =
                (UserAccountSelectionViewStrategy) getController()
                        .getLifeCycleObserver(LCO_TAG_ACCOUNT_SELECTION);

        if (strategy == null) {
            strategy = new UserAccountSelectionViewStrategy(LCO_TAG_ACCOUNT_SELECTION);
            strategy.setParentLifeCycleObserverTag(getTag());
            strategy.setDisallowedAccounts(mPayParams.getDisallowedAccounts());
            getController().addLifeCycleObserver(strategy);
        }

        strategy.selectAccount(
                mPayParams.getShop(),
                mPayParams.getAmount(),
                mPayParams.getCurrency());
    }



    private Delegate getCompletionCallback() {
        ViewLifeCycleObserver parent = getParentLifeCycleObserver();
        if (parent instanceof Delegate)
            return (Delegate) parent;

        ViewController viewController = getController();
        if (viewController instanceof Delegate)
            return (Delegate) viewController;
        return null;
    }

    private class PaymentCallbackWrapper extends RequestCallbackWrapper<UserPayment> {

        PaymentCallbackWrapper(RequestCallback<UserPayment> callback) {
            super(callback);
        }

        @Override
        public boolean onError(RpError error) {
            mPaymentProcessModel = null;
            mPayParams = null;
            return super.onError(error);
        }

        @Override
        public boolean onComplete(final Response<UserPayment> response) {
            if (response.isStatusPending()) {
                return super.onComplete(response);
            }
            else if(response.isStatusReject()) {
                return onError(response.getAnyError());
            }
            else {
                mPaymentProcessModel = null;
                mPayParams = null;
            }
            return super.onComplete(response);
        }
    }
}
