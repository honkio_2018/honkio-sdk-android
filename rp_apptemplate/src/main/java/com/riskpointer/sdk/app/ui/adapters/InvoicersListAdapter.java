package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.model.entity.Invoicer;
import com.riskpointer.sdk.app.R;

import java.util.ArrayList;
import java.util.Set;

/**
 * @author Shurygin Denis on 2015-04-02.
 */
public class InvoicersListAdapter extends BaseArrayListAdapter<Invoicer, InvoicersListAdapter.ViewHolder> {

    public InvoicersListAdapter(Context context, ArrayList<Invoicer> list, int layout) {
        super(context, list, layout);
    }

    @Override
    public ViewHolder newHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void bindHolder(ViewHolder holder, int position) {
        Invoicer invoicer = getItem(position);
        holder.text.setText(invoicer.getName());
    }

    public static ArrayList<Invoicer> extractInvoicersList(User user) {
        Set<String> keys = user.getInvoicerKeys();
        ArrayList<Invoicer> list = new ArrayList<>(keys.size());
        for (String key: keys) {
            Invoicer invoicer = user.getInvoicer(key);
            if (invoicer != null)
                list.add(invoicer);
        }
        return list;
    }

    class ViewHolder {
        final TextView text;

        public ViewHolder(View view) {
            text = (TextView) view.findViewById(R.id.hk_txtName);
        }
    }
}
