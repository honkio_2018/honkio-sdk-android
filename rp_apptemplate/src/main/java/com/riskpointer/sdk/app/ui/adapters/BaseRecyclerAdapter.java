package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shurygin Denis on 2016-07-25.
 */
public abstract class BaseRecyclerAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected Context mContext;
    protected LayoutInflater mInflater;
    private int[] mLayouts;
    private int mTypesCount;

    private List<AdditionalView> mHeaderViews = new ArrayList<>();
    private List<AdditionalView> mFooterViews = new ArrayList<>();

    public abstract VH onCreateItemViewHolder(ViewGroup parent, View view, int viewType);
    public abstract void onBindItemViewHolder(VH holder, int position);

    public BaseRecyclerAdapter(Context context, int layout) {
        this(context, new int[]{layout});
    }

    public BaseRecyclerAdapter(Context context, int[] layouts) {
        mContext = context;
        mLayouts = layouts;
        mTypesCount = mLayouts.length;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType < mLayouts.length)
            return onCreateItemViewHolder(parent, viewType);
        return new HeaderFooterViewHolder(findView(viewType));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position >= getHeaderViewCount() && position < getItemCount() - getFooterViewCount()) {
            onBindItemViewHolder((VH) holder, position);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position < getHeaderViewCount())
            return mHeaderViews.get(position).type;
        if (position >= getItemCount() - getFooterViewCount())
            return mFooterViews.get(mFooterViews.size() + position - getItemCount()).type;
        return getListItemViewType(position);
    }

    /**
     * Return the view type of the list item (except headers and footers) at <code>position</code>.
     *
     * <p>The default implementation of this method returns getItemViewType(position).
     *
     * @param position position to query
     * @return integer value identifying the type of the view needed to represent the item at
     *                 <code>position</code>. Type codes need not be contiguous.
     */
    protected int getListItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return mHeaderViews.size() + mFooterViews.size();
    }

    protected VH onCreateItemViewHolder(ViewGroup parent, int viewType) {
        return onCreateItemViewHolder(parent,
                mInflater.inflate(mLayouts[viewType], parent, false), viewType);
    }

    public void addHeaderView(View view) {
        mHeaderViews.add(new AdditionalView(view, mTypesCount++));
        notifyItemInserted(mHeaderViews.size() - 1);
    }

    public void addFooterView(View view) {
        mFooterViews.add(new AdditionalView(view, mTypesCount++));
        notifyItemInserted(getItemCount() - 1);
    }

    public int getHeaderViewCount() {
        return mHeaderViews.size();
    }

    public int getFooterViewCount() {
        return mFooterViews.size();
    }

    private View findView(int viewType) {
        for (AdditionalView additionalView: mHeaderViews) {
            if (additionalView.type == viewType)
                return additionalView.view;
        }
        for (AdditionalView additionalView: mFooterViews) {
            if (additionalView.type == viewType)
                return additionalView.view;
        }
        throw new RuntimeException("View with type " + viewType + " not found.");
    }

    private class AdditionalView {
        public final View view;
        public final int type;

        AdditionalView(View view, int type) {
            this.view = view;
            this.type = type;
        }
    }

    private class HeaderFooterViewHolder extends RecyclerView.ViewHolder {

        public HeaderFooterViewHolder(View itemView) {
            super(itemView);
        }
    }
}
