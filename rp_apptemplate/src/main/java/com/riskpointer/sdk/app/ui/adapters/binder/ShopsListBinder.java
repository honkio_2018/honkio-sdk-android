package com.riskpointer.sdk.app.ui.adapters.binder;

import android.content.Context;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.utils.FormatUtils;
import com.riskpointer.sdk.app.utils.ImageUtils;
import com.squareup.picasso.Callback;

/**
 * Created by Shurygin Denis on 2016-05-26.
 */
public class ShopsListBinder implements BaseShopsListBinder<Shop, ShopsListBinder.ViewHolder> {

    private final float[] mDistanceResults = new float[1];

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public final TextView text;
        public final TextView text2;
        public final ImageView imageLogo;
        public final ImageView imageType;
        public final View imageProgress;

        public ViewHolder(View view) {
            super(view);
            text = (TextView) view.findViewById(android.R.id.text1);
            text2 = (TextView) view.findViewById(android.R.id.text2);
            imageLogo = (ImageView) view.findViewById(R.id.hk_imgShopLogo);
            imageType = (ImageView) view.findViewById(R.id.hk_imgShopType);
            imageProgress = view.findViewById(R.id.hk_progressContainer);
        }
    }

    private Location mMyLocation;
    private boolean mServiceTypeVisible = true;

    @Override
    public ViewHolder newHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void bind(Context context, ViewHolder holder, Shop item) {

        holder.text.setText(item.getName());

        if ( holder.imageProgress != null && holder.imageLogo != null) {
            int serviceTypeImageId = ImageUtils.getImageResourceIdForService(item.getServiceType());
            if (item.getLogoSmall() != null && !"".equals(item.getLogoSmall())) {
                showProgress(holder);
                ImageUtils.loadImage(
                        context,
                        holder.imageLogo,
                        holder.imageProgress,
                        item.getLogoSmall(),
                        serviceTypeImageId,
                        new DownloadShopImageCallback(
                                holder,
                                serviceTypeImageId
                        )
                );
            } else
                setServiceTypeAsLogo(holder, serviceTypeImageId);
        }

        if(mMyLocation != null) {
            holder.text2.setText(context.getResources().getString(R.string.shops_list_distance)
                    + " "+ FormatUtils.formatDistance(
                    context, distanceTo(item.getLatitude(), item.getLongitude())));
            holder.text2.setVisibility(View.VISIBLE);
        }
        else
            holder.text2.setVisibility(View.INVISIBLE);
    }

    @Override
    public void setLocation(Location location) {
        mMyLocation = location;
    }

    @Override
    public Location getLocation() {
        return mMyLocation;
    }

    @Override
    public void setServiceTypeVisible(boolean visible) {
        mServiceTypeVisible = visible;
    }

    protected float distanceTo(double latitude, double longitude) {
        if (mMyLocation != null) {
            Location.distanceBetween(
                    mMyLocation.getLatitude(),
                    mMyLocation.getLongitude(),
                    latitude, longitude, mDistanceResults);
            return mDistanceResults[0];
        }
        return 0;
    }

    private void showProgress(ViewHolder holder) {
        holder.imageProgress.setVisibility(View.VISIBLE);
        holder.imageLogo.setVisibility(View.INVISIBLE);
        if (holder.imageType != null)
            holder.imageType.setVisibility(View.GONE);
    }

    private void setServiceTypeAsLogo(ViewHolder holder, int serviceTypeResId) {
        holder.imageProgress.setVisibility(View.GONE);
        holder.imageLogo.setImageResource(serviceTypeResId);
        holder.imageLogo.setVisibility(View.VISIBLE);
        if (holder.imageType != null)
            holder.imageType.setVisibility(View.GONE);
    }

    private class DownloadShopImageCallback implements Callback {
        private ViewHolder mHolder;
        private int mServiceTypeImageResId;

        public DownloadShopImageCallback(ViewHolder holder, int serviceTypeImageResId) {
            mHolder = holder;
            mServiceTypeImageResId = serviceTypeImageResId;
        }

        @Override
        public void onSuccess() {
            if (mServiceTypeVisible && mHolder.imageType != null) {
                if (mServiceTypeImageResId != R.drawable.hk_ic_all_services) {
                    mHolder.imageType.setImageResource(mServiceTypeImageResId);
                    mHolder.imageType.setVisibility(View.VISIBLE);
                } else {
                    mHolder.imageType.setVisibility(View.GONE);
                }
            }
        }

        @Override
        public void onError() {

        }
    }

}
