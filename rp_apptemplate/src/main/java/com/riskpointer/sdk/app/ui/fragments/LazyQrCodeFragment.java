package com.riskpointer.sdk.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Asset;
import com.riskpointer.sdk.api.model.entity.HkQrCode;
import com.riskpointer.sdk.api.model.entity.ShopInfo;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.FragmentComponents;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.ui.strategy.ProgressContainerManager;

/**
 * @author Shurygin Denis
 */

public class LazyQrCodeFragment extends BaseFragment {

    public final static String ARG_QR_CODE = "LazyQrCodeFragment.ARG_QR_CODE";

    public final static String SAVED_QR_CODE = "LazyQrCodeFragment.SAVED_QR_CODE";

    private static final String FLO_TAG_PROGRESS = "LazyQrCodeFragment.FLO_TAG_PROGRESS";

    public final static String DLG_TAG_QR_CODE_LOAD_ERROR =
            "LazyQrCodeFragment.DLG_TAG_QR_CODE_LOAD_ERROR";

    private HkQrCode mQrCode;
    private Task mQrCodeLoadTask;

    private ProgressContainerManager mProgressManager;

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_lazy_container;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mProgressManager = (ProgressContainerManager) getLifeCycleObserver(FLO_TAG_PROGRESS);
        if (mProgressManager == null) {
            mProgressManager = new ProgressContainerManager(FLO_TAG_PROGRESS,
                    R.id.hk_progress, R.id.hk_content);
            addLifeCycleObserver(mProgressManager);
        }

        if (savedInstanceState != null) {
            onQrCodeGet((HkQrCode) savedInstanceState.getSerializable(SAVED_QR_CODE));
        } else {
            Bundle args = getArguments();
            if (args != null) {
                if (args.containsKey(ARG_QR_CODE)) {
                    Object object = args.get(ARG_QR_CODE);
                    if (object instanceof String) {
                        loadQrCode((String) object);
                    }
                    else
                        onQrCodeGet((HkQrCode) args.getSerializable(ARG_QR_CODE));
                }
                else
                    throw new IllegalStateException("Please, provide fragment argument ARG_QR_CODE");
            }
            else
                throw new IllegalStateException("Please, provide fragment argument ARG_QR_CODE");
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SAVED_QR_CODE, mQrCode);
    }

    @Override
    public void onDestroy() {
        if (mQrCodeLoadTask != null)
            mQrCodeLoadTask.abort();
        super.onDestroy();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mQrCode == null)
            mProgressManager.hideContent(false);
        else {
            onQrCodeGet(mQrCode);
            mProgressManager.showContent(false);
        }
    }

    @Override
    public boolean onDismissDialog(BaseDialogFragment dialogFragment) {
        if (DLG_TAG_QR_CODE_LOAD_ERROR.equals(dialogFragment.getTag())) {
            finish();
            return true;
        }
        return super.onDismissDialog(dialogFragment);
    }

    public HkQrCode getQrCode() {
        return mQrCode;
    }

    protected void onQrCodeGet(HkQrCode qrCode) {
        mQrCode = qrCode;

        if (mQrCode != null && getView() != null
                && getChildFragmentManager().findFragmentById(R.id.hk_content) == null) {

            getChildFragmentManager()
                    .beginTransaction()
                    .add(R.id.hk_content, getFragmentComponents(mQrCode).newFragment(), null)
                    .commitAllowingStateLoss();
        }
    }

    protected boolean onQrLoadError(RpError error) {
        if (!onError(error)) {
            showErrorMessage(DLG_TAG_QR_CODE_LOAD_ERROR, error);
        }
        return true;
    }

    protected FragmentComponents getFragmentComponents(HkQrCode qrCode) {
        if (TextUtils.isEmpty(qrCode.getObjectType()) || qrCode.getObject() == null)
            return AppController.getFragmentComponents(
                    AppController.SCREEN_QR_CODE_NO_TYPE,
                    qrCode);

        switch (qrCode.getObjectType()) {
            case HkQrCode.Type.ASSET:
                return AppController.getFragmentComponents(
                        AppController.SCREEN_ASSET,
                        qrCode.<Asset>getObject().getId(), qrCode);
            case HkQrCode.Type.SHOP:
                return AppController.getFragmentComponents(
                        AppController.SCREEN_SHOP,
                        qrCode.<ShopInfo>getObject().getShop(), qrCode);
            default:
                return new FragmentComponents(Fragment.class, null);
        }
    }

    protected void loadQrCode(String id) {
        mQrCodeLoadTask = HonkioApi.qrInfo(id, 0, new RequestCallback<HkQrCode>() {
            @Override
            public boolean onComplete(Response<HkQrCode> response) {
                mQrCodeLoadTask = null;
                if (response.isStatusAccept()) {
                    onQrCodeGet(response.getResult());
                    mProgressManager.showContent(true);
                    return true;
                }
                return onError(response.getAnyError());
            }

            @Override
            public boolean onError(RpError error) {
                mQrCodeLoadTask = null;
                return onQrLoadError(error);
            }
        });
    }
}
