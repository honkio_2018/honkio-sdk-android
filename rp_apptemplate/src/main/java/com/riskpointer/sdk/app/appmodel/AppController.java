package com.riskpointer.sdk.app.appmodel;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.app.FragmentComponents;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;

import java.io.IOException;
import java.io.Serializable;

/**
 * @author Shurygin Denis on 2014-11-28.
 */
public abstract class AppController {
    public static final String LOG_TAG = "AppController";

    //                                                       |<- 7 ->|<-- 8 -->|<----- 16 ----->|
    public static final int _CUSTOM          = 0x40000000; // 1000000 0000 0000 0000000000000000

    public static final int _SCREEN          = 0x20000000; // 0100000 0000 0000 0000000000000000
    public static final int _FRAGMENT        = 0x10000000; // 0010000 0000 0000 0000000000000000
    public static final int _DIALOG_FRAGMENT = 0x08000000; // 0001000 0000 0000 0000000000000000

    public static final int _SHOP            = 0x00800000; // 0000000 1000 0000 0000000000000000
    public static final int _SETTINGS        = 0x00400000; // 0000000 0100 0000 0000000000000000
    public static final int _REGISTRATION    = 0x00200000; // 0000000 0010 0000 0000000000000000

    public static final int ACTIVITY_SPLASH          = _SCREEN + 1;
    public static final int ACTIVITY_LOGIN           = _SCREEN + 2;
    public static final int ACTIVITY_PIN             = _SCREEN + 3;
    public static final int ACTIVITY_MAIN            = _SCREEN + 4;
    public static final int ACTIVITY_PAGER           = _SCREEN + 5;

    public static final int ACTIVITY_CAPTURE       = _SCREEN + 6;
    public static final int ACTIVITY_ABOUT         = _SCREEN + 7;
    public static final int ACTIVITY_TOU           = _SCREEN + 8;
    public static final int ACTIVITY_GDPR          = _SCREEN + 9;
    public static final int SCREEN_INVENTORY       = _SCREEN + 10;

    /**
     * Required following parameters:<br>
     * 1) Asset to show. There is able following options:<br>
     *  <ul>
     *    <li>String - Id of the Asset</li>
     *    <li>Asset object</li>
     *  </ul>
     * 2) HkQrCode of the asset if it was get by QR-Code.
     */
    public static final int SCREEN_ASSET           = _SCREEN + 10;

    /**
     * Required following parameters:<br>
     * 1) QrCode to show. There is able following options:<br>
     *  <ul>
     *    <li>String - Id of the QR-Code</li>
     *    <li>HkQrCode object</li>
     *  </ul>
     */
    public static final int SCREEN_QR_CODE         = _SCREEN + 11;

    /**
     * Required following parameters:<br>
     * 1) QrCode to show. There is able following options:<br>
     *  <ul>
     *    <li>String - Id of the QR-Code</li>
     *    <li>HkQrCode object</li>
     *  </ul>
     */
    public static final int SCREEN_QR_CODE_NO_TYPE = _SCREEN + 12;

    public static final int SCREEN_MAP_SELECT_ADDRESS   = _SCREEN + 13;
    public static final int SCREEN_ASSET_DETAILS = _SCREEN + 14;
    public static final int SCREEN_INVITATION_DETAILS = _SCREEN + 15;

    public static final int SCREEN_SETTINGS_MAIN                = _SCREEN + _SETTINGS + 1;
    public static final int SCREEN_SETTINGS_USER_ACCOUNTS       = _SCREEN + _SETTINGS + 2;
    public static final int SCREEN_SETTINGS_USER_ADD_ACCOUNT    = _SCREEN + _SETTINGS + 3;
    public static final int SCREEN_SETTINGS_USER_ACCOUNT_LOAN   = _SCREEN + _SETTINGS + 5;
    public static final int SCREEN_SETTINGS_USER_PROFILE        = _SCREEN + _SETTINGS + 6;

    public static final int SCREEN_SETTINGS_CHANGE_PASSWORD = _SCREEN + _SETTINGS + 8;
    public static final int SCREEN_SETTINGS_LOST_PASSWORD   = _SCREEN + _SETTINGS + 9;
    public static final int SCREEN_SETTINGS_CHANGE_PIN      = _SCREEN + _SETTINGS + 10;

    public static final int SCREEN_SHOPS_LIST           = _SCREEN + _SHOP + 1;
    public static final int SCREEN_SHOP                 = _SCREEN + _SHOP + 2;
    public static final int SCREEN_SHOP_PRODUCT         = _SCREEN + _SHOP + 3;
    public static final int SCREEN_SHOP_TRANSACTION     = _SCREEN + _SHOP + 5;

    /**
     * Required following parameters:<br>
     * 1) Order to show. There is able following options:<br>
     *  <ul>
     *    <li>String - Id of the order</li>
     *    <li>Oder</li>
     *  </ul>
     * 2) Identity - identity of the order shop<br>
     */
    public static final int SCREEN_SHOP_ORDER           = _SCREEN + _SHOP + 6;
    public static final int SCREEN_SHOPS_MAP            = _SCREEN + _SHOP + 7;
    public static final int SCREEN_ORDERS_LIST          = _SCREEN + _SHOP + 8;
    public static final int SCREEN_TRANSACTIONS_LIST    = _SCREEN + _SHOP + 9;

    /** Require Order as 1-st parameter. */
    public static final int SCREEN_CHAT                 = _SCREEN + _SHOP + 10;

    public static final int SCREEN_PUSH                 = _SCREEN + _SHOP + 31;

    public static final int FRAGMENT_SHOP_DETAILS       = _FRAGMENT + 1;
    public static final int FRAGMENT_PUSHES_LIST        = _FRAGMENT + 101;

    public static final int DIALOG_FRAGMENT_MESSAGE          = _DIALOG_FRAGMENT + 1;
    public static final int DIALOG_FRAGMENT_PROGRESS         = _DIALOG_FRAGMENT + 2;
    public static final int DIALOG_FRAGMENT_ACCOUNT_CHOOSE   = _DIALOG_FRAGMENT + 3;
    public static final int DIALOG_FRAGMENT_BARCODE          = _DIALOG_FRAGMENT + 4;

    public static final class Extra {
        public static final String USER_ACCOUNT = "Extra_USER_ACCOUNT";
    }

    private static AppController sInstance;

    public static void initInstance(AppController instance) {
        if (sInstance != null)
            throw new IllegalStateException("Instance already initialized.");
        sInstance = instance;

        HonkioApi.initInstance(sInstance.mAppContext,
                sInstance.appUrl,
                sInstance.appIdentity);

        sInstance.onInitialized();
    }

    public static AppController getInstance() {
        if (sInstance == null)
            throw new IllegalStateException("Init instance before calling getInstance.");
        return sInstance;
    }

    public static boolean isInitialized() {
        return sInstance != null;
    }

    protected final Context mAppContext;

    /** Server URL on which the application will send requests. */
    public final String appUrl;
    /** Identity of the app which contains application ID and password. */
    public final Identity appIdentity;

    protected abstract Intent buildNewIntent(int intentId, Object... params);
    protected abstract FragmentComponents buildNewFragmentComponents(int fragmentId, Object... params);

    public AppController(Context applicationContext, String appUrl, Identity appIdentity) {
        if (applicationContext == null)
            throw new IllegalArgumentException("applicationContext should be not null!");
        if (appUrl == null)
            throw new IllegalArgumentException("getAppUrl should be not null!");
        if (appIdentity == null)
            throw new IllegalArgumentException("appIdentity should be not null!");

        if (applicationContext.getApplicationContext() == null)
            mAppContext = applicationContext;
        else
            mAppContext = applicationContext;

        this.appUrl = appUrl;
        this.appIdentity = appIdentity;
    }

    public static Intent getIntent(int intentId, Object... params) {
        Intent intent = getInstance().buildNewIntent(intentId, params);
        intent.putExtra(BaseActivity.EXTRA_INTENT_ID, intentId);
        return intent;
    }

    public static FragmentComponents getFragmentComponents(int fragmentId, Object... params) {
        return getInstance().buildNewFragmentComponents(fragmentId, params);
    }

    public static void trackScreen(String screenName) {
        getInstance().onTrackScreen(screenName);
    }

    public boolean isGuestModeAvailable() { return false; }

    protected static boolean isActivityId(int id) {
        return isIdByMask(id, _SCREEN);
    }

    protected static boolean isFragmentId(int id) {
        return isIdByMask(id, _FRAGMENT);
    }

    protected static boolean isDialogId(int id) {
        return isIdByMask(id, _DIALOG_FRAGMENT);
    }

    protected static boolean isIdByMask(int id, int mask) {
        return (id & mask) == mask;
    }

    protected static boolean isHasParam(Object[] params, int index) {
        return params != null && params.length > index && params[index] != null;
    }

    protected static Object getParam(Object[] params, int index) {
        if (params != null && params.length > index)
            return params[index];
        return null;
    }

    protected static boolean assertClass(Object[] params, int index, Class expectedClass) {
        return assertClass(params, index, expectedClass, true);
    }

    protected static boolean assertClass(Object[] params, int index, Class expectedClass, boolean allowNull) {
        Object object = getParam(params, index);
        if (object == null && !allowNull)
            throw new IllegalArgumentException("Parameter at index " + index + " is null. Not null value expected.");
        if (object != null && !expectedClass.isInstance(object))
            throw new IllegalArgumentException("Parameter at index " + index + " is " + object.getClass().getName() + ". Instance of " + expectedClass.getName() + " expected.");
        return object != null;
    }

    protected static void putString(Bundle args, String key, Object[] params, int index) {
        if (assertClass(params, index, String.class)) {
            args.putString(key, (String) params[index]);
        }
    }

    protected static void putInt(Bundle args, String key, Object[] params, int index) {
        if (assertClass(params, index, Integer.class)) {
            args.putInt(key, (Integer) params[index]);
        }
    }

    protected static void putDouble(Bundle args, String key, Object[] params, int index) {
        if (assertClass(params, index, Double.class)) {
            args.putDouble(key, (Double) params[index]);
        }
    }

    protected static void putBoolean(Bundle args, String key, Object[] params, int index) {
        if (assertClass(params, index, Boolean.class)) {
            args.putBoolean(key, (Boolean) params[index]);
        }
    }

    protected static void putBundle(Bundle args, String key, Object[] params, int index) {
        if (assertClass(params, index, Bundle.class)) {
            args.putBundle(key, (Bundle) params[index]);
        }
    }

    protected static void putSerializable(Bundle args, String key, Object[] params, int index, Class paramClass) {
        if (assertClass(params, index, paramClass)) {
            args.putSerializable(key, (Serializable) params[index]);
        }
    }

    protected static void putParcelable(Bundle args, String key, Object[] params, int index, Class paramClass) {
        if (assertClass(params, index, paramClass)) {
            args.putParcelable(key, (Parcelable) params[index]);
        }
    }

    protected void onTrackScreen(String screenName) { }

    protected void onInitialized() {
        initGcm();
    }

//==================================================================================================
//====================================== Google Cloud Messages =====================================
//==================================================================================================
    public static final String PROPERTY_SENDER_ID = "sender_id";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION_NUMBER = "appVersionNumber";
    private static final String PROPERTY_APP_VERSION_NAME = "appVersionName";

    /**
     * Gets GCM seder id. If id is null the app doesn't use GCM.
     *
     * @return GCM seder id or null if app doesn't use GCM.
     */
    public String getGcmSenderId() {
        return null;
    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    public String getGcmRegistrationId() {
        final SharedPreferences prefs = getGCMPreferences();
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(LOG_TAG, "GCM Registration not found.");
            return null;
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersionNumber = prefs.getInt(PROPERTY_APP_VERSION_NUMBER, Integer.MIN_VALUE);
        int currentVersionNumber = getAppVersionNumber();
        String registeredVersionName = prefs.getString(PROPERTY_APP_VERSION_NAME, null);
        String currentVersionName = getAppVersionName();
        if (registeredVersionNumber != currentVersionNumber || !currentVersionName.equals(registeredVersionName)) {
            Log.i(LOG_TAG, "App version changed.");
            return null;
        }

        // Check if senderId was changed.
        String senderId = prefs.getString(PROPERTY_SENDER_ID, null);
        if (senderId == null || senderId.isEmpty() || !senderId.equals(getGcmSenderId())) {
            Log.i(LOG_TAG, "Sender ID changed or empty.");
            return null;
        }
        return registrationId;
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    public boolean checkGcmPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mAppContext);
        if (resultCode != ConnectionResult.SUCCESS) {
            Log.i(LOG_TAG, "This device is not supported.");
            return false;
        }
        return true;
    }

    public void onGcmRegistrationComplete(String regId) {

    }

    private void initGcm() {
        if (getGcmSenderId() != null && checkGcmPlayServices()) {
            if (getGcmRegistrationId() == null) {
                registerInBackground();
            }
        }
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences() {
        return mAppContext.getSharedPreferences(this.getClass().getName(), Context.MODE_PRIVATE);
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private int getAppVersionNumber() {
        try {
            PackageInfo packageInfo = mAppContext.getPackageManager()
                    .getPackageInfo(mAppContext.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private String getAppVersionName() {
        try {
            PackageInfo packageInfo = mAppContext.getPackageManager()
                    .getPackageInfo(mAppContext.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        final String senderId = getGcmSenderId();
        if (senderId != null) {
            AsyncTask<Void, Void, String> registerTask = new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    String regId = null;
                    try {
                        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(mAppContext);
                        regId = gcm.register(senderId);

                        storeRegistrationId(senderId, regId);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return regId;
                }

                @Override
                protected void onPostExecute(String regId) {
                    if (regId != null)
                        onGcmRegistrationComplete(regId);
                }
            };
            registerTask.execute(null, null, null);
        }
    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param regId registration ID
     */
    private void storeRegistrationId(String senderId, String regId) {
        final SharedPreferences prefs = getGCMPreferences();
        int appVersionNumber = getAppVersionNumber();
        String appVersionName = getAppVersionName();
        Log.i(LOG_TAG, "Saving GCM regId on app version " + appVersionName + "(" + appVersionNumber + ")");
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_SENDER_ID, senderId);
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION_NUMBER, appVersionNumber);
        editor.putString(PROPERTY_APP_VERSION_NAME, appVersionName);
        editor.commit();
    }
}
