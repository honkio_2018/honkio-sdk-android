package com.riskpointer.sdk.app.ui.adapters.binder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.riskpointer.sdk.api.model.entity.Product;
import com.riskpointer.sdk.api.utils.Currency;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.utils.FormatUtils;

/**
 * Created by Shurygin Denis on 2016-11-28.
 */

public abstract class AbsShopProductListBinder
        <ListItem extends Product, Holder extends AbsShopProductListBinder.ViewHolder>
        implements ListBinder<ListItem, Holder> {

    @Override
    public void bind(Context context, Holder holder, ListItem product) {
        if (holder.name != null)
            holder.name.setText(product.getName());

        if (holder.desc != null)
            holder.desc.setText(product.getDescription());

        if (holder.amount != null) {
            holder.amount.setText(FormatUtils.format(product.getAmount()));
        }

        if (holder.currency != null)
            holder.currency.setText(Currency.getSymbol(product.getCurrency()));
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView name;
        public final TextView desc;
        public final TextView amount;
        public final TextView currency;
        public final TextView duration;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.hk_txtName);
            desc = (TextView) view.findViewById(R.id.hk_txtDesc);
            amount = (TextView) view.findViewById(R.id.hk_txtAmount);
            currency = (TextView) view.findViewById(R.id.hk_txtCurrency);
            duration = (TextView) view.findViewById(R.id.hk_txtDuration);
        }
    }
}
