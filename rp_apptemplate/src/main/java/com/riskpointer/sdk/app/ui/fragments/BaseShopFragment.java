package com.riskpointer.sdk.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.ShopFind;
import com.riskpointer.sdk.api.model.entity.ShopInfo;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.FragmentComponents;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.strategy.ProgressContainerManager;

/**
 * @author Shurygin Denis
 */

public abstract class BaseShopFragment extends BaseFragment {

    public static final String ARG_SHOP = "BaseShopFragment.ARG_SHOP";

    private static final String FLO_TAG_PROGRESS = "BaseShopFragment.ProgressContainerManager";

    private static final String SAVED_SHOP = "BaseShopFragment.SAVED_SHOP";

    private ShopInfo mShopInfo;

    private ProgressContainerManager mProgressManager;

    protected abstract FragmentComponents buildContentFragmentComponents(ShopInfo shopInfo);

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_lazy_container;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mProgressManager = (ProgressContainerManager) getLifeCycleObserver(FLO_TAG_PROGRESS);
        if (mProgressManager == null) {
            mProgressManager = new ProgressContainerManager(FLO_TAG_PROGRESS,
                    R.id.hk_progress, R.id.hk_content);
            addLifeCycleObserver(mProgressManager);
        }

        if (savedInstanceState != null)
            mShopInfo = (ShopInfo) savedInstanceState.getSerializable(SAVED_SHOP);

        if (mShopInfo == null) {
            Bundle args = getArguments();
            if (args == null || !args.containsKey(ARG_SHOP))
                throw new IllegalStateException("Missing argument BaseShopFragment.ARG_SHOP");

            String shopId = args.getString(ARG_SHOP);
            if (shopId != null) {
                loadShop(shopId);
            }
            else {
                Object shopObject = args.getSerializable(ARG_SHOP);
                if (shopObject instanceof ShopInfo) {
                    mShopInfo = (ShopInfo) shopObject;
                }
                else if (shopObject instanceof Identity) {
                    loadShop((Identity) shopObject);
                }
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mShopInfo != null) {
            updateContent(mShopInfo);
            mProgressManager.showContent(false);
        }
        else
            mProgressManager.hideContent(false);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SAVED_SHOP, mShopInfo);
    }

    private void loadShop(String shopId) {
        HonkioApi.shopFindById(shopId, 0, new RequestCallback<ShopFind>() {
            @Override
            public boolean onComplete(Response<ShopFind> response) {
                if (response.isStatusAccept()) {
                    if (response.getResult().getList().size() > 0) {
                        loadShop(response.getResult().getList().get(0));
                        return true;
                    }
                    else {
                        // TODO Shop not found
                        return onError(response.getAnyError());
                    }
                }
                return onError(response.getAnyError());
            }

            @Override
            public boolean onError(RpError error) {
                return BaseShopFragment.this.onError(error);
            }
        });
    }

    private void loadShop(final Identity shopIdentity) {
        HonkioApi.shopGetInfo(shopIdentity, 0, new RequestCallback<ShopInfo>() {
            @Override
            public boolean onComplete(Response<ShopInfo> response) {
                if (response.isStatusAccept()) {
                    mShopInfo = response.getResult();
                    updateContent(mShopInfo);
                    mProgressManager.showContent(true);
                    return true;
                }
                return onError(response.getAnyError());
            }

            @Override
            public boolean onError(RpError error) {
                return BaseShopFragment.this.onError(error);
            }
        });
    }

    private void updateContent(ShopInfo shop) {
        if (isAdded() && getFragmentManager().findFragmentById(R.id.hk_content) == null) {
            FragmentComponents fragmentComponents = buildContentFragmentComponents(shop);

            if (fragmentComponents == null)
                fragmentComponents = new FragmentComponents(
                        ShopDetailsUnsupportedFragment.class, null);

            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.hk_content, fragmentComponents.newFragment())
                    .commit();
        }

    }
}
