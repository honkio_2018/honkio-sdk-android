package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.riskpointer.sdk.app.ui.adapters.binder.SimpleListBinder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shurygin Denis on 2016-07-25.
 */
public abstract class BaseRecyclerListAdapter<Item, VH extends RecyclerView.ViewHolder> extends BaseRecyclerAdapter<VH> {

    public interface OnItemClickListener<Item, VH extends RecyclerView.ViewHolder> {
        void onItemClick(VH itemHolder, View view, Item item);
    }

    public interface OnItemLongClickListener<Item, VH extends RecyclerView.ViewHolder> {
        boolean onItemLongClick(VH itemHolder, Item item);
    }

    private OnItemLongClickListener<Item, VH> mItemLongClickListener;

    private List<ItemClick<Item, VH>> mItemClicks = new ArrayList<>();

    private List<Item> mList;
    private List<SimpleListBinder<Item, VH>> mBinders = new ArrayList<>();

    public BaseRecyclerListAdapter(Context context, List<Item> list, int layout) {
        this(context, list, new int[]{layout});
    }

    public BaseRecyclerListAdapter(Context context, List<Item> list, int[] layouts) {
        super(context, layouts);
        mList = list;
    }

    @Override
    public VH onCreateItemViewHolder(ViewGroup parent, int viewType) {
        final VH holder = super.onCreateItemViewHolder(parent, viewType);

        for (final ItemClick<Item, VH> itemClick: mItemClicks) {
            View clickView = itemClick.viewId == 0 ?
                    holder.itemView :
                    holder.itemView.findViewById(itemClick.viewId);

            if (clickView != null)
                clickView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        itemClick.listener.onItemClick(holder, view,
                                getItem(holder.getAdapterPosition()));
                    }
                });
        }

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return onItemLongClick(holder);
            }
        });

        return holder;
    }

    @Override
    public int getItemCount() {
        if(mList == null) return super.getItemCount();
        return mList.size() + super.getItemCount();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        if (position >= getHeaderViewCount() && position < getItemCount() - getFooterViewCount()) {
            for (SimpleListBinder<Item, VH> binder : mBinders) {
                binder.bind(mContext, (VH) holder, getItem(position));
            }
        }
    }

    public Item getItem(int position) {
        if (isListPosition(position) && mList.size() > 0)
            return mList.get(position - getHeaderViewCount());

        return null;
    }

    public void move(int position, int newPosition) {
        Item object = mList.remove(position - getHeaderViewCount());
        mList.add(newPosition - getHeaderViewCount(), object);
        notifyItemMoved(position, newPosition);
    }

    public void add(Item item) {
        insert(item, mList.size());
    }

    public void insert(Item item, int position) {
        mList.add(position, item);
        //notifyItemInserted(position);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        mList.remove(position);
        notifyItemRemoved(position);
    }

    public void changeList(List<Item> list) {
        mList = list;
        notifyDataSetChanged();
    }

    public void add(List<Item> list) {
        mList.addAll(list);
        notifyItemRangeInserted(mList.size() - list.size(), mList.size());
    }

    public void addOnItemClickListener(OnItemClickListener<Item, VH> listener) {
        addOnItemClickListener(listener, 0);
    }

    public void addOnItemClickListener(OnItemClickListener<Item, VH> listener, int viewId) {
        mItemClicks.add(new ItemClick<>(listener, viewId));
    }

    public void setOnItemLongClickListener(OnItemLongClickListener<Item, VH> listener) {
        mItemLongClickListener = listener;
    }

    public void addItemBinder(SimpleListBinder<Item, VH> binder) {
        mBinders.add(binder);
    }

    public void removeItemBinder(SimpleListBinder<Item, VH> binder) {
        mBinders.remove(binder);
    }

    protected boolean onItemLongClick(VH itemHolder) {
        if (mItemLongClickListener != null)
            return mItemLongClickListener.onItemLongClick(itemHolder, getItem(itemHolder.getAdapterPosition()));
        return false;
    }

    protected boolean isListPosition(int position) {
        return mList != null && (position >= getHeaderViewCount() && position < getHeaderViewCount() + mList.size());
    }

    private static class ItemClick<Item, VH extends RecyclerView.ViewHolder> {

        private final OnItemClickListener<Item, VH> listener;
        private final int viewId;

        private ItemClick(OnItemClickListener<Item, VH> listener, int viewId) {
            this.listener = listener;
            this.viewId = viewId;
        }

    }

}
