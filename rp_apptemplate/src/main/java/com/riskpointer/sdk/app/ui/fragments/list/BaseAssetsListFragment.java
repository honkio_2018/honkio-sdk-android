package com.riskpointer.sdk.app.ui.fragments.list;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.merchant.MerchantCommand;
import com.riskpointer.sdk.api.model.entity.Asset;
import com.riskpointer.sdk.api.model.entity.AssetList;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.support.BroadcastManager;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.message.filter.AssetFilter;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.utils.FragmentUtils;

import java.util.List;

/**
 * @author Shurygin Denis
 */

public abstract class BaseAssetsListFragment extends BaseRecycleLazyListFragment<Asset> {

    public static final String ARG_FILTER = "BaseAssetsListFragment.ARG_FILTER";

    private AssetFilter mFilter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        BroadcastManager.getInstance(getActivity()).registerReceiver(mAssetUpdateListener,
                new IntentFilter(BroadcastHelper.getActionOnComplete(MerchantCommand.MERCHANT_ASSET_SET)));


        if (savedInstanceState != null) {
            mFilter = (AssetFilter) savedInstanceState.getSerializable(ARG_FILTER);
        } else if (mFilter == null) {
            Bundle args = getArguments();
            if (args != null)
                mFilter = (AssetFilter) args.getSerializable(ARG_FILTER);
        }
    }

    @Override
    public void onDestroy() {
        BroadcastManager.getInstance(getActivity()).unregisterReceiver(mAssetUpdateListener);
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(ARG_FILTER, mFilter);
    }

    public void setFilter(AssetFilter filter) {
        mFilter = filter;
    }

    public AssetFilter getFilter() {
        if (mFilter == null) {
            Bundle args = getArguments();
            if (args != null)
                mFilter = (AssetFilter) args.getSerializable(ARG_FILTER);

            if (mFilter == null) {
                mFilter = new AssetFilter();
            }

        }
        return mFilter;
    }

    @Override
    public ListLoader<Asset, ?> onCreateLoader(int id, Bundle args) {
        return new AssetListLoader(getActivity().getApplicationContext(), getFilter(),
                FragmentUtils.getShopIdentity(this, null));
    }

    public static class AssetListLoader extends ListLoader<Asset, AssetList> {
        protected AssetFilter mFilter;
        protected Identity mShop;

        public AssetListLoader(Context context, AssetFilter filter, Identity shop) {
            super(context);
            mFilter = filter;
            mShop = shop;
        }

        @Override
        protected List<Asset> toList(Response<AssetList> response) {
            return response.getResult().getList();
        }

        @Override
        protected Task loadList(Context context, RequestCallback<AssetList> callback, int count, int skip) {
            if (mFilter == null) {
                mFilter = new AssetFilter();
            }
            mFilter.setCount(count);
            mFilter.setSkip(skip);
            return HonkioApi.userAssetList(mFilter, mShop, 0, callback);
        }
    }

    private BroadcastReceiver mAssetUpdateListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            reloadList();
        }
    };
}
