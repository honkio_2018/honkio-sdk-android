/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.riskpointer.sdk.app.R;

public class ServiceListAdapter extends BaseAdapter {
	
	private ListItem[] mList;
	private LayoutInflater mInflater;
    private int mLayout;

	public ServiceListAdapter(Context context, int layout, ListItem[] list) {
		mInflater = LayoutInflater.from(context);
		mList = list;
        mLayout = layout;
	}

	@Override
	public int getCount() {
		return mList.length;
	}

	@Override
	public ListItem getItem(int position) {
		return mList[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder holder = null;
		if(view == null) {
			view = mInflater.inflate(mLayout, null);
			holder = new ViewHolder();
			holder.text = (TextView) view.findViewById(android.R.id.text1);
			holder.image = (ImageView) view.findViewById(R.id.hk_imgTypeLogo);
			view.setTag(holder);
		}
		else
			holder = (ViewHolder) view.getTag();
		
		ListItem item = getItem(position);
		holder.text.setText(item.text);

		if (holder.image != null)
			holder.image.setImageResource(item.image);
		
		return view;
	}
	
	private class ViewHolder {
		TextView text;
		ImageView image;
	}

	
	public static class ListItem {
		public final String service_type;
		public final int text;
		public final int image;
		
		public ListItem(String service_type, int text, int image) {
			this.service_type = service_type;
			this.text = text;
			this.image = image;
		}
	}
}