package com.riskpointer.sdk.app.ui.strategy;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.BookedProduct;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.Order;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.api.model.process.BaseProcessModel;
import com.riskpointer.sdk.api.model.process.PinReason;
import com.riskpointer.sdk.api.model.process.user.OrderSetProcessModel;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.ui.dialogs.MessageDialog;
import com.riskpointer.sdk.app.ui.fragments.PinFragment;
import com.riskpointer.sdk.app.utils.RequestCodes;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Shurygin Denis on 2016-07-14.
 */
public class OrderSetViewStrategy extends BaseViewLifeCycleObserver implements RequestCallback<Order>, UserAccountSelectionViewStrategy.Delegate {

    public interface Delegate {
        void onStart(OrderSetViewStrategy strategy);
        boolean onComplete(OrderSetViewStrategy strategy, Response<Order> response);
        boolean onError(OrderSetViewStrategy strategy, RpError error);
    }

    protected static final int REQUEST_LOGIN = 4;

    private static final String SAVED_ASK_PIN = "OrderSetViewStrategy.SAVED_ASK_PIN";
    private static final String SAVED_USE_MERCHANT_API = "OrderSetViewStrategy.SAVED_USE_MERCHANT_API";
    private static final String SAVED_ACCOUNT_REQUIRED = "OrderSetViewStrategy.SAVED_ACCOUNT_REQUIRED";
    private static final String SAVED_REMOVE_ON_COMPLETION = "OrderSetViewStrategy.SAVED_REMOVE_ON_COMPLETION";
    private static final String SAVED_PROCESS_MODEL = "OrderSetViewStrategy.SAVED_PROCESS_MODEL";

    private static final String DLG_TAG_NEED_LOGIN = "OrderSetViewStrategy.DLG_TAG_NEED_LOGIN";

    private static final String LCO_TAG_ACCOUNT_SELECTION = "OrderSetViewStrategy.LCO_TAG_ACCOUNT_SELECTION";

    private boolean isPinRequired = true;
    private boolean isAccountRequired = true;
    private boolean isRemoveOnCompletion = false;
    private boolean isUseMerchantApi = false;

    private OrderSetProcessModelImpl mProcessModel;

    public OrderSetViewStrategy() {
        this(OrderSetViewStrategy.class.getName());
    }

    public OrderSetViewStrategy(String tag) {
        super(tag);
        if (tag == null)
            throw new IllegalArgumentException("Tag must be not null");
    }

    public void userSetOrder(Order order, int flags) {
        userSetOrder(null, order, flags);
    }

    public void userSetOrder(Identity shopIdentity, Order order, int flags) {
        if (!isAttached())
            throwAttachFirst();

        onProcessStart();

        if (mProcessModel != null && mProcessModel.getStatus() != BaseProcessModel.Status.CREATED) {
            mProcessModel.abort();
        }
        mProcessModel = newProcessModel();
        mProcessModel.setStrategy(this);
        mProcessModel.setPinRequired(isPinRequired);
        mProcessModel.setUseMerchantApi(isUseMerchantApi);
        mProcessModel.setAccountRequired(isAccountRequired);
        mProcessModel.userSetOrder(shopIdentity, order, flags);
    }

    @Override
    public void restoreState(ViewController viewController, Bundle savedInstanceState) {
        super.restoreState(viewController, savedInstanceState);
        isPinRequired = savedInstanceState.getBoolean(SAVED_ASK_PIN, true);
        isUseMerchantApi = savedInstanceState.getBoolean(SAVED_USE_MERCHANT_API, true);
        isAccountRequired = savedInstanceState.getBoolean(SAVED_ACCOUNT_REQUIRED, true);
        isRemoveOnCompletion = savedInstanceState.getBoolean(SAVED_REMOVE_ON_COMPLETION, false);
        if (savedInstanceState.containsKey(SAVED_PROCESS_MODEL)) {
            if (mProcessModel == null) {
                mProcessModel = (OrderSetProcessModelImpl) BaseProcessModel
                        .restoreFromSavedState(viewController.getContext(),
                                savedInstanceState.getBundle(SAVED_PROCESS_MODEL));
                if (mProcessModel != null)
                    mProcessModel.setStrategy(this);
            }
        }
    }

    @Override
    public void saveState(ViewController viewController, Bundle outState) {
        super.saveState(viewController, outState);

        outState.putBoolean(SAVED_ASK_PIN, isPinRequired);
        outState.putBoolean(SAVED_USE_MERCHANT_API, isUseMerchantApi);
        outState.putBoolean(SAVED_ACCOUNT_REQUIRED, isAccountRequired);
        outState.putBoolean(SAVED_REMOVE_ON_COMPLETION, isRemoveOnCompletion);

        if (mProcessModel != null) {
            outState.putBundle(SAVED_PROCESS_MODEL, mProcessModel.saveInstanceState());
        }
    }

    @Override
    public void onActivityResult(ViewController viewController, int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCodes.ORDER_SET_FE_PIN_REQUEST) {
            if (resultCode == Activity.RESULT_OK)
                mProcessModel.onPinEntered(data.getStringExtra(PinFragment.EXTRA_PIN));
            else
                mProcessModel.onError(new RpError(RpError.Api.CANCELED_BY_USER));
        }
        else if (requestCode == REQUEST_LOGIN) {
            if (HonkioApi.isConnected())
                mProcessModel.resume();
            else
                mProcessModel.onError(new RpError(RpError.Api.CANCELED_BY_USER));
        }
        else
            super.onActivityResult(viewController, requestCode, resultCode, data);
    }

    @Override
    public boolean onClickDialog(ViewController viewController, BaseDialogFragment dialogFragment, int which) {
        if (DLG_TAG_NEED_LOGIN.equals(dialogFragment.getTag())) {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                getController().startActivityForResult(
                        AppController.getIntent(AppController.ACTIVITY_LOGIN), REQUEST_LOGIN);
            }
            else
                mProcessModel.onError(new RpError(RpError.Api.CANCELED_BY_USER));
            return true;
        }
        return super.onClickDialog(viewController, dialogFragment, which);
    }

    /** Will call only for response with status = accept */
    @Override
    public boolean onComplete(Response<Order> response) {
        Delegate delegate = getDelegate();

        if (isRemoveOnCompletion) {
            ViewController viewController = getController();
            if (viewController != null)
                viewController.removeLifeCycleObserver(this);
        }

        boolean result = delegate != null && delegate.onComplete(this, response);

        return result;
    }

    @Override
    public boolean onError(RpError error) {
        Delegate delegate = getDelegate();

        if (isRemoveOnCompletion) {
            ViewController viewController = getController();
            if (viewController != null)
                viewController.removeLifeCycleObserver(this);
        }

        boolean result = (delegate != null && delegate.onError(this, error))
                || (getController() != null && onError(getController(), error));

        return result;
    }

    @Override
    public void onAccountSelect(UserAccountSelectionViewStrategy strategy, UserAccount account) {
        if (LCO_TAG_ACCOUNT_SELECTION.equals(strategy.getTag())) {
            mProcessModel.onAccountSelected(account);
        }
    }

    @Override
    public void onAccountSelectionCancel(UserAccountSelectionViewStrategy strategy) {
        if (LCO_TAG_ACCOUNT_SELECTION.equals(strategy.getTag())) {
            mProcessModel.onError(new RpError(RpError.Api.CANCELED_BY_USER));
        }
    }

    public OrderSetViewStrategy setUseMerchantApi(boolean use) {
        isUseMerchantApi = use;
        if (mProcessModel != null)
            mProcessModel.setUseMerchantApi(use);
        return this;
    }

    public OrderSetViewStrategy setPinRequired(boolean pinRequired) {
        isPinRequired = pinRequired;
        if (mProcessModel != null)
            mProcessModel.setPinRequired(pinRequired);
        return this;
    }

    public OrderSetViewStrategy setAccountRequired(boolean accountRequired) {
        isAccountRequired = accountRequired;
        if (mProcessModel != null)
            mProcessModel.setAccountRequired(accountRequired);
        return this;
    }

    public OrderSetViewStrategy setRemoveOnCompletion(boolean remove) {
        isRemoveOnCompletion = remove;
        return this;
    }

    public BaseProcessModel.Status getStatus() {
        if (mProcessModel != null)
            return mProcessModel.getStatus();
        return BaseProcessModel.Status.CREATED;
    }

    public boolean isFinishedOrAborted() {
        if (mProcessModel != null) {
            return mProcessModel.getStatus() == BaseProcessModel.Status.FINISHED
                    || mProcessModel.getStatus() == BaseProcessModel.Status.ABORTED;
        }
        return false;
    }

    protected void onProcessCancel() {
        mProcessModel.abort();
    }

    protected OrderSetProcessModelImpl newProcessModel() {
        return new OrderSetProcessModelImpl();
    }

    private void onAccountRequired() {
        UserAccountSelectionViewStrategy strategy = getController()
                        .getLifeCycleObserver(LCO_TAG_ACCOUNT_SELECTION);

        if (strategy == null) {
            strategy = new UserAccountSelectionViewStrategy(LCO_TAG_ACCOUNT_SELECTION);
            strategy.setParentLifeCycleObserverTag(getTag());

            Set<String> disallowedAccounts = new HashSet<>();
            if (mProcessModel.getOrder().getProducts() != null)
                for (BookedProduct bookedProduct: mProcessModel.getOrder().getProducts())
                    disallowedAccounts.addAll(bookedProduct.getProduct().getDisallowedAccounts());
            strategy.setDisallowedAccounts(disallowedAccounts);

            getController().addLifeCycleObserver(strategy);
        }

        strategy.selectAccount(
                mProcessModel.getShopIdentity() instanceof Shop
                        ? (Shop) mProcessModel.getShopIdentity() : null,
                mProcessModel.getOrder().getAmount(),
                mProcessModel.getOrder().getCurrency());
    }

    private void onLoginRequired() {
        showMessage(DLG_TAG_NEED_LOGIN,
                R.string.dlg_login_needed_title,
                R.string.dlg_login_needed_message,
                MessageDialog.BTN_OK_CANCEL);
    }

    private void onPinRequired() {
        getController().startActivityForResult(
                AppController.getIntent(AppController.ACTIVITY_PIN, PinReason.PURCHASE),
                RequestCodes.ORDER_SET_FE_PIN_REQUEST
        );
    }

    public static class OrderSetProcessModelImpl extends OrderSetProcessModel {

        private OrderSetViewStrategy mStrategy;

        public OrderSetProcessModelImpl() {
            super();
        }

        @Override
        protected void stepRequest() {
            if (mStrategy != null)
                mStrategy.showProgressDialog();
            super.stepRequest();
        }

        @Override
        protected void stepShopRequest() {
            if (mStrategy != null)
                mStrategy.showProgressDialog();
            super.stepShopRequest();
        }

        @Override
        public void onAccountRequired() {
            if (mStrategy != null)
                mStrategy.onAccountRequired();
        }

        @Override
        public void onLoginRequired() {
            if (mStrategy != null)
                mStrategy.onLoginRequired();
        }

        @Override
        public void onPinRequired() {
            if (mStrategy != null)
                mStrategy.onPinRequired();
        }

        @Override
        public boolean onComplete(Response<Order> response) {
            if (mStrategy != null) {
                mStrategy.dismissProgressDialog();
                return mStrategy.onComplete(response);
            }
            return false;
        }

        @Override
        public boolean onError(RpError error) {
            if (mStrategy != null) {
                mStrategy.dismissProgressDialog();
                mStrategy.onProcessCancel();
                return super.onError(error) || mStrategy.onError(error);
            }
            return false;
        }

        private void setStrategy(OrderSetViewStrategy strategy) {
            mStrategy = strategy;
        }
    }

    private void onProcessStart() {
        Delegate delegate = getDelegate();
        if (delegate != null)
            delegate.onStart(this);
    }

    private Delegate getDelegate() {
        ViewLifeCycleObserver parent = getParentLifeCycleObserver();
        if (parent instanceof Delegate)
            return (Delegate) parent;

        ViewController viewController = getController();
        if (viewController instanceof Delegate)
            return (Delegate) viewController;
        return null;
    }
}
