package com.riskpointer.sdk.app.ui.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.riskpointer.sdk.app.R;

/**
 * @author Shurygin Denis
 */
public class ForgetConfirmDialogFragment extends BaseDialogFragment {

    private AlertDialog mDialog;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext())
                .inflate(R.layout.hk_dialog_fragment_forget_confirm, null, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(view);
        builder.setTitle(R.string.hk_screen_settings_gdpr_dlg_forget_title);
        builder.setPositiveButton(R.string.hk_screen_settings_gdpr_btn_forget, this);
        builder.setNegativeButton(android.R.string.cancel, this);

        final CheckBox cbAgree = view.findViewById(R.id.hk_cbAgree);
        cbAgree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setPositiveButtonEnabled(isChecked);
            }
        });

        builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP)
                    onClick(dialog, DialogInterface.BUTTON_NEUTRAL);
                return false;
            }
        });
        mDialog = builder.create();
        mDialog.setCanceledOnTouchOutside(false);

        mDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                setPositiveButtonEnabled(cbAgree.isChecked());
            }
        });

        return mDialog;
    }

    private void setPositiveButtonEnabled(boolean enabled){
        Button button = mDialog.getButton(Dialog.BUTTON_POSITIVE);
        button.setEnabled(enabled);
        button.setAlpha(enabled ? 1.0f : 0.5f);
    }
}
