package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.riskpointer.sdk.app.utils.ApiLogTracker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Shurygin Denis on 2017-02-08.
 */

public class LogsListAdapter extends BaseRecyclerListAdapter<ApiLogTracker.Record, LogsListAdapter.ViewHolder> {

    private final SimpleDateFormat mDateFormat = new SimpleDateFormat(" (yyyy-MM-dd HH:mm:ss)", Locale.ENGLISH);
    private final List<ApiLogTracker.Record> mListCopy, mListItems;
    public LogsListAdapter(Context context, List<ApiLogTracker.Record> list, int layout) {
        super(context, list, layout);
        mListItems = list;
        mListCopy = new ArrayList<>();
        mListCopy.addAll(list);
    }

    @Override
    public ViewHolder onCreateItemViewHolder(ViewGroup parent, View view, int viewType) {
        return new ViewHolder(view);
    }

    @Override
    public void onBindItemViewHolder(ViewHolder holder, int position) {
        ApiLogTracker.Record item = getItem(position);
        holder.text2.setText(item.message);

        holder.text1.setText(
                item.level.toString() +
                mDateFormat.format(item.time)
        );
        switch (item.level) {
            case DEBUG:
                holder.text1.setTextColor(Color.BLACK);
                return ;
            case ERROR:
                holder.text1.setTextColor(Color.RED);
                return ;
            default:
                throw new RuntimeException("Unknown log level");
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView text1;
        public TextView text2;

        public ViewHolder(View itemView) {
            super(itemView);

            text1 = (TextView) itemView.findViewById(android.R.id.text1);
            text2 = (TextView) itemView.findViewById(android.R.id.text2);
        }
    }
    public void filter(String text) {
        mListItems.clear();
        if(text.isEmpty()){
            mListItems.addAll(mListCopy);
        } else{
            text = text.toLowerCase();
            for(ApiLogTracker.Record item: mListCopy){
                if(item.message.toLowerCase().contains(text) ){
                    mListItems.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }
}

