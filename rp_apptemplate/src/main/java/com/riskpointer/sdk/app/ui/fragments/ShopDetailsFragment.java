/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.fragments;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.utils.UriUtils;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.utils.FragmentUtils;
import com.riskpointer.sdk.app.utils.ImageUtils;
import com.riskpointer.sdk.app.R;

public class ShopDetailsFragment extends BaseFragment {
	
	private Shop mShop;
	private ImageView mLogoImage;

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_shop_details;
    }

	@Override
	public String getTitle(Context context) {
		return context.getString(R.string.shop_details_title);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mShop = FragmentUtils.getShop(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(getLayoutId(), container, false);

		TextView shopName = (TextView) rootView.findViewById(R.id.hk_txtShopName);
		if (shopName != null)
			shopName.setText(mShop.getName());

		TextView merchantName = (TextView) rootView.findViewById(R.id.hk_txtMerchantName);
		if (merchantName != null)
			merchantName.setText(mShop.getMerchant().getName());

		mLogoImage = (ImageView) rootView.findViewById(R.id.hk_imgShopLargeLogo);
		if (mLogoImage != null) {
			ImageUtils.loadImage(getActivity(), mLogoImage,
					rootView.findViewById(R.id.hk_progressContainer),
					mShop.getLogoLarge(),
					getDummyLogo());
		}

		View btnShowLocation = rootView.findViewById(R.id.hk_btnShowLocation);
		if (btnShowLocation != null)
			btnShowLocation.setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						String uriBegin = "geo:"
								+ mShop.getLatitude() + ","
								+ mShop.getLongitude();
						String uriString = uriBegin + "?q="
								+ mShop.getLatitude() + ","
								+ mShop.getLongitude();
						Uri uri = Uri.parse(uriString);
						Intent intent = new Intent(
								android.content.Intent.ACTION_VIEW, uri);
						try {
							//TODO android.content.ActivityNotFoundException
							startActivity(intent);
						} catch (ActivityNotFoundException e) {
							showMessage(null, 0, R.string.dlg_show_location_fail);
						}
					}
				});

		View btnShowQrCode = rootView.findViewById(R.id.hk_btnShowQrCode);
		if (btnShowQrCode != null)
			btnShowQrCode.setOnClickListener(
					new OnClickListener() {
						@Override
						public void onClick(View v) {
							showDialog(AppController.DIALOG_FRAGMENT_BARCODE, null, UriUtils.buildShopUri(UriUtils.URI_AUTHORITY, mShop.getId()));
						}
					});
		return rootView;
	}

	@Override
	public void onDetach() {
		if (mLogoImage != null)
			ImageUtils.stopLoadWithPicasso(getActivity(), mLogoImage);
		super.onDetach();
	}

	protected int getDummyLogo() {
    	return 0;
	}
}
