package com.riskpointer.sdk.app.ui.fragments.wizard;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.View;

import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.activities.SingleFragmentActivity;
import com.riskpointer.sdk.app.ui.fragments.BaseFragment;

import java.io.Serializable;

/**
 * Created by Shurygin Denis on 2015-10-23.
 */
public abstract class BaseWizardFragment<WizardObject extends Serializable> extends BaseFragment
        implements WizardController, View.OnKeyListener {
    public static final String ARG_FRAGMENTS = "BaseWizardFragment.ARG_FRAGMENTS";
    public static final String ARG_ARGUMENTS = "BaseWizardFragment.ARG_ARGUMENTS";

    private static final String ACTIVE_PAGE_INDEX = "BaseWizardFragment.ACTIVE_PAGE_INDEX";

    private static final String SAVED_WIZARD_OBJECT = "BaseWizardFragment.SAVED_WIZARD_OBJECT";

    protected WizardObject mObject;
    protected View mNextButton;
    protected View mPrevButton;

    protected Class[] mFragmentsArray;
    protected Parcelable[] mFragmentsArguments;
    protected int mPageIndex;
    private WizardPage<WizardObject> mActivePage;

    protected abstract WizardObject newWizardObject();
    protected abstract void onWizardComplete();

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_wizard;
    }

    @Override
    public String getTitle(Context context) {
        if (mActivePage instanceof SingleFragmentActivity.SingleFragment)
            return ((SingleFragmentActivity.SingleFragment) mActivePage).getTitle(context);
        return "";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mPageIndex = savedInstanceState.getInt(ACTIVE_PAGE_INDEX, 0);
            mObject = (WizardObject) savedInstanceState.getSerializable(SAVED_WIZARD_OBJECT);
        }
        else
            mPageIndex = 0;

        if (mObject == null)
            mObject = newWizardObject();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mNextButton = view.findViewById(R.id.hk_btnNext);
        mPrevButton = view.findViewById(R.id.hk_btnPrev);

        if (mNextButton!=null) {
            mNextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToNext();
                }
            });
        }

        if (mPrevButton != null) {
            mPrevButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToPrevious();
                }
            });
        }

        if (getChildFragmentManager().findFragmentById(R.id.hk_content) == null )
            onPageIndexChanged(mPageIndex);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ACTIVE_PAGE_INDEX, mPageIndex);
        outState.putSerializable(SAVED_WIZARD_OBJECT, mObject);
    }

    @Override
    public boolean onBackPressed() {
        return goToPrevious() || super.onBackPressed();
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent event) {
        if ((event.getAction() == KeyEvent.ACTION_DOWN)
                && (keyCode == KeyEvent.KEYCODE_ENTER)) {
            goToNext();
            return true;
        }
        return false;
    }

    protected Class[] getFragmentsArray() {
        if (mFragmentsArray == null) {
            loadFragmentsFromArguments();
        }
        return mFragmentsArray;
    }

    protected Parcelable[] getFragmentsArguments() {
        if (mFragmentsArray == null) {
            loadFragmentsFromArguments();
        }
        return mFragmentsArguments;
    }

    private void loadFragmentsFromArguments() {
        Bundle args = getArguments();
        if (args != null) {
            try { // may cause ClassCastException: java.lang.Object[] cannot be cast to java.lang.Class[] on some devices
                mFragmentsArray = (Class[]) args.getSerializable(ARG_FRAGMENTS);
            } catch (ClassCastException ex) { //Cast by 1 by 1
                Object[] objects = (Object[]) args.getSerializable(ARG_FRAGMENTS);
                mFragmentsArray = new Class[objects.length];
                for (int i = 0; i < objects.length; i++) {
                    mFragmentsArray[i] = (Class) objects[i];
                }
            }
            mFragmentsArguments = args.getParcelableArray(ARG_ARGUMENTS);
        }
    }

    @Override
    public boolean goToNext() {
        WizardPage<WizardObject> fragment = (WizardPage<WizardObject>) getChildFragmentManager().findFragmentById(R.id.hk_content);
        if (fragment != null) {
            mObject = fragment.getWizardObject();
            if (fragment.isValid()) {
                if (mPageIndex >= getFragmentsArray().length - 1)
                    onWizardComplete();
                else {
                    mPageIndex++;
                    onPageIndexChanged(mPageIndex);
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean goToPrevious() {
        WizardPage<WizardObject> fragment = (WizardPage<WizardObject>) getChildFragmentManager().findFragmentById(R.id.hk_content);
        if (fragment != null)
            mObject = fragment.getWizardObject();
        if (mPageIndex > 0) {
            mPageIndex--;
            onPageIndexChanged(mPageIndex);
            return true;
        }
        return false;
    }

    public boolean goTo(int position) {
        WizardPage<WizardObject> fragment = (WizardPage<WizardObject>) getChildFragmentManager().findFragmentById(R.id.hk_content);
        if (fragment != null)
            mObject = fragment.getWizardObject();
        mPageIndex = position;
        onPageIndexChanged(mPageIndex);
        return true;
    }

    protected void onPageIndexChanged(int index) {
        Fragment fragment;
        try {
            fragment = (Fragment) getFragmentsArray()[index].newInstance();
            if (getFragmentsArguments() != null && getFragmentsArguments().length > index)
                fragment.setArguments((Bundle) getFragmentsArguments()[index]);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        mActivePage = (WizardPage<WizardObject>) fragment;
        mActivePage.setWizardObject(mObject);

        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.hk_content, fragment,
                        getFragmentsArray()[index].getName())
                .commit();

        if (mNextButton != null)
            mNextButton.setVisibility(mActivePage instanceof WizardController
                    ? View.GONE : View.VISIBLE);

        if (mPrevButton != null)
            mPrevButton.setVisibility(mActivePage instanceof WizardController || index <= 0
                    ? View.GONE : View.VISIBLE);


        // On android 2.3.3 - 2.3.6 keyboard doesn't show after change fragment.
        // This is a quick fix for this.
        DialogFragment dialog = new DialogFragment();
        dialog.show(getChildFragmentManager(), "Quick Fix");
        dialog.dismiss();

        onTitleChanged();
    }
}
