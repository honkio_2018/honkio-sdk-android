package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.riskpointer.sdk.app.ui.adapters.binder.ListBinder;

import java.util.List;

/**
 * Created by Shurygin Denis on 2016-07-25.
 */
public class BaseBindableRecyclerListAdapter<Item, VH extends RecyclerView.ViewHolder> extends BaseRecyclerListAdapter<Item, VH> {

    private ListBinder<Item, VH> mBinder;

    public BaseBindableRecyclerListAdapter(Context context, List<Item> list, int layout, ListBinder<Item, VH> binder) {
        this(context, list, new int[]{layout}, binder);
    }

    public BaseBindableRecyclerListAdapter(Context context, List<Item> list, int[] layouts, ListBinder<Item, VH> binder) {
        super(context, list, layouts);
        mBinder = binder;
    }

    @Override
    public VH onCreateItemViewHolder(ViewGroup parent, View view, int viewType) {
        return mBinder.newHolder(view);
    }

    @Override
    public void onBindItemViewHolder(VH holder, int position) {
        mBinder.bind(mContext, holder, getItem(position));
    }
}
