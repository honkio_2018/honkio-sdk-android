package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.app.ui.adapters.binder.UserAccountItemBinder;

import java.util.List;

/**
 * @author Shurygin Denis
 */
public class UserAccountsAdapter extends BaseRecyclerListAdapter<UserAccount, UserAccountItemBinder.ViewHolder> {

    private UserAccountItemBinder mBinder;

    public UserAccountsAdapter(Context context, List<UserAccount> list, int layout) {
        this(context, list, layout, new UserAccountItemBinder());
    }

    public UserAccountsAdapter(Context context, List<UserAccount> list, int layout, UserAccountItemBinder binder) {
        super(context, list, layout);
        mBinder = binder;
    }

    @Override
    public UserAccountItemBinder.ViewHolder onCreateItemViewHolder(ViewGroup parent, View view,
                                                                   int viewType) {
        return mBinder.newHolder(view);
    }

    @Override
    public void onBindItemViewHolder(UserAccountItemBinder.ViewHolder holder, int position) {
        mBinder.bind(mContext, holder, getItem(position));
    }

}
