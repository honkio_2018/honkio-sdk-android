/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.textvalidator;

import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.TextView;

public abstract class BaseTextValidator implements OnFocusChangeListener {
	
	private String mErrorMessage;
	
	public BaseTextValidator(String errorMessage) {
		mErrorMessage = errorMessage;
	}
	
	public abstract boolean validate(String string);
	
	public boolean validate(View view) {
		return validate(view, mErrorMessage);
	}
	
	public boolean validate(View view, String errorMessage) {
		if(view instanceof TextView) {
			TextView textView = (TextView) view;
			boolean valid = validate(textView.getText().toString());
			if(valid)
				textView.setError(null);
			else
				textView.setError(errorMessage);
			return valid;
		}
		else
			return true;
	}
	
	@Override
	public void onFocusChange(View view, boolean hasFocus) {
		if(!hasFocus)
			validate(view);
	}
	
	public void setErrorMessage(String string) {
		mErrorMessage = string;
	}
	
}
