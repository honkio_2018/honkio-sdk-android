package com.riskpointer.sdk.app.ui.fragments;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.riskpointer.sdk.app.R;

/**
 * Created by Shurygin Denis on 2015-11-13.
 */
public class BaseAboutFragment extends BaseFragment {
    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_about;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        try {
            PackageInfo packageInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            ((TextView) view.findViewById(R.id.hk_txtVersion)).setText(packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
