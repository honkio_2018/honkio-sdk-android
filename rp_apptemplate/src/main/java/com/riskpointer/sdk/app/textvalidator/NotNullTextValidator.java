package com.riskpointer.sdk.app.textvalidator;

/**
 * Created by Shurygin Denis on 2016-10-11.
 */
public class NotNullTextValidator extends BaseTextValidator {

    public NotNullTextValidator(String errorMessage) {
        super(errorMessage);
    }

    @Override
    public boolean validate(String string) {
        return string != null;
    }

}
