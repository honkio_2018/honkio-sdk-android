package com.riskpointer.sdk.app.ui.fragments.list;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.callback.ErrorCallback;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.adapters.BaseArrayListAdapter;

import java.util.List;

/**
 * @author Shurygin Denis on 2015-04-10.
 */
@Deprecated // Use BaseRecycleLazyListFragment instead
public abstract class BaseListRefLazyFragment<ListType, CallbackType> extends BaseListRefFragment implements LoaderManager.LoaderCallbacks<BaseListRefLazyFragment.ListLoaderResult<ListType>> {
    private static volatile int LIST_LOADER_ID = 1;

    private static final String SAVED_LOADER_ID = "BaseListRefLazyFragment.SAVED_LOADER_ID";

    private ListLoader<ListType, CallbackType> mListLoader;
    protected BaseArrayListAdapter<ListType, ?> mAdapter;
    private List<ListType> mList;

    private View mFooter;

    private int mListLoaderId = -1;

// =================================================================================================
// Abstract methods
// =================================================================================================

    @Override
    public abstract ListLoader<ListType, CallbackType> onCreateLoader(int id, Bundle args);

    protected abstract BaseArrayListAdapter<ListType, ?> newAdapter(List<ListType> list);

// =================================================================================================
// Methods of Super Class
// =================================================================================================


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null)
            mListLoaderId = savedInstanceState.getInt(SAVED_LOADER_ID, -1);

        if (mListLoaderId == -1)
            mListLoaderId = LIST_LOADER_ID++;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        if (rootView != null) {
            ListView listView = (ListView) rootView.findViewById(android.R.id.list);

            mFooter = newFooterProgressView(inflater, listView);

            listView.addFooterView(mFooter);
        }

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mListLoader = (ListLoader<ListType, CallbackType>) getLoaderManager().initLoader(mListLoaderId, null, this);

        getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                if ((firstVisibleItem + visibleItemCount >= totalItemCount - 1) && !(mListLoader.isUpdating()) && mListLoader.isCanBeUpdated()) {
                    mListLoader.loadNext();
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVED_LOADER_ID, mListLoaderId);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mAdapter = null;
        setListAdapter(null);
    }

    @Override
    public void onRefresh() {
        setRefreshing(true);
        if (isAdded() && mListLoader != null && !mListLoader.isUpdating())
            reloadList(getActivity());
    }

    @Override
    public void onResume() {
        if (mListLoader.isReset() || (mListLoader.getList() == null && !mListLoader.isUpdating())) {
            mListLoader.forceLoad();
        }
        else if (mListLoader.getList() != null) {
            onLoadFinished(mListLoader, new ListLoaderResult<>(mListLoader.getList()));
        }

        super.onResume();
        setRefreshing(mListLoader != null && mListLoader.isUpdating());

    }

    @Override
    protected void reloadList(Context context) {
        getLoaderManager().destroyLoader(mListLoaderId);
        mListLoader = (ListLoader<ListType, CallbackType>) getLoaderManager().restartLoader(mListLoaderId, null, this);
        mListLoader.forceLoad();
    }

    protected void stopLoading() {
        getLoaderManager().destroyLoader(mListLoaderId);
        setRefreshing(false);
    }

    protected void notifyDataSetChanged() {
        if (mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }

// =================================================================================================
// Methods from Interfaces
// =================================================================================================

    @Override
    public void onLoaderReset(Loader<ListLoaderResult<ListType>> loader) {
        // Do nothing
    }

    @Override
    public void onLoadFinished(Loader<ListLoaderResult<ListType>> loader, ListLoaderResult<ListType> data) {
        if (data.list != null) {
            ListLoader<ListType, CallbackType> listLoader = (ListLoader<ListType, CallbackType>) loader;

            setRefreshing(false);
            updateList(data.list);

            if (mFooter != null && getView() != null) {
                if (!listLoader.isCanBeUpdated() && getListView().getFooterViewsCount() > 0)
                    mFooter.setVisibility(View.GONE);
                else if (listLoader.isCanBeUpdated() && getListView().getFooterViewsCount() == 0)
                    mFooter.setVisibility(View.VISIBLE);
            }
        }
        else if (data.error != null) {
            if (!onError(data.error))
                BroadcastHelper.sendActionUnhandledError(getContext(), "", data.error,
                        Thread.getAllStackTraces().get(Thread.currentThread()));
        }
    }

    @Override
    public boolean onError(RpError error) {
        if(getActivity() != null) {
            setRefreshing(false);
            if(getView() != null)
                setListShown(true);
            if(getActivity() instanceof ErrorCallback)
                if(((ErrorCallback) getActivity()).onError(error))
                    return true;
        }
        return false;
    }

// =================================================================================================
// Public methods and classes
// =================================================================================================

    public List<ListType> getList() {
        return mList;
    }

    public Loader<ListLoaderResult<ListType>> getListLoader() {
        return getLoaderManager().getLoader(mListLoaderId);
    }

// =================================================================================================
// Protected methods and classes
// =================================================================================================

    protected View newFooterProgressView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.hk_list_item_history_progress, container, false);
    }
// =================================================================================================
// Private methods and classes
// =================================================================================================

    private void updateList(List<ListType> list) {
        mList = list;
        if (getActivity() != null) {
            if (mAdapter == null) {
                mAdapter = newAdapter(list);

                setListAdapter(mAdapter);

                if (isAdded())
                    setListShown(true);
            }
            else {
                mAdapter.changeList(list);
                if (isAdded())
                    setListShown(true);
            }
            if(mAdapter.getCount() == 0 && mListLoader.isCanBeUpdated()) {
                if (isAdded())
                    setListShown(false);
                mListLoader.loadNext();
            }
        }
    }

    public static abstract class ListLoader<ListType, CallbackType> extends Loader<ListLoaderResult<ListType>> {
        public static final int QUERY_COUNT = 20;

        private Task mTask;
        protected List<ListType> mList;

        private boolean isUpdating = false;
        private boolean isCanBeUpdated = false;

        public ListLoader(Context context) {
            super(context);
        }

        public List<ListType> getList() {
            return mList;
        }

        protected abstract Task loadList(Context context, RequestCallback<CallbackType> callback, int count, int skip);
        protected abstract List<ListType> toList(Response<CallbackType> response);

        public void loadNext() {
            loadNext(getContext(), mListUpdater, mList.size());
        }

        @Override
        protected void onForceLoad() {
            mTask = loadList(getContext(), mListLoader, QUERY_COUNT, 0);
        }

        @Override
        protected void onReset() {
            super.onReset();
            if (mTask != null)
                mTask.abort();
        }

        private void loadNext(Context context, RequestCallback<CallbackType> callback, int skip) {
            isUpdating = true;
            mTask = loadList(context, callback, QUERY_COUNT, skip);
        }

        protected List<ListType> mergeLists(List<ListType> list1, List<ListType> list2) {
            list1.addAll(list2);
            return list1;
        }

        private RequestCallback<CallbackType> mListLoader = new RequestCallback<CallbackType>() {
            @Override
            public boolean onError(RpError error) {
                mTask = null;
                isUpdating = false;
                isCanBeUpdated = false;
                if (!isReset()) {
                    deliverResult(new ListLoaderResult<ListType>(error));
                }
                return true;
            }

            @Override
            public boolean onComplete(Response<CallbackType> response) {
                mTask = null;
                isUpdating = false;
                if (!isReset()) {
                    if (response.isStatusAccept()) {
                        mList = toList(response);
                        isCanBeUpdated = mList.size() >= QUERY_COUNT;
                        deliverResult(new ListLoaderResult<>(mList));
                        return true;
                    } else
                        return onError(response.getAnyError());
                }
                return true;
            }
        };

        private RequestCallback<CallbackType> mListUpdater = new RequestCallback<CallbackType>() {
            @Override
            public boolean onError(RpError error) {
                mTask = null;
                isUpdating = false;
                isCanBeUpdated = false;
                if (!isReset()) {
                    deliverResult(new ListLoaderResult<ListType>(error));
                }
                return true;
            }

            @Override
            public boolean onComplete(Response<CallbackType> response) {
                mTask = null;
                isUpdating = false;
                if (!isReset()) {
                    if (response.isStatusAccept()) {
                        List<ListType> list = toList(response);
                        isCanBeUpdated = list.size() >= QUERY_COUNT;
                        mList = mergeLists(mList, list);
                        deliverResult(new ListLoaderResult<>(mList));
                        return true;
                    } else
                        return onError(response.getAnyError());
                }
                return true;
            }
        };

        public boolean isUpdating() {
            return isUpdating;
        }

        public boolean isCanBeUpdated() {
            return isCanBeUpdated;
        }
    }

    public static class ListLoaderResult<ListType> {

        public final List<ListType> list;
        public final RpError error;

        public ListLoaderResult(List<ListType> list) {
            this.list = list;
            error = null;
        }

        public ListLoaderResult(RpError error) {
            list = null;
            this.error = error;
        }
    }
}
