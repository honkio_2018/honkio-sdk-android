package com.riskpointer.sdk.app.ui.adapters.binder;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.riskpointer.sdk.api.model.entity.BookedProduct;
import com.riskpointer.sdk.api.utils.Currency;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.utils.FormatUtils;

/**
 * Created by Shurygin Denis on 2016-11-23.
 */

public class BookedProductListBinder implements ListBinder<BookedProduct, BookedProductListBinder.ViewHolder> {

    private ShopProductListBinder mProductBinder;

    public BookedProductListBinder() {
        mProductBinder = new ShopProductListBinder();
    }

    @Override
    public void bind(Context context, BookedProductListBinder.ViewHolder viewHolder, BookedProduct product) {
        mProductBinder.bind(context, viewHolder, product.getProduct());

        if (viewHolder.amount != null) {
            viewHolder.amount.setText(FormatUtils.format(product.getPrice()));
        }

        if (product.getCount() > 1) {
            viewHolder.count.setText("" + product.getCount() + "x");
            viewHolder.count.setVisibility(View.VISIBLE);

            if (viewHolder.total != null) {
                viewHolder.total.setText(FormatUtils.format(product.getProduct().getAmount()) + " "
                        + Currency.getSymbol(product.getProduct().getCurrency()));
                viewHolder.total.setVisibility(View.VISIBLE);
            }

        }
        else {
            viewHolder.count.setVisibility(View.GONE);

            if (viewHolder.total != null)
                viewHolder.total.setVisibility(View.GONE);
        }
    }

    @Override
    public BookedProductListBinder.ViewHolder newHolder(View view) {
        return new BookedProductListBinder.ViewHolder(view);
    }

    public static class ViewHolder extends ShopProductListBinder.ViewHolder {
        public final TextView count;
        public final TextView total;

        public ViewHolder(View view) {
            super(view);
            count = (TextView) view.findViewById(R.id.hk_txtCount);
            total = (TextView) view.findViewById(R.id.hk_txtTotal);
        }
    }
}