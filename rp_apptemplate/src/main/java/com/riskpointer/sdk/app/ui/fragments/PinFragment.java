package com.riskpointer.sdk.app.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.riskpointer.sdk.api.BaseHonkioApi;
import com.riskpointer.sdk.api.model.process.PinReason;
import com.riskpointer.sdk.api.web.Connection;
import com.riskpointer.sdk.app.ui.view.PinDotView;
import com.riskpointer.sdk.app.R;

/**
 * @author Shurygin Denis on 2015-04-06.
 */
public class PinFragment extends BaseFragment {

    public static final String EXTRA_REASON = "PinFragment.EXTRA_REASON";
    public static final String EXTRA_PIN = "PinFragment.EXTRA_PIN";
    public static final String EXTRA_DELAYS = "PinFragment.EXTRA_DELAYS";

    private static final int[] KEYS =  {R.id.hk_pin_btn0, R.id.hk_pin_btn1,
            R.id.hk_pin_btn2, R.id.hk_pin_btn3,
            R.id.hk_pin_btn4, R.id.hk_pin_btn5,
            R.id.hk_pin_btn6, R.id.hk_pin_btn7,
            R.id.hk_pin_btn8, R.id.hk_pin_btn9};

    private static final int[] DOTS =  {R.id.hk_pinDot1, R.id.hk_pinDot2,
            R.id.hk_pinDot3, R.id.hk_pinDot4};
    private static final int PIN_SIZE = 4;

    private StringBuffer mPinKeys;
    private PinDotView[] mDots;

    private long mLastKeyPressingTime;
    private long[] mDelays;

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_activity_pin;
    }

    @Override
    public String getTitle(Context context) {
        return context.getString(R.string.screan_pin_code_title);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent data = new Intent();
        data.putExtra(EXTRA_REASON, getArguments().getInt(EXTRA_REASON));
        getActivity().setResult(Activity.RESULT_CANCELED, data);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        mDelays = new long[PIN_SIZE - 1];

        for(int i = 0; i < KEYS.length; i++) {
            View button = rootView.findViewById(KEYS[i]);
            if(button != null)
                button.setOnClickListener(mButtonClickListener);
        }
        rootView.findViewById(R.id.hk_btnDelete).setOnClickListener(mButtonClickListener);
        rootView.findViewById(R.id.hk_btnClear).setOnClickListener(mButtonClickListener);
        TextView txtMessage = (TextView) rootView.findViewById(R.id.hk_txtMessage);
        mDots = new PinDotView[DOTS.length];
        for(int i = 0; i < DOTS.length; i++)
            mDots[i] = (PinDotView) rootView.findViewById(DOTS[i]);

        mPinKeys = new StringBuffer(PIN_SIZE);

        String reasonMessage = getMessage();
        if(reasonMessage != null) {
            txtMessage.setVisibility(View.VISIBLE);
            txtMessage.setText(reasonMessage);
        }
        else txtMessage.setVisibility(View.GONE);

        updateDots();

        return rootView;
    }

    @Override
    public boolean onBackPressed() {
        Intent data = new Intent();
        data.putExtra(EXTRA_REASON, getArguments().getInt(EXTRA_REASON));
        getActivity().setResult(Activity.RESULT_CANCELED, data);
        onPinEnterComplete();
        return true;
    }

    protected String getMessage() {
        switch (getArguments().getInt(EXTRA_REASON, 0)) {
            case PinReason.NEW:
                return getString(R.string.pin_reason_new);
            case PinReason.REENTER:
                return getString(R.string.pin_reason_reenter)
                        .replace("[user]", BaseHonkioApi.getApiPreferences().getString(Connection.LOGIN, ""));
            case PinReason.TIMEOUT:
                return getString(R.string.pin_reason_time_out);
            case PinReason.PURCHASE:
                return getString(R.string.pin_reason_purchare);
        }
        return null;
    }

    protected void onPinEnterComplete() {
        getActivity().finish();
    }

    private void onKeyPressed(String key) {
        if(mPinKeys.length() + key.length() <= PIN_SIZE) {
            if(mPinKeys.length() > 0) {
                long delay = System.currentTimeMillis() - mLastKeyPressingTime;
                mDelays[mPinKeys.length() - 1] = delay;
            }
            mLastKeyPressingTime = System.currentTimeMillis();
            mPinKeys.append(key);
        }
        if(mPinKeys.length() >= PIN_SIZE){
            Intent data = new Intent();
            data.putExtra(EXTRA_REASON, getArguments().getInt(EXTRA_REASON));
            data.putExtra(EXTRA_PIN, mPinKeys.toString());
            data.putExtra(EXTRA_DELAYS, mDelays);
            getActivity().setResult(Activity.RESULT_OK, data);
            onPinEnterComplete();
        }
        updateDots();
    }

    private void deleteLastKey() {
        mLastKeyPressingTime = System.currentTimeMillis();
        if(mPinKeys.length() > 0) {
            mPinKeys.deleteCharAt(mPinKeys.length() - 1);
        }
        updateDots();
    }

    private void clearPin() {
        mLastKeyPressingTime = System.currentTimeMillis();
        mPinKeys.delete(0, mPinKeys.length());
        updateDots();
    }

    private void updateDots() {
        for(int i = 0; i < mDots.length; i++)
            if(i >= mPinKeys.length())
                mDots[i].setState(PinDotView.STATE_DISABLE);
            else
                mDots[i].setState(PinDotView.STATE_DONE);
    }

    private View.OnClickListener mButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int id = view.getId();
            boolean keyFound = false;
            for(int i = 0; i < KEYS.length && !keyFound; i++) {
                if(KEYS[i] == id) {
                    onKeyPressed(Integer.toString(i));
                    keyFound = true;
                }
            }
            if(!keyFound && id == R.id.hk_btnDelete)
                deleteLastKey();
            else if(!keyFound && id == R.id.hk_btnClear)
                clearPin();
        }
    };
}
