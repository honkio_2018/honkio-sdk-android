package com.riskpointer.sdk.app.ui.fragments.list;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Applicant;
import com.riskpointer.sdk.api.model.entity.ApplicantsList;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.message.filter.ApplicantFilter;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.utils.FragmentUtils;

import java.util.List;

/**
 * @author Shurygin Denis
 */

public abstract class BaseApplicantsListFragment extends BaseRecycleLazyListFragment<Applicant> {

    public static final String ARG_ORDER_ID = "BaseApplicantsListFragment.ARG_ORDER_ID";

    private ApplicantFilter mFilter = new ApplicantFilter();

    @Override
    public ListLoader<Applicant, ApplicantsList> onCreateLoader(int id, Bundle args) {
        return new ApplicantsListLoader(getContext(), getFilter(),
                FragmentUtils.getShopIdentity(this, null));
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager(Context context) {
        return new LinearLayoutManager(context);
    }

    protected ApplicantFilter getFilter() {
        mFilter.setOrderId(getOrderId());
        return mFilter;
    }

    protected String getOrderId() {
        Bundle args = getArguments();
        if (args != null && args.containsKey(ARG_ORDER_ID))
            return args.getString(ARG_ORDER_ID);

        throw new IllegalStateException("Order ID not defined." +
                " Please set fragment argument BaseApplicantsListFragment.ARG_ORDER_ID");
    }

    protected static class ApplicantsListLoader extends ListLoader<Applicant, ApplicantsList> {
        private ApplicantFilter mFilter;
        private Identity mShopIdentity;

        protected ApplicantsListLoader(Context context, ApplicantFilter filter,
                                       Identity shopIdentity) {
            super(context);
            mFilter = filter;
            mShopIdentity = shopIdentity;
        }

        @Override
        protected List<Applicant> toList(Response<ApplicantsList> response) {
            return response.getResult().getList();
        }

        @Override
        protected Task loadList(Context context, RequestCallback<ApplicantsList> callback, int count, int skip) {
            mFilter.setCount(count);
            mFilter.setSkip(skip);
            return HonkioApi.orderGetApplicants(mFilter, mShopIdentity, 0, callback);
        }
    }
}
