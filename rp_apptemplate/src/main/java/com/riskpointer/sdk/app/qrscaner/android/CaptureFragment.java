package com.riskpointer.sdk.app.qrscaner.android;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.qrscaner.android.camera.CameraManager;
import com.riskpointer.sdk.app.ui.fragments.BaseFragment;

import java.util.Collection;

public class CaptureFragment extends BaseFragment implements SurfaceHolder.Callback {

	public interface DecodeListener {
		void onDecode(Result rawResult, Bitmap barcode, float scaleFactor);
		void onCameraInitError();
	}

	private static final String TAG = CaptureFragment.class.getSimpleName();
	
	private View mRootView;
	private DecodeListener mDecodeListener;

	private CameraManager cameraManager;
	private CaptureFragmentHandler handler;
	private ViewfinderView viewfinderView;
	private boolean hasSurface;
	private Collection<BarcodeFormat> decodeFormats;
	private String characterSet;
	private BeepManager beepManager;
	private AmbientLightManager ambientLightManager;

	private boolean isScannerStarted = true;

	ViewfinderView getViewfinderView() {
		return viewfinderView;
	}

	public Handler getHandler() {
		return handler;
	}

	CameraManager getCameraManager() {
		return cameraManager;
	}

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_qr_scaner;
    }

	@Override
	public String getTitle(Context context) {
		return context.getString(R.string.screen_title_capture);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = super.onCreateView(inflater, container, savedInstanceState);
		hasSurface = false;
		return mRootView;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		beepManager = new BeepManager(activity);
		ambientLightManager = new AmbientLightManager(activity);
	}

	@Override
	public void onResume() {
		super.onResume();

		// CameraManager must be initialized here, not in onCreate(). This is
		// necessary because we don't
		// want to open the camera driver and measure the screen size if we're
		// going to show the help on
		// first launch. That led to bugs where the scanning rectangle was the
		// wrong size and partially
		// off screen.
		if (isScannerStarted)
			restartCamera();
	}

	@Override
	public void onPause() {
		if (handler != null) {
			handler.quitSynchronously();
			handler = null;
		}
		ambientLightManager.stop();
		cameraManager.closeDriver();
		if (!hasSurface) {
			SurfaceView surfaceView = (SurfaceView) mRootView.findViewById(R.id.hk_preview_view);
			SurfaceHolder surfaceHolder = surfaceView.getHolder();
			surfaceHolder.removeCallback(this);
		}
		super.onPause();
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if (holder == null) {
			Log.e(TAG,
					"*** WARNING *** surfaceCreated() gave us a null surface!");
		}
		if (!hasSurface) {
			hasSurface = true;
			initCamera(holder);
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		hasSurface = false;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

	}

	/**
	 * A valid barcode has been found, so give an indication of success and show
	 * the results.
	 * 
	 * @param rawResult
	 *            The contents of the barcode.
	 * @param scaleFactor
	 *            amount by which thumbnail was scaled
	 * @param barcode
	 *            A greyscale bitmap of the camera data which was decoded.
	 */
	public void handleDecode(Result rawResult, Bitmap barcode, float scaleFactor) {
		if(mDecodeListener != null)
			mDecodeListener.onDecode(rawResult, barcode, scaleFactor);
		beepManager.playBeepSoundAndVibrate();
	}

	public void restartPreviewAfterDelay(long delayMS) {
		if (handler != null) {
			handler.sendEmptyMessageDelayed(R.id.restart_preview, delayMS);
		}
		resetStatusView();
	}

	public void setDecodeListener(DecodeListener listener) {
		mDecodeListener = listener;
	}

	protected void restartCamera() {
		cameraManager = new CameraManager(getActivity().getApplication());

		viewfinderView = (ViewfinderView) mRootView.findViewById(R.id.hk_viewfinder_view);

		if (viewfinderView != null)
			viewfinderView.setCameraManager(cameraManager);

		handler = null;

		resetStatusView();

		SurfaceView surfaceView = (SurfaceView) mRootView.findViewById(R.id.hk_preview_view);
		SurfaceHolder surfaceHolder = surfaceView.getHolder();
		if (hasSurface) {
			// The activity was paused but not stopped, so the surface still
			// exists. Therefore
			// surfaceCreated() won't be called, so init the camera here.
			initCamera(surfaceHolder);
		} else {
			// Install the callback and wait for surfaceCreated() to init the
			// camera.
			surfaceHolder.addCallback(this);
			surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}

		beepManager.updatePrefs();
		ambientLightManager.start(cameraManager);

		decodeFormats = null;
		characterSet = null;
	}

	private void initCamera(SurfaceHolder surfaceHolder) {
		if (surfaceHolder == null) {
			throw new IllegalStateException("No SurfaceHolder provided");
		}
		if (cameraManager.isOpen()) {
			Log.w(TAG,
					"initCamera() while already open -- late SurfaceView callback?");
			return;
		}
		try {
			cameraManager.openDriver(surfaceHolder);
			// Creating the handler starts the preview, which can also throw a
			// RuntimeException.
			if (handler == null) {
				handler = new CaptureFragmentHandler(this, decodeFormats,
						null, characterSet, cameraManager);
			}

			isScannerStarted = true;
		} catch (Exception ioe) {
			Log.w(TAG, ioe);
			if (mDecodeListener != null)
				mDecodeListener.onCameraInitError();

			isScannerStarted = false;
		}
	}

	private void resetStatusView() {
		if (viewfinderView != null)
			viewfinderView.setVisibility(View.VISIBLE);
	}

	public void drawViewfinder() {
		if (viewfinderView != null)
			viewfinderView.drawViewfinder();
	}
}