package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.riskpointer.sdk.api.model.entity.HkStructure;

import java.util.List;

/**
 * @author Shurygin Denis
 */
public class StructureEnumAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;
    private final int mLayoutId;
    private List<HkStructure.EnumProperty.Value> mList;

    public StructureEnumAdapter(Context context, List<HkStructure.EnumProperty.Value> items) {
        this(context, android.R.layout.simple_list_item_1, items);
    }

    public StructureEnumAdapter(Context context, int layoutId, List<HkStructure.EnumProperty.Value> items) {
        mInflater = LayoutInflater.from(context);
        mLayoutId = layoutId;
        mList = items;
    }

    @Override
    public int getCount() {
        return mList != null ? mList.size() : 0;
    }

    @Override
    public HkStructure.EnumProperty.Value getItem(int position) {
        return (mList != null && mList.size() > position && position >= 0)
                ? mList.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(mLayoutId, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        HkStructure.EnumProperty.Value item = getItem(position);
        if (item != null) {
            viewHolder.text1.setText(item.getName());
        }

        return convertView;
    }

    public int getPosition(String value) {
        int pos = 0;
        for (HkStructure.EnumProperty.Value item : mList) {
            if (item.getValue() != null && item.getValue().equals(value)) {
                return pos;
            } else {
                pos++;
            }
        }
        return 0;
    }

    private static class ViewHolder {
        private TextView text1;

        public ViewHolder(View view) {
            text1 = view.findViewById(android.R.id.text1);
        }
    }
}
