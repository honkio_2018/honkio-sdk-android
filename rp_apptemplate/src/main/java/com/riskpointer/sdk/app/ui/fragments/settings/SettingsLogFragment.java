package com.riskpointer.sdk.app.ui.fragments.settings;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.TextView;

import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.fragments.BaseFragment;
import com.riskpointer.sdk.app.utils.ApiLogTracker;

import java.util.Formatter;

/**
 * Created by Shurygin Denis on 2017-02-08.
 */

public class SettingsLogFragment extends BaseFragment {

    public static final String ARG_LOG_RECORD = "SettingsLogFragment.ARG_LOG_RECORD";

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_log;
    }

    @Override
    public String getTitle(Context context) {
        ApiLogTracker.Record record = (ApiLogTracker.Record) getArguments()
                .getSerializable(ARG_LOG_RECORD);

        Formatter formatter = DateUtils.formatDateRange(context,
                new Formatter(new StringBuilder()),
                record.time.getTime(), record.time.getTime(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE |
                        DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_SHOW_TIME);

        return record.level.toString() + " (" + formatter.toString() + ")";
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ApiLogTracker.Record record = (ApiLogTracker.Record) getArguments()
                .getSerializable(ARG_LOG_RECORD);

        ((TextView) view.findViewById(R.id.hk_txtMessage))
                .setText(record.message);
    }
}
