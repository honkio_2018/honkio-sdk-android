/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.textvalidator;

import java.util.regex.Pattern;

public class EmailTextValidator extends BaseTextValidator {
	
	private static final Pattern mPattern = Pattern.compile(".+@.+\\.[a-z]+");

	public EmailTextValidator(String errorMessage) {
		super(errorMessage);
	}

	@Override
	public boolean validate(String string) {
		string = string.trim();
		return string.length() <= 0 || mPattern.matcher(string).matches();
	}

}
