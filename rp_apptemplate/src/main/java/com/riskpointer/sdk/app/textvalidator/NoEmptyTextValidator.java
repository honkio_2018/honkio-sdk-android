/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.textvalidator;

public class NoEmptyTextValidator extends BaseTextValidator {

	public NoEmptyTextValidator(String errorMessage) {
		super(errorMessage);
	}

	@Override
	public boolean validate(String string) {
		return string != null && !"".equals(string.trim());
	}

}
