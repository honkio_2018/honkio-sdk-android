/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.fragments.settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.User;
import com.riskpointer.sdk.api.model.process.PinReason;
import com.riskpointer.sdk.api.web.callback.ErrorCallback;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.textvalidator.PasswordTextValidator;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;
import com.riskpointer.sdk.app.ui.activities.SingleFragmentActivity;
import com.riskpointer.sdk.app.ui.fragments.BaseFragment;
import com.riskpointer.sdk.app.ui.fragments.PinFragment;
import com.riskpointer.sdk.app.utils.AppPreferences;
import com.riskpointer.sdk.app.utils.DoneClickListener;

public class SettingsChangePinFragment extends BaseFragment implements SingleFragmentActivity.SingleFragment {

    private static final int REQUEST_NEW_PIN = 1;

    private PasswordTextValidator mPasswordTextValidator;

    private EditText mPasswordEdit;

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_dialog_fragment_change_pin;
    }

    @Override
    public String getTitle(Context context) {
        return context.getString(R.string.change_pin_title);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof ErrorCallback))
            throw new ClassCastException("Activity should implement com.riskpointer.api.callbacks.ErrorCallback.");
        mPasswordTextValidator = new PasswordTextValidator(activity.getResources());
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPasswordEdit = (EditText) view.findViewById(R.id.hk_edPassword);
        mPasswordEdit.setOnKeyListener(new DoneClickListener() {
            @Override
            public void onDoneClick(View view) {
                onPositiveButtonClick();
            }
        });

        view.findViewById(R.id.hk_btnNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPositiveButtonClick();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_NEW_PIN && resultCode == Activity.RESULT_OK && data != null) {
            String newPin = data.getStringExtra(PinFragment.EXTRA_PIN);

            HonkioApi.changePin(newPin);

            SharedPreferences appPrefs = getActivity()
                    .getSharedPreferences(AppPreferences.PREFERENCES_FILE, Activity.MODE_PRIVATE);
            if (appPrefs.getBoolean(AppPreferences.PREF_SAVE_PIN, AppPreferences.PREF_SAVE_PIN_DEFAULT)) {
                HonkioApi.getApiPreferences().edit()
                        .putString(AppPreferences.PREF_PIN, newPin).apply();
            }

            showMessage(BaseActivity.DLG_TAG_SUCCESS, getString(R.string.change_pin_dlg_accept_title),
                    getString(R.string.change_pin_dlg_accept_message));

            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void onPositiveButtonClick() {
        if (mPasswordTextValidator.validate(mPasswordEdit)) {
            showProgressDialog();
            HonkioApi.userCheckPassword(mPasswordEdit.getText().toString(), 0, mCheckPassCallback);
        }
    }

    private RequestCallback<User> mCheckPassCallback = new RequestCallback<User>() {
        @Override
        public boolean onComplete(Response<User> response) {
            dismissProgressDialog();
            if (response.isStatusAccept()) {
                BaseActivity.startPinActivity(SettingsChangePinFragment.this, PinReason.NEW, REQUEST_NEW_PIN);
                return true;
            }
            return onError(response.getAnyError());
        }

        @Override
        public boolean onError(RpError error) {
            dismissProgressDialog();
            if (error.code == RpError.Common.INVALID_USER_LOGIN) {
                showMessage(null, getString(R.string.change_pin_title),
                        getString(R.string.change_pin_wrong_password));
                return true;
            }
            return false;
        }
    };
}
