package com.riskpointer.sdk.app.utils;

/**
 * Created by Shurygin Denis on 2016-05-30.
 */
public class RequestCodes {

    private static int sCurrentCode = 300;

    public static int newCode() { return sCurrentCode++; }

    public static final int LOGIN = newCode();
    public static final int TOU_UPDATE = newCode();

    public static final int ORDER_SET_FE_ADD_ACCOUNT = newCode();
    public static final int ORDER_SET_FE_PIN_REQUEST = newCode();

    public static final int REQUEST_PICK_IMAGE = newCode();
    public static final int REQUEST_PICK_CAMERA = newCode();
    public static final int REQUEST_PICK_VIDEO_FILE = newCode();

    public static final int REQUEST_PERMISSION = newCode();

    public static final int REQUEST_PICK_LOCATION = newCode();

}
