package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.riskpointer.sdk.api.model.entity.InventoryProduct;
import com.riskpointer.sdk.app.ui.adapters.binder.BoughtProductListBinder;

import java.util.List;

/**
 * Created by Shurygin Denis on 2016-11-03.
 */

public class InventoryProductsListAdapter extends BaseRecyclerListAdapter<InventoryProduct, BoughtProductListBinder.ViewHolder> {

    private BoughtProductListBinder mBinder;

    public InventoryProductsListAdapter(Context context, List<InventoryProduct> list, int layout) {
        this(context, list, layout, new BoughtProductListBinder());
    }

    public InventoryProductsListAdapter(Context context, List<InventoryProduct> list, int layout,
                                        BoughtProductListBinder binder) {
        super(context, list, layout);
        mBinder = binder;
    }

    @Override
    public BoughtProductListBinder.ViewHolder onCreateItemViewHolder(ViewGroup parent, View view,
                                                                     int viewType) {
        return mBinder.newHolder(view);
    }

    @Override
    public void onBindItemViewHolder(BoughtProductListBinder.ViewHolder holder, int position) {
        mBinder.bind(mContext, holder, getItem(position));
    }
}
