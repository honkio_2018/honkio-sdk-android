package com.riskpointer.sdk.app.ui.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.view.Window;
import android.view.WindowManager;

import com.google.zxing.Result;
import com.riskpointer.sdk.api.support.BroadcastManager;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.qrscaner.android.CaptureFragment;
import com.riskpointer.sdk.app.qrscaner.android.InactivityTimer;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.utils.RequestCodes;

/**
 * @author Shurygin Denis on 2015-04-07.
 */
public class QrScannerFragment extends CaptureFragment implements CaptureFragment.DecodeListener,
        BaseDialogFragment.OnDismissListener {

    public static final String EXTRA_CODE_TEXT = "QrScannerFragment.EXTRA_CODE_TEXT";

    public static final String ACTION_RESTART_PREVIEW = "QrScannerFragment.ACTION_RESTART_PREVIEW";

    public static final String DLG_TAG_RESTART_PREVIEW = "QrScannerFragment.DLG_TAG_RESTART_PREVIEW";
    public static final String DLG_FRAMEWORK_BUG = "QrScannerFragment.DLG_FRAMEWORK_BUG";
    public static final String DLG_OPEN_PERMISSION_SETTING = "QrScannerFragment.DLG_TAG_OPEN_PERMISSION_SETTING";

    private InactivityTimer inactivityTimer;
    private BroadcastReceiver mRestartActionReceiver;

    @Override
    public String getTitle(Context context) {
        return null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setDecodeListener(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        Window window = activity.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        inactivityTimer = new InactivityTimer(activity);

        mRestartActionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                restartPreviewAfterDelay(0);
            }
        };

        BroadcastManager.getInstance(activity).registerReceiver(mRestartActionReceiver, new IntentFilter(ACTION_RESTART_PREVIEW));
    }

    @Override
    public void onResume() {
        inactivityTimer.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        inactivityTimer.onPause();
        super.onPause();
    }

    @Override
    public void onDetach() {
        inactivityTimer.shutdown();
        BroadcastManager.getInstance(getActivity()).unregisterReceiver(mRestartActionReceiver);
        super.onDetach();
    }

    @Override
    public boolean onDismissDialog(BaseDialogFragment dialogFragment) {
        if (DLG_TAG_RESTART_PREVIEW.equals(dialogFragment.getTag())) {
            restartPreviewAfterDelay(0);
        }
        else if (DLG_FRAMEWORK_BUG.equals(dialogFragment.getTag()))
            stopScanner();
            return false;
    }

    @Override
    public boolean onClickDialog(BaseDialogFragment dialogFragment, int which) {
        if (DLG_OPEN_PERMISSION_SETTING.equals(dialogFragment.getTag())) {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + getContext().getPackageName()));
                startActivity(appSettingsIntent);
            }
            else
                stopScanner();
            return true;
        }
        else
            return super.onClickDialog(dialogFragment, which);
    }

    /**
     * A valid barcode has been found, so give an indication of success and show
     * the results.
     *
     * @param rawResult
     *            The contents of the barcode.
     * @param scaleFactor
     *            amount by which thumbnail was scaled
     * @param barcode
     *            A greyscale bitmap of the camera data which was decoded.
     */
    @Override
    public void onDecode(Result rawResult, Bitmap barcode, float scaleFactor) {
        if (isAdded()) {
            if (isModal()) {
                Intent intent = new Intent();
                intent.putExtra(EXTRA_CODE_TEXT, rawResult.getText());
                getActivity().setResult(Activity.RESULT_OK, intent);
                getActivity().finish();
            } else {
                if (!handleUri(rawResult.getText())) {
                    showMessage(DLG_TAG_RESTART_PREVIEW, R.string.dlg_invalid_qrcode_title, R.string.dlg_invalid_qrcode_message);
                }
            }
        }
    }

    public boolean isModal() {
        Activity activity = getActivity();
        return activity != null && activity.getCallingActivity() != null;
    }

    protected boolean handleUri(String uriString) {
        Activity activity = getActivity();
        if (activity != null && activity instanceof BaseActivity)
            return ((BaseActivity) activity).handleUri(uriString);
        return false;
    }

    @Override
    public void onCameraInitError(){
        if (checkRuntimeCameraPermissions())
            showMessage(DLG_FRAMEWORK_BUG, 0, R.string.msg_camera_framework_bug);
        else {
            requestPermissions(new String[]{Manifest.permission.CAMERA},
                    RequestCodes.REQUEST_PERMISSION);
        }
    }

    @Override
    protected void onPermissionDenied(String permission, int requestCode) {
        if (Manifest.permission.CAMERA.equals(permission))
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CAMERA)) {
                stopScanner();
            } else {
                showDialog(
                        AppController.DIALOG_FRAGMENT_MESSAGE,
                        DLG_OPEN_PERMISSION_SETTING,
                        getString(R.string.dlg_request_permission_title),
                        getString(R.string.dlg_request_permission_camera_in_settings),
                        getString(R.string.dlg_request_permission_btn_settings),
                        getString(android.R.string.cancel));
            }
    }

    @Override
    protected void onPermissionGranted(String permission, int requestCode) {
        if (Manifest.permission.CAMERA.equals(permission)) {
            restartCamera();
        }
    }

    protected void stopScanner() {
        finish();
    }

    private boolean checkRuntimeCameraPermissions() {
        return checkRuntimePermission(Manifest.permission.CAMERA);
    }
}
