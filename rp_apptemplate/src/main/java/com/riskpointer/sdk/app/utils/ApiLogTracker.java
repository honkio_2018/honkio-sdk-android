package com.riskpointer.sdk.app.utils;

import com.riskpointer.sdk.api.utils.ApiLog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Shurygin Denis on 2017-02-08.
 */

public class ApiLogTracker implements ApiLog.LogListener {

    public static class Record implements Serializable {

        public final ApiLog.LogLevel level;
        public final String message;
        public final Date time;

        public Record(ApiLog.LogLevel level, String message, Date time) {
            this.level = level;
            this.message = message;
            this.time = time;
        }
    }

    private static ApiLogTracker sInstance;

    public static ApiLogTracker getInstance() {
        if (sInstance == null) {
            synchronized (ApiLogTracker.class) {
                if (sInstance == null) {
                    sInstance = new ApiLogTracker();
                }
            }
        }
        return sInstance;
    }

    public final List<Record> records = new ArrayList<>();

    @Override
    public void onLog(ApiLog.LogLevel level, String message) {
        synchronized (records) {
            records.add(new Record(level, message, new Date()));
        }
    }
}
