package com.riskpointer.sdk.app.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.riskpointer.sdk.app.R;

/**
 * @author Shurygin Denis on 2014-12-30.
 */
public class ShopDetailsUnsupportedFragment extends BaseFragment {
    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_shop_details_unsupported;
    }

    @Override
    public String getTitle(Context context) {
        return context.getString(R.string.shop_details_title);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        return view;
    }
}
