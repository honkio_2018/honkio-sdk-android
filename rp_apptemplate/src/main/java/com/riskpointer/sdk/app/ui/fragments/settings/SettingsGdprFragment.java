package com.riskpointer.sdk.app.ui.fragments.settings;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.ui.dialogs.ForgetConfirmDialogFragment;
import com.riskpointer.sdk.app.ui.dialogs.InputDialogFragment;
import com.riskpointer.sdk.app.ui.fragments.BaseFragment;

import static com.riskpointer.sdk.app.ui.activities.BaseActivity.DLG_TAG_LOGOUT;

/**
 * @author Shurygin Denis
 */
public class SettingsGdprFragment extends BaseFragment {

    private static final String DAG_FORGET_ME = "SettingsGdprFragment.DAG_FORGET_ME";
    private static final String DLG_REPORT = "SettingsGdprFragment.DLG_REPORT";

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_gdpr;
    }

    @Override
    public String getTitle(Context context) {
        return context.getString(R.string.hk_screen_settings_gdpr_title);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findViewById(R.id.hk_btnForget).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(new ForgetConfirmDialogFragment(), DAG_FORGET_ME);
            }
        });

        findViewById(R.id.hk_btnReport).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputDialogFragment dialog = InputDialogFragment.build(
                        HonkioApi.getActiveUser().getLogin(), 0,
                        R.string.hk_screen_settings_gdpr_msg_report_title,
                        R.string.hk_screen_settings_gdpr_msg_report_message,
                        EditorInfo.TYPE_TEXT_VARIATION_EMAIL_ADDRESS, true, true);

                showDialog(dialog, DLG_REPORT);
            }
        });
    }

    @Override
    public boolean onClickDialog(BaseDialogFragment dialogFragment, int which) {
        if (DAG_FORGET_ME.equals(dialogFragment.getTag())) {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                doForgetMe();
            }
            return true;
        }
        else if (DLG_REPORT.equals(dialogFragment.getTag())) {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                doReport(((InputDialogFragment) dialogFragment).getText());
            }
            return true;
        }
        return super.onClickDialog(dialogFragment, which);
    }

    @Override
    public boolean onError(RpError error) {
        if (!super.onError(error))
            showErrorMessage(null, error);
        return true;
    }

    private void doForgetMe() {
        showProgressDialog();
        // TODO implement forget me command on the server
        HonkioApi.userForgetMe(HonkioApi.getActiveUser().getUserId(),0,
                new RequestCallback<Void>() {
            @Override
            public boolean onComplete(Response<Void> response) {
                dismissProgressDialog();
                if (response.isStatusAccept()) {
                    showMessage(DLG_TAG_LOGOUT, 0,
                            R.string.hk_screen_settings_gdpr_msg_forget_ok_message);
                }
                return onError(response.getAnyError());
            }

            @Override
            public boolean onError(RpError error) {
                dismissProgressDialog();
                return SettingsGdprFragment.this.onError(error);
            }
        });
    }

    private void doReport(String email) {
        showProgressDialog();
        // TODO use report command
        HonkioApi.userLostPassword(email, 0, new RequestCallback<Void>() {
            @Override
            public boolean onComplete(Response<Void> response) {
                dismissProgressDialog();
                if (response.isStatusAccept()) {
                    showMessage(null, 0,
                            R.string.hk_screen_settings_gdpr_msg_report_ok_message);
                }
                return onError(response.getAnyError());
            }

            @Override
            public boolean onError(RpError error) {
                dismissProgressDialog();
                return SettingsGdprFragment.this.onError(error);
            }
        });
    }
}
