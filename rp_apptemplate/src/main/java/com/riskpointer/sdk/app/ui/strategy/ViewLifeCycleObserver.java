package com.riskpointer.sdk.app.ui.strategy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.utils.uri.UriHandler;

/**
 * @author Shurygin Denis
 */

public interface ViewLifeCycleObserver {

    interface ViewController {
        void addLifeCycleObserver(ViewLifeCycleObserver observer);
        <T extends ViewLifeCycleObserver> T getLifeCycleObserver(String tag);
        void removeLifeCycleObserver(String tag);
        void removeLifeCycleObserver(ViewLifeCycleObserver observer);

        Context getContext();
        Activity getActivity();
        void startActivity(Intent intent);
        void startActivityForResult(Intent intent, int requestCode);
        void requestPermissions(String[] permissions, int requestCode);

        DialogFragment showDialog(int dialogId, String tag, Object... params);
        DialogFragment showDialog(DialogFragment dialog, String tag);
        void dismissDialog(String tag);

        <T extends View> T findViewById(int viewId);
    }

    String getTag();

    void setParentLifeCycleObserverTag(String tag);
    String getParentLifeCycleObserverTag();
    <T extends ViewLifeCycleObserver> T getParentLifeCycleObserver();

    void onAttachToController(ViewController viewController);
    void onDetachFromController(ViewController viewController);
    ViewController getController();
    boolean isAttached();

    void saveState(ViewController viewController, Bundle outState);
    void restoreState(ViewController viewController, Bundle savedInstanceState);

    void onWillCreate(ViewController viewController, Bundle savedInstanceState);
    void onCreate(ViewController viewController, Bundle savedInstanceState);
    void onViewCreated(ViewController viewController, Bundle savedInstanceState);

    void onStart(ViewController viewController);
    void onResume(ViewController viewController);
    void onPause(ViewController viewController);
    void onStop(ViewController viewController);
    void onDestroy(ViewController viewController);

    void onActivityResult(ViewController viewController, int requestCode, int resultCode, Intent data);

    boolean onCreateOptionsMenu(ViewController viewController, Menu menu, MenuInflater inflater);
    boolean onOptionsItemSelected(ViewController viewController, MenuItem item);
    boolean onActionClick(ViewController viewController, int actionId);
    boolean onBackPressed(ViewController viewController);

    boolean onError(ViewController viewController, RpError error);

    boolean onDismissDialog(ViewController viewController, BaseDialogFragment dialogFragment);
    boolean onClickDialog(ViewController viewController, BaseDialogFragment dialogFragment, int which);

    void onPermissionDenied(ViewController viewController, String permission, int requestCode);
    void onPermissionGranted(ViewController viewController, String permission, int requestCode);

    @Deprecated
    UriHandler getUriHandler(BaseActivity activity);

}
