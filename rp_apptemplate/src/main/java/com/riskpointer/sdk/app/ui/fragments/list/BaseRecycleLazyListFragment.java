package com.riskpointer.sdk.app.ui.fragments.list;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.callback.ErrorCallback;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.adapters.BaseRecyclerListAdapter;

import java.util.List;

/**
 * Created by Shurygin Denis on 2016-07-25.
 */
public abstract class BaseRecycleLazyListFragment<ListType> extends BaseRecycleListFragment implements LoaderManager.LoaderCallbacks<BaseRecycleLazyListFragment.ListLoaderResult<ListType>> {

    public static final int LIST_LOADER_ID = 1;

    private ListLoader<ListType, ?> mListLoader;
    protected BaseRecyclerListAdapter<ListType, ?> mAdapter;
    private List<ListType> mList;

    private View mProgressFooter;
    private boolean isProgressFooterVisible = false;
// =================================================================================================
// Abstract methods
// =================================================================================================

    @Override
    public abstract ListLoader<ListType, ?> onCreateLoader(int id, Bundle args);

    protected abstract BaseRecyclerListAdapter<ListType, ?> newAdapter(List<ListType> list);

// =================================================================================================
// Methods of Super Class
// =================================================================================================

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mListLoader = (ListLoader<ListType, ?>) getLoaderManager().initLoader(LIST_LOADER_ID, null, this);

        if (mListLoader.isReset() || (mListLoader.getList() == null && !mListLoader.isUpdating()))
            mListLoader.forceLoad();
        else if (mListLoader.getList() != null) {
            onLoadFinished(mListLoader, new ListLoaderResult<>(mListLoader.getList()));
        }

        getListView().addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if (!(mListLoader.isUpdating()) && mListLoader.isCanBeUpdated() && isShouldBeUpdated(recyclerView, dx, dy)) {
                    mListLoader.loadNext();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mAdapter = null;
        mProgressFooter = null;
        setListAdapter(null);
    }

    @Override
    public void onRefresh() {
        setRefreshing(true);
        if (isAdded() && mListLoader != null && !mListLoader.isUpdating())
            reloadList();
    }

    @Override
    public void onResume() {
        if (mListLoader.isReset() || (mListLoader.getList() == null && !mListLoader.isUpdating())) {
            mListLoader.forceLoad();
        }
        else if (mListLoader.getList() != null) {
            onLoadFinished(mListLoader, new ListLoaderResult<>(mListLoader.getList()));
        }

        super.onResume();
        setRefreshing(mListLoader != null && mListLoader.isUpdating());
    }

    @Override
    protected void reloadList() {
        getLoaderManager().destroyLoader(LIST_LOADER_ID);
        mListLoader = (ListLoader<ListType, ?>) getLoaderManager().restartLoader(LIST_LOADER_ID, null, this);
        mListLoader.forceLoad();
    }

    protected void notifyDataSetChanged() {
        if (mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }

    @Override
    protected boolean isListEmpty() {
        return (!(mList != null && mList.size() > 0));
    }

    // =================================================================================================
// Methods from Interfaces
// =================================================================================================

    @Override
    public void onLoaderReset(Loader<ListLoaderResult<ListType>> loader) {
        // Do nothing
    }

    @Override
    public void onLoadFinished(Loader<ListLoaderResult<ListType>> loader, ListLoaderResult<ListType> data) {
        if (data.list != null) {
            ListLoader<ListType, ?> listLoader = (ListLoader<ListType, ?>) loader;

            setRefreshing(false);
            updateList(data.list);

            setProgressFooterVisibility(listLoader.isCanBeUpdated());
        }
        else if (data.error != null) {
            if (!onError(data.error)) {
                Message message = data.error.message;
                if (message != null) {
                    BroadcastHelper.sendActionUnhandledError(getContext(),
                            message.getCommand(), data.error,
                            Thread.getAllStackTraces().get(Thread.currentThread()));
                }
            }
        }
    }

    @Override
    public boolean onError(RpError error) {
        if(getActivity() != null) {
            setRefreshing(false);
            if(getView() != null)
                setListShown(true);
            if(getActivity() instanceof ErrorCallback)
                if(((ErrorCallback) getActivity()).onError(error))
                    return true;
        }
        return false;
    }

// =================================================================================================
// Public methods and classes
// =================================================================================================

    public List<ListType> getList() {
        return mList;
    }

// =================================================================================================
// Protected methods and classes
// =================================================================================================

    protected View newFooterProgressView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.hk_list_item_history_progress, container, false);
    }

    protected boolean isShouldBeUpdated(RecyclerView recyclerView, int dx, int dy) {
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof LinearLayoutManager) {
            final int visibleThreshold = 5;
            int totalItemCount = layoutManager.getItemCount();
            int lastVisibleItem = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();

            return totalItemCount <= (lastVisibleItem + visibleThreshold);
        }
        throw new IllegalStateException("Unknown Layout manager. Override 'isShouldBeUpdated' method to define behavior for custom layout manager.");
    }
// =================================================================================================
// Private methods and classes
// =================================================================================================

    private void updateList(List<ListType> list) {
        mList = list;
        if (getActivity() != null) {
            if (mAdapter == null) {
                mAdapter = newAdapter(list);
                attachProgressFooter();
                setListAdapter(mAdapter);

                if (isAdded())
                    setListShown(true);
            }
            else {
                mAdapter.changeList(list);
                if (isAdded())
                    setListShown(true);
            }
            if(mAdapter.getItemCount() == 0 && mListLoader.isCanBeUpdated()) {
                if (isAdded())
                    setListShown(false);
                mListLoader.loadNext();
            }
        }
    }

    private void setProgressFooterVisibility(boolean isVisible) {
        isProgressFooterVisible = isVisible;
        attachProgressFooter();
        if (mProgressFooter != null) {
            mProgressFooter.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        }
    }

    private void attachProgressFooter() {
        if (isProgressFooterVisible && isAdded()) {
            RecyclerView listView = getListView();
            if (listView != null) {
                if (mProgressFooter == null) {
                    mProgressFooter = newFooterProgressView(LayoutInflater.from(getContext()), listView);

                    if (mProgressFooter != null && mAdapter != null)
                        mAdapter.addFooterView(mProgressFooter);
                }
            }
        }
    }

    public static abstract class ListLoader<ListType, CallbackType> extends Loader<ListLoaderResult<ListType>> {
        public static final int QUERY_COUNT = 20;
        private int mSkipRecords=0;
        private Task mTask;
        protected List<ListType> mList;

        private boolean isUpdating = false;
        private boolean isCanBeUpdated = false;

        public ListLoader(Context context) {
            super(context);
        }

        public List<ListType> getList() {
            return mList;
        }

        protected abstract Task loadList(Context context, RequestCallback<CallbackType> callback, int count, int skip);
        protected abstract List<ListType> toList(Response<CallbackType> response);

        public void loadNext() {
            loadNext(getContext(), mListUpdater, getSkipRecordsCount());
        }

        protected int getSkipRecordsCount() {
            return mSkipRecords;
        }

        @Override
        protected void onForceLoad() {
            isUpdating = true;
            mTask = loadList(getContext(), mListLoader, QUERY_COUNT, 0);
        }

        @Override
        protected void onReset() {
            super.onReset();
            if (mTask != null)
                mTask.abort();
        }

        private void loadNext(Context context, RequestCallback<CallbackType> callback, int skip) {
            isUpdating = true;
            mTask = loadList(context, callback, QUERY_COUNT, skip);
        }

        protected List<ListType> filterList(List<ListType> list) {
            return list;
        }

        protected List<ListType> mergeLists(List<ListType> list1, List<ListType> list2) {
            list1.addAll(list2);
            return list1;
        }

        private RequestCallback<CallbackType> mListLoader = new RequestCallback<CallbackType>() {
            @Override
            public boolean onError(RpError error) {
                mTask = null;
                isUpdating = false;
                isCanBeUpdated = false;
                if (!isReset()) {
                    deliverResult(new ListLoaderResult<ListType>(error));
                }
                return true;
            }

            @Override
            public boolean onComplete(Response<CallbackType> response) {
                mTask = null;
                isUpdating = false;
                if (!isReset()) {
                    if (response.isStatusAccept()) {
                        mList = toList(response);
                        mSkipRecords = mList.size();
                        isCanBeUpdated = mList.size() >= QUERY_COUNT;
                        mList = filterList(mList);
                        deliverResult(new ListLoaderResult<>(mList));
                        return true;
                    } else
                        return onError(response.getAnyError());
                }
                return true;
            }
        };

        private RequestCallback<CallbackType> mListUpdater = new RequestCallback<CallbackType>() {
            @Override
            public boolean onError(RpError error) {
                mTask = null;
                isUpdating = false;
                isCanBeUpdated = false;
                if (!isReset()) {
                    deliverResult(new ListLoaderResult<ListType>(error));
                }
                return true;
            }

            @Override
            public boolean onComplete(Response<CallbackType> response) {
                mTask = null;
                isUpdating = false;
                if (!isReset()) {
                    if (response.isStatusAccept()) {
                        List<ListType> list = toList(response);
                        isCanBeUpdated = list.size() >= QUERY_COUNT;
                        mSkipRecords = mSkipRecords + list.size();
                        mList = mergeLists(mList, filterList(list));
                        deliverResult(new ListLoaderResult<>(mList));
                        return true;
                    } else
                        return onError(response.getAnyError());
                }
                return true;
            }
        };

        public boolean isUpdating() {
            return isUpdating;
        }

        public boolean isCanBeUpdated() {
            return isCanBeUpdated;
        }
    }

    public static class ListLoaderResult<ListType> {

        public final List<ListType> list;
        public final RpError error;

        public ListLoaderResult(List<ListType> list) {
            this.list = list;
            error = null;
        }

        public ListLoaderResult(RpError error) {
            list = null;
            this.error = error;
        }
    }
}
