package com.riskpointer.sdk.app.ui.dialogs;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.riskpointer.sdk.api.web.callback.ErrorCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.app.FragmentComponents;
import com.riskpointer.sdk.app.appmodel.AppController;

/**
 * @author Shurygin Denis on 2014-12-01.
 */
public abstract class BaseDialogFragment extends DialogFragment implements ErrorCallback, DialogInterface.OnClickListener {

    public static final String ARG_SHOW_AS_DIALOG = "BaseFragment.ARG_SHOW_AS_DIALOG";

    public static final String ARG_LAYOUT_ID = "BaseFragment.ARG_LAYOUT_ID";
    public static final String ARG_BUNDLE = "BaseDialogFragment.ARG_BUNDLE";

    public static final String DLG_TAG_PROGRESS = "BaseDialogFragment.DLG_TAG_PROGRESS";

    private DialogFragment mProgressDialog;
    private int mResult;

// =================================================================================================
// Methods of Super Class
// =================================================================================================


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mProgressDialog = (DialogFragment) getChildFragmentManager().findFragmentByTag(DLG_TAG_PROGRESS);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Fragment parentFragment = getParentFragment();
        if (!(parentFragment instanceof OnDismissListener)
                || !((OnDismissListener) parentFragment).onDismissDialog(this)) {

            Activity activity = getActivity();

            if (activity instanceof OnDismissListener)
                ((OnDismissListener) activity).onDismissDialog(this);
        }
    }

// =================================================================================================
// Methods from Interfaces
// =================================================================================================

    @Override
    public void onClick(DialogInterface dialog, int which) {
        onClick(which);
    }

    @Override
    public boolean onError(RpError error) {
        Activity activity = getActivity();
        return activity instanceof ErrorCallback
                && ((ErrorCallback) activity).onError(error);
    }

// =================================================================================================
// Public methods and classes
// =================================================================================================

    public interface OnDismissListener {
        boolean onDismissDialog(BaseDialogFragment dialogFragment);
    }

    public interface OnClickListener {
        boolean onClickDialog(BaseDialogFragment dialogFragment, int which);
    }

    public void setResult(int result) {
        mResult = result;
    }

    public int getResult() {
        return mResult;
    }

    public void show(FragmentManager manager, String tag, boolean allowingStateLoss) {
        if (allowingStateLoss)
            manager.beginTransaction().add(this, tag).commitAllowingStateLoss();
        else
            super.show(manager, tag);
    }

    public void showProgressDialog() {
        showProgressDialog(null, null);
    }

    public void showProgressDialog(int titleId, int messageId) {
        if (isAdded())
            showProgressDialog(getString(titleId), getString(messageId));
        else
            showProgressDialog();
    }

    public void showProgressDialog(String title, String message) {
        if (mProgressDialog == null)
            mProgressDialog = showDialog(AppController.DIALOG_FRAGMENT_PROGRESS, DLG_TAG_PROGRESS, title, message);
    }

    public void dismissProgressDialog() {
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
        mProgressDialog = null;
    }

    public DialogFragment showMessage(String tag, int titleId, int messageId) {
        return showMessage(tag, titleId, messageId, MessageDialog.BTN_NOT_SET);
    }

    public DialogFragment showMessage(String tag, int titleId, int messageId, int buttonsVariant) {
        return showMessage(tag, titleId != 0 ? getString(titleId) : null, getString(messageId), buttonsVariant);
    }

    public DialogFragment showMessage(String tag, int titleId, int messageId, int buttonsVariant, Bundle bundle) {
        return showMessage(tag, titleId != 0 ? getString(titleId) : null, getString(messageId), buttonsVariant, bundle);
    }

    public DialogFragment showMessage(String tag, String title, String message) {
        return showMessage(tag, title, message, MessageDialog.BTN_NOT_SET);
    }

    public DialogFragment showMessage(String tag, String title, String message, int buttonsVariant) {
        return showMessage(tag, title, message, buttonsVariant, null);
    }

    public DialogFragment showMessage(String tag, String title, String message, int buttonsVariant, Bundle bundle) {
        return showDialog(AppController.DIALOG_FRAGMENT_MESSAGE, tag, title, message, buttonsVariant, bundle);
    }

    public DialogFragment showDialog(int dialogId, String tag, Object... params) {
        return showDialog(getChildFragmentManager(), dialogId, tag, params);
    }

    public DialogFragment showDialog(FragmentManager fragmentManager, int dialogId, String tag, Object... params) {
        FragmentComponents components = AppController.getFragmentComponents(dialogId, params);
        if (components != null && components.isDialog()) {
            DialogFragment dialog = components.newDialog();
            fragmentManager.beginTransaction().add(dialog, tag).commitAllowingStateLoss();
            return dialog;
        }
        return null;
    }

    public Bundle getBundle() {
        Bundle args = getArguments();
        if (args != null)
            return args.getBundle(ARG_BUNDLE);
        return null;
    }

    public void setBundle(Bundle bundle) {
        Bundle args = getArguments();
        if (args == null)
            args = new Bundle();

        args.putBundle(ARG_BUNDLE, bundle);
    }

// =================================================================================================
// Protected methods and classes
// =================================================================================================

    protected void onClick(int which) {
        Fragment parentFragment = getParentFragment();
        if (!(parentFragment instanceof OnClickListener)
                || !((OnClickListener) parentFragment).onClickDialog(this, which)) {

            Activity activity = getActivity();

            if (activity instanceof OnClickListener)
                ((OnClickListener) activity).onClickDialog(this, which);
        }
    }

    protected int getDefaultLayoutId() {
        return 0;
    }

    protected final int getLayoutId() {
        Bundle args = getArguments();
        if (args != null && args.containsKey(ARG_LAYOUT_ID))
            return args.getInt(ARG_LAYOUT_ID, getDefaultLayoutId());
        return getDefaultLayoutId();
    }

    protected String getArgsString(Bundle args, String key) {
        if (args != null && key != null && args.containsKey(key)) {
            String string = args.getString(key);
            if (string != null)
                return string;
            int resId = args.getInt(key, 0);
            if (resId != 0)
                return getContext().getString(resId);
        }
        return null;
    }
}
