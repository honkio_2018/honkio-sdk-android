package com.riskpointer.sdk.app.utils;

import android.graphics.Bitmap;
import android.graphics.Color;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.util.EnumMap;
import java.util.Map;

/**
 * @author Shurygin Denis on 2015-08-20.
 */
public class BarcodeUtils {

    public static final int QR_SIZE_MIN = 200;

    public static final int BARCODE_DEF_BACKGROUND = Color.TRANSPARENT;
    public static final int BARCODE_DEF_FOREGROUND = Color.BLACK;

    private static final BarcodeOptions DEFAULT_OPTIONS = new BarcodeOptions();

    public static Bitmap build(String data, int imgWidth, int imgHeight) throws WriterException {
        return build(data, imgWidth, imgHeight, DEFAULT_OPTIONS);
    }

    public static Bitmap build(String data, int imgWidth, int imgHeight, BarcodeOptions options) throws WriterException {
        if (imgWidth < QR_SIZE_MIN)
            imgWidth = QR_SIZE_MIN;
        if (imgHeight < QR_SIZE_MIN)
            imgHeight = QR_SIZE_MIN;

        Map<EncodeHintType, Object> hints = new EnumMap<>(EncodeHintType.class);
        hints.put(EncodeHintType.CHARACTER_SET, options.getCharset());
        hints.put(EncodeHintType.MARGIN, options.getMargin());

        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix encoded = writer.encode(data, options.format, imgWidth, imgHeight, hints);

        int width = encoded.getWidth();
        int height = encoded.getHeight();
        int[] pixels = new int[width * height];

        for (int y = 0; y < height; y++) {
            int dy = y * width;
            for (int x = 0; x < width; x++) {
                pixels[dy + x] = encoded.get(x, y) ? options.getForegroundColor() : options.getBackgroundColor();
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height, options.getBitmapConfig());
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);

        return bitmap;
    }

    public static class BarcodeOptions {
        private String charset = "UTF-8";
        private BarcodeFormat format = BarcodeFormat.QR_CODE;
        private int margin = 0;
        private int foregroundColor = BARCODE_DEF_FOREGROUND;
        private int backgroundColor = BARCODE_DEF_BACKGROUND;
        private Bitmap.Config bitmapConfig = Bitmap.Config.ARGB_8888;

        public String getCharset() {
            return charset;
        }

        public void setCharset(String charset) {
            this.charset = charset;
        }

        public int getMargin() {
            return margin;
        }

        public void setMargin(int margin) {
            this.margin = margin;
        }

        public int getForegroundColor() {
            return foregroundColor;
        }

        public void setForegroundColor(int foregroundColor) {
            this.foregroundColor = foregroundColor;
        }

        public int getBackgroundColor() {
            return backgroundColor;
        }

        public void setBackgroundColor(int backgroundColor) {
            this.backgroundColor = backgroundColor;
        }

        public Bitmap.Config getBitmapConfig() {
            return bitmapConfig;
        }

        public void setBitmapConfig(Bitmap.Config bitmapConfig) {
            this.bitmapConfig = bitmapConfig;
        }
    }
}
