package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.riskpointer.sdk.api.model.database.PushStorage;
import com.riskpointer.sdk.api.model.entity.Invoice;
import com.riskpointer.sdk.api.utils.Currency;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.utils.FormatUtils;

import java.util.List;

/**
 * @author KamiSempai on 2015-03-27.
 */
@Deprecated
public class InvoicesListAdapter extends BaseArrayListAdapter<Invoice, InvoicesListAdapter.ViewHolder> {

    private PushStorage mPushStorage;
    private int mAlertColor;

    public InvoicesListAdapter(Context context, List<Invoice> list, int layout) {
        super(context, list, layout);
        mPushStorage = PushStorage.getInstance(context);

        TypedArray themeArray = context.getTheme().obtainStyledAttributes(new int[] {R.attr.hk_alertTextColor});
        mAlertColor = themeArray.getColor(0, Color.RED);
        themeArray.recycle();
    }

    @Override
    public ViewHolder newHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void bindHolder(ViewHolder holder, int position) {
        Invoice invoice = getItem(position);
        if (invoice.getMerchantName() != null)
            holder.name.setText(invoice.getMerchantName());
        else
            holder.name.setText("-");

        if (invoice.getSubject() != null)
            holder.invoice.setText(invoice.getSubject());
        else
            holder.invoice.setText("-");

        holder.date.setText(FormatUtils.formatDate(mContext, invoice.getDate(), false));
        holder.dueDate.setText(FormatUtils.formatDate(mContext, invoice.getDueDate(), false));

        if (!invoice.isStatusPaid() && invoice.isOverDue())
            holder.dueDate.setTextColor(mAlertColor);
        else
            holder.dueDate.setTextColor(holder.originalDueColors);

        holder.amount.setText(FormatUtils.format(invoice.getAmount()));
        holder.currency.setText(Currency.getSymbol(invoice.getCurrency()));

        long count = mPushStorage.getInvoicePushesCount(invoice.getId());
        if (count > 0) {
            holder.count.setText("New");
            holder.count.setVisibility(View.VISIBLE);
        }
        else
            holder.count.setVisibility(View.GONE);
    }

    class ViewHolder {
        final TextView name;
        final TextView invoice;
        final TextView date;
        final TextView dueDate;
        final TextView amount;
        final TextView currency;
        final TextView count;
        final ColorStateList originalDueColors;

        public ViewHolder(View view) {
            name = (TextView) view.findViewById(R.id.hk_txtName);
            invoice = (TextView) view.findViewById(R.id.hk_txtInvoice);
            date = (TextView) view.findViewById(R.id.hk_txtDate);
            dueDate = (TextView) view.findViewById(R.id.hk_txtDueDate);
            amount = (TextView) view.findViewById(R.id.hk_txtAmount);
            currency = (TextView) view.findViewById(R.id.hk_txtCurrency);
            count = (TextView) view.findViewById(R.id.hk_txtCount);
            originalDueColors = dueDate.getTextColors();
        }
    }
}
