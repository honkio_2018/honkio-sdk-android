package com.riskpointer.sdk.app.ui.strategy;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.HkAddress;
import com.riskpointer.sdk.api.model.entity.ServerInfo;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.textvalidator.BaseTextValidator;
import com.riskpointer.sdk.app.textvalidator.NoEmptyTextValidator;
import com.riskpointer.sdk.app.ui.adapters.CountriesSpinAdapter;
import com.riskpointer.sdk.app.utils.Countries;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * @author Shurygin Denis
 */

public class BaseAddressViewGroupLCO extends BaseViewLifeCycleObserver {
    public static final String TAG = "BaseAddressViewGroupLCO.TAG";

    private static final String SAVED_LOCATION = "BaseAddressViewGroupLCO.SAVED_LOCATION";
    private static final String SAVED_LOCATION_REQUIRE = "BaseAddressViewGroupLCO.SAVED_LOCATION_REQUIRE";
    private static final String SAVED_LOCATION_TEXT_CHANGE = "BaseAddressViewGroupLCO.SAVED_LOCATION_TEXT_CHANGE";

    private EditText mAddress1Edit;
    private EditText mAddress2Edit;
    private EditText mAddressCityEdit;
    private EditText mAddressAdminEdit;
    private EditText mPostCodeEdit;

    private Spinner mSpinnerAddressCountry;
    private CountriesSpinAdapter mCountriesAdapter;

    private Location mLocation;
    private boolean isLocationRequired = false;
    private boolean isAddressTextChanged = false;//set true, when user modified address directly
    private boolean isAddressTextValid = false;

    private EditText[] mRequiredFields;
    private BaseTextValidator mTextValidator;

    private Geocoder mGeoCoder;
    private SpinnerInteractionListener mCountrySelectListener;

    public BaseAddressViewGroupLCO(String tag) {
        super(tag);
    }

    public BaseAddressViewGroupLCO() {
        this(TAG);
    }

    @Override
    public void restoreState(ViewController viewController, Bundle savedInstanceState) {
        super.restoreState(viewController, savedInstanceState);

        if (savedInstanceState != null) {
            mLocation = savedInstanceState.getParcelable(SAVED_LOCATION);
            isLocationRequired = savedInstanceState.getBoolean(SAVED_LOCATION_REQUIRE);
            isAddressTextChanged = savedInstanceState.getBoolean(SAVED_LOCATION_TEXT_CHANGE);
        }
    }

    @Override
    public void saveState(ViewController viewController, Bundle outState) {
        super.saveState(viewController, outState);

        outState.putParcelable(SAVED_LOCATION, mLocation);
        outState.putBoolean(SAVED_LOCATION_REQUIRE, isLocationRequired);
        outState.putBoolean(SAVED_LOCATION_TEXT_CHANGE, isAddressTextChanged);
    }

    @Override
    public void onViewCreated(ViewController viewController, Bundle savedInstanceState) {
        super.onViewCreated(viewController, savedInstanceState);

        mTextValidator = new NoEmptyTextValidator(getString(R.string.text_validator_field_empty));

        mAddress1Edit = viewController.findViewById(R.id.hk_edAddress1);
        mAddress2Edit = viewController.findViewById(R.id.hk_edAddress2);
        mAddressCityEdit = viewController.findViewById(R.id.hk_edCity);
        mAddressAdminEdit = viewController.findViewById(R.id.hk_edAddressAdmin);
        mPostCodeEdit = viewController.findViewById(R.id.hk_edPostNumber);
        mSpinnerAddressCountry = viewController.findViewById(R.id.hk_spinCountry);

        mCountriesAdapter = new CountriesSpinAdapter(getContext(), Countries.LIST, R.layout.hk_list_item_spinner);
        mSpinnerAddressCountry.setAdapter(mCountriesAdapter);
        mCountrySelectListener = new SpinnerInteractionListener();
        enableAddressTextChangeListeners();

        viewController.findViewById(R.id.hk_btnSelectCurrentLocation)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestFocus();
                        setCurrentLocation();
                    }
                });
    }

    public void setLocation(Location location) {
        mLocation = location;
    }

    private void enableAddressTextChangeListeners() {
        if (mAddress1Edit != null)
            mAddress1Edit.addTextChangedListener(mAddressTextWatcher);
        if (mAddress2Edit != null)
            mAddress2Edit.addTextChangedListener(mAddressTextWatcher);
        if (mAddressCityEdit != null)
            mAddressCityEdit.addTextChangedListener(mAddressTextWatcher);
        if (mAddressAdminEdit != null)
            mAddressAdminEdit.addTextChangedListener(mAddressTextWatcher);
        if (mPostCodeEdit != null)
            mPostCodeEdit.addTextChangedListener(mAddressTextWatcher);
        if (mSpinnerAddressCountry != null)
            mSpinnerAddressCountry.setOnItemSelectedListener(mCountrySelectListener);
    }

    private void disableAddressTextChangeListener() {
        if (mAddress1Edit != null)
            mAddress1Edit.removeTextChangedListener(mAddressTextWatcher);
        if (mAddress2Edit != null)
            mAddress2Edit.removeTextChangedListener(mAddressTextWatcher);
        if (mAddressCityEdit != null)
            mAddressCityEdit.removeTextChangedListener(mAddressTextWatcher);
        if (mAddressAdminEdit != null)
            mAddressAdminEdit.removeTextChangedListener(mAddressTextWatcher);
        if (mPostCodeEdit != null)
            mPostCodeEdit.removeTextChangedListener(mAddressTextWatcher);
        if (mSpinnerAddressCountry != null)
            mSpinnerAddressCountry.setOnItemSelectedListener(null);
    }

    public void setAddress(HkAddress address) {
        if (address != null) {
            disableAddressTextChangeListener();
            setAddress1(address.getAddress1());
            setAddress2(address.getAddress2());
            setAddressCity(address.getCity());
            setAddressAdmin(address.getAdmin());
            setAddressPostalCode(address.getPostalCode());
            setAddressCountry(address.getCountryIso());
            enableAddressTextChangeListeners();
        }
        else {
            setAddress1("");
            setAddress2("");
            setAddressCity("");
            setAddressAdmin("");
            setAddressPostalCode("");
        }
        isAddressTextChanged = false; //because user didn't modified address
        isAddressTextValid = true; //because it is already validated before savings
    }

    private void setAddress1(String string) {
        if (mAddress1Edit != null)
            mAddress1Edit.setText(string);
    }

    private void setAddress2(String string) {
        if (mAddress2Edit != null)
            mAddress2Edit.setText(string);
    }

    private void setAddressCity(String string) {
        if (mAddressCityEdit != null)
            mAddressCityEdit.setText(string);
    }

    private void setAddressAdmin(String string) {
        if (mAddressAdminEdit != null)
            mAddressAdminEdit.setText(string);
    }

    private void setAddressCountry(String iso) {
        if (mSpinnerAddressCountry != null) {
            int index = mCountriesAdapter.findCountryByIso(iso);
            if (index == -1)
                index = mCountriesAdapter.findCountryByIso("FIN");
            mSpinnerAddressCountry.setSelection(index);
        }
    }

    private void setAddressPostalCode(String string) {
        if (mPostCodeEdit != null)
            mPostCodeEdit.setText(string);
    }

    public void setRequiredField(EditText[] fields) {
        mRequiredFields = fields;
    }

    public Location getLocation() {
        return mLocation;
    }

    public String getAddress1() {
        if (mAddress1Edit != null)
            return mAddress1Edit.getText().toString();
        return null;
    }

    public String getAddress2() {
        if (mAddress2Edit != null)
            return mAddress2Edit.getText().toString();
        return null;
    }

    public String getAddressCity() {
        if (mAddressCityEdit != null)
            return mAddressCityEdit.getText().toString();
        return null;
    }

    public String getAddressAdmin() {
        if (mAddressAdminEdit != null)
            return mAddressAdminEdit.getText().toString();
        return null;
    }

    public ServerInfo.Country getAddressCountry() {
        if (mCountriesAdapter != null)
            return mCountriesAdapter.getItem(mSpinnerAddressCountry.getSelectedItemPosition());
        return null;
    }

    public String getAddressPostalCode() {
        if (mPostCodeEdit != null)
            return mPostCodeEdit.getText().toString();
        return null;
    }

    public HkAddress getAddress() {
        HkAddress address = new HkAddress();
        address.setAddress1(getAddress1());
        address.setAddress2(getAddress2());
        address.setCity(getAddressCity());
        address.setAdmin(getAddressAdmin());
        address.setPostalCode(getAddressPostalCode());

        ServerInfo.Country country = getAddressCountry();
        address.setCountry(country.getIso(), country.getName());

        return address;
    }

    public boolean validate() {
        boolean isValid = true;
        if (mRequiredFields != null && mRequiredFields.length > 0) {
            for (EditText edit : mRequiredFields) {
                isValid = mTextValidator.validate(edit) && isValid;
            }
        }
        return isValid && isAddressTextValid && !(isLocationRequired && mLocation == null
                && TextUtils.isEmpty(mAddress1Edit.getText().toString()));
    }

    public void syncLocation(Runnable completionRunnable) {
        if (isAddressTextChanged &&
                mPostCodeEdit != null && mAddress1Edit != null &&
                mAddressCityEdit != null && mAddressAdminEdit != null) {

            String addressString = mPostCodeEdit.getText().toString()
                    + ", " + mAddress1Edit.getText().toString()
                    + ", " + mAddressCityEdit.getText().toString()
                    + ", " + mAddressAdminEdit.getText().toString()
                    + ", " + getAddressCountry().getName();

            new AsyncSyncLocation(this, completionRunnable).execute(addressString);
        } else {
            completionRunnable.run();
        }
    }

    public void setLocation(String locationName) {
        new AsyncSetLocation(this).execute(locationName);
    }

    protected void setCurrentLocation() {
        new AsyncSetCurrentLocation(this).execute();
    }

    protected void requestFocus() {
        if (!mAddress1Edit.isFocused()
                && !mAddress2Edit.isFocused()
                && !mAddressCityEdit.isFocused()
                && !mAddressAdminEdit.isFocused()
                && !mPostCodeEdit.isFocused()) {
            mAddress1Edit.requestFocus();
        }
    }

    protected void onAddressSelected(Address address, Location location) {
        if (address != null) {
            setAddressPostalCode(address.getPostalCode());
            setAddress1(address.getThoroughfare() == null
                    ? ""
                    : address.getThoroughfare()
                    + (address.getFeatureName() == null
                    ? ""
                    : ", " + address.getFeatureName()));
            setAddress2(address.getSubLocality());
            setAddressCity(notNullString(address.getLocality()));
            setAddressCountry(address.getCountryCode());

            if (!TextUtils.isEmpty(address.getAdminArea()))
                setAddressAdmin(notNullString(address.getAdminArea()));
            else
                setAddressAdmin(notNullString(address.getSubAdminArea()));

            isAddressTextChanged = false;
            isAddressTextValid = true;
        }

        mLocation = location;
    }

    protected synchronized Geocoder getGeoCoder() {
        if (mGeoCoder == null)
            mGeoCoder = new Geocoder(getContext(), Locale.getDefault());
        return mGeoCoder;
    }

    protected void onAddressNotFound() {

    }

    private String notNullString(String string) {
        return string != null ? string : "";
    }

    private TextWatcher mAddressTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            isAddressTextChanged = true;
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private class SpinnerInteractionListener implements AdapterView.OnItemSelectedListener, View.OnTouchListener {
        boolean userSelect = false;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            userSelect = true;
            return false;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            if (userSelect) {
                userSelect = false;
                isAddressTextChanged = true;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
            if (userSelect) {
                userSelect = false;
                isAddressTextChanged = true;
            }
        }
    }

    private static class AsyncSyncLocation extends AsyncTask<String, Void, Address> {

        private BaseAddressViewGroupLCO mAddressLco;
        private Runnable mCompletionRunnable;

        AsyncSyncLocation(BaseAddressViewGroupLCO addressLco, Runnable completionRunnable) {
            super();
            mAddressLco = addressLco;
            mCompletionRunnable = completionRunnable;
        }

        @Override
        protected Address doInBackground(String... params) {
            try {
                List<Address> addresses = mAddressLco.getGeoCoder().getFromLocationName(params[0], 5);
                for (Address address : addresses) {
                    Log.d("ADDR", "syncLocation " + address.toString());
                }
                if (addresses.size() > 0)
                    return addresses.get(0);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Address address) {
            mAddressLco.isAddressTextChanged = false;
            if (address != null) {
                mAddressLco.mLocation = new Location(LocationManager.GPS_PROVIDER);
                mAddressLco.mLocation.setLatitude(address.getLatitude());
                mAddressLco.mLocation.setLongitude(address.getLongitude());

                mAddressLco.isAddressTextValid = !TextUtils.isEmpty(address.getPostalCode()) &&
                        (
                                !TextUtils.isEmpty(address.getThoroughfare()) ||
                                        !TextUtils.isEmpty(address.getSubThoroughfare())
                        ) && (
                        !TextUtils.isEmpty(address.getLocality()) ||
                                !TextUtils.isEmpty(address.getSubLocality())
                );
            } else
                mAddressLco.isAddressTextValid = false;

            mCompletionRunnable.run();

            mCompletionRunnable = null;
            mAddressLco = null;
        }
    }

    private static class AsyncSetLocation extends AsyncTask<String, Void, Address> {

        private BaseAddressViewGroupLCO mAddressLco;

        AsyncSetLocation(BaseAddressViewGroupLCO addressLco) {
            super();
            mAddressLco = addressLco;
        }

        @Override
        protected Address doInBackground(String... params) {
            Location location = HonkioApi.getLocationFinder().getLocation();
            if (location == null)
                return null;
            try {
                List<Address> addresses = mAddressLco.getGeoCoder().getFromLocationName(params[0], 5);
                for (Address address : addresses) {
                    Log.d("ADDR", "setLocation " + address.toString());
                }

                if (addresses.size() > 0)
                    return addresses.get(0);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Address address) {
            if (address != null) {
                Location location = new Location(LocationManager.GPS_PROVIDER);
                location.setLatitude(address.getLatitude());
                location.setLongitude(address.getLongitude());
                mAddressLco.onAddressSelected(address, location);
            }
        }
    }

    private static class AsyncSetCurrentLocation extends AsyncTask<String, Void, Address> {

        private BaseAddressViewGroupLCO mAddressLco;

        AsyncSetCurrentLocation(BaseAddressViewGroupLCO addressLco) {
            super();
            mAddressLco = addressLco;
        }

        @Override
        protected void onPreExecute() {
            mAddressLco.showProgressDialog();
        }

        @Override
        protected Address doInBackground(String... params) {
            Location location = HonkioApi.getLocationFinder().getLocation();
            if (location == null)
                return null;
            try {
                List<Address> addresses = mAddressLco.getGeoCoder().getFromLocation(
                        location.getLatitude(),
                        location.getLongitude(), 1);

                if (addresses.size() > 0)
                    return addresses.get(0);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Address address) {
            mAddressLco.dismissProgressDialog();
            if (address != null) {
                Location location = new Location(LocationManager.GPS_PROVIDER);
                location.setLatitude(address.getLatitude());
                location.setLongitude(address.getLongitude());
                mAddressLco.onAddressSelected(address, location);
            } else
                mAddressLco.onAddressNotFound();
        }
    }
}
