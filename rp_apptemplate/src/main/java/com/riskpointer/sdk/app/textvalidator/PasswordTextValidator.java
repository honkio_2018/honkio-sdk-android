/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.textvalidator;

import android.content.res.Resources;

import com.riskpointer.sdk.app.R;

public class PasswordTextValidator extends ValidatorsSet {
	private static final int MIN_PASSWORD_SIZE = 6;

	public PasswordTextValidator(Resources resources) {
		add(new BaseTextValidator(resources.getString(R.string.text_validator_passwor_short)) {
			@Override
			public boolean validate(String string) {
				return string.length() >= MIN_PASSWORD_SIZE;
			}
		});
		add(new BaseTextValidator(resources.getString(R.string.text_validator_passwor_cont_spaces)) {
			
			@Override
			public boolean validate(String string) {
				if(string.length() > 0)
					return string.charAt(0) != ' ' && string.charAt(string.length() - 1) != ' ';
				return true;
			}
		});
	}
}
