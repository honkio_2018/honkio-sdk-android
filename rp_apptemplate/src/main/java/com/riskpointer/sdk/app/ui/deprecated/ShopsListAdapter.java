/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.deprecated;

import android.content.Context;
import android.location.Location;
import android.view.View;

import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.app.ui.adapters.BaseArrayListAdapter;
import com.riskpointer.sdk.app.ui.adapters.binder.BaseShopsListBinder;
import com.riskpointer.sdk.app.ui.adapters.binder.ShopsListBinder;
import com.riskpointer.sdk.app.ui.adapters.binder.ShopsListBinder.ViewHolder;

import java.util.List;

/**
 * DEPRECATED. Use RecyclerShopsListAdapter instead.
 */
@Deprecated
public class ShopsListAdapter extends BaseArrayListAdapter<Shop, ViewHolder> {

	private BaseShopsListBinder<Shop, ViewHolder> mListBinder;

	public ShopsListAdapter(Context context, List<Shop> list, int layoutId) {
		this(context, list, layoutId, new ShopsListBinder());
	}

	public ShopsListAdapter(Context context, List<Shop> list, int layoutId, BaseShopsListBinder<Shop, ViewHolder> binder) {
		super(context, list, new int[]{layoutId} );
		mListBinder = binder;
	}

	@Override
	public ViewHolder newHolder(View view) {
		return new ViewHolder(view);
	}

	@Override
	public void bindHolder(ShopsListBinder.ViewHolder holder, int position) {
		Shop shop = getItem(position);
		mListBinder.bind(mContext, holder, shop);
	}

	@Override
	public int getItemViewType(int position) {
		return 0;
	}

	public void setLocation(Location location) {
		mListBinder.setLocation(location);
	}

    public void setServiceTypeVisible(boolean visible) {
        mListBinder.setServiceTypeVisible(visible);
    }
}
