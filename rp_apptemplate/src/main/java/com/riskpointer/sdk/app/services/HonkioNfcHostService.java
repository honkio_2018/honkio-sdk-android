/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.services;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.nfc.cardemulation.HostApduService;
import android.os.Bundle;

import com.riskpointer.sdk.app.utils.AppPreferences;

@SuppressLint("NewApi")
public class HonkioNfcHostService extends HostApduService {
	public static final String PREF_NFC_MESSAGE = "NfcMessage";

	@Override
	public byte[] processCommandApdu(byte[] commandApdu, Bundle extras) {
		SharedPreferences preferences = getSharedPreferences(AppPreferences.PREFERENCES_FILE, MODE_PRIVATE);
		String message = preferences.getString(PREF_NFC_MESSAGE, null);
		if(message != null)
			return message.getBytes();
		return null;
	}

	@Override
	public void onDeactivated(int reason) {
		// TODO Auto-generated method stub
	}

}
