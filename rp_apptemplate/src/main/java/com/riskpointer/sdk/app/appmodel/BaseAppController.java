package com.riskpointer.sdk.app.appmodel;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.riskpointer.sdk.api.model.entity.AppInfo;
import com.riskpointer.sdk.api.model.entity.Asset;
import com.riskpointer.sdk.api.model.entity.HkQrCode;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.Order;
import com.riskpointer.sdk.api.model.entity.ShopInfo;
import com.riskpointer.sdk.api.model.push.Push;
import com.riskpointer.sdk.api.model.push.entity.AssetChangedPushEntity;
import com.riskpointer.sdk.api.model.push.entity.InvitationPushEntity;
import com.riskpointer.sdk.api.model.push.entity.OrderChatMessagePushEntity;
import com.riskpointer.sdk.api.model.push.entity.OrderStatusChangedPushEntity;
import com.riskpointer.sdk.api.utils.ApiDebugUtils;
import com.riskpointer.sdk.api.utils.ApiLog;
import com.riskpointer.sdk.api.web.message.filter.ShopFilter;
import com.riskpointer.sdk.app.FragmentComponents;
import com.riskpointer.sdk.app.ui.activities.PagerActivity;
import com.riskpointer.sdk.app.ui.activities.SingleFragmentActivity;
import com.riskpointer.sdk.app.ui.dialogs.BarcodeDialog;
import com.riskpointer.sdk.app.ui.dialogs.MessageDialog;
import com.riskpointer.sdk.app.ui.dialogs.ProgressDialog;
import com.riskpointer.sdk.app.ui.fragments.BaseAboutFragment;
import com.riskpointer.sdk.app.ui.fragments.BaseAssetDetailsFragment;
import com.riskpointer.sdk.app.ui.fragments.BaseAssetFragment;
import com.riskpointer.sdk.app.ui.fragments.BaseOrderChatFragment;
import com.riskpointer.sdk.app.ui.fragments.BaseOrderFragment;
import com.riskpointer.sdk.app.ui.fragments.BaseShopFragment;
import com.riskpointer.sdk.app.ui.fragments.LazyQrCodeFragment;
import com.riskpointer.sdk.app.ui.fragments.PinFragment;
import com.riskpointer.sdk.app.ui.fragments.QrScannerFragment;
import com.riskpointer.sdk.app.ui.fragments.TermsOfUseFragment;
import com.riskpointer.sdk.app.ui.fragments.list.BaseShopsListFragment;
import com.riskpointer.sdk.app.ui.fragments.list.PushesListFragment;
import com.riskpointer.sdk.app.ui.fragments.settings.SettingsGdprFragment;
import com.riskpointer.sdk.app.utils.ApiLogTracker;
import com.riskpointer.sdk.app.utils.FragmentUtils;

/**
 * @author Shurygin Denis on 2015-09-08.
 */
public class BaseAppController extends AppController {

    public BaseAppController(Context applicationContext, String appUrl, Identity appIdentity) {
        super(applicationContext, appUrl, appIdentity);

        if (ApiDebugUtils.isDebuggable(applicationContext))
            ApiLog.addListener(ApiLogTracker.getInstance());
    }

    @Override
    public Intent buildNewIntent(int intentId, Object... params) {
        FragmentComponents components = getFragmentComponents(intentId, params);
        if (components != null && components.isFragment())
            return SingleFragmentActivity.newIntent(mAppContext, components);
        else if (intentId == SCREEN_PUSH && assertClass(params, 0, Push.class, false)) {
            Push<?> push = (Push<?>) params[0];
            switch (push.getType()) {
                case Push.Type.TRANSACTION:
                    return getIntent(SCREEN_SHOP_TRANSACTION, push.getEntity());
                case Push.Type.CHAT_MESSAGE:
                    OrderChatMessagePushEntity chatEntity = (OrderChatMessagePushEntity) push.getEntity();
                    return getIntent(SCREEN_SHOP_ORDER, chatEntity.getOrderId());
                case Push.Type.ORDER_STATUS_CHANGED:
                    OrderStatusChangedPushEntity entity = (OrderStatusChangedPushEntity) push.getEntity();
                    return getIntent(SCREEN_SHOP_ORDER, entity.getOrderId());
                case Push.Type.ASSET_CHANGED:
                    AssetChangedPushEntity assetEntity = (AssetChangedPushEntity) push.getEntity();
                    return getIntent(SCREEN_ASSET_DETAILS, assetEntity.getAssetId());
                case Push.Type.INVITATION:
                    InvitationPushEntity invitationEntity = (InvitationPushEntity) push.getEntity();
                    return getIntent(SCREEN_INVITATION_DETAILS, invitationEntity.getInvitation());
                default:
                    return getIntent(FRAGMENT_PUSHES_LIST);
            }
        }
        else if (intentId == ACTIVITY_PAGER) {
            return PagerActivity.newIntent(mAppContext, (String) params[0],
                    (FragmentComponents[]) params[1],
                    (String[]) params[2]);
        }

        StringBuilder builder = new StringBuilder("Activity not found. Activity Id: ");
        builder.append(Integer.toString(intentId));
        if (params != null && params.length > 0) {
            for (int i = 0; i < params.length; i++) {
                builder.append("  Param_").append(Integer.toString(i));
                Object param = params[i];
                if (param != null) {
                    builder.append(" class: ").append(param.getClass().getName());
                    builder.append(" value: ").append(param.toString());
                }
                else
                    builder.append(" is null");
            }
        }
        throw new IllegalArgumentException(builder.toString());
    }

    @Override
    public FragmentComponents buildNewFragmentComponents(int fragmentId, Object... params) {
        FragmentComponents components = new FragmentComponents();
        Bundle args = new Bundle();
        components.setArguments(args);
        switch (fragmentId) {
            case ACTIVITY_PIN:
                components.setFragmentClass(PinFragment.class);
                putInt(args, PinFragment.EXTRA_REASON, params, 0);
                break;
            case ACTIVITY_TOU:
                components.setFragmentClass(TermsOfUseFragment.class);
                putInt(args, TermsOfUseFragment.ARG_REASON, params, 0);
                putSerializable(args, TermsOfUseFragment.ARG_TOU, params, 1, AppInfo.TouInfo.class);
                putString(args, TermsOfUseFragment.ARG_DIALOG_TITLE, params, 2);
                putString(args, TermsOfUseFragment.ARG_DIALOG_MESSAGE, params, 3);
                break;
            case ACTIVITY_GDPR:
                components.setFragmentClass(SettingsGdprFragment.class);
                break;
            case ACTIVITY_ABOUT:
                components.setFragmentClass(BaseAboutFragment.class);
                break;
            case ACTIVITY_CAPTURE:
                components.setFragmentClass(QrScannerFragment.class);
                break;
            case FRAGMENT_PUSHES_LIST:
                components.setFragmentClass(PushesListFragment.class);
                break;
            case SCREEN_QR_CODE:
                components.setFragmentClass(LazyQrCodeFragment.class);
                if (getParam(params, 0) instanceof String)
                    putString(args, LazyQrCodeFragment.ARG_QR_CODE, params, 0);
                else if (getParam(params, 0) instanceof HkQrCode)
                    putSerializable(args, LazyQrCodeFragment.ARG_QR_CODE, params, 0, HkQrCode.class);
                break;
            case SCREEN_ASSET:
                // Fragment class not defined!
                if (getParam(params, 0) instanceof String)
                    putString(args, BaseAssetFragment.ARG_ASSET, params, 0);
                else if (getParam(params, 0) instanceof Asset)
                    putSerializable(args, BaseAssetFragment.ARG_ASSET, params, 0, Asset.class);
                break;
            case SCREEN_ASSET_DETAILS:
                components.setFragmentClass(BaseAssetDetailsFragment.class);
                if (getParam(params, 0) instanceof String)
                    putString(args, BaseAssetDetailsFragment.ARG_ASSET, params, 0);
                else if (getParam(params, 0) instanceof Asset)
                    putSerializable(args, BaseAssetDetailsFragment.ARG_ASSET, params, 0, Asset.class);
                break;
            case SCREEN_SHOP:
                // Fragment class not defined!
                if (getParam(params, 0) instanceof String)
                    putString(args, BaseShopFragment.ARG_SHOP, params, 0);
                else if (getParam(params, 0) instanceof Identity)
                    putSerializable(args, BaseShopFragment.ARG_SHOP, params, 0, Identity.class);
                else if (getParam(params, 0) instanceof ShopInfo)
                    putSerializable(args, BaseShopFragment.ARG_SHOP, params, 0, ShopInfo.class);
                break;
            case SCREEN_SHOPS_LIST:
                components.setFragmentClass(BaseShopsListFragment.class);
                assertClass(params, 0, ShopFilter.class, true);
                putSerializable(args, BaseShopsListFragment.ARG_FILTER, params, 0, ShopFilter.class);
                break;
            case SCREEN_SHOP_ORDER:
                Object param = getParam(params, 0);
                if (param != null && param instanceof String) {
                    putString(args, BaseOrderFragment.ARG_ORDER_ID, params, 0);
                } else {
                    assertClass(params, 0, Order.class, false);
                    putSerializable(args, BaseOrderFragment.ARG_ORDER, params, 0, Order.class);
                }
                if (assertClass(params, 1, Identity.class, true)) {
                    putSerializable(args, FragmentUtils.ARG_SHOP, params, 1, Identity.class);
                }
                break;
            case SCREEN_CHAT:
                putSerializable(args, BaseOrderChatFragment.ARG_ORDER, params, 0, Order.class);
                break;
            case DIALOG_FRAGMENT_MESSAGE:
                components.setFragmentClass(MessageDialog.class);
                putString(args, MessageDialog.ARG_TITLE, params, 0);
                putString(args, MessageDialog.ARG_MESSAGE, params, 1);
                if (getParam(params, 2) instanceof Integer) {
                    putInt(args, MessageDialog.ARG_BUTTONS, params, 2);
                    putBundle(args, MessageDialog.ARG_BUNDLE, params, 3);
                }
                else {
                    args.putInt(MessageDialog.ARG_BUTTONS, MessageDialog.BTN_NOT_SET);
                    if (getParam(params, 2) instanceof String) {
                        putString(args, MessageDialog.ARG_BUTTON_POSITIVE, params, 2);
                        if (getParam(params, 3) instanceof String) {
                            putString(args, MessageDialog.ARG_BUTTON_NEGATIVE, params, 3);
                            if (getParam(params, 4) instanceof String) {
                                putString(args, MessageDialog.ARG_BUTTON_NEUTRAL, params, 4);
                                putBundle(args, MessageDialog.ARG_BUNDLE, params, 5);
                            }
                            else
                                putBundle(args, MessageDialog.ARG_BUNDLE, params, 4);
                        }
                        else
                            putBundle(args, MessageDialog.ARG_BUNDLE, params, 3);
                    }
                    else {
                        putBundle(args, MessageDialog.ARG_BUNDLE, params, 2);
                    }
                }
                break;
            case DIALOG_FRAGMENT_PROGRESS:
                components.setFragmentClass(ProgressDialog.class);
                putString(args, ProgressDialog.ARG_TITLE, params, 0);
                putString(args, ProgressDialog.ARG_MESSAGE, params, 1);
                break;
            case DIALOG_FRAGMENT_BARCODE:
                components.setFragmentClass(BarcodeDialog.class);
                putString(args, BarcodeDialog.ARG_BARCODE, params, 0);
                if (params.length > 1)
                    putString(args, BarcodeDialog.ARG_TITLE, params, 1);
                break;
        }
        return components;
    }
}
