package com.riskpointer.sdk.app.ui.fragments.settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.UserAccount;
import com.riskpointer.sdk.api.model.process.user.AccountCreationProcessModel;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;
import com.riskpointer.sdk.app.ui.fragments.WebViewFragment;
import com.riskpointer.sdk.app.utils.FragmentUtils;

/**
 * @author Shurygin Denis on 2015-04-08.
 */
public class SettingsAccountAddCreditCardFragment extends WebViewFragment implements AccountCreationProcessModel.Callback {

    private static final String SAVED_MODEL = "UserLoginProcessModel.SAVED_MODEL";

    private static final String CARD_TYPE = UserAccount.Type.CREDITCARD;

    private AccountCreationProcessModel mModel;

    @Override
    public String getTitle(Context context) {
        return context.getString(R.string.screen_title_add_creditcard);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mModel = new AccountCreationProcessModel(this, zeroAccount(), FragmentUtils.getShop(this));

        if (savedInstanceState != null)
            mModel.restoreInstanceState(getActivity().getApplicationContext(), savedInstanceState.getBundle(SAVED_MODEL));
        else
            mModel.start();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mModel != null)
            outState.putBundle(SAVED_MODEL, mModel.saveInstanceState());
    }

    @Override
    public void onPageLoaded(String url) {
        if(mModel != null)
            mModel.checkStatus();
    }

    @Override
    public void onDestroy() {
        if (mModel != null && getActivity().isFinishing())
            mModel.abort();
        mModel = null;
        super.onDestroy();
    }

    @Override
    public boolean onComplete(UserAccount account) {
        if (isAdded()) {
            Intent data = new Intent();
            data.putExtra(AppController.Extra.USER_ACCOUNT, account);
            getActivity().setResult(Activity.RESULT_OK, data);
            showMessage(BaseActivity.DLG_TAG_FINISH,
                    R.string.pay_creditcard_add_dlg_accept_title,
                    R.string.pay_creditcard_add_dlg_accept_message);
        }
        return false;
    }

    @Override
    public boolean handlePending(Response<?> response) {
        if (response.hasPending("creditcardverify")
                || response.hasPending("seitatechverify")) {
            loadUrl(response.getUrl());
            return true;
        }
        else {
            if (mModel != null)
                mModel.abort();
            return onError(RpError.UNKNOWN);
        }
    }

    @Override
    public boolean onError(RpError error) {
        if (isAdded()) {
            switch (error.code) {
                case RpError.Api.CANCELED_BY_USER:
                    getActivity().finish();
                    return true;
                case RpError.UserPayment.USER_NOT_ACTIVE:
                    showMessage(BaseActivity.DLG_TAG_FINISH,
                            R.string.dlg_need_verify_email_title,
                            R.string.dlg_need_verify_email_message);
                    return true;
                case RpError.Api.ACCOUNT_NOT_SUPPORTED:
                    showMessage(BaseActivity.DLG_TAG_FINISH, R.string.dlg_unsuported_account_title, R.string.dlg_unsuported_account_message);
                    return true;
                case RpError.Api.INVALID_PIN:
                    if (HonkioApi.isConnected())
                        showMessage(BaseActivity.DLG_TAG_FINISH, 0, R.string.dlg_pin_invalid_message);
                    else
                        super.onError(error);
                    return true;
                default:
                    if (!super.onError(error))
                        showErrorMessageAndFinish(error);
                    break;
            }
        }
        return true;
    }

    protected UserAccount zeroAccount() {
        return new UserAccount(CARD_TYPE, UserAccount.ZERO_NUMBER);
    }

    @Override
    protected void setupWebView(WebView webView, WebSettings settings) {
        super.setupWebView(webView, settings);
        settings.setUseWideViewPort(true);
    }

    private void showErrorMessageAndFinish(RpError error) {
        showErrorMessage(BaseActivity.DLG_TAG_FINISH, error,
                R.string.pay_creditcard_add_dlg_failed_title,
                R.string.pay_creditcard_add_dlg_failed_message);
    }
}
