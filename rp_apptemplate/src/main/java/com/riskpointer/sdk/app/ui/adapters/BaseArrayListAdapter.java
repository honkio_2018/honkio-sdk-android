/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;
import android.view.View;

import java.util.List;

public abstract class BaseArrayListAdapter<T, H> extends BaseContextualAdapter<H> {

	private List<T> mList;
	private int[] mLayouts;
	
	public BaseArrayListAdapter(Context context, List<T> list, int layout) {
		this(context, list, new int[]{layout});
	}

	public BaseArrayListAdapter(Context context, List<T> list, int[] layouts) {
		super(context);
		mList = list;
		mLayouts = layouts;
	}
	
	public abstract H newHolder(View view);
	public abstract void bindHolder(H holder, int position);

	@Override
	public int getCount() {
		if(mList == null) return 0;
		return mList.size();
	}

	@Override
	public T getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getViewTypeCount() {
		return mLayouts.length;
	}

	@Override
	protected int getViewLayoutId(int position) {
		return mLayouts[getItemViewType(position)];
	}
	
	public void move(int position, int newPosition) {
		T object = mList.remove(position);
		mList.add(newPosition, object);
		notifyDataSetChanged();
	}
	
	public void remove(int position) {
		mList.remove(position);
		notifyDataSetChanged();
	}

	public void changeList(List<T> list) {
		mList = list;
		notifyDataSetChanged();
	}

	public void appendList(List<T> list) {
		mList.addAll(list);
		notifyDataSetChanged();
	}
}
