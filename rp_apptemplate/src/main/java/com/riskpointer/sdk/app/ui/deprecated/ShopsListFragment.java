/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.deprecated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.ShopFind;
import com.riskpointer.sdk.api.model.entity.ShopInfo;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.support.BroadcastManager;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.filter.ShopFilter;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.adapters.BaseArrayListAdapter;
import com.riskpointer.sdk.app.ui.fragments.list.BaseListRefLazyFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * DEPRECATED. Use BaseShopsListFragment instead.
 */
@Deprecated
public class ShopsListFragment extends BaseListRefLazyFragment<Shop, ShopFind> {
    public static final String ARG_FILTER = "ShopsListFragment.ARG_FILTER";

    private static final long DEF_SEARCH_RADIUS = 5000;

    private ShopsListAdapter mShopAdapter;
    private BroadcastReceiver mLocationFindListener;

    private ShopFilter mFilter;

    @Override
    protected int getDefaultListItemLayoutId(int type) {
        return R.layout.hk_list_item_shop;
    }

    @Override
    public String getTitle(Context context) {
        return context.getString(R.string.screen_title_shops);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mFilter = (ShopFilter) savedInstanceState.getSerializable(ARG_FILTER);
        } else if (mFilter == null) {
            Bundle args = getArguments();
            if (args != null)
                mFilter = (ShopFilter) args.getSerializable(ARG_FILTER);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setEmptyText(getString(R.string.shops_list_shop_not_found));
    }

    @Override
    protected void reloadList(Context context) {
        ShopFilter filter = getFilter();
        if ((filter.getLatitude() != null && filter.getLongitude() != null) || HonkioApi.getLocation() != null) {
            super.reloadList(context);
        } else {
            onError(new RpError(RpError.Api.ABORTED));
            Toast.makeText(context, "Failed to define location.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(ARG_FILTER, mFilter);
    }

    @Override
    public void onAttach(Context context) {
        BroadcastManager.getInstance(context).registerReceiver(mLocationChangeListener, new IntentFilter(BroadcastHelper.BROADCAST_ACTION_ON_LOCATION_CHANGED));
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        if (mLocationFindListener != null)
            BroadcastManager.getInstance(getActivity()).unregisterReceiver(mLocationFindListener);
        BroadcastManager.getInstance(getActivity()).unregisterReceiver(mLocationChangeListener);
        super.onDetach();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (getList() != null && position < getList().size()) {
            showProgressDialog();
            Shop item = getList().get(position);
            HonkioApi.shopGetInfo(item, 0, mShopOpenCallback);
        }

    }

    public ShopFilter getFilter() {
        if (mFilter == null) {
            Bundle args = getArguments();
            if (args != null)
                mFilter = (ShopFilter) args.getSerializable(ARG_FILTER);

            if (mFilter == null) {
                mFilter = new ShopFilter();
                mFilter.setRadius(Long.toString(DEF_SEARCH_RADIUS));
            }

        }
        return mFilter;
    }

    @Override
    public ListLoader onCreateLoader(int id, Bundle args) {
        return new ShopsListLoader(getActivity().getApplicationContext(), getFilter());
    }

    @Override
    protected BaseArrayListAdapter<Shop, ?> newAdapter(List<Shop> list) {

        return new ShopsListAdapter(getActivity(), list, getListItemLayoutId(0));
    }

    private RequestCallback<ShopInfo> mShopOpenCallback = new RequestCallback<ShopInfo>() {

        @Override
        public boolean onError(RpError error) {
            dismissProgressDialog();
            return isAdded() && ShopsListFragment.this.onError(error);
        }

        @Override
        public boolean onComplete(Response<ShopInfo> response) {
            dismissProgressDialog();
            if (response.isStatusAccept()) {
                openShop(response.getResult().getShop());

                return true;
            } else
                return onError(response.getAnyError());
        }
    };

    protected void openShop(Shop shop) {
        Intent intent = AppController.getIntent(AppController.SCREEN_SHOP, shop);
        startActivity(intent);
    }

    private BroadcastReceiver mLocationChangeListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (mShopAdapter != null) {
                Location location = intent.getParcelableExtra(BroadcastHelper.BROADCAST_EXTRA_LOCATION);
                if (location != null) {
                    mShopAdapter.setLocation(location);
                    mShopAdapter.notifyDataSetChanged();
                }
            }
        }
    };

    @Override
    public void onLoadFinished(Loader<ListLoaderResult<Shop>> loader, ListLoaderResult<Shop> data) {
        super.onLoadFinished(loader, data);
        mShopAdapter = (ShopsListAdapter) mAdapter;
        if (mShopAdapter != null) {
            mShopAdapter.setLocation(HonkioApi.getLocation());
            mShopAdapter.setServiceTypeVisible(TextUtils.isEmpty(getFilter().getServiceType()));
            mShopAdapter.notifyDataSetChanged();
        }
    }

    public static class ShopsListLoader extends ListLoader<Shop, ShopFind> {
        private ShopFilter mFilter;

        public ShopsListLoader(Context context, ShopFilter filter) {
            super(context);
            mFilter = filter;
        }

        @Override
        protected ArrayList<Shop> toList(Response<ShopFind> response) {
            return response.getResult().getList();
        }

        @Override
        protected Task loadList(Context context, RequestCallback<ShopFind> callback, int count, int skip) {
            if (mFilter == null) {
                mFilter = new ShopFilter();
            }
            mFilter.setCount(count);
            mFilter.setSkip(skip);
            return HonkioApi.shopFind(mFilter, 0, callback);
        }
    }
}
