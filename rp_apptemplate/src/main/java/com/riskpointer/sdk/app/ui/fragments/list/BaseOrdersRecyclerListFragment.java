package com.riskpointer.sdk.app.ui.fragments.list;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.merchant.HonkioMerchantApi;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.Order;
import com.riskpointer.sdk.api.model.entity.OrdersList;
import com.riskpointer.sdk.api.model.push.Push;
import com.riskpointer.sdk.api.model.task.Task;
import com.riskpointer.sdk.api.support.BroadcastManager;
import com.riskpointer.sdk.api.utils.BroadcastHelper;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.OrderFilter;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.utils.FragmentUtils;

import java.util.List;

/**
 * @author Shurygin Denis on 2015-04-10.
 */
public abstract class BaseOrdersRecyclerListFragment extends BaseRecycleLazyListFragment<Order> {

    public static final String ARG_FILTER = "BaseOrdersRecyclerListFragment.ARG_FILTER";

    private OrderFilter mFilter;
    private boolean isUseMerchantApi = false;

    @Override
    protected int getDefaultListItemLayoutId(int type) {
        return R.layout.hk_list_item_history_1;
    }

    @Override
    public void onDetach() {
        mAdapter = null;
        super.onDetach();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BroadcastManager.getInstance(getActivity()).registerReceiver(mOrderUpdateListener,
                new IntentFilter(BroadcastHelper.getActionOnComplete(Message.Command.USER_SET_ORDER)));

        BroadcastManager.getInstance(getActivity()).registerReceiver(mPushReceiver,
                new IntentFilter(BroadcastHelper.BROADCAST_ACTION_ON_PUSH_RECEIVED));

        if (savedInstanceState != null)
            mFilter = (OrderFilter) savedInstanceState.getSerializable(ARG_FILTER);
        else {
            Bundle args = getArguments();
            if (args != null && args.containsKey(ARG_FILTER))
                mFilter = (OrderFilter) args.getSerializable(ARG_FILTER);
            else
                mFilter = new OrderFilter();
        }
    }

    @Override
    public void onDestroy() {
        BroadcastManager.getInstance(getActivity()).unregisterReceiver(mOrderUpdateListener);
        BroadcastManager.getInstance(getActivity()).unregisterReceiver(mPushReceiver);
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mFilter != null)
            outState.putSerializable(ARG_FILTER, mFilter);
    }

    @Override
    public ListLoader<Order, OrdersList> onCreateLoader(int id, Bundle args) {
        OrdersListLoader loader = new OrdersListLoader(getContext(),
                getFilter(), FragmentUtils.getShopIdentity(this, null));
        loader.setUseMerchantApi(isUseMerchantApi);
        return loader;
    }

    public void setFilter(OrderFilter filter) {
        mFilter = filter;
    }

    public OrderFilter getFilter() {
        return mFilter;
    }

    public void setUseMerchantApi(boolean value) {
        isUseMerchantApi = value;
    }

    public boolean isUseMerchantApi() {
        return isUseMerchantApi;
    }

    protected void onOrderClick(Order order) {
        startActivity(AppController.getIntent(AppController.SCREEN_SHOP_ORDER, order.getOrderId()));
    }

    public static class OrdersListLoader extends ListLoader<Order, OrdersList> {
        protected OrderFilter mFilter;
        protected Identity mShop;

        private boolean isUseMerchantApi = false;

        public OrdersListLoader(Context context, OrderFilter filter, Identity shop) {
            super(context);
            mFilter = filter;
            mShop = shop;
        }

        public void setUseMerchantApi(boolean value) {
            isUseMerchantApi = value;
        }

        public boolean isUseMerchantApi() {
            return isUseMerchantApi;
        }

        @Override
        protected List<Order> toList(Response<OrdersList> response) {
            return response.getResult().getList();
        }

        @Override
        protected Task loadList(Context context, RequestCallback<OrdersList> callback,  int count, int skip) {
            OrderFilter filter = getFilter();
            setupFilter(filter, count, skip);

            if (isUseMerchantApi)
                return HonkioMerchantApi.merchantGetOrders(filter, mShop, 0, callback);
            else
                return HonkioApi.userGetOrders(filter, mShop,0, callback);
        }

        protected OrderFilter getFilter() {
            if (mFilter == null)
                mFilter = new OrderFilter();
            return mFilter;
        }

        protected void setupFilter(OrderFilter filter, int count, int skip) {
            filter.setCount(count);
            filter.setSkip(skip);
        }
    }

    private BroadcastReceiver mOrderUpdateListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            reloadList();
        }
    };

    private BroadcastReceiver mPushReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent!= null && intent.hasExtra(BroadcastHelper.BROADCAST_EXTRA_PUSH)) {
                Push<?> push = (Push<?>) intent.getSerializableExtra(BroadcastHelper.BROADCAST_EXTRA_PUSH);
                if (Push.Type.ORDER_STATUS_CHANGED.equals(push.getType()))
                    reloadList();
                else if(Push.Type.CHAT_MESSAGE.equals(push.getType())) {
                    notifyDataSetChanged();
                }
            }
        }
    };
}
