package com.riskpointer.sdk.app.ui.adapters;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.riskpointer.sdk.api.model.entity.Invoice;
import com.riskpointer.sdk.app.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @author KamiSempai on 2015-03-30.
 */
public class InvoiceCommunicationListAdapter extends BaseArrayListAdapter<InvoiceCommunicationListAdapter.Item, InvoiceCommunicationListAdapter.ViewHolder> {

    public InvoiceCommunicationListAdapter(Context context, Invoice invoice) {
        super(context, toList(invoice), R.layout.hk_list_item_invoice_communication);
    }

    @Override
    public ViewHolder newHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    public void bindHolder(ViewHolder holder, int position) {
        holder.text.setText(getItem(position).title);
    }

    @Override
    public void move(int position, int newPosition) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void remove(int position) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void changeList(List<Item> list) {
        throw new UnsupportedOperationException();
    }

    public static class Item {
        public static final int TYPE_INTERNAL = 1;
        public static final int TYPE_EXTERNAL = 2;

        public final int type;
        public final String title;
        public final String value;

        public Item(int type, String title, String value) {
            this.type = type;
            this.title = title;
            this.value = value;
        }

        public Item(Invoice.InternalContentItem item) {
            type = TYPE_INTERNAL;
            title = item.title;
            value = item.content;
        }

        public Item(Invoice.ExternalContentItem item) {
            type = TYPE_EXTERNAL;
            title = item.title;
            value = item.url;
        }
    }

    class ViewHolder {
        final TextView text;

        public ViewHolder(View view) {
            text = (TextView) view.findViewById(android.R.id.text1);
        }
    }

    private static ArrayList<Item> toList(Invoice invoice) {
        ArrayList<Item> list = new ArrayList<>();
        if (invoice.getOnlineLinkTitle() != null && invoice.getOnlineLinkUrl() != null)
            list.add(new Item(Item.TYPE_EXTERNAL, invoice.getOnlineLinkTitle(), invoice.getOnlineLinkUrl()));
        if (invoice.getInternalContents() != null)
            for (Invoice.InternalContentItem item: invoice.getInternalContents())
                list.add(new Item(item));
        if (invoice.getExternalContents() != null)
            for (Invoice.ExternalContentItem item: invoice.getExternalContents())
                list.add(new Item(item));
        return list;
    }
}
