package com.riskpointer.sdk.app.utils;

import com.riskpointer.sdk.api.model.entity.ServerInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Set;

/**
 * @author Shurygin Denis
 */

public class Countries {

    public static final List<ServerInfo.Country> LIST = new ArrayList<>();

    public static final Map<String, String> ISO2_TO_ISO3_MAP = new HashMap<>();
    static {
        String[] iso2Codes = Locale.getISOCountries();
        Set<String> addedCountries = new HashSet<>();
        for (String iso2 : iso2Codes) {
            Locale locale = new Locale(Locale.getDefault().getLanguage(), iso2);
            try {
                String iso3 = locale.getISO3Country().trim();
                if (iso3.length() > 0 && !addedCountries.contains(iso3)) {
                    addedCountries.add(iso3);
                    ServerInfo.Country country = new ServerInfo.Country();
                    country.setIso(iso3);
                    if (Locale.getDefault().getLanguage().isEmpty()) {
                        country.setName(locale.getDisplayCountry(Locale.ENGLISH));
                    } else {
                        country.setName(locale.getDisplayCountry());
                    }
                    country.setNameTranslated(locale.getDisplayCountry());
                    country.setLocale(locale.toString());
                    LIST.add(country);
                    ISO2_TO_ISO3_MAP.put(iso2, iso3);
                }
            }
            catch (MissingResourceException e) {
                // Throws in locale.getISO3Country()
                // Ignore this locale
            }
        }
        Collections.sort(LIST, new Comparator<ServerInfo.Country>() {
            @Override
            public int compare(ServerInfo.Country o1, ServerInfo.Country o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
    }

}
