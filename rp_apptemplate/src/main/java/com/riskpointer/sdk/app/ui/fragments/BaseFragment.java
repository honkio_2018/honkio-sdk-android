/* *******************************************************************************************************************
  Copyright (c) 2014 RiskPointer. All rights reserved.
 * *******************************************************************************************************************/

package com.riskpointer.sdk.app.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.web.callback.ErrorCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.app.FragmentComponents;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.activities.SingleFragmentActivity;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.ui.dialogs.MessageDialog;
import com.riskpointer.sdk.app.ui.strategy.ViewLifeCycleObserver;
import com.riskpointer.sdk.app.ui.strategy.ViewLifeCycleObserversSet;
import com.riskpointer.sdk.app.utils.FragmentUtils;
import com.riskpointer.sdk.app.utils.RequestCodes;

public abstract class BaseFragment extends Fragment implements ErrorCallback, View.OnClickListener,
        SingleFragmentActivity.SingleFragment, ViewLifeCycleObserver.ViewController,
        BaseDialogFragment.OnDismissListener, BaseDialogFragment.OnClickListener {
    public static final String ARG_LAYOUT_ID = "BaseFragment.ARG_LAYOUT_ID";
    public static final String ARG_LIST_ITEM_LAYOUT_ID = "BaseFragment.ARG_LIST_ITEM_LAYOUT_ID";
    public static final String ARG_LCO = "BaseFragment.ARG_LCO";

    private static final String DLG_TAG_PROGRESS = "BaseFragment.DLG_TAG_PROGRESS";

    private static final String SAVED_LCO_SET = "BaseFragment.SAVED_LCO_SET";

    private boolean isStateSaved = false;

    private final ViewLifeCycleObserversSet mLcoSet;

    public BaseFragment() {
        super();
        mLcoSet = new ViewLifeCycleObserversSet(this);
    }

// =================================================================================================
// Abstract methods
// =================================================================================================

    protected abstract int getDefaultLayoutId();

// =================================================================================================
// Methods of Super Class
// =================================================================================================

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Bundle args = getArguments();
        if (args != null && args.containsKey(ARG_LCO)) {
            Class<?> observerClass = (Class<?>) args.getSerializable(ARG_LCO);
            if (observerClass != null
                    && ViewLifeCycleObserver.class.isAssignableFrom(observerClass))
                try {
                    ViewLifeCycleObserver observer = (ViewLifeCycleObserver)
                            observerClass.newInstance();
                    addLifeCycleObserver(observer);
                    observer.onWillCreate(this, savedInstanceState);
                } catch (java.lang.InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
        }

        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            Bundle lcoBundle = savedInstanceState.getBundle(SAVED_LCO_SET);
            if (lcoBundle != null)
                mLcoSet.restoreState(savedInstanceState);
        }

        mLcoSet.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLcoSet.onViewCreated(savedInstanceState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle(SAVED_LCO_SET, mLcoSet.saveState());
        isStateSaved = true;
    }

    @Override
    public void onStart() {
        super.onStart();
        mLcoSet.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();

        isStateSaved = false;
        mLcoSet.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mLcoSet.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mLcoSet.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLcoSet.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mLcoSet.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        mLcoSet.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item) || mLcoSet.onOptionsItemSelected(item);
    }

// =================================================================================================
// Methods from Interfaces
// =================================================================================================

    @Override
    public boolean onError(RpError error) {
        return isAdded() && handleError(error);
    }

    @Override
    public String getScreenName() {
        return null;
    }

    @Override
    public String getTitle(Context context) {
        Shop shop = FragmentUtils.getShop(this);
        if (shop != null)
            return shop.getName();
        return "";
    }

    @Override
    public boolean onBackPressed() {
        return mLcoSet.onBackPressed();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onClickDialog(BaseDialogFragment dialogFragment, int which) {
        if (mLcoSet.onClickDialog(dialogFragment, which))
            return true;

        Fragment parent = getParentFragment();

        if (parent instanceof BaseDialogFragment.OnClickListener)
            return ((BaseDialogFragment.OnClickListener) parent).onClickDialog(dialogFragment, which);

        Activity activity = getActivity();
        if (activity instanceof BaseDialogFragment.OnClickListener)
            return ((BaseDialogFragment.OnClickListener) activity).onClickDialog(dialogFragment, which);

        return false;
    }

    @Override
    public boolean onDismissDialog(BaseDialogFragment dialogFragment) {
        if (mLcoSet.onDismissDialog(dialogFragment))
            return true;

        Fragment parent = getParentFragment();

        if (parent instanceof BaseDialogFragment.OnDismissListener)
            return ((BaseDialogFragment.OnDismissListener) parent).onDismissDialog(dialogFragment);

        Activity activity = getActivity();
        if (activity instanceof BaseDialogFragment.OnDismissListener)
            return ((BaseDialogFragment.OnDismissListener) activity).onDismissDialog(dialogFragment);

        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (permissions.length > 0 && requestCode == RequestCodes.REQUEST_PERMISSION) {
            if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED)
                onPermissionDenied(permissions[0], requestCode);
            else
                onPermissionGranted(permissions[0], requestCode);
        }
        else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

// =================================================================================================
// Public methods and classes
// =================================================================================================

    public void addLifeCycleObserver(ViewLifeCycleObserver observer) {
        mLcoSet.addLifeCycleObserver(observer);
    }

    public <T extends ViewLifeCycleObserver> T getLifeCycleObserver(String tag) {
        return mLcoSet.getLifeCycleObserver(tag);
    }

    public void removeLifeCycleObserver(final String tag) {
        mLcoSet.removeLifeCycleObserver(tag);
    }

    public void removeLifeCycleObserver(ViewLifeCycleObserver observer) {
        mLcoSet.removeLifeCycleObserver(observer);
    }

    @Override
    public <T extends View> T findViewById(int viewId) {
        View view = getView();
        if (view != null)
            return view.findViewById(viewId);
        return null;
    }

    public boolean handleError(RpError error) {
        Activity activity = getActivity();
        return activity instanceof ErrorCallback
                && ((ErrorCallback) activity).onError(error);
    }

    public void finish() {
        Activity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    public final void finish(int resultCode) {
        finish(resultCode, null);
    }

    public final void finish(int resultCode, Intent data) {
        Activity activity = getActivity();
        if (activity != null) {
            activity.setResult(resultCode, data);
            activity.finish();
        }
    }

    public void showProgressDialog() {
        showProgressDialog(null, null);
    }

    public void showProgressDialog(String tag) {
        showProgressDialog(null, null, tag);
    }

    public void showProgressDialog(int titleId, int messageId) {
        showProgressDialog(titleId, messageId, DLG_TAG_PROGRESS);
    }

    public void showProgressDialog(int titleId, int messageId, String tag) {
        if (isAdded())
            showProgressDialog(getString(titleId), getString(messageId));
        else
            showProgressDialog();
    }

    public void showProgressDialog(String title, String message) {
        showProgressDialog(title, message, DLG_TAG_PROGRESS);
    }

    public void showProgressDialog(String title, String message, String tag) {
        if (getChildFragmentManager().findFragmentByTag(tag) == null)
            showDialog(AppController.DIALOG_FRAGMENT_PROGRESS, tag, title, message);
    }

    public void dismissProgressDialog() {
        dismissDialog(DLG_TAG_PROGRESS);
    }

    public DialogFragment showMessage(String tag, int titleId, int messageId) {
        return showMessage(tag, titleId, messageId, MessageDialog.BTN_NOT_SET);
    }

    public DialogFragment showMessage(String tag, int titleId, int messageId, int buttonsVariant) {
        return showMessage(tag, titleId, messageId, buttonsVariant, null);
    }

    public DialogFragment showMessage(String tag, int titleId, int messageId, int buttonsVariant, Bundle bundle) {
        if (isAdded())
            return showMessage(tag, titleId != 0 ? getString(titleId) : null, getString(messageId), buttonsVariant, bundle);
        return null;
    }

    public DialogFragment showMessage(String tag, String title, String message) {
        return showMessage(tag, title, message, MessageDialog.BTN_NOT_SET);
    }

    public DialogFragment showMessage(String tag, String title, String message, int buttonsVariant) {
        return showMessage(tag, title, message, buttonsVariant, null);
    }

    public DialogFragment showMessage(String tag, String title, String message, int buttonsVariant, Bundle bundle) {
        return showDialog(AppController.DIALOG_FRAGMENT_MESSAGE, tag, title, message, buttonsVariant, bundle);
    }

    public DialogFragment showErrorMessage(String tag, RpError error) {
        return showErrorMessage(tag, error, R.string.dlg_unknown_error_title, R.string.dlg_unknown_error_message);
    }

    public DialogFragment showErrorMessage(String tag, RpError error, int defaultTitleId, int defaultMessageId) {
        if (isAdded()) {
            String title = getString(defaultTitleId);
            String message = error.description != null ? error.description.trim() : null;
            if (TextUtils.isEmpty(message))
                message = getString(defaultMessageId);
            return showMessage(tag, title, message);
        }
        return null;
    }

    @Override
    public DialogFragment showDialog(int dialogId, String tag, Object... params) {
        FragmentComponents components = AppController.getFragmentComponents(dialogId, params);
        if (components != null && components.isDialog()) {
            return showDialog(components.newDialog(), tag);
        }
        return null;
    }

    @Override
    public DialogFragment showDialog(DialogFragment dialog, String tag) {
        if (isAdded())
            getChildFragmentManager().beginTransaction().add(dialog, tag).commitAllowingStateLoss();
        return dialog;
    }

    @Override
    public void dismissDialog(String tag) {
        if(tag != null && isAdded()) {
            Fragment fragment = getChildFragmentManager().findFragmentByTag(tag);
            if (fragment instanceof DialogFragment)
                ((DialogFragment) fragment).dismiss();
        }
    }

    @Deprecated
    public boolean isFragmentStateSaved() {
        return isStateSaved;
    }

// =================================================================================================
// Protected methods and classes
// =================================================================================================

    protected void setTextIfExists(int textViewId, String text) {
        if (text != null && getView() != null) {
            TextView textView = getView().findViewById(textViewId);
            if (textView != null)
                textView.setText(text);
        }
    }

    protected void setVisibilityIfExists(int viewId, int visibility) {
        if (getView() != null) {
            View view = getView().findViewById(viewId);
            if (view != null)
                view.setVisibility(visibility);
        }
    }

    protected void setTextIfExists(TextView textView, String text) {
        if (text != null && textView != null) {
            textView.setText(text);
        }
    }

    protected void setVisibilityIfExists(View view, int visibility) {
        if (view != null)
            view.setVisibility(visibility);
    }

    protected int getLayoutId() {
        Bundle args = getArguments();
        if (args != null && args.containsKey(ARG_LAYOUT_ID))
            return args.getInt(ARG_LAYOUT_ID, getDefaultLayoutId());
        return getDefaultLayoutId();
    }

    protected void onTitleChanged() {
        Activity activity = getActivity();
        if (activity instanceof SingleFragmentActivity)
            ((SingleFragmentActivity) activity).refreshTitle();
    }

    protected boolean checkRuntimePermission(String permission) {
        boolean result = ContextCompat.checkSelfPermission(getContext(), permission)
                == PackageManager.PERMISSION_GRANTED;

        // Throw for SDK < VERSION_CODES.M
        // For this versions permission must be requested in manifest
        if (!result && Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            throw new SecurityException(
                    "Permission denied (missing " + permission + " permission?)");

        return result;
    }

    protected void onPermissionDenied(String permission, int requestCode) {
        mLcoSet.onPermissionDenied(permission, requestCode);
    }

    protected void onPermissionGranted(String permission, int requestCode) {
        mLcoSet.onPermissionGranted(permission, requestCode);
    }

// =================================================================================================
// Private methods and classes
// =================================================================================================
}