package com.riskpointer.sdk.app.ui.strategy;

import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;

/**
 * Created by Shurygin Denis on 2016-10-26.
 */

public class ProgressContainerManager extends BaseViewLifeCycleObserver {

    public static final String TAG = "ProgressContainerManager.TAG";

    public final static int ANIMATION_DURATION = 500;

    private int mProgressViewId;
    private int mContainerViewId;
    private View mProgressView;
    private View mContainerView;

    private boolean isShown = false;

    public ProgressContainerManager(int progressViewId, int containerViewId) {
        this(TAG, progressViewId, containerViewId);
    }

    public ProgressContainerManager(String tag, int progressViewId, int containerViewId) {
        super(tag);
        mProgressViewId = progressViewId;
        mContainerViewId = containerViewId;
    }

    @Override
    public void onViewCreated(ViewController viewController, Bundle savedInstanceState) {
        super.onViewCreated(viewController, savedInstanceState);

        mProgressView = viewController.findViewById(mProgressViewId);
        mContainerView = viewController.findViewById(mContainerViewId);

        if (isShown)
            showContent(false);
        else
            hideContent(false);
    }

    public void showContent(boolean animation) {
        isShown = true;
        if (mContainerView != null && mProgressView != null) {
            if (mContainerView.getVisibility() != View.VISIBLE) {
                mContainerView.setVisibility(View.VISIBLE);
                if (animation)
                    mContainerView.startAnimation(newInAnimation());
            }
            if (mProgressView.getVisibility() != View.INVISIBLE) {
                mProgressView.setVisibility(View.INVISIBLE);
                if (animation)
                    mProgressView.startAnimation(newOutAnimation());
            }
        }
    }

    public void hideContent(boolean animation) {
        isShown = false;
        if (mContainerView != null && mProgressView != null) {
            if (mContainerView.getVisibility() != View.INVISIBLE) {
                mContainerView.setVisibility(View.INVISIBLE);
                if (animation)
                    mContainerView.startAnimation(newOutAnimation());
            }
            if (mProgressView.getVisibility() != View.VISIBLE) {
                mProgressView.setVisibility(View.VISIBLE);
                if (animation)
                    mProgressView.startAnimation(newInAnimation());
            }
        }
    }

    protected AlphaAnimation newOutAnimation() {
        AlphaAnimation outAnimation = new AlphaAnimation(1f, 0f);
        outAnimation.setDuration(ANIMATION_DURATION);
        return outAnimation;
    }

    protected AlphaAnimation newInAnimation() {
        AlphaAnimation inAnimation = new AlphaAnimation(0f, 1f);
        inAnimation.setDuration(ANIMATION_DURATION);
        return inAnimation;
    }
}
