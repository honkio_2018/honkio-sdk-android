package com.riskpointer.sdk.app.utils.uri;

import android.database.Cursor;
import android.os.Handler;

import com.riskpointer.sdk.api.BaseHonkioApi;
import com.riskpointer.sdk.api.model.database.StampColumns;
import com.riskpointer.sdk.api.model.database.StampStorage;
import com.riskpointer.sdk.api.model.entity.Transaction;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;

import java.security.InvalidParameterException;
import java.security.SignatureException;
import java.text.ParseException;

/**
 * @author Shurygin Denis on 2015-08-19.
 */
@Deprecated
public abstract class StampValidationUriHandler implements UriHandler {
    public static final long SECOND_VALIDATION_DELAY = 600000;

    public static final int ERROR_UNKNOWN = 0;
    public static final int ERROR_INVALID_SIGNATURE = 1;
    public static final int ERROR_INVALID_SHOP = 2;
    public static final int ERROR_PARSE = 3;

    public abstract void onValidateComplete(ShopStamp stamp);
    public abstract void onValidateTwice(ShopStamp stamp);
    public abstract void onValidateError(int error);

    private BaseActivity mActivity;

    public StampValidationUriHandler(BaseActivity activity) {
        mActivity = activity;
    }

    @Override
    public boolean handle(String uriString) {
        new ValidationTask(uriString, System.currentTimeMillis()).start();
        return true;
    }

    public BaseActivity getActivity() {
        return mActivity;
    }

    private class ValidationTask extends Thread {
        private Handler mHandler;
        private String mTicketDataString;
        private long mValidationTime;

        public ValidationTask(String ticketDataString, long validationTime) {
            mHandler = new Handler();
            mTicketDataString = ticketDataString;
            mValidationTime = validationTime;
        }

        @Override
        public void run() {
            final StampStorage stampStorage = StampStorage.getInstance(mActivity);
            Cursor cursor = stampStorage.getTicketByString(mTicketDataString, SECOND_VALIDATION_DELAY);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    if (cursor.getInt(cursor.getColumnIndex(StampColumns.IS_VALID)) >= 1) {
                        notifyValidateTwice(new ShopStamp(cursor));
                        return;
                    }
                }
                cursor.close();
            }
            long id = 0;
            try {
                id = stampStorage.putStamp(mTicketDataString);
            } catch (InvalidParameterException e) {
                notifyValidateError(ERROR_INVALID_SHOP);
                return;
            } catch (SignatureException e) {
                notifyValidateError(ERROR_INVALID_SIGNATURE);
                return;
            } catch (ParseException e) {
                notifyValidateError(ERROR_PARSE);
                return;
            }
            if (id > 0) {
                cursor = stampStorage.getTicketById(id);
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        String transactionId = cursor.getString(cursor.getColumnIndex(StampColumns.TRANSACTION_ID));
                        final long stampId = id;
                        final String ticketName = cursor.getString(cursor.getColumnIndex(StampColumns.NAME));
                        BaseHonkioApi.shopStamp(transactionId, null, 0,
                                new RequestCallback<Transaction>() {
                            @Override
                            public boolean onComplete(Response<Transaction> response) {
                                stampStorage.updateStampFromResponse(response, stampId, ticketName);
                                ValidationTask.this.interrupt();
                                return true;
                            }

                            @Override
                            public boolean onError(RpError error) {
                                stampStorage.updateStampFromError(error, stampId);
                                // TODO  ReStampTask.schedule(mContext);
                                ValidationTask.this.interrupt();
                                return mActivity.onError(error);
                            }
                        });
                    } else {
                        notifyValidateError(ERROR_UNKNOWN);
                        cursor.close();
                        return;
                    }
                    cursor.close();
                } else {
                    notifyValidateError(ERROR_UNKNOWN);
                    return;
                }

                try {
                    Thread.sleep(mValidationTime);
                } catch (InterruptedException e) {
                    //Do nothing
                }

                cursor = stampStorage.getTicketById(id);
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        final ShopStamp stamp = new ShopStamp(cursor);
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                onValidateComplete(stamp);
                            }
                        });
                    } else {
                        notifyValidateError(ERROR_UNKNOWN);
                        cursor.close();
                        return;
                    }
                    cursor.close();
                } else
                    notifyValidateError(ERROR_UNKNOWN);
            } else
                notifyValidateError(ERROR_UNKNOWN);
        }

        private void notifyValidateError(final int error) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    onValidateError(error);
                }
            });
        }

        private void notifyValidateTwice(final ShopStamp stamp) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    onValidateTwice(stamp);
                }
            });
        }
    }

    public static class ShopStamp {
        public final String name;
        public final long readingTime;
        public final long validFrom;
        public final long validTo;
        public final boolean isValid;
        public final boolean isSentToServer;
        public  final int serverErrorCode;

        public ShopStamp(Cursor cursor) {
            name = getString(cursor, StampColumns.NAME);
            readingTime = getLong(cursor, StampColumns.READING_TIME);
            validFrom = getLong(cursor, StampColumns.VALID_FROM);
            validTo = getLong(cursor, StampColumns.VALID_TO);
            isValid = getInt(cursor, StampColumns.IS_VALID) >= 1;
            isSentToServer = getInt(cursor, StampColumns.IS_SENT_TO_SERVER) >= 1;
            serverErrorCode = getInt(cursor, StampColumns.SERVER_ERROR_CODE);
        }

        private String getString(Cursor cursor, String column) {
            int colId = cursor.getColumnIndex(column);
            if (colId > 0)
                cursor.getString(colId);
            return null;
        }

        private long getLong(Cursor cursor, String column) {
            int colId = cursor.getColumnIndex(column);
            if (colId > 0)
                cursor.getLong(colId);
            return 0;
        }

        private int getInt(Cursor cursor, String column) {
            int colId = cursor.getColumnIndex(column);
            if (colId > 0)
                cursor.getInt(colId);
            return 0;
        }
    }
}
