package com.riskpointer.sdk.app.ui.fragments;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.riskpointer.sdk.api.utils.ApiLog;
import com.riskpointer.sdk.api.utils.ApiWebUtils;
import com.riskpointer.sdk.app.R;

import java.util.Map;

/**
 * @author Shurygin Denis on 2015-04-08.
 */
public abstract class WebViewFragment extends BaseFragment {

    private View mProgressView;
    private WebView mWebView;

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_webview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mProgressView = view.findViewById(R.id.hk_progress);
        mWebView = view.findViewById(R.id.hk_webView);

        setupWebView(mWebView, mWebView.getSettings());

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                onPageLoaded(url);
                showWebView();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                ApiLog.d("WebView open url: " + url);
                hideWebView();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return onPageWillLoaded(url) || super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                if (ApiWebUtils.sIgnoreSSL)
                    handler.proceed(); // Ignore SSL certificate errors
                else
                    super.onReceivedSslError(view, handler, error);
            }
        });
    }

    @Override
    public boolean onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }
        return super.onBackPressed();
    }

    public void loadUrl(String url) {
        mWebView.loadUrl(url);
    }

    public void loadUrl(String url, Map<String, String> headers) {
        mWebView.loadUrl(url, headers);
    }

    public void showWebView() {
        mWebView.setVisibility(View.VISIBLE);
        mProgressView.setVisibility(View.INVISIBLE);
    }

    public void hideWebView() {
        mWebView.setVisibility(View.INVISIBLE);
        mProgressView.setVisibility(View.VISIBLE);
    }

    public void onPageLoaded(String url) {

    }

    protected boolean onPageWillLoaded(String url) {
        return false;
    }

    protected void setupWebView(WebView webView, WebSettings settings) {
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true);
    }
}
