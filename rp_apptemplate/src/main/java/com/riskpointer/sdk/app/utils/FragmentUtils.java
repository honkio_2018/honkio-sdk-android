package com.riskpointer.sdk.app.utils;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.Product;
import com.riskpointer.sdk.api.model.entity.Shop;

/**
 * Created by Shurygin Denis on 2016-06-02.
 */
public class FragmentUtils {

    public static final String ARG_PRODUCT = "FragmentUtils.ARG_PRODUCT";
    public static final String ARG_SHOP = "FragmentUtils.ARG_SHOP";

    public static void putPoduct(Fragment fragment, Product product) {
        Bundle args = fragment.getArguments();
        if (args == null)
            args = new Bundle();
        args.putSerializable(ARG_PRODUCT, product);
        fragment.setArguments(args);
    }

    public static Product getProduct(Fragment fragment) {
        Bundle args = fragment.getArguments();
        if (args != null)
            return (Product) args.getSerializable(ARG_PRODUCT);
        return null;
    }

    public static void putShop(Bundle args, Shop shop) {
        args.putSerializable(ARG_SHOP, shop);
    }

    public static void putShop(Fragment fragment, Shop shop) {
        Bundle args = fragment.getArguments();
        if (args == null)
            args = new Bundle();
        putShop(args, shop);
        fragment.setArguments(args);
    }

    public static Shop getShop(Fragment fragment) {
        return getShop(fragment, null);
    }

    public static Shop getAnyShop(Fragment fragment) {
        return getShop(fragment, HonkioApi.getMainShopInfo().getShop());
    }

    public static Shop getShop(Fragment fragment, Shop fallback) {
        Bundle args = fragment.getArguments();
        if (args != null && args.containsKey(ARG_SHOP)) {
            Object shop = args.getSerializable(ARG_SHOP);
            if (shop instanceof Shop)
                return (Shop) shop;
        }
        return fallback;
    }

    public static Identity getShopIdentity(Fragment fragment, Identity fallback) {
        Bundle args = fragment.getArguments();
        if (args != null && args.containsKey(ARG_SHOP))
            return (Identity) args.getSerializable(ARG_SHOP);
        return fallback;
    }
}
