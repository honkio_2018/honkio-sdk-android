package com.riskpointer.sdk.app.ui.strategy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.ui.dialogs.MessageDialog;
import com.riskpointer.sdk.app.utils.uri.UriHandler;

/**
 * @author Shurygin Denis
 */

public class BaseViewLifeCycleObserver implements ViewLifeCycleObserver {

    public static final String DLG_TAG_PROGRESS = "BaseViewLifeCycleObserver.DLG_TAG_PROGRESS";

    private static final String SAVED_TAG = "BaseViewLifeCycleObserver.SAVED_TAG";
    private static final String SAVED_PARENT_TAG = "BaseViewLifeCycleObserver.SAVED_PARENT_TAG";

    private String mTag;
    private String mParentTag;
    private ViewController mController;

    public BaseViewLifeCycleObserver() {
        this(null);
    }

    public BaseViewLifeCycleObserver(String tag) {
        mTag = tag;
    }

    @Override
    public String getTag() {
        return mTag;
    }

    @Override
    public void setParentLifeCycleObserverTag(String tag) {
        mParentTag = tag;
    }

    @Override
    public String getParentLifeCycleObserverTag() {
        return mParentTag;
    }

    @Override
    public <T extends ViewLifeCycleObserver> T getParentLifeCycleObserver() {
        if (mController != null)
            return mController.getLifeCycleObserver(getParentLifeCycleObserverTag());
        return null;
    }

    @Override
    public void onAttachToController(ViewController viewController) {
        mController = viewController;
    }

    @Override
    public void onDetachFromController(ViewController viewController) {
        mController = null;
    }

    @Override
    public ViewController getController() {
        return mController;
    }

    @Override
    public boolean isAttached() {
        return mController != null;
    }

    @Override
    public void saveState(ViewController viewController, Bundle outState) {
        outState.putString(SAVED_TAG, mTag);
        outState.putString(SAVED_PARENT_TAG, mParentTag);
    }

    @Override
    public void restoreState(ViewController viewController, Bundle savedInstanceState) {
        mController = viewController;
        mTag = savedInstanceState.getString(SAVED_TAG);
        mParentTag = savedInstanceState.getString(SAVED_PARENT_TAG);
    }

    @Override
    public void onWillCreate(ViewController viewController, Bundle savedInstanceState) { }

    @Override
    public void onCreate(ViewController viewController, Bundle savedInstanceState) { }

    @Override
    public void onViewCreated(ViewController viewController, Bundle savedInstanceState) { }

    @Override
    public void onStart(ViewController viewController) { }

    @Override
    public void onResume(ViewController viewController) { }

    @Override
    public void onPause(ViewController viewController) { }

    @Override
    public void onStop(ViewController viewController) { }

    @Override
    public void onDestroy(ViewController viewController) { }

    @Override
    public void onActivityResult(ViewController viewController, int requestCode, int resultCode, Intent data) { }

    @Override
    public boolean onCreateOptionsMenu(ViewController viewController, Menu menu, MenuInflater inflater) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(ViewController viewController, MenuItem item) {
        return onActionClick(viewController, item.getItemId());
    }

    public boolean onActionClick(ViewController viewController, int actionId) { return false; }

    @Override
    public boolean onBackPressed(ViewController viewController) {
        return false;
    }

    @Override
    public boolean onError(ViewController viewController, RpError error) {
        return false;
    }

    @Override
    public boolean onDismissDialog(ViewController viewController, BaseDialogFragment dialogFragment) {
        return false;
    }

    @Override
    public boolean onClickDialog(ViewController viewController, BaseDialogFragment dialogFragment, int which) {
        return false;
    }

    @Override
    public void onPermissionDenied(ViewController viewController, String permission, int requestCode) { }

    @Override
    public void onPermissionGranted(ViewController viewController, String permission, int requestCode) { }

    @Deprecated
    @Override
    public UriHandler getUriHandler(BaseActivity activity) {
        return null;
    }


    protected Context getContext() {
        if (mController == null)
            throwAttachFirst();
        return mController.getContext();
    }

    protected String getString(int stringId) {
        return getContext().getString(stringId);
    }

    protected String getString(int stringId, Object... formatArgs) {
        return getContext().getString(stringId, formatArgs);
    }

    protected DialogFragment showMessage(String tag, int titleId, int messageId) {
        return showMessage(tag, titleId, messageId, MessageDialog.BTN_NOT_SET);
    }

    protected DialogFragment showMessage(String tag, int titleId, int messageId, int buttonsVariant) {
        return showMessage(tag, titleId, messageId, buttonsVariant, null);
    }

    protected DialogFragment showMessage(String tag, int titleId, int messageId, int buttonsVariant, Bundle bundle) {
        if (isAttached())
            return showMessage(tag,
                    titleId != 0 ? getString(titleId) : null,
                    getString(messageId), buttonsVariant, bundle);
        return null;
    }

    protected DialogFragment showMessage(String tag, String title, String message) {
        return showMessage(tag, title, message, MessageDialog.BTN_NOT_SET);
    }

    protected DialogFragment showMessage(String tag, String title, String message, int buttonsVariant) {
        return showMessage(tag, title, message, buttonsVariant, null);
    }

    protected DialogFragment showMessage(String tag, String title, String message, int buttonsVariant, Bundle bundle) {
        return showDialog(AppController.DIALOG_FRAGMENT_MESSAGE, tag, title, message, buttonsVariant, bundle);
    }

    protected DialogFragment showErrorMessage(String tag, RpError error) {
        return showErrorMessage(tag, error, R.string.dlg_unknown_error_title, R.string.dlg_unknown_error_message);
    }

    protected DialogFragment showErrorMessage(String tag, RpError error, int defaultTitleId, int defaultMessageId) {
        if (isAttached()) {
            String title = getString(defaultTitleId);
            String message = error.description != null ? error.description.trim() : null;
            if (TextUtils.isEmpty(message))
                message = getString(defaultMessageId);
            return showMessage(tag, title, message);
        }
        return null;
    }

    protected DialogFragment showDialog(int dialogId, String tag, Object... params) {
        ViewController controller = mController;
        if (controller != null) {
            return controller.showDialog(dialogId, tag, params);
        }
        return null;
    }

    protected void dismissDialog(String tag) {
        ViewController controller = mController;
        if (controller != null) {
            controller.dismissDialog(tag);
        }
    }

    protected void showProgressDialog() {
        dismissDialog(DLG_TAG_PROGRESS);
        showDialog(AppController.DIALOG_FRAGMENT_PROGRESS, DLG_TAG_PROGRESS);
    }

    protected void dismissProgressDialog() {
        dismissDialog(DLG_TAG_PROGRESS);
    }

    protected void throwAttachFirst() {
        throw new IllegalStateException("ViewLifeCycleObserver should be attached.");
    }
}
