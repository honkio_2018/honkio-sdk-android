package com.riskpointer.sdk.app.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.BoughtProduct;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.api.model.entity.ShopFind;
import com.riskpointer.sdk.api.model.entity.Transaction;
import com.riskpointer.sdk.api.model.entity.Transactions;
import com.riskpointer.sdk.api.model.push.entity.TransactionPushEntity;
import com.riskpointer.sdk.api.utils.Currency;
import com.riskpointer.sdk.api.web.callback.ErrorCallback;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.PushFilter;
import com.riskpointer.sdk.api.web.message.filter.TransactionFilter;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;
import com.riskpointer.sdk.app.utils.FormatUtils;

import java.util.List;

/**
 * @author Shurygin Denis on 2015-04-08.
 */
public class TransactionDetailsFragment extends BaseFragment {

    public static final String EXTRA_TRANSACTION = "Transaction";
    public static final String EXTRA_TRANSACTION_PUSH = "TransactionPush";

    public static final int MAX_MESSAGES_ON_LIST = 3;

    protected Transaction mTransaction;

    @Override
    protected int getDefaultLayoutId() {
        return R.layout.hk_fragment_purchase_details;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mTransaction = (Transaction) getArguments().getSerializable(EXTRA_TRANSACTION);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(mTransaction != null) {
            updateUi();
        }
        else  {
            TransactionPushEntity push = (TransactionPushEntity) getArguments().getSerializable(EXTRA_TRANSACTION_PUSH);
            if (push != null) {
                loadTransaction(push);
            }
            else
                finish();
        }
    }

    protected void loadTransaction(final TransactionPushEntity pushEntity) {
        if (isAdded()) {
            setVisibilityIfExists(R.id.hk_content, View.GONE);
            setVisibilityIfExists(R.id.hk_progress, View.VISIBLE);

            HonkioApi.shopFindById(pushEntity.getShopId(), 0, new RequestCallback<ShopFind>() {
                @Override
                public boolean onComplete(Response<ShopFind> response) {
                    if (response.isStatusAccept() && response.getResult().getList().size() > 0) {
                        Shop shop = response.getResult().getList().get(0);
                        loadTransactionFromShop(pushEntity, shop);
                    } else
                        return onError(null);
                    return true;
                }

                @Override
                public boolean onError(RpError error) {
                    showMessage(BaseActivity.DLG_TAG_FINISH, 0, R.string.purchase_details_find_shop_fail);
                    return true;
                }
            });
        }
    }

    protected void loadTransactionFromShop(final TransactionPushEntity pushEntity, Identity shopIdentity) {
        if (isAdded()) {
            TransactionFilter filter = new TransactionFilter();
            filter.setId(pushEntity.getTransactionId());

            HonkioApi.userGetTransactions(filter, shopIdentity, 0,
                    new RequestCallback<Transactions>() {
                @Override
                public boolean onComplete(Response<Transactions> response) {
                    if (response.isStatusAccept() && response.getResult().getTransactionsList()
                            .size() > 0) {
                        if (isAdded()) {
                            mTransaction = response.getResult().getTransactionsList().get(0);
                            updateUi();

                            if (getView() != null) {
                                View content = getView().findViewById(R.id.hk_content);
                                View progress = getView().findViewById(R.id.hk_progress);
                                progress.startAnimation(AnimationUtils.loadAnimation(
                                        getActivity(), android.R.anim.fade_out));
                                content.startAnimation(AnimationUtils.loadAnimation(
                                        getActivity(), android.R.anim.fade_in));
                                content.setVisibility(View.VISIBLE);
                                progress.setVisibility(View.GONE);
                            }
                        }
                        return true;
                    } else
                        return onError(null);
                }

                @Override
                public boolean onError(RpError error) {
                    if (isAdded())
                        showMessage(BaseActivity.DLG_TAG_FINISH, 0, R.string.purchase_details_get_transaction_fail);
                    return true;
                }
            });
        }
    }

    protected void updateUi() {
        if (isAdded() && getView() != null) {
            setTextIfExists(R.id.hk_txtMerchantName, mTransaction.getMerchantName());
            setTextIfExists(R.id.hk_txtShopName, mTransaction.getShopName());
            setTextIfExists(R.id.hk_txtDate, FormatUtils.formatDate(getActivity(), mTransaction.getTimeStamp()));
            setTextIfExists(R.id.hk_txtPrice, FormatUtils.format(mTransaction.getAmount()));
            setTextIfExists(R.id.hk_txtCurrency, Currency.getSymbol(mTransaction.getCurrency()));
            setTextIfExists(R.id.hk_txtStatus, mTransaction.getStatus());

            if (mTransaction.getProductsCount() > 0) {
                setTextIfExists(R.id.hk_txtProduct, getProductsSummary(mTransaction.getProductsList()));
                setVisibilityIfExists(R.id.hk_layoutProduct, View.VISIBLE);
            }
            else {
                setTextIfExists(R.id.hk_txtProduct, "-");
                setVisibilityIfExists(R.id.hk_layoutProduct, View.GONE);
            }


            List<Transaction.TransactionMessage> messagesList = mTransaction.getMessagesList();
            if (messagesList == null || messagesList.size() == 0) {
                setVisibilityIfExists(R.id.hk_layoutMessages, View.GONE);
            } else {
                setVisibilityIfExists(R.id.hk_layoutMessages, View.VISIBLE);
                if (messagesList.size() > MAX_MESSAGES_ON_LIST)
                    setVisibilityIfExists(R.id.hk_txtMoreMessages, View.VISIBLE);
                else
                    setVisibilityIfExists(R.id.hk_txtMoreMessages, View.GONE);

                LinearLayout messagesLayout = (LinearLayout) getView().findViewById(R.id.hk_layoutMessagesList);
                if (messagesLayout != null) {
                    messagesLayout.removeAllViews();
                    LayoutInflater inflater = LayoutInflater.from(getActivity());
                    for (int i = 0; i < MAX_MESSAGES_ON_LIST && i < messagesList.size(); i++) {
                        Transaction.TransactionMessage message = messagesList.get(messagesList.size() - 1 - i);
                        View view = inflater.inflate(R.layout.hk_list_item_transaction_message, messagesLayout, false);
                        ((TextView) view.findViewById(R.id.hk_txtSender)).setText(message.from);
                        ((TextView) view.findViewById(R.id.hk_txtDate)).setText(FormatUtils.formatDate(getActivity(), message.timeStamp));
                        ((TextView) view.findViewById(R.id.hk_txtMessage)).setText(message.text);
                        messagesLayout.addView(view);
                    }
                }
            }

            View btnShowLocation = getView().findViewById(R.id.hk_btnShowLocation);
            if (btnShowLocation != null)
                btnShowLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Activity activity = getActivity();
                        showProgressDialog();
                        HonkioApi.shopFindById(mTransaction.getShopId(), 0,
                                new RequestCallback<ShopFind>() {

                            @Override
                            public boolean onError(RpError error) {
                                dismissProgressDialog();
                                if (activity instanceof ErrorCallback) {
                                    if (!((ErrorCallback) activity).onError(error))
                                        showFailMessage();
                                    return true;
                                }
                                return false;
                            }

                            @Override
                            public boolean onComplete(Response<ShopFind> response) {
                                dismissProgressDialog();
                                if (response.isStatusAccept()) {
                                    if (response.getResult().getList().size() > 0) {
                                        Shop item = response.getResult().getList().get(0);
                                        String uriBegin = "geo:" + item.getLatitude() + "," + item.getLongitude();
                                        String uriString = uriBegin + "?q=" + item.getLatitude() + "," + item.getLongitude();
                                        Uri uri = Uri.parse(uriString);
                                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
                                        activity.startActivity(intent);
                                    } else
                                        showFailMessage();
                                    return true;
                                } else
                                    return onError(response.getAnyError());
                            }

                            private void showFailMessage() {
                                Toast.makeText(activity, R.string.purchase_details_fail, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });

            View txtShopName = getView().findViewById(R.id.hk_txtShopName);
            if (txtShopName != null)
                txtShopName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(AppController.getIntent(
                                AppController.SCREEN_SHOP, mTransaction.getShopId()));
                    }
                });


            HonkioApi.userSetPush(true, new PushFilter()
                            .addContent(PushFilter.QUERY_CONTENT_TRANSACTION, mTransaction.getId()),
                    Message.FLAG_NO_WARNING, null);
        }
    }

    private String getProductsSummary(List<BoughtProduct> products) {
        StringBuilder builder = new StringBuilder();

        for (BoughtProduct product: products) {
            if (builder.length() > 0)
                builder.append(", ");
            builder.append(product.getName());
            if (product.getCount() > 1)
                builder.append("(")
                        .append(Integer.toString(product.getCount()))
                        .append(")");
        }
        return builder.toString();
    }
}
