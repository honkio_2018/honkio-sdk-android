package com.riskpointer.sdk.app.ui.fragments.settings;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ShareCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.riskpointer.sdk.app.R;
import com.riskpointer.sdk.app.ui.activities.SingleFragmentActivity;
import com.riskpointer.sdk.app.ui.adapters.BaseRecyclerListAdapter.OnItemClickListener;
import com.riskpointer.sdk.app.ui.adapters.BaseRecyclerListAdapter.OnItemLongClickListener;
import com.riskpointer.sdk.app.ui.adapters.LogsListAdapter;
import com.riskpointer.sdk.app.ui.adapters.LogsListAdapter.ViewHolder;
import com.riskpointer.sdk.app.ui.adapters.binder.SimpleListBinder;
import com.riskpointer.sdk.app.ui.adapters.recyclerdecorations.RecyclerDividerDefaultDecoration;
import com.riskpointer.sdk.app.ui.fragments.list.BaseRecycleListFragment;
import com.riskpointer.sdk.app.utils.ApiLogTracker;
import com.riskpointer.sdk.app.utils.ApiLogTracker.Record;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Created by Shurygin Denis on 2017-02-08.
 */

public class SettingsLogsListFragment extends BaseRecycleListFragment implements SearchView.OnQueryTextListener {

    private Selector mSelector;
    private MenuItem mItemShare;
    private SearchView mItemSearch;

    @Override
    protected int getDefaultListItemLayoutId(int type) {
        return R.layout.hk_list_item_log;
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager(Context context) {
        return new LinearLayoutManager(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final LogsListAdapter adapter = new LogsListAdapter(getContext(),
                ApiLogTracker.getInstance().records, getListItemLayoutId(0));
        mSelector = new Selector(adapter);
        adapter.addOnItemClickListener(new OnItemClickListener<Record, ViewHolder>() {
            @Override
            public void onItemClick(ViewHolder itemHolder, View view, Record record) {
                if (mSelector.selection.size() > 0) {
                    mSelector.switchSelection(record);
                }
                else {
                    Bundle args = new Bundle();
                    args.putSerializable(SettingsLogFragment.ARG_LOG_RECORD, record);
                    startActivity(SingleFragmentActivity.newIntent(getContext(), null,
                            SettingsLogFragment.class, args));
                }
            }
        });
        adapter.setOnItemLongClickListener(new OnItemLongClickListener<Record, ViewHolder>() {
            @Override
            public boolean onItemLongClick(ViewHolder itemHolder, Record record) {
                if (mSelector.selection.size() == 0) {
                    mSelector.switchSelection(record);
                    return true;
                }
                return false;
            }
        });

        adapter.addItemBinder(mSelector);

        setListAdapter(adapter);
        getListView().addItemDecoration(new RecyclerDividerDefaultDecoration());

        getListView().scrollToPosition(ApiLogTracker.getInstance().records.size() - 1);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.hk_log_context_menu, menu);
        mItemShare = menu.findItem(R.id.hk_action_share);
        mItemSearch = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.hk_action_filter));
        mItemSearch.setOnQueryTextListener(this);
    }

    private void toggleShareMenu() {
        boolean isHasSelection = mSelector.selection.size() > 0;
        mItemShare.setVisible(isHasSelection);
        mItemSearch.setVisibility(isHasSelection ? View.GONE : View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.hk_action_share) {
            showProgressDialog();
            new LogStringBuilderAsync(new LogStringBuilderAsync.Callback() {
                @Override
                public void onComplete(String result) {
                    dismissProgressDialog();
                    mSelector.resetSelection();
                    shareString(result);
                }
            }).execute(mSelector.selection.toArray(new Record[mSelector.selection.size()]));
            return true;
        }
        else if (item.getItemId() == R.id.hk_action_share_all) {
            showProgressDialog();
            new LogStringBuilderAsync(new LogStringBuilderAsync.Callback() {
                @Override
                public void onComplete(String result) {
                    dismissProgressDialog();
                    mSelector.resetSelection();
                    shareString(result);
                }
            }).execute();
            return true;
        }
        else if (item.getItemId() == R.id.hk_action_copy_all) {
            showProgressDialog();
            new LogStringBuilderAsync(new LogStringBuilderAsync.Callback() {
                @Override
                public void onComplete(String result) {
                    dismissProgressDialog();
                    copyToClipboard(result);
                }
            }).execute();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onBackPressed() {
        if (mSelector.selection.size() > 0) {
            mSelector.resetSelection();
            return true;
        }
        return super.onBackPressed();
    }

    @Override
    protected void reloadList() {
        getListAdapter().notifyDataSetChanged();
        setRefreshing(false);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        ((LogsListAdapter) getListAdapter()).filter(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        ((LogsListAdapter) getListAdapter()).filter(newText);
        return true;
    }

    private void shareString(String string) {
        ShareCompat.IntentBuilder builder
                = ShareCompat.IntentBuilder.from(getActivity())
                .setText(string)
                .setChooserTitle("Share logs")
                .setType("text/plain");

        builder.startChooser();
    }

    private void copyToClipboard(String string) {
        ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("label", string);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(getContext(), "Text copied", Toast.LENGTH_SHORT).show();
    }

    private static class LogStringBuilderAsync extends AsyncTask<Record, Void, String> {

        interface Callback {
            void onComplete(String result);
        }

        public Callback mCallback;

        LogStringBuilderAsync(Callback callback) {
            super();
            mCallback = callback;
        }

        @Override
        protected String doInBackground(Record... params) {
            SimpleDateFormat dateFormat
                    = new SimpleDateFormat(" (yyyy-MM-dd HH:mm:ss)", Locale.ENGLISH);
            StringBuilder builder = new StringBuilder();
            for (Record record: ApiLogTracker.getInstance().records) {
                if (params.length == 0 || paramsContains(params, record)) {
                    if (builder.length() > 0)
                        builder.append("\n\n");

                    builder.append(record.level.toString())
                            .append(dateFormat.format(record.time))
                            .append("\n")
                            .append(record.message);
                }
            }
            return builder.toString();
        }

        @Override
        protected void onPostExecute(String string) {
            if (mCallback != null)
                    mCallback.onComplete(string);
        }

        boolean paramsContains(Record[] params, Record record) {
            for (Record param: params)
                if (param == record)
                    return true;
            return false;
        }
    }

    private class Selector implements SimpleListBinder<Record, ViewHolder> {

        final List<Record> selection = new ArrayList<>();
        final RecyclerView.Adapter<?> mAdapter;

        public Selector(RecyclerView.Adapter<?> adapter) {
            super();
            mAdapter = adapter;
        }

        @Override
        public void bind(Context context, ViewHolder holder, Record record) {
            if (!selection.contains(record))
                holder.itemView.setBackgroundColor(Color.TRANSPARENT);
            else
                holder.itemView.setBackgroundColor(Color.DKGRAY);
        }

        public void switchSelection(Record record) {
            boolean isHasSelection = selection.size() > 0;
            if (!mSelector.selection.remove(record))
                mSelector.selection.add(record);
            if (isHasSelection != selection.size() > 0) {
                toggleShareMenu();
            }
            mAdapter.notifyDataSetChanged();
        }

        public void resetSelection() {
            mSelector.selection.clear();
            mAdapter.notifyDataSetChanged();
            toggleShareMenu();
        }
    }
}
