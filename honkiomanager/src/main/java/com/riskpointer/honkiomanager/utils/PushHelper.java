package com.riskpointer.honkiomanager.utils;

import android.content.Context;

import com.riskpointer.honkiomanager.R;
import com.riskpointer.sdk.api.model.push.Push;

/**
 * Created by Shurygin Denis on 2016-01-15.
 */
public class PushHelper {

    public static int iconByPush(Context context, Push<?> push) {
        return R.mipmap.ic_launcher;
    }

    public static String messageByPush(Context context, Push<?> push) {
        return push.getMessage();
    }
}
