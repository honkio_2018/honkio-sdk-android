package com.riskpointer.honkiomanager.api;

import android.content.Context;

import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.task.RequestTask;
import com.riskpointer.sdk.api.web.WebServiceAsync;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.message.Message;

import java.util.Map;

/**
 * Created by I.N. on 06.06.2016.
 */
public class ManagerApi extends HonkioApi {

    public static RequestTask<Void> pushMessage(Context context, RequestCallback<Void> callback, String messageString, Map<String, Object> payload, String[] userIds, int flags) {

        Message<Void> message = new Message<>(context, "notify",
                getUrl(),
                getActiveConnection(),
                getDevice(),
                getMainShopIdentity(),
                getAppIdentity(),
                HonkioApi.getProtocolFactory().newRequestBuilder(),
                HonkioApi.getProtocolFactory().newParser(Void.class),
                flags);

        message.addParameter("message", messageString);
        message.addParameter("payload", payload);
        if (userIds != null && userIds.length > 0)
            message.addParameter("user_ids", userIds);

        WebServiceAsync<Void> webService = new WebServiceAsync<>(getContext(), message, callback);
        webService.execute();
        return webService;
    }
}
