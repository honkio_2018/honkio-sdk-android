package com.riskpointer.honkiomanager;

import android.content.Context;

import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.webtask.MockWebTask;
import com.riskpointer.sdk.api.web.webtask.WebTask;
import com.riskpointer.sdk.api.web.webtask.WebTaskFactory;
import com.riskpointer.sdk.app.applications.BaseApplication;

/**
 * Created by I.N. on 03.06.2016.
 */
public class ManagerApplication extends BaseApplication {

    @Override
    public void onCreate() {
        WebTaskFactory.setInstance(new WebTaskFactoryMock());
        super.onCreate();
    }

    @Override
    protected ManagerAppController getAppController() {
        return new ManagerAppController(this);
    }

    private static class WebTaskFactoryMock extends WebTaskFactory.DefaultInstance {

        @Override
        public <T> WebTask<T> buildTask(Context context, Message<T> message) {
            if (message != null && message.getCommand() != null) {
                switch (message.getCommand()) {
                    case Message.Command.USER_INVENTORY_LIST:
                        return new MockWebTask<>(context, message,
                            "{\n" +
                            "    \"status\": \"accept\",\n" +
                            "    \"product\": [\n" +
                            "        {\n" +
                            "            \"count\": 1, \n" +
                            "            \"name\": \"Default Task\",\n" +
                            "            \"description\":\"Task description\",\n" +
                            "            \"currency\": \"EUR\",\n" +
                            "            \"amount\": \"1.00\",\n" +
                            "            \"amount_vat\": \"0.19\", \n" +
                            "            \"amount_no_vat\": \"0.81\", \n" +
                            "            \"amount_vat_percent\": 24.0, \n" +
                            "            \"id\": \"580f56ea284059821051eee1\",\n" +
                            "            \"item_id\": \"5819b09e284059ab4ad5c153\",\n" +
                            "            \"validity_time\": 1,\n" +
                            "            \"validity_time_type\": \"month\",\n" +
                            "            \"validfrom\": \"2016-11-02 09:30:24\",\n" +
                            "            \"validto\": \"2016-12-02 09:30:24\",\n" +
                            "            \"stamp\": \"2016-11-02 09:31:24\"\n" +
                            "        }],\n" +
                            "    \"hash\": \"foobar\",\n" +
                            "    \"timestamp\": \"2016-11-02 09:30:24\",\n" +
                            "    \"random\": \"wLYbQc9tMVKb86noZK1o1uPRzosiERwqep0BCypJ\"\n" +
                            "}", 1000);
                }
            }
            return super.buildTask(context, message);
        }
    }
}
