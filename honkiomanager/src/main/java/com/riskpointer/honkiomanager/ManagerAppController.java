package com.riskpointer.honkiomanager;

import android.content.Context;
import android.content.Intent;

import com.riskpointer.honkiomanager.ui.activitycontrollers.MainActivityLifeCycleObserver;
import com.riskpointer.honkiomanager.ui.fragments.NotificationsListFragment;
import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Identity;
import com.riskpointer.sdk.api.model.entity.Shop;
import com.riskpointer.sdk.app.FragmentComponents;
import com.riskpointer.sdk.app.appmodel.BaseUserAppController;
import com.riskpointer.sdk.app.ui.activities.BaseActivity;
import com.riskpointer.sdk.app.ui.activities.PagerActivity;


/**
 * Created by I.N. on 03.06.2016.
 */
public class ManagerAppController extends BaseUserAppController {
    public static final Identity APP_IDENTITY = new Identity(BuildConfig.APP_ID, BuildConfig.APP_PASSWORD);

    private static final String GCM_SENDER_ID = "381246911618";

    @Override
    public String getGcmSenderId() {
        return GCM_SENDER_ID;
    }
    public ManagerAppController(Context applicationContext) {
        super(applicationContext, BuildConfig.API_URL, APP_IDENTITY);
    }

    @Override
    public Intent buildNewIntent(int intentId, Object... params) {
        Intent intent = null;
        switch (intentId) {
            case ACTIVITY_MAIN:

                Shop shop = HonkioApi.getMainShopInfo().getShop();
                intent = PagerActivity.newIntent(mAppContext, mAppContext.getString(R.string.app_name),
                        new FragmentComponents[]{
                                getFragmentComponents(FRAGMENT_PUSHES_LIST)},
                        new String[]{
                                "Pushes"});
                intent.putExtra(BaseActivity.EXTRA_LCO, MainActivityLifeCycleObserver.class);
                break;
            default:
                intent = super.buildNewIntent(intentId, params);
        }
        return intent;
    }
    @Override
    public FragmentComponents buildNewFragmentComponents(int fragmentId, Object... params) {
        FragmentComponents components = super.buildNewFragmentComponents(fragmentId, params);

        switch (fragmentId) {
            case FRAGMENT_PUSHES_LIST:
                components.setFragmentClass(NotificationsListFragment.class);
                break;
        }
        return components;
    }
}
