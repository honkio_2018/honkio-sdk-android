package com.riskpointer.honkiomanager.ui.activitycontrollers;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.riskpointer.honkiomanager.R;
import com.riskpointer.honkiomanager.ui.fragments.InventoryListFragment;
import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.activities.SingleFragmentActivity;
import com.riskpointer.sdk.app.ui.strategy.BaseViewLifeCycleObserver;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by I.N. on 06.06.2016.
 */
public class MainActivityLifeCycleObserver extends BaseViewLifeCycleObserver {

    @Override
    public boolean onCreateOptionsMenu(ViewController viewController, Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(ViewController viewController, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.hk_action_settings:
                viewController.startActivity(AppController.getIntent(AppController.SCREEN_SETTINGS_MAIN));
                return true;
            case R.id.action_inventory:
                viewController.startActivity(SingleFragmentActivity.newIntent(
                        viewController.getContext(), "Inventory",
                        InventoryListFragment.class, null));
                return true;
            case R.id.testpush:
                Map<String, Object> payload = new HashMap<>();
                payload.put("type", "test");
//                payload.put("user_id", role.getUserId());
//                payload.put("user_name", userName.toString());
                payload.put("message", "This is a test message for a test push ");
                HonkioApi.userNotify("This is a test push.."+System.currentTimeMillis(), payload,
                        null, 0, new RequestCallback<Void>() {
                    @Override
                    public boolean onComplete(Response<Void> response) {
                        return true;
                    }

                    @Override
                    public boolean onError(RpError error) {
                        return true;
                    }
                });
                return true;
        }
        return super.onOptionsItemSelected(viewController, item);
    }
}
