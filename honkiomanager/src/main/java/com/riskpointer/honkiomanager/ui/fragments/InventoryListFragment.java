package com.riskpointer.honkiomanager.ui.fragments;

import com.riskpointer.honkiomanager.R;
import com.riskpointer.sdk.api.model.entity.InventoryProduct;
import com.riskpointer.sdk.api.utils.UriUtils;
import com.riskpointer.sdk.app.ui.dialogs.BarcodeDialog;
import com.riskpointer.sdk.app.ui.fragments.list.BaseInventoryListFragment;

/**
 * Created by Shurygin Denis on 2016-11-03.
 */

public class InventoryListFragment extends BaseInventoryListFragment {

    @Override
    protected int getDefaultListItemLayoutId(int type) {
        return R.layout.list_item_product;
    }

    @Override
    public void onItemClick(InventoryProduct item) {
        BarcodeDialog.build(item.getName(),
                UriUtils.buildInventoryProductUri(UriUtils.URI_AUTHORITY,
                item.getId())).show(getChildFragmentManager(), null, true);
    }
}
