package com.riskpointer.honkiomanager.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.riskpointer.honkiomanager.R;
import com.riskpointer.honkiomanager.utils.PushHelper;
import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.database.PushStorage;
import com.riskpointer.sdk.api.model.push.Push;
import com.riskpointer.sdk.api.model.push.entity.OrderStatusChangedPushEntity;
import com.riskpointer.sdk.api.web.message.Message;
import com.riskpointer.sdk.api.web.message.filter.PushFilter;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.adapters.PushesListAdapter;
import com.riskpointer.sdk.app.ui.fragments.list.PushesListFragment;
import com.riskpointer.sdk.app.utils.FormatUtils;

import java.util.HashMap;

/**
 * Created by Shurygin Denis on 2015-10-28.
 */
public class NotificationsListFragment extends PushesListFragment {

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setEmptyText(getString(R.string.empty_list_push));
    }

    @Override
    protected void onListItemClick(PushesListAdapter.ViewHolder itemHolder, View view, Push<?> push) {
        if (push != null) {
            switch (push.getType()) {
                case Push.Type.TEST:
                    showMessage(null, "Test push", push.getMessage() != null ? push.getMessage() : "Message is null!");
                    break;
                case Push.Type.NONE:
                    showMessage(null, "Unknown push",
                            (push.getMessage() != null ? "Message:" + push.getMessage() : "Message is null!")+
                                    "\n\nBody:\n" + push.getBody());
                    break;
                case Push.Type.ORDER_STATUS_CHANGED:
                    Intent intent = AppController.getIntent(AppController.SCREEN_PUSH, push);
                    if (intent != null)
                        startActivity(intent);
                    break;
               /* case WorkerApplyPushEntity.TYPE:
                    PushStorage.getInstance(getActivity()).removePush(id);
                    intent = AppController.getIntent(AppController.SCREEN_SHOP_ORDER, ((WorkerApplyPushEntity) push.getEntity()).getOrderId());
                    if (intent != null)
                        startActivity(intent);
                    break;*/
                default:
                    break;
            }

            PushFilter filter = new PushFilter();
            filter.setId(push.getId());

            HonkioApi.userSetPush(true, filter,
                    Message.FLAG_LOW_PRIORITY | Message.FLAG_NO_WARNING, null);
        }
    }

    public class ListAdapter extends CursorAdapter {
        private Context mContext;
        private LayoutInflater mInflater;
        private HashMap<Long, Push<?>> mCache = new HashMap<>();
        private PushStorage mPushStorage;

        public ListAdapter(Context context, Cursor cursor) {
            super(context, cursor);
            mContext = context;
            mInflater = LayoutInflater.from(context);
            mPushStorage = PushStorage.getInstance(context);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View view = mInflater.inflate(R.layout.list_item_push, parent, false);
            ViewHolder holder = new ViewHolder(view);
            view.setTag(holder);
            return view;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            ViewHolder holder = (ViewHolder) view.getTag();
            //Log.d("ManagerApp", "DUMP = "+ DatabaseUtils.dumpCursorToString(cursor));
            Long currentPushId = cursor.getLong(cursor.getColumnIndex(BaseColumns._ID));
            Push<?> push = getPush(currentPushId);

            //holder.icon.setImageResource(PushHelper.iconByPush(mContext, push));
            holder.title.setText(PushHelper.messageByPush(mContext, push));
            if (push.getDate() > 0)
                holder.date.setText(FormatUtils.formatDate(mContext, push.getDate()));
            else
                holder.dateHolder.setVisibility(View.GONE);

            switch (push.getType()) {
                case Push.Type.ORDER_STATUS_CHANGED:
                    OrderStatusChangedPushEntity entity = (OrderStatusChangedPushEntity) push.getEntity();
                    
                    holder.userName.setText(entity.getUserName());
                    holder.orderTitle.setText(entity.getOrderTitle());
                    break;
                /*case WorkerApplyPushEntity.TYPE:
                    WorkerApplyPushEntity workerEntity = (WorkerApplyPushEntity) push.getEntity();

                    holder.userName.setText(workerEntity.getUserName());
                    holder.orderTitle.setText(workerEntity.getOrderTitle());
                    break;*/
                default:
                    holder.userName.setText("");
                    holder.orderTitle.setText("");
            }
        }

        private Push<?> getPush(long id) {
            if (mCache.containsKey(id))
                return mCache.get(id);

            Push<?> push = mPushStorage.getPush(id);
            mCache.put(id, push);

            return push;
        }

        private class ViewHolder {
            public final View view;
            //public final ImageView icon;
            public final TextView title;
            public final TextView date;
            public final TextView userName;
            public final TextView orderTitle;
            public final View dateHolder;

            public ViewHolder(View view) {
                this.view = view;
                //icon = (ImageView) view.findViewById(R.id.list_item_push_icon);
                title = (TextView) view.findViewById(R.id.list_item_push_title);
                userName = (TextView) view.findViewById(R.id.list_item_push_username);
                date = (TextView) view.findViewById(R.id.list_item_push_date);
                orderTitle = (TextView) view.findViewById(R.id.list_item_push_job_title);
                dateHolder =  view.findViewById(R.id.list_item_push_date_holder);
            }
        }
    }

    @Override
    public String getTitle(Context context) {
        return context.getString(R.string.notifications_title);
    }

}