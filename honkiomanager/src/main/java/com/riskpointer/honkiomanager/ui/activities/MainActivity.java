package com.riskpointer.honkiomanager.ui.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.riskpointer.honkiomanager.R;
import com.riskpointer.honkiomanager.ui.fragments.InventoryListFragment;
import com.riskpointer.sdk.api.HonkioApi;
import com.riskpointer.sdk.api.model.entity.Inventory;
import com.riskpointer.sdk.api.web.callback.RequestCallback;
import com.riskpointer.sdk.api.web.error.RpError;
import com.riskpointer.sdk.api.web.response.Response;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.activities.PagerActivity;
import com.riskpointer.sdk.app.ui.activities.SingleFragmentActivity;

public class MainActivity extends PagerActivity {


    @Override
    protected int getDefaultLayoutId() {
        return 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HonkioApi.userGetInventory(0, new RequestCallback<Inventory>() {
            @Override
            public boolean onComplete(Response<Inventory> response) {
                return false;
            }

            @Override
            public boolean onError(RpError error) {
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.hk_action_settings:
                startActivity(AppController.getIntent(AppController.SCREEN_SETTINGS_MAIN));
                return true;
            case R.id.action_inventory:
                startActivity(SingleFragmentActivity.newIntent(this, "Inventory", InventoryListFragment.class, null));
                return true;
        }
        return true;
    }
}
