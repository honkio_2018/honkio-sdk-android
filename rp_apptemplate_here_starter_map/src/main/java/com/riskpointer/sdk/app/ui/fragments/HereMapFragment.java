package com.riskpointer.sdk.app.ui.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;

import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.ViewObject;
import com.here.android.mpa.mapping.Map.Animation;
import com.here.android.mpa.mapping.MapGesture;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapObject;
import com.here.android.mpa.mapping.SupportMapFragment;
import com.riskpointer.sdk.app.FragmentComponents;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.fragments.BaseMapFragment.MapAnnotation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HereMapFragment extends SupportMapFragment implements BaseMapFragment.MapView,
        OnEngineInitListener {

    private static final String DLG_TAG_INIT_ERROR = "HereMapFragment.DLG_TAG_INIT_ERROR";

    private volatile boolean isMapReady = false;
    private boolean isShowUserLocation = false;

    private final Map<MapMarker, MapAnnotation> mMarkersMap = new HashMap<>();

    private DeferredLocationMove mDeferredLocationMove;
    private final List<MapAnnotation> mDeferredAnnotations = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(this);
    }

    @Override
    public void onEngineInitializationCompleted(Error error) {
        if (error == OnEngineInitListener.Error.NONE) {
            isMapReady = true;

            setShowUserLocation(isShowUserLocation);

            synchronized (mDeferredAnnotations) {
                for (MapAnnotation annotation: mDeferredAnnotations) {
                    addAnnotation(annotation);
                }
                mDeferredAnnotations.clear();
            }

            DeferredLocationMove deferredMove = mDeferredLocationMove;
            if (deferredMove != null) {
                moveToLocation(deferredMove.location, deferredMove.zoom, false);
                mDeferredLocationMove = null;
            }

            getMapGesture().addOnGestureListener(new MapGesture.OnGestureListener() {
                @Override
                public void onPanStart() { }

                @Override
                public void onPanEnd() {
                    Fragment parent = getParentFragment();
                    if (parent instanceof BaseMapFragment) {
                        ((BaseMapFragment) parent).onCameraMove();
                    }
                }

                @Override
                public void onMultiFingerManipulationStart() { }

                @Override
                public void onMultiFingerManipulationEnd() {
                    onPanEnd();
                }

                @Override
                public boolean onMapObjectsSelected(List<ViewObject> list) {
                    if (list.size() > 0) {
                        ViewObject item = list.get(0);
                        if (item.getBaseType() == ViewObject.Type.USER_OBJECT) {
                            if (((MapObject) item).getType() == MapObject.Type.MARKER) {
                                MapMarker mapMarker = (MapMarker) item;

                                MapAnnotation annotation = mMarkersMap.get(mapMarker);
                                if (annotation != null) {
                                    Fragment parent = getParentFragment();
                                    if (parent instanceof BaseMapFragment) {
                                        BaseMapFragment mapFragment = (BaseMapFragment) parent;
                                        return mapFragment.onMapAnnotationClick(annotation);
                                    }
                                }
                            }
                        }
                    }
                    return false;
                }

                @Override
                public boolean onTapEvent(PointF pointF) {
                    Fragment parent = getParentFragment();
                    if (parent instanceof BaseMapFragment) {
                        GeoCoordinate coordinate = getMap().pixelToGeo(pointF);
                        Location location = new Location("");
                        location.setLatitude(coordinate.getLatitude());
                        location.setLongitude(coordinate.getLongitude());
                        return ((BaseMapFragment) parent).onMapClick(location);
                    }
                    return false;
                }

                @Override
                public boolean onDoubleTapEvent(PointF pointF) {
                    return false;
                }

                @Override
                public void onPinchLocked() { }

                @Override
                public boolean onPinchZoomEvent(float v, PointF pointF) {
                    return false;
                }

                @Override
                public void onRotateLocked() { }

                @Override
                public boolean onRotateEvent(float v) {
                    return false;
                }

                @Override
                public boolean onTiltEvent(float v) {
                    return false;
                }

                @Override
                public boolean onLongPressEvent(PointF pointF) {
                    return false;
                }

                @Override
                public void onLongPressRelease() { }

                @Override
                public boolean onTwoFingerTapEvent(PointF pointF) {
                    return false;
                }
            });
        }
        else {
            showDialog(AppController.DIALOG_FRAGMENT_MESSAGE,
                    DLG_TAG_INIT_ERROR, null, error.getDetails());
        }
    }

    @Override
    public boolean isMapReady() {
        return isMapReady;
    }

    @Override
    public void setShowUserLocation(boolean value) {
        isShowUserLocation = value;

        if (isMapReady) {
            //TODO
            getMap().getPositionIndicator().setVisible(isShowUserLocation);
        }
    }

    @Override
    public boolean isShowUserLocation() {
        return isShowUserLocation;
    }

    @Override
    public void addAnnotation(MapAnnotation annotation) {
        if (isMapReady) {
            MapMarker mapMarker = new MapMarker(locationToCoordinate(annotation.location))
                    .setTitle(annotation.title)
                    .setDescription(annotation.subtitle);

            if (annotation.isHasIcon()) {

                Bitmap bitmap;

                int resId = annotation.getIconResId();
                if (resId != 0)
                    bitmap = decodeIcon(resId);
                else
                    bitmap = annotation.getIconBitmap();

                if (bitmap != null) {
                    Image image = new Image();
                    image.setBitmap(bitmap);
                    mapMarker.setIcon(image);
                }
            }

            if (getMap().addMapObject(mapMarker)) {
                synchronized (mMarkersMap) {
                    mMarkersMap.put(mapMarker, annotation);
                }
            }
        }
        else {
            synchronized (mDeferredAnnotations) {
                mDeferredAnnotations.add(annotation);
            }
        }
    }

    @Override
    public void removeAnnotation(MapAnnotation annotation) {
        synchronized (mDeferredAnnotations) {
            mDeferredAnnotations.remove(annotation);
        }

        if (isMapReady) {
            synchronized (mMarkersMap) {
                for (Map.Entry<MapMarker, MapAnnotation> entry: mMarkersMap.entrySet())
                    if (annotation == entry.getValue()) {
                        MapMarker mapMarker = entry.getKey();
                        getMap().removeMapObject(mapMarker);
                        mMarkersMap.remove(mapMarker);
                        break;
                    }
            }
        }
    }

    @Override
    public void removeAllAnnotations() {
        synchronized (mDeferredAnnotations) {
            mDeferredAnnotations.clear();
        }

        if (isMapReady) {
            synchronized (mMarkersMap) {
                for (MapMarker mapMarker: mMarkersMap.keySet())
                    getMap().removeMapObject(mapMarker);
                mMarkersMap.clear();
            }
        }
    }

    @Override
    public void moveToLocation(Location location, float zoom, boolean animated) {
        if (isMapReady) {
            Animation animation = animated ? Animation.LINEAR : Animation.NONE;
            getMap().setZoomLevel(zoom, animation);
            getMap().setCenter(new GeoCoordinate(location.getLatitude(), location.getLongitude()),
                    animation);
        }
        else {
            mDeferredLocationMove = new DeferredLocationMove(location, zoom);
        }
    }

    @Override
    public Location getCenter() {
        if (!isMapReady())
            return null;

        GeoCoordinate center = getMap().getCenter();

        Location location = new Location("");
        location.setLatitude(center.getLatitude());
        location.setLongitude(center.getLongitude());

        return location;
    }

    @Override
    public double getRadius() {
        if (isMapReady) {
            GeoCoordinate center = getMap().getBoundingBox().getCenter();
            GeoCoordinate bottomRight = getMap().getBoundingBox().getBottomRight();

            float[] results = new float[1];
            Location.distanceBetween(
                    center.getLatitude(), center.getLongitude(),
                    bottomRight.getLatitude(), bottomRight.getLongitude(), results);

            return results[0];
        }
        return 0.0;
    }

    private DialogFragment showDialog(int dialogId, String tag, Object... params) {
        FragmentComponents components = AppController.getFragmentComponents(dialogId, params);
        if (components != null && components.isDialog()) {
            return showDialog(components.newDialog(), tag);
        }
        return null;
    }

    private DialogFragment showDialog(DialogFragment dialog, String tag) {
        if (isAdded())
            getChildFragmentManager().beginTransaction().add(dialog, tag).commitAllowingStateLoss();
        return dialog;
    }

    private GeoCoordinate locationToCoordinate(Location location) {
        return new GeoCoordinate(
                location.getLatitude(),
                location.getLongitude()
        );
    }

    private Bitmap decodeIcon(int resId) {
        Context context = getContext();

        if (context == null)
            return null;

        Drawable drawable = ContextCompat.getDrawable(context, resId);

        if (drawable == null)
            return null;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            drawable = (DrawableCompat.wrap(drawable)).mutate();

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private class DeferredLocationMove {

        Location location;
        float zoom;

        DeferredLocationMove(Location location, float zoom) {
            this.location = location;
            this.zoom = zoom;
        }

    }
}
