package com.riskpointer.sdk.app.Entities;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterItem;

public abstract class AbstractClusterMarker implements ClusterItem {
    protected double latitude;
    protected double longitude;

    protected MarkerOptions marker;
    protected boolean isSelected;

    @Override
    public LatLng getPosition() {
        return new LatLng(latitude, longitude);
    }

    protected AbstractClusterMarker(double latitude, double longitude) {
        setLatitude(latitude);
        setLongitude(longitude);
    }

    @Override
    public abstract String toString();

    public abstract MarkerOptions getMarker();

    public void setMarker(MarkerOptions marker) {
        this.marker = marker;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelected() {
        return isSelected;
    }
}
