package com.riskpointer.sdk.app.Entities;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class ItemClusterMarker<T> extends AbstractClusterMarker {
    private String description;
    private T item;

    public ItemClusterMarker(double latitude, double longitude, String description, T asset, boolean isSelected) {
        super(latitude,longitude);
        setDescription(description);
        setSelected(isSelected);
        setItem(asset);
        setMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude)).title(getDescription()));
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public MarkerOptions getMarker() {
        return null;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        if (getMarker()!=null) getMarker().title(getDescription());
    }

    public void setPicture(BitmapDescriptor picture) {
        if (getMarker()!=null) getMarker().icon(picture);
    }

    public void setItem(T asset) {
        this.item = asset;
    }

    public T getItem() {
        return item;
    }
}
