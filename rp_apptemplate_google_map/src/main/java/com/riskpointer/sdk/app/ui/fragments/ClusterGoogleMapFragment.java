package com.riskpointer.sdk.app.ui.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.riskpointer.sdk.app.Entities.ItemClusterMarker;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shurygin Denis
 */
public abstract class ClusterGoogleMapFragment<T> extends GoogleMapFragment {

    public interface ClusterMap<T> {
        void onItemsListClick(List<T> list);
    }

    public static final String ARG_FILTER = "BaseClusterAssetMapFragment.ARG_FILTER";

    private ClusterManager<ItemClusterMarker<T>> mClusterManager;

    protected abstract ItemClusterMarker<T> itemToClusterMarker(T item);

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        super.onMapReady(googleMap);

        if (mClusterManager == null)
            mClusterManager = new ClusterManager<>(getActivity(), googleMap);

        mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<ItemClusterMarker<T>>() {
            @Override
            public boolean onClusterItemClick(ItemClusterMarker<T> item) {
                onItemsListClick(getListFromItem(item.getItem()));
                return false;
            }
        });
        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                mClusterManager.onCameraIdle();
            }
        });
        mClusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<ItemClusterMarker<T>>() {
            @Override
            public boolean onClusterClick(Cluster<ItemClusterMarker<T>> cluster) {
                onItemsListClick(getListFromClusters(cluster));
                return false;
            }
        });

        googleMap.setOnMarkerClickListener(mClusterManager);
        mClusterManager.setRenderer(new AssetIconRendered<>(getActivity(), googleMap, mClusterManager));
    }

    public boolean cluster() {
        if (mClusterManager != null) {
            mClusterManager.cluster();
            return true;
        }
        return false;
    }

    @Override
    public void removeAllAnnotations() {
        super.removeAllAnnotations();
        if (mClusterManager != null)
            mClusterManager.clearItems();
    }

    protected void onItemsListClick(List<T> list) {
        Fragment parent = getParentFragment();
        if (parent instanceof ClusterMap)
            //noinspection unchecked
            ((ClusterMap<T>) parent).onItemsListClick(list);
    }

    @Override
    protected Marker addAnnotationToMap(GoogleMap googleMap, BaseMapFragment.MapAnnotation annotation) {
        T item = annotation.getItem();
        ItemClusterMarker<T> clusterMarker = itemToClusterMarker(item);
        if (clusterMarker != null)
            mClusterManager.addItem(clusterMarker);
        return null;
    }

    private List<T> getListFromClusters(Cluster<ItemClusterMarker<T>> cluster) {
        ArrayList<T> list = new ArrayList<>();
        for (ItemClusterMarker<T> itemClusterMarker : cluster.getItems()) {
            list.add(itemClusterMarker.getItem());
        }
        return list;
    }

    private List<T> getListFromItem(T asset) {
        ArrayList<T> list = new ArrayList<>();
        list.add(asset);
        return list;
    }

    private class AssetIconRendered<ClusterType> extends DefaultClusterRenderer<ItemClusterMarker<ClusterType>> {

        AssetIconRendered(Context context, GoogleMap map,
                                 ClusterManager<ItemClusterMarker<ClusterType>> clusterManager) {
            super(context, map, clusterManager);
        }

        @Override
        protected void onClusterRendered(Cluster<ItemClusterMarker<ClusterType>> cluster, Marker marker) {
            super.onClusterRendered(cluster, marker);
        }

        @Override
        protected void onBeforeClusterItemRendered(ItemClusterMarker<ClusterType> item,
                                                   MarkerOptions markerOptions) {
            markerOptions.title(item.getDescription());
            super.onBeforeClusterItemRendered(item, markerOptions);
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster<ItemClusterMarker<ClusterType>> cluster) {
            return cluster.getSize() >= 2;
        }
    }

}
