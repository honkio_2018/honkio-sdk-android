package com.riskpointer.sdk.app.ui.fragments;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Pair;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.riskpointer.sdk.app.FragmentComponents;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.map.R;
import com.riskpointer.sdk.app.ui.dialogs.BaseDialogFragment;
import com.riskpointer.sdk.app.ui.fragments.BaseMapFragment.MapAnnotation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Shurygin Denis
 */
public class GoogleMapFragment extends SupportMapFragment implements BaseMapFragment.MapView,
        OnMapReadyCallback, BaseDialogFragment.OnClickListener, GoogleMap.OnMyLocationChangeListener,
        GoogleMap.OnCameraMoveListener, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener {

    private static final String DLG_TAG_NO_PLAY_SERVICES = "BaseMapFragment.DLG_TAG_NO_PLAY_SERVICES";

    private GoogleMap mMap;
    private boolean isShowUserLocation = false;
    private final Map<String, Pair<MapAnnotation, Marker>> mMarkersMap = new HashMap<>();

    private DeferredLocationMove mDeferredLocationMove;
    private final List<MapAnnotation> mDeferredAnnotations = new ArrayList<>();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getMapAsync(this);
        checkGoogleServices();
    }

    @Override
    public boolean onClickDialog(BaseDialogFragment dialogFragment, int which) {
        if (DLG_TAG_NO_PLAY_SERVICES.equals(dialogFragment.getTag())) {

            if (which == DialogInterface.BUTTON_POSITIVE) {
                try {
                    Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.gms"));
                    startActivity(myIntent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getContext(),
                            "No application can handle this request. Please install a web browser",
                            Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;

        googleMap.setOnCameraMoveListener(this);
        googleMap.setOnMapClickListener(this);

        setShowUserLocation(isShowUserLocation);

        synchronized (mDeferredAnnotations) {
            for (MapAnnotation annotation: mDeferredAnnotations) {
                addAnnotation(annotation);
            }
            mDeferredAnnotations.clear();
        }

        DeferredLocationMove deferredMove = mDeferredLocationMove;
        if (deferredMove != null) {
            moveToLocation(deferredMove.location, deferredMove.zoom, false);
            mDeferredLocationMove = null;
        }
    }

    @Override
    public void onCameraMove() {
        Fragment parent = getParentFragment();
        if (parent instanceof BaseMapFragment) {
            ((BaseMapFragment) parent).onCameraMove();
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Fragment parent = getParentFragment();
        if (parent instanceof BaseMapFragment) {

            Location location = new Location("");
            location.setLatitude(latLng.latitude);
            location.setLongitude(latLng.longitude);

            ((BaseMapFragment) parent).onMapClick(location);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Pair<MapAnnotation, Marker> annotation = mMarkersMap.get(marker.getId());
        if (annotation != null && annotation.first != null) {
            Fragment parent = getParentFragment();
            if (parent instanceof BaseMapFragment) {
                return ((BaseMapFragment) parent).onMapAnnotationClick(annotation.first);
            }
        }
        return false;
    }

    @Override
    public void onMyLocationChange(Location location) {
        Fragment parent = getParentFragment();
        if (parent instanceof BaseMapFragment) {
            ((BaseMapFragment) parent).onUserLocationChange(location);
        }
    }

    @Override
    public boolean isMapReady() {
        return mMap != null;
    }

    @Override
    public void moveToLocation(Location location, float zoom, boolean animated) {
        if (mMap != null) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLng, zoom);

            if (animated)
                mMap.animateCamera(update);
            else
                mMap.moveCamera(update);
        }
        else {
            mDeferredLocationMove = new DeferredLocationMove(location, zoom);
        }
    }

    @Override
    public void setShowUserLocation(boolean value) {
        isShowUserLocation = value;
        if (mMap != null)
            mMap.setMyLocationEnabled(value);
    }

    @Override
    public boolean isShowUserLocation() {
        return isShowUserLocation;
    }

    @Override
    public void addAnnotation(MapAnnotation annotation) {
        if (mMap != null) {
            Marker marker = addAnnotationToMap(mMap, annotation);
            if (marker != null) {
                synchronized (mMarkersMap) {
                    mMarkersMap.put(marker.getId(), new Pair<>(annotation, marker));
                }
            }
        }
        else {
            synchronized (mDeferredAnnotations) {
                mDeferredAnnotations.add(annotation);
            }
        }
    }

    @Override
    public void removeAnnotation(MapAnnotation annotation) {
        synchronized (mDeferredAnnotations) {
            mDeferredAnnotations.remove(annotation);
        }

        if (mMap != null) {
            Marker marker = null;
            synchronized (mMarkersMap) {
                for (Pair<MapAnnotation, Marker> pair : mMarkersMap.values()) {
                    if (annotation == pair.first) {
                        marker = pair.second;
                        break;
                    }
                }
                if (marker != null) {
                    mMarkersMap.remove(marker.getId());
                    marker.remove();
                }
            }
        }
    }

    @Override
    public void removeAllAnnotations() {
        synchronized (mDeferredAnnotations) {
            mDeferredAnnotations.clear();
        }

        synchronized (mMarkersMap) {
            for (Pair<?, Marker> pair : mMarkersMap.values()) {
                pair.second.remove();
            }
            mMarkersMap.clear();
        }
    }

    @Override
    public Location getCenter() {
        if (!isMapReady())
            return null;

        LatLng latLng = mMap.getCameraPosition().target;

        Location location = new Location("");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);

        return location;
    }

    @Override
    public double getRadius() {
        GoogleMap googleMap = mMap;
        if (googleMap != null) {

            VisibleRegion visibleRegion = googleMap.getProjection().getVisibleRegion();

            float[] results = new float[1];
            Location.distanceBetween(
                    visibleRegion.latLngBounds.getCenter().latitude,
                    visibleRegion.latLngBounds.getCenter().longitude,
                    visibleRegion.latLngBounds.northeast.latitude,
                    visibleRegion.latLngBounds.northeast.longitude,
                    results);

            return results[0];
        }

        return 0.0;
    }

    protected Marker addAnnotationToMap(GoogleMap googleMap, MapAnnotation annotation) {
        MarkerOptions options = new MarkerOptions()
                .title(annotation.title)
                .position(new LatLng(annotation.location.getLatitude(),
                        annotation.location.getLongitude()));
        if (annotation.isHasIcon()) {
            int resId = annotation.getIconResId();
            if (resId != 0)
                options.icon(BitmapDescriptorFactory.fromResource(resId));
            else {
                Bitmap bitmap = annotation.getIconBitmap();
                if (bitmap != null)
                    options.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
            }
        }
        return googleMap.addMarker(options);
    }

    private void checkGoogleServices() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        if (googleApiAvailability.isGooglePlayServicesAvailable(getContext()) != ConnectionResult.SUCCESS) {
            showDialog(
                AppController.DIALOG_FRAGMENT_MESSAGE,
                DLG_TAG_NO_PLAY_SERVICES,
                getString(R.string.dlg_map_not_available_no_play_services_title),
                getString(R.string.dlg_map_not_available_no_play_services_message),
                getString(R.string.dlg_map_not_available_no_play_services_btn_ok),
                getString(R.string.dlg_map_not_available_no_play_services_btn_cancel));
        }
    }

    private DialogFragment showDialog(int dialogId, String tag, Object... params) {
        FragmentComponents components = AppController.getFragmentComponents(dialogId, params);
        if (components != null && components.isDialog()) {
            return showDialog(components.newDialog(), tag);
        }
        return null;
    }

    private DialogFragment showDialog(DialogFragment dialog, String tag) {
        if (isAdded())
            getChildFragmentManager().beginTransaction().add(dialog, tag).commitAllowingStateLoss();
        return dialog;
    }

    private class DeferredLocationMove {

        Location location;
        float zoom;

        DeferredLocationMove(Location location, float zoom) {
            this.location = location;
            this.zoom = zoom;
        }

    }
}
