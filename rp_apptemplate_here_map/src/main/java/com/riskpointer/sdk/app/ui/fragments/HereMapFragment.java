package com.riskpointer.sdk.app.ui.fragments;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.SupportMapFragment;
import com.riskpointer.sdk.app.FragmentComponents;
import com.riskpointer.sdk.app.appmodel.AppController;
import com.riskpointer.sdk.app.ui.fragments.BaseMapFragment.MapAnnotation;

import java.util.ArrayList;
import java.util.List;

public class HereMapFragment extends SupportMapFragment implements BaseMapFragment.MapView,
        OnEngineInitListener {

    private static final String DLG_TAG_INIT_ERROR = "HereMapFragment.DLG_TAG_INIT_ERROR";

    private volatile boolean isMapReady = false;
    private boolean isShowUserLocation = false;

    private DeferredLocationMove mDeferredLocationMove;
    private final List<MapAnnotation> mDeferredAnnotations = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(this);
    }

    @Override
    public void onEngineInitializationCompleted(Error error) {
        if (error == OnEngineInitListener.Error.NONE) {
            isMapReady = true;

            setShowUserLocation(isShowUserLocation);

            synchronized (mDeferredAnnotations) {
                for (MapAnnotation annotation: mDeferredAnnotations) {
                    addAnnotation(annotation);
                }
                mDeferredAnnotations.clear();
            }

            DeferredLocationMove deferredMove = mDeferredLocationMove;
            if (deferredMove != null) {
                moveToLocation(deferredMove.location, deferredMove.zoom, false);
                mDeferredLocationMove = null;
            }
        }
        else {
            showDialog(AppController.DIALOG_FRAGMENT_MESSAGE,
                    DLG_TAG_INIT_ERROR, null, error.getDetails());
        }
    }

    @Override
    public boolean isMapReady() {
        return isMapReady;
    }

    @Override
    public void setShowUserLocation(boolean value) {
        isShowUserLocation = value;

        if (isMapReady) {
            //TODO
        }
    }

    @Override
    public boolean isShowUserLocation() {
        return isShowUserLocation;
    }

    @Override
    public void addAnnotation(MapAnnotation annotation) {
        if (isMapReady) {
            //TODO
        }
        else {
            synchronized (mDeferredAnnotations) {
                mDeferredAnnotations.add(annotation);
            }
        }
    }

    @Override
    public void removeAnnotation(MapAnnotation annotation) {
        synchronized (mDeferredAnnotations) {
            mDeferredAnnotations.remove(annotation);
        }

        if (isMapReady) {
            //TODO
        }
    }

    @Override
    public void removeAllAnnotations() {
        synchronized (mDeferredAnnotations) {
            mDeferredAnnotations.clear();
        }

        if (isMapReady) {
            //TODO
        }
    }

    @Override
    public void moveToLocation(Location location, float zoom, boolean animated) {
        if (isMapReady) {
            Map map = getMap();
            map.setCenter(new GeoCoordinate(location.getLatitude(), location.getLongitude()),
                    animated ? Map.Animation.BOW : Map.Animation.NONE);
            map.setZoomLevel(zoom, animated ? Map.Animation.BOW : Map.Animation.NONE);
        }
        else {
            mDeferredLocationMove = new DeferredLocationMove(location, zoom);
        }

    }

    @Override
    public Location getCenter() {
        if (!isMapReady())
            return null;

        GeoCoordinate center = getMap().getCenter();

        Location location = new Location("");
        location.setLatitude(center.getLatitude());
        location.setLongitude(center.getLongitude());

        return location;
    }

    @Override
    public double getRadius() {
        return 0; //TODO
    }

    private DialogFragment showDialog(int dialogId, String tag, Object... params) {
        FragmentComponents components = AppController.getFragmentComponents(dialogId, params);
        if (components != null && components.isDialog()) {
            return showDialog(components.newDialog(), tag);
        }
        return null;
    }

    private DialogFragment showDialog(DialogFragment dialog, String tag) {
        if (isAdded())
            getChildFragmentManager().beginTransaction().add(dialog, tag).commitAllowingStateLoss();
        return dialog;
    }

    private class DeferredLocationMove {

        Location location;
        float zoom;

        DeferredLocationMove(Location location, float zoom) {
            this.location = location;
            this.zoom = zoom;
        }

    }
}
